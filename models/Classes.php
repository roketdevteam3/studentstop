<?php
namespace app\models;

use app\models\user\User;
use app\models\user\UserFriendRequest;
use app\modules\users\models\UserInfo;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class Classes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%class}}';
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
            $this->creator = \Yii::$app->user->id;
            if(\Yii::$app->user->identity->group == 'admin'){
                $this->status = 1;
            }else{
                $this->status = 0;
            }
        }

        return parent::beforeSave($insert);
    } 
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_create',
                'updatedAtAttribute' => 'date_update',
                'value' => new Expression('NOW()'),
            ],
            'slug' => [
                'class'         => 'app\behaviors\Slug',
                'in_attribute'  => 'name',
                'out_attribute' => 'url_name',
                'translit'      => true
            ]
        ];
    }
    
    public function scenarios()
    {
        return [
            'add_class' => ['name', 'id_course', 'url_name','professor','description', 'img_src', 'id_university', 'id_major'],
            'class_update' => ['name', 'id_course', 'url_name','professor','description', 'img_src', 'id_university', 'id_major'],
            'status_update' => ['status', 'date_active']
        ];
    }
    
    public function rules()
    {
        return [
            [['name','id_course'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Course name',
            'id_course' => 'Course',
            'url_name' => 'Url name',
            'description' => 'Description',
        ];
    }
    
}
