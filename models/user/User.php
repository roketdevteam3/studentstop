<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 25.02.16
 * Time: 21:44
 * Email: mackraiscms@gmail.com
 * Site: http://mackrais.tk
 */
namespace app\models\user;

use app\models\ClassUser;
use app\modules\users\models\User as BaseUser;
use app\models\user\UserInfo;


/**
* User extendet by modules
*/
class User extends BaseUser
{
	
	 /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserInfos()
    {
        return $this->hasOne(UserInfo::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClass()
    {
        return $this->hasOne(ClassUser::className(), ['id_user' => 'id']);
    }

    public function getFriendsRequest()
    {
        return $this->hasOne(UserFriendRequest::className(), ['who_send' => 'id']);
    }

    public function getFriends()
    {
        return $this->hasOne(UserFriend::className(), ['id_friend' => 'id']);
    }

    public function getUsertUniversity(){
        $modelUserInfo = UserInfo::find()->where(['id_user' => \Yii::$app->user->id])->one();
        if(($modelUserInfo != null) && ($modelUserInfo->university_id != 0)){
            return $modelUserInfo->university_id;
        }else{
            return false;
        }
    }
    
    public function checkFriendRequest($whom_send = 0, $who_send = 0){
        return UserFriendRequest::find()->where(
            [
                'whom_send'=>$whom_send ? $whom_send : $this->id,
                'who_send'=>$who_send ? $who_send : \Yii::$app->user->getId(),
            ]
        )
            ->one();
    }

}