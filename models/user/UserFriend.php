<?php
/**
 * Created by PhpStorm.
 * User: minace
 * Date: 3/3/16
 * Time: 8:09 PM
 */

namespace app\models\user;

use Yii;

/**
 * This is the model class for table "{{%user_friend}}".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_friend
 * @property integer $type
 */
class UserFriend extends \yii\db\ActiveRecord
{
    const TYPE_FRIEND_CLASS = 1;
    const TYPE_FRIEND_PROFESSOR = 2;
    const TYPE_FRIEND_UNIVERSITY = 3;
    const TYPE_FRIEND_OTHER = 4;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_friend}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_friend'], 'required'],
            [['id_user', 'id_friend'], 'integer'],
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user' => Yii::t('app', 'Id User'),
            'id_friend' => Yii::t('app', 'Id Friend'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    /** List statuses
     * @return array
     */
    public static function getStatuses(){
        return [
            self::TYPE_FRIEND_CLASS => \Yii::t('app','Friend by class'),
            self::TYPE_FRIEND_PROFESSOR => \Yii::t('app','You are a friend'),
            self::TYPE_FRIEND_UNIVERSITY => \Yii::t('app','Declined a request'),
            self::TYPE_FRIEND_OTHER => \Yii::t('app','Declined a request'),
        ];
    }
}