<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 03.03.16
 * Time: 10:37
 * Email: mackraiscms@gmail.com
 * Site: http://mackrais.tk
 */

namespace app\models\user;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "mr_user".
 *
 * @property integer $id
 * @property string $name
 * @property string $surname
 * @property string $password
 * @property string $auth_key
 * @property string $token
 * @property string $email
 * @property string $group
 * @property integer $status
 * @property string $date_create
 * @property string $date_update
 * @property string $password_reset_token
 *
 * @property UniversityManager[] $universityManagers
 * @property UniversityMember[] $universityMembers
 * @property UserInfo[] $userInfos
 */

class UserEdit extends User
{

    public $confirm_password;
    public $old_password;

    public $attr_old_password;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'auth_key', 'token', 'email', 'group', 'password_reset_token'], 'required'],
            [['status'], 'integer'],
            [['date_create', 'date_update'], 'safe'],
            [['name', 'surname', 'password', 'auth_key', 'token', 'email', 'group', 'password_reset_token', 'old_password', 'confirm_password'], 'string', 'max' => 255],
            [['email'], 'unique'],
            ['password', 'string', 'min' => 8],
            ['confirm_password', 'compare', 'compareAttribute'=>'password', 'message'=>Yii::t('app',"Passwords don`t match" )],
            //['old_password','validatePassword']
        ];
    }


    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_create',
                'updatedAtAttribute' => 'date_update',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'surname' => Yii::t('app', 'Surname'),
            'password' => Yii::t('app', 'Password'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'token' => Yii::t('app', 'Token'),
            'email' => Yii::t('app', 'Email'),
            'group' => Yii::t('app', 'Group'),
            'status' => Yii::t('app', 'Status'),
            'date_create' => Yii::t('app', 'Date Create'),
            'date_update' => Yii::t('app', 'Date Update'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'confirm_password' => Yii::t('app', 'Confirm password'),
            'old_password' => Yii::t('app', 'Old password'),
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     * @return bool
     */

//    public function validatePassword($attribute, $params)
//    {
//        $user = User::findIdentity($this->id);
//
//        if(!empty($this->old_password) || !empty($this->confirm_password) || !empty($this->password)){
//
//            if($user && $user->validatePassword($this->old_password) ){
//                return true;
//            }
//
//            if (!$user || !$user->validatePassword($this->old_password)) {
//                $this->addError($attribute, 'Невірний старий пароль');
//            }
//        }
//
//    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        // set password change
        if(!empty($this->confirm_password)){
            $this->setPassword($this->confirm_password);
        }else{
            $user = User::findIdentity($this->id);
            $this->password = $user->password;
        }

        if (parent::beforeSave($insert)) {
            return true;
        } else {
            return false;
        }
    }
}