<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 25.02.16
 * Time: 21:44
 * Email: mackraiscms@gmail.com
 * Site: http://mackrais.tk
 */
namespace app\models\user;

use app\modules\users\models\UserInfo as BaseUser;
use app\models\University;

/**
* User extendet by modules
*/
class UserInfo extends BaseUser
{
	
	/**
     * @return \yii\db\ActiveQuery
     */
    public function getUniversity()
    {
        return $this->hasOne(University::className(), ['id' => 'university_id']);
    }

}