<?php
/**
 * Created by PhpStorm.
 * User: minace
 * Date: 3/3/16
 * Time: 7:53 PM
 */

namespace app\models\user;


use Yii;

/**
 * This is the model class for table "{{%user_friend_type}}".
 *
 * @property integer $id
 * @property string $title
 * @property integer $status
 * @property string $date_create
 * @property string $date_update
 */
class UserFriendType extends \yii\db\ActiveRecord
{


    public $status = [
        1 => [
            'edit'=>false,
            'delete'=>false,
        ],
        2 => [
            'edit'=>true,
            'delete'=>false,
        ],
        3 => [
            'edit'=>false,
            'delete'=>true,
        ],
        4 => [
            'edit'=>true,
            'delete'=>true,
        ]

    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_friend_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['status'], 'integer'],
            [['date_create', 'date_update'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'status' => Yii::t('app', 'Status'),
            'date_create' => Yii::t('app', 'Date Create'),
            'date_update' => Yii::t('app', 'Date Update'),
        ];
    }
}