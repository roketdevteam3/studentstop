<?php
/**
 * Created by PhpStorm.
 * User: minace
 * Date: 3/3/16
 * Time: 7:58 PM
 */

namespace app\models\user;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%user_friend_request}}".
 *
 * @property integer $id
 * @property integer $who_send
 * @property integer $whom_send
 * @property integer $status
 * @property string $date_create
 * @property string $date_update
 */
class UserFriendRequest extends \yii\db\ActiveRecord
{
     const STATUS_WAIT = 1;
     const STATUS_APPROVED = 2;
     const STATUS_DECLINED= 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_friend_request}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['who_send', 'whom_send', 'status'], 'required'],
            [['who_send', 'whom_send', 'status'], 'integer'],
            [['date_create', 'date_update'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'who_send' => Yii::t('app', 'Who Send'),
            'whom_send' => Yii::t('app', 'Whom Send'),
            'status' => Yii::t('app', 'Status'),
            'date_create' => Yii::t('app', 'Date Create'),
            'date_update' => Yii::t('app', 'Date Update'),
        ];
    }

    /** List statuses
     * @return array
     */
    public static function getStatuses(){
        return [
            self::STATUS_WAIT => \Yii::t('app','You have sent a request to friends'),
            self::STATUS_APPROVED => \Yii::t('app','You are a friend'),
            self::STATUS_DECLINED => \Yii::t('app','Declined a request'),
        ];
    }

    /** Get status text
     * @param $id
     * @return null
     */
    public static function getStatus($id){
        return isset(self::getStatuses()[$id]) ? self::getStatuses()[$id]  :  null;
    }


    /**
     * @param int $who_send
     * @param int $whom_send
     * @return array|null|\yii\db\ActiveRecord
     */
    public function checkFriend($whom_send = 0, $who_send = 0){
        return self::find()->where(
            [
                'whom_send'=>$whom_send ? $whom_send : $this->id_class,
                'who_send'=>$who_send ? $who_send : \Yii::$app->user->getId(),
            ]
        )
            ->one();
    }



    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_create',
                'updatedAtAttribute' => 'date_update',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

}