<?php

namespace app\models\user;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "mr_user_bonus".
 *
 * @property integer $id
 * @property integer $user_id
 * @property double $bonus
 * @property string $type
 * @property integer $created_at
 * @property integer $updated_at
 */
class UserBonus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mr_user_bonus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at', 'updated_at'], 'integer'],
            [['bonus'], 'number'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'bonus' => 'Bonus',
            'type' => 'Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }


}
