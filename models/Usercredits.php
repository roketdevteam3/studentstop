<?php
namespace app\models;

use app\models\user\User;
use app\models\user\UserFriendRequest;
use app\modules\users\models\UserInfo;
use Yii;


class Usercredits extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_credits}}';
    }

//    public function beforeSave($insert)
//    {
//        if ($this->isNewRecord)
//        {
//            $this->date_create = date("Y-m-d H:i:s");
//        }
//
//        return parent::beforeSave($insert);
//    }
    
    public function scenarios()
    {
        return [
            'add_credits' => ['user_id', 'credit_count', 'many'],
            'update_credits_count' => ['credit_count'],
            'update_many_count' => ['many'],
        ];
    }
    
    public function rules()
    {
        return [
            [['user_id', 'credit_count'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
        ];
    }
    
}
