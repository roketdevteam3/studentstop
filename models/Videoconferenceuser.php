<?php
namespace app\models;

use app\models\user\User;
use app\models\user\UserFriendRequest;
use app\modules\users\models\UserInfo;
use Yii;
//status = [0 => 'not active', 1 => 'acctive', 2 => 'close', '3' => 'alumni']

class Videoconferenceuser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%videoconference_users}}';
    }

//    public function beforeSave($insert)
//    {
//        if ($this->isNewRecord)
//        {
//            $this->date_create = date("Y-m-d H:i:s");
//        }
//
//        return parent::beforeSave($insert);
//    } 
    
    public function scenarios()
    {
        return [
            'add_conference' => ['conference_id','user_id','status'],
            'change_status' => ['status'],
        ];
    }
    
    public function rules()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
        ];
    }
    
}
