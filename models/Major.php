<?php
namespace app\models;

use app\models\user\User;
use app\models\user\UserFriendRequest;
use app\modules\users\models\UserInfo;
use Yii;


class Major extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%major}}';
    }

    public function scenarios()
    {
        return [
            'add_major' => ['name', 'university_id', 'url_name','description'],
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    } 
    
    public function rules()
    {
        return [
            [['name','university_id','url_name'], 'required'],
            ['url_name', 'unique'],
            ['url_name', 'match','pattern'=>'/^[a-zA-Z0-9_]+$/'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Major name',
            'university_id' => 'University',
            'url_name' => 'Url name',
            'description' => 'Description',
        ];
    }
    
}
