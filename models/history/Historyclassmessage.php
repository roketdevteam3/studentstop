<?php
namespace app\models\history;

use app\models\user\User;
use app\models\user\UserFriendRequest;
use app\modules\users\models\UserInfo;
use Yii;


class Historyclassmessage extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%history_class_message}}';
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              $this->date_history_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    } 
    
    public function scenarios()
    {
        return [
            'add_message' => ['user_id', 'class_id', 'message','created_add'],
        ];
    }
    public function rules()
    {
        return [
        ];
    }

    public function attributeLabels()
    {
        return [
        ];
    }
}
