<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 29.02.16
 * Time: 17:37
 * Email: mackraiscms@gmail.com
 * Site: http://mackrais.tk
 */

namespace app\models\history;

use app\models\user\User;
use app\models\user\UserFriendRequest;
use app\modules\users\models\UserInfo;
use Yii;

/**
 * This is the model class for table "{{%follower}}".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_object
 * @property string $type_object
 */

class Historyfollower extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%history_follower}}';
    }

     public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_history_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    } 
    
    public function scenarios()
    {
        return [
            'add_follower' => ['id_user', 'id_object', 'type_object', 'date_create'],
        ];
    }
    
    public function rules()
    {
        return [
        ];
    }

    public function attributeLabels()
    {
        return [
        ];
    }
}
