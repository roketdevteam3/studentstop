<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MainMenuItem;

/**
 * MainMenuItemSearch represents the model behind the search form about `app\models\MainMenuItem`.
 */
class MainMenuItemSearch extends MainMenuItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_num', 'absolute', 'status'], 'integer'],
            [['link', 'title'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MainMenuItem::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'order_num' => $this->order_num,
            'absolute' => $this->absolute,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }

    public function getMenuList()
    {
        return new ActiveDataProvider([
            'query' => MainMenuItem::find()->orderBy(['order_num' => SORT_DESC]),
        ]);
    }


    public function getMenuListLogged()
    {
        return new ActiveDataProvider([
            'query' => MainMenuItem::find()
                ->andWhere(['logged' => MainMenuItem::LOGGED])
                ->orderBy(['order_num' => SORT_DESC]),
        ]);
    }

    public function getMenuListNonLogin()
    {
        return new ActiveDataProvider([
            'query' => MainMenuItem::find()
                ->andWhere(['logged' => MainMenuItem::NOT_LOGGED])
                ->orderBy(['order_num' => SORT_DESC]),
        ]);
    }
}