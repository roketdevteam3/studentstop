<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 01.03.16
 * Email: mackraiscms@gmail.com
 * Site: http://mackrais.tk
 */
namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%university_course_with_major}}".
 *
 * @property integer $id
 * @property integer $id_major
 * @property integer $id_course
 * @property integer $id_university
 * @property integer $creator
 */
class UniversityCourseWithMajor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%university_course_with_major}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_major', 'id_course', 'id_university', 'creator'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_major' => Yii::t('app', 'Id Major'),
            'id_university' => Yii::t('app', 'Id University'),
            'id_course' => Yii::t('app', 'Id Course'),
            'creator' => Yii::t('app', 'Creator'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            $this->creator = \Yii::$app->user->getId();
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $id_university
     * @param $id_major
     * @param $id_course
     * @return array|null|\yii\db\ActiveRecord
     */
    public function checkRelate($id_university,$id_major,$id_course){
        return self::find()->where(
            [
                'id_university'=>$id_university,
                'id_major'=>$id_major,
                'id_course'=>$id_course,
            ]
        )->one();

    }
}

