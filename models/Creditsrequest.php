<?php
namespace app\models;

use Yii;


class Creditsrequest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%credits_request}}';
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    public function scenarios()
    {
        return [
            'create_request' => ['user_id', 'modules', 'recepient_id', 'recepient_type', 'credits_count', 'request_type'],
            'add_credits' => ['user_id', 'modules', 'recepient_type', 'credits_count', 'request_type'],
        ];
    }
    
    public function rules()
    {
        return [
//            [['user_id', 'credit_count'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
        ];
    }
    
}
