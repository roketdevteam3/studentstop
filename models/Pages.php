<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use app\components\ActiveRecord;

/**
 * This is the model class for table "{{%pages}}".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $title
 * @property string $slug
 * @property string $content
 * @property string $meta_t
 * @property string $meta_k
 * @property string $meta_d
 * @property string $seo_text
 * @property integer $order_num
 * @property string $date_create
 * @property string $date_update
 * @property integer $status
 */
class Pages extends ActiveRecord
{

    var $create_menu = true;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pages}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'order_num', 'status', 'create_menu'], 'integer'],
            [['title'], 'required'],
            [['content', 'seo_text'], 'string'],
            [['date_create', 'date_update'], 'safe'],
            [['title', 'slug', 'meta_t', 'meta_k', 'meta_d'], 'string', 'max' => 255],
            [['slug'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'title' => Yii::t('app', 'Title'),
            'slug' => Yii::t('app', 'Slug'),
            'content' => Yii::t('app', 'Content'),
            'meta_t' => Yii::t('app', 'Meta T'),
            'meta_k' => Yii::t('app', 'Meta K'),
            'meta_d' => Yii::t('app', 'Meta D'),
            'seo_text' => Yii::t('app', 'Seo Text'),
            'order_num' => Yii::t('app', 'Order Num'),
            'date_create' => Yii::t('app', 'Date Create'),
            'date_update' => Yii::t('app', 'Date Update'),
            'status' => Yii::t('app', 'Status'),
            'create_menu' =>  Yii::t('app', 'Create Menu'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_create',
                'updatedAtAttribute' => 'date_update',
                'value' => new Expression('NOW()'),
            ],
            'slug' => [
                'class'         => 'app\behaviors\Slug',
                'in_attribute'  => 'title',
                'out_attribute' => 'slug',
                'translit'      => true
            ]
        ];
    }

    /**
     * @param $slug
     * @return $this
     */
    public static function findBySlug($slug){
        return self::find()->where(['slug'=>$slug]);
    }
}