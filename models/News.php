<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;


/**
 * This is the model class for table "{{%news}}".
 *
 * @property integer $id
 * @property string $place_type
 * @property integer $place_id
 * @property integer $owner_id
 * @property string $title
 * @property string $image
 * @property string $slug
 * @property string $date_publication
 * @property string $short_desc
 * @property string $content
 * @property string $date_create
 * @property string $date_update
 * @property integer $status
 */
class News extends ActiveRecord
{

    const STATUS_OFF = 0;
    const STATUS_ON = 1;
    const DEFAULT_IMG = 'uploads/news/default.jpg';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['place_type', 'title', 'slug'], 'required'],
            [['place_id', 'owner_id', 'status'], 'integer'],
            [['date_publication', 'date_create', 'date_update'], 'safe'],
            [['short_desc', 'content'], 'string'],
            [['place_type', 'title', 'slug'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            //[['image'], 'image']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'place_type' => Yii::t('app', 'Place Type'),
            'place_id' => Yii::t('app', 'Place ID'),
            'owner_id' => Yii::t('app', 'Owner ID'),
            'title' => Yii::t('app', 'Title'),
            'image' => Yii::t('app', 'Image'),
            'slug' => Yii::t('app', 'Slug'),
            'date_publication' => Yii::t('app', 'Date Publication'),
            'short_desc' => Yii::t('app', 'Short Desc'),
            'content' => Yii::t('app', 'Content'),
            'date_create' => Yii::t('app', 'Date Create'),
            'date_update' => Yii::t('app', 'Date Update'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_create',
                'updatedAtAttribute' => 'date_update',
                'value' => new Expression('NOW()'),
            ],
            'slug' => [
                'class'         => 'app\behaviors\Slug',
                'in_attribute'  => 'title',
                'out_attribute' => 'slug',
                'translit'      => true
            ]
        ];
    }

    public function getImg($id = 0){
        if((int)$id){
            $model = $this->findOne($id);
        }else{
            $model = $this->findOne($this->id);
        }

        if(!empty($model)){
            $path = \Yii::getAlias('@webroot'.$model->image);

            if(!file_exists($path) || !is_file($path)){
                return Url::to(self::DEFAULT_IMG,true);
            }
            return Url::to($model->image,true);

        }else{
            return Url::to(self::DEFAULT_IMG,true);
        }
    }
}
