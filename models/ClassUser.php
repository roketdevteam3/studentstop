<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 03.03.16
 * Time: 15:25
 * Email: mackraiscms@gmail.com
 * Site: http://mackrais.tk
 */

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%class_user}}".
 *
 * @property integer $id
 * @property integer $id_class
 * @property integer $id_user
 * @property string $date_join
 */
class ClassUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%class_user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_class', 'id_user'], 'required'],
            [['id_class', 'id_user'], 'integer'],
            [['date_join'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_class' => Yii::t('app', 'Id Class'),
            'id_user' => Yii::t('app', 'Id User'),
            'date_join' => Yii::t('app', 'Date Join'),
        ];
    }



    public function checkJoin($id_class = 0,$id_user = 0){
        return self::find()->where(
            [
                'id_class'=>$id_class ? $id_class : $this->id_class,
                'id_user'=>$id_user ? $id_user : \Yii::$app->user->getId(),
            ]
        )->one();

    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            $this->date_join = date('Y-m-d H:i:s');
            return true;
        } else {
            return false;
        }
    }

}