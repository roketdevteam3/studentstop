<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 01.03.16
 * Time: 15:33
 * Email: mackraiscms@gmail.com
 * Site: http://mackrais.tk
 */

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%university_major}}".
 *
 * @property integer $id
 * @property integer $id_major
 * @property integer $id_university
 *
 * @property University $idUniversity
 */
class UniversityMajor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%university_major}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_major', 'id_university', 'creator'], 'integer'],
            [['id_university'], 'exist', 'skipOnError' => true, 'targetClass' => University::className(), 'targetAttribute' => ['id_university' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_major' => Yii::t('app', 'Id Major'),
            'id_university' => Yii::t('app', 'Id University'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUniversity()
    {
        return $this->hasOne(University::className(), ['id' => 'id_university']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            $this->creator = \Yii::$app->user->getId();
            return true;
        } else {
            return false;
        }
    }
}