<?php
namespace app\models;

use app\models\user\User;
use app\models\user\UserFriendRequest;
use app\modules\users\models\UserInfo;
use Yii;


class Country extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%countries}}';
    }

    public function rules()
    {
        return [
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Countries name',
        ];
    }
    
}
