<?php
namespace app\models;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use app\models\user\User;
use app\models\user\UserFriendRequest;
use app\modules\users\models\UserInfo;
use Yii;
//status = 2 -close, 1-active

class Videoconference extends \yii\db\ActiveRecord
{
    public $users_id;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%videoconference}}';
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    } 
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_create',
                'updatedAtAttribute' => 'date_update',
                'value' => new Expression('NOW()'),
            ],
            'slug' => [
                'class'         => 'app\behaviors\Slug',
                'in_attribute'  => 'name',
                'out_attribute' => 'url_name',
                'translit'      => true
            ]
        ];
    }
    
    public function scenarios()
    {
        return [
            'add_conference' => ['name', 'tutors_id', 'date_start', 'users_id', 
                'id_university', 'id_major', 'description', 'img_src', 'url_name',
                'price_type' ,'price_m', 'price_c',  'status', 'id_course'],
            'change_status' => ['status']
        ];
    }
    
    public function rules()
    {
        return [
            [['name','date_start', 'price'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'users_id' => 'Select users',
        ];
    }
    
}
