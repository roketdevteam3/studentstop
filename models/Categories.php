<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;


class Categories extends ActiveRecord
{
    public $child_category;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%post_categories}}';
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    public function scenarios()
    {
        return [
            'add_category' => ['category_name', 'parent_id', 'category_type', 'url_name'],
	];
    }
    
    public function rules()
    {
        return [
            ['category_name', 'required'],
            ['url_name', 'required'],
            ['url_name', 'unique'],
            ['url_name', 'match','pattern'=>'/^[a-zA-Z0-9_]+$/'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parent_id' => 'Parent Categories',
        ];
    }

    static function getCategoryName($category_id){
        $modelCategory = static::find()->where(['id' => $category_id])->one();
        
        return $modelCategory['category_name'];
    }
}
