<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

class Notification extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%notification}}';
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    } 
    
    public function scenarios()
    {
        return [
            'add_notification' => ['module', 'from_object_id', 'to_object_id', 'university_id', 'notification_type'],
        ];
    }

    public function rules()
    {
        return [
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
        ];
    }

}
