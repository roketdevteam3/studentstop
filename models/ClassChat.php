<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%class_chat}}".
 *
 * @property integer $id
 * @property integer $id_class
 * @property integer $id_user
 * @property string $message
 * @property string $type
 * @property string $data_create
 */
class ClassChat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%class_chat}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_class', 'id_user', 'type'], 'integer'],
            [['message'], 'string'],
            [['data_create'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_class' => Yii::t('app', 'Id Class'),
            'id_user' => Yii::t('app', 'Id User'),
            'message' => Yii::t('app', 'Message'),
            'type' => Yii::t('app', 'Type'),
            'data_create' => Yii::t('app', 'Data Create'),
        ];
    }
}
