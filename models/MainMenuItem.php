<?php

namespace app\models;

use app\behaviors\SortableModel;
use Yii;
use app\components\ActiveRecord;

/**
 * This is the model class for table "{{%main_menu_item}}".
 *
 * @property integer $id
 * @property string $link
 * @property string $title
 * @property integer $order_num
 * @property integer $absolute
 * @property integer $status
 */
class MainMenuItem extends ActiveRecord
{

    const STATUS_OFF = 0;
    const STATUS_ON = 1;

    const LOGGED = 1;
    const NOT_LOGGED = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%main_menu_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_num', 'absolute', 'status', 'logged'], 'integer'],
            [['link', 'title','onclick'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'link' => Yii::t('app', 'Link'),
            'title' => Yii::t('app', 'Title'),
            'order_num' => Yii::t('app', 'Order Num'),
            'absolute' => Yii::t('app', 'Absolute'),
            'status' => Yii::t('app', 'Status'),
            'logged' => Yii::t('app','Logged'),
            'onclick'=> Yii::t('app','onclick'),
        ];
    }

    public function behaviors()
    {
        return [
            SortableModel::className()
        ];
    }
}
