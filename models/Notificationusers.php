<?php
namespace app\models;

use app\models\user\User;
use app\models\user\UserFriendRequest;
use app\modules\users\models\UserInfo;
use Yii;

class Notificationusers extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%notification_users}}';
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
            $this->status = 0;
        }

        return parent::beforeSave($insert);
    } 
    
    public function scenarios()
    {
        return [
            'new_notification' => ['user_id', 'notification_id'],
            'change_status' => ['status'],
        ];
    }
    
    public function rules()
    {
        return [
        ];
    }

    public function attributeLabels()
    {
        return [
        ];
    }
    
}
