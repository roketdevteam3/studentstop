<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mr_paypal_setting".
 *
 * @property integer $id
 * @property string $client_id
 * @property string $secret
 * @property string $email
 * @property integer $user_id
 */
class PaypalSetting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mr_paypal_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['email'], 'required'],
            [['client_id', 'secret'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'secret' => 'Secret',
            'email' => 'Paypal email',
            'user_id' => 'User ID',
        ];
    }
}
