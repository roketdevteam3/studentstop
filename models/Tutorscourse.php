<?php
namespace app\models;

use app\models\user\User;
use app\models\user\UserFriendRequest;
use app\modules\users\models\UserInfo;
use Yii;


class Tutorscourse extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tutors_course}}';
    }

//    public function beforeSave($insert)
//    {
//        if ($this->isNewRecord)
//        {
//            $this->date_create = date("Y-m-d H:i:s");
//        }
//
//        return parent::beforeSave($insert);
//    } 
    
    public function scenarios()
    {
        return [
            'add_tutors' => ['tutors_id', 'course_id'],
        ];
    }
    
    public function rules()
    {
        return [
            [['tutors_id', 'course_id'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
        ];
    }
    
}
