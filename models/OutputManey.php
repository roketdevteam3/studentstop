<?php

namespace app\models;

use app\models\user\User;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "mr_output_maney".
 *
 * @property integer $id
 * @property double $maney
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 * @property string $email_paypal
 */
class OutputManey extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mr_output_maney';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['maney','email_paypal'], 'required'],
            [['maney'], 'number'],
            [['email_paypal'], 'email'],
            [['user_id', 'created_at', 'updated_at', 'status'], 'integer'],
            [['email_paypal'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'maney' => 'Maney ($)',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Paid',
            'email_paypal' => 'Email Paypal',
        ];
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
