<?php
/**
 * Created by PhpStorm.
 * User: minace
 * Date: 3/1/16
 * Time: 9:12 PM
 */
namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%class}}".
 *
 * @property integer $id
 * @property integer $id_course
 * @property string $name
 * @property string $description
 * @property string $date_create
 * @property string $date_update
 * @property integer $status
 * @property integer $creator
 * @property integer $professor
 * @property integer $id_major
 * @property integer $id_university
 *
 * @property Course $idCourse
 */
class UniversityClass extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%class}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_course', 'status', 'creator', 'professor', 'id_major', 'id_university'], 'integer'],
            [['name'], 'required'],
            [['description'], 'string'],
            [['date_create', 'date_update'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['id_course'], 'exist', 'skipOnError' => true, 'targetClass' => Course::className(), 'targetAttribute' => ['id_course' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_course' => Yii::t('app', 'Id Course'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'date_create' => Yii::t('app', 'Date Create'),
            'date_update' => Yii::t('app', 'Date Update'),
            'status' => Yii::t('app', 'Status'),
            'creator' => Yii::t('app', 'Creator'),
            'professor' => Yii::t('app', 'Professor'),
            'id_major' => Yii::t('app', 'Id Major'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'id_course']);
    }
}