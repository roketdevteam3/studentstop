<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "mr_transaction".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $complete
 * @property string $payment_id
 * @property string $hash
 * @property string $othe
 * @property double $price
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $project_id
 */
class Transaction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mr_transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'complete', 'created_at', 'updated_at', 'project_id'], 'integer'],
            [['othe'], 'string'],
            [['price'], 'number'],
            [['payment_id', 'hash'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'complete' => 'Complete',
            'payment_id' => 'Payment ID',
            'hash' => 'Hash',
            'othe' => 'Othe',
            'price' => 'Price',
            'created_at' => 'Created At',
            'updated_at' => 'Created',
            'project_id' => 'Project ID',
        ];
    }

  public function behaviors()
  {
    return [
      TimestampBehavior::className(),
    ];
  }
}
