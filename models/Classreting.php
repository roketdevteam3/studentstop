<?php
namespace app\models;

use app\models\user\User;
use app\models\user\UserFriendRequest;
use app\modules\users\models\UserInfo;
use Yii;


class Classreting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%class_rating}}';
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    } 
    
    public function scenarios()
    {
        return [
            'add_rating' => ['class_id', 'user_id', 'rate_professor', 'level_of_difficulty', 'prof_again', 'take_for_credit', 'attendance', 'hotness', 'more_specific', 'textbook_use'],
        ];
    }
    
    public function rules()
    {
        return [
        ];
    }

    public function attributeLabels()
    {
        return [
        ];
    }
    
}
