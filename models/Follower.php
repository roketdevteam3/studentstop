<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 29.02.16
 * Time: 17:37
 * Email: mackraiscms@gmail.com
 * Site: http://mackrais.tk
 */

namespace app\models;

use app\models\user\User;
use app\models\user\UserFriendRequest;
use app\modules\users\models\UserInfo;
use Yii;

/**
 * This is the model class for table "{{%follower}}".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_object
 * @property string $type_object
 * status: 1) end class; 2)end by not confirm;  0) add class first time
 */

class Follower extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%follower}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_object', 'status'], 'integer'],
            [['type_object'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user' => Yii::t('app', 'Id User'),
            'id_object' => Yii::t('app', 'Id Object'),
            'type_object' => Yii::t('app', 'Type Object'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserInfo()
    {
        return $this->hasOne(UserInfo::className(), ['id_user' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    public function checkFriendRequest($whom_send = 0, $who_send = 0){
        return UserFriendRequest::find()->where(
            [
                'whom_send'=>$whom_send ? $whom_send : $this->id_user,
                'who_send'=>$who_send ? $who_send : \Yii::$app->user->getId(),
            ]
        )
            ->one();
    }
}
