<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 01.03.16
 * Time: 15:35
 * Email: mackraiscms@gmail.com
 * Site: http://mackrais.tk
 */


namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%university_course}}".
 *
 * @property integer $id
 * @property integer $id_major
 * @property integer $id_course
 * @property integer $id_university
 */
class UniversityCourse extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%university_course}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'id_course', 'id_university', 'creator'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_course' => Yii::t('app', 'Id Course'),
            'id_university' => Yii::t('app', 'Id University'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            $this->creator = \Yii::$app->user->getId();
            return true;
        } else {
            return false;
        }
    }
}