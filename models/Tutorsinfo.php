<?php
namespace app\models;

use app\models\user\User;
use app\models\user\UserFriendRequest;
use app\modules\users\models\UserInfo;
use Yii;


class Tutorsinfo extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%user_tutors_info}}';
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    } 
    
    public function scenarios()
    {
        return [
            'add_tutors' => ['user_id', 'about','price'],
        ];
    }
    
    public function rules()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
//            'users_id' => 'Select users',
        ];
    }
    
}
