<?php
namespace app\models;

use app\models\user\User;
use app\models\Classes;
use app\models\user\UserFriendRequest;
use app\modules\users\models\UserInfo;
use Yii;


class Course extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%course}}';
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              $this->date_create = date("Y-m-d H:i:s");
              $this->creator = \Yii::$app->user->id;
                if(\Yii::$app->user->identity->group == 'admin'){
                    $this->status = 1;
                }else{
                    $this->status = 0;
                }
        }

        return parent::beforeSave($insert);
    } 
    
    public function scenarios()
    {
        return [
            'add_course' => ['name', 'id_major', 'url_name','description'],
            'update_course' => ['name', 'id_major', 'url_name','description'],
        ];
    }
    
    public function rules()
    {
        return [
            [['name','id_major','url_name'], 'required'],
            ['url_name', 'unique'],
            ['url_name', 'match','pattern'=>'/^[a-zA-Z0-9_]+$/'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Course name',
            'id_major' => 'Major',
            'url_name' => 'Url name',
            'description' => 'Description',
        ];
    }
    
    public function getClasses()
    {
        return $this->hasMany(Classes::className(), ['id_course' => 'id']);
    }
}
