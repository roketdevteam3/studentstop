<?php
/**
 * @link http://www.astwellsoft.com/
 * @copyright Copyright (c) 2015 Astwell Soft
 * @license http://www.astwellsoft.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

class MrScriptAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/preview_img.css',
        'css/administration/sb-admin.css'
    ];
    public $js = [
        'js/administration/bootbox.js',
        'js/preview_img.js',
        'js/dropzone.js',
        'js/administration/admin.js',
        'https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.0/jquery.nicescroll.min.js'
    ];

    public $jsOptions = array(
       // 'position' => \yii\web\View::PH_
    );
}
