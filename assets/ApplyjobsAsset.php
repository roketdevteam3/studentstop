<?php
/**
 * @link http://www.astwellsoft.com/
 * @copyright Copyright (c) 2015 Astwell Soft
 * @license http://www.astwellsoft.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

class ApplyjobsAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
      'css/ls_style.css',
      'css/search_block.css',
      'css/fix_style_CSS.css',
    ];
    public $js = [
        'js/applyjobs.js',
        'js/search_block.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'kartik\grid\GridViewAsset',
    ];

    public $jsOptions = array(
        'position' => \yii\web\View::POS_HEAD
    );
}
