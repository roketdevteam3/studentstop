<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/css?family=Roboto:400,100,500,700',
        'css/site.css',
        'css/admin1.css',
        'css/elements.css',
        'css/sweetalert.css',
        'css/animate.css',
        'css/custom.css',
        'css/font-awesome.min.css',
        'css/preview_img.css',
        'css/mr.notifications.css',
        'css/chat.css',
        'css/style_sass.css',
        'css/owl.carousel.css'
    ];
    public $js = [
        //'https://maps.googleapis.com/maps/api/js?key=AIzaSyCc3FyFkad1Wy9dSBqglWLp9HecGuEBfog&callback=initMap',
        'js/socket.io-1.3.5.js',
        'js/wow.min.js',
        'js/main.js',
        'js/s.js',
        'js/sweetalert.min.js',
        'js/bootbox.js',
        'js/mr.functions.helper.js',
        'js/preview_img.js',
        'js/mr.notifications.js',
        'js/mr.user.functions.js',
        'js/notification.js',
        'js/velocity.min.js',
        // 'js/global-vendors.js',
        'js/pleasure.js',
        'js/layout.js',
        'js/chat.js',
        'js/adapter.js',
        'js/video.js',
        'js/country_change.js',
        'js/custom.js',
        'js/owl.carousel.js',
        'https://simplewebrtc.com/latest-v2.js',
        'js/notification_header.js',
    ];
    public $jsOptions = array(
        //'position' => \yii\web\View::PH_BODY_END
        'defer' => 'defer'
    );
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',

    ];
}
