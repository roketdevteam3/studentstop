<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LandingAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/css?family=Roboto:400,100,500,700',
        'css/site.css',
        'css/animate.css',
        'css/custom.css',
        'css/font-awesome.min.css',
        'css/preview_img.css',
        'css/mr.notifications.css',
        'css/landing-page.css',
        'css/elements.css',
        'css/style.css',
        'globals/plugins/modernizr/modernizr.min.js',
        'globals/plugins/bootstrap-social/bootstrap-social.css',
        'css/plugins.css'
    ];
    public $js = [
       // 'js/socket.io-1.3.5.js',

        'globals/plugins/typer/jquery.typer.min.js',
       // 'js/one-page-parallax.js',
        'js/global-vendors.js',
        'js/wow.min.js',
        'js/main.js',
        'js/s.js',
        'js/bootbox.js',
        'js/mr.functions.helper.js',
        'js/preview_img.js',
        'js/mr.notifications.js',
        'js/mr.user.functions.js',
        'globals/plugins/scrollMonitor/scrollMonitor.js',
        'globals/plugins/skrollr/skrollr.min.js',

        'js/pleasure.js',

    ];

    public $jsOptions = array(
        'position' => \yii\web\View::POS_HEAD
    );

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',

    ];
}
