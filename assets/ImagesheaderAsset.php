<?php
/**
 * @link http://www.astwellsoft.com/
 * @copyright Copyright (c) 2015 Astwell Soft
 * @license http://www.astwellsoft.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

class ImagesheaderAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/user_header_photo.css',
        'http://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css',
    ];
    public $js = [
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyDcBLaan_dFaE2Nxr9HkJwZJOSHYUeKo4c',
        /*'https://code.jquery.com/ui/1.12.0/jquery-ui.js',
        'http://plugin.bearsthemes.com/jquery/MasonryHybrid/isotope.pkgd.min.js',
        'http://plugin.bearsthemes.com/jquery/MasonryHybrid/jquery.masonry-hybrid.min.js',*/
        //'http://www.google.com/jsapi',
        'js/plugin2_sviat.js',
        'js/pluding1_sviat.js',
        'js/jquery_sviat.js',
        'js/dropzone.js',
        'js/users_header_photo.js',
        'js/user_about.js',
        'js/star-rating.js',
        'js/friends.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'kartik\grid\GridViewAsset',
    ];

    public $jsOptions = array(
        'position' => \yii\web\View::POS_HEAD
    );
}
