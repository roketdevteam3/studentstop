<?php
/**
 * @link http://www.astwellsoft.com/
 * @copyright Copyright (c) 2015 Astwell Soft
 * @license http://www.astwellsoft.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

class Error403Asset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/errors/error403.css',
        'css/site.css',
    ];
    public $js = [
    ];
    public $depends = [
        'app\assets\FontAwesomeAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];

    public $jsOptions = array(
        'position' => \yii\web\View::POS_HEAD
    );
}
