<?php
/**
 * @link http://www.astwellsoft.com/
 * @copyright Copyright (c) 2015 Astwell Soft
 * @license http://www.astwellsoft.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

class DatepaginatorAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap-datepicker.css',
        'css/bootstrap-datepaginator.css',
    ];
    public $js = [
        'js/bootstrap-datepicker.js',
        'js/moment.js',
        'js/bootstrap-datepaginator.js',
        'js/dropzone.js',
        'js/university_calendar.js',
        'js/post_university_page.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
