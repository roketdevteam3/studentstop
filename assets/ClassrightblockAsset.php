<?php
/**
 * @link http://www.astwellsoft.com/
 * @copyright Copyright (c) 2015 Astwell Soft
 * @license http://www.astwellsoft.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

class ClassrightblockAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/class_chat.css',
        'css/dropzone.css',
        'css/fresco/fresco.css',
    ];
    public $js = [
        'js/star-rating.js',
        'js/bootstrap-datepicker.js',
        'js/moment.js',
        'js/bootstrap-datepaginator.js',
        'js/dropzone.js',
        'js/fresco/fresco.js',
        'js/classrightblock.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
