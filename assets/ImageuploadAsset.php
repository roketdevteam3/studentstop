<?php
/**
 * @link http://www.astwellsoft.com/
 * @copyright Copyright (c) 2015 Astwell Soft
 * @license http://www.astwellsoft.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

class ImageuploadAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/dropzone.css',
        'css/prettyPhoto.css',
    ];
    public $js = [
        'js/dropzone.js',
        'js/users_photos.js',
        'js/jquery.prettyPhoto.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'kartik\grid\GridViewAsset',
    ];

    public $jsOptions = array(
        'position' => \yii\web\View::POS_HEAD
    );
}
