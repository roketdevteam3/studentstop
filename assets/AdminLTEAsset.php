<?php
/**
 * @link http://www.astwellsoft.com/
 * @copyright Copyright (c) 2015 Astwell Soft
 * @license http://www.astwellsoft.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

class AdminLTEAsset extends AssetBundle
{
    public $sourcePath = '@bower/';
    public $css = [
        'admin-lte/dist/css/AdminLTE.min.css',
        'admin-lte/dist/css/skins/_all-skins.min.css',
        'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css',

    ];
    public $js = [
        'admin-lte/dist/js/app.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'app\assets\FontAwesomeAsset',
        'app\assets\MrScriptAsset'
    ];

    public $jsOptions = array(
        //'position' => \yii\web\View::PH_BODY_END
    );


    public static function overrideSystemConfirm()
    {   $language = \Yii::$app->language;
        $language = explode('-',$language);

        \Yii::$app->view->registerJs('
            yii.confirm = function(message, ok, cancel) {

                bootbox.setLocale("'.current($language).'");
                bootbox.confirm(message, function(result) {
                    if (result) { !ok || ok(); } else { !cancel || cancel(); }
                });
            }
        ');
    }
}
