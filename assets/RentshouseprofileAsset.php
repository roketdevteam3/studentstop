<?php
/**
 * @link http://www.astwellsoft.com/
 * @copyright Copyright (c) 2015 Astwell Soft
 * @license http://www.astwellsoft.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

class RentshouseprofileAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
         //'css/user_photos.css',
        'css/dropzone.css',
        'css/ls_style.css',
    ];
    public $js = [
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyDcBLaan_dFaE2Nxr9HkJwZJOSHYUeKo4c',
        'js/dropzone.js',
        'js/rentshouseprofile.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'kartik\grid\GridViewAsset',
    ];

    public $jsOptions = array(
        'position' => \yii\web\View::POS_HEAD
    );
}
