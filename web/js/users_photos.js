$(document).ready(function(){
    
    
        function getProfilePhoto(){
            var user_id = $('.userPhotosId').text();
                $.ajax({
                    type: 'POST',
                    url: '../../../university/default/getmyfiles',
                    data: {type:'0',user_id:user_id},
                    dataType:'json',
                    success: function(response){
                        $('.fileContainer').html('');
                        $('.fileContainer').append('<div class="user-image-list"></div>')
                        for(var key in response){
                            var img = response[key];
                            $('.user-image-list').append('<div class="profileImageClick img-thumbnail" data-photo_id="'+img.id+'" style="cursor:pointer;width:160px;height:160px;background-image: url(../../../images/users_images/'+img.image_name+');background-size:cover;background-repeat:no-repeat;" /></div>');
                        }
                    }
                });
        }
        $(document).on('click','.my_photo_profile',function(){
            getProfilePhoto();
            $('.my_video_profile').removeClass('active');
            $(this).addClass('active');
        })        
        getProfilePhoto();
        
        $(document).on('click','.my_video_profile',function(){
            getProfileVideo();
            $('.my_photo_profile').removeClass('active');
            $(this).addClass('active');
        })        
    
        function getProfileVideo(){
            var user_id = $('.userPhotosId').text();
                $.ajax({
                    type: 'POST',
                    url: '../../../university/default/getmyfiles',
                    data: {type:'1',user_id:user_id},
                    dataType:'json',
                    success: function(response){
                        $('.fileContainer').html('');
                        $('.fileContainer').append('<div class="user-video-list"></div>')
                        for(var key in response){
                            var video = response[key];
                            $('.user-video-list').append('<video width="200" style="margin:5px;" class="myVideo" data-action="not_add" data-src="'+video.image_name+'" controls>'+
                                '<source src="../../../images/users_images/'+video.image_name+'" type="video/mp4">'+
                                'Your browser does not support HTML5 video.'+
                            '</video>')
                        }
                    }
                });
        }
    
        $(document).on('click','.myVideo',function(){
            $('.myVideo').each(function(){
                if($(this).get(0).pause()){
                    $(this).get(0).pause()
                }
            });
            var video_src = $(this).data('src')
            $('#videoProfile').modal('show')
            $('.videoContainer').html('')
            $('.videoContainer').append('<video  style="width:100%;margin:5px;" class="video" data-action="not_add" data-src="'+video_src+'" controls>'+
                                '<source src="../../../images/users_images/'+video_src+'" type="video/mp4">'+
                                'Your browser does not support HTML5 video.'+
                            '</video>')
            
        })
        $('#videoProfile').on('hidden.bs.modal', function () {
            $(this).find('.video').get(0).pause();
            $(this).find('.videoContainer').html('');
        })
    
        var photosArray = {};
        $("a[rel^='prettyPhoto']").prettyPhoto({
            custom_markup: '<div id="map_canvas" style="width:260px; height:265px">vsvcsvzcvcvcfvfsv</div>',
        });
        
        function deleteFile(file_name){
            $.ajax({
                type: 'POST',
                url: '../../../deletefile',
                data: {file_name:file_name},
                success: function(response){
                }
            });
        }
        if($('.user_photos').length){
            
            var myDropzone = new Dropzone($('.user_photos')[0], { // Make the whole body a dropzone
                //maxFilesize: 50,
                dictDefaultMessage: 'dvdvadvadvdavv adv davdavdav',
                acceptedFiles:'image/*,video/*',
            });
            
            myDropzone.on("sending", function(file) {
                console.log(file);
              //  file.name = file.name+Math.random().toString(36).substring(7); 
              // Hookup the start button
              //file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file); };
            });
            // Update the total progress bar
            
            myDropzone.on("complete", function(response) {
                console.log(response);
                if (response.status == 'success') {
                    
                     var fileType = response.type;
                    
                    console.log('---------');
                    console.log('---------');
                    console.log(response.xhr.response);
                    console.log('---------');
                    console.log('---------');
                     
                    if(typeof photosArray[response.xhr.response] == 'undefined'){
                        photosArray[response.xhr.response] = {};
                    }
                    
                    console.log(photosArray);
                    photosArray[response.xhr.response] = fileType;
                    
                    //console.log(response);
                    $(response.previewElement).prepend('<i class="fa fa-times removePhoto fa-2x" title="delete" data-name="'+response.name+'"></i>');
                    //photosArray.push(response.xhr.response);
                    $('#formSavePhotos > div').html('');
                    $('#formSavePhotos > div').append("<input type='hidden' name='photosInput' value='"+JSON.stringify(photosArray)+"'> ");
                            //console.log(response.xhr.response)
                    $('#formSavePhotos').show();
                }
            });
            myDropzone.on("removedfile", function(response) {
              //alert('dvdv');
                //console.log();
                var removeItem = response.xhr.response;

//                photosArray = jQuery.grep(photosArray, function(value) {
//                  return value != removeItem;
//                });
                delete(photosArray[removeItem])
                
                $('#formSavePhotos > div').html('');
                $('#formSavePhotos > div').append("<input type='hidden' name='photosInput' value='"+JSON.stringify(photosArray)+"'> ");
                deleteFile(response.xhr.response);
                if(myDropzone.files.length == 0){
                    $('#formSavePhotos').hide();
                }
            });
            
            $(document).on('click','.removePhoto',function(){
                var shos = $(this).parent();
                
                var name_photo = $(this).data('name');
                $.each( myDropzone.files, function( key, value ) {
                    if(value.name == name_photo){
                        myDropzone.removeFile(myDropzone.files[key]);
                    }
                });
            });
        }
            
        $(document).on('click','.profileImageClick',function(){
            var photo_id = $(this).data('photo_id');
            console.log(photo_id);
            $.ajax({
                type: 'POST',
                url: '../../../showphotoinformation',
                data: {photo_id:photo_id},
                dataType:'json',
                success: function(response){
                    console.log(response);
                    $('.like_count').text(response.like_count);
                    $('.saveComment').data('photo_id',photo_id)
                    $('.photoLike').data('photo_id',photo_id)
                    $('.modalImagesShow').attr('src', '../../../images/users_images/'+response.modelUserImages['image_name']);
                    $('.commentList').html('');
                    var commentArray = response.photosComment;
                    for(var key in commentArray){
                        $('.commentList').append('<div class="col-sm-12" style="text-align:left;margin-bottom:20px;">'+
                            '<h4>'+commentArray[key].name+' '+commentArray[key].surname+'<span style="font-size:12px;" class="pull-right">'+commentArray[key].dateCreateComment+'</span></h4>'+
                            '<p style="border-bottom:1px solid white;">'+commentArray[key].comment_content+'</p>'+
                        '</div></ hr>');
                    }
                }
            });
            //alert(photo_id);
            $('#imagesProfile').modal('show');
        });
        
        $(document).on('click','.saveComment',function(){
            var commentContent = $('.newComment').val();
            var photo_id = $(this).data('photo_id');
            if(commentContent.length > '3'){
                 $.ajax({
                    type: 'POST',
                    url: '../../../addcomentphoto',
                    data: {comment_content:commentContent,photo_id:photo_id},
                    dataType:'json',
                    success: function(response){
                        if(response.status == 'success'){
                            $('.newComment').val(' ');
                           $('.commentList').prepend('<div class="col-sm-12" style="text-align:left;margin-bottom:20px;">'+
                            '<h4>'+response.photoComment['name']+' '+response.photoComment['surname']+'<span style="font-size:12px;" class="pull-right">'+response.photoComment['dateCreateComment']+'</span></h4>'+
                            '<p style="border-bottom:1px solid white;">'+response.photoComment['comment_content']+'</p>'+
                        '</div>');
                        }
                    }
                });
            }else{
                swal("", "Comment should be more than 3 characters!", "error")
            }
            //if()
        });
            
        $(document).on('click','.photoLike',function(){
            var photo_id = $(this).data('photo_id');
             $.ajax({
                type: 'POST',
                url: '../../../../addlikephoto',
                data: {photo_id:photo_id},
                dataType:'json',
                success: function(response){
                    if(response.status == 'add'){
                        
                    }else if(response.status == 'delete'){
                        
                    }
                    $('.like_count').text(response.like_count);
                    
                }
            });
        });
        
});