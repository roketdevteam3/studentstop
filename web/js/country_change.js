$(document).ready(function(){
    $('[name=country_list]').change(function(){
        var country_id = $(this).val();
        $.ajax({
            url:'../../../../getstates',
            method:'POST',
            data:{country_id:country_id},
            dataType:'json',
        }).done(function(response){
            $('[name=states_list]').html('');
            $('[name=states_list]').append('<option>Select city</option>');
            for(var key in response){
                $('[name=states_list]').append('<option value="'+key+'">'+response[key]+'</option>');
            }
        });
    });
});