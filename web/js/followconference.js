$(document).ready(function(){
        
    $(document).on('click','.JoinConference',function(){
        $('#VirtualClassInfoModal').modal('show');
        var conference_id = $(this).data('conference_id');
        $.ajax({
            type: 'POST',
            url: '../../../course/info/getvirtualclassinfo',
            data: {conference_id:conference_id},
            dataType:'json',
        }).done(function(response){
            if(response.status == 'success'){
                console.log(response);
                $('#VirtualClassInfoModal').find('.vitrualClassTitle').text(response.conference['name'])
                $('#VirtualClassInfoModal').find('.vitrualClassDescription').text(response.conference['description'])
                $('#VirtualClassInfoModal').find('.vitrualClassDateStart').text(response.conference['date_start'])
                if((response.conference['price_m'] != '') && (response.conference['price_m']) != null){
                    $('#VirtualClassInfoModal').find('.vitrualClassPrice_M').text(response.conference['price_m']+'$');
                    $('.moneyform #pay_price').val(response.conference['price_m']);
                    $('.moneyform #pay_class_id').val(response.conference['id']);
                    $('.moneyform').show()
                }else{
                    $('.moneyform').hide()
                }
                if((response.conference['price_c'] != '') && (response.conference['price_c']) != null){
                    $('#VirtualClassInfoModal').find('.vitrualClassPrice_C').text(response.conference['price_c']+' Credits');
                    $('.creditsform #pay_price').val(response.conference['price_m']);
                    $('.creditsform #pay_class_id').val(response.conference['id']);
                    $('.creditsform').show();
                }else{
                    $('.creditsform').hide();
                }
//                var type = ''
//                if(response.conference['price_type'] == 0){
//                    type = ' $';
//                    $("#form_pay").attr("action", "/virtualclass/pay_class");
//                }else{
//                    type = ' credits';
//                    $("#form_pay").attr("action", "/virtualclass/pay_class_credits");
//                }
                $('#VirtualClassInfoModal').find('.tutorUsername').text(response.tutors_info['username']+' '+response.tutors_info['surname'])
                if(response.tutors_info['avatar'] != ''){
                    $('#VirtualClassInfoModal').find('.tutorAvatar').text(response.tutors_info['avatar'])
                }
//                $('#pay_price').val(response.conference['price']);
//                $('#pay_class_id').val(response.conference['id']);
            }
        });
        
    });
    
//    $(document).on('click','.JoinConference',function(){
//        var conference_id = $(this).data('conference_id');
//        var thisElement = $(this);
//        $.ajax({
//            type: 'POST',
//            url: '../../../followconference',
//            data: {conference_id:conference_id, status:"Follow"},
//            dataType:'json',
//        }).done(function(response){
//            if(response.status == 'success'){
//                thisElement.removeClass('JoinConference');
//                thisElement.addClass('UnfollowConference');
//                thisElement.find('.joinText').text('Unfollow');
//            }
//        });
//    });
    
//    $(document).on('click','.UnfollowConference',function(){
//        var conference_id = $(this).data('conference_id');
//        var thisElement = $(this);
//        $.ajax({
//            type: 'POST',
//            url: '../../../followconference',
//            data: {conference_id:conference_id, status:"Unfollow"},
//            dataType:'json',
//        }).done(function(response){
//            if(response.status == 'success'){
//                thisElement.removeClass('UnfollowConference');
//                thisElement.addClass('JoinConference');
//                thisElement.find('.joinText').text('Follow');
//            }
//        });
//    });
        
    
});        