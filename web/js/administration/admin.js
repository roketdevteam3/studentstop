// Flot Charts sample data for SB Admin template

$(document).ready(function () {
    $(document).on('change','#categories-parent_id',function(){
            if($(this).val() != ''){
                $('#categories-category_type').parent().hide()
            }else{
                $('#categories-category_type').parent().show()
            }
        });

    /**
     * Check height blocks dashboard
     */
    if($('.mr-dashboard .mr-item-dashboard').length > 0){
        var max_height_dahboard_item = 0;
        $('.mr-dashboard .mr-item-dashboard').each(function(index,item){
            if($(item).height() > max_height_dahboard_item){
                max_height_dahboard_item = $(item).height();
            }
        });
        $('.mr-dashboard .info-box').css({"height":max_height_dahboard_item+'px'});
    }

    $("[ajax-form]").submit(function (e) {
        e.preventDefault();
        var $form = $(this);
        var formData = new FormData($form[0]);
        $.ajax({
            url: $(this).attr("action"),
            dataType: "json",
            method: "POST",
            mimeType: "multipart/form-data",
            contentType: false,
            processData: false,
            data: formData,
            success: function (data) {
                if (data.status == "ok") {
                    $form.find(".mr-msg").removeClass().addClass("mr-msg alert alert-success").show("slow");
                    $form.find(".mr-msg").find(".mr-msg-text").empty().append(data.msg);
                } else {
                    $form.find(".mr-msg").removeClass().addClass("mr-msg alert alert-danger").show("slow");
                    $form.find(".mr-msg").find(".mr-msg-text").empty().append(data.msg);
                }

            }
        });

    });

    $("[ajax-delete]").click(function (e) {
        e.preventDefault();
        var block = $(this).data("parent-block");
        var $block = $(block);
        $.ajax({
            url: $(this).data("url"),
            dataType: "json",
            method: "GET",
            success: function (data) {
                if (data.status == "ok") {
                    $block.find(".mr-msg").removeClass().addClass("mr-msg alert alert-success").show("slow");
                    $block.find(".mr-msg").find(".mr-msg-text").empty().append(data.msg);
                } else {
                    $block.find(".mr-msg").removeClass().addClass("mr-msg alert alert-danger").show("slow");
                    $block.find(".mr-msg").find(".mr-msg-text").empty().append(data.msg);
                }

            }
        });

    });


});


$(function () {

    var body = $('body');
    body.on('click', '.confirm-delete', function () {
        var button = $(this).addClass('disabled');
        var title = button.attr('title');

        if (confirm(title ? title + '?' : 'Confirm the deletion')) {
            if (button.data('reload')) {
                return true;
            }
            $.getJSON(button.attr('href'), function (response) {
                button.removeClass('disabled');
                if (response.result === 'success') {
                    notify.success(response.message);
                    button.closest('tr').fadeOut(function () {
                        this.remove();
                    });
                } else {
                    alert(response.error);
                }
            });
        }
        return false;
    });

    body.on('click', '.move-up, .move-down', function (e) {
        e.preventDefault();
        var button = $(this).addClass('disabled');

        $.getJSON(button.attr('href'), function (response) {
            button.removeClass('disabled');
            if (response.result === 'success' && response.swap_id) {
                var current = button.closest('tr');
                var swap = $('tr[data-id=' + response.swap_id + ']', current.parent());
                swap = swap.length > 0 ? swap : $('tr[data-key=' + response.swap_id + ']', current.parent());

                if (swap.get(0)) {
                    if (button.hasClass('move-up')) {
                        swap.before(current);
                    } else {
                        swap.after(current);
                    }
                } else {
                    //  location.reload();
                }
            }
            else if (response.error) {
                alert(response.error);
            }
        });

        return false;
    });

});




function drag_block(e, ui) {
    if (ui.item.data('url') !== undefined && ui.item.data('url') !== '') {
        if (ui.item.data('id') !== undefined && ui.item.data('id') !== '') {
            var item = {};
            ui.endparent.find('li').each(function (i, obj) {
                i++;
                if (ui.item.data('id') !== undefined && ui.item.data('id') !== '')
                    item[$(this).data('id')] = i;
            });
            $.ajax({
                type: "POST",
                url: ui.item.data('url'),
                data: item,
                success: function (msg) {
                    console.log(msg);
                },
                error: function (xhr, status, text) {
                    console.log(text);
                }
            });
        }

    }
    return false;

}

/**
 *
 * @param _this
 */
function switcher(_this) {
    $.getJSON(_this.data('url') + (_this.siblings('.mr-switch-checkbox').is(':checked') ? 'off' : 'on') + '/?id=' + _this.data('id'), function (response) {
        if (response.result === 'error') {
            alert(response.error);
        } else {
            _this.toggleClass('off');
            _this.closest('tr').toggleClass('mr-disabled');
            if (_this.hasClass('off')) {
                _this.siblings('.mr-switch-checkbox').prop("checked", false);
            } else {
                _this.siblings('.mr-switch-checkbox').prop("checked", true);
            }
        }
    });
}

$(document).ready(function() {
    $("body").niceScroll({
        cursorcolor: "#222D32", // change cursor color in hex
        cursorborder: "1px solid #222D32", // css definition for cursor border
        cursorwidth: "15px",
        cursorborderradius: "0px", // border radius in pixel for cursor
        zindex: "auto"
    });
    
    if($('.newPhotoU').length){
        var previewNode = document.querySelector(".previewTemplateQ");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzoneA = new Dropzone($('.newPhotoU')[0], {
            maxFiles:1,
            uploadMultiple:false,
            url: "../../../university/saveduniversityphoto",
            previewTemplate:previewTemplate,
            clickable: ".newPhoto",
            previewsContainer: ".newPhotoU",
        });

        myDropzoneA.on("maxfilesexceeded", function(file) {
                myDropzoneA.removeAllFiles();
                myDropzoneA.addFile(file);

        });

        myDropzoneA.on("complete", function(response) {
            if (response.status == 'success') {
                $('#university-image').val(response.xhr.response);
                //$('.eventButton').show();
            }
        });

        myDropzoneA.on("removedfile", function(response) {
            if(response.xhr != null){
               //deleteFile(response.xhr.response);
            }
        });
    }
});