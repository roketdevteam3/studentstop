$(document).ready(function(){
     var photosArray = [];
     
     
            function deleteFile(file_name){
                $.ajax({
                    type: 'POST',
                    url: '../../posts/posts/deletepostsphoto',
                    data: {file_name:file_name},
                    success: function(response){
                    }
                });
            }
            if($('.post_container_photo').length){
                var previewNode = document.querySelector(".previewTemplateQ");
                previewNode.id = "";
                var previewTemplate = previewNode.parentNode.innerHTML;
                previewNode.parentNode.removeChild(previewNode);
                if($('.post_container_photo').length > 0){
                    var myDropzoneA = new Dropzone($('.post_container_photo')[0], { 
                        maxFiles:1,
                        uploadMultiple:false,
                        url: "../../posts/posts/savepostsphoto",
                        previewTemplate:previewTemplate,
                        clickable: ".newPostPhoto",
                        previewsContainer: ".post_container_photo",
                    });

                    myDropzoneA.on("maxfilesexceeded", function(file) {
                            myDropzoneA.removeAllFiles();
                            myDropzoneA.addFile(file);

                    });

                    myDropzoneA.on("complete", function(response) {
                        if (response.status == 'success') {
                            $('#posts-image_src').val(response.xhr.response);
                        }
                    });

                    myDropzoneA.on("removedfile", function(response) {
                        if(response.xhr != null){
                           deleteFile(response.xhr.response);
                        }
                    });
                }
            }
            
            
            if($('.post_photos').length > 0){
            
                var myDropzoneR = new Dropzone($('.post_photos')[0], { // Make the whole body a dropzone
                    maxFiles:3,
                });

                myDropzoneR.on("sending", function(file) {
                    console.log(file);
                  //  file.name = file.name+Math.random().toString(36).substring(7); 
                  // Hookup the start button
                  //file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file); };
                });
                // Update the total progress bar

                myDropzoneR.on("complete", function(response) {
                    console.log(response);
                    if (response.status == 'success') {
                        //console.log(response);
                        $(response.previewElement).prepend('<i class="fa fa-times removePhoto fa-2x" title="delete" style="color:#DBDBDB;cursor:pointer;right:-5px;top:-10px;position:absolute;z-index:999" data-name="'+response.name+'"></i>');
                        photosArray.push(response.xhr.response);
                        $('#postsphotos-image_src').val(JSON.stringify(photosArray));
                    }
                });

                myDropzoneR.on("removedfile", function(response) {
                  //alert('dvdv');
                    //console.log();
                    var removeItem = response.xhr.response;

                    photosArray = jQuery.grep(photosArray, function(value) {
                      return value != removeItem;
                    });

                    $('#postsphotos-image_src').val(JSON.stringify(photosArray));
                    deleteFile(response.xhr.response);
                });
            }
            $(document).on('click','.removePhoto',function(){
                var shos = $(this).parent();
                
                var name_photo = $(this).data('name');
                $.each( myDropzoneR.files, function( key, value ) {
                    if(value.name == name_photo){
                        myDropzoneR.removeFile(myDropzoneR.files[key]);
                    }
                });
            });
});