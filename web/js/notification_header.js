$(document).ready(function(){
    var paginations = {
        limit: 16,
        offset: 0
    };
    
    function getNotifications(user_id, module_type){
        $.ajax({
            type: 'POST',
            url: '../../../../users/notification/getnotification',
            data: {user_id:user_id, module_type:module_type,limit:paginations.limit,offset:paginations.offset},
            dataType:'json',
        }).done(function(response){
            var n_block = $('.modalNotificationRightBlock').find('.notificationTable');
            
            if(response.status == 'success'){
//                n_block.append('<table class="table notificationTable"></table>')
                
                if(response.info.length < 16){
                    $(".show_more_ntf").hide();
                }else{
                    $(".show_more_ntf").show();
                }
                for(var key in response.info){
                    var notification = response.info[key];
                    var notificationColor = 'background-color:white;';
                    if(notification.status == 0){
                        notificationColor = 'background-color:#C7D6FF;';
                    }
                        $('.notificationTable').append('<tr class="notificationUnreaded" data-notification_id = '+notification.id+'; style="'+notificationColor+'">'+
                                '<td>'+
                                    notification.id+
                                '</td>'+
                                '<td>'+
                                    notification.module+
                                '</td>'+
                                '<td>'+
                                    notification.notification_type+
                                '</td>'+
                                '<td>'+
                                    notification.date_create+
                                '</td>'+
                            '</tr>')                        
                }
            }else{                
            }
        });        
    }
    
    $(document).on('click', '.refreshNotification',function(){
        var user_id = $('.nav-notification').find('.notification_v').data('id')
        var filterValue = $('.changeNotificationModal').val();
        $('.modalNotificationRightBlock').find('.notificationTable').html('')
        
        paginations.offset = 0;
        getNotifications(user_id, filterValue);
    });
    
    
    $(document).on('click', '.nav-notification',function(){
        var n_block = $('.modalNotificationRightBlock');
        var user_id = $(this).find('.notification_v').data('id')
        $('.modalNotificationRightBlock').find('.notificationTable').html('')
        var filterValue = $('.changeNotificationModal').val();
        n_block.show();
        
        paginations.offset = 0;
        getNotifications(user_id, filterValue);
    });
    
    $(document).on('click', '.show_more_ntf',function(){
        var user_id = $('.nav-notification').find('.notification_v').data('id')
        paginations.offset += paginations.limit;
        var filterValue = $('.changeNotificationModal').val();
        
        getNotifications(user_id, filterValue);
    })
    
    $(document).on('change', '.changeNotificationModal',function(){
        var user_id = $('.nav-notification').find('.notification_v').data('id')
        var module_t = $(this).val();
        $('.modalNotificationRightBlock').find('.notificationTable').html('')
        paginations.offset = 0;
        
        getNotifications(user_id, module_t);
        
    })
    
    
    
    $(document).on('mouseenter', '.notificationUnreaded',function(){
        var notification_id = $(this).data('notification_id');  
        var thisElement = $(this);
        $.ajax({
            type: 'POST',
            url: '../../../../users/notification/notificationunreaded',
            data: {notification_id:notification_id},
            dataType:'json',
        }).done(function(response){
            if(response.status == 'success'){
                thisElement.removeClass('notificationUnreaded');
                thisElement.css('background-color','white');
                $('.ntfCount').find('.badge').text(response.ntf_count);
            }
        });
        
    })
    
    
    
    
    function getnotificationunread(){
        $.ajax({
            type: 'POST',
            url: '../../../../users/notification/getnotificationunread',
            data: {},
            dataType:'json',
        }).done(function(response){
            $('.ntfCount').find('.badge').text(response);
        })
    }
    
    setInterval(function(){
        getnotificationunread();
    },5000);
    
})