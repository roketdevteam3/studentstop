$(document).ready(function(){
    $('#rm-info').click(function() {
        $('#info-rm').toggleClass('size-rm-block-style');
    });
    $('#rm-about').click(function() {
        $('#about-mr').toggleClass('size-rm-block-style');
        $('#rm-text-about').toggleClass('size-rm-block-style');
    });


    $(document).on('change','#userinfo-university_id',function(){
        var university_id = $(this).val();
        $.ajax({
            type: 'POST',
            url: '../../../getmajorwithuniversity',
            data: {university_id:university_id},
            dataType:'json',
            success: function(response){
                $('#userinfo-major_id').html('');
                for(var key in response){
                    $('#userinfo-major_id').append('<option value="'+key+'">'+response[key]+'</option>');
                }

            }
        });

      $.ajax({
        type: 'POST',
        url: '../../../get_address_university',
        data: {university_id:university_id},
        dataType:'json',
        success: function(response){
          if(response)
            $('#userinfo-address').val(response.location);
        }
      });
        
    });

});