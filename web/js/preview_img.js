/**
 * Created by mackrais on 28.10.15.
 */

function readURL(input) {
    $('.mr-preview-block').remove();
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var $img = $('<img />');
        var $div = $('<div></div>');
        var $remove = $('<span>✖</span>');
        var type =  input.files[0].type.toString();
        var type_sigment = type.split("/");

        if(type_sigment[0] !== 'image'){
            $(input).val('');
            return false;
        }

        reader.onload = function (e) {
            $remove.addClass('mr-preview-remove-img');

            $img.addClass('mr-preview-img').attr('src', e.target.result);
            $div.addClass('mr-preview-block');
            $div.append($remove);
            $div.append($img);
            $div.insertBefore(input);
            $div.fadeIn('slow');
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$(document).on('click','.mr-preview-remove-img',function(){
    $(this).parents('.mr-preview-block').siblings('input:file').val('');
    $.when( $(this).closest('.mr-preview-block').fadeOut('slow') ).then(function() {
        $('.mr-preview-block').remove();
    });
})

$('[data-preview="true"]').change(function(){
    readURL(this);
});