$(document).ready(function() {

  $(document).on('click', '#apply_job_status_accept', function () {
    var job_id = $(this).data('job_id');
    var user_id = $(this).data('user_id');
    var user_job_id = $(this).data('user_job_id');

    $.post( '../../apply_job_status',{user_job_id:user_job_id, user_id: user_id, job_id: job_id, status: 1 }, function( data ) {
      swal("Accepted!", "", "success");
      // location.reload();
      $('#job_status_'+user_job_id).html('Accept');
    });

  });

  $(document).on('click', '#apply_job_status_maybe', function () {
    var job_id = $(this).data('job_id');
    var user_id = $(this).data('user_id');
    var user_job_id = $(this).data('user_job_id');

    $.post( '../../apply_job_status',{user_job_id:user_job_id, user_id: user_id, job_id: job_id, status: 2 }, function( data ) {
      swal("Maybe!", "", "warning");
      // location.reload();
      $('#job_status_'+user_job_id).html('Maybe');
    });

  });

  $(document).on('click', '#apply_job_status_decline', function () {
    var job_id = $(this).data('job_id');
    var user_id = $(this).data('user_id');
    var user_job_id = $(this).data('user_job_id');

    $.post( '../../apply_job_status',{user_job_id:user_job_id, user_id: user_id, job_id: job_id, status: 3 }, function( data ) {
      swal("Not suitable!", "", "error");
      // location.reload();
      $('#job_status_'+user_job_id).html('Not suitable');
    });

  });

  $(document).on('click', '#offer_job_status_accept', function () {
    var job_id = $(this).data('job_id');
    var user_id = $(this).data('user_id');
    var user_job_id = $(this).data('user_job_id');

    $.post( '../../offer_job_status',{user_job_id:user_job_id, user_id: user_id, job_id: job_id, status: 1 }, function( data ) {
      swal("Accepted!", "", "success");
      // location.reload();
      $('#offer_job_status_'+user_job_id).html('Accept');
    });

  });

  $(document).on('click', '#apply_job_status_maybe', function () {
    var job_id = $(this).data('job_id');
    var user_id = $(this).data('user_id');
    var user_job_id = $(this).data('user_job_id');

    $.post( '../../offer_job_status',{user_job_id:user_job_id, user_id: user_id, job_id: job_id, status: 2 }, function( data ) {
      swal("Maybe!", "", "warning");
      // location.reload();
      $('#offer_job_status_'+user_job_id).html('Maybe');
    });

  });

  $(document).on('click', '#offer_job_status_decline', function () {
    var job_id = $(this).data('job_id');
    var user_id = $(this).data('user_id');
    var user_job_id = $(this).data('user_job_id');

    $.post( '../../offer_job_status',{user_job_id:user_job_id, user_id: user_id, job_id: job_id, status: 3 }, function( data ) {
      swal("Not suitable!", "", "error");
      // location.reload();
      $('#offer_job_status_'+user_job_id).html('Not suitable');
    });

  });

});