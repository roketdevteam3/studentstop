$(document).ready(function(){
    
//        function deleteFile(file_name){
//            $.ajax({
//                type: 'POST',
//                url: '../../../deletefile',
//                data: {file_name:file_name},
//                success: function(response){
//                }
//            });
//        }
            
        if($('.company_logo_photo').length){
            var previewNode = document.querySelector("#previews");
            previewNode.id = "";
            var previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);

            var myDropzoneA = new Dropzone($('.company_logo_photo')[0], {
                maxFiles:1,
                uploadMultiple:false,
                url: "../../companylogosave",
                previewTemplate:previewTemplate,
                clickable: ".new_logo_photo",
                previewsContainer: ".company_logo_photo",
            });

            myDropzoneA.on("maxfilesexceeded", function(file) {
                    myDropzoneA.removeAllFiles();
                    myDropzoneA.addFile(file);
            });

            myDropzoneA.on("complete", function(response) {
                if (response.status == 'success') {
                    $('#company-logo_src').val(response.xhr.response);
                    //$('.eventButton').show();
                }
            });

            myDropzoneA.on("removedfile", function(response) {
                if(response.xhr != null){
                    alert('dcdc');
                   //deleteFile(response.xhr.response);
                }
            });
        }
        
        
        if($('.jobs_logo_photo').length){
            var previewNode = document.querySelector("#previews");
            previewNode.id = "";
            var previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);

            var myDropzoneA = new Dropzone($('.jobs_logo_photo')[0], {
                maxFiles:1,
                uploadMultiple:false,
                url: "../../jobslogosave",
                previewTemplate:previewTemplate,
                clickable: ".new_logo_photo",
                previewsContainer: ".jobs_logo_photo",
            });

            myDropzoneA.on("maxfilesexceeded", function(file) {
                    myDropzoneA.removeAllFiles();
                    myDropzoneA.addFile(file);
            });

            myDropzoneA.on("complete", function(response) {
                if (response.status == 'success') {
                    if($('.img_logo_preview').length){
                        $('.img_logo_preview').remove();
                    }
                    $('#jobs-logo_src').val(response.xhr.response);
                    //$('.eventButton').show();
                }
            });

            myDropzoneA.on("removedfile", function(response) {
                if(response.xhr != null){
                    $('#jobs-logo_src').val('');
                   //deleteFile(response.xhr.response);
                }
            });
        }
    
//        if($('.jobs_logo_photo').length){
//            var previewNode = document.querySelector("#previews");
//            previewNode.id = "";
//            var previewTemplate = previewNode.parentNode.innerHTML;
//            previewNode.parentNode.removeChild(previewNode);
//
//            var myDropzoneA = new Dropzone($('.jobs_logo_photo')[0], {
//                maxFiles:1,
//                uploadMultiple:false,
//                url: "../../jobslogosave",
//                previewTemplate:previewTemplate,
//                clickable: ".new_logo_photo",
//                previewsContainer: ".jobs_logo_photo",
//            });
//
//            myDropzoneA.on("maxfilesexceeded", function(file) {
//                    myDropzoneA.removeAllFiles();
//                    myDropzoneA.addFile(file);
//            });
//
//            myDropzoneA.on("complete", function(response) {
//                if (response.status == 'success') {
//                    $('#jobs-logo_src').val(response.xhr.response);
//                    //$('.eventButton').show();
//                }
//            });
//
//            myDropzoneA.on("removedfile", function(response) {
//                if(response.xhr != null){
//                   //deleteFile(response.xhr.response);
//                }
//            });
//        }
    
    $(document).on('click','.panelTitleClick',function(){
        $(this).parent().find('.panelBodyShow').toggle();
    });
    
    $(document).on('change','#jobs-vip_status',function(){
        
    });
    
    
});