$(document).ready(function(){
    $(document).on('click','.showResume',function(){
        $('#user_resume').modal('show');
        var user_id = $(this).data('user_id');
        $.ajax({
            type: 'POST',
            url: '../../userdataresume',
            data: {user_id:user_id},
            dataType:'json',
        }).done(function(response){
            
            console.log(response);
            console.log(response.resume);
            
            if(response.resume != null){
                if(response.resume.avatar != ''){
                    $('.UserAvatar').attr('src','../../../images/users_images/'+response.resume.avatar);
                }else{
                    if(response.user_info.avatar != ''){
                        $('.UserAvatar').attr('src','../../../images/users_images/'+response.user_info.avatar);
                    }else{
                        $('.UserAvatar').attr('src','../../../images/default_avatar.jpg');
                    }
                }
            }else{
                if(response.user_info.avatar != ''){
                    $('.UserAvatar').attr('src','../../../images/users_images/'+response.user_info.avatar);
                }else{
                    $('.UserAvatar').attr('src','../../../images/default_avatar.jpg');
                }
            }
            
            $('.UserAbout').html('');
            $('.UserAbout').append('<h2>'+response.user.name+' '+response.user.surname+'</h2>');
            if(response.resume != null){
                $('.UserAbout').append('<p>'+response.resume.about+'</p>');
            }
            
            $('.tableAbout').html('');
            if(response.user_info.birthday != ''){
                $('.tableAbout').append('<tr><td><b>Birthday</b></td><td>'+response.user_info.birthday+'</td></tr>');
            }
            if(response.user_info.mobile != ''){
                $('.tableAbout').append('<tr><td><b>Mobile</b></td><td>'+response.user_info.mobile+'</td></tr>');
            }
            if(response.user_info.address != ''){
                $('.tableAbout').append('<tr><td><b>Address</b></td><td>'+response.user_info.address+'</td></tr>');
            }
            if(response.user_info.website != ''){
                $('.tableAbout').append('<tr><td><b>Website</b></td><td>'+response.user_info.website+'</td></tr>');
            }
            if(response.user_info.facebook != ''){
                $('.tableAbout').append('<tr><td><b>Facebook</b></td><td>'+response.user_info.facebook+'</td></tr>');
            }
            if(response.user_info.twitter != ''){
                $('.tableAbout').append('<tr><td><b>Twitter</b></td><td>'+response.user_info.twitter+'</td></tr>');
            }   
        });
    });
    
    $(document).on('click','.inviteUser',function(){
        var jobs_id = $(this).data('job_id');
        var user_id = $(this).data('user_id');
        $.ajax({
            type: 'POST',
            url: '../../../userinvitejob',
            data: {user_id:user_id,jobs_id:jobs_id},
            dataType:'json',
        }).done(function(response){
            console.log(response.status);
            if(response.status == 'added'){
                swal('Sent invitations','','success');
            }else{
                swal('Invitation has been sent','','warning');
            }
        });
    });
    
});        