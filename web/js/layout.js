var massegeMap = '';
function initMasseageMap() {
    var myLatLng = {lat: 45.8152087, lng: -9.8816965};

    massegeMap = new google.maps.Map(document.getElementById('massegeMap'), {
      zoom: 2,
      center: myLatLng
    });
}

$(document).ready(function(){

    $(document).on('click','.messageMapButton',function(){
        var element = document.getElementById("massegeMap");
        var mapMes = new google.maps.Map(element, {
            center: new google.maps.LatLng( 45.8152087,  -9.8816965),
            zoom: 2,
        });

        $('#massegeMap').show();
        //initMasseageMap();
        $('.message-send-container').hide();
        var user_id = $(this).data('user_id');
        $.ajax({
            type: 'POST',
            url: '../../../users/default/messagemapdata',
            data: {user_id:user_id},
            dataType:'json',
            success: function(response){

                var markerUniver = '';
                var addressU = response.university_info['location'];
                if(addressU != ''){
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({'address': addressU}, function(results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {
                            var Local = results[0].geometry.location;
                            var latLng = {lat:parseFloat(Local.lat()),lng:parseFloat(Local.lng())};

                            var icon = {
                                url:'../../../../images/university_map_icon.png',
                                scaledSize: new google.maps.Size(32,34)
                            };
                            markerUniver = new google.maps.Marker({
                                position: latLng,
                                map: mapMes,
                                icon:icon
                            });
                        }
                    });
                }
                var universityUsers = response.university_users;


                for(var key in universityUsers){
                    var user = universityUsers[key];
                    console.log(user)
                    var markerUser;
                    var icon = {
                        url:'../../../../images/marker_offline.png',
                        scaledSize: new google.maps.Size(32,34)
                    };
                    var zIndex = 2;
                    if(user['id'] == user_id){
                        var icon = {
                            url:'../../../../images/marker_online.png',
                            scaledSize: new google.maps.Size(32,34)
                        };
                        zIndex = 10;

                    }
                    var user_address = user['address'];

                    var position_lat = user['position_lat'];
                    var position_lng = user['position_lng'];
//                    console.log(user_address);
//                    console.log(position_lat);
//                    console.log(position_lng);
                    if((position_lat != '') && (position_lat != null)){
                        markerUser = new google.maps.Marker({
                            position: {lat: parseFloat(position_lat), lng: parseFloat(position_lng)},
                            map: mapMes,
                            icon:icon,
                            zIndex: zIndex,
                            title: user['name']+' '+user['surname'],
                        });
                        console.log('lat-lng')
                    }else if((user_address != '')&& (user_address != null)){
                        var geocoder = new google.maps.Geocoder();
                        geocoder.geocode({'address': user_address}, function(results, status) {
                            if (status === google.maps.GeocoderStatus.OK) {
                                var Local = results[0].geometry.location;
                                var latLng = {lat:parseFloat(Local.lat()),lng:parseFloat(Local.lng())};
                                markerUser = new google.maps.Marker({
                                    position: latLng,
                                    map: mapMes,
                                    icon:icon,
                                    zIndex: zIndex,
                                    title: user['name']+' '+user['surname'],
                                });
                            }
                        });
                        console.log('geocode')
                    }
                }

            }
        });
    })

    $(document).on('click','.allMassage',function(){
        $('#massegeMap').hide();
        $('.message-send-container').show();
    });

})

var Layout = {

	// Layout Theme Settings
	settings: {

		autoScrollWhenMenuIsActive: false,
		improvePerformance: true,
		rtl: false,
		themeClass: '' // default color is indigo, available classes: brown, deep-purple, grey, red, teal

	},

	handleRtlLayout: function () {
		$body = $('body');

		if(this.settings.rtl)
			$body.addClass('layout-rtl');
		else
			$body.removeClass('layout-rtl');
	},

	handleThemeColor: function () {
		$body = $('body');

		$body.removeClass(function (index, css) {
			return (css.match (/(^|\s)theme-\S+/g) || []).join(' ');
		}).addClass('theme-'+this.settings.themeClass);
	},

	initLayer: function () {
		// Define neccessary elements
		var $windowHeight = $(window).height(),
				$windowWidth = $(window).width(),
				$startingPoint = $('.starting-point');

		// Calculate the diameter
		var diameterValue = (Math.sqrt( Math.pow($windowHeight, 2) + Math.pow($windowWidth, 2) ) * 2);

		// Animate
		$startingPoint.children('span').velocity({
			scaleX: 0,
			scaleY: 0,
			translateZ: 0,
		}, 50).velocity({
			height: diameterValue+'px',
			width: diameterValue+'px',
			top: -(diameterValue/2)+'px',
			left: -(diameterValue/2)+'px'
		}, 0);
	},

	layerResponsive: function () {
		// Define neccessary elements
		var $windowHeight = $(window).height(),
				$windowWidth = $(window).width(),
				$startingPoint = $('.starting-point');

		// Calculate the diameter
		var diameterValue = (Math.sqrt( Math.pow($windowHeight, 2) + Math.pow($windowWidth, 2) ) * 2);

		$startingPoint.children('span').css({
			height: diameterValue+'px',
			width: diameterValue+'px',
			top: -(diameterValue/2)+'px',
			left: -(diameterValue/2)+'px'
		});
	},

	listenForMenuLayer: function () {
		$('.nav-menu').on('click', function() {

			// Define neccessary elements
			var $this = $(this),
					$startingPoint = $('.starting-point'),
					$overlay = $('.overlay'),
					$menu = $('.menu-layer'),
					$overlaySecondary = $('.overlay-secondary'),
					$content = $('.content'),
					$body = $('body'),
					$scrollableTabs = $('.scrollable-tabs');

			// Add "open" class to active menu
			$menu.find('[data-open-after="true"]').addClass('open');

			// If Menu Layer is not active
			if( !$this.hasClass('active') ) {

				if( !Pleasure.checkTouchScreen() && !Layout.settings.improvePerformance ) // If screen is desktop, add scaled effect
					$content.addClass('scaled');

				$menu.addClass('activating');
				$scrollableTabs.addClass('disabled');

				setTimeout(function () {
					$body.addClass('disable-scroll');
					$overlay.addClass('overlay-nav active');
					$this.addClass('active');
					$menu.addClass('active');
					$overlaySecondary.addClass('fade-in');

					$menu.find('[data-open-after="true"]').parents('li').addClass('open animate');

					$.Velocity.animate($startingPoint.children('span'), {
						translateZ: 0,
						scaleX: 1,
						scaleY: 1
					}, { duration: 500, easing: [.42, 0, .58, 1] });

					if( Layout.settings.autoScrollWhenMenuIsActive ) {
						setTimeout(function () {
							$menu.animate({ scrollTop: $menu.find('[data-open-after="true"]').position().top + 200 }, 300);
						}, 600);
					}
				}, 50);

			} else {
			// If Menu Layer is active
				$this.addClass('rotating');

				$overlaySecondary.removeClass('fade-in');
				$content.removeClass('scaled');

				setTimeout(function() {
					$menu.removeClass('active');
					$.Velocity.animate($startingPoint.children('span'), {
						translateZ: 0,
						scaleX: 0,
						scaleY: 0
					}, {
						duration: 500,
						easing: [.42, 0, .58, 1],
						complete: function () {
							$overlay.removeClass('active overlay-nav');
							$this.removeClass('active rotating');
							$menu.removeClass('activating');
							$body.removeClass('disable-scroll');
							$menu.find('.open').removeClass('open animate');
							$scrollableTabs.removeClass('disabled');
						}
					})
				}, 200);

			}
		});

		// Close menu by clicking overlay
		$('.overlay-secondary').on('click', function () {
			$('.nav-menu').trigger('click');
		});
	},

	listenForSearchLayer: function () {
		$('.nav-search').on('click', function(){

			// Define neccessary elements
			var $this = $(this),
					$startingPoint = $('.starting-point'),
					$overlay = $('.overlay'),
					$body = $('body'),
					$content = $('.content'),
					$searchLayer = $('.search-layer'),
					$scrollableTabs = $('.scrollable-tabs');

			// If Search Layer is not active
			if( !$this.hasClass('active') ) {

				if( !Pleasure.checkTouchScreen() && !Layout.settings.improvePerformance ) // If screen is desktop, add scaled effect
					$content.addClass('scaled');

				$searchLayer.addClass('activating');
				$scrollableTabs.addClass('disabled');

				setTimeout(function () {
					$body.addClass('disable-scroll');
					$overlay.addClass('overlay-search active');
					$this.addClass('active');
					$searchLayer.addClass('active');
					$.Velocity.animate($startingPoint.children('span'), {
						translateZ: 0,
						scaleX: 1,
						scaleY: 1
					}, {
						duration: 500,
						easing: [.42, 0, .58, 1],
						complete: function () {
							if( !Pleasure.checkTouchScreen() ) // if screen is desktop, focus search input
								$searchLayer.find('input').focus();
						}
					});
				}, 50);

			} else {
			// If Search Layer is active
				$searchLayer.find('input').blur();
				$searchLayer.removeClass('active');
				$this.addClass('rotating');
				$content.removeClass('scaled');

				setTimeout(function() {
					$.Velocity.animate($startingPoint.children('span'), {
						translateZ: 0,
						scaleX: 0,
						scaleY: 0
					}, {
						duration: 500,
						easing: [.42, 0, .58, 1],
						complete: function () {
							$this.removeClass('active rotating');
							$overlay.removeClass('active overlay-search');
							$searchLayer.removeClass('activating');
							$body.removeClass('disable-scroll');
							$scrollableTabs.removeClass('disabled');
						}
					});
				}, 200);
			}
		});
	},

	listenForUserLayer: function () {
		$('.nav-user').on('click', function() {
                    $('.correspondence-user-col').show();
                    $('.message-send-col').hide();
                    // Define neccessary elements
			var $this = $(this),
					$startingPoint = $('.starting-point'),
					$overlay = $('.overlay'),
					$body = $('body'),
					$content = $('.content'),
					$userLayer = $('.user-layer'),
					$messages = $('.messages'),
					$scrollableTabs = $('.scrollable-tabs');

			// When Message Tab is active
			$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
				var target = $(e.target).attr('href');
				if ((target == '#messages'))
					$messages.scrollTop( $messages.prop('scrollHeight') );
			});


			// If User Layer is not active
			if( !$this.hasClass('active') ) {

				if( !Pleasure.checkTouchScreen() && !Layout.settings.improvePerformance ) // If screen is desktop, add scaled effect
					$content.addClass('scaled');

				$userLayer.addClass('activating');
				$scrollableTabs.addClass('disabled');

				setTimeout(function () {
					$('#send-message-input').val('').trigger('input');

					//$body.addClass('disable-scroll');
					$overlay.addClass('overlay-user active');
					//$this.addClass('active');
					$userLayer.addClass('active');

					$messages.scrollTop( $messages.prop('scrollHeight') );

					$.Velocity.animate($startingPoint.children('span'), {
						translateZ: 0,
						scaleX: 1,
						scaleY: 1
					}, { duration: 500, easing: [.42, 0, .58, 1] });
				}, 50);

			} else {
			// If User Layer is active
				$userLayer.removeClass('active');
				$this.addClass('rotating');
				$content.removeClass('scaled');

				setTimeout(function() {
					$.Velocity.animate($startingPoint.children('span'), {
						translateZ: 0,
						scaleX: 0,
						scaleY: 0
					}, {
						duration: 500,
						easing: [.42, 0, .58, 1],
						complete: function () {
							$this.removeClass('active rotating');
							$overlay.removeClass('active overlay-user');
							$userLayer.removeClass('activating');
							$body.removeClass('disable-scroll');
							$scrollableTabs.removeClass('disabled');
						}
					});
				}, 200);
			}
		});
	},
	listenForVideoLayer: function () {
		$('.nav-video').on('click', function() {

			// Define neccessary elements
			var $this = $(this),
					$startingPoint = $('.starting-point'),
					$overlay = $('.overlay'),
					$body = $('body'),
					$content = $('.content'),
					$userLayer = $('.video-layer'),
					$messages = $('.messages'),
					$scrollableTabs = $('.scrollable-tabs');

			// If User Layer is not active
			if( !$this.hasClass('active') ) {

				if( !Pleasure.checkTouchScreen() && !Layout.settings.improvePerformance ) // If screen is desktop, add scaled effect
					$content.addClass('scaled');

				$userLayer.addClass('activating');
				$scrollableTabs.addClass('disabled');

				setTimeout(function () {
					$('#send-message-input').val('').trigger('input');

					$body.addClass('disable-scroll');
					$overlay.addClass('overlay-user active');
					$this.addClass('active');
					$userLayer.addClass('active');

					$messages.scrollTop( $messages.prop('scrollHeight') );

					$.Velocity.animate($startingPoint.children('span'), {
						translateZ: 0,
						scaleX: 1,
						scaleY: 1
					}, { duration: 500, easing: [.42, 0, .58, 1] });
				}, 50);

			} else {
			// If User Layer is active
				$userLayer.removeClass('active');
				$this.addClass('rotating');
				$content.removeClass('scaled');

				setTimeout(function() {
					$.Velocity.animate($startingPoint.children('span'), {
						translateZ: 0,
						scaleX: 0,
						scaleY: 0
					}, {
						duration: 500,
						easing: [.42, 0, .58, 1],
						complete: function () {
							$this.removeClass('active rotating');
							$overlay.removeClass('active overlay-user');
							$userLayer.removeClass('activating');
							$body.removeClass('disable-scroll');
							$scrollableTabs.removeClass('disabled');
						}
					});
				}, 200);
			}
		});
	},

	listenKeyboardEvents: function () {

		$(document).keyup(function(e) {

			// Define neccessary elements
			var $navUser = $('.nav-user'),
					$navSearch = $('.nav-search'),
					$navMenu = $('.nav-menu');

			// If user press ESC and Menu Layer is active
			if (e.keyCode == 27 && $navMenu.hasClass('active')) {
				$navMenu.trigger('click');
			}

			// If user press ESC and Search Layer is active
			if (e.keyCode == 27 && $navSearch.hasClass('active')) {
				$navSearch.trigger('click');
			}

			// If user press ESC and User Layer is active
			if (e.keyCode == 27 && $navUser.hasClass('active')) {
				$navUser.trigger('click');
			}

		});
	},

	listenMenu: function () {

		// If user click any menu link
		$('.menu-layer a').on('click', function () {

			// Define neccessary elements
			var $this = $(this),
					$menu = $(this).parents('.menu-layer');

			if( $this.parents('ul').hasClass('child-menu') ) {
				// If user clicks child menu
				if( $this.parent().hasClass('open') ) {
					// If user wants to close the opened child menu
					$this.parent().removeClass('open animate');
				} else {
					// If user clicks new child menu in parent menu
					$this.parent().siblings('.has-child').removeClass('open animate');
					$this.parent().addClass('open');
					setTimeout(function () {
						$this.parent().addClass('animate');
					}, 100);
				}
			} else {
				// If user clicks parent menu
				if( $this.parents('li').hasClass('open') ) {
					// If user wants to close the parent menu
					$this.parents('li').removeClass('animate open');
				} else {
					// If user clicks another parent menu
					$menu.find('.open').removeClass('animate open');
					$this.parents('li').addClass('open');
					setTimeout(function () {
						$this.parents('li').addClass('animate');
					}, 10);
				}
			}
		});

		// Adding child icon to parent menu links
		$('.menu-layer').find('li:has("ul")').each( function () {
			$(this).addClass('has-child');
		});

		// Adding neccessary elements to the menu links to use for animation
		$('.menu-layer').find('li').each( function () {
			$(this).append('<span class="hover-bg"></span>');
		});

	},

	// Handle Sidebar Menu by Viewport
	handleSidebar: function () {
		var $window = $(window),
				$body = $('body');

		if( $window.width() <= 767 ) {
			$body.addClass('layout-device layout-tablet');
		} else if( $window.width() > 767 && $window.width() < 990 ) {
			$body.addClass('layout-tablet');
			$body.removeClass('layout-device');
		} else if( $window.width() > 990 ) {
			$body.removeClass('layout-device layout-tablet');
		}
		Layout.resetSendMessage();
	},

	// Listen Message Events
	listenMessageEvents: function () {

		// Open Single Message
		$(document).on('click','.message-list>li', function () {
			$(this).parent().find('.selected').removeClass('selected');
			$(this).addClass('selected');
			$('#messages').addClass('open');

			Layout.getMessageById( $(this).find('a').data('message-id') );
		});

		// When mobile close message overlay
		$('.message-list-overlay').on('click', function () {
			$('#messages').removeClass('open');
			$('.message-list').find('.selected').removeClass('selected');
		});

		$('.mobile-back-button').on('click', function () {
			$('.message-list-overlay').trigger('click');
			$(this).parent().removeClass('active');
		});
	},

	getMessageById: function (e) {
        var a = $("#messages").find(".messages"),
          s = $(".message-send-container"),
          t = $(".mobile-back");
        s.addClass("loading").prepend('<div class="loading-bar indeterminate"></div>')
        socket.emit('getAllMessages',{id:e});
        $('.correspondence-user-col').hide();
        $('.message-send-col').show();
        $('.message-send-col').find('.call').attr('data-id', e);

        $.ajax({
            type: 'POST',
            url: '../../../users/default/getuserinfo',
            data: {user_id:e},
            dataType:'json',
            success: function(response){
                console.log(response);
                var avatar = '../../../images/users_images/'+response['avatar'];
                if(!response['avatar']){
                    avatar = '../../../../images/default_avatar.jpg';
                }
                $('.message-send-col').find('.user-avatar').attr('src',avatar);
                $('.message-send-col').find('.profileSrc a').attr('href','../../../profile/'+response['id']);
                $('.message-send-col').find('.user-username').text(response['name']+' '+response['surname']);
                $('.message-send-col').find('.messageMapButton').data('user_id', response['id']);
                $('#massegeMap').hide();
                $('.message-send-container').show();
            }
        });
        $("body").hasClass("layout-device") && t.addClass("active")
      },

	resetSendMessage: function () {
		// Define neccessary elements
		var $sendMessageInput = $('#send-message-input');

		$sendMessageInput.val('').trigger('input');
		$sendMessageInput.trigger('change');
	},

	listenSendButtonOnMessages: function () {
    var e = $("#send-message-input"),
      a = $("#send-message-button"),
      s = $("#messages").find(".messages");
      s.height("calc(100% - " + ($('.send-message').height() + 20) + "px")
    e.on("change keyup paste", function () {
      s.height("calc(100% - " + (e.height() + 47) + "px"), $("body").hasClass("layout-device") && s.height("calc(100% - " + (e.height() + 30) + "px"), a.height(e.height() + 5)
    }),

		 a.on("click", function () {
		 var a = e.val().trim();
  		 if(a.length){
  			 var id = $('.message-list>li.selected').find('.msg-id').data('message-id')
  			 socket.emit('message', {'id': id, 'message': a});
         Layout.resetSendMessage()
  		 }
    })
  },

	parallaxHeader: function () {
		var $pageHeader = $('.page-header');

		if( $pageHeader.hasClass('parallax') ) {
			$( window ).scroll(function() {
				var documentScrollTop = $(document).scrollTop(),
						headerHeight = $pageHeader.height(),
						$parallaxBg = $('.parallax-bg');

				$parallaxBg.css('top', (documentScrollTop*0.5));
				$parallaxBg.css('opacity', (1 - documentScrollTop/headerHeight*1));

			});
		}
	},

	init: function () {

		// Layout settings
		/*
		this.handleThemeColor();
		this.handleRtlLayout();
		*/

		// Layers
		this.initLayer();
		Pleasure.callOnResize.push( this.layerResponsive ); // Call layerResponsive function when resizing

		this.listenForMenuLayer();
		this.listenForSearchLayer();
		this.listenForVideoLayer();
		this.listenForUserLayer();
		this.listenKeyboardEvents();

		// Menu Links
		this.listenMenu();

		// Handle Sidebar
		this.handleSidebar();
		Pleasure.callOnResize.push( this.handleSidebar );

		// Listen Message Events
		this.listenMessageEvents();
		this.listenSendButtonOnMessages();

		// Parallax
		this.parallaxHeader();

	}

};
