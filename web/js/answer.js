$(document).ready(function(){
    $(document).on('click','.showFullAnswer',function(){
        //alert($(this).data('answer_id'));
        $('#showFullAnswer').modal('show');
        $('#showFullAnswer').find('.money_for_answer').text($(this).data('money'));
        $('.modalAnswerId').val($(this).data('answer_id'));
    });
    
    $(document).on('click','.PayWidthCredits',function(){
        var answer_id = $('.modalAnswerId').val();
        $.ajax({
            url:'../../../getcreditscount',
            method:'post',
            data:{},
            dataType:'json'
        }).done(function(response){
            swal({
                title: "Do you want to pay answer?",
                text: "<div style='color:white;'>Your have - "+response.user_credits_count+" credits, cost of answers - "+$('.money_for_answer').text()+"</div>",
                type: "warning",
                html:true,
                showCancelButton: true,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Yes, pay!",
                closeOnConfirm: true
            },
            function(){
                $.ajax({
                    url:'../../../getanswertext',
                    method:'post',
                    data:{answer_id:answer_id},
                    dataType:'json'
                }).done(function(response){
                    if(response.status == 'pay'){
                        $('#showFullAnswer').modal('hide');
                        swal({
                            title: "Payment!",
                            text: "",
                            type: "success",
                            html:true,
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                        function(){
                            $('.answer'+answer_id).find('.short-answer').text("Answer - "+response.answer);
                            $('.answer'+answer_id).find('.showFullAnswer').remove();
                            $('#showAnswerText').modal('show');
                            $('.AnswerText').text(response.answer);
                            $('#showAnswerText').find('.GoodQuestionRequest').data('answer_id',answer_id);
                            $('#showAnswerText').find('.BadQuestionRequest').data('answer_id',answer_id);
                        });
                    }
                    
                });
            });
        });
    });
        
    $(document).on('click', '.BadQuestionRequest', function(){
        var answer_id = $(this).data('answer_id');
        alert(answer_id);
//        $.ajax({
//            url:'../../../getanswertext',
//            method:'post',
//            data:{answer_id:answer_id},
//            dataType:'json'
//        }).done(function(response){
//            
//        });
        
    });
    
    $(document).on('click', '.GoodQuestionRequest', function(){
        var answer_id = $(this).data('answer_id');
        alert(answer_id);
        $('[name="Rating[answer_id]"]').val(answer_id);
        $('#showAnswerText').modal('hide');
        $('#show_rating_modal').modal('show');
    });
    
});
