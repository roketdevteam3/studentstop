/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    
    $(document).on('click', '.ViewEventModal',function(){
        var event_id = $(this).data('event_id');
            $.ajax({
                type: 'POST',
                url: '../../../university/default/geteventsdata',
                data: {event_id:event_id},
                dataType:'json',
                success: function(response){
                    $('#modalShowEvent').modal('show');
                    console.log(response);
                        $('#modalShowEvent').find('.eventTitle').html(response['title']);
                        $('#modalShowEvent').find('.eventShortContent').html(response['short_desc']);
                        $('#modalShowEvent').find('.eventContent').html(response['content']);
                        $('#modalShowEvent').find('.eventDate').html(response['date_start']+' - '+response['date_end']);
                }
            })
    })
    
        function dateChange(date){
            var date = new Date(date);
            var year = date.getFullYear()
            var month = date.getMonth()+1;
            if(month < 10){
                month = '0'+month;
            }
            var day = date.getDate()
            var dayClick = year+'-'+month+'-'+day;

            $.ajax({
                type: 'POST',
                url: '../../../university/default/geteventsajax',
                data: {date:dayClick},
                dataType:'json',
                success: function(response){
                    var month = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
                    var university_slug = $('.university_slug').val();
                    $('.eventBlock').html('')
                    if(response.length == 0){
                        $('.eventBlock').append('<span class="nf-event">sorry but events not found</span>')
                    }
                    //var date_start = 
                    for(var key in response){
//                        if(response[key].type = 'news'){
//                            var date_start = new Date(response[key].date_create);
//                        }else{
//                        }
                        var date_start = new Date(response[key].date_start);
                        var date_end = new Date(response[key].date_end);
                        var date_create = new Date(response[key].date_create);

                        $('.eventBlock').append(
                            '<div class="event-wrap">'+
                                '<div class="e-img">'+
                                   '<img class="img-responsive" src="'+response[key].image+'">'+
                                   '<div class="e-calendar">'+
                                       '<i class="fa fa-calendar"> </i> <span>'+month[date_start.getMonth()]+' '+date_start.getDate()+'-'+month[date_end.getMonth()]+' '+date_end.getDate()+'</span>'+
                                   '</div>'+
                                '</div>'+
                                '<div class="e-info">'+
                                   '<h4 class="e-date"><span>'+month[date_create.getMonth()]+'</span>'+date_create.getDate()+':'+date_create.getHours()+':'+date_create.getMinutes()+'</h4>'+
                                   '<h1 class="e-head">'+response[key].title+'</h1>'+
                                   '<p class="e-desc">'+response[key].short_desc+'</p>'+
                                    '<a class="btn btn-view ViewEventModal" data-event_id="'+response[key].id+'">View</a>'+
                                '</div>'+
                            '</div>'
                        );
                    }
                }
            })
        }
   	
        $('.calendarToday').click(function(){
            var dateT = new Date;
            dateChange(dateT);
            $('#paginator').datepaginator({
                onSelectedDateChanged: function(event, date){
                    dateChange(date);
                },
                itemWidth:50
            });
        });
        
        var options = {
            onSelectedDateChanged: function(event, date) {
                dateChange(date);
            },
            itemWidth:50
        }
        $('#paginator').datepaginator(options);
          
         
});