/**
 * Created by MackRais
 */
$(document).ready(function(){
    
    if($('.ClassInfoContainer').length){
        getClassUsers();
    }
    $(document).on('click','.classStudentShow',function(e){
        e.preventDefault();
        $(this).parent().find('li').removeClass('active');
        $(this).addClass('active');
        getClassUsers();
    })
    $(document).on('click','.classInfoShow',function(e){
        e.preventDefault();
        $(this).parent().find('li').removeClass('active');
        $(this).addClass('active');
        getClassInfo();
    })
    
    function getClassInfo(){
        var class_id = $('[name=ClassInfoId]').val();
        $.ajax({
            url:'../../../../course/default/getclassinfo',
            method:'POST',
            data:{class_id:class_id},
            dataType:'json',
            beforeSend:function(){
            },
            success:function(response){
                console.log(response);
                $('.ClassInfoContainer').html('');
                $('.ClassInfoContainer').append('<div class="">'+
                        response.class_info['description']+
                        '</div>');
                
            }
        })
    }
    function getClassUsers(){
        var class_id = $('[name=ClassInfoId]').val();
        $.ajax({
            url:'../../../../course/default/getclassusers',
            method:'POST',
            data:{class_id:class_id},
            dataType:'json',
            beforeSend:function(){
            //    thisButton.prop('disabled', true);
            },
            success:function(response){
                $('.ClassInfoContainer').html('');
                for(var key in response){
                    var userInfo = response[key];
                    var myId = $('.nav-user.message-count').find('.user').data('id');
                    var avatar = '../../../images/default_avatar.jpg';
                    if(userInfo != ''){
                        avatar = '<img src="../../../images/users_images/'+userInfo['avatar']+'">';
                    }
                    $('.ClassInfoContainer').append('<div class="startChat startChatFriend" data-sender-id="'+myId+'" data-recipient-id="'+userInfo['id']+'">'+
                        '<table class="table">'+
                            '<tr>'+
                                '<td>'+
                                    '<div class="user-img">'+
                                        avatar+
                                    '</div>'+
                                    '<span class="user-name">'+userInfo['name']+' '+userInfo['surname']+'</span>'+
                                '</td>'+
                                '<td>'+
                                    '<p class="user-role">('+userInfo['user_role_name']+')</p>'+
                                '</td>'+
                            '</tr'+
                        '</table>'+
                    '</div>');
                }
            }
        });
    }
    
//    $(document).on('click','.followClass',function(){
//        var followStatus = $(this).data('status');
//        var id_object = $(this).data('id_object');
//        console.log($(this).data('id_object'));
//        var thisButton = $(this);
//        $.ajax({
//            url:'../../../../ajaxfollowclass',
//            method:'POST',
//            data:{id_object:id_object,followStatus:followStatus},
//            dataType:'json',
//            beforeSend:function(){
//                thisButton.prop('disabled', true);
//            },
//            success:function(response){
//                if(response.status == 'success'){
//                    thisButton.data('status',response.statusFoloow);
//                    thisButton.text(response.statusFoloow);
//                    thisButton.prop('disabled', false);
//                }
//            }
//        });
//    });
    
//        if($('.user_class_photo').length){
//            var myDropzone = new Dropzone($('.user_class_photo')[0], { // Make the whole body a dropzone
//                uploadMultiple:false,
//                maxFiles:1,
//            });
//
//            myDropzone.on("maxfilesexceeded", function(file) {
//                myDropzone.removeAllFiles();
//                myDropzone.addFile(file);
//            });
//
//            myDropzone.on("complete", function(response) {
//                if (response.status == 'success') {
//                    $('.user_class_photo').find('.dz-preview').hide();
//                    $('.user_class_photo').css('background-image',"url('../../../images/classes/"+response.xhr.response+"')");
//                    $('#classes-img_src').val(response.xhr.response);
//                }
//            });
//
//            myDropzone.on("removedfile", function(response) {
//                if(response.xhr != null){
////                   deleteFile(response.xhr.response);
//                }
//            });
//        }
        
//    $(document).on('click','.JoinClass',function(){
//        var followStatus = 'Follow';
//        var id_object = $(this).data('class_id');
//        var thisButton = $(this);
//        
//        $.ajax({
//            url:'../../../../ajaxfollowclass',
//            method:'POST',
//            data:{id_object:id_object,followStatus:followStatus},
//            dataType:'json',
//            success:function(response){
//                if(response.status == 'success'){
//                    thisButton.text(response.statusFoloow);
//                }
//            }
//        });
//    });
    
//    $(document).on('click','.UnfollowClass',function(){
//        var followStatus = 'Unfollow';
//        var id_object = $(this).data('class_id');
//        var thisButton = $(this);
////        console.log()
//        $.ajax({
//            url:'../../../../ajaxfollowclass',
//            method:'POST',
//            data:{id_object:id_object,followStatus:followStatus},
//            dataType:'json',
//            success:function(response){
//                if(response.status == 'success'){
//                    thisButton.text(response.statusFoloow);
//                }
//            }
//        });
//    });
    
    if($('.user_class_new_file').length){
        var previewNode = document.querySelector(".previewTemplateFiles");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzoneA = new Dropzone($('.user_class_new_file')[0], {
            maxFiles:1,
            uploadMultiple:false,
            url: "../../course/classes/saveclassfile",
            previewTemplate:previewTemplate,
            clickable: ".newFiles",
            previewsContainer: ".user_class_new_file",
        });

        myDropzoneA.on("maxfilesexceeded", function(file) {
                myDropzoneA.removeAllFiles();
                myDropzoneA.addFile(file);

        });

        myDropzoneA.on("complete", function(response) {
            if (response.status == 'success') {
                $('#files-file_src').val(response.xhr.response);
//                    $('.eventButton').show();
            }
        });

        myDropzoneA.on("removedfile", function(response) {
            if(response.xhr != null){
               deleteFile(response.xhr.response);
            }
        });
    }
    
    
    if($('.user_class_new_file_product').length){
        var previewNode = document.querySelector(".previewTemplateFiles");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzoneA = new Dropzone($('.user_class_new_file_product')[0], {
            maxFiles:1,
            uploadMultiple:false,
            url: "../../course/classes/saveclassfile",
            previewTemplate:previewTemplate,
            clickable: ".newFilesProduct",
            previewsContainer: ".user_class_new_file_product",
        });

        myDropzoneA.on("maxfilesexceeded", function(file) {
                myDropzoneA.removeAllFiles();
                myDropzoneA.addFile(file);

        });

        myDropzoneA.on("complete", function(response) {
            if (response.status == 'success') {
                $('.filesIdProduct').val(response.xhr.response);
//                    $('.eventButton').show();
            }
        });

        myDropzoneA.on("removedfile", function(response) {
            if(response.xhr != null){
               deleteFile(response.xhr.response);
            }
        });
    }
    
    
});

