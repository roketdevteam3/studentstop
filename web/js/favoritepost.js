$(document).ready(function(){
    
    $(document).on('click','.addToFavorite',function(){
        var post_id = $(this).data('post_id');
        var thisElement = $(this);
        $.ajax({
            type: 'POST',
            url: '../../../addposttofavorite',
            data: {post_id:post_id},
            dataType:'json',
            success: function(response){
                if(response.status == 'success'){
                    thisElement.removeClass('addToFavorite');
                    thisElement.addClass('deleteWithFavorite');
                    thisElement.find('.text').text('Delete with favorite');
                    swal("Add to favorite", "", "success");
                }
            } 
        });
        
    });
    
    $(document).on('click','.deleteWithFavorite',function(){
        var post_id = $(this).data('post_id');
        var thisElement = $(this);
        $.ajax({
            type: 'POST',
            url: '../../../deletepostwithfavorite',
            data: {post_id:post_id},
            dataType:'json',
            success: function(response){
                if(response.status == 'success'){
                    thisElement.removeClass('deleteWithFavorite');
                    thisElement.addClass('addToFavorite');
                    thisElement.find('.text').text('Add to favorite');
                    swal("Deleted with favorite", "", "success");
                }
            } 
        });
    });

    $(document).on('click','#view',function(){
        var post_id = $(this).data('post_id');
        $('#view_post_'+post_id).show();
        $.get('/market/addviews?id='+post_id, function( data ) {});
    });
    
});        