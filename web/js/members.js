function initMap() {
    var myLatLng = {lat: 36.2456967, lng: -113.7266248};
    
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 1,
      center: myLatLng
    });

//    var marker = new google.maps.Marker({
//      position: myLatLng,
//      map: map,
//      title: 'Hello World!'
//    });
}


function addressLocation(address){
    var geocoder = new google.maps.Geocoder();
    //var address = $(this).val();
    geocoder.geocode({'address': address}, function(results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            var Local = results[0].geometry.location;
            var latLng = {lat:parseFloat(Local.lat()),lng:parseFloat(Local.lng())};
            return latLng;
        }else{
            return 'error';
        }
    });
}



$(document).ready(function(){
   // initMap();
   function findWithAttr(array, attr, value) {
        for(var i = 0; i < array.length; i += 1) {
            if(array[i][attr] == value) {
                return i;
            }
        }
    }
//    
    var marker = '';
    var markersArray = [];
    var markersUniversityArray = [];
    var element = document.getElementById("map");
    var map = new google.maps.Map(element, {
        center: new google.maps.LatLng(44.194689, -102.4903543),
        zoom: 2,
    });
//    
//    
    var socket = io.connect('https://thestudentstop.com:3000');
//    var socket = io.connect('https://localhost:3000');
    
    socket.emit('login', {
      'id': $('.sender_id').val()
    });
        
    getUserData();
    getUniversityData();
    
    socket.on("responseusersmember", function(data) {
        console.log('responseusersmember');
        //buildNotification(data)
        //console.log(data);
        for (var i = 0; i < markersArray.length; i++) {
            markersArray[i].setMap(null);
            //console.log(markersArray[i]);
        }
        markersArray = [];
        
        $('.usersClass').html('');
        $('.userButtonsInfo').html('');
        for(var key in data){
            var users = data;
            var onlineStatus = '<span class="off">offline</span>';
            var iconUrl = '../../../images/marker_offline.png';
            if(users[key].onlineStatus == 1){
                onlineStatus = '<span class="on">online</span>';
                iconUrl = '../../../images/marker_online.png';
            }
            var avatar = 'default_avatar.jpg'
            if((users[key].avatar != '') && (users[key].avatar != null)){
                avatar = 'users_images/'+users[key].avatar;
            }
            var address = '';
            if((users[key].address != '') && (users[key].address != null)){
                address = users[key].address
            }
            
            var birthday = '';
            if((users[key].birthday != '') && (users[key].birthday != null)){
                var dob = new Date(users[key].birthday);
                var today = new Date();
                var birthday = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
                //birthday = date.getDate() + ' - ' + date.getMonth() + ' - ' + date.getFullYear();
            }
            
            
            $('.usersClass').append('<div class="row">'+
                                        '<div class="col-sm-12">'+
                                            '<div class="userClickClass user-block user-map userClass'+users[key].id+'" data-user_id="'+users[key].id+'">'+
                                                '<div class="user-img">'+
                                                    '<img class="img-responsive" src="../../../images/'+avatar+'" alt="">'+
                                                '</div>'+
                                                '<div class="center-block">'+
                                                    '<div class="user-data">'+
                                                        '<input type="hidden" class="userlatitude" value="'+users[key].position_lat+'">'+
                                                        '<input type="hidden" class="userlongitude" value="'+users[key].position_lng+'">'+
                                                        '<p  class="userNameSurname">'+users[key].name+' '+users[key].surname+'</p>'+
                                                        '<p class="post">('+users[key].userRoleName+')</p>'+

                                                        '<p class="userStatus">'+onlineStatus+'</p>'+
                                                    '</div>'+
                                                    '<table class="table">'+
                                                        '<tr>'+
                                                            '<td class="table-label">Age:</td>'+
                                                            '<td class="table-value">'+birthday+'</td>'+
                                                            '<td class="table-label">University:</td>'+
                                                            '<td class="table-value">'+users[key].universityName+'</td>'+
                                                        '</tr>'+
                                                        '<tr>'+
                                                            '<td class="table-label">Classes:</td>'+
                                                            '<td class="table-value" class="classCount"></td>'+
                                                            '<td class="table-label">Friends:</td>'+
                                                            '<td class="table-value"><span class="friendCount"></span> members</td>'+
                                                        '</tr>'+
                                                    '</table>'+
                                                    '<div class="address">'+
                                                        'Address: '+
                                                        '<span>'+address+'</span>'+
                                                    '</div>'+
                                                '</div>'+
                                                '<div class="user-btn-wrap">'+
                                                    '<div class="user-data userRating mystar"></div>'+
                                                '</div>'+
                                            '<div class="my_bottom" style="position:relative; top: 30px">'+
                                                '<a  href="../../../profile/'+users[key].id+'" class="btn profile-btn">Profile</a>'+
                                                '<div class="followBtnBlock my_bottom"></div>'+
                                                '</div>'+
                                            '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>');
                
                getAnoutherData(users[key].id);
                    //addMarketUserInMap(users,key);
        }
    });
    
    function getAnoutherData(user_id){
//        console.log(user_id);
         $.ajax({
            type: 'POST',
            dataType: "json",
            url: '../../../users/default/getanoutheruserinformation',
            data: {user_id:user_id},
            success: function(response){
  //              console.log(response);
                $('.userClass'+user_id).find('.followBtnBlock').html(response.follow_status)
                $('.userClass'+user_id).find('.friendCount').text(response.friend_count)
                $('.userClass'+user_id).find('.classCount').text('enrolled in '+response.class_count)
                $('.userClass'+user_id).find('.userRating').append('<input id="userInputRating'+user_id+'" name="clRating" value="'+response.star_count+'" class="rating-loading">')

                $('#userInputRating'+user_id).rating({displayOnly: true, step: 0.1, size:'xs'});
            }
        });
    }
    
    
    function friendRequest(friend_id,my_id,action,thisElement){
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '../../../friendrequest',
                data: {friend_id:friend_id,my_id:my_id,action:action},
                success: function(response){
                    if(response.status == 'success'){
                        if(action == 'follow'){
                            thisElement.removeClass('followFriend')
                            thisElement.addClass('unfollowFriend')
                            thisElement.text('Unfollow')
                        }else if(action == 'unfollow'){
                            thisElement.removeClass('unfollowFriend')
                            thisElement.addClass('followFriend')
                            thisElement.text('Follow')
                        }else if(action == 'confirm'){
                            thisElement.removeClass('followFriend')
                            thisElement.addClass('unfollowFriend')
                            thisElement.text('Unfollow')
                        }else if(action == 'delete'){
                            $('.deleteFriendBlock'+friend_id).remove();
                        }
                    }
                }
            });
          }
        //friendRequest();
        
        $(document).on('click','.followFriend',function(){
            var thisElement = $(this);
            var friend_id = $(this).data('friend_id');
            var my_id = $(this).data('my_id');
            var action = 'follow';
            friendRequest(friend_id, my_id,action,thisElement);
        });
        
        $(document).on('click','.confirmFriend',function(){
            var thisElement = $(this);
            var friend_id = $(this).data('friend_id');
            var my_id = $(this).data('my_id');
            var action = 'confirm';
            friendRequest(friend_id, my_id,action, thisElement);
        });
        
        $(document).on('click','.unfollowFriend',function(){
            var thisElement = $(this);
            var friend_id = $(this).data('friend_id');
            var my_id = $(this).data('my_id');
            var action = 'unfollow';
            friendRequest(friend_id, my_id, action, thisElement);
        });
        
        $(document).on('click','.deleteFriend',function(){
            var thisElement = $(this);
            var friend_id = $(this).data('friend_id');
            var my_id = $(this).data('my_id');
            var action = 'delete';
            friendRequest(friend_id, my_id, action, thisElement);
        });

    
    
    function addMarketUserInMap(users,key){
            var onlineStatus = '<span class="off">offline</span>';
            var iconUrl = '../../../images/marker_offline.png';
            if(users[key].onlineStatus == 1){
                onlineStatus = '<span class="on">online</span>';
                iconUrl = '../../../images/marker_online.png';
            }
                var icon = {
                    url:iconUrl,
                    scaledSize: new google.maps.Size(32,34)
                };
                
                var infowindow = new google.maps.InfoWindow({
                        content: '<div>'+
                                    '<h2>'+users[key].name+' '+users[key].surname+'</h2>'+
                                '</div>'
                    });
                
                marker = new google.maps.Marker({
                    position: {lat: parseFloat(users[key].position_lat), lng: parseFloat(users[key].position_lng)},
                    //label: 'Title',
                    map: map,
                    icon:icon
                });
                marker.addListener('click', function() {
                    infowindow.open(map, marker);
                });
                
                marker['id'] = users[key].id;
                marker['class'] = 'markerClass'+users[key];
                markersArray.push(marker);
    }
    
    socket.on('onlineChange', function(data) {
        //console.log('online change');
        if(data.status){
            
            var id = data.user.toString();
            var index = findWithAttr(markersArray,'id',id);
            var icon = {
                url:'../../../images/marker_online.png',
                scaledSize: new google.maps.Size(32,34)
            };
            markersArray[index].setMap(null)
            markersArray[index].icon = icon;
            markersArray[index].setMap(map)
            $('.userClass'+id).find('.userStatus').text('online');
            if($('.userInfoOnlineStatus'+id).length != 0){
               $('.userInfoOnlineStatus'+id).text('online'); 
            }
        }else{
            
            var id = data.user.toString();
            
            var index = findWithAttr(markersArray,'id',id);
            var icon = {
                url:'../../../images/marker_offline.png',
                scaledSize: new google.maps.Size(32,34)
            };
            markersArray[index].setMap(null)
            markersArray[index].icon = icon;
            markersArray[index].setMap(map)
            $('.userClass'+id).find('.userStatus').text('');
            if($('.userInfoOnlineStatus'+id).length != 0){
               $('.userInfoOnlineStatus'+id).text(''); 
            }
        }
        
    });
    
         $(document).on('click','.userClickClass',function(){
//            var userId = $(this).data('user_id');
//            var userStatus = $(this).find('.userStatus').text();
//            var userNameSurname = $(this).find('.userNameSurname').text();
//            $('.userButtonsInfo').html('');
//            $('.userButtonsInfo').append('<li role="presentation">'+userNameSurname+'</li>');
//            $('.userButtonsInfo').append('<li role="presentation" ><i style="color:#d1d1d1;" class="userInfoOnlineStatus'+userId+'">'+userStatus+'</i></li>');
//            $('.userButtonsInfo').append('<li class="pull-right" role="presentation"><a href="#">Follow/Unfollow</a></li>');
//            $('.userButtonsInfo').append('<li class="pull-right" role="presentation">'+
//                '<button class="btn btn-primary startChat btn-ripple" data-sender-id="'+$('.sender_id').val()+'" data-recipient-id="'+userId+'">Write <i class="fa fa-envelope"></i></button>'
//            +'</li>');
//            $('.userButtonsInfo').append('<li class="pull-right" role="presentation"><a href="../../../profile/'+userId+'">View Profile</li>');
        }) 
        
        function getSearchUsers(choiseValue,inputValue){
           // console.log('getsearch user');
            var sql = '';
            if(choiseValue == ''){
                choiseValue = $('.filterUsers').val();
            }
                switch (choiseValue) {
                    case '1':
                        sql = '';
                    break;

                    case '2':
                        sql = '';
                    break;

                    case '3':
                        sql = ' AND `mr_user_info`.`university_id` = '+$('.user_university_id').val();
                    break;

                    case '4':
                        sql = '';
                    break;
                }
            
            if(inputValue.length > 2){
                sql += " AND (`mr_user`.`name` LIKE '%"+inputValue+"%' ";
                sql += " OR `mr_user`.`surname` LIKE '%"+inputValue+"%')";
            }else if($('.getSearchInput').val().length > 2){
                sql += " AND (`mr_user`.`name` LIKE '%"+$('.getSearchInput').val()+"%' ";
                sql += " OR `mr_user`.`surname` LIKE '%"+$('.getSearchInput').val()+"%') ";
            }
            //console.log(sql);
            socket.emit('getusersmember', {
                sql:sql,
            });
        }
        
//        $(document).on('change','.filterUsers',function(){
//            getSearchUsers($(this).val(), '');
//        });
        
        function getUniversityData(){
            //alert('unievrsity_work');
            var search_key = $('.getSearchInput').val();
            var major_id = $('[name=major_ufilter]').val();
            var course_id = $('[name=course_ufilter]').val();
            var rating_id = $('[name=rating_ufilter]').val();
            
            $.ajax({
                type: 'POST',
                url: '../../../users/default/getuniversitiesmembers',
                data: {major_id:major_id, course_id:course_id, rating_id:rating_id, search_key:search_key},
                dataType:'json',
                success: function(response){
                    $('.universityClass').html('');
                    for(var id in response){
                        var universities = response[id]['university'];
                        map.setZoom(3);
                        for (var i = 0; i < markersUniversityArray.length; i++) {
                            markersUniversityArray[i].setMap(null);
                            //console.log(markersUniversityArray[i]);
                        }
                        
                        markersUniversityArray = [];
                        if(universities != 'null'){
                            addDefaultMarketUniversity(response[id]['state_lat'], response[id]['state_lng'],response[id]['state_friend_count'])
                            for(var university_id in universities){
                                var university = universities[university_id];
                                var users_count = university.users_count;
                                var avatar = '<img src="/uploads/university/default.jpg" style="width:100%;">';
                                if(university.university_avatar != ''){
                                    avatar = '<img src="'+university.university_avatar+'" style="width:100%;">';
                                }
                                $('.universityClass').append('<div class="row">'+
                                            '<div class="col-sm-12">'+
                                                '<div class="user-block user-map" >'+
                                                    '<div class="user-img">'+
                                                        avatar+
                                                    '</div>'+
                                                    '<div class="center-block">'+
                                                        '<div class="user-data">'+
                                                            '<p  class="userNameSurname">'+university.university_name+'</p>'+
                                                        '</div>'+
                                                        '<div class="user-job">'+
                                                            '<p class="work-place">'+university.location+'</p>'+
                                                            '<p class="post">'+university.history+'</p>'+
                                                        '</div>'+
                                                    '</div>'+
                                                    '<div class="user-btn-wrap">'+
                                                        // '<div class="col-xs-10 no-padding">'+
                                                           // '<a class="univeristyClick univeristyClickClass'+university.university_id+' show-in-map" data-university_address="">Show in map</a>'+
                                                            '<span class="univerAddress" style="display:none;">'+university.location+'</span>'+
                                                        // '</div>'+
                                                        '<div class="col-xs-2 no-padding">'+
                                                        // '</div>'+
                                                    // '</div>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>');
                                        addMarketUniversityInMap(university);

                            }
                        }else{
                            addDefaultMarketUniversity(response[id]['state_lat'], response[id]['state_lng'],response[id]['state_friend_count'])
                        }
                    }
                }
            })
        }
        
        function addDefaultMarketUniversity(s_lat,s_lng, count){
                    var latLng = {lat:parseFloat(s_lat),lng:parseFloat(s_lng)};
                    var icon = {
                        url:'../../../images/default_university_count.png',
                        scaledSize: new google.maps.Size(32,34)
                    };
                    
                    var marker = '';
                    marker = new google.maps.Marker({
                        position: latLng,
                        map: map,
                        label:count.toString(),
                        zIndex:999+parseInt(count),
                        icon:icon,                        
                    });
                    google.maps.event.addListener(map, 'zoom_changed', function() {
                        var zoom = map.getZoom();
                        marker.setVisible(zoom <= 6);
                    });
                    
                    
        }
        
        function addMarketUniversityInMap(university){
            var address = university.location;
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({'address': university.location}, function(results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    var Local = results[0].geometry.location;
                    var latLng = {lat:parseFloat(Local.lat()),lng:parseFloat(Local.lng())};
                    $('.univeristyClickClass'+university.university_id).data('university_address',JSON.stringify(latLng));
                    //$('.univeristyClickClass'+university.university_id).parent().find('.univerAddress').text(JSON.stringify(latLng));

                    var icon = {
                        url:'../../../images/university_map_icon.png',
                        scaledSize: new google.maps.Size(32,34)
                    };
                    
                    
                    var infowindow = new google.maps.InfoWindow({
                        content: '<div>'+
                                    '<h3>'+university.university_name+'</h3>'+
                                    '<p>Website - '+university.website+'</p>'+
                                    '<p>Telefone - '+university.telefone+'</p>'+
                                    '<p>Email - '+university.email+'</p>'+
                                    '<button class="btn btn-primary btn-xs showUniversityMembers" data-university_id="'+university.university_id+'">Show friends</button>'+
                                '</div>'
                    });
                    
                    var marker = '';
                    marker = new google.maps.Marker({
                        position: latLng,
                        map: map,
                        icon:icon,
                        zIndex:999+parseInt(university.users_count),
//                        label: {
//                            text: university.users_count,
//                            color: 'white',
//                        }
                    });
                    
                    
                    var zoom = map.getZoom();
                    marker.setVisible(zoom >= 7);
                    
                    var icon2 = {
                        url:'../../../images/default_university_count2.png',
                        scaledSize: new google.maps.Size(18,18)
                    };
                    var marker2 = '';
                    marker2 = new google.maps.Marker({
                        position: latLng,
                        map: map,
                        zIndex:999+parseInt(university.users_count)+1,
                        label:{
                            text:university.users_count,
                            color:'white',
                        },
                        icon:icon2,
                    });
                    marker2.addListener('click', function() {
                        infowindow.open(map, marker2);
                    });
                    var zoom = map.getZoom();
                    marker2.setVisible(zoom >= 7);
                    
                    
                    google.maps.event.addListener(map, 'zoom_changed', function() {
                        var zoom = map.getZoom();
                        marker2.setVisible(zoom >= 7);
                        marker.setVisible(zoom >= 7);
                    });
                    
                    marker['id'] = university.university_id;
                    marker['class'] = 'markerClass'+university.university_id;
                    markersUniversityArray.push(marker);
                }
            });
        }
        $(document).on('change','[name=major_ufilter], [name=course_ufilter], [name=rating_ufilter]', function(){
            getUniversityData();
        });
        
        $(document).on('click', '.showUniversityMembers', function(){
            var university_id = $(this).data('university_id');
            $.ajax({
                type: 'POST',
                url: '../../../users/default/getfrienduniversity',
                data: {university_id:university_id},
                dataType:'json',
                success: function(response){
                    $('.usersClass').html('')
                    for(var user_id in response){
                        var users = response[user_id]
                        var avatar = 'default_avatar.jpg'
                        if((users['user_info']['avatar'] != '') && (users['user_info']['avatar'] != null)){
                            avatar = 'users_images/'+users['user_info']['avatar'];
                        }
                        
                        var address = '';
                        if((users['user_info']['address'] != '') && (users['user_info']['address'] != null)){
                            address = users['user_info']['address']
                        }

                        var birthday = '';
                        if((users['user_info']['birthday'] != '') && (users['user_info']['birthday'] != null)){
                            var dob = new Date(users['user_info']['birthday']);
                            var today = new Date();
                            var birthday = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
                        }
                        
                        $('.usersClass').append('<div class="row">'+
                                        '<div class="col-sm-12">'+
                                            '<div class="userClickClass user-block user-map userClass'+users['user']['id']+'" data-user_id="'+users['user']['id']+'">'+
                                                '<div class="user-img">'+
                                                    '<img class="img-responsive" src="../../../images/'+avatar+'" alt="">'+
                                                '</div>'+
                                                '<div class="center-block">'+
                                                    '<div class="user-data">'+
                                                        '<input type="hidden" class="userlatitude" value="'+users['user']['position_lat']+'">'+
                                                        '<input type="hidden" class="userlongitude" value="'+users['user']['position_lng']+'">'+
                                                        '<p  class="userNameSurname">'+users['user']['name']+' '+users['user']['surname']+'</p>'+
                                                        '<p class="post">('+users['user_role']+')</p>'+

                                                    '</div>'+
                                                    '<table class="table">'+
                                                        '<tr>'+
                                                            '<td class="table-label">Age:</td>'+
                                                            '<td class="table-value">'+birthday+'</td>'+
                                                            '<td class="table-label">University:</td>'+
                                                            '<td class="table-value">'+users['university_name']+'</td>'+
                                                        '</tr>'+
                                                        '<tr>'+
                                                            '<td class="table-label">Classes:</td>'+
                                                            '<td class="table-value" class="classCount"></td>'+
                                                            '<td class="table-label">Friends:</td>'+
                                                            '<td class="table-value"><span class="friendCount"></span> members</td>'+
                                                        '</tr>'+
                                                    '</table>'+
                                                    '<div class="address">'+
                                                        'Address: '+
                                                        '<span>'+address+'</span>'+
                                                    '</div>'+
                                                '</div>'+
                                                '<div class="user-btn-wrap">'+
                                                    '<div class="user-data userRating mystar"></div>'+
                                                '</div>'+
                                            '<div class="my_bottom" style="position:relative; top: 30px">'+
                                                '<a  href="../../../profile/'+users['user']['id']+'" class="btn profile-btn">Profile</a>'+
                                                '<div class="followBtnBlock my_bottom"></div>'+
                                                '</div>'+
                                            '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>');
                            
                            getAnoutherData(users['user']['id']);
                            
                    }
                }
            })
            
            
//            var university_id = $(this).data('university_id');
//            $('#university').removeClass('active')
//            $('#university').removeClass('in')
//            $('#members').addClass('active')
//            $('#members').addClass('in')
//            //console.log(university_id);
//            socket.emit('getusersmember', {
//                search_by_key:'',
//                university:university_id,
//                course:'',
//                user_status:'',
//                age_from:'',
//                age_to:'',
//                sex:'',
//            });
        });
        
        $(document).on('click', '.univeristyClick', function(){
            var address = $(this).parent().find('.univerAddress').text();
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({'address': address}, function(results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    var Local = results[0].geometry.location;
                    var latLng = {lat:parseFloat(Local.lat()),lng:parseFloat(Local.lng())};
                    map.setCenter(latLng);
                    map.setZoom(12);
                    $('html, body').animate({ scrollTop: 0 }, 'slow', function () { });
                }
            });
        })
        
        $(document).on('click', '.userClickShowMap', function(){
            var position_lat = $(this).data('position_lat');
            var position_lng = $(this).data('position_lng');
            if((position_lat != '') && (position_lng != '')){
                var latLng = {lat:parseFloat(position_lat), lng:parseFloat(position_lng)};
                map.setCenter(latLng);
                map.setZoom(14);
            }
        })
        
        function getUserData(){
            console.log('------------')
            console.log('------------')
            console.log($('[name=course]').val())
            console.log('------------')
            console.log('------------')
            $('.membersFilter').show();
            $('.universityFilter').hide();
            $('.getSearchInput').data('tabs_type', 'members');
            //$('.getSearchInput').val('');
            var search_by_key = $('.getSearchInput').val();
            var country_city = $('[name=country_city]').val();
            var university = $('[name=university]').val();
            var course = $('[name=course]').val();
            var user_status = $('[name=user_status]').val();
            var sex = $('[name=sex]').val();
            var age_from = $('[name=age_from]').val();
            var age_to = $('[name=age_to]').val();
            socket.emit('getusersmember', {
                search_by_key:search_by_key,
                university:university,
                course:course,
                user_status:user_status,
                age_from:age_from,
                age_to:age_to,
                sex:sex,
            });
        }
        
        $(document).on('change','[name=age_from], [name=age_to], [name=country_city], [name=university], [name=course], [name=user_status], [name=sex]',function(){
            //getSearchUsers('', $(this).val());
            getUserData();
        });
        
        
        $(document).on('click','.usersClass',function(){
            var id = $(this).parent().data('user_id').toString();
            var latitude = $('.userClass'+id).find('.userlatitude').val();
            var longitude = $('.userClass'+id).find('.userlongitude').val();
            map.setCenter(new google.maps.LatLng(latitude, longitude));
            map.setZoom(15);
            
            $('.usersClass li>a').removeClass('usersMembersColor');
            $(this).addClass('usersMembersColor');
        });
        
        $(document).on('change','[name=university]',function(){
            if($(this).val() != ''){
                var university_id = $(this).val();
                $.ajax({
                    type: 'POST',
                    url: '../../../users/default/getcourse',
                    data: {university_id:university_id},
                    dataType:'json',
                    success: function(response){
                        $('[name=course]').html('');
                        $('[name=course]').append('<option value="">Course</option>');
                        for(var key in response){
                            $('[name=course]').append('<option value="'+key+'">'+response[key]+'</option>');
                        }
                    }
                });
            }

        });
        
        $('.membersButton').on('shown.bs.tab', function (e) {
            $('.getSearchInput').data('tabs_type', 'members');
            $('.getSearchInput').val('');
            getUserData();
            $('.membersFilter').show();
            $('.universityFilter').hide();
        });
        
        $('.universityButton').on('shown.bs.tab', function (e) {
            $('.getSearchInput').data('tabs_type', 'university');
            $('.getSearchInput').val('');
            getUniversityData();
            $('.membersFilter').hide();
            $('.universityFilter').show();
        });
        
        
        $(document).on('click','.topSearchClick',function(){
            var tabs_type = $('.getSearchInput').data('tabs_type');
            //alert(tabs_type)
            if(tabs_type == 'members'){
                getUserData();
            }else{
                getUniversityData();
            }
        })
        
        $(document).on('click', '.searchUniversityButton', function(){
            getUniversityData();
        })
        
});