function compare(a, b) {
    if (a.id < b.id)
        return -1;
    else if (a.id > b.id)
        return 1;
    else
        return 0;
}

function buildNotification(data) {
    notif = notif.concat(data.notifications)
    notif.sort(compare)
    var newarr = [];
    var unique = {};

    $.each(notif, function(key, item) {
        if (!unique[item.id]) {
            newarr.push(item);
            unique[item.id] = item;
        }
    });
    notif = newarr
    $("#notifications").find('ul').html('')
    $.each(notif, function(key, value) {
        // console.log($('li[data-id="'+ value.id +'"]'));
        // if(!$('li[data-id="'+ value.id +'"]').length){
        var loginTemplate = $("li.templateN").clone()
        loginTemplate.removeClass('templateN')
        loginTemplate.attr('data-id', value.id)
        loginTemplate.find('.title').html(value.name + ' ' + value.surname)
        loginTemplate.find('.top').html(value.created_add)
        loginTemplate.find('.textD').html(value.title)
        loginTemplate.find('.deleteN').attr('data-id', value.id)
        loginTemplate.find('.applyN').attr('data-id', value.id)
        if((value.avatar != null) && (value.avatar != '')){
            loginTemplate.find('img').attr("src", '../../../images/users_images/' + value.avatar);
        }else{
            loginTemplate.find('img').attr("src", '../../../../images/default_avatar.jpg');
        }
        switch (value.type) {
            case "rent_request":

                loginTemplate.find('.applyN').attr('data-type', value.type)
                loginTemplate.find('.type').addClass('fa-home')
                break;
            case "contact_request":
                loginTemplate.find('.applyN').attr('data-type', value.type)
                loginTemplate.find('.type').addClass('fa-user-plus')

                break;
            default:

        }
        // fa-user-plus
        $("#notifications").find('ul').prepend(loginTemplate)
        // }
    })
    var i = $("#notifications").find('ul > li').length
    $('.ntf>.badge').html((i == 0) ? '' : i)
    mC = isNaN(parseInt($('.msg>.badge').html())) ? 0 : parseInt($('.msg>.badge').html());
    nC = isNaN(parseInt($('.ntf>.badge').html())) ? 0 : parseInt($('.ntf>.badge').html());
    var k = mC + nC
    $('.user>.badge').html((k == 0) ? '' : k)
}
var notif = []
var online = []
var id;
var socket = io.connect('https://thestudentstop.com:3000');
// var socket = io.connect('https://127.0.0.1:3000');
var Chat = {
    init: function() {
        if ($('.rent_request').length) {
            var rent_id = $('.rent_request').data('rent-id')
            var recipient_id = $('.rent_request').data('recipient-id')
            socket.emit('rentRequest', {
                rent_id: rent_id,
                recipient_id: recipient_id,
            })
            // $('.rent_request').find('.close').trigger('click')
        }

        socket.on("responseNotifications", function(data) {
            buildNotification(data)
            console.log(data);
        })

        socket.on('responseUnreadedCount', function(data) {
            console.log(['responseUnreadedCount', data])
            var k = 0;
            $('#conactList>li').each(function() {
                data_id = $(this).find('.msg-id').data('message-id')
                var ind = data.find(function(ele) {
                    if (ele.sender == data_id) {
                        return ele
                    }
                })
                if (data.indexOf(ind) > -1) {
                    $(this).find(".badge").html(data[data.indexOf(ind)].unreadedCount)
                    k += parseInt(data[data.indexOf(ind)].unreadedCount)
                } else {
                    $(this).find(".badge").html('')
                }
            })

            $('.msg>.badge').html((k == 0) ? '' : k)
            mC = isNaN(parseInt($('.msg>.badge').html())) ? 0 : parseInt($('.msg>.badge').html());
            nC = isNaN(parseInt($('.ntf>.badge').html())) ? 0 : parseInt($('.ntf>.badge').html());
            var t = mC + nC
            $('.user>.badge').html((t == 0) ? '' : t)
        });
        socket.on('connect', function() {
            console.log(id);
            if (id) {
                socket.emit('login', {
                    'id': id
                });
            }
        });
        socket.on('onlineChange', function(data) {
            if (data.status) {
                $('.ifonline[data-recipient-id = "' + data.user + '"]').parent('li').removeClass('hidden')
                $('#conactList>li').find('[data-message-id = "' + data.user + '"]').find(".top").html('online')
            } else {
                $('.ifonline[data-recipient-id = "' + data.user + '"]').parent('li').addClass('hidden')
                $('#conactList>li').find('[data-message-id = "' + data.user + '"]').find(".top").html('')
            }
        });
        socket.on('responseAllMessages', function(data) {               
            var a = $("#messages").find(".messages"),
                s = $(".message-send-container"),
                t = $(".mobile-back");
            a.html('')
            
            $.each(data, function(key, value) {
                if (value.sender == id) {
                    var side = "right";
                    var img_src = $('.myHeaderAvatar > img').attr('src')
                    if(!img_src){
                        img_src = '../../../../images/default_avatar.jpg';
                        console.log('default');
                    }
                } else {
                    var side = "left";
                    var img_src = $('.message-list>li.selected').find('img').attr('src')
                    
                    if(!img_src){
                        img_src = '../../../../images/default_avatar.jpg';
                    }
                }
                
                var t = '<div class="message animate ' + side + '"><div class="user-picture-wrap"><img src="' + img_src + '" class="img-responsive user-picture" alt=""></div><div class="message-text">' + value.message + '</div><span class="time"> ' + value.created_add + '</span></div>';
                a.prepend(t)
            })
            // a.html(data)
            a.scrollTop(a.prop("scrollHeight"))
            s.removeClass("loading").find(".loading-bar").remove()
        });



        socket.on('responseMessage', function(data) {
            
            var id = $('.message-list>li.selected').find('.msg-id').data('message-id')
            var img_src = $('.myHeaderAvatar > img').attr('src')
            if(!img_src){
                img_src = '../../../../images/default_avatar.jpg';
            }
            if (id == data.id) {
                var s = $("#messages").find(".messages");
                var t = '<div class="message animate right">\n\
                    <div class="user-picture-wrap">\n\
                        <img src="' + img_src + '" class="img-responsive user-picture" alt="">\n\
                    </div>\n\
                    <div class="message-text">' + data.message + '</div><span class="time">' + data.created_add + '</span></div>';
                s.append(t)
                s.scrollTop(s.prop("scrollHeight"))
            }
        });
        socket.on('reciveMessages', function(data) {
            var id = $('.message-list>li.selected').find('.msg-id').data('message-id')
            var img_src = $('.resipienrUserAvatar').attr('src')
            if(!img_src){
                img_src = '../../../../images/default_avatar.jpg';
            }

            if (id == data.sender_id) {
                var s = $("#messages").find(".messages");
                var t = '<div class="message animate right">\n\
                    <div class="user-picture-wrap">\n\
                        <img src="' + img_src + '" class="img-responsive user-picture" alt="">\n\
                    </div>\n\
                    <div class="message-text">' + data.message + '</div><span class="time">' + data.created_add + '</span></div>';
                s.append(t)
                s.scrollTop(s.prop("scrollHeight"))
            }
        });

        socket.on('onlineUsers', function(data) {
            $(".ifonline").each(function() {
                if (data.indexOf($(this).data('recipient-id').toString()) > -1) {
                    $(this).parent('li').removeClass('hidden')
                } else {
                    $(this).parent('li').addClass('hidden')
                }
                // if(data.indexOf($(this)))
            })
            $('#conactList>li').each(function() {
                if (data.indexOf($(this).find('.msg-id').data('message-id').toString()) > -1) {
                    $(this).find('.top').html('online')
                } else {
                    $(this).find('.top').html('')
                }
            })
        });
        socket.on('responseContactList', function(data) {
            console.log('responseContactList');

            var id = ($('#conactList').find('.selected').length) ? $('#conactList').find('.selected').find('.msg-id').attr('data-message-id') : "0";
            //
            $('#conactList').html('')
            $.each(data.list, function(index, value) {
                console.log(value);
                var userTemplate = $("li.template").clone()
                userTemplate.removeClass('template')
                userTemplate.find('.msg-id').attr('data-message-id', value.id)
                userTemplate.find('span.title').text(value.name + ' ' + value.surname);
                if((value.avatar != null) && (value.avatar != '')){
                    userTemplate.find('.face-radius').attr('src','/images/users_images/'+value.avatar);
                }else{
                    userTemplate.find('.face-radius').attr('src','/images/default_avatar.jpg');                    
                }
                if (data.select.selected && ((data.select.id == value.id))) {
                    $("#messages").addClass("open")
                    Layout.getMessageById(value.id)
                    //$('.user').trigger('click')
                    userTemplate.addClass("selected")
                } else if (value.id == id) {
                    userTemplate.addClass("selected")
                }
                //
                $('#conactList').append(userTemplate)
            })
            socket.emit('getOnline');
        });
        $(document).on('click', '.startChat', function() {
            var sender_id = $(this).data('sender-id')
            var recipient_id = $(this).data('recipient-id')
            socket.emit('newConwersation', {
                sender_id: sender_id,
                recipient_id: recipient_id
            })
        })
        $(document).on('click', '.deleteN', function() {
            var id = $(this).data('id')
            socket.emit('deleteN', {
                id: id,
            })
        })
        $(document).on('click', '.deleteN', function() {
            var id = $(this).data('id')
            var type = $(this).data('type')
            switch (type) {
                case "rent_request":
                    $.ajax({
                        type: 'POST',
                        url: '/apply_rent',
                        data: {
                            notification_id: id
                        },
                        success: function(id) {
                            socket.emit('deleteN', {
                                id: id,
                            })
                        },
                        error: function(data) {
                            swal({
                                title: "Oooops!",
                                text: "Server error, try again later",
                                type: "warning",
                            });
                        }
                    });

                    break;
                case "contact_request":
                    socket.emit('applyN', {
                        id: id,
                    })
                    break;
                default:

            }
            socket.emit('deleteN', {
                id: id,
            })
        })
        $(document).on('click', '.contact-request', function() {
            var user_id = $(this).data('user-id')
            var recipient_id = $(this).data('recipient-id')
            var post_id = $(this).data('post-id')
            socket.emit('contactRequest', {
                user_id: user_id,
                recipient_id: recipient_id,
                post_id: post_id
            })
            swal({
                title: "Ok!",
                text: "Your request has been sent. Wait",
                type: "success",
            })
        })
    },
}
$(document).ready(function() {
    id = $('.user').data('id')
    Chat.init()
    Pleasure.init();
    Layout.init();
});
