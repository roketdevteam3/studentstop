/**
 * Created by Listat on 28.03.2017.
 */
$(document).ready(function(){
  $('#rents-price').change(function () {
    if (this.value !== ''){
      $('#rents-credits').prop('required',false);
    } else {
      $('#rents-credits').prop('required',true);
    }
  })
  $('#rents-credits').change(function () {
    if (this.value !== ''){
      $('#rents-price').prop('required',false);
    } else {
      $('#rents-price').prop('required',true);
    }
  })
  $('#rentshouse-price').change(function () {
    if (this.value !== ''){
      $('#rentshouse-credits').prop('required',false);
    } else {
      $('#rentshouse-credits').prop('required',true);
    }
  })
  $('#rentshouse-credits').change(function () {
    if (this.value !== ''){
      $('#rentshouse-price').prop('required',false);
    } else {
      $('#rentshouse-price').prop('required',true);
    }
  })
});