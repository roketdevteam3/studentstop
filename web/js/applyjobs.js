$(document).ready(function(){
    $('.gr-info').hover(function(){
        $(this).find('.gr-info-show').show();
    },
    function(){
        $(this).find('.gr-info-show').hide();
    });

    if($('div#w2').length != 0){
        $('#w2').css('display', 'none');
        swal("Company update", "You clicked the button!", "success");
    }

    $(document).on('click','.applyJob',function(){
        var job_id = $(this).data('job_id');
        var page_type = $(this).data('page_type');
        var thisElement = $(this);
        
        swal({
            title: "Are you sure?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#5cb85c",
            confirmButtonText: "Apply",
            closeOnConfirm: false 
        }, function(){
            
            $.ajax({
                type: 'POST',
                url: '../../../applyjob',
                data: {jobs_id:job_id},
                dataType:'json',
                success: function(response){
                    if(response.status == 'success'){
                        if(page_type == 'search'){
                            thisElement.removeClass('applyJob');
                            thisElement.addClass('unapplyJob');
                            thisElement.text('Unapply');
                            swal("Apply", "", "success");
                        }else if(page_type == 'dashboard'){
                            thisElement.removeClass('applyJob');
                            thisElement.addClass('unapplyJob');
                            thisElement.text('Unapply');
                            swal("Apply", "", "success");
                        }
                        thisElement.parent().find('.mailToUser').trigger("click");
                    }
                } 
            });
            
        });
    });
    
    $(document).on('click','.unapplyJob',function(){
        var job_id = $(this).data('job_id');
        alert(job_id);
        var page_type = $(this).data('page_type');
        var thisElement = $(this);
        swal({
            title: "Are you sure?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#5cb85c",
            confirmButtonText: "Unapply",
            closeOnConfirm: false 
        }, function(){
            
            $.ajax({
                type: 'POST',
                url: '../../../unapplyjob',
                data: {jobs_id:job_id},
                dataType:'json',
                success: function(response){
                    if(response.status == 'success'){
                        if(page_type == 'search'){
                            thisElement.removeClass('unapplyJob');
                            thisElement.addClass('applyJob');
                            thisElement.text('Apply');
                            swal("Unapply", "", "success");
                        }else if(page_type == 'dashboard'){
                            thisElement.removeClass('unapplyJob');
                            thisElement.addClass('applyJob');
                            thisElement.text('Apply');
                            swal("Unapply", "", "success");
                        }
                    }
                } 
            });
            
        });
    });
    
    $(document).on('click','.addToFavorite',function(){
        var job_id = $(this).data('job_id');
        var thisElement = $(this);
        $.ajax({
            type: 'POST',
            url: '../../addjobtofavorite',
            data: {jobs_id:job_id},
            dataType:'json',
            success: function(response){
                if(response.status == 'success'){
                    thisElement.removeClass('addToFavorite');
                    thisElement.addClass('deleteWithFavorite');
                    thisElement.find('.text').text('Delete with favorite');
                    swal("Add to favorite", "", "success");
                }
            } 
        });
    });
    
    $(document).on('click','.deleteWithFavorite',function(){
        var job_id = $(this).data('job_id');
        var thisElement = $(this);
        $.ajax({
            type: 'POST',
            url: '../../deletejobwithfavorite',
            data: {jobs_id:job_id},
            dataType:'json',
            success: function(response){
                if(response.status == 'success'){
                    thisElement.removeClass('deleteWithFavorite');
                    thisElement.addClass('addToFavorite');
                    thisElement.find('.text').text('Add to favorite');
                    swal("Deleted with favorite", "", "success");
                }
            } 
        });
    });


  $(document).on('click','.addToFavoriteRezume',function(){
    var rezume_id = $(this).data('rezume_id');
    var job_id = $(this).data('job_id');
    var thisElement = $(this);
    $.ajax({
      type: 'POST',
      url: '../../addrezumetofavorite',
      data: {rezume_id:rezume_id, job_id:job_id},
      dataType:'json',
      success: function(response){
        if(response.status == 'success'){
          thisElement.removeClass('addToFavoriteRezume');
          thisElement.addClass('deleteWithFavoriteRezume');
          thisElement.find('.text').text('Delete with favorite');
          swal("Add to favorite", "", "success");
        }
      }
    });
  });

  $(document).on('click','.deleteWithFavoriteRezume',function(){
    var rezume_id = $(this).data('rezume_id');
    var job_id = $(this).data('job_id');
    var thisElement = $(this);
    $.ajax({
      type: 'POST',
      url: '../../deleterezumewithfavorite',
      data: {rezume_id:rezume_id, job_id:job_id},
      dataType:'json',
      success: function(response){
        if(response.status == 'success'){
          thisElement.removeClass('deleteWithFavoriteRezume');
          thisElement.addClass('addToFavoriteRezume');
          thisElement.find('.text').text('Add to favorite');
          swal("Deleted with favorite", "", "success");
        }
      }
    });
  });


  $(document).on('click','.addToFavoriteCompany',function(){
    var company_id = $(this).data('company_id');
    var thisElement = $(this);
    $.ajax({
      type: 'POST',
      url: '../../addcompanytofavorite',
      data: {company_id:company_id},
      dataType:'json',
      success: function(response){
        if(response.status == 'success'){
          thisElement.removeClass('addToFavoriteCompany');
          thisElement.addClass('deleteWithFavoriteCompany');
          thisElement.find('.text').text('Delete with favorite');
          swal("Add to favorite", "", "success");
        }
      }
    });
  });

  $(document).on('click','.deleteWithFavoriteCompany',function(){
    var company_id = $(this).data('company_id');
    var thisElement = $(this);
    $.ajax({
      type: 'POST',
      url: '../../deletecompanywithfavorite',
      data: {company_id:company_id},
      dataType:'json',
      success: function(response){
        if(response.status == 'success'){
          thisElement.removeClass('deleteWithFavoriteCompany');
          thisElement.addClass('addToFavoriteCompany');
          thisElement.find('.text').text('Add to favorite');
          swal("Deleted with favorite", "", "success");
        }
      }
    });
  });

  $(document).on('click','#actionViewList',function(){

    $('#actionViewList').addClass('active');
    $('#actionViewGrid').removeClass('active');
    $('#view_list_company').show();
    $('#view_grid_company').hide();
    $('#view_list_vacancy').show();
    $('#view_grid_vacancy').hide();
    $('#view_list_student').show();
    $('#view_grid_student').hide();
  });

  $(document).on('click','#actionViewGrid',function(){
    $('#actionViewGrid').addClass('active');
    $('#actionViewList').removeClass('active');
    $('#view_grid_student').show();
    $('#view_list_student').hide();
    $('#view_grid_vacancy').show();
    $('#view_list_vacancy').hide();
    $('#view_grid_company').show();
    $('#view_list_company').hide();
  });

});        