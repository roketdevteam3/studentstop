var classChat = (function () {
    var socket = {},
        user_id = '',
        class_id = '',
        files_msg = [],
        message_box = {},
        classChat = {};

    classChat.init = function () {
//        socket = io.connect('localhost:2700');
        socket = io.connect('https://thestudentstop.com:2500');
        user_id = $("input#user_id").val();
        class_id = $("input#class_id").val();
        //alert(class_id);
        message_box = $("#text-message-class");

        socket.on('connect', function () {
            socket.emit('loginClass', {
                user_id: user_id,
                class_id: class_id
            });

            classChat.setListener(socket);
        });

        socket.on('JoinChat', function (data) {
            console.info(data);
        });
    };
    classChat.setListener = function (socket) {
        $("#send-message-class").click(function () {
            if (message_box.val() != '' || files_msg.length > 0) {
                socket.emit('classMessage', {
                    user_id: user_id,
                    class_id: class_id,
                    message: message_box.val(),
                    files: files_msg
                });
                message_box.val('');
                $(".select-file-box .dz-preview").remove();
                $(".select-file-box .user_photos").removeClass('dz-started');
                $(".select-file-box").removeClass('fadeInUp');
                files_msg = [];
            }
        });

        $(".send-message .add-files").click(function () {
            $(".select-file-box").addClass('fadeInUp');
        });
        $(".select-file-box .close-box").click(function () {
            $(this).parent().removeClass('fadeInUp');
        });

        socket.emit('getListMessage', {
            class_id: class_id
        });

        socket.emit('getOnlineUsers', {
            class_id: class_id
        });

        socket.on('reciveClassMessage', function (data) {
            classChat.addMessage(data);
        });
        socket.on('reciveListMessage', function (data) {
            console.log(data);
            classChat.addMessageHistory(data);
        });
        socket.on('responseOnlineUsers', function (data) {
            classChat.addListUsers(data);
        });
        socket.on('JoinChat', function (data) {
            classChat.addListUsers(data);
        });
        socket.on('LeaveChat', function (data) {
            $(".class_users").find("#user-" + data).remove();
        });


        function deleteFile(file_name) {
            $.ajax({
                type: 'POST',
                url: '../../../course/default/deletefilechat/' + class_id,
                data: {file_name: file_name},
                success: function (response) {
                }
            });
        }

        var myDropzone = new Dropzone($('.user_photos')[0], { // Make the whole body a dropzone
            maxFilesize: 50,
            acceptedFiles: 'video/*, image/bmp, image/gif, image/jpeg, image/png, application/x-7z-compressed, application/x-gtar, application/zip, application/x-rar-compressed, application/x-zip-compressed, application/msword, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.openxmlformats-officedocument.wordprocessingml.template, application/vnd.ms-word.document.macroEnabled.12, application/vnd.ms-word.template.macroEnabled.12, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.openxmlformats-officedocument.spreadsheetml.template, application/vnd.ms-excel.sheet.macroEnabled.12, application/vnd.ms-excel.template.macroEnabled.12, application/vnd.ms-excel.addin.macroEnabled.12, application/vnd.ms-excel.sheet.binary.macroEnabled.12, application/vnd.ms-powerpoint, application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation, application/vnd.openxmlformats-officedocument.presentationml.template, application/vnd.openxmlformats-officedocument.presentationml.slideshow, application/vnd.ms-powerpoint.addin.macroEnabled.12, application/vnd.ms-powerpoint.presentation.macroEnabled.12, application/vnd.ms-powerpoint.template.macroEnabled.12,application/vnd.ms-powerpoint.slideshow.macroEnabled.12',
        });

        myDropzone.on("sending", function (file) {
            //console.log(file);
            //  file.name = file.name+Math.random().toString(36).substring(7);
            // Hookup the start button
            //file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file); };
        });
        // Update the total progress bar

        myDropzone.on("complete", function (response) {
            if (response.status == 'success') {
                var type = response.type.split("/");


                files_msg.push({
                    lastModified: response.lastModified,
                    type: type[0],
                    src: response.xhr.response
                });

                $(response.previewElement).prepend('<i class="fa fa-times removePhoto fa-2x" title="delete" style="color:#DBDBDB;cursor:pointer;right:-5px;top:-10px;position:absolute;z-index:999" data-name="' + response.name + '"></i>');
            }
        });
        myDropzone.on("removedfile", function (response) {
            var removeItem = response.xhr.response;
            deleteFile(response.xhr.response);
            files_msg.forEach(function (element, index, array) {
                if ((element.lastModified == response.lastModified)) {
                    array.splice(index, 1);
                }
            });
        });

        $(document).on('click', '.removePhoto', function () {
            var shos = $(this).parent();

            var name_photo = $(this).data('name');
            $.each(myDropzone.files, function (key, value) {
                if (value.name == name_photo) {
                    myDropzone.removeFile(myDropzone.files[key]);
                }
            });
        });

    };

    classChat.addListUsers = function (data) {
        var tpl = '',
            users_box = $(".class_users");
        data.forEach(function (element, index, array) {
            if (users_box.find("#user-" + element.id).length == 0) {
                tpl = '<li class="has-action-left has-action-right" id="user-' + element.id + '"><div class="list-action-left"><img src="/images/users_images/' + element.avatar + '" class="face-radius" alt=""></div><div class="list-content"><span class="title" style="color: #3e50b4;">' + element.name + ' ' + element.surname + '</span><span class="caption"></span></div><div class="list-action-right"><span class="top">online</span></div></li>';
                users_box.append(tpl);
                users_box.scrollTop(users_box.prop("scrollHeight"));
            }
        });
    };

    classChat.addMessageHistory = function (data) {
        var tpl = '',
            tpl_doc = '',
            message_box = $(".message-class-container > .messages");
        var tmp_id = -1;
        data.forEach(function (element, index, array) {
            if (tmp_id != element.id) {
                var msq_text = '<p>' + element.message + '</p>';
                var msq_cadd = element.created_add;
                
                var avatar = '/images/default_avatar.jpg';
                if((element.avatar != null) && (element.avatar != '')){
                     avatar = '/images/users_images/'+element.avatar;
                }
                
                var side = 'right';
                if (element.user_id == user_id) {
                    side = 'left'
                }
                tpl_doc = classChat.addFilesHistoryChat(data, element.id);
                tpl = '<div class="message animate ' + side + '"><div class="user-picture-wrap"><img style="width:100%;" src="' + avatar + '" class="user-picture" alt=""></div><span class="time">' + msq_cadd + '</span> <div class="message-text">' + tpl_doc + msq_text + '</div></div>';
                message_box.append(tpl);
                message_box.scrollTop(message_box.prop("scrollHeight"));
            } else {

            }
            tmp_id = element.id;
        });
    };

    classChat.addFilesHistoryChat = function (data, id) {
        var tpl = '';
            //alert(class_id);
        data.forEach(function (element, index, array) {
            if (element.id == id) {
                if(element.file_src){
                    switch (element.mime_type) {
                        case 'image':
                        {
                            tpl += '<a href="/files/' + class_id + '/' + element.file_src + '" class="fresco" data-fresco-group="example-' + id + '" data-fresco-caption="Sir Joshua Reynolds, The Countess of Dartmouth, 1757"> <img src="/files/' + class_id + '/' + element.file_src + '" style="height:40px;"/> </a>';
                            break;
                        }
                        default:
                        {

                            tpl += '<a target="_blank" href="/files/' + class_id + '/' + element.file_src + '">' + element.file_src + '</a>';
                        }
                    }
                }else{
                    tpl = '';
                }
            }
        });
        return tpl;
    };

    classChat.addMessage = function (data) {
        var side = 'right',
            tpl = '',
            tpl_docs = classChat.addFilesMessage(data.files),
            message_box = $(".message-class-container > .messages");

         var avatar = '/images/default_avatar.jpg';
        if((data.avatar[0].avatar != null) && (data.avatar[0].avatar != '')){
            avatar = '/images/users_images/'+data.avatar[0].avatar;
        }

        if (data.user_id == user_id) {
            side = 'left'
        }
        console.log(data);

        tpl = '<div class="message animate ' + side + '"><div class="user-picture-wrap"><img style="width:100%;" src="' + avatar + '" class="user-picture" alt=""></div><span class="time">' + data.created_add + '</span><div class="message-text">' + tpl_docs + '<p>' + data.message + ' </p> </div> </div>';
        message_box.append(tpl);
        message_box.scrollTop(message_box.prop("scrollHeight"));
    };

    classChat.addFilesMessage = function (files) {
        var tpl = '';
        files.forEach(function (element, index, array) {
            console.log(element);
            switch (element.type) {
                case 'image':
                {
                    tpl += '<a href="/files/' + class_id + '/' + element.src + '" class="fresco" data-fresco-group="example-' + id + '" data-fresco-caption="Sir Joshua Reynolds, The Countess of Dartmouth, 1757"> <img src="/files/' + class_id + '/' + element.src + '"/> </a>';
                    break;
                }
                default:
                {
                    tpl += '<a target="_blank" href="/files/' + class_id + '/' + element.src + '">' + element.src.split('/')[1] + '</a>';
                }
            }
        });
        console.log(tpl);
        return tpl;
    };

    return classChat;
})();

$(document).ready(function () {
    classChat.init();    
});