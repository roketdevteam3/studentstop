


$(document).ready(function(){
    var OnlineUser = [];
    
    var socket = io.connect('https://thestudentstop.com:3000');
    socket.on('connect', function () {
            socket.on('onlineChange', function(data) {
                if (data.status) {
//                    console.log(data.user);
                    if (data.status) {
                        $('.friendClass'+data.user).find('.UserStatus').html('(Online)');
                    }else{
                        $('.friendClass'+data.user).find('.UserStatus').html('(Offline)');
                    }
                }
            })
                
            socket.on('onlineUsers', function(data) {
                OnlineUser = data;
                showFriendRightBlock('',$('[name=my_input_id]').val(),'friend');
            })
    })
    
    
    
    $('.carousel').carousel()
    if($('.universityRatingCount').length){
        var university_id = $('.universityInfoId').text();
        $.ajax({
            type: 'POST',
            url: '../../../university/default/getuniversityratinglevel',
            data: {university_id:university_id},
            dataType:'json',
            success: function(response){
                $('.universityRatingCount').html(response['k']+'/'+response['c']);
            }
        });
    }
    var filesCommentArray = [];
    var filesCommentArrayModal = [];
    var filesPostArray = {};
//    $(document).on('click','.showFriendMessage',function(){
//        $('.modalFriendMassegeBlock').show();
//    })
    var notification_from = 5;
    $(document).on('click','.loadMoreNotification', function(){
        var university_id = $(this).data('university_id');
        var thisElement = $(this);
        $.ajax({
            type: 'POST',
            url: '../../../university/default/getnotification',
            data: {university_id:university_id,offest:notification_from},
            dataType:'json',
            success: function(response){
                notification_from = parseInt(notification_from) + 5;
                console.log(notification_from);
                if(notification_from >= response['count']){
                    thisElement.hide();
                }
                //console.log(response['model'])
                if(response['model'].length != 0){
                    var notificationModel = response['model'];
                    for(var key in notificationModel){
                        var notification = notificationModel[key];
                        $('.notificationTable').append('<tr class="classTr'+notification.id+'">');
                        switch (notification.event_type){
                           case 'post':
                                $('.notificationTable').find('.classTr'+notification.id).append('<td>'+notification.name+' '+notification.surname+'; posted post</td>');
                                break;
                            case 'event':
                                $('.notificationTable').find('.classTr'+notification.id).append('<td>'+notification.name+' '+notification.surname+'; add event</td>');
                                break;
                            case 'comment_to_post':
                                $('.notificationTable').find('.classTr'+notification.id).append('<td>'+notification.name+' '+notification.surname+'; comment</td>');
                                break;
                        }
                        
                    }
                }
            }
        });
        
    })
    
    $(window).click(function() {
        $('.modalFriendMassegeBlock').hide();
    });

//    $('#menucontainer').click(function(event){
//    });
    
    function showFriendRightBlock(inputValue,my_id,filter){
        console.log(filter);
        $.ajax({
            type: 'POST',
            url: '../../../university/default/getsearchfriend',
            data: {inputValue:inputValue,filter:filter},
            dataType:'json',
            success: function(response){
                $('.friendSearchBlock').html('')
                for(var key in response){
                    var modelUser = response[key];
                    var avatar_src = '../../../images/default_avatar.jpg'
                    if(modelUser.avatar != ''){
                        avatar_src = '../../../images/users_images/'+modelUser.avatar;
                    }
                    
                    $('.friendSearchBlock').append('<div class="friendClass'+modelUser.id+' startChat startChatFriend" data-sender-id="'+my_id+'" data-recipient-id="'+modelUser.id+'">'+
                            '<div class="avatar-chat-wrap">'+
                                '<img class="" src="'+avatar_src+'" class="friendImg">'+
                            '</div>'+
                            '<div class="user-name" style="line-height:14px;">'+
                                    modelUser.name+' '+modelUser.surname+
                                '<br><span class="UserStatus">'+
                                    'Offline'+
                                '</span>'+
                            '</div>'+
                        '</div>')
                    
                    var statusUserO = '';
                    for(var key in OnlineUser){
                        if(OnlineUser[key] == modelUser.id){
                           $('.friendClass'+modelUser.id).find('.UserStatus').html('Online');
                        }
                    }
                }
            }
        });
    }
    
    $(document).on('keyup','.friendSearch',function(){
        var inputValue = $(this).val();
        var my_id = $(this).data('my_id');
        var filterAction = $('.fiterBlock').find('.active').data('action');
        showFriendRightBlock(inputValue,my_id,filterAction);
    });
    
    $(document).on('click','.filterFriend',function(){
        var inputValue = $('.friendSearch').val();
        $('.fiterBlock').find('li').removeClass('active');
        $(this).addClass('active');
        showFriendRightBlock(inputValue,$('[name=my_input_id]').val(),'friend');
    });
    
    $(document).on('click','.filterAlumni',function(){
        var inputValue = $('.friendSearch').val();
        $('.fiterBlock').find('li').removeClass('active');
        $(this).addClass('active');
        showFriendRightBlock(inputValue,$('[name=my_input_id]').val(),'alumni');
    });
    
    $(document).on('click','.filterClassmates',function(){
        var inputValue = $('.friendSearch').val();
        $('.fiterBlock').find('li').removeClass('active');
        $(this).addClass('active');
        showFriendRightBlock(inputValue,$('[name=my_input_id]').val(),'classmates');
    });
    
    
    
    
    $(document).on('click','.showFriendMessage',function(){
        event.stopPropagation();
        var user_id = $(this).data('friend_id');
        $('.modalFriendMassegeBlock').show();
        $.ajax({
                type: 'POST',
                url: '../../../users/default/getuserinfo',
                data: {user_id:user_id},
                dataType:'json',
                success: function(response){
                    $('.modalFriendMassegeBlock').find('.userAvatar').attr('src','../../../images/users_images/'+response['avatar']);
                    $('.modalFriendMassegeBlock').find('.username').text(response['name']+' '+response['surname']);
                }
        });
        
    });
    $(document).on('click','.modalFriendMassegeBlock',function(){
        event.stopPropagation();
    });
    
    
    $(document).on('focus','.contentNews',function(){
        $(this).attr('rows', '2');
    });
    $(document).on('focusout','.contentNews',function(){
        $(this).attr('rows', '1');
    });
    $(document).on('focus','[name=newCommentPost]',function(){
        $(this).attr('rows', '2');
    });
    $(document).on('focusout','[name=newCommentPost]',function(){
        $(this).attr('rows', '1');
    });
    if($('.addFilePost').length){
            
            var previewNode = document.querySelector("#previews");
            var previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);

            var myDropzoneA = new Dropzone($('.addFilePost')[0], {
                url: '../../../../university/default/savedropedfile',
                previewTemplate:previewTemplate,
                clickable: ".addFilePost",
                acceptedFiles:'image/*,video/*',
                previewsContainer: ".previewFileBlock",
            });

            myDropzoneA.on("maxfilesexceeded", function(file) {
                    myDropzoneA.removeAllFiles();
                    myDropzoneA.addFile(file);

            });

            myDropzoneA.on("complete", function(response) {
                if (response.status == 'success'){
                    console.log(response.type);
                    var fileType = response.type;
                    
                    if(typeof filesPostArray[response.xhr.response] == 'undefined'){
                        filesPostArray[response.xhr.response] = {};
                    }
                    
                    console.log(filesPostArray);
                    filesPostArray[response.xhr.response] = fileType;
                    $("[name=filesInput]").val(JSON.stringify(filesPostArray));
                }
            });

            myDropzoneA.on("removedfile", function(response) {
                if(response.xhr != null){
                    var removeItem = response.xhr.response;

                    //console.log(filesArray);
                    delete(filesPostArray[removeItem])
                    
                    console.log(filesPostArray);
//                    filesArray.splice(filesArray.indexOf(removeItem), 1);
                    $("[name=filesInput]").val(JSON.stringify(filesPostArray));
                }
            });
    }


        function addFileDropzone(id){
                    $('body').append('<div style="display:none;">'+
                                        '<div id="previews'+id+'" class="previews-comment-file">'+
                                            '<div class="preview" style="position:relative;width:100%;">'+
                                                '<img data-dz-thumbnail style="width:100%;"/>'+
                                                '<div class="x-button">'+
                                                    '<i class="fa fa-times" data-dz-remove></i>'+
                                                ' </div>'+
                                           ' </div>'+
                                        '</div>'+
                                    '</div>');
            if(typeof filesCommentArray[id] == 'undefined'){
                filesCommentArray[id] = [];
            }
            var previewNode = document.querySelector("#previews"+id);
            var previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);

            var myDropzone = new Dropzone($('.addFileComment'+id)[0], {
                url: '../../../../university/default/savedropedfile',
                previewTemplate:previewTemplate,
                acceptedFiles:'image/*,video/*',
                clickable: ".addFileComment"+id,
                previewsContainer: '.previewFileBlockComment'+id,
            });
            
            myDropzone.on("complete", function(response) {
                if (response.status == 'success') {
                    filesCommentArray[id].push(response.xhr.response);
                    $("[name=commentFilesInput"+id+"]").val(JSON.stringify(filesCommentArray[id]));
                }else{
                    response.status();
                }
            });

            myDropzone.on("removedfile", function(response) {
                if(response.xhr != null){
                    var removeItem = response.xhr.response;
                    filesCommentArray[id].splice(filesCommentArray[id].indexOf(removeItem), 1);
                    $("[name=commentFilesInput"+id+"]").val(JSON.stringify(filesCommentArray));
                }
            });
            $(".addFileComment"+id).parent().find('.clickCommentFilesAdd').removeClass('clickCommentFilesAdd');
            $(".addFileComment"+id).trigger('click');
        }
    
    
    $(document).on('click','.clickCommentFilesAdd',function(){
        //alert($(this).data('post_id'));
        addFileDropzone($(this).data('post_id'));
    });
            
    $(document).on('click', '.ButtonAddedPost', function(event){
        if(($('.contentNews').val() == '') && ( ($('[name=filesInput]').val() == '') || ($('[name=filesInput]').val() == '[]') || ($('[name=filesInput]').val() == '{}' )) ){
            event.preventDefault();
            $('.contentNews').focus();
        }
    });
    $(document).on('submit', ".add_comment_to_post",function( event ) {
        event.preventDefault();
        var thisElement = $(this);
        var post_id = $(this).data('post_id');
        var commentContent = $(this).find('[name=newCommentPost]').val();
        var commentFiles = $(this).find('[name=commentFilesInput'+post_id+']').val();
        if((commentContent == '') && ((commentFiles == '') || (commentFiles == '[]') )){
            $(this).find('[name=newCommentPost]').focus();
        }else{            
            $.ajax({
                type: 'POST',
                url: '../../../university/default/addcomment',
                data: {commentFiles:commentFiles, post_id:post_id, commentContent:commentContent},
                dataType:'json',
                success: function(response){
                    console.log(response);
                    //alert(post_id)
                    if(response.status == 'success'){
                        thisElement.find('[name=newCommentPost]').val('');
                        thisElement.find('[name=commentFilesInput'+post_id+']').val('');
                        thisElement.find('.previewFileBlockComment'+post_id).html('');
                        var avatar = '<img src="../../../images/default_avatar.jpg" style="width:100%;">';
                        var comment = response.data.comment;
                        var filess = response.data.files;
                        var files = '';
                        for(var key in filess){
                            files += filess[key].file_src+'; ';
                        }
                        if(comment.avatar != ''){
                            avatar = '<img class="img-responsive" src="/images/users_images/'+comment.avatar+'">';
                        }
                        //console.log(files);
                        $('.comConte'+post_id).prepend('<div class="comment-block">'+
                                '<div class="comment-header">'+
                                    '<div class="author-image">'+
                                        avatar+
                                    '</div>'+
                                    '<div class="comment-header-info">'+
                                        '<div class="comment-autor">'+
                                            comment.name+
                                            comment.surname+
                                        '</div>'+
                                        '<div class="comment-date">'+
                                                comment.commentCreate+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="comment-text">'+
                                    comment.content+
                                '</div>'+
                                '<div class="comment-file commentFile'+comment.id+'">'+
                                '</div>'+
                            '</div>'+
                            '<hr class="comments-separator">');
                                if(filess.length){
                                    for(var keyF in filess){
                                        var com_file = filess[keyF];
                                        $('.commentFile'+comment.id).append('<img src="../../../images/users_images/'+com_file['file_src']+'" style="width:100px;">');
                                    }
                                }
                    }
                }
            });
        }
    });
    
    $( ".add_comment_to_post_modal" ).submit(function(event){
        event.preventDefault();
        
        var thisElement = $(this);
        var post_id = $(this).data('post_id');
        var commentContent = $(this).find('[name=newCommentPost]').val();
        var commentFiles = $(this).find('[name=commentFilesInputModal]').val();        
        if((commentContent == '') && ((commentFiles == '') || (commentFiles == '[]') )){
            $(this).find('[name=newCommentPost]').focus();
        }else{     
            $.ajax({
                type: 'POST',
                url: '../../../university/default/addcomment',
                data: {commentFiles:commentFiles, post_id:post_id, commentContent:commentContent},
                dataType:'json',
                success: function(response){
                    console.log(response);
                    if(response.status == 'success'){
                        thisElement.find('[name=newCommentPost]').val('');
                        thisElement.find('[name=commentFilesInputModal]').val('');
                        thisElement.find('.previewFileBlockCommentModal').html('');
                        var avatar = '<img src="../../../images/default_avatar.jpg">';
                        var comment = response.data.comment;
                        var filess = response.data.files;
                        var files = '';
                        for(var key in filess){
                            files += filess[key].file_src+'; ';
                        }
                        if(comment.avatar != ''){
                            avatar = '<img class="img-responsive" src="/images/users_images/'+comment.avatar+'">';
                        }
                        //console.log(files);
                        thisElement.parent().parent().parent().parent().find('.commentContentBlock').prepend('<div class="comment-wrap new">'+
                                '<div class="comment-header">'+
                                    '<div class="author-image">'+
                                        avatar+
                                    '</div>'+
                                    '<div class="comment-header-info">'+
                                        '<div class="comment-autor">'+
                                            comment.name+
                                            comment.surname+
                                        '</div>'+
                                        '<div class="comment-date">'+
                                                comment.commentCreate+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="comment-text">'+
                                    comment.content+
                                    '<div class="comment-file">'+
                                        files+
                                    '</div>'+
                                '</div>'+
                            '</div>');
                    }
                }
            });
        }
    });
    
    $(document).on('click','.showPostClick',function(){
        $('#modalShowPost').modal('show');
        filesCommentArrayModal = [];
        var post_id = $(this).data('post_id');
        $.ajax({
            type: 'POST',
            url: '../../../university/default/getpostinformation',
            data: {post_id:post_id},
            dataType:'json',
            success: function(response){
                console.log(response);
                
                filesCommentArrayModal = [];
                $('.add_comment_to_post_modal').find('[name=newCommentPost]').val('');
                if(!jQuery.isEmptyObject(response)){
                    if(response['views'] == 'add'){
                        var viewsCount = $('.viewsCount'+response.post_info['id']).find('span').text();
                        $('.viewsCount'+response.post_info['id']).find('span').text(parseInt(viewsCount) + 1);
                    }
                    var avatar = '';
                    if(response.post_info['avatar']){
                        avatar = '<img class="img-responsive" src="../../../images/users_images/'+response.post_info['avatar']+'">';
                    }else{
                        avatar = '<img class="img-responsive" src="../../../images/default_avatar.jpg">';
                    }
                    var username = response.post_info['name']+' '+response.post_info['surname'];
                    var postCreate = response.post_info['postCreate'];
                    var postContent = response.post_info['content'];
                    
                    var username = response.post_info['name']+' '+response.post_info['surname'];
                    if((response.post_info['id_user'] == 0) || (response.post_info['id_user'] == null)){
                        var username = $('.universityInfoName').text();
                        avatar = '<img src="'+$('.universityInfoImage').text()+'" style="width:100%;position:relative;z-index:6;cursor:default;">';
                    }
                    
                    
                    $('#modalShowPost').find('.avatar').html(avatar);
                    $('#modalShowPost').find('.username').html(username);
                    $('#modalShowPost').find('.postCreate').html(postCreate);
                    $('#modalShowPost').find('.postContent').html(postContent);
                    var filesArray = response.files;
                    $('#modalShowPost').find('.postFilesBlock').html('');
                    if(filesArray.length){
                        for(var key in filesArray){
                            if(filesArray[key]['file_type'] == '0'){
                                $('#modalShowPost').find('.postFilesBlock').append('<img class="img-responsive" src="../../../images/users_images/'+filesArray[key]['file_src']+'" style="">');
                            }else{
                                $('#modalShowPost').find('.postFilesBlock').append(' '+
                                '<video width="400" controls >'+
                                    '<source src="../../../images/users_images/'+filesArray[key]['file_src']+'" >'+
                                    'Your browser does not support HTML5 video.'+
                                '</video>')
                            }
                        }
                    }
                    $('.add_comment_to_post_modal').data('post_id', post_id);
                    $('.addFileCommentModal').data('post_id', post_id);
                    var commentsArray = response.comments;
                    if(commentsArray.length){
                        $('#modalShowPost').find('.commentContentBlock').html('');
                        for(var keyComent in commentsArray){
                            var commentInfo = commentsArray[keyComent].comment_info;
                            var avatarComment;
                            if(commentInfo.avatar){
                                avatarComment = '<img src="../../../images/users_images/'+commentInfo.avatar+'" style="width:100%;">';
                            }else{
                                avatarComment = '<img src="../../../images/default_avatar.jpg" style="width:100%;">';
                            }
                            //console.log(commentsArray[key].content)
                            $('#modalShowPost').find('.commentContentBlock').append('<div class="comment-wrap comment'+commentInfo.id+'">'+
                                                '<div class="comment-header">'+
                                                    '<div class="author-image">'+
                                                        avatar+
                                                    '</div>'+
                                                    '<div class="comment-header-info">'+
                                                        '<div class="comment-autor">'+
                                                            commentInfo.name+
                                                            commentInfo.surname+
                                                        '</div>'+
                                                    '</div>'+
                                                '</div>'+
                                                '<div class="comment-text">'+
                                                    commentInfo.content+
                                                    '<div class="comment-file commentFilesBlock'+commentInfo.id+'">'+
                                                    '</div>'+
                                                    '<div class="comment-date">'+
                                                        commentInfo.commentCreate+
                                                    '</div>'+
                                                '</div>'+
                            '</div>');
                            var commentFiles = commentsArray[keyComent].files;
                            for(var keycfile in commentFiles){
                                $('#modalShowPost').find('.commentFilesBlock'+commentFiles[keycfile].comment_id).append('<img src="../../../images/news_images/'+commentFiles[keycfile].file_src+'" style="width:100px;">');
                            }
                        }
                    }else{
                        console.log(commentsArray[key])
                    }
                                
                }
            }
        });
    })
    if($('.addFileCommentModal').length){
        var previewNode = document.querySelector("#previewsFileModal");
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzoneFM = new Dropzone($('.addFileCommentModal')[0], {
            url: '../../../../university/default/savedropedfile',
            previewTemplate:previewTemplate,
            acceptedFiles:'image/*,video/*',
            clickable: ".addFileCommentModal",
            previewsContainer: '.previewFileBlockCommentModal',
        });

        myDropzoneFM.on("complete", function(response) {
            if (response.status == 'success') {
                filesCommentArrayModal.push(response.xhr.response);
                $("[name=commentFilesInputModal]").val(JSON.stringify(filesCommentArrayModal));
            }else{
                response.status();
            }
        });

        myDropzoneFM.on("removedfile", function(response) {
            if(response.xhr != null){
                var removeItem = response.xhr.response;
                filesCommentArrayModal.splice(filesCommentArrayModal.indexOf(removeItem), 1);
                $("[name=commentFilesInputModal]").val(JSON.stringify(filesCommentArrayModal));
            }
        });
    }
    
    function outputPostOnPage(university_id,filterType){
        var page_type = $('.containerPost').data('page_type');
        $('.postFilterButton').removeClass('active');
        $.each($('.postFilterButton'),function(i, val){
            if($(this).data('filter_type') == filterType){
                $(this).addClass('active')
            }
        })
        $(this).addClass('active');
        
        $.ajax({
            type: 'POST',
            url: '../../../university/default/getfilterpost',
            data: {university_id:university_id, filterType:filterType},
            dataType:'json',
            success: function(response){
                if(response.length){
                    $('.containerPost').html('');
                    for(var key in response){
                        var post_info = response[key].post_info;
                        var post_files = response[key].files;
                        var comments = response[key].comments;
                        
                        
                        var avatarPost = '<img src="../../../../images/default_avatar.jpg" style="width:100%;position:relative;z-index:6;cursor:default;">';
                            if(post_info['avatar'] != ''){
                                avatarPost = '<img src="../../../../images/users_images/'+post_info['avatar']+'" style="width:100%;position:relative;z-index:6;">';
                            }
                            var settingClassPost = '';
                            if(page_type == 'setting'){
                                settingClassPost = 'settingBlockPost';
                            }
                            var updateButton = '';
                            var deleteButton = '';
                            if(post_info['user_id'] == $('[name=my_input_id]').val()){
                                updateButton = '<span class="postUpdate" data-post_id="'+post_info['id']+'"><i class="fa fa-edit"></i></span>';
                                deleteButton = '<span class="postDelete" data-post_id="'+post_info['id']+'"><i class="fa fa-times"></i></span>';
                            }                            
                            
                            var avtorName = post_info['name']+' '+post_info['surname'];
                            if((post_info['id_user'] == 0) || (post_info['id_user'] == null)){
                                var avtorName = $('.universityInfoName').text();
                                avatarPost = '<img src="'+$('.universityInfoImage').text()+'" style="width:100%;position:relative;z-index:6;cursor:default;">';
                            }
                        $('.containerPost').append('<div style="position:relative;" class="post-block '+settingClassPost+post_info['id']+' postBlockNumber'+settingClassPost+post_info['id']+'">'+
                                                                '<div class="post-header">'+
                                                                    '<div class="user-img">'+
                                                                        avatarPost+
                                                                    '</div>'+
                                                                    '<div class="post-header-info">'+
                                                                        '<h4 class="post-author">'+
                                                                            avtorName+
                                                                        '</h4>'+
                                                                        '<h6 class="post-date">'+
                                                                            //post_info['name']+
                                                                            ' '+post_info['postCreate']+
                                                                        '</h6>'+
                                                                    '</div>'+
                                                                    '<div class="post-like-comment-count">'+
                                                                        '<span class="count like-count clickLike likePost'+post_info['id']+'" data-post_id="'+post_info['id']+'">'+
                                                                             '<img src="../../../images/icon/like-ic.png">'+
                                                                            '<span>'+post_info['like_count']+'</span>'+
                                                                        '</span>'+
                                                                        '<span class="count post-count">'+
                                                                            '<img src="../../../images/icon/comment-ic.png">'+
                                                                            response[key].modelCommentCount+
                                                                        '</span>'+
                                                                        '<span class="count views-count viewsCount'+post_info['id']+'">'+
                                                                            '<img src="../../../images/icon/views-count-ic.png">'+
                                                                            '<span>'+post_info['views']+'</span>'+
                                                                        '</span>'+
                                                                        updateButton+
                                                                        deleteButton+
                                                                    '</div>'+
                                                                    '<div class="clearfix"></div>'+
                                                                '</div>'+
                                                        '<div class="post-content">'+
                                                            '<div style="position:absolute;z-index:5;width:100%;height:100%;cursor:pointer;" data-post_id="'+post_info['id']+'" class="showPostClick">'+
                                                            '</div>'+
                                                            '<div class="post-text">'+
                                                                post_info['content']+
                                                            '</div>'+
                                                            '<div class="post-file postFiles'+post_info['id']+'">'+
                                                            '</div>'+
                                                            '<div style="clear:both"></div>'+
                                                        '</div>'+
                                                        '<form class="add_comment_to_post commentPost'+post_info['id']+'" data-post_id="'+post_info['id']+'" style="position:relative;z-index:6;cursor:default">'+
                                                            '<div class="row">'+
                                                                '<div class="col-sm-10 no-padding-right">'+
                                                                    '<textarea class="form-control comment-input" name="newCommentPost" rows="1" placeholder="Write your comment ..."></textarea>'+
                                                                    '<input type="hidden" name="commentFilesInput'+post_info['id']+'" value="">'+
                                                                    '<div class="add-files btn-ripple clickCommentFilesAdd addFileComment'+post_info['id']+'" data-post_id="'+post_info['id']+'"></div>'+
                                                                    '<div class="previewFileBlockComment'+post_info['id']+'"></div>'+
                                                                '</div>'+
                                                                '<div class="col-sm-2 no-padding-left">'+
                                                                    '<input type="submit" class="btn add-comment" name="addComment" value="Add">'+
                                                                '</div>'+
                                                            '</div>'+
                                                        '</form>'+
                                                        '<div class="commentContainer comConte'+post_info['id']+'">'+
                                                        '</div>'+
                                                        
                                                        '<div class="adminActionSetting'+post_info['id']+'" data-post_id="'+post_info['id']+'" style="position:absolute;top:-10px;right:0px;width:auto;height:auto;cursor:ponter;"></div>'+
                                                        
                                                    '</div>');
                                            if(page_type == 'setting'){
                                                    if(post_info['slider'] == 1){
                                                        $('.adminActionSetting'+post_info['id']).append('<i class="deletePostWithSlider fa fa-times fa-2x"></i>')
                                                    }else{
                                                        $('.adminActionSetting'+post_info['id']).append('<i class="addPostInSlider fa fa-check fa-2x"></i>')
                                                    }
                                            }
                                
//                            if(page_type=='setting'){
//                            }
                            if((post_files.length != 1) && (page_type == 'setting')){
                                $('.settingBlockPost'+post_info['id']).hide();
                            }
                            if(post_files.length){
                                console.log(post_files.length);
                                //for(var keyFP in post_files){
                                    var pos_file = post_files[0];
                                    //$('.postFiles'+post_info['id']).append('<img src="../../../images/users_images/'+pos_file['file_src']+'" style="width:100px;">');
                                        if(pos_file['file_type'] == '0'){
                                            $('.postFiles'+post_info['id']).css('background-image', "url(../../../images/users_images/"+pos_file['file_src']+ ")" );
                                            $('.postFiles'+post_info['id']).css('width', "100%" );
                                            $('.postFiles'+post_info['id']).css('height', "289.72px" );
                                            $('.postFiles'+post_info['id']).css('background-repeat', "no-repeat");
                                            $('.postFiles'+post_info['id']).css('background-size', "cover");
                                            $('.postFiles'+post_info['id']).css('background-position', "center");
                                        }else{
                                            $('.postFiles'+post_info['id']).append(' '+
                                            '<video width="200" controls>'+
                                                '<source src="../../../images/users_images/'+pos_file['file_src']+'" >'+
                                                'Your browser does not support HTML5 video.'+
                                            '</video>')
                                        }
                                //}
                            }
                        //console.log(response);
                        var comment_info = comments['comment_info'];
                        if(comment_info){
                            var comment_files = comments['files'];
                            var commentAvatar = '<img src="../../../../images/default_avatar.jpg" style="width:100%;">';
                            if(comment_info['avatar'] != ''){
                                commentAvatar = '<img src="../../../../images/users_images/'+comment_info['avatar']+'" style="width:100%;">';
                            }
                            $('.comConte'+post_info['id']).append('<div class="commentContainer">'+
                                            '<div class="comment-block">'+
                                                '<div class="comment-header">'+
                                                    '<div class="author-image">'+
                                                        commentAvatar+
                                                    '</div>'+
                                                    '<div class="comment-header-info">'+
                                                        '<div class="comment-autor">'+
                                                            comment_info['name']+' '+comment_info['surname']+
                                                        '</div>'+
                                                        '<div class="comment-date">'+
                                                            comment_info['commentCreate']+
                                                        '</div>'+
                                                    '</div>'+
                                                '</div>'+
                                                '<div class="comment-text">'+
                                                    comment_info['content']+
                                                '</div>'+
                                                '<div class="comment-file commentFile'+comment_info['id']+'">'+
                                                '</div>'+
                                            '</div>'+
                                            '<hr class="comments-separator">'+
                                        '</div>');
                                if(comment_files.length){
                                    for(var keyF in comment_files){
                                        var com_file = comment_files[keyF];
                                        $('.commentFile'+comment_info['id']).append('<img src="../../../images/users_images/'+com_file['file_src']+'" style="width:100px;">');
                                    }
                                }
                        }
                    }
                }else{
                    $('.containerPost').html('<div class="not-found-post">This page does not have any post yet</div>')
                }
            }
        });
    }
    
    $(document).on('click','.postFilterButton',function(){
        var university_id = $(this).data('university_id');
        var filterType = $(this).data('filter_type');
        outputPostOnPage(university_id,filterType);
    });
    outputPostOnPage($('.universityInfoId').text(),'all');
    
    
    
    $(document).on('click','.clickLike',function(){
        var post_id = $(this).data('post_id');
        $.ajax({
            type: 'POST',
            url: '../../../university/default/postslikes',
            data: {post_id:post_id},
            dataType:'json',
            success: function(response){
                if(response.status == 'success'){
                    console.log(response.action)
                    $('.likePost'+post_id).find('span').text(response.like_count);
                }else{
                    console.lgo('error');
                }
            }
        });
    });
    
    
    
    function outputSliderPostOnPage(university_id){
        if(university_id != ''){
            
            $.ajax({
                type: 'POST',
                url: '../../../university/default/getsliderpost',
                data: {university_id:university_id},
                dataType:'json',
                success: function(response){
                    if(response.length){
                            for(var key in response){
                            var post = response[key];
                            
                            var avatarPost = '<img src="../../../../images/default_avatar.jpg" style="width:100%;position:relative;z-index:6;cursor:default;">';
                            if(post.avatar != ''){
                                avatarPost = '<img src="../../../../images/users_images/'+post.avatar+'" style="width:100%;position:relative;z-index:6;">';
                            }
                            
                            var avtorName = post.name+' '+post.surname;
                            if((post.id_user == 0) || (post.id_user == null)){
                                var avtorName = $('.universityInfoName').text();
                                avatarPost = '<img src="'+$('.universityInfoImage').text()+'" style="width:100%;position:relative;z-index:6;cursor:default;">';
                            }
                            
                                $('.previewSliderPost').prepend('<div class="col-sm-12 postSliderBlock'+post.id+'">'+
                                    '<div class="post-block" style="postion:relative;" data-post_id="'+post.id+'" >'+
                                        '<div style="position:absolute;z-index:5;width:100%;height:100%;cursor:pointer;" data-post_id="'+post.id+'" class="showPostClick">'+
                                        '</div>'+
                                        '<div class="post-header">'+
                                            '<div class="user-img">'+
                                                avatarPost+
                                            '</div>'+
                                            '<div class="post-header-info">'+
                                                '<h4 class="post-author">'+
                                                    avtorName+
                                                '</h4>'+
                                                '<h6 class="post-date">'+
                                                    post.postCreate+
                                                '</h6>'+
                                            '</div>'+
                                            '<div class="post-like-comment-count" style="position:relative;z-index:6;cursor:default">'+
                                                '<span class="count like-count clickLike likePost'+post.id+'" data-post_id="'+post.id+'">'+
                                                     '<img src="../../../images/icon/like-ic.png">'+
                                                    '<span>'+post.like_count+'</span>'+
                                                '</span>'+
                                                '<span class="count post-count">'+
                                                    '<img src="../../../images/icon/comment-ic.png">'+
                                                    post.comment_count+
                                                '</span>'+
                                            '</div>'+
                                            '<div class="clearfix"></div>'+
                                        '</div>'+
                                        '<div class="post-text">'+
                                            post.content+
                                        '</div>'+
                                        '<i class="deletePostWithSlider fa fa-times"></i>'+
                                    '</div>'+
                                '</div>');
                            }
                    }
                }
            });
        
        }
    }
    if($('.previewSliderPost').length){
        outputSliderPostOnPage($('.universityInfoId').text());
    }
    
    $(document).on('click', '.deletePostWithSlider',function(){
        var post_id = $(this).parent().data('post_id');
        $.ajax({
            type: 'POST',
            url: '../../../university/default/deletepostwithslider',
            data: {post_id:post_id},
            dataType:'json',
            success: function(response){
                if(response.status == 'save'){
                    $('.adminActionSetting'+post_id).html('<i class="addPostInSlider fa fa-check fa-2x"></i>');
                    $('.postSliderBlock'+post_id).remove();
                }
            }
        });
    });
    
    $(document).on('click', '.addPostInSlider',function(){
        var post_id = $(this).parent().data('post_id');
        if($('.previewSliderPost>div').length <= 7){
                $.ajax({
                    type: 'POST',
                    url: '../../../university/default/addpostinslider',
                    data: {post_id:post_id},
                    dataType:'json',
                    success: function(response){
                        if(response.status == 'save'){
                            $('.adminActionSetting'+post_id).html('<i class="deletePostWithSlider fa fa-times fa-2x"></i>');
                                    var post = response.post;
                                    var avatarPost = '<img src="../../../../images/default_avatar.jpg" style="width:100%;position:relative;z-index:6;cursor:default;">';
                                    if(post.avatar != ''){
                                        avatarPost = '<img src="../../../../images/users_images/'+post.avatar+'" style="width:100%;position:relative;z-index:6;">';
                                    }

                                    var avtorName = post.name+' '+post.surname;
                                    if((post.id_user == 0) || (post.id_user == null)){
                                        var avtorName = $('.universityInfoName').text();
                                        avatarPost = '<img src="'+$('.universityInfoImage').text()+'" style="width:100%;position:relative;z-index:6;cursor:default;">';
                                    }
                                    
                                        $('.previewSliderPost').prepend('<div class="col-sm-12 postSliderBlock'+post.id+'">'+
                                            '<div class="post-block" style="postion:relative;" data-post_id="'+post.id+'">'+
                                                '<div style="position:absolute;z-index:5;width:100%;height:100%;" data-post_id="'+post.id+'" class="showPostClick">'+
                                                '</div>'+
                                                '<div class="post-header">'+
                                                    '<div class="user-img">'+
                                                        avatarPost+
                                                    '</div>'+
                                                    '<div class="post-header-info">'+
                                                        '<h4 class="post-author">'+
                                                            avtorName+
                                                        '</h4>'+
                                                        '<h6 class="post-date">'+
                                                            post.postCreate+
                                                        '</h6>'+
                                                    '</div>'+
                                                    '<div class="post-like-comment-count">'+
                                                        '<span class="like-count clickLike likePost'+post.id+'" data-post_id="'+post.id+'">'+
                                                             '<img style="padding-right: 10px;" src="../../../images/icon/like-ic.png">'+
                                                            '<span>'+post.like_count+'</span>'+
                                                        '</span>'+
                                                        '<span class="post-count">'+
                                                            '<img style="padding-right: 7px; padding-left: 29px;" src="../../../images/icon/comment-ic.png">'+
                                                            post.comment_count+
                                                        '</span>'+
                                                    '</div>'+
                                                    '<div class="clearfix"></div>'+
                                                '</div>'+
                                                '<div class="post-text">'+
                                                    post.content+
                                                '</div>'+
                                                '<i style="position:absolute;top:-10px;right:0px;" class="deletePostWithSlider fa fa-times"></i>'+
                                            '</div>'+
                                        '</div>');
                        }
                    }
                });
        }else{
            alert('In a limited number of posts slider at 7')
        }
        
        
    });
    
    $(document).on('click','.addPostMyPhoto',function(){
        $('#modalPostMyFiles').modal('show');
        $('#modalPostMyFiles').find('.modalContent').html('');
        
        $.ajax({
            type: 'POST',
            url: '../../../university/default/getmyfiles',
            data: {type:'0'},
            dataType:'json',
            success: function(response){
                console.log(response);
                for(var key in response){
                    var img = response[key];
                    $('#modalPostMyFiles').find('.modalContent').append('<div><img class="img-responsive myImageToPost" data-action="not_add" data-src="'+img.image_name+'" src="../../../images/users_images/'+img.image_name+'"></div>');
                    $('.addMyFileToPost').data('type_action','photo')
                }
            }
        })
        
    });
    
    $(document).on('click','.myImageToPost',function(){
        var ThisElement = $(this);
        if(ThisElement.data('action') == 'not_add'){
           $(this).data('action','add') 
            ThisElement.parent().addClass('activeMyFileToPost');
        }else{
           $(this).data('action','not_add') 
            ThisElement.parent().removeClass('activeMyFileToPost');
           // ThisElement.parent().css('border','0px solid green');
        }
    });
     $(document).on('click','.addMyFileToPost',function(){
        if($(this).data('type_action') == 'photo'){
            $('#modalPostMyFiles').find('.activeMyFileToPost').find('img').each(function(){
                var imageSrc = $(this).data('src');
                if(typeof filesPostArray[imageSrc] == 'undefined'){
                    filesPostArray[imageSrc] = {};
                }

                filesPostArray[imageSrc] = 'image';
                $("[name=filesInput]").val(JSON.stringify(filesPostArray));
                console.log(filesPostArray);
                $('.previewFileBlock').append('' +
                        '<div id="previews" style="width:100px !important;margin:0px !important;float:left !important;padding:10px;">'+
                            '<div class="preview" style="position:relative;width:100%;">'+
                                '<img data-dz-thumbnail src="../../../images/users_images/'+imageSrc+'" style="width:100%;"/>'+
                                '<i class="fa fa-times deletePhotoWithAddForm" data-src="'+imageSrc+'" data-dz-remove style="position:absolute;top:-7px;right:-7px;font-size:25px;cursor:pointer;"></i>'+
                            '</div>'+
                        '</div>');
            })
        }else{
            $('#modalPostMyFiles').find('.activeMyFileToPost').find('video').each(function(){
                var imageSrc = $(this).data('src');
                if(typeof filesPostArray[imageSrc] == 'undefined'){
                    filesPostArray[imageSrc] = {};
                }

                filesPostArray[imageSrc] = 'video';
                $("[name=filesInput]").val(JSON.stringify(filesPostArray));
                console.log(filesPostArray);
                $('.previewFileBlock').append('' +
                        '<div id="previews" style="width:220px !important;margin:0px !important;float:left !important;padding:10px;">'+
                            '<div class="preview" style="position:relative;width:100%;">'+
                                '<video width="200" class="myVideoToPost" data-action="not_add" data-src="'+imageSrc+'" controls>'+
                                    '<source src="../../../images/users_images/'+imageSrc+'" type="video/mp4">'+
                                    'Your browser does not support HTML5 video.'+
                                '</video>'+
                                '<i class="fa fa-times deletePhotoWithAddForm" data-src="'+imageSrc+'" data-dz-remove style="position:absolute;top:-7px;right:-7px;font-size:25px;cursor:pointer;"></i>'+
                            '</div>'+
                        '</div>');
            })
        }
        $('#modalPostMyFiles').modal('hide');
        
    });
    $(document).on('click','.deletePhotoWithAddForm',function(){
        delete(filesPostArray[$(this).data('src')])
        $(this).parent().parent().remove();
        $("[name=filesInput]").val(JSON.stringify(filesPostArray));
    })
    
    
    
    
    $(document).on('click','.addPostMyVideo',function(){
        $('#modalPostMyFiles').modal('show');
        $('#modalPostMyFiles').find('.modalContent').html('');
        $.ajax({
            type: 'POST',
            url: '../../../university/default/getmyfiles',
            data: {type:'1'},
            dataType:'json',
            success: function(response){
                console.log(response);
                for(var key in response){
                    var video = response[key];
                    $('#modalPostMyFiles').find('.modalContent').append('<div style="width:220px;float:left;padding:10px;">'+
                            '<video width="200" class="myVideoToPost" data-action="not_add" data-src="'+video.image_name+'" controls>'+
                                '<source src="../../../images/users_images/'+video.image_name+'" type="video/mp4">'+
                                'Your browser does not support HTML5 video.'+
                            '</video>'+
                        '</div>');
                    $('.addMyFileToPost').data('type_action','video');
                }
            }
        });
    });
    
    $(document).on('click','.myVideoToPost',function(){
        var ThisElement = $(this);
        if(ThisElement.data('action') == 'not_add'){
           $(this).data('action','add') 
            ThisElement.parent().addClass('activeMyFileToPost');
        }else{
           $(this).data('action','not_add') 
            ThisElement.parent().removeClass('activeMyFileToPost');
        }
    });
    
    var filesPostUpdateArray = {};
    
    $(document).on('click','.deleteFileUpdatePhoto',function(){
        var file_src = $(this).data('file_src');
        delete(filesPostUpdateArray[file_src])
        $('#modalPostUpdate').find("[name=filesInput]").val(JSON.stringify(filesPostUpdateArray));
        $(this).parent().remove()
    });
    
    $(document).on('click','.postUpdate',function(){
        var post_id = $(this).data('post_id');
        $.ajax({
            type: 'POST',
            url: '../../../university/default/getpostinformation',
            data: {post_id:post_id},
            dataType:'json',
            success: function(response){
                    $('#modalPostUpdate').modal('show');
                        var postContent = response.post_info['content'];
                        $('[name=post_update_id]').val(response.post_info['id']);
                        $('#modalPostUpdate').find('.contentNews').val(postContent);
                        var filesArray = response.files;
                        $('#modalPostUpdate').find('.previewFilesBlock').html('');
                        filesPostUpdateArray = {};
                        
                        if(filesArray.length){
                            for(var key in filesArray){
                                if(typeof filesPostUpdateArray[filesArray[key]['file_src']] == 'undefined'){
                                    filesPostUpdateArray[filesArray[key]['file_src']] = {};
                                }
                                filesPostUpdateArray[filesArray[key]['file_src']] = filesArray[key]['file_type'];
                                if(filesArray[key]['file_type'] == '0'){
                                    $('#modalPostUpdate').find('.previewFilesBlock').append(
                                        '<div class="updateFiles">'+
                                            '<span class="deleteFileUpdatePhoto fa fa-times" data-file_src="'+filesArray[key]['file_src']+'"></span>'+
                                            '<img class="img-responsive" src="../../../images/users_images/'+filesArray[key]['file_src']+'" style="">'+
                                        '</div>'
                                    );
                                }else{
                                    $('#modalPostUpdate').find('.previewFilesBlock').append(' '+
                                    '<div style="position:relative;display: inline-block;"><video width="200" controls>'+
                                        '<source src="../../../images/users_images/'+filesArray[key]['file_src']+'" >'+
                                        'Your browser does not support HTML5 video.'+
                                    '</video>'+
                                    '<span class="deleteFileUpdatePhoto fa fa-times" data-file_src="'+filesArray[key]['file_src']+'" style="position:absolute;top:-5px;right:-5px;"></span>'+
                                    '</div>');
                                }
                            }
                            $('#modalPostUpdate').find("[name=filesInput]").val(JSON.stringify(filesPostUpdateArray));
                        }
                        
                        var previewNode = document.querySelector("#previewsUpdate");
                        var previewTemplate = previewNode.parentNode.innerHTML;
                        previewNode.parentNode.removeChild(previewNode);

                        var myDropzoneUpdate = new Dropzone($('.addFileUpdatePost')[0], {
                            url: '../../../../university/default/savedropedfile',
                            previewTemplate:previewTemplate,
                            clickable: ".addFileUpdatePost",
                            acceptedFiles:'image/*,video/*',
                            previewsContainer: ".previewFilesBlock",
                        });

                        myDropzoneUpdate.on("maxfilesexceeded", function(file) {
                                myDropzoneUpdate.removeAllFiles();
                                myDropzoneUpdate.addFile(file);

                        });

                        myDropzoneUpdate.on("complete", function(response) {
                            if (response.status == 'success'){
                                console.log(response.type);
                                var fileType = response.type;

                                if(typeof filesPostUpdateArray[response.xhr.response] == 'undefined'){
                                    filesPostUpdateArray[response.xhr.response] = {};
                                }

                                console.log(filesPostUpdateArray);
                                filesPostUpdateArray[response.xhr.response] = fileType;
                                $('#modalPostUpdate').find("[name=filesInput]").val(JSON.stringify(filesPostUpdateArray));
                            }
                        });

                        myDropzoneUpdate.on("removedfile", function(response) {
                            if(response.xhr != null){
                                var removeItem = response.xhr.response;

                                //console.log(filesArray);
                                delete(filesPostUpdateArray[removeItem])

                                console.log(filesPostUpdateArray);
            //                    filesArray.splice(filesArray.indexOf(removeItem), 1);
                                $('#modalPostUpdate').find("[name=filesInput]").val(JSON.stringify(filesPostUpdateArray));
                            }
                        });
            
                        
            }
        });
    });
    
    $(document).on('click','.SaveUpdatedPost',function(){
        var post_id = $('#modalPostUpdate').find('[name=post_update_id]').val();
        var newFileArray = $('#modalPostUpdate').find('[name=filesInput]').val();
        var contentNews = $('#modalPostUpdate').find('.contentNews').val();
        $.ajax({
            type: 'POST',
            url: '../../../university/default/saveupdatepost',
            data: {post_id:post_id,contentNews:contentNews,newFileArray:newFileArray},
            dataType:'json',
            success: function(response){
                if(response.status == 'update'){
                    $('#modalPostUpdate').modal('hide');
                }
            }
        });
        
    })
    
    $(document).on('click','.postDelete',function(){
        var post_id = $(this).data('post_id');
        swal({
            title: "Are you sure?",
            text: "Delete post!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
          },
          function(){
            $.ajax({
                type: 'POST',
                url: '../../../university/default/deletepost',
                data: {post_id:post_id},
                dataType:'json',
                success: function(response){
                    if(response.status == 'delete'){
                        $('.postBlockNumber'+post_id).remove();
                        swal("Deleted!", "", "success");
                    }else{
                        swal("Sorry but post not delete!", "")
                    }
                }
            });
          });
    });
    
    
    if($('.topClassContainer').length){
        getTopUsers();
    }
    
    function getTopUsers(){
        var university_id = $('.universityInfoId').text();
        $.ajax({
            type: 'POST',
            url: '../../../university/default/getclasstopuser',
            data: {university_id:university_id},
            beforeSend: function( xhr ) {
                $('.classSearchContainer').append('<div style="text-align:center;"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>');
            },
            dataType:'json',
            success: function(response){
                    $('.topClassContainer').html('')
                    var students_array = [];
                    for(var key in response){
                        var student = response[key];
                        students_array.push({info: student.user_info, rating: student.user_rating});
                    }

                    students_array.sort(function(a,b) {
                        return a.rating + b.rating;
                    });
                    var my_id = $('.message-count').find('.user').data('id');
                    for(var prof_key in students_array){
                        var info = students_array[prof_key].info;
                        console.log(info)
                        var rating = students_array[prof_key].rating;
                        var mail_button = '';
                        var avatar = 'default_avatar.jpg'
                        if(info.avatar){
                            avatar = 'users_images/'+info.avatar
                        }
                        if(my_id != info.id){
                            mail_button = '<button class="btn startChat startChatFriend" data-sender-id="'+my_id+'" data-recipient-id="'+info.id+'">mail to user</button>';
                        }
                        $('.topClassContainer').prepend('<table class="table class-table">'+
                                '<tr>'+
                                    '<td>'+
                                        '<div class="user-img">'+
                                            '<img style="width:40px;" class="" src="../../../images/'+avatar+'">'+
                                        '</div>'+
                                        '<span class="user-name">'+info.username+' '+info.surname+'</span>'+
                                    '</td>'+

                                    '<td>'+'<z style="position: relative; top: 5px; padding-right: 5px">'+'Rank:'+'</z>' +
                                        '<input id="studentInputRatingClass'+info.id+'" name="studentRating" value="'+rating+'" class="rating-loading">'+
                                    '</td>'+
                                    '<td>'+
                                        mail_button+
                                    '</td>'+
                                '</tr>'+
                            '</table>');
                        $('#studentInputRatingClass'+info.id).rating({displayOnly: true, step: 0.1, size:'xs'});
                    }
            }
        });
    }
    
    $(document).on('click', '.topProfessor', function(){
        $(this).parent().find('li').removeClass('active');
        $(this).addClass('active');
        getTopProfessor();
    });
    $(document).on('click', '.topStudents', function(){
        $(this).parent().find('li').removeClass('active');
        $(this).addClass('active');
        getTopUsers();
    });
    
    $(document).on('click', '.topUniversity', function(){
        $(this).parent().find('li').removeClass('active');
        $(this).addClass('active');
        getTopUniversity();
    });
    function getTopUniversity(){
        $.ajax({
            type: 'POST',
            url: '../../../university/default/gettopuniversity',
            beforeSend: function( xhr ) {
                $('.classSearchContainer').append('<div style="text-align:center;"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>');
            },
            data: {university_id:university_id},
            dataType:'json',
            success: function(response){
                $('.topClassContainer').html('')
                var university_array = [];
                for(var key in response){
                    var univer = response[key];
                    university_array.push({info: univer.university_info, rating: univer.university_rating});
                }
                university_array.sort(function(a,b) {
                    return a.rating + b.rating;
                });
                
                for(var university_key in university_array){
                    var info = university_array[university_key].info;
                    var rating = university_array[university_key].rating;
                    var view_button = '';
                    view_button = '';
                    $('.topClassContainer').prepend('<table class="table class-table">'+
                        '<tr>'+
                            '<td>'+
                                '<span>'+info.name+'</span>'+
                            '</td>'+
                            '<td>'+'<z style="position: relative; top: 5px; padding-right: 5px">'+'Rank:'+'</z>' +
                                '<input id="classInputRatingUniversity'+info.id+'" name="clRating" value="'+rating+'" class="rating-loading">'+
                            '</td>'+
                            '<td>'+
                                view_button+
                            '</td>'+
                        '</tr>'+
                    '</table>');
                    $('#classInputRatingUniversity'+info.id).rating({displayOnly: true, step: 0.1, size:'xs'});
                }
            }
        });
    }
    
    
    
    $(document).on('click', '.topMajor', function(){
        $(this).parent().find('li').removeClass('active');
        $(this).addClass('active');
        getTopMajor();
    });
    function getTopMajor(){
        var university_id = $('.universityInfoId').text();
        $.ajax({
            type: 'POST',
            url: '../../../university/default/gettopmajor',
            beforeSend: function( xhr ) {
                $('.classSearchContainer').append('<div style="text-align:center;"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>');
            },
            data: {university_id:university_id},
            dataType:'json',
            success: function(response){
                $('.topClassContainer').html('')
                var major_array = [];
                for(var key in response){
                    var maj = response[key];
                    major_array.push({info: maj.major_info, rating: maj.major_rating});
                }
                major_array.sort(function(a,b) {
                    return a.rating + b.rating;
                });
                
                for(var major_key in major_array){
                    var info = major_array[major_key].info;
                    var rating = major_array[major_key].rating;
                    var view_button = '';
                    view_button = '';
                    $('.topClassContainer').append('<table class="table class-table">'+
                        '<tr>'+
                            '<td>'+
                                '<span>'+info.name+'</span>'+
                            '</td>'+
                            '<td>'+'<z style="position: relative; top: 5px; padding-right: 5px">'+'Rank:'+'</z>' +
                                '<input id="classInputRatingMajor'+info.id+'" name="clRating" value="'+rating+'" class="rating-loading">'+
                            '</td>'+
                            '<td>'+
                                view_button+
                            '</td>'+
                        '</tr>'+
                    '</table>');
                    $('#classInputRatingMajor'+info.id).rating({displayOnly: true, step: 0.1, size:'xs'});
                }
            }
        });
    }



    function getTopProfessor(){
        var university_id = $('.universityInfoId').text();
        $.ajax({
            type: 'POST',
            url: '../../../university/default/getclasstopprofessor',
            beforeSend: function( xhr ) {
                $('.classSearchContainer').append('<div style="text-align:center;"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>');
            },
            data: {university_id:university_id},
            dataType:'json',
            success: function(response){
                $('.topClassContainer').html('')
                var professor_array = [];
                for(var key in response){
                    var professor = response[key];
                    professor_array.push({info: professor.professor_info, rating: professor.professor_rating});
                }
                
                professor_array.sort(function(a,b) {
                    return a.rating + b.rating;
                });
                var my_id = $('.message-count').find('.user').data('id');
                for(var prof_key in professor_array){
                    var info = professor_array[prof_key].info;
                    var rating = professor_array[prof_key].rating;
                    var mail_button = '';
                    var avatar = 'default_avatar.jpg'
                    if(info.avatar){
                        avatar = 'users_images/'+info.avatar
                    }
                    if(my_id != info.id){
                        mail_button = '<button class="btn startChat startChatFriend" data-sender-id="'+my_id+'" data-recipient-id="'+info.id+'">mail to user</button>';
                    }
                    $('.topClassContainer').append('<table class="table class-table">'+
                        '<tr>'+
                            '<td>'+
                                '<div class="user-img">'+
                                    '<img style="width:40px;" class="" src="../../../images/'+avatar+'">'+
                                '</div>'+
                                '<span class="user-name">'+info.username+' '+info.surname+'</span>'+
                                '</td>'+
                                '<td>'+'<z style="position: relative; top: 5px; padding-right: 5px">'+'Rank:'+'</z>' +
                                    '<input id="proffesorInputRatingClass'+info.id+'" name="profRating" value="'+rating+'" class="rating-loading">'+
                                '</td>'+
                                '<td>'+
                                    mail_button+
                                '</td>'+
                            '</tr>'+
                        '</table>');
                    $('#proffesorInputRatingClass'+info.id).rating({displayOnly: true, step: 0.1, size:'xs'});
//
                }
            }
        });
    }
    $(document).on('click','.avtorTypeChoise img',function(){
        $('.avtorType').show()
    });
    
    $(document).on('click','.avtorType img',function(){
        $('.avtorTypeChoise img').attr('src', $(this).attr('src'))
        $(this).parent().find('img').css('border', '1px solid transparent');
        $(this).css('border', '1px solid red');
        $('#post-avtor_type').val($(this).data('id'));
    })
    
    
    $(window).click(function() {
        $('.avtorType').hide();
    });
    $(document).on('click','.avtorTypeChoise,.avtorType',function(event){
       event.stopPropagation();
    });
    
});