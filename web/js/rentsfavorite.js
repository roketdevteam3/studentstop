$(document).ready(function(){
    
    $(document).on('click','.addToFavorite',function(){
        var renthouse_id = $(this).data('renthouse_id');
        var thisElement = $(this);
        $.ajax({
            type: 'POST',
            url: '../../addrenthousetofavorite',
            data: {renthouse_id:renthouse_id},
            dataType:'json',
            success: function(response){
                if(response.status == 'success'){
                    thisElement.removeClass('addToFavorite');
                    thisElement.addClass('deleteWithFavorite');
                    thisElement.find('.text').text('Delete with favorite');
                    swal("Add to favorite", "", "success");
                }
            } 
        });
    });

    $(document).on('click','#view',function(){
        var renthouse_id = $(this).data('renthouse_id');
        $('#view_rentshouse_'+renthouse_id).show();
        console.log('calendar', $('#calendar_w_'+renthouse_id));
        $('#calendar_w_'+renthouse_id).fullCalendar('today');

        $.get( '../../addviews/'+renthouse_id, function( data ) {});
    });

    $(document).on('click','#book',function(){
        var renthouse_id = $(this).data('renthouse_id');
        var renthouse_price_d = $(this).data('renthouse_price_d');
        var renthouse_price_c = $(this).data('renthouse_price_c');

        if (!renthouse_price_c || (renthouse_price_c == '0')){
            $('#credits_show').hide();
            $('#credits_show_button').hide();
        }

        $('#view_rentshouse_'+renthouse_id).hide();
        $('#form_rent_id').val(renthouse_id);
        $('#form_price_rent_d').text(renthouse_price_d*2);
        $('#price_d').val(renthouse_price_d);
        var date_for =  $('#form_date_for').val().split('-');
        var date_to =  $('#form_date_to').val().split('-');
        var day = Math.round((new Date(date_to[0], date_to[1], date_to[2]) - new Date(date_for[0], date_for[1], date_for[2]))/(1000*60*60*24));
        console.log('date_for',date_for);
        console.log('date_for',new Date(date_for[0], date_for[1], date_for[2]));
        console.log('date_to',date_to);
        console.log('date_to',new Date(date_to[0], date_to[1], date_to[2]));
        console.log('day_count',Math.round((new Date(date_to[0], date_to[1], date_to[2]) - new Date(date_for[0], date_for[1], date_for[2]))/(1000*60*60*24)))

        $('#form_price_rent_d').text(parseInt($('#price_d').val())*day);
        $('#rent_day').text(''+day);
        $('#day').val(day);
        $('#reserve').modal('show');
    });

    $(document).on('change','#house_type',function(){
        console.log('sdsd',$('#house_type').val())
        if ($('#house_type').val() == 'Shared') {
            $('#room_count_st').show();
          console.log('ok')
        } else {
            $('#room_count_st').hide();
          console.log('false')
        }
    });

    /*$(document).on('change','#form_date_for',function(){
        var date_for =  $('#form_date_for').val().split('-');
        var date_to =  $('#form_date_to').val().split('-');
        console.log('date_for',date_for);
        console.log('date_for',new Date(date_for[0], date_for[1], date_for[2]));
        console.log('date_to',date_to);
        console.log('date_to',new Date(date_to[0], date_to[1], date_to[2]));
        console.log('day_count',Math.round((new Date(date_to[0], date_to[1], date_to[2]) - new Date(date_for[0], date_for[1], date_for[2]))/(1000*60*60*24)))
        var day = Math.round((new Date(date_to[0], date_to[1], date_to[2]) - new Date(date_for[0], date_for[1], date_for[2]))/(1000*60*60*24));
        $('#form_price_rent_d').text(parseInt($('#price_d').val())*day);
        $('#rent_day').text(''+day);
        $('#day').val(day);
     });

    $(document).on('change','#form_date_to',function(){
        var date_for =  $('#form_date_for').val().split('-');
        var date_to =  $('#form_date_to').val().split('-');
        console.log('date_for',date_for);
        console.log('date_for',new Date(date_for[0], date_for[1], date_for[2]));
        console.log('date_to',date_to);
        console.log('date_to',new Date(date_to[0], date_to[1], date_to[2]));
        console.log('day_count',Math.round((new Date(date_to[0], date_to[1], date_to[2]) - new Date(date_for[0], date_for[1], date_for[2]))/(1000*60*60*24)))
        var day = Math.round((new Date(date_to[0], date_to[1], date_to[2]) - new Date(date_for[0], date_for[1], date_for[2]))/(1000*60*60*24));
        $('#form_price_rent_d').text(parseInt($('#price_d').val())*day);
        $('#rent_day').text(day);
        $('#day').val(day);
    });*/
    
    $(document).on('click','.deleteWithFavorite',function(){
        var renthouse_id = $(this).data('renthouse_id');
        var thisElement = $(this);
        $.ajax({
            type: 'POST',
            url: '../../deleterenthousewithfavorite',
            data: {renthouse_id:renthouse_id},
            dataType:'json',
            success: function(response){
                if(response.status == 'success'){
                    thisElement.removeClass('deleteWithFavorite');
                    thisElement.addClass('addToFavorite');
                    thisElement.find('.text').text('Add to favorite');
                    swal("Deleted with favorite", "", "success");
                }
            } 
        });
    });
    
});