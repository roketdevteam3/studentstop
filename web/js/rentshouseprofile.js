function initMapR(address){
    var myLatLng = {lat: 45.8152087, lng: -9.8816965};
    var my_location_lat;
    var my_location_lng;
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 2,
      center: myLatLng
    });
    console.log(address);
    if(address != ''){
        var geocoder = new google.maps.Geocoder();
        //var address = $(this).val();
            geocoder.geocode({'address': address}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                var Local = results[0].geometry.location;
                var latLng = {lat:parseFloat(Local.lat()),lng:parseFloat(Local.lng())};
                    my_location_lat = document.getElementById('location_lat');
                    my_location_lat.value = parseFloat(Local.lat());
                    my_location_lng = document.getElementById('location_lng');
                    my_location_lng.value = parseFloat(Local.lng());
                    
                    var address_r = document.getElementById('rentahouse_address');
                    
                    geocoder.geocode({'location': latLng}, function(results, status) {
                        if (status === 'OK') {
                          if (results[1]) {
                                address_r.value = results[1].formatted_address;
                                console.log(results[1]);
                          } else {
//                            window.alert('No results found');
                          }
                        } else {
  //                        window.alert('Geocoder failed due to: ' + status);
                        }
                      });
                    
                    
                  var marker = new google.maps.Marker({
                    position: latLng,
                    zoom:10,
                    map: map,
                 //   title: 'Hello World!'
                  });
                  map.setCenter(latLng);
                  map.setZoom(14);
            }else{
                //alert('error');
            }
        });
    }

//    var marker = new google.maps.Marker({
//      position: myLatLng,
//      map: map,
//      title: 'Hello World!'
//    });
}

function initMapEdit(address){
    var myLatLng = {lat: 45.8152087, lng: -9.8816965};
    var my_location_lat;
    var my_location_lng;
    var map = new google.maps.Map(document.getElementById('mapEdit'), {
      zoom: 2,
      center: myLatLng
    });
    
    if(address != ''){
        var geocoder = new google.maps.Geocoder();
        //var address = $(this).val();
            geocoder.geocode({'address': address}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                console.log(results[0].geometry.location);
                var Local = results[0].geometry.location;
                var latLng = {lat:parseFloat(Local.lat()),lng:parseFloat(Local.lng())};
                    my_location_lat = document.getElementById('location_lat_c');
                    my_location_lat.value = parseFloat(Local.lat());
                    my_location_lng = document.getElementById('location_lng_c');
                    my_location_lng.value = parseFloat(Local.lng());
                    
                    
                    var address_r = document.getElementById('rentahouse_address_c');
                    
                    geocoder.geocode({'location': latLng}, function(results, status) {
                        if (status === 'OK') {
                          if (results[1]) {
                                address_r.value = results[1].formatted_address;
                                console.log(results[1]);
                          } else {
//                            window.alert('No results found');
                          }
                        } else {
  //                        window.alert('Geocoder failed due to: ' + status);
                        }
                      });
                    
                    
                  var marker = new google.maps.Marker({
                    position: latLng,
                    zoom:10,
                    map: map,
                 //   title: 'Hello World!'
                  });
                  map.setCenter(latLng);
                  map.setZoom(14);
            }else{
                alert('error');
            }
        });
    }
    
    
//    var marker = new google.maps.Marker({
//      position: myLatLng,
//      map: map,
//      title: 'Hello World!'
//    });
}


    
$(document).ready(function(){
    //initMap();

    $("#addRent").on('shown.bs.modal', function(){
        initMapR('');
    });
//    $("#editRent").on('shown.bs.modal', function(){
//        initMapEdit(''); 
////        initMapR('');
//    });
    $(document).on('change','.enter_your_location',function(){
        console.log($(this).val());
        initMapR($(this).val()); 
    });
    $(document).on('change','.enter_your_location_c',function(){
        initMapEdit($(this).val()); 
    });
    $(document).on('click','[name=add_new_rents_2]',function(){
        $('[name=add_new_rents]').trigger('click');
    })
    
    $(document).on('click','[name=edit_new_rents_2]',function(){
        $('[name=edit_new_rents]').trigger('click');
    })
    
    function deleteFile(file_name){
            $.ajax({
                type: 'POST',
                url: '../../../../posts/posts/deletepostsphoto',
                data: {file_name:file_name},
                success: function(response){
                }
            });
        }
    
    if($('.add_header_photo').length){
            var previewNode = document.querySelector(".previewTemplateQ");
            previewNode.id = "";
            var previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);

            var myDropzoneA = new Dropzone($('.add_header_photo')[0], {
                maxFiles:1,
                uploadMultiple:false,
                url: "../../../posts/posts/savepostsphoto",
                previewTemplate:previewTemplate,
                clickable: ".add_header_photo",
                previewsContainer: ".show_header_photo",
            });

            myDropzoneA.on("maxfilesexceeded", function(file) {
                    myDropzoneA.removeAllFiles();
                    myDropzoneA.addFile(file);

            });

            myDropzoneA.on("complete", function(response) {
                if (response.status == 'success') {
                    $('#rentshouse-img_src').val(response.xhr.response);
                    $('#rents-img_src').val(response.xhr.response);
                    //$('.eventButton').show();
                }
            });

            myDropzoneA.on("removedfile", function(response) {
                if(response.xhr != null){
                   deleteFile(response.xhr.response);
                }
            });
        }
        
    if($('.change_header_photo').length){
            var previewNode = document.querySelector(".previewTemplateC");
            previewNode.id = "";
            var previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);

            var myDropzoneC = new Dropzone($('.change_header_photo')[0], {
                maxFiles:1,
                uploadMultiple:false,
                url: "../../../posts/posts/savepostsphoto",
                previewTemplate:previewTemplate,
                clickable: ".change_header_photo",
                previewsContainer: ".show_change_header_photo",
            });

            myDropzoneC.on("maxfilesexceeded", function(file) {
                    myDropzoneC.removeAllFiles();
                    myDropzoneC.addFile(file);
            });

            myDropzoneC.on("complete", function(response) {
                $('.show_change_header_photo').find('.previewImgC').remove();
                if (response.status == 'success') {
                    $('#editRent').find('#rentshouse-img_src').val(response.xhr.response);
                    $('#editRent').find('#rents-img_src').val(response.xhr.response);
                }
            });

            myDropzoneC.on("removedfile", function(response) {
                if(response.xhr != null){
                   deleteFile(response.xhr.response);
                }
            });
        }
        
        
        
        
        var photosArray = [];
        
        var myDropzone = new Dropzone($('.user_photos')[0], { // Make the whole body a dropzone
            maxFilesize: 50,
        });

        myDropzone.on("sending", function(file) {
            console.log(file);
          //  file.name = file.name+Math.random().toString(36).substring(7); 
          // Hookup the start button
          //file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file); };
        });
        // Update the total progress bar

        myDropzone.on("complete", function(response) {
            console.log(response);
            if (response.status == 'success') {
                //console.log(response);
                $(response.previewElement).prepend('<i class="fa fa-times removePhoto fa-2x" title="delete" style="color:#DBDBDB;cursor:pointer;right:-5px;top:-10px;position:absolute;z-index:999" data-name="'+response.name+'"></i>');
                photosArray.push(response.xhr.response);
                $('#inputPhotos').html('');
                $('#inputPhotos').append("<input type='hidden' name='photosInput' value='"+JSON.stringify(photosArray)+"'> ");
                        //console.log(response.xhr.response)
                //$('#formAddrenthouse').show();
            }
        });
        myDropzone.on("removedfile", function(response) {
          //alert('dvdv');
            //console.log();
            var removeItem = response.xhr.response;

            photosArray = jQuery.grep(photosArray, function(value) {
              return value != removeItem;
            });
            $('#inputPhotos').html('');
            $('#inputPhotos').append("<input type='hidden' name='photosInput' value='"+JSON.stringify(photosArray)+"'> ");
            deleteFile(response.xhr.response);
//            if(myDropzone.files.length == 0){
//                $('#formSavePhotos').hide();
//            }
        });

        $(document).on('click','.removePhoto',function(){
            var shos = $(this).parent();

            var name_photo = $(this).data('name');
            $.each( myDropzone.files, function( key, value ) {
                if(value.name == name_photo){
                    myDropzone.removeFile(myDropzone.files[key]);
                }
            });
        });
    
        var photosArrayChange = [];
    
    
        $(document).on('click','.deletePhotoRentEdit',function(){
            var removeItem = $(this).data('img_src');
            photosArrayChange = jQuery.grep(photosArrayChange, function(value) {
              return value != removeItem;
            });
            $(this).parent().remove();
            $('#inputPhotosC').html('');
            $('#inputPhotosC').append("<input type='hidden' name='photosInputC' value='"+JSON.stringify(photosArrayChange)+"'> ");
            
        })
        
        $(document).on('click','.editRent',function(){
            //alert('dfdfd');
            $('#editRent').modal('show')
            var rent_id = $(this).data('rent_id');
             $.ajax({
                type: 'POST',
                url: '../../../../posts/rentshouse/getrentdata',
                data: {rent_id:rent_id},
                dataType: 'json',
                success: function(response){
                    
                    if(response.status != 'error'){
                        console.log(response.rent_data);
                        $('#editRent').find('#rentshouse-title').val(response.rent_data['title']);
                        $('#editRent').find('#rentshouse-id').val(response.rent_data['id']);
                        $('#editRent').find('#rentshouse-address').val(response.rent_data['address']);
                        
                        $('#editRent').find('#rentshouse-adults').val(response.rent_data['adults']); 
                            
                        if(response.rent_data['animal'] == 1){
                            $('#editRent').find('#rentshouse-animal').val(response.rent_data['animal']);
                            $('#editRent').find('#rentshouse-animal').attr('checked',true);
                        }else{
                            $('#editRent').find('#rentshouse-animal').attr('checked',false);                            
                        }
                        if(response.rent_data['conditioner'] == 1){
                            $('#editRent').find('#rentshouse-conditioner').val(response.rent_data['conditioner']);
                            $('#editRent').find('#rentshouse-conditioner').attr('checked',true);
                        }else{
                            $('#editRent').find('#rentshouse-conditioner').attr('checked',false);                            
                        }
                        if(response.rent_data['parking'] == 1){
                            $('#editRent').find('#rentshouse-parking').val(response.rent_data['parking']);
                            $('#editRent').find('#rentshouse-parking').attr('checked',true);
                        }else{
                            $('#editRent').find('#rentshouse-parking').attr('checked',false);
                        }
                        if(response.rent_data['smoking'] == 1){
                            $('#editRent').find('#rentshouse-smoking').val(response.rent_data['smoking']);
                            $('#editRent').find('#rentshouse-smoking').attr('checked',true);
                        }else{
                            $('#editRent').find('#rentshouse-smoking').attr('checked',false);
                        }
                        if(response.rent_data['tv'] == 1){
                            $('#editRent').find('#rentshouse-tv').val(response.rent_data['tv']);
                            $('#editRent').find('#rentshouse-tv').attr('checked',true);
                        }else{
                            $('#editRent').find('#rentshouse-tv').attr('checked',false);
                        }
                        if(response.rent_data['wifi'] == 1){
                            $('#editRent').find('#rentshouse-wifi').val(response.rent_data['wifi']);
                            $('#editRent').find('#rentshouse-wifi').attr('checked',true);
                        }else{
                            $('#editRent').find('#rentshouse-wifi').attr('checked',false);
                        }
                        
                        $('#editRent').find('#rentshouse-room_count').val(response.rent_data['room_count']);
                        $('#editRent').find('#rentshouse-city').val(response.rent_data['city']);
                        $('#editRent').find('#rentshouse-content').val(response.rent_data['content']);
                        $('#editRent').find('#rentshouse-credits').val(response.rent_data['credits']);
                        $('#editRent').find('#rentshouse-house_type').val(response.rent_data['house_type']);
                        $('#editRent').find('#rentshouse-img_src').val(response.rent_data['img_src']);
                        $('#editRent').find('#rentshouse-instant_pay').val(response.rent_data['instant_pay']);
                        $('#editRent').find('#rentshouse-position_lat').val(response.rent_data['position_lat']);
                        $('#editRent').find('#rentshouse-position_lng').val(response.rent_data['position_lng']);
                        $('#editRent').find('#rentshouse-price').val(response.rent_data['price']);
                        $('#editRent').find('#rentshouse-price_for').val(response.rent_data['price_for']);
                        $('#editRent').find('#rentshouse-university_id').val(response.rent_data['university_id']);
                        $('#editRent').find('#rentshouse-user_id').val(response.rent_data['user_id']);
                        $('#editRent').find('.show_change_header_photo').append('<div class="previewImgC"><img src="/images/posts_images/'+response.rent_data['img_src']+'"></div>');
//                        initMapEdit(response.rent_data['address'])
                        
                            initMapEdit(response.rent_data['address'])                            
                        
                        $('.enter_your_location_c').val(response.rent_data['address']);
                        $('.previewsPhotosRenthouse').html('');
                        photosArrayChange = [];
                        $('#inputPhotosC').html('');
                        $('#inputPhotosC').append("<input type='hidden' name='photosInputC'>");
                        var rent_image = response.rent_img;
                        for(var key in rent_image){
                            if(rent_image[key].image_src != ''){
                                photosArrayChange.push(rent_image[key].image_src);
                                $('input[name=photosInputC]').val(JSON.stringify(photosArrayChange));
                                
                                $('.previewsPhotosRenthouse').append('<div style="width:100px;float:left;position:relative;">\n\
                                    <img style="width:100px;height:100px;" src="/images/users_images/'+rent_image[key].image_src+'">\n\
                                    <i class="fa fa-times deletePhotoRentEdit" data-img_src="'+rent_image[key].image_src+'" style="position:absolute;right:-5px;top:-5px;"></i>\n\
                                </div>');                                
                            }
                        }
                        
//                        $('.previewsPhotosRenthouse')
                    }else{
                       // alert('error');
                    }
                }
            });
        })
    
    
    
        
        var myDropzone = new Dropzone($('.user_photosC')[0], { // Make the whole body a dropzone
            maxFilesize: 50,
        });

        myDropzone.on("sending", function(file) {
        });

        myDropzone.on("complete", function(response) {
            console.log(response);
            if (response.status == 'success') {
                //console.log(response);
                $(response.previewElement).prepend('<i class="fa fa-times removePhotoC fa-2x" title="delete" style="color:#DBDBDB;cursor:pointer;right:-5px;top:-10px;position:absolute;z-index:999" data-name="'+response.name+'"></i>');
                photosArrayChange.push(response.xhr.response);
                $('#inputPhotosC').html('');
                $('#inputPhotosC').append("<input type='hidden' name='photosInputC' value='"+JSON.stringify(photosArrayChange)+"'> ");
            }
        });
        myDropzone.on("removedfile", function(response) {
            var removeItem = response.xhr.response;

            photosArrayChange = jQuery.grep(photosArrayChange, function(value) {
              return value != removeItem;
            });
            $('#inputPhotosC').html('');
            $('#inputPhotosC').append("<input type='hidden' name='photosInputC' value='"+JSON.stringify(photosArrayChange)+"'> ");
            deleteFile(response.xhr.response);
        });
    
});        