$(document).ready(function(){
        if($('.imageConference').length){
            var previewNode = document.querySelector(".previewTemplateQ");
            previewNode.id = "";
            var previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);

            var myDropzoneA = new Dropzone($('.imageConference')[0], {
                maxFiles:1,
                uploadMultiple:false,
                url: "../../../course/classes/savedropedfile",
                previewTemplate:previewTemplate,
                clickable: ".newImageConference",
                previewsContainer: ".imageConference",
            });

            myDropzoneA.on("maxfilesexceeded", function(file) {
                    myDropzoneA.removeAllFiles();
                    myDropzoneA.addFile(file);

            });

            myDropzoneA.on("complete", function(response) {
                if (response.status == 'success') {
                    $('#videoconference-img_src').val(response.xhr.response);
                    $('.imageConference').attr('src', '../../../../images/classes/'+response.xhr.response);
                }
            });

            myDropzoneA.on("removedfile", function(response) {
                if(response.xhr != null){
                   //deleteFile(response.xhr.response);
                }
            });
          }
          
          
          
          
          
          
          var userArray = [];
          function getUserInfo(value){
                if(value.length >= 2){
                    $.ajax({
                        type: 'POST',
                        dataType: "json",
                        url: '../../../users/default/getfilternameusers',
                        data: {value:value},
                        success: function(response){
                            $('.choiseUserBlock').html('');
                            for(var key in response){
                                var user = response[key];
                                var avatar = '<img src="../../../images/default_avatar.jpg" style="width:40px;">';
                                if((user.avatar != '') && (user.avatar != null)){
                                    avatar = '<img src="../../../images/users_images/'+user.avatar+'" style="width:40px;">';
                                }
                                $('.choiseUserBlock').append('<li data-user_id="'+user.id+'" >'+avatar+' '+user.name+' '+user.surname+'</li>');
                            }
                        }
                    });
                }else{
                    $('.choiseUserBlock').html('')
                }
          }
          
            $(document).on('keyup','#videoconference-choise_user',function(){
                var value = $(this).val();
                getUserInfo(value);
            });
            
            $(document).on('click','#videoconference-choise_user',function(){
                var value = $(this).val();
                getUserInfo(value);
            });
            
            $(document).on('click', '.choiseUserBlock li', function(){
                var user_info = $(this).html()
                var user_id = $(this).data('user_id');
                
                if($('.userInfoBlock'+user_id).length){
                }else{
                    userArray.push(user_id);
                    $('#choise_user_array').val(JSON.stringify(userArray))
                    $('.tagsContainer').append('<div data-user_id="'+user_id+'" class="userInfoBlock'+user_id+'" style="display:inline-block;background-color:blue;float:left;margin:2px;border-radius:5px;">'+
                            user_info+ ' <i data-user_id="'+user_id+'" class="fa fa-times deleteUser"></i>' +
                    '</div>')
                }
                
                $('.choiseUserBlock').html('')
            })
            
            $(document).on('change','#videoconference-choise_user',function(){
//                $('.choiseUserBlock').html('')
            })
            $(document).on('click','.deleteUser',function(){
                var user_id = $(this).data('user_id');
                userArray = jQuery.grep(userArray, function(value) {
                  return value != user_id;
                });
                $('.userInfoBlock'+user_id).remove();
                $('#choise_user_array').val(JSON.stringify(userArray))
            })



            $(document).on('click','#videoconference-status',function(){
                if($(this).val() == '1'){
                    $('.classClosedBlock').show()
                }else{
                    $('.classClosedBlock').hide();
                    $('#choise_user_array').val('');
                    $('.tagsContainer').html('');
                    userArray = [];
                }
            });
            
});
