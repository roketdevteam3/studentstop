$(document).ready(function(){
    
    if($('.topClassContainer').length){
        getTopUsers();
    }
    
    function getTopUsers(){
        var videoconference_id = $('[name=vitrual_class_id]').val();
        $.ajax({
            type: 'POST',
            url: '../../../course/info/getclasstopuser',
            data: {videoconference_id:videoconference_id},
            beforeSend: function( xhr ) {
                $('.topClassContainer').append('<div style="text-align:center;"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>');
            },
            dataType:'json',
            success: function(response){
                    $('.topClassContainer').html('')
                    var students_array = [];
                    for(var key in response){
                        var student = response[key];
                        students_array.push({info: student.user_info, rating: student.user_rating});
                    }

                    students_array.sort(function(a,b) {
                        return a.rating + b.rating;
                    });
                    var my_id = $('.message-count').find('.user').data('id');
                    for(var prof_key in students_array){
                        var info = students_array[prof_key].info;
                        var rating = students_array[prof_key].rating;
                        var mail_button = '';
                        if(my_id != info.id){
                            mail_button = '<button class="btn startChat startChatFriend" data-sender-id="'+my_id+'" data-recipient-id="'+info.id+'">mail to user</button>';
                        }
                        $('.topClassContainer').prepend('<table class="table class-table">'+
                                '<tr>'+
                                    '<td>'+
                                        '<div class="user-img">'+
                                            '<img class="" src="../../../images/users_images/'+info.avatar+'" style="width:40px;">'+
                                        '</div>'+
                                        '<span class="user-name">'+info.username+' '+info.surname+'</span>'+
                                    '</td>'+
                                    '<td>'+
                                        '<input id="studentInputRatingClass'+info.id+'" name="studentRating" value="'+rating+'" class="rating-loading">'+
                                    '</td>'+
                                    '<td>'+
                                        mail_button+
                                    '</td>'+
                                '</tr>'+
                            '</table>');
                        $('#studentInputRatingClass'+info.id).rating({displayOnly: true, step: 0.1, size:'xs'});
                    }
            }
        });
    }
    
    $(document).on('click', '.topProfessor', function(){
        $(this).parent().find('li').removeClass('active');
        $(this).addClass('active');
        getTopProfessor();
    });
    $(document).on('click', '.topStudents', function(){
        $(this).parent().find('li').removeClass('active');
        $(this).addClass('active');
        getTopUsers();
    });
    
    
    function getTopProfessor(){
        var videoconference_id = $('[name=vitrual_class_id]').val();
        $.ajax({
            type: 'POST',
            url: '../../../course/info/getclasstopprofessor',
            data: {videoconference_id:videoconference_id},
            beforeSend: function( xhr ) {
                $('.topClassContainer').append('<div style="text-align:center;"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>');
            },
            dataType:'json',
            success: function(response){
                $('.topClassContainer').html('')
                var professor_array = [];
                for(var key in response){
                    var professor = response[key];
                    professor_array.push({info: professor.professor_info, rating: professor.professor_rating});
                }
                
                professor_array.sort(function(a,b) {
                    return a.rating + b.rating;
                });
                var my_id = $('.message-count').find('.user').data('id');
                for(var prof_key in professor_array){
                    var info = professor_array[prof_key].info;
                    var rating = professor_array[prof_key].rating;
                    var mail_button = '';
                    if(my_id != info.id){
                        mail_button = '<button class="btn startChat startChatFriend" data-sender-id="'+my_id+'" data-recipient-id="'+info.id+'">mail to user</button>';
                    }
                    $('.topClassContainer').append('<table class="table class-table">'+
                        '<tr>'+
                            '<td>'+
                                '<div class="user-img">'+
                                    '<img src="../../../images/users_images/'+info.avatar+'" style="width:40px">'+
                                '</div>'+
                                '<span class="user-name">'+info.username+' '+info.surname+'</span>'+
                                '</td>'+
                                '<td>'+
                                    '<input id="proffesorInputRatingClass'+info.id+'" name="profRating" value="'+rating+'" class="rating-loading">'+
                                '</td>'+
                                '<td>'+
                                    mail_button+
                                '</td>'+
                            '</tr>'+
                        '</table>');
                    $('#proffesorInputRatingClass'+info.id).rating({displayOnly: true, step: 0.1, size:'xs'});
//            
                }
            }
        });
    }
    
    
    
    $(document).on('click', '.topClass', function(){
        $(this).parent().find('li').removeClass('active');
        $(this).addClass('active');
        getTopClass();
    });
    
    function getTopClass(){
        var videoconference_id = $('[name=vitrual_class_id]').val();
        $.ajax({
            type: 'POST',
            url: '../../../course/info/getclasstop',
            data: {videoconference_id:videoconference_id},
            beforeSend: function( xhr ) {
                $('.topClassContainer').append('<div style="text-align:center;"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>');
            },
            dataType:'json',
            success: function(response){
                $('.topClassContainer').html('')
                var class_array = [];
                for(var key in response){
                    var clas = response[key];
                    class_array.push({info: clas.class_info, rating: clas.class_rating});
                }
//                
                class_array.sort(function(a,b) {
                    return a.rating + b.rating;
                });
                console.log('------');
                console.log(class_array);
                console.log('------');
                for(var class_key in class_array){
                    var info = class_array[class_key].info;
                    var rating = class_array[class_key].rating;
                    var view_button = '';
                    view_button = '';
                    $('.topClassContainer').append('<table class="table class-table">'+
                        '<tr>'+
                            '<td>'+
                                '<span>'+info.name+'</span>'+
                            '</td>'+
                            '<td>'+
                                '<input id="classInputRatingClass'+info.id+'" name="clRating" value="'+rating+'" class="rating-loading">'+
                            '</td>'+
                            '<td>'+
                                view_button+
                            '</td>'+
                        '</tr>'+
                    '</table>');
                    $('#classInputRatingClass'+info.id).rating({displayOnly: true, step: 0.1, size:'xs'});
                }
            }
        });
    }
    
    
    
    
    
    
    
    
    
    
    
    if($('.virtualClassInfoContainer').length){
        getVitrualClassUsers();
    }
    
    $(document).on('click','.virtualclassStudentShow',function(e){
        e.preventDefault();
        $(this).parent().find('li').removeClass('active');
        $(this).addClass('active');
        getVitrualClassUsers();
    })
    $(document).on('click','.virtualclassInfoShow',function(e){
        e.preventDefault();
        $(this).parent().find('li').removeClass('active');
        $(this).addClass('active');
        getVitrualClassInfo();
    })
    
    
    function getVitrualClassInfo(){
        var videoconference_id = $('[name=vitrual_class_id]').val();
        $.ajax({
            url:'../../../../course/info/getvitrualclassinfo',
            method:'POST',
            data:{videoconference_id:videoconference_id},
            dataType:'json',
            beforeSend:function(){
            },
            success:function(response){
                $('.virtualClassInfoContainer').html('');
                $('.virtualClassInfoContainer').append('<div class="">'+
                        response.description+
                        '</div>');
                
            }
        })
    }
    
    function getVitrualClassUsers(){
        var videoconference_id = $('[name=vitrual_class_id]').val();
        $.ajax({
            url:'../../../../course/info/getvitrualclassusers',
            method:'POST',
            data:{videoconference_id:videoconference_id},
            dataType:'json',
            beforeSend:function(){
            //    thisButton.prop('disabled', true);
            },
            success:function(response){
                $('.virtualClassInfoContainer').html('');
                for(var key in response){
                    var userInfo = response[key];
                    var myId = $('.nav-user.message-count').find('.user').data('id');
                    var avatar = '../../../images/default_avatar.jpg';
                    if(userInfo != ''){
                        avatar = '<img src="../../../images/users_images/'+userInfo['avatar']+'">';
                    }
                    $('.virtualClassInfoContainer').append('<div class="startChat startChatFriend" data-sender-id="'+myId+'" data-recipient-id="'+userInfo['id']+'">'+
                        '<table class="table">'+
                            '<tr>'+
                                '<td>'+
                                    '<div class="user-img">'+
                                        avatar+
                                    '</div>'+
                                    '<span class="user-name">'+userInfo['name']+' '+userInfo['surname']+'</span>'+
                                '</td>'+
                                '<td>'+
                                    '<p class="user-role">('+userInfo['user_role_name']+')</p>'+
                                '</td>'+
                            '</tr'+
                        '</table>'+
                    '</div>');
                }
            }
        });
    }
   
   
   
    $(document).on('change','[name=university_class]',function(){
        var university_id = $(this).val();
        $.ajax({
            url:'../../../../getmajorsarray',
            method:'POST',
            data:{university_id:university_id},
            dataType:'json',
            success:function(response){
                $('[name=major_class]').html('');
                $('[name=major_class]').append('<option value="0">Major</option>');
                
                $('[name=course_class]').html('');
                $('[name=course_class]').append('<option value="0">Course</option>');
                
                $('[name=class_class]').html('');
                $('[name=class_class]').append('<option value="0">Class</option>');
                
                for(var key in response){
                    $('[name=major_class]').append('<option value='+key+'>'+response[key]+'</option>');
                }
            }
        });
    });
    
    $(document).on('change','[name=major_class]',function(){
        var major_id = $(this).val();
        $.ajax({
            url:'../../../../getcoursearray',
            method:'POST',
            data:{major_id:major_id},
            dataType:'json',
            success:function(response){
                $('[name=course_class]').html('');
                $('[name=course_class]').append('<option value="0">Course</option>');
                
                $('[name=class_class]').html('');
                $('[name=class_class]').append('<option value="0">Class</option>');
                
                for(var key in response){
                    $('[name=course_class]').append('<option value='+key+'>'+response[key]+'</option>');
                }
            }
        });
    });
    
    $(document).on('change','[name=course_class]',function(){
        var course_id = $(this).val();
        $.ajax({
            url:'../../../../getclassarray',
            method:'POST',
            data:{course_id:course_id},
            dataType:'json',
            success:function(response){
                $('[name=class_class]').html('');
                $('[name=class_class]').append('<option value="0">Class</option>');
                for(var key in response){
                    $('[name=class_class]').append('<option value='+key+'>'+response[key]+'</option>');
                }
            }
        });
    });
    
    $(document).on('click','.InviteConference',function(){
        var user_id = $(this).data('user_id');
        var conference_id = $(this).data('conference_id');
        var thisElement = $(this);
        $.ajax({
            url:'../../../../conferenceinvite',
            method:'POST',
            data:{user_id:user_id, conference_id:conference_id},
            dataType:'json',
            success:function(response){
                if(response.status == 'success'){
                    thisElement.remove();
                }
            }
        });
    });
    
    
    
    if($('.showClassEnded').length){
        swal({
            title: "Your class is over!",
            text: "<p style='color:white;'>If you want to keep this class for another semester, please click 'stay in class'</p>",
            type: "warning",
            html: true,
            showCancelButton: true,
            confirmButtonText: "Keep this class",
            cancelButtonText: "Graduated",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function(isConfirm) {
            var class_id = $('[name=this_class_id]').val();
            if(isConfirm) {
                changeFollowStatus(class_id, 'confirm')
            }else{
                changeFollowStatus(class_id, 'cancel');
            }
        });
    }
    
    function changeFollowStatus(class_id,status){
        $.ajax({
            type: 'POST',
            url: '../../../course/info/changefollowstatus',
            data: {class_id:class_id, status:status},
            dataType:'json',
            success: function(response){
                if(response.status == 'success'){
                    $('#modalRating').modal('show');
                }
            }
        });
    }
    
    $(document).on('click', '.closeConference',function(){
        var videoclass_id = $(this).data('videoclass_id');
        alert(videoclass_id);
        swal({
            title: "Are you sure?",
            text: "You really want to close this class?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, close it!",
            closeOnConfirm: false
        },
        function(){
            $.ajax({
                type: 'POST',
                url: '../../../course/info/closevideoclass',
                data: {videoclass_id:videoclass_id},
                dataType:'json',
                success: function(response){
                    swal({title:"Closed!",
                        text:"",
                        type:'success',
                        confirmButtonText: "Ok",
                    },function(){
                        window.location.reload();
                    });
                    
                }
            })
        });
        
    
    });
});