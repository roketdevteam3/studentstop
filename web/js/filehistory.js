$(document).ready(function(){
    $(document).on('click','.fileImageShow',function(){
        $('#myModalImageFile').modal('show');
        $('#myModalImageFile').find('img').attr('src',$(this).attr('src'));
    });
    
    $(document).on('click','.getMembers',function(){
        var type = $(this).data('type')
         $.ajax({
            type: 'POST',
            url: '../../../filehistory/files/getuserforsort',
            data: {type:type},
            dataType:'json',
        }).done(function(response){
            $('.userFilesChoise').show();
            if($('.userFilesChoise').html() == ''){
                $('.userFilesChoise').html('');
                $('.userFilesChoise').append('<a href="?" style="text-align:left;display:block;">All user</a>');
                for(var key in response){
                    var user = response[key];
                    if(user.id == $('.userFilesChoisen').text()){
                        $('.userFilesChoise').append('<a href="?sort=file_user&user_id='+user.id+'" style="background-color:red;text-align:left;display:block;">'+user.name+' '+user.surname+'</a>')
                    }else{
                        $('.userFilesChoise').append('<a href="?sort=file_user&user_id='+user.id+'" style="text-align:left;display:block;">'+user.name+' '+user.surname+'</a>')
                    }
                }
            }
        });
    });
    
    $(document).on('click','.getClass',function(){
        var type = $(this).data('type')
         $.ajax({
            type: 'POST',
            url: '../../../filehistory/files/getclassforsort',
            data: {type:type},
            dataType:'json',
        }).done(function(response){
            $('.classFilesChoise').show();
            if($('.userFilesChoise').html() == ''){
                $('.classFilesChoise').html('');
                $('.classFilesChoise').append('<a href="?" style="text-align:left;display:block;">All class</a>');
                for(var key in response){
                    var clas = response[key];
                    if(clas.id == $('.classFilesChoisen').text()){
                        $('.classFilesChoise').append('<a href="?sort=file_class&class_id='+clas.id+'" style="background-color:red;text-align:left;display:block;">'+clas.name+'</a>')
                    }else{
                        $('.classFilesChoise').append('<a href="?sort=file_class&class_id='+clas.id+'" style="text-align:left;display:block;">'+clas.name+'</a>')
                    }
                }
            }
        });
    });
    
});