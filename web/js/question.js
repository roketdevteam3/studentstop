$(document).ready(function(){
    function changeCategory(category_id){
        $.ajax({
            type: 'POST',
            url: '../../getchildqusetioncategory',
            data: {category_id:category_id},
            dataType:'json',
            success: function(response){
                $('#question-category_id').html('');
                for(var key in response){
                    var obje = response[key];
                    for(var key2 in obje){
                        $('#question-category_id').append('<option value="'+key2+'">'+key+'</option>');
                    }
                }
            }
        });
    }
    
    if($('#question-category').length){
        var categoryParentId = $('#question-category').val();
        changeCategory(categoryParentId);
    }
    
    
    $(document).on('change','#question-category',function(){
        var categoryParentId = $('#question-category').val();
        changeCategory(categoryParentId);
    });
    
    $(document).on('click','.parentCategory',function(e){
        e.preventDefault();
        var question_id =  $(this).data('question_id');
        var thisElement = $(this).parent();
        thisElement.find('.childCategory').toggle();
        $.ajax({
            type: 'POST',
            url: '../../getchildqusetioncategory',
            data: {category_id:question_id},
            dataType:'json',
            success: function(response){
                //console.log(response);
                $('.childCategory').html('');
                for(var key in response){
                    var obje = response[key];
                    for(var key2 in obje){
                        thisElement.find('.childCategory').append('<li value="'+key2+'"><a href="../../../question_category/'+obje[key2]+'">'+key+'</a></li>');
                    }
                }
            }
        });
        
    });
    
    $(document).on('click','.questionedit',function(){
    //    alert($(this).data('question_id'));
        $('#questionUpdateModal').modal('show');
    });
    
    $(document).on('click','.answeredit',function(){
        $('.answer_id').val($(this).data('answer_id'));
        $('.money').val($(this).data('money'));
        $('.answer_text').text($(this).data('answer_text'));
        $('.show_text').text($(this).data('show_text'));
        $('#answerUpdateModal').modal('show');
    });
    
});
