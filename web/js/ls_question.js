$(document).ready(function(){

  $(document).on('click','#view',function(){
    var question_id = $(this).data('question_id');
    $('#view_question_'+question_id).show();
    $.get( '/question/view_count?id='+question_id, function( data ) {});
  });

  $(document).on('click','#answer_pay',function(){
    $('#showFullAnswer').modal('show');
    $('#money_answer').text($(this).data('money_answer'));
    $('#text_answer').text($(this).data('text_answer'));
    $('#pay_answer_id').val($(this).data('answer_id'));
    if (parseInt($(this).data('credits_answer')) > 0){
      $('#via_credits').show();
      $('#credits_answer').text($(this).data('credits_answer'));
      $('#show_credits').show();
    }
  });
  $(document).on('click','#answer_show',function(){
    $('#text_answer_paymend').text($(this).data('text_answer'));
    $('#showFullAnswerPaymend').modal('show');
  });

  $( "#privacy_select" ).change(function() {
    if ($(this).val() == 4){
      $(this).attr('required',true);
      $('#users').show();
    } else {
      $('#users').hide();
      $(this).attr('required',false);
    }

  });


    $(document).on('click','.addToFavoriteQuestion',function(){
        var question_id = $(this).data('question_id');
        var thisElement = $(this);
        $.ajax({
            type: 'POST',
            url: '/add_favorite_question',
            data: {question_id:question_id},
            dataType:'json',
            success: function(response){
                if(response.status == 'success'){
                    thisElement.removeClass('addToFavoriteQuestion');
                    thisElement.addClass('deleteWithFavoriteQuestion');
                    swal("Add to favorite", "", "success");
                    thisElement.addClass('class_disconect');
                    thisElement.removeClass('class_connect');
                }
            }
        });
    });

    $(document).on('click','.deleteWithFavoriteQuestion',function(){
        var question_id = $(this).data('question_id');
        var thisElement = $(this);
        $.ajax({
            type: 'POST',
            url: '/delete_favorite_question',
            data: {question_id:question_id},
            dataType:'json',
            success: function(response){
                if(response.status == 'success'){
                    thisElement.removeClass('deleteWithFavoriteQuestion');
                    thisElement.addClass('addToFavoriteQuestion');
                    thisElement.removeClass('class_disconect');
                    thisElement.addClass('class_connect');
                    swal("Deleted with favorite", "", "success");
                }
            }
        });
    });


});