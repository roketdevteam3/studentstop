$(document).ready(function(){

    var grid_demo_classic = $('#masonry_hybrid_demo1');
    new MasonryHybrid(grid_demo_classic, {col: 4, space: 0});

    $(document).on('click','.buttonAboutRightPages',function(event){
        event.preventDefault();
        var user_id = $(this).data('user_id');
        var my = $(this).data('my');
            $('.modalRightPages').show();
            $('.modalRightPages').html('');
            $('.modalRightPages').append('<div class="col-sm-12 modalUserAboutHeader"></div>');
            $('.modalRightPages').append('<div class="col-sm-12 modalUserAboutContainer"></div>');
            
            var modalAllMessage = '';
            if(my == 'yes'){
            //    modalAllMessage = '<li role="presentation" class="myAllMessage"><i class=" fa fa-envelope"></i>All message</li>';
            }else{
                var my_id = $('.my_user_id').text();
                modalAllMessage = '<li role="presentation" class="panel-tab startChat" data-sender-id="'+my_id+'" data-recipient-id="'+user_id+'"><img src="../../images/icon/massage-chat-ic.png">All message</li>'+
                        '<li role="presentation" data-toggle="tab" class="panel-tab call" data-id="'+user_id+'"><img src="../../images/icon/call-ic.png"></i>Call</li>'+
                    '<li role="presentation" data-toggle="tab" class="panel-tab call" data-id="'+user_id+'"><img src="../../images/icon/video-ic.png">Video Call</li>';
            }
            $('.modalUserAboutHeader').append('<ul class="nav nav-tabs nav-justified">'+
                    '<li role="presentation" data-toggle="tab" data-user_id="'+user_id+'" data-my="'+my+'" class="panel-tab profileTabButton active"><img src="../../images/icon/profile-ic-chat.png">Profile</li>'+
                    '<li role="presentation" data-toggle="tab" data-user_id="'+user_id+'" data-my="'+my+'" class="panel-tab mapTabButton"><img src="../../images/icon/map-chat-ic.png">Map</li>'+
                    modalAllMessage+
                '</ul>')
        ProfileTab(user_id,my)
    })
    
    $(document).on('click','.myAllMessage',function(event){
        $('.user').trigger('click')
    })
    
    $(document).on('click','.profileTabButton',function(event){
        var user_id = $(this).data('user_id');
        var my = $(this).data('my');
        ProfileTab(user_id,my)        
    })
    $(document).on('click','.mapTabButton',function(event){
        var user_id = $(this).data('user_id');
        var my = $(this).data('my');
        MapTab(user_id,my)        
    })
    
    function MapTab(user_id,my){
        if(my == 'yes'){
            $('.modalUserAboutContainer').prepend(
                '<div class="col-sm-12 position_saved" style="display:none;color:green;border:1px solid green;">Position saved!</div>'+
                '<div class="col-sm-12 position_not_saved" style="display:none;color:red;border:1px solid red;">Position not saved!</div>'+
                '<input type="hidden" name="location_lat" id="location_lat" value="'+$('#location_lat').val()+'">'+
                '<input type="hidden" name="location_lng" id="location_lng" value="'+$('#location_lng').val()+'">'+
                '<div class="local-btn-wrap">'+
                    '<button class="btn datermine_my_location">Determine my location</button>'+
                    '<button name="" class="btn savePosition" >Save</button>'+
                '</div>'+
                '<label class="control-label">Enter your location:</label>'+
                '<input type="text" name="" class="form-control enter_your_location" placeholder="enter your location">'
            );
        }
    }
    $(document).on('click','.savePosition',function(event){
        var myLocationLat = $('[name=location_lat]').val();
        var myLocationLng = $('[name=location_lng]').val();
        if((myLocationLat != '') && (myLocationLng != '')){
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '../../../mylocationsave',
                data: {location_lat:myLocationLat,location_lng:myLocationLng},
                success: function(response){
                    if(response.status == 'success'){
                        $('.position_saved').show();
                        //swal('Position saved','','success');
                    }else{
                        $('.position_not_saved').show();
                        //swal('Position not saved','pleace try again','error');
                    }
                }
            });
            
        }else{
            console.log('error');
        }
    })
    
    $(document).on('click','.position_saved',function(event){
        $(this).hide();
    })
    $(document).on('click','.position_not_saved',function(event){
        $(this).hide();
    })
    
    function ProfileTab(user_id,my){
        $.ajax({
            type: 'POST',
            url: '../../../../users/default/getuserinfo',
            data: {user_id:user_id},
            dataType:'json',
        }).done(function(response){
            $('.modalUserAboutContainer').html('')
            if(my == 'yes'){
                $('.modalUserAboutContainer').append('<form class="profile-form">'+
                        '<div class="form-group field-user-name">'+                            
                            '<label class="control-label" for="user-name">First name</label>'+
                            '<input type="text" id="user-name" class="form-control" name="user_name" value="'+response.name+'">'+
                            '<p class="help-block help-block-error"></p>'+
                        '</div>'+
                        '<div class="form-group field-user-name">'+
                            '<label class="control-label" for="user-name">Last name</label>'+
                            '<input type="text" id="user-surname" class="form-control" name="user_surname" value="'+response.surname+'">'+
                            '<p class="help-block help-block-error"></p>'+
                        '</div>'+
                        '<div class="form-group field-user-name">'+
                            '<label class="control-label" for="user-name">Date of birth</label>'+
                            '<input type="text" id="user-date_of_birth" class="form-control" name="user_birthsday" value="'+response.birthsday+'">'+
                            '<p class="help-block help-block-error"></p>'+
                        '</div>'+
                        '<div class="form-group field-user-name">'+
                            '<label class="control-label" for="user-name">Address</label>'+
                            '<input type="text" id="user-address" class="form-control" name="user_address" value="'+response.address+'">'+
                            '<p class="help-block help-block-error"></p>'+
                        '</div>'+
                        '<div class="form-group field-user-name">'+
                            '<label class="control-label" for="user-name">Mobile</label>'+
                            '<input type="text" id="user-mobile" class="form-control" name="user_about" value="'+response.mobile+'">'+
                            '<p class="help-block help-block-error"></p>'+
                        '</div>'+
                        '<div class="form-group field-user-name">'+
                            '<label class="control-label" for="user-name">About</label>'+
                            '<input type="text" id="user-about" class="form-control" name="user_about" value="'+response.about+'">'+
                            '<p class="help-block help-block-error"></p>'+
                        '</div>'+
                        '<div class=col-sm-12>'+
                            '<button class="btn profileModalUpdate">Save</button>'+
                        '</div>'+
                        '</form>');
            }else{
                $('.modalUserAboutContainer').append('<table class="table">'+
                        '<tbody>'+
                            '<tr>'+
                                   ' <td>First name: </td>'+
                                    '<td>'+response.name+'</td>'+
                            '</tr>'+
                            '<tr>'+
                                    '<td>Last name: </td>'+
                                    '<td>'+response.surname+'</td>'+
                            '</tr>'+
                            '<tr>'+
                                    '<td>Date of birth: </td>'+
                                    '<td>'+response.birthsday+'</td>'+
                            '</tr>'+
                            '<tr>'+
                                   ' <td>Address: </td>'+
                                    '<td>'+response.address+'</td>'+
                            '</tr>'+
                            '<tr>'+
                                    '<td>Mobile: </td>'+
                                    '<td>'+response.mobile+'</td>'+
                            '</tr>'+
                            '<tr>'+
                                    '<td>About: </td>'+
                                    '<td>'+response.about+'</td>'+
                            '</tr>'+
                        '</tbody>'+
                    '</table>')
            }
            console.log(response);
        })
    }
    
    $(document).on('click','.profileModalUpdate',function(event){
        event.preventDefault();
        var first_name = $('.modalUserAboutContainer').find('#user-name').val();
        var surname = $('.modalUserAboutContainer').find('#user-surname').val();
        var birth = $('.modalUserAboutContainer').find('#user-date_of_birth').val();
        var address = $('.modalUserAboutContainer').find('#user-address').val();
        var mobile = $('.modalUserAboutContainer').find('#user-mobile').val();
        var about = $('.modalUserAboutContainer').find('#user-about').val();
        var user_id = $('.modalUserAboutContainer').find('#user-id').val();
        
        $.ajax({
            type: 'POST',
            url: '../../../../users/default/userupdateprofile',
            data: {user_id:user_id, first_name:first_name, surname:surname, birth:birth, address:address, mobile:mobile, about:about},
            dataType:'json',
        }).done(function(response){
            console.log(response)
        });
    })
    
})