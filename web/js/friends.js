$(document).ready(function(){
    
    if($('.usersContainer').length){
        
        $(".userBlock").each(function(){
                var user_id = $(this).data('user_id');
                var university_id = $(this).find('.universityName').text();
                var thisElement = $(this);
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '../../../users/default/getanoutheruserinformation',
                    data: {user_id:user_id,university_id:university_id},
                    success: function(response){
                        $('.userClass'+user_id).find('.universityName').text(response.university_name)
                        $('.userClass'+user_id).find('.friendCount').text(response.friend_count)
                        $('.userClass'+user_id).find('.classCount').text('enrolled in '+response.class_count)
                        $('.userClass'+user_id).find('.userRating').append('<input id="userInputRating'+user_id+'" name="clRating" value="'+response.star_count+'" class="rating-loading">')
                        
                        $('#userInputRating'+user_id).rating({displayOnly: true, step: 0.1, size:'xs'});
                    }
                });
        })
        
    }
    
})