$(document).ready(function(){
   
    var owl = $("#owl-demo");
    $(document).on('click','.modalClassShow',function(e){
        e.preventDefault();
    })

    owl.owlCarousel({
        items : 4, //10 items above 1000px browser width
        itemsDesktop : [1000,5], //5 items between 1000px and 901px
        itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
        itemsTablet: [600,2], //2 items between 600 and 0;
        itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
    });

    $(".next").click(function(){
      owl.trigger('owl.next');
    })
    $(".prev").click(function(){
      owl.trigger('owl.prev');
    })
    $(".play").click(function(){
      owl.trigger('owl.play',1000);
    })
    $(".stop").click(function(){
      owl.trigger('owl.stop');
    })
   
   
   
    $(document).on('change','[name=university_class]',function(){
        var university_id = $(this).val();
        $.ajax({
            url:'../../../../getmajorsarray',
            method:'POST',
            data:{university_id:university_id},
            dataType:'json',
            success:function(response){
                $('[name=major_class]').html('');
                $('[name=major_class]').append('<option value="0">Major</option>');
                
                $('[name=course_class]').html('');
                $('[name=course_class]').append('<option value="0">Course</option>');
                
                $('[name=class_class]').html('');
                $('[name=class_class]').append('<option value="0">Class</option>');
                
                for(var key in response){
                    $('[name=major_class]').append('<option value='+key+'>'+response[key]+'</option>');
                }
            }
        });
    });
    
    $(document).on('change','[name=major_class]',function(){
        var major_id = $(this).val();
        $.ajax({
            url:'../../../../getcoursearray',
            method:'POST',
            data:{major_id:major_id},
            dataType:'json',
            success:function(response){
                $('[name=course_class]').html('');
                $('[name=course_class]').append('<option value="0">Course</option>');
                
                $('[name=class_class]').html('');
                $('[name=class_class]').append('<option value="0">Class</option>');
                
                for(var key in response){
                    $('[name=course_class]').append('<option value='+key+'>'+response[key]+'</option>');
                }
            }
        });
    });
    
    $(document).on('change','[name=course_class]',function(){
        var course_id = $(this).val();
        $.ajax({
            url:'../../../../getclassarray',
            method:'POST',
            data:{course_id:course_id},
            dataType:'json',
            success:function(response){
                $('[name=class_class]').html('');
                $('[name=class_class]').append('<option value="0">Class</option>');
                for(var key in response){
                    $('[name=class_class]').append('<option value='+key+'>'+response[key]+'</option>');
                }
            }
        });
    });
    
    
    
    
    
    $(document).on('change','#classes-id_university',function(){
        var university_id = $(this).val();
        if(university_id != ''){
            $.ajax({
                url:'../../../../getmajorsarray',
                method:'POST',
                data:{university_id:university_id},
                dataType:'json',
                success:function(response){
                    $('#classes-id_major').html('');
                    $('#classes-id_major').append('<option value="0">Major</option>');

                    $('#classes-id_course').html('');
                    $('#classes-id_course').append('<option value="0">Course</option>');

                    for(var key in response){
                        $('#classes-id_major').append('<option value='+key+'>'+response[key]+'</option>');
                    }
                }
            });
        }else{
            $('#classes-id_major').html('');
            $('#classes-id_major').append('<option value="0">Major</option>');

            $('#classes-id_course').html('');
            $('#classes-id_course').append('<option value="0">Course</option>');
        }
    });
    
    $(document).on('change','#classes-id_major',function(){
        var major_id = $(this).val();
        if(major_id != ''){
            $.ajax({
                url:'../../../../getcoursearray',
                method:'POST',
                data:{major_id:major_id},
                dataType:'json',
                success:function(response){
                    $('#classes-id_course').html('');
                    $('#classes-id_course').append('<option value="0">Course</option>');
                    
                    for(var key in response){
                        $('#classes-id_course').append('<option value='+key+'>'+response[key]+'</option>');
                    }
                }
            });
        }else{
            $('#classes-id_course').html('');
            $('#classes-id_course').append('<option value="0">Course</option>');
        }
    });
    
    
    
    
    if($('.user_class_photo').length){
        var myDropzone = new Dropzone($('.user_class_photo')[0], { // Make the whole body a dropzone
            uploadMultiple:false,
            maxFiles:1,
        });

        myDropzone.on("maxfilesexceeded", function(file) {
            myDropzone.removeAllFiles();
            myDropzone.addFile(file);
        });

        myDropzone.on("complete", function(response) {
            if (response.status == 'success') {
                $('.user_class_photo').find('.dz-preview').hide();
                $('.user_class_photo').css('background-image',"url('../../../images/classes/"+response.xhr.response+"')");
                $('#classes-img_src').val(response.xhr.response);
            }
        });

        myDropzone.on("removedfile", function(response) {
            if(response.xhr != null){
//                   deleteFile(response.xhr.response);
            }
        });
    }
    
    $(document).on('click','.JoinClass',function(){
        var followStatus = 'Follow';
        var id_object = $(this).data('class_id');
        var thisButton = $(this);
        
        $.ajax({
            url:'../../../../ajaxfollowclass',
            method:'POST',
            data:{id_object:id_object,followStatus:followStatus},
            dataType:'json',
            success:function(response){
                if(response.status == 'success'){
                    thisButton.text(response.statusFoloow);
                    thisButton.addClass('UnfollowClass');
                    thisButton.removeClass('JoinClass');
                }
            }
        });
    });
    
    $(document).on('click','.UnfollowClass',function(){
        var followStatus = 'Unfollow';
        var id_object = $(this).data('class_id');
        var thisButton = $(this);
        
        $.ajax({
            url:'../../../../ajaxfollowclass',
            method:'POST',
            data:{id_object:id_object,followStatus:followStatus},
            dataType:'json',
            success:function(response){
                if(response.status == 'success'){
                    thisButton.text(response.statusFoloow);
                    thisButton.addClass('JoinClass');
                    thisButton.removeClass('UnfollowClass');
                }
            }
        });
    });
    
    function getClassRank(class_id){
        $.ajax({
            url:'../../../../course/classes/getclassrank',
            method:'POST',
            data:{class_id:class_id},
            dataType:'json',
            success:function(response){
                $('.classRank'+class_id).append(response.rank);
            }
        });
        
    }
    
    function getCourseRating(course_id){
        $.ajax({
            url:'../../../../course/classes/getcourserating',
            method:'POST',
            data:{course_id:course_id},
            dataType:'json',
            success:function(response){
                $('.courseRating'+course_id).append('<td>Rating: <input  id="courseInputRatingClass'+course_id+'" name="courseClass" value="'+response.count_star+'" class="rating-loading"></td>')
                $('.courseClassesCount'+course_id).append('<k style="color:red;">Classes: </k>'+response.class_count)
                $('#courseInputRatingClass'+course_id).rating({displayOnly: true, step: 0.1, size:'xs'});
            }
        })
        
    }
    
//    if($('.classSearchContainer').length){
//        
//    }
    
    function getCourseClass(major_id, major_class, course_class, professor_class, search_key){
        $.ajax({
            url:'../../../../course/classes/getclasscourse',
            method:'POST',
            beforeSend: function( xhr ) {
                $('.classNotFound').hide();
                $('.classSearchContainer').html('');
                $('.classSearchContainer').append('<div style="text-align:center;"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>');
            },
            data:{major_id:major_id, major_class:major_class, course_class:course_class, professor_class:professor_class, search_key:search_key},
            dataType:'json',
            success:function(response){
                $('.universityMajor').removeClass('active');
                $(this).addClass('active');
                $('.classSearchContainer').html('');
                //console.log(response.length);
                if(response.length != 0){
                    $('.classNotFound').hide();
                    //console.log(response);
                    for(var course_id in response){
                        var course = response[course_id];
                        console.log(course)
                        $('.classSearchContainer').append('<div class="search-table-wrap">'+
                                                        '<table class="table">'+
                                                            '<tbody>'+
                                                                '<tr>'+
                                                                    '<td>'+
                                                                        '<k style="color:red;">Course:</k> '+course['course_info']['course_name']+
                                                                    '</td>'+
                                                                    '<td>'+
                                                                        '<k style="color:red;">Major:</k> '+course['course_info']['major_name']+
                                                                    '</td>'+
                                                                    '<td>'+
                                                                        '<k style="color:red;">University:</k>' +
                                                                        '<div class="university-img">'+
                                                                            '<img class="img-responsive" src="../../..'+course['course_info']['university_image']+'"">'+
                                                                        '</div>'+
                                                                        course['course_info']['university_name']+
                                                                    '</td>'+
                                                                    '<td style="color:red; " class="courseRating'+course_id+'">'+
                                                                    '</td>'+
                                                                    '<td  class="courseClassesCount'+course_id+'">'+
                                                                    '</td>'+
                                                                    '<td>'+
                                                                        '<span class="down-arrow showClassInCourse" data-course_id="'+course_id+'" ></span>'+
                                                                    '</td>'+
                                                                '</tr>'+
                                                            '</tbody>'+
                                                        '</table>' +
                                                        '<table class="table table-open courseClassesTable'+course_id+'" style="display:none;">' +
                                                            '<thead>' +
                                                                '<tr">'+
                                                                    '<td>'+
                                                                    '#'+
                                                                    '</td>'+
                                                                    '<td>'+
                                                                        'Class'+
                                                                    '</td>'+
                                                                    '<td>'+
                                                                        'Professor'+
                                                                    '</td>'+
                                                                    '<td>'+
                                                                        'Status'+
                                                                    '</td>'+
                                                                    '<td>'+
                                                                        'Students'+
                                                                    '</td>'+
                                                                    '<td>'+
                                                                        'Rank'+
                                                                    '</td>'+
                                                                    '<td>'+
                                                                        'Rating'+
                                                                    '</td>'+
                                                                    '<td>'+
                                                                    '</td>'+
                                                                '</tr>'+
                                                            '</thead>' +
                                                            '<tbody class="courseClasses'+course_id+'" >' +
                                                            '</tbody>' +
                                                        '</table>'+
                                                        '</div>');
                                                getCourseRating(course_id);
                                                var course_classes = course['course_classes'];
                                                var k = 0;
                                                for(var key in course_classes){
                                                    var class_status = '';
                                                    if(course_classes[key].class_status == '1'){
                                                        var class_follow = course_classes[key].follow;
                                                            if (class_follow != null){
                                                                if (class_follow.status == '0'){
                                                                    class_status = '<span class="active">Active</span>';
                                                                }else{
                                                                    class_status = '<span class="no-active">Inactive</span>';
                                                                }
                                                            }else{
                                                                class_status = '';
                                                            }
                                                    }else{
                                                        if(course_classes[key].members < 25){ 
                                                            class_status = '<span class="no-active">Pending</span>';
                                                        }else{ 
                                                            class_status = '<span class="no-active">Inactive</span>';
                                                        }
                                                    }
                                                    
                                                    k = k + 1;
                                                      var professor_avatar = '/default_avatar.jpg';
                                                      if((course_classes[key].professor_avatar != '') && (course_classes[key].professor_avatar != null)){
                                                        professor_avatar = 'users_images/'+course_classes[key].professor_avatar;
                                                      }
//                                                        course_classes[key].class_date_active
                                                    $('.courseClasses'+course_id).prepend('<tr class="hover_backround_color">'+
                                                        '<td>'+k+'</td>'+
                                                        '<td>'+course_classes[key].class_name+'</td>'+
                                                        '<td><div class="user-img"><img src="../../../images/'+professor_avatar+'" style="width:40px;height:40px;"></div><span class="name">'+course_classes[key].professor+'</span></td>'+
                                                        '<td>'+class_status+'</td>'+
                                                        '<td>'+course_classes[key].members+'</td>'+
                                                        '<td class="classRank'+course_classes[key].class_id+'"></td>'+
                                                        '<td><input id="starClass'+course_classes[key].class_id+'" name="starClass" value="'+course_classes[key].count_star+'" class="rating-loading"></td>'+
                                                        '<td><a href="../../../class/'+course_classes[key].class_url+'" class="btn btn-watch">View</a></td>'+
                                                    +'</tr>');
                                                $('#starClass'+course_classes[key].class_id).rating({displayOnly: true, step: 0.1, size:'xs'});
                                                    getClassRank(course_classes[key].class_id);
                                                }
//                        $('.classSearchContainer').append('<div class="col-sm-12 classBlock" style="background-color: white;margin-top:20px;">'+
//                                '<b>Class:</b> '+clases.class_name+'; '+
//                                '<b>Profesor:</b> '+clases.professor+'; '+
//                                '<b>Students:</b> '+clases.members+'; '+
//                                '<b>Count Star:</b> '+clases.count_star+'; '+
//                                '<a href="../../../class/'+clases.class_url+'" class="btn btn-watch">Watching</a>'+
//                                clases.class_join+
//                        '</div>');
                    }
                }else{
                    $('.classNotFound').show();
                }
            }
        })
    }
    
    $(document).on('click','.universityMajor',function(){
        var major_id = $(this).data('major_id');
        getCourseClass(major_id,'','','','');
    });
    
    if($('.classSearchContainer').length){
        getCourseClass('ready',$('[name=major_class]').val(), $('[name=course_class]').val(), $('[name=professor_class]').val(),$('[name=search_key]').val());
    }
    
    $(document).on('click','.showClassInCourse',function(){
        console.log($(this).hasClass('down-arrow'))
        var course_id = $(this).data('course_id');
        if($(this).hasClass('down-arrow')){
            $(this).removeClass('down-arrow');
            $(this).addClass('top-arrow');
            $('.courseClassesTable'+course_id).show()
        }else{
            $(this).removeClass('top-arrow');
            $(this).addClass('down-arrow');
            $('.courseClassesTable'+course_id).hide()
        }
    })
   
   
   $(document).on('change','.change_form_course, .class_new_professor',function(){
       $('.class_new_name').val($('.change_form_course option:checked').text())
   })
});
