function initMap() {
    var myLatLng = {lat: 45.8152087, lng: -9.8816965};
    
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 2,
      center: myLatLng
    });
}

$(document).ready(function(){
    
    $(document).on('click','.closeModal',function(){
        var rent_id = $(this).data('rent_id');
        $('#view_rentshouse_'+rent_id).hide();
        window.location.hash=""
    });
    
    $(document).on('click','.rent_tab_show_map',function(){
           var markerr = '';
            var rent_id = $(this).data('rent_id');
            if($(this).data('position_lat') != ''){
                console.log($(this).data('position_lat'))
                console.log($(this).data('position_lng'))

                var element = document.getElementById("map_modal"+rent_id);
                var mapss = new google.maps.Map(element, {
                    center: new google.maps.LatLng(48, 11),
                    zoom: 2,
                });
                    markerr = new google.maps.Marker({
                        position: {lat: parseFloat($(this).data('position_lat')), lng: parseFloat($(this).data('position_lng'))},
                        //label: 'Title Hello',
                        map: mapss,
                    });
                    mapss.setCenter({lat: parseFloat($(this).data('position_lat')), lng: parseFloat($(this).data('position_lng'))});                
            }else{
                $("#map_modal"+rent_id).text('No data addresses')
            }
        
    })


  $(document).on('click','#delete',function(){
    var rent_id = $(this).data('rent_id');
    $.get('/rentshouse/delete_renthouse?id='+rent_id, function (data) {
      window.location = '';
    })
  });
    
    
    //initMap();
    var marker = '';
    var element = document.getElementById("map");
    var map = new google.maps.Map(element, {
        center: new google.maps.LatLng(48, 11),
        zoom: 2,
    });
    
    if($('.rentBlockInfo').length){
        $(".rentBlockInfo").each(function() {
//            console.log('---------');
//            console.log($(this).data('position_lat'));
//            console.log($(this).data('position_lng'));
//            console.log('---------');
            marker = new google.maps.Marker({
                position: {lat: parseFloat($(this).data('position_lat')), lng: parseFloat($(this).data('position_lng'))},
                //label: 'Title Hello',
                map: map,
            });
        });
    }

});        