$(document).ready(function(){
    var grid_demo_classic = $('#masonry_hybrid_demo2');
    new MasonryHybrid(grid_demo_classic, {col: 3, space: 20});


    $(document).on('change','[name=university_conference]',function(){
        var university_id = $(this).val();
        $.ajax({
            url:'../../../../getmajorsarray',
            method:'POST',
            data:{university_id:university_id},
            dataType:'json',
            success:function(response){
                $('[name=major_conference]').html('');
                $('[name=major_conference]').append('<option value="0">Major</option>');
                
                $('[name=course_conference]').html('');
                $('[name=course_conference]').append('<option value="0">Course</option>');
                
                $('[name=tutors_conference]').html('');
                $('[name=tutors_conference]').append('<option value="0">Tutors</option>');
                
                for(var key in response){
                    $('[name=major_conference]').append('<option value='+key+'>'+response[key]+'</option>');
                }
            }
        });
    });
    
    $(document).on('change','[name=major_conference]',function(){
        var major_id = $(this).val();
        $.ajax({
            url:'../../../../getcoursearray',
            method:'POST',
            data:{major_id:major_id},
            dataType:'json',
            success:function(response){
                $('[name=course_conference]').html('');
                $('[name=course_conference]').append('<option value="0">Course</option>');
                
                $('[name=tutors_conference]').html('');
                $('[name=tutors_conference]').append('<option value="0">Tutors</option>');
                
                for(var key in response){
                    $('[name=course_conference]').append('<option value='+key+'>'+response[key]+'</option>');
                }
            }
        });
    });
    
    $(document).on('change','[name=course_conference]',function(){
        var course_id = $(this).val();
        $.ajax({
            url:'../../../../gettutorsarray',
            method:'POST',
            data:{course_id:course_id},
            dataType:'json',
            success:function(response){
                $('[name=tutors_conference]').html('');
                $('[name=tutors_conference]').append('<option value="0">Tutors</option>');
                for(var key in response){
                    $('[name=tutors_conference]').append('<option value='+key+'>'+response[key]+'</option>');
                }
            }
        });
    });
   
    $(document).on('click','.deleteConferenceWithFavorite',function(){
        var conference_id = $(this).data('conference_id');
        var thisElement = $(this);
        $.ajax({
            url:'../../../../ajaxfavoritevirtualclass',
            method:'POST',
            data:{conference_id:conference_id,status:'delete'},
            dataType:'json',
            success:function(response){
                if(response.status == 'success'){
                    thisElement.removeClass('fa-star');
                    thisElement.addClass('fa-star');
                    thisElement.removeClass('deleteConferenceWithFavorite');
                    thisElement.addClass('addConferenceToFavorite');
                    thisElement.find('.text').text('Add to favorite');
                }
            }
        });
    });
    
    $(document).on('click','.addConferenceToFavorite',function(){
        var conference_id = $(this).data('conference_id');
        var thisElement = $(this);
        $.ajax({
            url:'../../../../ajaxfavoritevirtualclass',
            method:'POST',
            data:{conference_id:conference_id,status:'Add'},
            dataType:'json',
            success:function(response){
                if(response.status == 'success'){
                    thisElement.removeClass('fa-star-o');
                    thisElement.addClass('fa-star');
                    thisElement.addClass('deleteConferenceWithFavorite');
                    thisElement.removeClass('addConferenceToFavorite');
                    thisElement.find('.text').text('Delete with favorite');
                }
            }
        });
    });

});