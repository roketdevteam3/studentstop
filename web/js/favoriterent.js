$(document).ready(function(){
    
    $(document).on('click','.addToFavorite',function(){
        var rent_id = $(this).data('rent_id');
        var thisElement = $(this);
        $.ajax({
            type: 'POST',
            url: '../../../addrenttofavorite',
            data: {rent_id:rent_id},
            dataType:'json',
            success: function(response){
                if(response.status == 'success'){
                    thisElement.removeClass('addToFavorite');
                    thisElement.addClass('deleteWithFavorite');
                    thisElement.find('.text').text('Delete with favorite');
                    swal("Add to favorite", "", "success");
                }
            } 
        });
    });
    
    $(document).on('click','.deleteWithFavorite',function(){
        var rent_id = $(this).data('rent_id');
        var thisElement = $(this);
        $.ajax({
            type: 'POST',
            url: '../../../deleterentwithfavorite',
            data: {rent_id:rent_id},
            dataType:'json',
            success: function(response){
                if(response.status == 'success'){
                    thisElement.removeClass('deleteWithFavorite');
                    thisElement.addClass('addToFavorite');
                    thisElement.find('.text').text('Add to favorite');
                    swal("Deleted with favorite", "", "success");
                }
            } 
        });
    });
    
});        