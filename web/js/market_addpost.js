$(document).ready(function(){
    $(document).on('click','.add_post_jobs',function(){
        $('.modalRightPages').show();
        $('.modalRightPages').html('');
        $('.modalRightPages').append('<div style="display:none;">'+
	'<div class="table table-striped previewTemplateQ" >'+
		'<div class="row">'+
			'<div id="template" class="file-row">'+
				'<div class="col-sm-3">'+
					'<span class="preview"><img data-dz-thumbnail /></span>'+
				'</div>'+
				'<div class="col-sm-3" style="    float: right;position: relative;bottom: 31px;right: 63px;">'+
				  '<button data-dz-remove class="btn btn-danger delete" style="font-size: 14px; height: 30px; background-color: #7364cc;border: 1px solid #7364cc;">'+
					'<i class="glyphicon glyphicon-trash" style="    padding-right: 8px;position: relative;top: 2px;"></i>'+
					'<span>Delete</span>'+
				  '</button>'+
				'</div>'+
			'</div>'+
		'</div>'+
	'</div>');
        $('.modalRightPages').append('<div class="market-modal">'+
            '<form  method="post" class="addPostRightBlock">'+
                '<div class="form-group field-posts-content">'+
                    '<label class="control-label" for="posts-title">Title</label>'+
                    '<input type="text" id="posts-title" class="form-control" required name="posts_title">'+
                '</div>'+
                '<div class="form-group field-posts-content">'+
                    '<label class="control-label" for="posts-content">Content</label>'+
                    '<textarea id="posts-content" class="form-control" required name="Posts[content]" rows="6"></textarea>'+
                '</div>'+
                '<span class="btn newPostPhoto dz-clickable btn-ripple">'+
                        '<span>Upload photo</span>'+
                '</span>'+
                '<div class="post_container_photo"></div>'+
                '<div class="form-group field-posts-image_src">'+
                    '<input type="hidden" id="posts-image_src" class="form-control" name="posts_image_src">'+
                '</div>'+
                '<div class="form-group field-posts-category_id ">'+
                    '<label class="control-label" for="posts-category_id">Category</label>'+
                    '<select class="PostCategorySelect form-control" required>'+
                    '</select>'+
                '</div>'+
                '<div class="form-group field-posts-address">'+
                    '<label class="control-label" for="posts-address">Address</label>'+
                    '<input type="text" id="posts-address" class="form-control" required name="Posts[address]">'+
                '</div>'+
                '<button type="submit" class="btn add-post btn-ripple" name="add_posts">'+
                    'Add post<span class="ripple _3 animate" style="height: 120px; width: 120px;"></span>'+
                '</button>'+
            '</form>'+
        '</div>')
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '../../../posts/posts/getcategory',
                data: {},
                success: function(response){
                    var coutn_v = 0;
                    for(var key in response){
                        var category = key;
                        coutn_v = coutn_v + 1;
                        $('.PostCategorySelect').append('<optgroup label="'+category+'" class="PostCategoryParent'+coutn_v+'"></optgroup>');
                        var categories = response[key];
                        for(var key2 in categories){
                            $('.PostCategoryParent'+coutn_v).append('<option value="'+key2+'">'+categories[key2]+'</option>');
                        }
                    }
                }
            });
            
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '../../../users/default/getuserinfo',
                data: {},
                success: function(response){
                    console.log(response.address);
                    $('#posts-address').val(response.address)
                }
            });
            
                var previewNode = document.querySelector(".previewTemplateQ");
                previewNode.id = "";
                var previewTemplate = previewNode.parentNode.innerHTML;
                previewNode.parentNode.removeChild(previewNode);
                if($('.post_container_photo').length > 0){
                    var myDropzoneA = new Dropzone($('.post_container_photo')[0], { 
                        maxFiles:1,
                        uploadMultiple:false,
                        url: "../../posts/posts/savepostsphoto",
                        previewTemplate:previewTemplate,
                        clickable: ".newPostPhoto",
                        previewsContainer: ".post_container_photo",
                    });

                    myDropzoneA.on("maxfilesexceeded", function(file) {
                            myDropzoneA.removeAllFiles();
                            myDropzoneA.addFile(file);

                    });
                    
                    myDropzoneA.on("complete", function(response) {
                        if (response.status == 'success') {
                            $('#posts-image_src').val(response.xhr.response);
                        }
                    });

                    myDropzoneA.on("removedfile", function(response) {
                        if(response.xhr != null){
                           deleteFile(response.xhr.response);
                        }
                    });
                }

    })
    
    $(document).on('click','.newPostPhoto',function(){
        var explode = function(){
            $('.modalRightPages').show()
        };
        setTimeout(explode, 200);
    })
    
    $(document).on("submit",'.addPostRightBlock', function( event ) {
        event.preventDefault();
        var post_title = $('#posts-title').val();
        var posts_content = $('#posts-content').val();
        var posts_image_src = $('#posts-image_src').val();
        var post_category = $('.PostCategorySelect').val();
        var posts_address = $('#posts-address').val();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '../../../posts/posts/savemarketpost',
            data: {post_title:post_title, posts_content:posts_content, posts_image_src:posts_image_src,
                   post_category:post_category, posts_address:posts_address},
            success: function(response){
                swal({
                    title: "Post added",
                    text: "",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonClass: "btn btn-success",
                    confirmButtonText: "Ok",
                  },
                  function(){
                    window.location.reload()
                  });
                //swal("Post added!", "", "success")
            }
        });
    });
    
    
    
$(document).on('click','.viewModalPost',function(){
    $('.modalRightPages').show();
    $('.modalRightPages').html('');
    var post_id = $(this).data('post_id');
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '../../../posts/posts/getpostinformation',
            data: {post_id:post_id},
            success: function(response){
                var avatar = '';
                if(response['avatar'] != ''){
                    avatar = "<img src='../../../../images/users_images/"+response['avatar']+"' style='width:100%;'>";
                }else{
                    avatar = '<img src="../../../images/default_avatar.jpg" style="width:100%;">';
                }
                var post_img = '';
                if(response['image_src'] != ''){
                    post_img = '<div class="col-sm-12 postInfo">'+
                                    '<div class="col-sm-2"></div>'+
                                    '<div class="col-sm-8">'+
                                        '<img src="../../../images/posts_images/'+response['image_src']+'" style="width:100%;">'+
                                    '</div>'+
                                '</div>';
                }
                var myUserId = $('[name=myUserId]').val();
                $('.modalRightPages').append('<div class="col-ms-12 userInfo">'+
                            '<div class="col-sm-3">'+
                                avatar+
                            '</div>'+
                            '<div class="col-sm-9">'+
                                '<div class="col-sm-9">'+
                                    '<table class="table">'+
                                        '<tr>'+
                                            '<td>email<td>'+
                                            '<td>'+response['email']+'<td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>email<td>'+
                                            '<td>'+response['mobile']+'<td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>address<td>'+
                                            '<td>'+response['address']+'<td>'+
                                        '</tr>'+
                                    '</table>'+
                                '</div>'+
                                '<div class="col-sm-2">'+
                                    '<button class="btn contact-request btn-ripple" data-recipient-id="'+myUserId+'" data-post-id="'+response['id']+'" data-user-id="'+response['id_user']+'">Contact request</button>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-sm-12 postInfo">'+
                            '<h4 style="text-align:center">'+response['title']+'</h4>'+
                                post_img+
                            '<p style="text-align:center:">'+response['content']+'</p>'+
                        '</div>');
                console.log(response);
            }
        });
        
    
}); 
    
    function changepublishstatus(post_id, status, thisElement){
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '../../../posts/posts/changepublishstatus',
            data: {post_id:post_id,status:status},
            success: function(response){
                if(response.status == 'save'){
                    if(status == 'publish'){
                        thisElement.removeClass('PublishPost');
                        thisElement.addClass('UnPublishPost');
                        thisElement.text('Unpublish');
                    }else if(status == 'unpublish'){
                        thisElement.removeClass('UnPublishPost');
                        thisElement.addClass('PublishPost');
                        thisElement.text('Publish');
                    }
                }
            }
        });
    }
    
    $(document).on('click','.UnPublishPost',function(){
        var post_id = $(this).data('post_id');
        changepublishstatus(post_id,'unpublish',$(this));
    });

    $(document).on('click','.PublishPost',function(){
        var post_id = $(this).data('post_id');
        changepublishstatus(post_id,'publish',$(this));
    });
    
    
    
//    $(document).on('click','.addPostRightBlock',function(event){
//        event.preventDefault();
//    })
        
});        