function initMap(address){
    var myLatLng = {lat: 45.8152087, lng: -9.8816965};
    var my_location_lat;
    var my_location_lng;
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 2,
      center: myLatLng
    });

    if(address != ''){
        var geocoder = new google.maps.Geocoder();
        //var address = $(this).val();
            geocoder.geocode({'address': address}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                var Local = results[0].geometry.location;
                var latLng = {lat:parseFloat(Local.lat()),lng:parseFloat(Local.lng())};
                
                geocoder.geocode({'location': latLng}, function(results, status) {
                    if (status === 'OK') {
                      if (results[1]) {
                          console.log(results[1])
                            var adderss_fields = document.getElementById('posts-address');
                            adderss_fields.value = results[1].formatted_address;
                            for (var i = 0; i < results.length; i++) {
                                if (results[i].types[0] === "locality") {
                                    var city = results[i].address_components[0].short_name;
                                    var state = results[i].address_components[2].short_name;
                                        //console.log(city);
                                }
                            }
                      } else {
                        window.alert('No results found');
                      }
                    } else {
                      window.alert('Geocoder failed due to: ' + status);
                    }
                });
                
                    
                  var marker = new google.maps.Marker({
                    position: latLng,
                    zoom:10,
                    map: map,
                 //   title: 'Hello World!'
                  });
                  map.setCenter(latLng);
                  map.setZoom(14);
            }else{
                alert('error');
            }
        });
    }
}

$(document).ready(function(){
    //initMap();
    //alert('adcdac');
    $("#modalAddPosts").on('shown.bs.modal', function(){
        initMap('');
    });
    
    $(document).on('change','.enter_your_location',function(){
        initMap($(this).val()); 
    });
    
    $(document).on('click','[name=add_posts_2]',function(){
        $('[name=add_posts]').trigger('click');
    })
    
    $(document).on('change','#category_filter',function(){
        var category_url = $(this).val();
        $.ajax({
            type: 'POST',
            url: '../../../getcategorypost',
            data: {category_url:category_url},
            dataType:'json',
            success: function(response){
                if(response != null){
                    $('#form_posts').attr('action','../../../market/'+response+'/'+category_url);
                    console.log(response);
                }else{
                    $('#form_posts').attr('action','../../../market/'+category_url);
                }
            }
        });
    });
    
});

