$(document).ready(function() {
    var webrtc;
    var configuration = {
        "iceServers": [{
                url: 'stun:stun01.sipphone.com'
            },
            {
                url: 'stun:stun.ekiga.net'
            },
            {
                url: 'stun:stun.fwdnet.net'
            },
            {
                url: 'stun:stun.ideasip.com'
            },
            {
                url: 'stun:stun.iptel.org'
            },
            {
                url: 'stun:stun.rixtelecom.se'
            },
            {
                url: 'stun:stun.schlund.de'
            },
            {
                url: 'stun:stun.l.google.com:19302'
            },
            {
                url: 'stun:stun1.l.google.com:19302'
            },
            {
                url: 'stun:stun2.l.google.com:19302'
            },
            {
                url: 'stun:stun3.l.google.com:19302'
            },
            {
                url: 'stun:stun4.l.google.com:19302'
            },
            {
                url: 'stun:stunserver.org'
            },
            {
                url: 'stun:stun.softjoys.com'
            },
            {
                url: 'stun:stun.voiparound.com'
            },
            {
                url: 'stun:stun.voipbuster.com'
            },
            {
                url: 'stun:stun.voipstunt.com'
            },
            {
                url: 'stun:stun.voxgratia.org'
            },
            {
                url: 'stun:stun.xten.com'
            },
            {
                url: 'turn:numb.viagenie.ca',
                credential: 'muazkh',
                username: 'webrtc@live.com'
            },
            {
                url: 'turn:192.158.29.39:3478?transport=udp',
                credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
                username: '28224511:1379330808'
            },
            {
                url: 'turn:192.158.29.39:3478?transport=tcp',
                credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
                username: '28224511:1379330808'
            }
        ]
    };

    function makeid() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 22; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }
    if ($('#join').length) {
        socket.emit('check', {
            vcId: $('#class_id').val()
        })
    }
    socket.on('vc-start', function(data) {
        if (data.vcId == $('#class_id').val()) {
            if (webrtc) {
                webrtc.leaveRoom()
                webrtc.stopLocalVideo()
            }else {
              var media = {
                  video: true,
                  audio: true
              }
              webrtc = new SimpleWebRTC({
                  // the id/element dom element that will hold "our" video
                  localVideoEl: 'reciver',
                  // the id/element dom element that will hold remote videos
                  remoteVideosEl: 'reciverScrren',
                  // immediately ask for camera access
                  autoRequestMedia: true,
                  media: media,
                  // localVideo:{mirror:false},
                  peerConnectionConfig: configuration,
                  url: 'https://thestudentstop.com:3001'
                  // url: 'https://127.0.0.1:3001'
              });
            }


            $('.unmute-video').show()
            $('.mute-video').hide()
            webrtc.startLocalVideo()

            webrtc.joinRoom(data.room, function(err, roomDescription) {
                if (err) {
                    swal("Oops...", "Something went wrong!", "error");
                } else {
                    webrtc.pauseVideo()
                    if ('' == 'video') {
                        webrtc.resumeVideo()
                        $('.unmute-video').hide()
                        $('.mute-video').show()
                    }
                    $('.call-controll-v').show();
                    room = roomDescription
                }
            })

        }
    })
    socket.on('vc-end', function(data) {
        if (data.vcId == $('#class_id').val()) {
            if (webrtc) {
                webrtc.leaveRoom()
                webrtc.stopLocalVideo()
            }
            $('.call-controll-v').hide();
        }
    })
    socket.on('incoming-call', function(data) {
        console.log(data);
        $('.incoming-call-controll').data('room-id', data.roomId);
        $('.sender').html(data.sender)
        $('.incoming-call-controll').data('r-id', data.sId);
        $('.incoming-call-controll').show();
    })
    socket.on('vc-f', function() {
        swal('stream start soon, automaticaly')
    })
    socket.on('decline-call', function(data) {
        if (webrtc) {
            webrtc.leaveRoom()
            webrtc.stopLocalVideo()
        }
        $('.call-box').hide()
        $('.incoming-call-controll').hide();
        $('.call-controll').hide();
        swal("User Canceled", "error");

    })
    $(document).on('click', '.call', function() {
        if (webrtc) {
            webrtc.leaveRoom()
            webrtc.stopLocalVideo()
        }else {
          webrtc = new SimpleWebRTC({
              // the id/element dom element that will hold "our" video
              localVideoEl: 'localVideo',
              // the id/element dom element that will hold remote videos
              remoteVideosEl: 'remotesVideos',
              // immediately ask for camera access
              autoRequestMedia: true,
              // localVideo:{mirror:false},
              peerConnectionConfig: configuration,
              url: 'https://thestudentstop.com:3001'
              // url: 'https://127.0.0.1:3001'
          });
        }
        $('.unmute-video').hide()
        $('.mute-video').show()
        $('.call-box').show()
        var rId = $('.call').attr('data-id');
        webrtc.startLocalVideo()

        webrtc.on('videoRemoved', function() {
            $('.call-box').hide()
            $('.incoming-call-controll').hide();
            $('.call-controll').hide();
            location.reload();
        })
        webrtc.createRoom(makeid(), function(err, roomDescription) {
            if (err) {
                swal("Oops...", "Something went wrong!", "error");
            } else {
                $('.unmute-video').hide()
                $('.mute-video').show()
                $('.call-controll').show();
                $('.incoming-call-controll').data('room-id', roomDescription);
                $('.incoming-call-controll').data('r-id', rId);
                socket.emit('incoming-call-server', {
                    rId: rId,
                    room: roomDescription
                })
            }
        })
    })
    $(document).on('click', '.answer', function() {
        $(this).data('type')
        if (webrtc) {
            webrtc.leaveRoom()
            webrtc.stopLocalVideo()
        }else {
          var media = {
              video: true,
              audio: true
          }
          webrtc = new SimpleWebRTC({
              // the id/element dom element that will hold "our" video
              localVideoEl: 'localVideo',
              // the id/element dom element that will hold remote videos
              remoteVideosEl: 'remotesVideos',
              // immediately ask for camera access
              autoRequestMedia: true,
              media: media,
              // localVideo:{mirror:false},
              peerConnectionConfig: configuration,
              url: 'https://thestudentstop.com:3001'
              // url: 'https://127.0.0.1:3001'
          });
        }

        $('.unmute-video').show()
        $('.mute-video').hide()
        webrtc.startLocalVideo()

        webrtc.on('videoRemoved', function() {
            $('.call-box').hide()
            $('.incoming-call-controll').hide();
            $('.call-controll').hide();
            location.reload();
        })
        var type = $(this).data('type');
        webrtc.joinRoom($(this).parent('.incoming-call-controll').data('room-id'), function(err, roomDescription) {
            if (err) {
                swal("Oops...", "Something went wrong!", "error");
            } else {
                webrtc.pauseVideo()
                if (type === 'video') {
                    webrtc.resumeVideo()
                    $('.unmute-video').hide()
                    $('.mute-video').show()
                }
                $('.incoming-call-controll').hide();
                $('.call-controll').show();
                $('.call-box').show()
            }
        })
    })
    $(document).on('click', '.decline', function() {
        $('.call-box').hide()
        $('.incoming-call-controll').hide();
        $('.call-controll').hide();
        socket.emit('decline-call-server', {
            roomId: $('.incoming-call-controll').data('room-id'),
            rId: $('.incoming-call-controll').data('r-id')
        })
    })
    $(document).on('click', '.cancel-call', function() {
        console.log('here');
        if (webrtc) {
            webrtc.leaveRoom()
            webrtc.stopLocalVideo()
            location.reload();
        }
        $('#start').show()
        $('.call-box').hide()
        $('.incoming-call-controll').hide();
        $('.call-controll').hide();
        socket.emit('decline-call-server', {
            roomId: $('.incoming-call-controll').data('room-id'),
            rId: $('.incoming-call-controll').data('r-id')
        })
    })

    $(document).on('click', '.mute-video', function() {
        if (webrtc) {
            webrtc.pauseVideo()
            $('.unmute-video').show()
            $('.mute-video').hide()
        }
    })
    $(document).on('click', '.unmute-video', function() {
        if (webrtc) {
            webrtc.resumeVideo()
            $('.unmute-video').hide()
            $('.mute-video').show()
        }
    })
    $(document).on('click', '.mute-sound', function() {
        if (webrtc) {
            webrtc.mute()
            $('.unmute-sound').show()
            $('.mute-sound').hide()
        }
    })
    $(document).on('click', '.unmute-sound', function() {
        if (webrtc) {
            webrtc.unmute()
            $('.unmute-sound').hide()
            $('.mute-sound').show()
        }
    })
    $(document).on('click', '.shareScreen', function() {
        if (webrtc) {
            webrtc.shareScreen(function(err, data) {
                if (err) {
                    swal('plugin not installed')
                } else {
                    $('.shareScreen').hide()
                    $('.unshareScreen').show()
                }
            })
        }
    })
    $(document).on('click', '.unshareScreen', function() {
        if (webrtc) {
            webrtc.stopScreenShare()
            $('.unshareScreen').hide()
            $('.shareScreen').show()
        }
    })
    $('#start').click(function() {
        if (webrtc) {
            webrtc.leaveRoom()
            webrtc.stopLocalVideo()
        } else {

            var media = {
                video: true,
                audio: true
            }
            webrtc = new SimpleWebRTC({
                // the id/element dom element that will hold "our" video
                localVideoEl: 'localVideoC',
                // the id/element dom element that will hold remote videos
                remoteVideosEl: 'remV',
                // immediately ask for camera access
                autoRequestMedia: false,
                media: media,
                // localVideo:{mirror:false},
                peerConnectionConfig: configuration,
                url: 'https://thestudentstop.com:3001'
                // url: 'https://127.0.0.1:3001'
            });
        }
        webrtc.startLocalVideo()

        webrtc.createRoom(makeid(), function(err, roomDescription) {
            if (err) {
                swal("Oops...", "Something went wrong!", "error");
            } else {
                $('#start').hide()
                $('.unmute-video').hide()
                $('.mute-video').show()
                $('.call-controll-v').show()
                room = roomDescription
                socket.emit('vc-s', {
                    vcId: $('#class_id').val(),
                    room: roomDescription
                })
            }
        })
    })
    $(document).on('click', '.cancel-vc', function() {
        if (webrtc) {
            webrtc.leaveRoom()
            webrtc.stopLocalVideo()
        }
        $('#start').show()
        $('.call-box').hide()
        $('.incoming-call-controll').hide();
        $('.call-controll-v').hide();
        socket.emit('decline-call-v-server', {
            roomId: room,
            vcId: $('#class_id').val()
        })
    })

})
// $(document).ready(function () {
// function gotStream(stream) {
//   $('#localVideo').attr('src',URL.createObjectURL(stream))
//   localStream = stream;
//
//   pc.addStream(stream);
//   createOffer()
// }
// function createOffer() {
//   pc.createOffer(
//     gotLocalDescription,
//     function(error) { console.log(error) },
//     { 'mandatory': { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true } }
//   );
// }
// function gotLocalDescription(description){
//   pc.setLocalDescription(description);
//   sendMessage(description);
// }
// function gotLocalDescription1(description){
//   pc.setLocalDescription(description);
//   sendMessage(description);
// }
// function gotIceCandidate(event){
//   if (event.candidate) {
//     sendMessage({
//       type: 'candidate',
//       label: event.candidate.sdpMLineIndex,
//       id: event.candidate.sdpMid,
//       candidate: event.candidate.candidate
//     });
//   }
// }
// function createAnswer() {
//   pc.createAnswer(
//     gotLocalDescription1,
//     function(error) { console.log(error) },
//     { 'mandatory': { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true } }
//   );
// }
// function gotRemoteStream(event){
//   if(!$('.nav-video').hasClass('active'))
//   $('.nav-video').trigger('click')
//   in_call = true
//   document.getElementById("remoteVideo").src = URL.createObjectURL(event.stream);
// }
//
//
//
//   // navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
//   var localStream,pc,recipient_id,in_call = false;
//   // var PeerConnection = window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
//   // var IceCandidate = window.mozRTCIceCandidate || window.RTCIceCandidate;
//   // var SessionDescription = window.mozRTCSessionDescription || window.RTCSessionDescription;
//   var server = {
// 	iceServers: [
// 		{url: "stun:23.21.150.121"},
// 		{url: "stun:stun.l.google.com:19302"},
// 		{url: "stun:stun1.l.google.com:19302"},
// 		{url: "stun:stun2.l.google.com:19302"},
// 		{url: "stun:stun3.l.google.com:19302"},
// 		{url: "stun:stun4.l.google.com:19302"},
// 		{url: "stun:stun01.sipphone.com"},
// 		{url: "stun:stun.voipstunt.com"},
// 		{url: "stun:stun.voxgratia.org"},
// 		{url: "stun:stun.xten.com"},
// 	]
// };
// // stun.l.google.com:19302
// // stun1.l.google.com:19302
// // stun2.l.google.com:19302
// // stun3.l.google.com:19302
// // stun4.l.google.com:19302
// // stun01.sipphone.com
// // stun.ekiga.net
// // stun.fwdnet.net
// // stun.ideasip.com
// // stun.iptel.org
// // stun.rixtelecom.se
// // stun.schlund.de
// // stunserver.org
// // stun.softjoys.com
// // stun.voiparound.com
// // stun.voipbuster.com
// // stun.voipstunt.com
// // stun.voxgratia.org
// // stun.xten.com
// var options = {
// 	optional: [
// 		{DtlsSrtpKeyAgreement: true}, // требуется для соединения между Chrome и Firefox
// 		{RtpDataChannels: true} // требуется в Firefox для использования DataChannels API
// 	]
// }
//   $("#hang").click(function () {
//       pc.close();
//       pc = null;
//       sendMessage({type:'endCall'})
//       recipient_id = null
//       $('#localVideo').removeAttr('src')
//       $('#remoteVideo').removeAttr('src')
//     })
//   $(".startVideo").click(function () {
//     pc = new RTCPeerConnection(server, options);
//     pc.onicecandidate = gotIceCandidate;
//     pc.onaddstream = gotRemoteStream;
//
//     recipient_id = $(this).data('recipient-id')
//     var sender_id = $(this).data('sender-id')
//     navigator.mediaDevices.getUserMedia({
//         audio: true,
//         video: true
//       })
//       .then(gotStream)
//       .catch(function(e) {
//         alert('getUserMedia() error: ' + e.name);
//       });
//     $('.nav-video').trigger('click')
//   })
//   function sendMessage(message){
//     console.log(message.type + "  "+ recipient_id);
//   socket.emit('video_message',{ recipient_id:recipient_id,message:message});
//   }
//   function sendMessage2(message){
//     console.log(message.type + "  "+ recipient_id);
//     socket.emit('video_message',message);
//   }
//
//   socket.on('video_message', function (message){
//     if (!pc){
//       pc = new RTCPeerConnection(server, options);
//       pc.onicecandidate = gotIceCandidate;
//       pc.onaddstream = gotRemoteStream;
//       pc.oniceconnectionstatechange = function() {
//        if(pc.iceConnectionState == 'leaveRoomed') {
//           message = 'UDP connection is dropped. Try ReJoin';
//           alert(message);
//           pc.close();
//           pc = null;
//           recipient_id = null
//           $('#localVideo').removeAttr('src')
//           $('#remoteVideo').removeAttr('src')
//        }
//      };
//     }
//     if (message.type === 'offer') {
//       pc.setRemoteDescription(new RTCSessionDescription(message));
//       if (recipient_id == message.recipient_id) {
//         var getPhone = false
//       }else{
//         var getPhone = ((typeof $('#localVideo').attr('src')=="undefined")||(in_call))?confirm("Ответить?"):false;
//       }
//       if(getPhone){
//         recipient_id = message.recipient_id
//         navigator.mediaDevices.getUserMedia({
//           audio: true,
//           video: true
//         })
//         .then(gotStream)
//         .catch(function(e) {
//           alert('getUserMedia() error: ' + e.name);
//         });
//           createAnswer();
//     }}
//     else if (message.type === 'answer') {
//       pc.setRemoteDescription(new RTCSessionDescription(message));
//     }
//     else if (message.type === 'candidate') {
//       var candidate = new RTCIceCandidate({sdpMLineIndex: message.label, candidate: message.candidate});
//       pc.addIceCandidate(candidate);
//     }
//     else if (message.type === 'endCall') {
//       pc.close();
//       pc = null;
//       recipient_id = null
//       $('#localVideo').removeAttr('src')
//       $('#remoteVideo').removeAttr('src')
//     }
//   });
// })
