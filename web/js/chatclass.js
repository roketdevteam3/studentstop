var classChat = (function () {
    var socket = {},
        user_id = '',
        class_id = '',
        files_msg = [],
        message_box = {},
        classChat = {};

    classChat.init = function () {
//        socket = io.connect('localhost:2700');
        socket = io.connect('https://thestudentstop.com:2700');
        user_id = $("input#user_id").val();
        class_id = $("input#class_id").val();
        //alert(class_id);
        message_box = $("#text-message-class");

        socket.on('connect', function () {
            socket.emit('loginClass', {
                user_id: user_id,
                class_id: class_id
            });

            classChat.setListener(socket);
        });

        socket.on('JoinChat', function (data) {
            console.info(data);
        });
    };
    classChat.setListener = function (socket) {
        $("#send-message-class").click(function () {
            if (message_box.val() != '' || files_msg.length > 0) {
                socket.emit('classMessage', {
                    user_id: user_id,
                    class_id: class_id,
                    message: message_box.val(),
                    files: files_msg
                });
                message_box.val('');
                $(".select-file-box .dz-preview").remove();
                $(".select-file-box .user_photos").removeClass('dz-started');
                $(".select-file-box").removeClass('fadeInUp');
                files_msg = [];
            }
        });

        $(".send-message .add-files").click(function () {
            $(".select-file-box").addClass('fadeInUp');
        });
        $(".select-file-box .close-box").click(function () {
            $(this).parent().removeClass('fadeInUp');
        });

        socket.emit('getListMessage', {
            class_id: class_id
        });

        socket.emit('getOnlineUsers', {
            class_id: class_id
        });

        socket.on('reciveClassMessage', function (data) {
            classChat.addMessage(data);
        });
        socket.on('reciveListMessage', function (data) {
            console.log(data);
            classChat.addMessageHistory(data);
            
        });
        socket.on('responseOnlineUsers', function (data) {
            classChat.addListUsers(data);
        });
        socket.on('JoinChat', function (data) {
            classChat.addListUsers(data);
        });
        socket.on('LeaveChat', function (data) {
            $(".class_users").find("#user-" + data).remove();
        });


        function deleteFile(file_name) {
            $.ajax({
                type: 'POST',
                url: '../../../course/default/deletefilechat/' + class_id,
                data: {file_name: file_name},
                success: function (response) {
                }
            });
        }

        var myDropzone = new Dropzone($('.user_photos')[0], { // Make the whole body a dropzone
            maxFilesize: 50,
            acceptedFiles: 'video/*, image/bmp, image/gif, image/jpeg, image/png, application/x-7z-compressed, application/x-gtar, application/zip, application/x-rar-compressed, application/x-zip-compressed, application/msword, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.openxmlformats-officedocument.wordprocessingml.template, application/vnd.ms-word.document.macroEnabled.12, application/vnd.ms-word.template.macroEnabled.12, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.openxmlformats-officedocument.spreadsheetml.template, application/vnd.ms-excel.sheet.macroEnabled.12, application/vnd.ms-excel.template.macroEnabled.12, application/vnd.ms-excel.addin.macroEnabled.12, application/vnd.ms-excel.sheet.binary.macroEnabled.12, application/vnd.ms-powerpoint, application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation, application/vnd.openxmlformats-officedocument.presentationml.template, application/vnd.openxmlformats-officedocument.presentationml.slideshow, application/vnd.ms-powerpoint.addin.macroEnabled.12, application/vnd.ms-powerpoint.presentation.macroEnabled.12, application/vnd.ms-powerpoint.template.macroEnabled.12,application/vnd.ms-powerpoint.slideshow.macroEnabled.12',
        });

        myDropzone.on("sending", function (file) {
            //console.log(file);
            //  file.name = file.name+Math.random().toString(36).substring(7);
            // Hookup the start button
            //file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file); };
        });
        // Update the total progress bar

        myDropzone.on("complete", function (response) {
            if (response.status == 'success') {
                var type = response.type.split("/");


                files_msg.push({
                    lastModified: response.lastModified,
                    type: type[0],
                    src: response.xhr.response
                });

                $(response.previewElement).prepend('<i class="fa fa-times removePhoto fa-2x" title="delete" style="color:#DBDBDB;cursor:pointer;right:-5px;top:-10px;position:absolute;z-index:999" data-name="' + response.name + '"></i>');
            }
        });
        myDropzone.on("removedfile", function (response) {
            var removeItem = response.xhr.response;
            deleteFile(response.xhr.response);
            files_msg.forEach(function (element, index, array) {
                if ((element.lastModified == response.lastModified)) {
                    array.splice(index, 1);
                }
            });
        });

        $(document).on('click', '.removePhoto', function () {
            var shos = $(this).parent();

            var name_photo = $(this).data('name');
            $.each(myDropzone.files, function (key, value) {
                if (value.name == name_photo) {
                    myDropzone.removeFile(myDropzone.files[key]);
                }
            });
        });

    };

    classChat.addListUsers = function (data) {
        var tpl = '',
            users_box = $(".class_users");
        data.forEach(function (element, index, array) {
            if (users_box.find("#user-" + element.id).length == 0) {
                tpl = '<li class="has-action-left has-action-right" id="user-' + element.id + '"><div class="list-action-left"><img src="/images/users_images/' + element.avatar + '" class="face-radius" alt=""></div><div class="list-content"><span class="title" style="color: #3e50b4;">' + element.name + ' ' + element.surname + '</span><span class="caption"></span></div><div class="list-action-right"><span class="top">online</span></div></li>';
                users_box.append(tpl);
                users_box.scrollTop(users_box.prop("scrollHeight"));
            }
        });
    };

    classChat.addMessageHistory = function (data) {
        var tpl = '',
            tpl_doc = '',
            message_box = $(".message-class-container > .messages");
        var tmp_id = -1;
        data.forEach(function (element, index, array) {
            //console.log(element);
            if (tmp_id != element.id) {
                var msq_text = '<p>' + element.message + '</p>';
                var msq_cadd = element.created_add;
                var avatar = '/images/default_avatar.jpg';
                if((element.avatar != null) && (element.avatar != '')){
                     avatar = '/images/users_images/'+element.avatar;
                }
                var side = 'right';
                if (element.user_id == user_id) {
                    side = 'left'
                }
                tpl_doc = classChat.addFilesHistoryChat(data, element.id);

                tpl = '<div class="message animate ' + side + '">\n\
<div class="user-picture-wrap">\n\
<img style="width:100%;" src="'+ avatar + '" class="user-picture" alt="">\n\
</div>\n\
<span >'+element.name+' '+element.name+'</span>\n\
<span class="time"  style="margin-bottom:0px;">' + msq_cadd + '</span> <div class="message-text"  style="margin-bottom:10px;">' + tpl_doc + msq_text + '</div></div>';
                message_box.prepend(tpl);
                message_box.scrollTop(message_box.prop("scrollHeight"));
            } else {

            }
            tmp_id = element.id;
        });
    };

    classChat.addFilesHistoryChat = function (data, id) {
        var tpl = '';
            //alert(class_id);
        data.forEach(function (element, index, array) {
            if (element.id == id) {
                if(element.file_src){
                        switch (element.mime_type) {
                            case 'image':
                            {
                                tpl += '<a href="/files/' + class_id + '/' + element.file_src + '" class="fresco" data-fresco-group="example-' + id + '" data-fresco-caption="Sir Joshua Reynolds, The Countess of Dartmouth, 1757"> <img src="/files/' + class_id + '/' + element.file_src + '" style="height:40px;"/> </a>';
                                break;
                            }
                            default:
                            {

                                tpl += '<a target="_blank" href="/files/' + class_id + '/' + element.file_src + '">' + element.file_src + '</a>';
                            }
                        }                    
                }else{
                    tpl = '';
                }
            }
        });
        return tpl;
    };

    classChat.addMessage = function (data) {
        var side = 'right',
            tpl = '',
            tpl_docs = classChat.addFilesMessage(data.files),
            message_box = $(".message-class-container > .messages");

        if (data.user_id == user_id) {
            side = 'left'
        }
        //console.log(data);
        var avatar = '/images/default_avatar.jpg';
        if((data.avatar != null) && (data.avatar != '')){
            avatar = '/images/users_images/'+data.avatar;
        }

        tpl = '<div class="message animate ' + side + '"><div class="user-picture-wrap">\n\
<img style="width:100%;" src="' + avatar + '" class="user-picture" alt="">\n\
</div>\n\
<span>'+data.name+' '+data.name+'</span>\n\
<span class="time" style="margin-bottom:0px;">' + data.created_add + '</span>\n\
<div class="message-text" style="margin-bottom:10px;">' + tpl_docs + '<p>' + data.message + ' </p> </div> </div>';
        message_box.prepend(tpl);
        message_box.scrollTop(message_box.prop("scrollHeight"));
    };

    classChat.addFilesMessage = function (files) {
        var tpl = '';
        files.forEach(function (element, index, array) {
            console.log(element);
            switch (element.type) {
                case 'image':
                {
                    tpl += '<a href="/files/' + class_id + '/' + element.src + '" class="fresco" data-fresco-group="example-' + id + '" data-fresco-caption="Sir Joshua Reynolds, The Countess of Dartmouth, 1757"> <img src="/files/' + class_id + '/' + element.src + '"/> </a>';
                    break;
                }
                default:
                {
                    tpl += '<a target="_blank" href="/files/' + class_id + '/' + element.src + '">' + element.src.split('/')[1] + '</a>';
                }
            }
        });
        console.log(tpl);
        return tpl;
    };

    return classChat;
})();
















$(document).ready(function () {
    classChat.init();
    
    
    
    $(document).on('click', '.eventsTabsButton', function () {
        $('.ClassTabscontainerInfo').show();
        $('.ClassTabscontainerChat').hide('');
        $('.ClassTabscontainerInfo').html('');
        $(this).parent().find('li').removeClass('active');
        $(this).addClass('active');
        
        $('.ClassTabscontainerInfo').append('<div class="btn-wrap">'+
            '<button class="btn addNewClassEvent " data-toggle="modal" data-target="#eventCreateModal">Add event</button>'+
            '</div><div class="eventContainer"></div>');
            $('.eventContainer').append('<div class="event-wrap">'+
                                                            '<div class="col-md-10 no-padding-left">'+
                                                                '<div id="paginator"></div>'+
                                                            '</div>'+
                                                            '<div class="col-md-2">'+
                                                                '<button class="btn calendarToday">Today</button>'+
                                                            '</div>'+
                                                            '<div class="clearfix"></div>'+
                                                    '</div>'+
                                                    '<div class="eventBlock">'+

                                                    '</div>');
        var options = {
            onSelectedDateChanged: function(event, date) {
                dateChange(date);
            },
            itemWidth:50
        }
        $('#paginator').datepaginator(options);
        var class_id = $('#class_id').val();
        var now = new Date();
        dateChange(now);
        /*$.ajax({
            type: 'POST',
            url: '../../../course/default/getclassevent',
            data: {class_id:class_id},
            dataType:'json',
            success: function(response){
                $('.eventContainer').html();
                for(var key in response){
                    var event = response[key];
                    $('.eventContainer').append('<div class="col-sm-12" style="margin-top:10px;background-color:white;">'+
                            '<span>'+event.date_start+' - '+event.date_end+'</span>'+
                            '<h5>'+event.title+'</h5>'+
                            '<p>'+event.content+'</p>'+
                            '</div><hr>');
                }
            }
        }); */
    });
    
    $( ".formCreateClassEvent" ).submit(function(event) {
        event.preventDefault();
        var title = $('[name=event_title]').val();
        var place_type = $('[name=event_place_type]').val();
        var place_id = $('[name=event_place_id]').val();
        var class_id = $('[name=event_class_id]').val();
        var image = $('[name=event_image]').val();
        var short_desc = $('[name=event_short_desc]').val();
        var date_start = $('[name=event_date_start]').val();
        var date_end = $('[name=event_date_end]').val();
        var content = $('[name=event_content]').val();
        $.ajax({
            type: 'POST',
            url: '../../../course/default/addclassevent',
            data: {title:title, place_type:place_type,  place_id:place_id, class_id:class_id, image:image,
                    short_desc:short_desc, date_start:date_start, date_end:date_end, content:content},
            dataType:'json',
            success: function(response){
                $('#eventCreateModal').modal('hide');
                swal("Event added!", "", "success");
                var now = new Date();
                dateChange(now);
            }
        })
        
    })
    
    $( ".add_comment_to_post_modal" ).submit(function(event) {
        event.preventDefault();
    });
    
    $( ".formCreateClassQuestion" ).submit(function(event) {
        event.preventDefault();
        var question_title = $('[name=question_question_title]').val();
        var question_text = $('[name=question_question_text]').val();
        var question_price = $('[name=question_price]').val();
        var question_category = $('[name=question_category]').val();
        var question_privacy = $('[name=question_privacy]').val();
        var question_class_id = $('#class_id').val();

        $.ajax({
            type: 'POST',
            url: '../../../course/default/addclassquestion',
            data: {question_title:question_title, question_text:question_text,  question_price:question_price,
                question_category:question_category, question_privacy:question_privacy,
                    question_class_id:question_class_id},
            dataType:'json',
            success: function(response){
                if(response.status != 'error'){
                    $('#questionCreateModal').modal('hide');
                    swal("Question added!", "", "success");
                    $('.questionContainer').prepend('<div style="margin-top:10px;background-color:white;" class="col-sm-12">'+
                            '<h3>'+question_title+'</h3>'+
                            '<p>'+question_text+'</p>'+
                            '<p>Price - '+question_price+'</p>'+
                            '</div>');
                }else{
//                    alert('error');
                }
            }
        })
    })
    
    
    
    
    
    function changeCategory(category_id){
        $.ajax({
            type: 'POST',
            url: '../../getchildqusetioncategory',
            data: {category_id:category_id},
            dataType:'json',
            success: function(response){
                $('#question-category_id').html('');
                for(var key in response){
                    var obje = response[key];
                    for(var key2 in obje){
                        $('#question-category_id').append('<option value="'+key2+'">'+key+'</option>');
                    }
                }
            }
        });
    }
    
    if($('#question-category').length){
        var categoryParentId = $('#question-category').val();
        changeCategory(categoryParentId);
    }
    
    
    $(document).on('change','#question-category',function(){
        var categoryParentId = $('#question-category').val();
        changeCategory(categoryParentId);
    });
    
    $(document).on('click', '.classViewQuestion', function () {
        var question_id = $(this).data('question_id');
        $('.modalRightPages').show();
        $('.modalRightPages').html('');
        $.ajax({
            type: 'POST',
            url: '../../course/default/classgetquestion',
            data: {question_id:question_id},
            dataType:'json',
            success: function(response){
                console.log(response);
                var user_info = response.user_info;
                var question = response.question;
                var answers = response.answers;
                $('.modalRightPages').append('<div class="col-sm-12">'+
                    '<img src="../../images/users_images/'+user_info.avatar+'" style="width:40px;">'+
                    '<h4>'+user_info.username+'</h4>'+
                    '<h5 style="text-align:center;">'+question.question_title+'</h5>'+
                    '<p>'+question.question_text+'</p>'+
                    '<p><b>Price - </b>'+question.question_text+'</p>'+
                    '<hr>'+
                    '<button class="btn btn-primary">Add answer</button>'+
                '</div><hr>'+
                '<div class="col-sm-12">'+
//                    '<form>'+
//                        '<div class="form-group field-answer-show_text">'+
//                            '<label class="control-label" for="answer-show_text">Show Text</label>'+
//                            '<textarea id="answer-show_text" class="form-control" name="answer_show_text"></textarea>'+
//                        '</div>'+
//                        '<div class="form-group field-answer-answer_text">'+
//                            '<label class="control-label" for="answer-answer_text">Answer Text</label>'+
//                            '<textarea id="answer-answer_text" class="form-control" name="answer_answer_text"></textarea>'+
//                        '</div>'+
//                        '<div class="form-group field-answer-money">'+
//                            '<label class="control-label" for="answer-money">Money</label>'+
//                            '<input type="number" id="answer-money" class="form-control" name="answer_money">'+
//                        '</div>'+
//                        '<button class="btn btn-primary addAnswer">Add answer</button>'+
//                    '</form>'+
                    '<div class="col-sm-12 textAnswers">'+
                            '<h3 style="text-align:center;">Answers</h3>'+
                    '</div>'+
                    '<div class="col-sm-12 QuestionAnswers">'+
                            
                    '</div>'+
                '</div>');
                
                for(var key in answers){
                    var answer = answers[key]
                    $('.QuestionAnswers').append('<div class="row" style="margin-top:10px;">'+
                            '<div class="col-sm-3">'+
                                '<img src="../../../images/users_images/'+answer.avatar+'" style="width:100%;">'+
                            '</div>'+
                            '<div class="col-sm-9">'+
                                '<h4>'+answer.name+' '+answer.surname+'</h4>'+
                                '<p>'+answer.show_text+'</p>'+
                                '<button class="btn btn-view">Show answer</button>'+
                            '</div>'+
                        '</div>');
                }
            }
        });
        
    });
    
    $(document).on('click', '.QuestionTabsButton', function () {
        $('.ClassTabscontainerInfo').show();
        $('.ClassTabscontainerChat').hide('');
        $('.ClassTabscontainerInfo').html('');
        $(this).parent().find('li').removeClass('active');
        $(this).addClass('active');
        
        $('.ClassTabscontainerInfo').append('<div class="btn-wrap">'+
            '<button class="btn btn-primary addNewClassEvent " data-toggle="modal" data-target="#questionCreateModal">Add question</button>'+
            '</div><div class="col-sm-12 questionContainer"></div>');        
        var class_id = $('#class_id').val();
        $.ajax({
            type: 'POST',
            url: '../../course/default/classquestion',
            data: {class_id:class_id},
            dataType:'json',
            success: function(response){
                console.log(response);
                $('.questionContainer').html('');
                for(var key in response){
                    var question = response[key];
                    $('.questionContainer').append('<div style="margin-top:10px;background-color:white;" class="col-sm-12">'+
                            '<h3>'+question.question_title+'</h3>'+
                            '<p>'+question.question_text+'</p>'+
                            '<p>Price - '+question.price+'</p>'+
                            '<button class="btn btn-info classViewQuestion" data-question_id="'+question.id+'" >Show more info</button>'+
                            '</div>');
                }
            }
        });
        
    });
    
    $(document).on('click', '.chatTabsButton', function () {
        $('.ClassTabscontainerInfo').hide();
        $('.ClassTabscontainerChat').show('');        
        $(this).parent().find('li').removeClass('active');
        $(this).addClass('active');
    });
    
    
    
    
    
    
    
    
    if($('.topClassContainer').length){
        getTopUsers();
    }
    
    function getTopUsers(){
        var class_id = $('#class_id').val();
        $.ajax({
            type: 'POST',
            url: '../../../course/default/getclasstopuser',
            data: {class_id:class_id},
            beforeSend: function( xhr ) {
                $('.classSearchContainer').append('<div style="text-align:center;"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>');
            },
            dataType:'json',
            success: function(response){
                    $('.topClassContainer').html('')
                    var students_array = [];
                    for(var key in response){
                        var student = response[key];
                        students_array.push({info: student.user_info, rating: student.user_rating});
                    }

                    students_array.sort(function(a,b) {
                        return a.rating + b.rating;
                    });
                    var my_id = $('.message-count').find('.user').data('id');
                    for(var prof_key in students_array){
                        var info = students_array[prof_key].info;
                        var rating = students_array[prof_key].rating;
                        var mail_button = '';
                        if(my_id != info.id){
                            mail_button = '<button class="btn startChat startChatFriend" data-sender-id="'+my_id+'" data-recipient-id="'+info.id+'">mail to user</button>';
                        }
                        $('.topClassContainer').append('<table class="table class-table">'+
                                '<tr>'+
                                    '<td>'+
                                        '<div class="user-img">'+
                                            '<img class="" src="../../../images/users_images/'+info.avatar+'">'+
                                        '</div>'+
                                        '<span class="user-name">'+info.username+' '+info.surname+'</span>'+
                                    '</td>'+
                                    '<td>'+
                                        '<input id="studentInputRatingClass'+info.id+'" name="studentRating" value="'+rating+'" class="rating-loading">'+
                                    '</td>'+
                                    '<td>'+
                                        mail_button+
                                    '</td>'+
                                '</tr>'+
                            '</table>');
                        $('#studentInputRatingClass'+info.id).rating({displayOnly: true, step: 0.1, size:'xs'});
                    }
            }
        });
    }
    
    $(document).on('click', '.topProfessor', function(){
        $(this).parent().find('li').removeClass('active');
        $(this).addClass('active');
        getTopProfessor();
    });
    $(document).on('click', '.topStudents', function(){
        $(this).parent().find('li').removeClass('active');
        $(this).addClass('active');
        getTopUsers();
    });
    
    $(document).on('click', '.topClass', function(){
        $(this).parent().find('li').removeClass('active');
        $(this).addClass('active');
        getTopClass();
    });
    
    function getTopClass(){
        var class_id = $('#class_id').val();
        $.ajax({
            type: 'POST',
            url: '../../../course/default/getclasstop',
            data: {class_id:class_id},
            beforeSend: function( xhr ) {
                $('.classSearchContainer').append('<div style="text-align:center;"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>');
            },
            dataType:'json',
            success: function(response){
                $('.topClassContainer').html('')
                var class_array = [];
                for(var key in response){
                    var clas = response[key];
                    class_array.push({info: clas.class_info, rating: clas.class_rating});
                }
//                
                class_array.sort(function(a,b) {
                    return a.rating + b.rating;
                });
                console.log('------');
                console.log(class_array);
                console.log('------');
                for(var class_key in class_array){
                    var info = class_array[class_key].info;
                    var rating = class_array[class_key].rating;
                    var view_button = '';
                    view_button = '';
                    $('.topClassContainer').append('<table class="table class-table">'+
                        '<tr>'+
                            '<td style="width: 60%">'+
                                '<span>'+info.name+'</span>'+
                            '</td>'+
                            '<td>'+
                                '<input id="classInputRatingClass'+info.id+'" name="clRating" value="'+rating+'" class="rating-loading">'+
                            '</td>'+
                            '<td>'+
                                view_button+
                            '</td>'+
                        '</tr>'+
                    '</table>');
                    $('#classInputRatingClass'+info.id).rating({displayOnly: true, step: 0.1, size:'xs'});
                }
            }
        });
    }
    function getTopProfessor(){
        var class_id = $('#class_id').val();
        $.ajax({
            type: 'POST',
            url: '../../../course/default/getclasstopprofessor',
            beforeSend: function( xhr ) {
                $('.classSearchContainer').append('<div style="text-align:center;"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>');
            },
            data: {class_id:class_id},
            dataType:'json',
            success: function(response){
                $('.topClassContainer').html('')
                var professor_array = [];
                for(var key in response){
                    var professor = response[key];
                    professor_array.push({info: professor.professor_info, rating: professor.professor_rating});
                }
                
                professor_array.sort(function(a,b) {
                    return a.rating + b.rating;
                });
                var my_id = $('.message-count').find('.user').data('id');
                for(var prof_key in professor_array){
                    var info = professor_array[prof_key].info;
                    var rating = professor_array[prof_key].rating;
                    var mail_button = '';
                    if(my_id != info.id){
                        mail_button = '<button class="btn startChat startChatFriend" data-sender-id="'+my_id+'" data-recipient-id="'+info.id+'">mail to user</button>';
                    }
                    $('.topClassContainer').append('<table class="table class-table">'+
                        '<tr>'+
                            '<td>'+
                                '<div class="user-img">'+
                                    '<img src="../../../images/users_images/'+info.avatar+'">'+
                                '</div>'+
                                '<span class="user-name">'+info.username+' '+info.surname+'</span>'+
                                '</td>'+
                                '<td>'+
                                    '<input id="proffesorInputRatingClass'+info.id+'" name="profRating" value="'+rating+'" class="rating-loading">'+
                                '</td>'+
                                '<td>'+
                                    mail_button+
                                '</td>'+
                            '</tr>'+
                        '</table>');
                    $('#proffesorInputRatingClass'+info.id).rating({displayOnly: true, step: 0.1, size:'xs'});
//            
                }
            }
        });
    }
    
    
    
    
    
    
    
    
    $(document).on('click', '.viewClassEvent',function(){
        var event_id = $(this).data('event_id');
        $('#modalShowPost').modal('show');
        $('.eventShowModalContainer').html('');
        
        $.ajax({
            type: 'POST',
            url: '../../../course/default/classevent',
            data: {event_id:event_id},
            dataType:'json',
            success: function(response){
                console.log(response);
                var event_info = response.event;
                var user_info = response.user_info;
                $('.eventShowModalContainer').append('<div class="modal-body modalContent">'+
					'<div class="left-content" style="border-right:1px solid grey;">'+
						'<div class="post-header">'+
							'<div class="avatar"><img src="../../../images/users_images/'+user_info.avatar+'">'+'</div>'+
							'<div class="info">'+
								'<h4 class="username">'+user_info.username+'</h4>'+
								'<h6 class="postCreate">'+event_info.date_create+'</h6>'+
							'</div>'+
							'<div class="clearfix"></div>'+
						'</div>'+
						'<h4 class="">'+event_info.title+'</h4>'+
                                                '<h6>'+event_info.date_start+' - '+event_info.date_end+'</h6>'+
						'<div class="postContent">'+event_info.content+'</div>'+
						'<div class="postFilesBlock"></div>'+
					'</div>'+
					'<div class="right-content">'+
						'<form class="add_comment_to_post_modal " data-post_id="">'+
							'<div class="comment-area">'+
								'<textarea class="form-control comment-input" name="newCommentPost" rows="1" placeholder="Write your comment ..."></textarea>'+
								'<input type="hidden" name="commentFilesInputModal" value="">'+
								'<div class="add-files btn-ripple clickCommentFilesAddModal addFileCommentModal dz-clickable" data-post_id=""></div>'+
								'<div class="previewFileBlockCommentModal"></div>'+
							'</div>'+
							'<div class="btn-comment-wrap">'+
								'<input type="submit" class="btn add-comment btn-ripple" name="addComment" value="Add">'+
							'</div>'+
						'</form>'+
						
						'<div class="commentContentBlock">'+
						'</div>'+
					'</div>'+
					'<div style="clear:both;"></div>'+
				'</div>');
                        
            }
        });
        
    })
    
    
    
    
    function dateChange(date){
            var date = new Date(date);
            var year = date.getFullYear()
            var month = date.getMonth()+1;
            if(month < 10){
                month = '0'+month;
            }
            var day = date.getDate()
            var dayClick = year+'-'+month+'-'+day;
            var class_id = $('#class_id').val();
            $.ajax({
                type: 'POST',
                url: '../../../university/default/geteventsajax',
                data: {date:dayClick,class_id:class_id},
                dataType:'json',
                success: function(response){
                    var month = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
                    var university_slug = $('.university_slug').val();
                    $('.eventBlock').html('')
                    if(response.length == 0){
                        $('.eventBlock').append('<span class="nf-event">sorry but events not found</span>')
                    }
                    //var date_start = 
                    for(var key in response){
                        if(response[key].type = 'news'){
                            var date_start = new Date(response[key].date_create);
                        }else{
                            var date_start = new Date(response[key].date_start);
                        }
                        var date_create = new Date(response[key].date_create);

                        $('.eventBlock').append(
                            '<div class="event-wrap">'+
                                '<div class="e-img">'+
                                   '<img class="img-responsive" src="'+response[key].image+'">'+
                                   '<div class="e-calendar">'+
                                       '<i class="fa fa-calendar"> </i> <span>'+month[date_start.getMonth()]+' '+date_start.getDate()+'</span>'+
                                   '</div>'+
                                '</div>'+
                                '<div class="e-info">'+
                                   '<h4 class="e-date"><span>'+month[date_create.getMonth()]+'</span>'+date_create.getDate()+':'+date_create.getHours()+':'+date_create.getMinutes()+'</h4>'+
                                   '<h1 class="e-head">'+response[key].title+'</h1>'+
                                   '<p class="e-desc">'+response[key].short_desc+'</p>'+
                                    '<a class="btn btn-view viewClassEvent" data-event_id="'+response[key].id+'">View</a>'+
                                '</div>'+
                            '</div>'
                        );
                    }
                }
            })
        }
   	
        $('.calendarToday').click(function(){
            var dateT = new Date;
            dateChange(dateT);
            $('#paginator').datepaginator({
                onSelectedDateChanged: function(event, date){
                    dateChange(date);
                },
                itemWidth:50
            });
        });
        
       
    
});