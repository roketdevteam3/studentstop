/**
 * Created by MackRais
 */
$(document).ready(function(){

    $(document).on('click', '.all_massage' ,function(){
        $('.correspondence-user-col').show();
        $('.message-send-col').hide();
    });
    
    $(window).click(function() {
        $('.userFilesChoise').hide();
        $('.classFilesChoise').hide();
        $('.user-layer').hide();
        $('.modalRightPages').hide();
        $('.modalRightPages').hide();
        $('.modalNotificationRightBlock').hide();
        
    });
    
    $(document).on('click','.buttonAboutRightPages, .modalNotificationRightBlock, .nav-notification, .modalRightPages,.add_post_jobs,.viewModalPost, .classViewQuestion',function(event){
        $('.user-layer').hide();
        event.stopPropagation();
    });

//    $('#menucontainer').click(function(event){
//    });
    
    
//    $(document).on('click','.startChatFriend',function(){
//        event.stopPropagation();
//        $('.user-layer').show();
//    });
    
    $(document).on('click','.nav-user,.startChat,.user,.user-layer',function(event){
        event.stopPropagation();
        $('.modalRightPages').hide();
        if(!$('.user-layer').hasClass('active')){
            $('.user-layer').addClass('active');
        }
        $('.user-layer').show();
    });
    
    $('#mr_course_dropdown').on('change',function(){
        var $this = $(this);
        var $select_replace = $($this.data('select-replace'));
        var id = parseInt($this.val());
        var url = $this.data('mr-url')+'?id='+id;

        var jqxhr = $.getJSON(url)
            .done(function(data) {
                $select_replace.empty();

                $.each(data,function(value,text){
                    $select_replace.append('<option value="'+value+'">'+text+'</option>');
                })
            })
            .fail(function() {
                console.log( "error" );
            });
    });


    $(document).on('click','[data-mr-loader="show"]',function(e){
         var $this = $(this);
         var $loader = typeof ($this.data('mr-loader-object')) != "undefined" ? $($this.data('mr-loader-object')) : $('.mr-waper-loader');
         var time_hide = typeof ($this.data('mr-hide-time')) != "undefined" ? parseInt($this.data('mr-hide-time')) : 5000;

        if(typeof ($loader) != "undefined"){
            $loader.show();
            setTimeout(function(){
                $loader.hide();
            }, time_hide);
        }

    })

          function friendRequest(friend_id,my_id,action,thisElement){
            console.log(friend_id);
            console.log(my_id);
            console.log(action);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '../../../friendrequest',
                data: {friend_id:friend_id,my_id:my_id,action:action},
                success: function(response){
                    if(response.status == 'success'){
                        if(action == 'follow'){
                            thisElement.removeClass('followFriend')
                            thisElement.addClass('unfollowFriend')
                            thisElement.text('Unfollow')
                        }else if(action == 'unfollow'){
                            thisElement.removeClass('unfollowFriend')
                            thisElement.addClass('followFriend')
                            thisElement.text('Follow')
                        }else if(action == 'confirm'){
                            thisElement.removeClass('followFriend')
                            thisElement.addClass('unfollowFriend')
                            thisElement.text('Unfollow')
                        }else if(action == 'delete'){
                            $('.deleteFriendBlock'+friend_id).remove();
                        }
                    }
                }
            });
          }
        //friendRequest();
        
        $(document).on('click','.followFriend',function(){
            var thisElement = $(this);
            var friend_id = $(this).data('friend_id');
            var my_id = $(this).data('my_id');
            var action = 'follow';
            friendRequest(friend_id, my_id,action,thisElement);
        });
        
        $(document).on('click','.confirmFriend',function(){
            var thisElement = $(this);
            var friend_id = $(this).data('friend_id');
            var my_id = $(this).data('my_id');
            var action = 'confirm';
            friendRequest(friend_id, my_id,action, thisElement);
        });
        
        $(document).on('click','.unfollowFriend',function(){
            var thisElement = $(this);
            var friend_id = $(this).data('friend_id');
            var my_id = $(this).data('my_id');
            var action = 'unfollow';
            friendRequest(friend_id, my_id, action, thisElement);
        });
        
        $(document).on('click','.deleteFriend',function(){
            var thisElement = $(this);
            var friend_id = $(this).data('friend_id');
            var my_id = $(this).data('my_id');
            var action = 'delete';
            friendRequest(friend_id, my_id, action, thisElement);
        });


    $('#price_min').attr('placeholder', '0');

    $('#price_min').focus(function(){
        if($(this).val() === placeholder){
            $(this).attr('placeholder', '');
        }
    });

    $('#price_min').blur(function(){
        if($(this).val() ===''){
            $(this).attr('placeholder', '0');
        }
    });

    $('#price_max').attr('placeholder', '99');

    $('#price_max').focus(function(){
        if($(this).val() === placeholder){
            $(this).attr('placeholder', '');
        }
    });

    $('#price_max').blur(function(){
        if($(this).val() ===''){
            $(this).attr('placeholder', '99');
        }
    });

});
