function initMap(address,LatLng) {
  var myLatLng = {lat: 40.5212813, lng: -107.7129514};
  var my_location_lat;
  var my_location_lng;
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 2,
    center: myLatLng
  });
    if(address != ''){
        var geocoder = new google.maps.Geocoder();
        //var address = $(this).val();
            geocoder.geocode({'address': address}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                var Local = results[0].geometry.location;
                var latLng = {lat:parseFloat(Local.lat()),lng:parseFloat(Local.lng())};
                    my_location_lat = document.getElementById('location_lat');
                    my_location_lat.value = parseFloat(Local.lat());
                    my_location_lng = document.getElementById('location_lng');
                    my_location_lng.value = parseFloat(Local.lng());
                    
                  var marker = new google.maps.Marker({
                    position: latLng,
                    zoom:10,
                    map: map,
                 //   title: 'Hello World!'
                  });
                  map.setCenter(latLng);
                  map.setZoom(14);
            }else{
                alert('error');
            }
        });
    }else if(LatLng != ''){
        //alert('dvdv');
        var marker = new google.maps.Marker({
            position: LatLng,
            map: map,
            //title: 'Hello World!'
        });
        map.setCenter(LatLng);
        map.setZoom(14);
        my_location_lat = document.getElementById('location_lat');
        my_location_lat.value = LatLng.lat;
        my_location_lng = document.getElementById('location_lng');
        my_location_lng.value = LatLng.lng;
    }
    
}

function initMap2(address,LatLng) {
  var myLatLng = {lat: 40.5212813, lng: -107.7129514};
  var my_location_lat;
  var my_location_lng;
  var map = new google.maps.Map(document.getElementById('map2'), {
    zoom: 2,
    center: myLatLng
  });
    if(address != ''){
        var geocoder = new google.maps.Geocoder();
        //var address = $(this).val();
            geocoder.geocode({'address': address}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                var Local = results[0].geometry.location;
                var latLng = {lat:parseFloat(Local.lat()),lng:parseFloat(Local.lng())};
                    my_location_lat = document.getElementById('location_lat');
                    my_location_lat.value = parseFloat(Local.lat());
                    my_location_lng = document.getElementById('location_lng');
                    my_location_lng.value = parseFloat(Local.lng());
                    
                  var marker = new google.maps.Marker({
                    position: latLng,
                    zoom:10,
                    map: map,
                 //   title: 'Hello World!'
                  });
                  map.setCenter(latLng);
                  map.setZoom(14);
            }else{
                alert('error');
            }
        });
    }else if(LatLng != ''){
        //alert('dvdv');
        var marker = new google.maps.Marker({
            position: LatLng,
            map: map,
            //title: 'Hello World!'
        });
        map.setCenter(LatLng);
        map.setZoom(14);
        my_location_lat = document.getElementById('location_lat');
        my_location_lat.value = LatLng.lat;
        my_location_lng = document.getElementById('location_lng');
        my_location_lng.value = LatLng.lng;
    }
    
}

$(document).ready(function(){
    if($('#map2').length){
        
//        var myLat = $('#location_lat').val();
//        var myLng = $('#location_lng').val();
//        var LatLng = '';
//        if((myLat != '') && (myLng != '')){
//            LatLng = {lat:parseFloat(myLat),lng:parseFloat(myLng)};
//        }
//
//        initMap2('',LatLng);
    }
    $(document).on('click','.mapTabButton',function(event){
        $('.modalUserAboutContainer').html('');
        $('.modalUserAboutContainer').append('<div id="map2" class="map2"></div>');
        var myLat = $('#location_lat').val();
        var myLng = $('#location_lng').val();
        var LatLng = '';
        if((myLat != '') && (myLng != '')){
            LatLng = {lat:parseFloat(myLat),lng:parseFloat(myLng)};
        }

        initMap2('',LatLng);
    })
    
    
    if($('.ratingCountUser').length){
        var user_id = $('#userInfoId').text();
        $.ajax({
            type: 'POST',
            dataType: "json",
            url: '../../../users/profile/getuserratinglevel',
            data: {user_id:user_id},
            beforeSend:function(){
                $('.ratingCountUser').html('<i class="fa fa-spinner fa-spin" aria-hidden="true"></i>')
            },
            success: function(response){
                $('.ratingCountUser').text(response['k']+'/'+response['c']);
            }
        });
    }
    
    $(document).on('click','.saveMyLocation', function(){
        var myLocationLat = $('#location_lat').val();
        var myLocationLng = $('#location_lng').val();
        
        if((myLocationLat != '') && (myLocationLng != '')){
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '../../../mylocationsave',
                data: {location_lat:myLocationLat,location_lng:myLocationLng},
                success: function(response){
                    if(response.status == 'success'){
                        swal('Position saved','','success');
                         location.reload();
                    }else{
                        swal('Position not saved','pleace try again','error');
                    }
                }
            });
            
        }else{
            console.log('error');
        }
    });
    
    function showPosition(position) {
        console.log(position.coords.latitude)
        console.log(position.coords.longitude)
        initMap2('',{lat:position.coords.latitude,lng:position.coords.longitude});
    }
    
    function getLocation() {
        if (navigator.geolocation) {
            return navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            return "Geolocation is not supported by this browser.";
        }
    }
    
    $(document).on('click','.datermine_my_location',function(e){
        e.preventDefault();
        getLocation();
        //initMap($(this).val(),'');
    });
    
    $(document).on('change','.enter_your_location',function(){
        initMap2($(this).val(),'');
    });
    
    $("#myLocation").on('shown.bs.modal', function(){
        var myLat = $('#location_lat').val();
        var myLng = $('#location_lng').val();
        var LatLng = '';
        if((myLat != '') && (myLng != '')){
            LatLng = {lat:parseFloat(myLat),lng:parseFloat(myLng)};
        }
        
        initMap('',LatLng);
        
    });
    
        
        function deleteFile(file_name){
            $.ajax({
                type: 'POST',
                url: '../../../deletefile',
                data: {file_name:file_name},
                success: function(response){
                }
            });
        }
        if($('.user_header_photo').length){
            var myDropzone = new Dropzone($('.user_header_photo')[0], { // Make the whole body a dropzone
                uploadMultiple:false,
                maxFiles:1,
            });

            myDropzone.on("maxfilesexceeded", function(file) {
                    myDropzone.removeAllFiles();
                    myDropzone.addFile(file);
            });

            myDropzone.on("complete", function(response) {
                if (response.status == 'success') {
                    $('.user_header_photo').find('.dz-preview').hide();
                    $('.user_header_photo').css('background-image',"url('/images/users_images/"+response.xhr.response+"')");
                    $('#userAvatarWrapper').val(response.xhr.response);
                }
            });

            myDropzone.on("removedfile", function(response) {
                if(response.xhr != null){
                   deleteFile(response.xhr.response);
                }
            });

//            function removeHeaderPhoto(fileNotRemove){
//
//                $.each(myDropzone.files, function( key, value ) {
//                    if(value.name != fileNotRemove){
//                        myDropzone.removeFile(myDropzone.files[key]);
//                    }
//                });
//                //console.log(myDropzone);
//            };
        }
        
        if($('.user_header_avatar').length){
            var previewNode = document.querySelector(".previewTemplateQ");
            previewNode.id = "";
            var previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);

            var myDropzoneA = new Dropzone($('.user_header_avatar')[0], {
                maxFiles:1,
                uploadMultiple:false,
                url: "../../users/profile/savedropedfile",
                previewTemplate:previewTemplate,
                clickable: ".newAvatar",
                previewsContainer: ".user_header_avatar",
            });

            myDropzoneA.on("maxfilesexceeded", function(file) {
                    myDropzoneA.removeAllFiles();
                    myDropzoneA.addFile(file);

            });

            myDropzoneA.on("complete", function(response) {
                if (response.status == 'success') {
                    $('#userAvatar').val(response.xhr.response);
                    $('.eventButton').show();
                }
            });

            myDropzoneA.on("removedfile", function(response) {
                if(response.xhr != null){
                   deleteFile(response.xhr.response);
                }
            });
          }

});
