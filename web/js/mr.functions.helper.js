/**
 * Created by mackrais on 16.12.15.
 * @author Oleh Boiko
 * @email minace002@gmail.com
 */

$(document).ready(function () {

    /**
     * Function clone block when entered characters
     * @param data-mr-characters-limit - limit characters when clone block
     * @param data-mr-clone-block -  parent block by cloning can be selector, class, tag ... Any appeal to member in jQuery
     * @data-mr-event - ID events for library functions
     */
    $(document).on('keyup','[data-mr-event="clone_when_entered"]',function(e){
        var count_characters_limit = typeof ($(this).data('mr-characters-limit')) == "undefined"  ? 10 : $(this).data('mr-characters-limit');
        var $clone_block = typeof ($(this).data('mr-clone-block')) == "undefined"  ? $(this) : $(this).parents($(this).data('mr-clone-block')).eq(0);
        $clone_block.val('');

        if($(this).val().length >= count_characters_limit){
            var  $tmp_block = $clone_block.eq(0).clone();
                 $tmp_block.find('input').val( $tmp_block.find('input').data('default-clone') );
             if($clone_block.next($(this).data('mr-clone-block')).length == 0){
                  $($tmp_block).insertAfter($clone_block);
             }
        }else{
            if($(this).val().length == 0){
                if($($(this).data('mr-clone-block')).length > 1)
                $clone_block.remove();
            }
        }
    })

    /**
     * Function clone block when click on object
     * @param data-mr-characters-limit - limit characters when clone block
     * @param data-mr-clone-block -  parent block by cloning can be selector, class, tag ... Any appeal to member in jQuery
     * @data-mr-event - ID events for library functions
     */
    $(document).on('click','[data-mr-event="clone_when_entered"]',function(e){
        var count_characters_limit = typeof ($(this).data('mr-characters-limit')) == "undefined"  ? 10 : $(this).data('mr-characters-limit');
        var $clone_block = typeof ($(this).data('mr-clone-block')) == "undefined"  ? $(this) : $(this).parents($(this).data('mr-clone-block')).eq(0);
        $clone_block.val('');

        if($(this).val().length >= count_characters_limit){
            var  $tmp_block = $clone_block.eq(0).clone();
            $tmp_block.find('input').val( $tmp_block.find('input').data('default-clone') );
            if($clone_block.next($(this).data('mr-clone-block')).length == 0){
                $($tmp_block).insertAfter($clone_block);
            }
        }
    })



    /**
     * Function clone block when click on object
     * @param data-mr-characters-limit - limit characters when clone block
     * @param data-mr-clone-block -  parent block by cloning can be selector, class, tag ... Any appeal to member in jQuery
     * @data-mr-event - ID events for library functions
     */
    $(document).on('click','[data-mr-event="clone_block"]',function(e){
        var $clone_block = typeof ($(this).data('mr-clone-block')) == "undefined"  ? $(this) : $(this).parents($(this).data('mr-clone-block')).eq(0);
        $clone_block.val('');

           var  $tmp_block = $clone_block.eq(0).clone(false,false);

      //  var  $tmp_block = $($clone_block.eq(0)[ 0 ]);


            $tmp_block.find('input').val( $(this).data('default-clone') );

        var cloneIndex = $($(this).data('mr-clone-block')).length;
          $tmp_block.find('input').attr('id','contact-phone'+cloneIndex);

            $tmp_block.find('.help-block').empty();
            $tmp_block.find('[data-mr-event="clone_block"]')
                .removeClass()
                .addClass('mr-btn-remove')
                .text(' ');

        console.log($clone_block);
        console.log($tmp_block);

             $tmp_block.find('[data-mr-event="clone_block"]').attr('data-mr-event','delete_block');
          $($tmp_block).insertBefore($clone_block);

    })


    /**
     * Function clone block when click on object
     * @param data-mr-characters-limit - limit characters when clone block
     * @param data-mr-clone-block -  parent block by cloning can be selector, class, tag ... Any appeal to member in jQuery
     * @data-mr-event - ID events for library functions
     */
    $(document).on('click','[data-mr-event="delete_block"]',function(e){
        var $clone_block = typeof ($(this).data('mr-clone-block')) == "undefined"  ? $(this) : $(this).parents($(this).data('mr-clone-block')).eq(0);
        $clone_block.eq(0).remove();
    })




    /**
     *
     * @type {number}
     */



    $(document).on('click','[data-mr-event="dynamic_load_top"]', function(e){
        e.preventDefault();
        var page = typeof ($(this).data('mr-param-page')) == "undefined" ? 1 : $(this).data('mr-param-page');
        var $this = $(this);
        var $block = typeof ($(this).data('mr-insert-block')) == "undefined"  ? $(this).next() : $($(this).data('mr-insert-block')).eq(0);
        var data_params = $(this).data();
        var data_post = {};
        var url = typeof ($(this).data('mr-url')) == "undefined" ? location.href : $(this).data('mr-url');
        var method = typeof ($(this).data('mr-method')) == "undefined" ? "POST" : $(this).data('mr-method');

        $(this).attr('data-mr-param-page',page);
        $(this).data('mr-param-page',page);

        if(typeof (data_params !== "undefined" )){
            for(var name in data_params){
                if(name.indexOf('mrParam') !== -1){
                    var tmp_name = name.replace('mrParam','').toLowerCase();
                    data_post[tmp_name] = data_params[name];
                }
            }
        }

        data_post['page']= page;
        bootbox.setLocale("uk");


        $.ajax({
            url:url,
            data:data_post,
            method:method,
            dataType: 'JSON',
            success: function(data) {
               if(data){
                   if(data.status == "ok" ){
                       if(data.hidden !== "true"){
                           page += 1;
                           $this.attr('data-mr-param-page',page);
                           $this.data('mr-param-page',page);
                       }else{
                           $this.remove();
                       }
                       $block.prepend(data.content);
                   }else
                       if(data.status == "error"){
                           bootbox.alert(data.msg);
                       }
               }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                bootbox.alert(jqXHR.status+' : '+ errorThrown);
            }
        });


    })


    $(document).on('click','[data-mr-event="dynamic_load_model"]', function(e){
        e.preventDefault();
        e.stopPropagation();
        var $this = $(this);
        var data_params = $(this).data();
        var data_post = {};
        var url = typeof ($(this).data('mr-url')) == "undefined" ? location.href : $(this).data('mr-url');
        var method = typeof ($(this).data('mr-method')) == "undefined" ? "POST" : $(this).data('mr-method');
        var title = typeof ($(this).data('mr-title')) == "undefined" ? "" : $(this).data('mr-title');

        var $form = typeof ($(this).data('mr-form')) == "undefined" ? false : $($(this).data('mr-form'));

        if($form){
            data_post = $form.serialize();
        }else
        if(typeof (data_params !== "undefined" )){
            for(var name in data_params){
                if(name.indexOf('mrParam') !== -1){
                    var tmp_name = name.replace('mrParam','').toLowerCase();
                    data_post[tmp_name] = data_params[name];
                }
            }
        }

        bootbox.setLocale("uk");

        $.ajax({
            url:url,
            data:data_post,
            method:method,
            dataType: 'JSON',
            success: function(data) {
                if(data){
                    if(data.status == "ok" ){

                        bootbox.dialog({
                            message: data.content,
                            title: data.title,
                        });

                    }else
                    if(data.status == "error"){
                        bootbox.alert(data.msg);
                    }
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                bootbox.alert(jqXHR.status+' : '+ errorThrown);
            }
        });

    })

    /**
     * Function send ajax by click
     * @param data_params - all post params by send ajax
     * @param mr-callb-success -  body success callback function have 2 arguments
     *        - _this - current object
     *        - response - response ajax
     * @param mr-callb-error -  body error callback function have 1 arguments
     *        - msg - message with error by alert window
     * @param mr-url - url by ajax send
     * @data-mr-event - ID events for library functions
     */

    $(document).on('click','[data-mr-event="ajax_send_click"]', function(e){

        e.preventDefault();
        e.stopPropagation();
        var $this = $(this);
        var data_params = $(this).data();
        var data_post = {};
        var url = typeof ($(this).data('mr-url')) == "undefined" ? location.href : $(this).data('mr-url');

        var callbsuccess = typeof ($(this).data('mr-callb-success')) == "undefined" ? '' : new Function("_this","response",$(this).data('mr-callb-success'));
        var callberror = typeof ($(this).data('mr-callb-error')) == "undefined" ? '' : new Function("msg",$(this).data('mr-callb-error'));


        if(typeof (data_params !== "undefined" )){
            for(var name in data_params){
                if(name.indexOf('mrParam') !== -1){
                    var tmp_name = name.replace('mrParam','').toLowerCase();
                    data_post[tmp_name] = data_params[name];
                }
            }
        }

      //  console.log($(this).data('mr-callb-success'));
        bootbox.setLocale("uk");

        $.ajax({
            url:url,
            data:data_post,
            method:"POST",
            dataType: 'JSON',
            success: function(data) {
                if(data){
                    if(data.status == "ok" ){
                        if(callbsuccess !== ''){
                            callbsuccess($this,data);
                        }
                    }else
                    if(data.status == "error"){
                        if(callberror !== ''){
                            callberror(data.msg);
                        }
                    }
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                bootbox.alert(jqXHR.status+' : '+ errorThrown);
            }
        });

    })

    $(document).on('change','.mr-filter-submit-change select',function(e){
        $(this).parents('form:eq(0)').submit();
    });


    /**
     *  Ajax submit + validation by events
     */

    $('[data-mr-ajax-form="true"]').off().submit(function(e){
        MrAjaxSubmitFrm(e,this);
    });


    /**
     *  Ajax submit + validation by events
     */
    function MrAjaxSubmitFrm(event,_this){

        event.preventDefault();

        var $this = $(_this);
        var events_str = $this.data('mr-events');
        var postData = new FormData(_this);
        var ajax_submit = 1;
        $this.find('.help-block').empty();
        $this.find('.form-group').removeClass('has-error');
        bootbox.setLocale("uk");

        if(event.type != 'submit'){
            ajax_submit = 0;

        }else{
            if(typeof(events_str) != "undefined" ){
                var events = events_str.split(',');
                $.each(events, function(i,item){

                    switch (item) {
                        case "keyup":
                        case "keydown":
                        case "focus":
                        case "blur":
                            $this.find('input,textarea').off(item).on(item,function(e){
                                ajax_submit = 0;
                                postData.append("ajax_submit", ajax_submit);
                                MrAjaxSubmitFrm(e,$(this).parents('form:eq(0)')[0]);
                            });
                            break;
                        case "change":
                            $this.find('select,input:checkbox,input:radio,input:file').off(item).on(item,function(e){
                                ajax_submit = 0;
                                postData.append("ajax_submit", ajax_submit);
                                MrAjaxSubmitFrm(e,$(this).parents('form:eq(0)')[0]);
                            });
                            break;
                    }

                })
            }
        }

        postData.append("ajax_submit", ajax_submit);

        $.ajax({
            url : $this.attr('action'),
            type: "POST",
            data : postData,
            processData: false,
            contentType: false,
            success:function(data, textStatus, jqXHR){
                if(data.status == 'valid_error'){
                    $.each(data.messages,function(key,msg){
                        if(typeof($this.find('.field-'+key)) != "undefined" ){
                            $this.find('.field-'+key).addClass('has-error');
                            $this.find('.field-'+key+' .help-block').append(msg);
                        }
                    });
                }else{
                    location.reload();
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                bootbox.alert(jqXHR.status+' : '+ errorThrown);
            }
        });
    }





})