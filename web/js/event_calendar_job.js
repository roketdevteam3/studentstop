/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    
        function dateChange(date){
            var date = new Date(date);
            var year = date.getFullYear()
            var month = date.getMonth()+1;
            if(month < 10){
                month = '0'+month;
            }
            var day = date.getDate()
            var dayClick = year+'-'+month+'-'+day;

            // alert('role:'+getRole());
            if (window.role == 1){
              $.ajax({
                type: 'POST',
                url: '../../../jobs/default/events_job_student',
                data: {date:dayClick},
                dataType:'json',
                success: function(response){
                  var month = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
                  var university_slug = $('.university_slug').val();
                  $('.eventBlock').html('')
                  if(response.length == 0){
                    $('.eventBlock').append('<span class="nf-event">sorry but events not found</span>')
                  }
                  //var date_start =
                  for(var key in response){
                    if(response[key].type = 'news'){
                      var date_start = new Date(response[key].date_create);
                    }else{
                      var date_start = new Date(response[key].date_start);
                    }
                    var date_create = new Date(response[key].date_create);

                    $('.eventBlock').append(
                      '<div class="event-wrap">'+
                      '<div class="e-img">'+
                      '<div class="e-calendar">'+
                      '<i class="fa fa-calendar"> </i> <span>'+month[date_start.getMonth()]+' '+date_start.getDate()+'</span>'+
                      '</div>'+
                      '</div>'+
                      '<div class="e-info">'+
                      '<h1 class="e-head" style="margin: 0;">'+response[key].title+'</h1>'+
                      '<div class="content">'+response[key].content+'</div>'+
                      '</div>'+
                      '</div>'
                    );
                  }
                }
              })
            } else {
              $.ajax({
                type: 'POST',
                url: '../../../jobs/default/events_job',
                data: {date:dayClick},
                dataType:'json',
                success: function(response){
                  var month = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
                  var university_slug = $('.university_slug').val();
                  $('.eventBlock').html('')
                  if(response.length == 0){
                    $('.eventBlock').append('<span class="nf-event">sorry but events not found</span>')
                  }
                  //var date_start =
                  for(var key in response){
                    if(response[key].type = 'news'){
                      var date_start = new Date(response[key].date_create);
                    }else{
                      var date_start = new Date(response[key].date_start);
                    }
                    var date_create = new Date(response[key].date_create);

                    $('.eventBlock').append(
                      '<div class="event-wrap">'+
                      '<div class="e-img">'+
                      '<div class="e-calendar">'+
                      '<i class="fa fa-calendar"> </i> <span>'+month[date_start.getMonth()]+' '+date_start.getDate()+'</span>'+
                      '</div>'+
                      '</div>'+
                      '<div class="e-info">'+
                      '<h1 class="e-head" style="margin: 0;">'+response[key].title+'</h1>'+
                      '<div class="content">'+response[key].content+'</div>'+
                      '</div>'+
                      '</div>'
                    );
                  }
                }
              })
            }

        }
   	
        $('.calendarToday').click(function(){
            var dateT = new Date;
            dateChange(dateT);
            $('#paginator').datepaginator({
                onSelectedDateChanged: function(event, date){
                    dateChange(date);
                },
                itemWidth:50
            });
        });
        
        var options = {
            onSelectedDateChanged: function(event, date) {
                dateChange(date);
            },
            itemWidth:50
        }

        $('#paginator').datepaginator(options);

  var date = new Date();

  var now = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);

  dateChange(now);



});