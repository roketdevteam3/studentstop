$(document).ready(function(){
    
    function changeFollowStatus(class_id,status){
        $.ajax({
            type: 'POST',
            url: '../../../changefollowclassstatus',
            data: {class_id:class_id, status:status},
            dataType:'json',
            success: function(response){
                if(response.status == 'success'){
                    $('#modalRating').modal('show');
                }
            }
        });
    }
    
    if($('.showClassEnded').length){
        swal({
            title: "Your class is over!",
            text: "<p style='color:white;'>If you want to keep this class for another semester, please click 'stay in class'</p>",
            type: "warning",
            html: true,
            showCancelButton: true,
            confirmButtonText: "Keep this class",
            cancelButtonText: "Graduated",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function(isConfirm) {
            var class_id = $('[name=this_class_id]').val();
            if(isConfirm) {
                changeFollowStatus(class_id, 'confirm')
            }else{
                changeFollowStatus(class_id, 'cancel');
            }
        });
    }
    
});