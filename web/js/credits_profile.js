$(document).ready(function(){
    function money_count(money_insert){
       var money_count = parseFloat(money_insert) * parseFloat($('.CreditsForMoney').text());
       $('.money_for_credits').text(money_count);
        
    }
   $(document).on('change','input[name=credit_pay_count]',function(){
        money_count($(this).val());
   });
   $(document).on('keyup','input[name=credit_pay_count]',function(){
        money_count($(this).val());
   });
    
   $(document).on('click','.pay_credits',function(){
       var money_insert = $('input[name=credit_pay_count]').val();
       if(money_insert > 0){
        $.ajax({
            url:'../../../../paycredits',
            method:'POST',
            data:{money_insert:money_insert},
            dataType:'json',
        }).done(function(response){
            if((response.status1 == 'save') && (response.status2 == 'save')){
                swal({
                    title: "purchased",
                    type: "success",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok!",
                }, function(isConfirm){
                     location.reload();
                });
            }
        });
       }else{
           swal('Pleace, insert money count!');
       }
   });
   
});        