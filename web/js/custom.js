$(document).ready(function(){

	$(".parentCategory ").on('click', function(){

		var thisItem = $(this),
			parentIcon = thisItem.children(".accordion-icon"),
			icon = thisItem.find(parentIcon);

		if(thisItem.next().is(':hidden')){
			icon.removeClass('fa-plus');
			icon.addClass('fa-minus');
		}else{
			icon.removeClass('fa-minus');
			icon.addClass('fa-plus');
		}
	})


	$(window).load(function(){
		var oldSSB = $.fn.modal.Constructor.prototype.setScrollbar;
		$.fn.modal.Constructor.prototype.setScrollbar = function () {
			oldSSB.apply(this);
			if(this.bodyIsOverflowing && this.scrollbarWidth) {
				$('.content, .nav-contents').css('margin-right', this.scrollbarWidth);
				$('#right-panel').css('right', this.scrollbarWidth);
			}
		}
		var oldRSB = $.fn.modal.Constructor.prototype.resetScrollbar;
		$.fn.modal.Constructor.prototype.resetScrollbar = function () {
			oldRSB.apply(this);
			$('.content, .nav-contents').css('margin-right', '0');
			$('#right-panel').css('right', '0');
		}
	});


	$('.modal').on('shown.bs.modal', function () {
		alignCenter($(this).find($(".modal-dialog")));
	})


	$(window).resize(function() {
		alignCenter($(this).find($(".modal-dialog")));
	})
	
	function alignCenter(elem) {
		elem.css({
			left: ($(window).width() - elem.width()) / 2 + 'px',
			top: ($(window).height() - elem.height()) / 2 + 'px'
		})
	}


//	document.getElementById("price_min").placeholder = "0";
//	document.getElementById("price_max").placeholder = "99";
})