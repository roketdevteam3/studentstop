/**
 * Created by minace on 3/3/16.
 */
$(document).ready(function(e){

    $('[data-mr-event="add_friend"]').on('click',function(e){
        e.preventDefault();
        add_friend($(this));

    });

    $('[data-mr-event="remove_friend"]').on('click',function(e){
        e.preventDefault();
        remove_friend($(this));
    });

    $('[data-mr-event="accept_friend"]').on('click',function(e){
        e.preventDefault();
        accept_friend($(this));
    });



    function add_friend($this){
        var url = typeof ($this.data('mr-url')) == "undefined" ? location.href : $this.data('mr-url');
      //  bootbox.setLocale("uk");

        $.ajax({
            url:url,
            method:'POST',
            dataType: 'JSON',
            success: function(data) {
                if(data){
                    if(data.status == "ok" ){
                        $this.off().click(function() {
                            remove_friend($(this));
                        });
                        $this.removeClass().addClass('btn btn-success');
                        $this.find('i').removeClass().addClass('fa fa-ellipsis-h');
                        $this.attr('title',data.title);
                        $.mr_notification({class:'success',content: data.msg,close_type: 'remove'});
                    }else
                    if(data.status == "error"){
                        bootbox.alert(data.msg);
                    }
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                bootbox.alert(jqXHR.status+' : '+ errorThrown);
            }
        });
    }


    function remove_friend($this){
        var url = typeof ($this.data('mr-url')) == "undefined" ? location.href : $this.data('mr-url');
        var msg = typeof ($this.data('mr-msg')) == "undefined" ? 'Do you want delete this item?' : $this.data('mr-msg');
      //  bootbox.setLocale("uk");

        bootbox.confirm(msg, function(result) {
            if (result) {

                $.ajax({
                    url:url,
                    method:'POST',
                    dataType: 'JSON',
                    success: function(data) {
                        if(data){
                            if(data.status == "ok" ){
                                $this.off().click(function() {
                                    add_friend($(this));
                                });
                                $this.attr('title',data.title);
                                $this.removeClass().addClass('btn btn-primary');
                                $this.find('i').removeClass().addClass('fa fa-user-plus');
                                $.mr_notification({class:'warning',content: data.msg,close_type: 'remove'});
                            }else
                            if(data.status == "error"){
                                bootbox.alert(data.msg);
                            }
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        bootbox.alert(jqXHR.status+' : '+ errorThrown);
                    }
                });
            }
        });
    }


    function accept_friend($this){
        var url = typeof ($this.data('mr-url')) == "undefined" ? location.href : $this.data('mr-url');

                $.ajax({
                    url:url,
                    method:'POST',
                    dataType: 'JSON',
                    success: function(data) {
                        if(data){
                            if(data.status == "ok" ){
                                var $count = $('[data-target="#friends"]').find('.badge');
                                 $count.text(parseInt($count.text())+1);
                                 $this.parents('.mr-university-user-block:eq(0)').remove();

                                var $request = $('[data-target="#request"]').find('.badge');
                                if(parseInt($request.text()) > 0)
                                   $request.text(parseInt($request.text())-1);

                                $this.parents('.mr-university-user-block:eq(0)').remove();
                                 $.mr_notification({class:'success',content: data.msg,close_type: 'remove'});
                                location.reload();
                            }else
                            if(data.status == "error"){
                                bootbox.alert(data.msg);
                            }
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        bootbox.alert(jqXHR.status+' : '+ errorThrown);
                    }
                });
    }

})