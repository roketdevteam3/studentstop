/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 03.03.16
 * Time: 14:08
 * Email: mackraiscms@gmail.com
 * Site: http://mackrais.tk
 */

$(document).ready(function(e){

    /**
     * set animation effects by 1s
     */
    setTimeout(function(){
        $(".mr-notification").animate({ opacity: '1'}, "slow");
    }, 1000);

});

/**
 * Events close button
 */
$(document).on('click','.mr-notification .mr-close',function(e){
    e.preventDefault();
    var $this = $(this);
    var $notification = $this.parents('.mr-notification:eq(0)')

    // hide
    if(typeof ($(this).data('mr-hide')) != "undefined"){
        $notification.animate({ opacity: '0'}, "slow");
    }

    // remove
    if(typeof ($(this).data('mr-remove')) != "undefined"){
        $notification.animate({ opacity: '0'}, "slow");
       // setTimeout(function(){
            $notification.remove();
      //  }, 1000);
    }
});

(function($) {

    $.mr_notification = function(params) {
        var $body = $('body');
        var $notification = $('<div class="mr-notification"></div>');
        var $h = $('<h4></h4>');
        var $close = $('<span class="mr-close" data-mr-remove="true" >&times;</span>');

        $notification.addClass(params.class);
        $h.append(params.content);

        if ( params.close_type == 'hide' ){
            $close.attr('data-mr-hide','true');
        }
        if ( params.close_type == 'remove' ){
            $close.attr('data-mr-remove','true');
        }

        $notification.append($h);
        $notification.append($close);

        if($('.mr-notification').length > 0){
            var last_id = $('.mr-notification').length -1 ;
            $notification.insertAfter('.mr-notification').eq(last_id);
        }else {
            $body.prepend($notification);

        }
        $notification.animate({ opacity: '1'}, "slow");


    }

})(jQuery);
