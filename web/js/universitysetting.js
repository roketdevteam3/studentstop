$(document).ready(function(){
   
            var previewNode = document.querySelector(".previewTemplateQ");
            previewNode.id = "";
            var previewTemplate = previewNode.parentNode.innerHTML;
            previewNode.parentNode.removeChild(previewNode);

            var myDropzoneA = new Dropzone($('.newPhotoU')[0], {
                maxFiles:1,
                uploadMultiple:false,
                url: "../../../university/saveduniversityphoto",
                previewTemplate:previewTemplate,
                clickable: ".newPhoto",
                previewsContainer: ".newPhotoU",
            });

            myDropzoneA.on("maxfilesexceeded", function(file) {
                    myDropzoneA.removeAllFiles();
                    myDropzoneA.addFile(file);

            });

            myDropzoneA.on("complete", function(response) {
                if (response.status == 'success') {
                    $('#university-image').val(response.xhr.response);
                    //$('.eventButton').show();
                }
            });

            myDropzoneA.on("removedfile", function(response) {
                if(response.xhr != null){
                   //deleteFile(response.xhr.response);
                }
            });
          

});
