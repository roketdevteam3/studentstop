<?php

use yii\web\Request;

$params = require(__DIR__ . '/params.php');

$baseUrl = str_replace('/web', '', (new Request())->getBaseUrl());

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'debug' => [
            'class' => 'yii\debug\Module',
            'allowedIPs' => [
                '127.0.0.1', '::1',
            ],
            'on beforeAction' => function(){
                \Yii::$app->response->format = 'html';
            },
        ],

        'gii' => 'yii\gii\Module',
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ],
        'rbac' =>  [
            'class' => 'app\modules\administration\RbacModule',
            'userModelClassName'=>null,
            'userModelIdField'=>'id',
            'userModelLoginField'=>'email',
            'userModelLoginFieldLabel'=>null,
            'userModelExtraDataColumls'=>null,
            'beforeCreateController'=>null,
            'beforeAction'=>null
        ]

    ],
    'components' => [

        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'localhost',
            'port' => 6379,
            'database' => 0,
        ],
    
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest'],
        ],
        'view' => [
            'theme' => [
                'basePath' => '@app/themes/basic',
                'baseUrl' => '@web/themes/basic',
                'pathMap' => [
                    '@app/views' => '@app/themes/basic',
                ],
            ],
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules'           => [
                '/'                                             => 'site/index',
                'getstates'                                             => 'site/getstates',
                '<controller:\w+>/<action:\w+>/*'               => '<controller>/<action>',
                'page/<slug>' => 'page/view',
            
            ]
        ],
        'mailer' => [
//            'class' => 'yii\swiftmailer\Mailer',
//            'useFileTransport' => true,
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host'     => 'smtp.gmail.com',
                'username'   => 'studentstopinfo@gmail.com',
                'password'   => 'Administrator85',
                'port'     => '587',
                'encryption' => 'tls',
            ],
            'messageConfig' => [
                'charset' => 'UTF-8',
            ]
        ],
        'assetManager' => [
            'class' => 'yii\web\AssetManager',
            'linkAssets' => false,
            'bundles' => [
                'dosamigos\google\maps\MapAsset' => [
                    'options' => [
                        'key' => 'AIzaSyB2unGYoNYON41RyDOOec5uPkZ-HcWzspI',
                        'language' => 'id',
                        'version' => '3.1.18'
                    ]
                ]
            ]
        ],

        'request' => [
            'baseUrl' => $baseUrl,
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'LT8A0ySJTJqhIMPUK1KnhTqvIqUGaRfM',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => '\app\models\user\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'i18n'         => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'db' => 'db',
                    'sourceLanguage' => 'en', // Developer language
                    'sourceMessageTable' => '{{%language_source}}',
                    'messageTable' => '{{%language_translate}}',
                    'cachingDuration' => 86400,
                    'enableCaching' => false,
                ],
                'app*' => [
                    'class' => 'yii\i18n\DbMessageSource',
                    'db' => 'db',
                    'sourceLanguage' => 'en', // Developer language
                    'sourceMessageTable' => '{{%language_source}}',
                    'messageTable' => '{{%language_translate}}',
                    'cachingDuration' => 86400,
                    'enableCaching' => false,
                ],
            ],
        ],
        'paypal'        => [
            'class'        => 'app\components\Paypal',
            'clientId'     => 'AcbjQn59gt0G9vfzk7vGa78SMYZ37GyqGU9Y8PcZ6dBYr2s9KwHpIqvuVkouE0gNWkXKDwtpCaEexZvx',
            'clientSecret' => 'EOffmSQFNXYkSKYmXvDZDWrAbKFFWvbsToaLDKFgQy-KvVHTzy9-5OQ7I0CCbFAw_-4AtTrBhOG4Aanx',
            'returnUrl'    => '/rest/order/confirm',
            'cancelUrl'    => 'cancel-url',
            'mode'         => 'live'
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                // 'google' => [
                //     'class' => 'yii\authclient\clients\GoogleOAuth',
                //     'clientId' => '36083377680-jvrqebc0ob6gimg9n7o8bds63cj13u3p.apps.googleusercontent.com',
                //     'clientSecret' => 'sP2MZmGPkVLyQnZVzj_l7Bpp',
                // ],
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '308450342908558',
                    'clientSecret' => 'e7697dfa6d07107c38abd51ebdce112b',
                ],
                /*'twitter' => [
                    'class' => 'yii\authclient\clients\Twitter',
                    'consumerKey' => '6U0fYZxmEg2BGpxvPCxtEwBfC',
                    'consumerSecret' => 'XAIE9uj5wTivPa4CtzpHbASWxS5V4bYHoh8D1TxUZrOQy6Yp3C',
                ],*/
                'linkedin' => [
                    'class' => 'yii\authclient\clients\LinkedIn',
                    'clientId' => '86a1mofoqix8m3',
                    'clientSecret' => 'EjeuJTE1zuIhFxWF',
                ],
            ],
        ],

        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

/*
 Dynamic loading MackRais CMS modules
   $modules_dir - default module directory
*/

$modules_dir = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR;
$handle = opendir($modules_dir);
while (false !== ($file = readdir($handle))) {
    if ($file != "." && $file != ".." && is_dir($modules_dir . $file)) {
        if(file_exists($modules_dir . $file . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'main.php'))
            $config = array_merge_recursive($config, require($modules_dir . $file . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'main.php'));
    }
}
closedir($handle);

return $config;
