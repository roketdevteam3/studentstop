<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\models\Major;
use app\models\Course;
use app\models\Classes;
use app\models\Follower;
use app\models\Tutorsinfo;
use app\models\user\User;
use app\modules\university\models\University;

class TutorsstatusWidget extends Widget{

    public function init(){
        parent::init();
    }

    public function run(){
        $modelTutorsinfo = Tutorsinfo::find()->where(['user_id' => \Yii::$app->user->id])->one();
        
        if($modelTutorsinfo == null){
            return '<li role="presentation" ><a href="'.Url::home().'tutorspage/'.\Yii::$app->user->id.'">Become a tutor</a></li>';
        }else{
            return '<li role="presentation" ><a href="'.Url::home().'tutorspage/'.\Yii::$app->user->id.'">My profile</a></li>';
        }
    }
}
?>
