<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Major;
use app\models\Course;
use app\models\Classes;
use app\models\Follower;
use app\models\user\User;
use app\modules\university\models\University;

class JoinclassWidget extends Widget{
	public $class_id;

	public function init(){
		parent::init();
	}

	public function run(){
		$modelFollow = Follower::find()->where(['id_user' => \Yii::$app->user->id, 'id_object' => $this->class_id, 'type_object' => 'class'])->one();
		
		if($modelFollow == null){
			return '<a class="btn JoinClass" data-class_id="'.$this->class_id.'">Join</a>';
		}else{
			return '<a class="btn UnfollowClass" data-class_id="'.$this->class_id.'">Unfollow</a>';
		}
	}
}
?>
