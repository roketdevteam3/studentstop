<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\jobs\models\Company;
use app\modules\jobs\models\Userjobs;
use app\modules\university\models\University;

class UserjobsWidget extends Widget{
	public $jobs_id;
	public $page_type;

	public function init(){
		parent::init();
	}

	public function run(){
		$modelUserJobs = Userjobs::find()->where(['user_id' => \Yii::$app->user->id, 'jobs_id' => $this->jobs_id])->one();
		if($this->page_type == 'search'){
			if($modelUserJobs != null){
				$result = '<a class="btn btn-white mailToUser startChat" data-sender-id="'.\Yii::$app->user->id.'" data-recipient-id="'.$modelUserJobs['user_id'].'" style="display:none;font-size:12px;"><i class="fa fa-envelope"></i> mail</a>'
                                        . '<a class="unapplyJob" data-page_type="search" data-job_id="'.$this->jobs_id.'">Unapply</a>';
			}else{
				$result = '<a class="btn btn-white mailToUser startChat" data-sender-id="'.\Yii::$app->user->id.'" data-recipient-id="'.$modelUserJobs['user_id'].'" style="display:none;font-size:12px;"><i class="fa fa-envelope"></i> mail</a>'
				        . '<a class="applyJob" data-page_type="search" data-job_id="'.$this->jobs_id.'">Apply</a>';
			}
		}else{
			if($modelUserJobs != null){
				$status = '';
				switch ($modelUserJobs->status){
          case 0:
            $status = 'Unapply';
            break;
					case 1:
						$status = 'Accept';
						return '<h5>Status: Accept</h5>';
						break;
          case 2:
            $status = 'Maybe';
            break;
          case 3:
            $status = 'Not suitable';
            break;
				}
				$result = '<a class="btn btn-white mailToUser startChat" data-sender-id="'.\Yii::$app->user->id.'" data-recipient-id="'.$modelUserJobs['user_id'].'" style="display:none;font-size:12px;"><i class="fa fa-envelope"></i> mail</a>'
				        . '<a class="unapplyJob" data-page_type="dashboard" data-job_id="'.$this->jobs_id.'">'.$status.'</a>';
			}else{
				$result = '<a class="btn btn-white mailToUser startChat" data-sender-id="'.\Yii::$app->user->id.'" data-recipient-id="'.$modelUserJobs['user_id'].'" style="display:none;font-size:12px;"><i class="fa fa-envelope"></i> mail</a>'
				        . '<a class="applyJob" data-page_type="dashboard" data-job_id="'.$this->jobs_id.'">Apply</a>';
			}
		}
		
		return $result;
	}
}
?>
