<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\posts\models\Userpost;
use app\modules\university\models\University;

class FavoritepostWidget extends Widget{
	public $post_id;

	public function init(){
		parent::init();
	}

	public function run(){
		$modelFavoritePost = Userpost::find()->where(['user_id' => \Yii::$app->user->id, 'post_id' => $this->post_id])->one();
		if($modelFavoritePost != null){
                    $result = '<button class="fav deleteWithFavorite class_styledeleteWithFavorite" data-post_id="'.$this->post_id.'"><span class="text">Delete with favorite</span><i class="fa fa-star pull-right"></i></button>';
		}else{
                    $result = '<button class="fav addToFavorite  class_styledeleteWithFavorite" data-post_id="'.$this->post_id.'"><span style="padding:0px; position:relative; left:7px;" class="text">Add to favorite</span><i class="fa fa-star pull-right"></i></button>';
		}
		return $result;
	}
}
?>
