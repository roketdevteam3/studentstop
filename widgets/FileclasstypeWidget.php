<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Classes;
use app\models\Videoconference;
use yii\helpers\Url;

class FileclasstypeWidget extends Widget{
    public $type;
    public $class_id;
    
    public function init(){
        parent::init();
    }

    public function run(){
        $name = '';
        if($this->type == 'class'){
            $modalClass = Classes::find()->where(['id' => $this->class_id])->asArray()->one();
            $name = '<a href="'.Url::home().'class/'.$modalClass['url_name'].'">'.$modalClass['name'].'</a>';
        }elseif($this->type == 'virtualclass'){
            $modalClass = Videoconference::find()->where(['id' => $this->class_id])->asArray()->one();
            $name = '<a href="'.Url::home().'videoconference/'.$modalClass['id'].'">'.$modalClass['name'].'</a>';            
        }
        
        return $name;
    }
}
?>
