<?php
namespace app\widgets;

use app\modules\jobs\models\OfferJob;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\jobs\models\Company;
use app\modules\jobs\models\Userjobs;
use app\modules\university\models\University;

class MakeOfferWidget extends Widget{
	public $user_id;
	public $offer_id;

	public function init(){
		parent::init();
	}

	public function run(){
    $OfferJob = OfferJob::find()->where(['id' => $this->offer_id])->one();
			if($OfferJob != null){
				$status = '';
				switch ($OfferJob->status){
          case 0:
            return HTML::a('Invite','#',[
              'class' => 'view-job',
              'data-toggle'=>"modal",
              'data-target'=>"#offer_job",
              'onclick'=>'$("#user_id_offer").val('.$this->user_id.')'
            ]);
            break;
					case 1:
						return '<h6>Status offer: Accept</h6>';
						break;
          case 2:
            return '<h6>Status offer: Maybe</h6>';
            break;
          case 3:
            return '<h6>Status offer: Not suitable</h6>';
            break;
				}
			}else{
				return HTML::a('Invite','#',[
          'class' => 'view-job',
          'data-toggle'=>"modal",
          'data-target'=>"#offer_job",
          'onclick'=>'$("#user_id_offer").val('.$this->user_id.')'
        ]);
      }
	}
}
?>
