<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\posts\models\Userrenthouse;
use app\modules\university\models\University;

class FavoriterenthouseWidget extends Widget{
	public $renthouse_id;

	public function init(){
		parent::init();
	}

	public function run(){
		$modelFavoriterenthouse = Userrenthouse::find()->where(['user_id' => \Yii::$app->user->id, 'renthouse_id' => $this->renthouse_id])->one();
		if($modelFavoriterenthouse != null){
			$result = '<button class="fav deleteWithFavorite" data-renthouse_id="'.$this->renthouse_id.'" ><span class="text">Delete with favorite</span><i class="fa fa-star pull-right"></i></button>';
		}else{
			$result = '<button class="fav addToFavorite" data-renthouse_id="'.$this->renthouse_id.'" ><span class="text">Add to favorite</span><i class="fa fa-star pull-right"></i></button>';
		}
		return $result;
	}
}
?>
