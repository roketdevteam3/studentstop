<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Videoconferenceuser;

class InvitevideocountWidget extends Widget{
	public $conference_id;
	public $user_id;

	public function init(){
		parent::init();
	}

	public function run(){
		$inviteCount = Videoconferenceuser::find()->where(['user_id' => \Yii::$app->user->id, 'status' => '0'])->count();
		
		return $inviteCount;
	}
}
?>
