<?php
namespace app\widgets;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use app\models\Major;
use app\models\Course;
use app\models\Classes;
use app\models\user\User;
use app\modules\university\models\University;

class FilterclassesWidget extends Widget{
    //public $message;

    public function init(){
        parent::init();
    }

    public function run(){
        $universityArray = ArrayHelper::map(University::find()->all(), 'id', 'name');
        $university_id = 0;
        if(!\Yii::$app->user->isGuest){
            $university_id = \Yii::$app->user->identity->getUsertUniversity();
        }
        $majorArray = ArrayHelper::map(Major::find()->where(['university_id' => $university_id])->all(), 'id', 'name');
        
        $courseArray = [];
        if(isset($_GET['major_class'])){
            if($_GET['major_class'] != ''){
                $courseArray = ArrayHelper::map(Course::find()->where(['id_major' => $_GET['major_class']])->all(), 'id', 'name');
            }
        }
        
        $classesArray = [];
        if(isset($_GET['course_class'])){
            if($_GET['course_class'] != ''){
                $classesArray = ArrayHelper::map(Classes::find()->where(['id_course' => $_GET['course_class']])->all(), 'id', 'name');
            }
        }
        
        $query = new Query();
        $query->select(['*', 'id' => '{{%user}}.id'])
                        ->from('{{%user}}')
                        ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%user}}.id'
                                )->where(['{{%user_info}}.academic_status' => 3]);

        $command = $query->createCommand();
        $modelProfessor = $command->queryAll();
        $professorArray = [];
        foreach($modelProfessor as $professor){
            $professorArray[$professor['id_user']] = $professor['name'].' '.$professor['surname'];
        }
        //var_dump($professorArray);exit;
        
        //$professorModal = User::find()->where([''])->asArray()->all();
        
        return $this->render('filterclasses',[
            'professorArray' => $professorArray,
            'universityArray' => $universityArray,
            'majorArray' => $majorArray,
            'courseArray' => $courseArray,
            'classesArray' => $classesArray,
            'university_id' => $university_id,
        ]);
    }
}
?>
