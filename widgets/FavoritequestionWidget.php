<?php
namespace app\widgets;

use app\modules\jobs\models\FavoriteCompany;
use app\modules\jobs\models\FavoriteRezume;
use app\modules\question\models\FavoriteQuestion;
use yii\base\Widget;

class FavoritequestionWidget extends Widget{
	public $question_id;

	public function init(){
		parent::init();
	}

	public function run(){
		$modelFavoriteQuestion = FavoriteQuestion::find()->where(['user_id' => \Yii::$app->user->id,'question_id'=>$this->question_id])->one();
		if($modelFavoriteQuestion != null){
                    $result = '<button  class="fav deleteWithFavoriteQuestion class_disconect"   data-question_id="'.$this->question_id.'"><span class="text"></span><i class="fa fa-star pull-right"></i></button>';
		}else{
                    $result = '<button class="fav addToFavoriteQuestion class_connect" data-question_id="'.$this->question_id.'"><span class="text"></span><i class="fa fa-star pull-right"></i></button>';
		}
		return $result;
	}
}
?>
