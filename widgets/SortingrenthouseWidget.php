<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\jobs\models\Company;
use app\modules\jobs\models\Jobsfunction;
use app\modules\jobs\models\Jobsindustry;
use app\modules\university\models\University;

class SortingrenthouseWidget extends Widget{

    public function init(){
        parent::init();
    }

    public function run(){
        $universityArray = ArrayHelper::map(University::find()->all(), 'id', 'name');
        
        return $this->render('sortingrenthouse',[
            'universityArray' => $universityArray,
        ]);
    }
}
?>
