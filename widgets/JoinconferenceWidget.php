<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\Videoconferenceuser;

class JoinconferenceWidget extends Widget{
    public $conference_id;

    public function init(){
        parent::init();
    }

    public function run(){
        $modeVideoconferenceUser = Videoconferenceuser::find()->where(['conference_id' => $this->conference_id, 'user_id' => \Yii::$app->user->id])->one();
        $modeVideoconference = \app\models\Videoconference::find()->where(['id' => $this->conference_id])->one();
        if($modeVideoconference->tutors_id != \Yii::$app->user->id){
            if($modeVideoconferenceUser == null){
                return '<a class="btn JoinConference" data-conference_id="'.$this->conference_id.'" >Join</a>';
            }else{
                if($modeVideoconferenceUser->status == 1){
                    return '<a class="btn btn-watch" href="'.Url::home().'videoconference/'.$this->conference_id.'" >Enrolled</a>';
                }else{
                    return '<a class="btn JoinConference" data-conference_id="'.$this->conference_id.'" >Join</a>';
                }
            }            
        }else{
            return '<a class="btn btn-watch" href="'.Url::home().'videoconference/'.$this->conference_id.'" >View</a>';            
        }
    }
}
?>
