<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Favorite;
use app\modules\posts\models\Userrents;
use app\modules\university\models\University;

class FavoritevirtualclassWidget extends Widget{
	public $virtual_class_id;

	public function init(){
            parent::init();
	}

	public function run(){
		$modelFavoriteClass = Favorite::find()->where(['user_id' => \Yii::$app->user->id,
		'object_id' => $this->virtual_class_id, 'type' => 'virtual_class'])->one();
		if($modelFavoriteClass != null){
			$result = '<i class="fa fa-star deleteConferenceWithFavorite" data-conference_id="'.$this->virtual_class_id.'" ></i>';
		}else{
			$result = '<i class="fa fa-star addConferenceToFavorite" data-conference_id="'.$this->virtual_class_id.'" ></i>';
		}
		return $result;
	}
}
?>
