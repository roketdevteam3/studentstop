<?php
namespace app\widgets;

use app\modules\jobs\models\FavoriteCompany;
use app\modules\jobs\models\FavoriteRezume;
use yii\base\Widget;

class FavoritecompanyWidget extends Widget{
	public $company_id;

	public function init(){
		parent::init();
	}

	public function run(){
		$modelFavoriteRezume = FavoriteCompany::find()->where(['user_id' => \Yii::$app->user->id,'company_id'=>$this->company_id])->one();
		if($modelFavoriteRezume != null){
			$result = '<button class="fav deleteWithFavoriteCompany" data-company_id="'.$this->company_id.'"><span class="text">Disconnect</span><i class="fa fa-star pull-right"></i></button>';
		}else{
			$result = '<button class="fav addToFavoriteCompany" data-company_id="'.$this->company_id.'"><span class="text">Connect</span><i class="fa fa-star pull-right"></i></button>';
		}
		return $result;
	}
}
?>
