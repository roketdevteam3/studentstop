<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\question\models\Questioncategory;
use app\modules\university\models\University;
use app\models\Country;

class QuestioncategoryWidget extends Widget{
    //public $jobs_id;
    
    public function init(){
        parent::init();
    }

    public function run(){
        $questonCategoryArray = ArrayHelper::map(Questioncategory::find()->where(['parent_id' => 0])->all(), 'id', 'name');
        
        return $this->render('questioncategory',[
            'questonCategoryArray' => $questonCategoryArray,
        ]);
    }
}
?>
