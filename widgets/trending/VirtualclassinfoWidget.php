<?php
namespace app\widgets\trending;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\modules\posts\models\Rentreserve;
use app\models\Classes;
use app\models\University;
use app\models\Videoconference;
use app\models\user\UserInfo;
use app\models\Classreting;
use app\modules\users\models\User;
use app\models\Videoconferenceuser;
use app\models\Ratingrequest;
use kartik\rating\StarRating;

class VirtualclassinfoWidget extends Widget{
    public $class_id;
    public $rating_count;

    public function init(){
        parent::init();
    }

    public function run(){
        $modelClass = Videoconference::find()->where(['id' => $this->class_id])->asArray()->one();
        $modelUniversity = University::find()->where(['id' => $modelClass['id_university']])->asArray()->one();
        $userCount = Videoconferenceuser::find()->where(['conference_id' => $this->class_id])->count();
        $modelProfessor = User::find()->where(['id' => $modelClass['tutors_id']])->one();
        
            $result = '<td>';
                $result .= '<a href="'.Url::home().'class/'.$modelClass['url_name'].'">'.$modelClass['name'].'</a>';
            $result .= '</td>';
            $result .= '<td>';
                $result .= '<a href="'.Url::home().'profile/'.$modelProfessor['id'].'">'.$modelProfessor['name'].' '.$modelProfessor['surname'].'</a>';
            $result .= '</td>';
            $result .= '<td>';
                $result .= '<a href="'.Url::home().'university/view/'.$modelUniversity['slug'].'">'.$modelUniversity['name'].'</a>';
            $result .= '</td>';
            $result .= '<td>';
                $result .= $userCount;
            $result .= '</td>';
            $result .= '<td style="font-size:12px;">';
                $result .= StarRating::widget([
                        'name' => 'rating_user',
                        'value' => $this->rating_count,
                        'pluginOptions' => [
                            'disabled'=>true,
                            'showClear'=>false,
                            'size'=>'xs',
                            'showCaption' => false,
                            ]
                    ]);
            $result .= '</td>';
        return $result;
    }
    
}
?>
