<?php
namespace app\widgets\trending;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\modules\posts\models\Rentreserve;
use app\models\Classes;
use app\models\University;
use app\models\user\User;
use app\models\user\UserInfo;
use app\models\Classreting;
use app\models\Follower;
use app\models\Videoconferenceuser;
use app\models\Ratingrequest;
use kartik\rating\StarRating;
use app\models\Major;

class UserinfoWidget extends Widget{
    public $user_id;
    public $rating_count;

    public function init(){
        parent::init();
    }

    public function run(){
        $modelUser = User::find()->where(['id' => $this->user_id])->asArray()->one();
        $modelUserInfo = UserInfo::find()->where(['id_user' => $this->user_id])->asArray()->one();
        $modelUniversity = University::find()->where(['id' => $modelUserInfo['university_id']])->one();
        $modelMajor = Major::find()->where(['id' => $modelUserInfo['major_id']])->asArray()->one();
        $major = '';
        if($modelMajor != null){
            $major = $modelMajor['name'];
        }
        
            $result = '<td>';
                $result .= '<a href="'.Url::home().'profile/'.$modelUser['id'].'">'.$modelUser['name'].' '.$modelUser['surname'].'</a>';
            $result .= '</td>';
            $result .= '<td>';
                $result .= $major;
            $result .= '</td>';
            $result .= '<td>';
                $result .= '<a href="'.Url::home().'university/view/'.$modelUniversity['slug'].'">'.$modelUniversity['name'].'</a>';
            $result .= '</td>';
            $result .= '<td>';
                $result .= Follower::find()->where(['id_user' => $this->user_id, 'type_object' => 'class'])->count();
            $result .= '</td>';
            $result .= '<td>';
                $result .= Videoconferenceuser::find()->where(['user_id' => $this->user_id])->count();
            $result .= '</td>';
            $result .= '<td style="font-size:12px;">';
                $result .= StarRating::widget([
                        'name' => 'rating_user',
                        'value' => $this->rating_count,
                        'pluginOptions' => [
                            'disabled'=>true,
                            'showClear'=>false,
                            'size'=>'xs',
                            'showCaption' => false,
                            ]
                    ]);
            $result .= '</td>';
        return $result;
    }
    
}
?>
