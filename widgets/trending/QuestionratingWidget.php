<?php
namespace app\widgets\trending;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\posts\models\Rentreserve;
use app\models\Classes;
use yii\helpers\Url;
use app\models\University;
use app\models\Videoconference;
use app\models\user\UserInfo;
use app\models\Classreting;
use app\models\Videoconferenceuser;
use app\models\Ratingrequest;
use app\modules\question\models\Question;
use kartik\rating\StarRating;

class QuestionratingWidget extends Widget{
    public $question_id;
    public $rating_count;

    public function init(){
        parent::init();
    }

    public function run(){
        $modelQuestion = Question::find()->where(['id' => $this->question_id])->asArray()->one();
        $modelUniversity = University::find()->where(['id' => $modelQuestion['university_id']])->asArray()->one();
        $modelUser = \app\modules\users\models\User::find()->where(['id' => $modelQuestion['user_id']])->asArray()->one();
        //$userCount = Videoconferenceuser::find()->where(['conference_id' => $this->class_id])->count();
        
            $result = '<td>';
                $result .= $modelQuestion['question_title'];
            $result .= '</td>';
            $result .= '<td>';
                $result .= '<a href="'.Url::home().'profile/'.$modelUser['id'].'">'.$modelUser['name'].' '.$modelUser['surname'].'</a>';
            $result .= '</td>';
            $result .= '<td>';
                $result .= '<a href="'.Url::home().'university/view/'.$modelUniversity['slug'].'">'.$modelUniversity['name'].'</a>';
            $result .= '</td>';
            $result .= '<td style="font-size:12px;">';
                $result .= StarRating::widget([
                        'name' => 'rating_user',
                        'value' => $this->rating_count,
                        'pluginOptions' => [
                            'disabled'=>true,
                            'showClear'=>false,
                            'size'=>'xs',
                            'showCaption' => false,
                            ]
                    ]);
            $result .= '</td>';
        return $result;
    }
    
}
?>
