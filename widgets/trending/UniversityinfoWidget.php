<?php
namespace app\widgets\trending;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\modules\posts\models\Rentreserve;
use app\models\Classes;
use app\models\University;
use app\models\user\UserInfo;
use app\models\Classreting;
use app\models\Ratingrequest;
use kartik\rating\StarRating;

class UniversityinfoWidget extends Widget{
    public $university_id;
    public $rating_count;

    public function init(){
        parent::init();
    }

    public function run(){
        $modelUniversity = University::find()->where(['id' => $this->university_id])->one();
        
        $classCount = Classes::find()->where(['id_university' => $this->university_id])->count();
//        $university
        
        $userCount = UserInfo::find()->where(['university_id' => $this->university_id])->count();
        
            $result = '<td>';
                $result .= '<a href="'.Url::home().'university/view/'.$modelUniversity['slug'].'">'.$modelUniversity['name'].'</a>';
            $result .= '</td>';
            $result .= '<td>';
                $result .= $classCount;
            $result .= '</td>';
            $result .= '<td>';
                $result .= $userCount;
            $result .= '</td>';
            $result .= '<td style="font-size:12px;">';
                $result .= StarRating::widget([
                        'name' => 'rating_user',
                        'value' => $this->rating_count,
                        'pluginOptions' => [
                            'disabled'=>true,
                            'showClear'=>false,
                            'size'=>'xs',
                            'showCaption' => false,
                            ]
                    ]);
            $result .= '</td>';
        return $result;
    }
    
}
?>
