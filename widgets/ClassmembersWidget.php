<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Classes;
use app\models\Follower;

class ClassmembersWidget extends Widget{
    public $class_id;

    public function init(){
        parent::init();
    }

    public function run(){
        $membersCount = Follower::find()->where(['id_object' => $this->class_id, 'type_object' => 'class'])->count();
        
        return $membersCount;        
    }
    
}
?>
