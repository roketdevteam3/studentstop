<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\university\models\University;
use app\models\Country;

class QuestionfilterWidget extends Widget{
    public $category_url;
    
    public function init(){
        parent::init();
    }

    public function run(){
        $universityArray = ArrayHelper::map(University::find()->all(), 'id', 'name');
        $countryArray = ArrayHelper::map(Country::find()->all(), 'id', 'name');
        return $this->render('questionfilter',[
            'universityArray' => $universityArray,
            'countryArray' => $countryArray,
            'category_url' => $this->category_url
        ]);
    }
}
?>
