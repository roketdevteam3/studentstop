<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\question\models\Question;

class QuestioncountWidget extends Widget{
    public $category_id;

    public function init(){
        parent::init();
    }

    public function run(){
        $countQuestion = Question::find()->where(['category_id' => $this->category_id])->count();
        return $countQuestion;
    }
}
?>
