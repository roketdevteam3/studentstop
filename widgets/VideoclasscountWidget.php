<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Follower;
use app\models\Videoconferenceuser;
use app\models\Videoconference;


class VideoclasscountWidget extends Widget{
    public $course_id;

    public function init(){
        parent::init();
    }

    public function run(){
        $videoclasscount = Videoconference::find()->where(['id_course' => $this->course_id])->count();
        
        return $videoclasscount;        
    }
    
}
?>
