<?php
namespace app\widgets;

use app\models\user\UserBonus;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\posts\models\Userpost;
use app\modules\university\models\University;

class UserLevelWidget extends Widget{
	public $user_id;

	public function init(){
		parent::init();
	}

	public function run(){

		$sum_bonuse = UserBonus::find()->where(['user_id'=>$this->user_id])->sum('bonus');
		return floor($sum_bonuse/50 - 1);
	}
}
?>
