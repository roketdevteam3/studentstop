<?php
namespace app\widgets;

use yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\user\User;
use app\models\user\UserFriend;

class FollowbuttonWidget extends Widget{
    public $friend_id;

    public function init(){
        parent::init();
    }

    public function run(){
        $modelFriend = UserFriend::find()->where(['id_user' => \Yii::$app->user->id, 'id_friend' => $this->friend_id])->one();
        if($modelFriend == null){
            //buuton confirm
            $modelFriendConfirm = UserFriend::find()->where(['id_user' => $this->friend_id, 'id_friend' => \Yii::$app->user->id])->one();
            if($modelFriendConfirm == null){
                $result = '<button class="btn btn-info followFriend" data-friend_id="'.$this->friend_id.'" data-my_id="'.\Yii::$app->user->id.'">Follow</button>';
            }else{
                $result = '<button class="btn btn-info confirmFriend" data-friend_id="'.$this->friend_id.'" data-my_id="'.\Yii::$app->user->id.'">Confirm</button>';
            }
        }else{
            $result = '<button class="btn btn-info unfollowFriend" data-friend_id="'.$this->friend_id.'" data-my_id="'.\Yii::$app->user->id.'">Unfollow</button>';
        }
        
        return $result;        
    }
    
}
?>
