<?php
namespace app\widgets\count;

use app\modules\posts\models\PostContsctRequest;
use app\modules\posts\models\Posts;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\posts\models\Rentreserve;
use kartik\rating\StarRating;

class PostscountWidget extends Widget{

    public function init(){
        parent::init();
    }

    public function run(){

        $coutn = 0;

        $posts = Posts::find()->where(['user_id'=>\Yii::$app->user->id])->all();

        foreach($posts as $post){
            $coutn += PostContsctRequest::find()->where(['post_id'=>$post->id,'view'=>0])->count();
        }
        return $coutn;
    }

}
?>
