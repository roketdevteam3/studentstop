<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Classes;
use app\models\Follower;
use app\modules\posts\models\Posts;

class PublishmarketpostWidget extends Widget{
    public $post_id;

    public function init(){
        parent::init();
    }

    public function run(){
        $postsCount = Posts::find()->where(['id' => $this->post_id])->one();
        if($postsCount != null){
            if($postsCount->publish_status != 0){
                return '<a class="btn btn-view UnPublishPost" data-post_id="'.$this->post_id.'">Unpublish</a>';
            }else{
                return '<a class="btn btn-view PublishPost" data-post_id="'.$this->post_id.'">Publish</a>';
            }
        }else{
            return '';        
        }
    }
    
}
?>
