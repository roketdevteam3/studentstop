<?php
namespace app\widgets\rating;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\posts\models\Rentreserve;
use app\models\Ratingrequest;
use kartik\rating\StarRating;

class QuestionbankratingWidget extends Widget{
    public $object_id;

    public function init(){
        parent::init();
    }

    public function run(){
        $modelRatingrequest = Ratingrequest::find()->where(['to_object_id' => $this->object_id, 'type' => 'question_bank'])->all();

        $countUser = 0;
        $countStar = 0;
        foreach($modelRatingrequest as $ratingrequest){
            $countUser++;
            $countStar = $countStar + $ratingrequest->rating_count;
        }
         if($countUser != 0){
            $countStarResult = $countStar/$countUser;
        }else{
            $countStarResult = 0;
        }
        
        $result = StarRating::widget([
                'name' => 'rating_user',
                'value' => $countStarResult,
                'pluginOptions' => [
                    'disabled'=>true,
                    'showClear'=>false,
                    'size'=>'xs',
                    'showCaption' => false,
                    ]
            ]);
        return $result;
    }
    
}
?>
