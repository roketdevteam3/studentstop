<?php
namespace app\widgets\rating;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Ratingrequest;
use kartik\rating\StarRating;

class UserratingWidget extends Widget{
    public $user_id;

    public function init(){
        parent::init();
    }

    public function run(){
        $modelRatingUser = Ratingrequest::find()->where(['to_object_id' => $this->user_id])->all();
        $countUser = 0;
        $countStar = 0;
        foreach($modelRatingUser as $request){
            $countUser++;
            $countStar = $countStar + $request['rating_count'];
        }
        $result = '';
        if($countUser == 0){
            $countStarResult = 0;
        }else{
            $countStarResult = round($countStar/$countUser,1);
        }
        $result .= StarRating::widget([
                'name' => 'rating_1',
                'value' => $countStarResult,
                'pluginOptions' => [
                    'disabled'=>true,
                    'showClear'=>false,
                    'size'=>'xs',
                    'showCaption' => false,
                    ]
            ]);
        
        return $result;
    }
    
}
?>
