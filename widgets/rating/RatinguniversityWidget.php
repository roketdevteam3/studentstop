<?php
namespace app\widgets\rating;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\posts\models\Rentreserve;
use app\models\Classes;
use app\models\Classreting;
use app\models\Ratingrequest;
use kartik\rating\StarRating;

class RatinguniversityWidget extends Widget{
    public $university_id;

    public function init(){
        parent::init();
    }

    public function run(){
        //$this->user_id;
        $modelClasses = Classes::find()->where(['id_university' => $this->university_id])->asArray()->all();
        $count_entry = 0;
        $count_star = 0;
        foreach($modelClasses as $class){
            $modelRatingClass = Classreting::find()->where(['class_id' => $class['id']])->all();
            $countUser = 0;
            $countStar = 0;
            foreach($modelRatingClass as $request){
                $countUser++;
                $countStar = $countStar + $request->rate_professor;
            }
            $result = '';
            if($countUser == 0){
                $countStarClassResult = 0;
            }else{
                $count_entry++;
                $countStarClassResult = round($countStar/$countUser,1); 
                $count_star = $count_star + $countStarClassResult;
            }
        }
        if($count_entry == 0){
            $countStarResult = 0;
        }else{
            $countStarResult = round($count_star/$count_entry,1); 
        }
        
        $result = StarRating::widget([
                'name' => 'rating_user',
                'value' => $countStarResult,
                'pluginOptions' => [
                    'disabled'=>true,
                    'showClear'=>false,
                    'size'=>'xs',
                    'showCaption' => false,
                    ]
            ]);
        return $result;
    }
    
}
?>
