<?php
namespace app\widgets\rating;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\posts\models\Rentreserve;
use app\models\Classes;
use app\models\Classreting;
use app\models\Ratingrequest;
use kartik\rating\StarRating;

class RatingvirtualprofessorWidget extends Widget{
    public $user_id;

    public function init(){
        parent::init();
    }

    public function run(){
        //$this->user_id;
        $arrayTypeRating = ['rental','rental_a_house','question_bank', 'professor'];
        $arrayRatingResult = [];
        foreach($arrayTypeRating as $rating_type){
            $modelRatingrequest = Ratingrequest::find()->where(['to_object_id' => $this->user_id, 'type' => 'virtualclass_professor'])->asArray()->all();
            $countUser = 0;
            $countStar = 0;
            foreach($modelRatingrequest as $rating){
                $countUser++;
                $countStar = $countStar + $rating['rating_count'];
            }
            $result = '';
            if($countUser == 0){
                $countStarResult = 0;
            }else{
                $countStarResult = round($countStar/$countUser,1); 
            }
        }
        
        $result = StarRating::widget([
                'name' => 'rating_user',
                'value' => $countStarResult,
                'pluginOptions' => [
                    'disabled'=>true,
                    'showClear'=>false,
                    'size'=>'xs',
                    'showCaption' => false,
                    ]
            ]);
        return $result;
    }
    
}
?>
