<?php
namespace app\widgets\rating;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Ratingrequest;
use app\models\Classreting;
use app\models\Classes;
use kartik\rating\StarRating;

class RankclassWidget extends Widget{
    public $course_id;
    public $class_id;

    public function init(){
        parent::init();
    }

    public function run(){
        $course_id = $this->course_id;
        $class_id = $this->class_id;
        $modelClasses = Classes::find()->where(['id_course' => $course_id])->all();
        $count_entry = 0;
        $count_star = 0;
        $classRatingArray = [];
        $class_count = 0;
        foreach($modelClasses as $class){
            $class_count++;
            $modelRatingClass = Classreting::find()->where(['class_id' => $class['id']])->all();
            $countUser = 0;
            $countStar = 0;
            foreach($modelRatingClass as $request){
                $countUser++;
                $countStar = $countStar + $request->rate_professor;
            }
            $result = '';
            if($countUser == 0){
                $countStarClassResult = 0;
            }else{
                $count_entry++;
                $countStarClassResult = round($countStar/$countUser,1); 
            }
            $classRatingArray[$class['id']] = $countStarClassResult;
        }
        arsort($classRatingArray);
        $rent_number = 0;
        foreach($classRatingArray as $key => $class_rating){
            $rent_number = $rent_number + 1;
            if($key == $class_id){
                break;
            }
        }
        $result = $rent_number.'/'.$class_count;
                
        return $result;
    }
    
}
?>
