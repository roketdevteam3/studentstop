<?php
namespace app\widgets\rating;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Ratingrequest;
use app\models\Classreting;
use kartik\rating\StarRating;

class RatingclassWidget extends Widget{
    public $class_id;
    public $type;
    public $type_show;

    public function init(){
        parent::init();
    }

    public function run(){
        $modelRatingClass = Classreting::find()->where(['class_id' => $this->class_id])->all();
        $countUser = 0;
        $countStar = 0;
        foreach($modelRatingClass as $request){
            $countUser++;
            $countStar = $countStar + $request->rate_professor;
        }
        $result = '';
        if($countUser == 0){
            $countStarResult = 0;
        }else{
            $countStarResult = round($countStar/$countUser,1); 
        }
        if($this->type_show == 'with_rat'){
            $result .= '<span class="rating-count">'.$countStarResult.'/'.$countUser.'</span>';
        }
            $result .= StarRating::widget([
                            'name' => 'rating_1',
                            'value' => $countStarResult,
                            'pluginOptions' => [
                                'disabled'=>true,
                                'showClear'=>false,
                                'size'=>'xs',
                                'showCaption' => false,
                                ]
                        ]);
        return $result;
    }
    
}
?>
