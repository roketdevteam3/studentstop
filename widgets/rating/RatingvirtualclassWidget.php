<?php
namespace app\widgets\rating;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Ratingrequest;
use app\models\Virtualclassreting;
use kartik\rating\StarRating;

class RatingvirtualclassWidget extends Widget{
    public $class_id;

    public function init(){
        parent::init();
    }

    public function run(){
        $modelRatingClass = Virtualclassreting::find()->where(['class_id' => $this->class_id])->all();
        $countUser = 0;
        $countStar = 0;
        foreach($modelRatingClass as $request){
            $countUser++;
            $countStar = $countStar + $request->rate_professor;
        }
        $result = '';
        if($countUser == 0){
            $countStarResult = 0;
        }else{
            $countStarResult = round($countStar/$countUser,1); 
        }
            $result = StarRating::widget([
                            'name' => 'rating_1',
                            'value' => $countStarResult,
                            'pluginOptions' => [
                                'disabled'=>true,
                                'showClear'=>false,
                                'size'=>'xs',
                                'showCaption' => false,
                                ]
                        ]);
        return $result;
    }
    
}
?>
