<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Categories;
use app\modules\jobs\models\Company;
use app\modules\jobs\models\Jobsfunction;
use app\modules\jobs\models\Jobsindustry;
use app\modules\university\models\University;

class FiltermarketWidget extends Widget{
    public $route;
    
    public function init(){
        parent::init();
    }

    public function run(){
        $universityArray = ArrayHelper::map(University::find()->all(), 'id', 'name');
        $postCategory = Categories::find()->where(['category_type' => 3])->all();
        $first_category = '';
        $parent_first_category = '';
        $categoryArray = [];
        foreach ($postCategory as $category){
            if($first_category == ''){
                $parent_first_category = $category['url_name'];
                $first_category = Categories::find()->where(['parent_id' => $category['id']])->andWhere(['not', ['category_type' => 2]])->one();
            }
            $categoryArray[$category->category_name] = ArrayHelper::map(Categories::find()->where(['parent_id' => $category['id']])->andWhere(['not', ['category_type' => 2]])->all(),'url_name','category_name');
        }
        if(isset($_GET['category'])){
            if($_GET['category'] != ''){
                $modelCategory = Categories::find()->where(['url_name' => $_GET['category']])->one();
                if($modelCategory != null){
                    $modelParentCategory = Categories::find()->where(['id' => $modelCategory->parent_id])->one();
                    $parent_first_category = $modelParentCategory->url_name;
                }
            }
        }
        
        return $this->render('filterposts',[
            'universityArray' => $universityArray,
            'categoryArray' => $categoryArray,
            'first_category' => $first_category,
            'parent_first_category' => $parent_first_category,
            'route' => $this->route,
        ]);
    }
}
?>
