<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Ratingrequest;
use kartik\rating\StarRating;

class RatingstarWidget extends Widget{
    public $user_id;
    public $type;
    public $type_show;

    public function init(){
        parent::init();
    }

    public function run(){
        $modelRatingrequest = Ratingrequest::find()->where(['to_object_id' => $this->user_id, 'type' => $this->type])->all();
        $countUser = 0;
        $countStar = 0;
        foreach($modelRatingrequest as $request){
            $countUser++;
            $countStar = $countStar + $request->rating_count;
        }
        $result = '';
        if($countUser == 0){
            $countStarResult = 0;
        }else{
            $countStarResult = round($countStar/$countUser,1); 
        }
        if($this->type_show == 'with_rat'){
            $result .= '<span class="rating-count">'.$countStarResult.'/'.$countUser.'</span>';
        }
            $result .= StarRating::widget([
                            'name' => 'rating_1',
                            'value' => $countStarResult,
                            'pluginOptions' => [
                                'disabled'=>true,
                                'showClear'=>false,
                                'size'=>'xs',
                                'showCaption' => false,
                                ]
                        ]);
        return $result;
    }
    
}
?>
