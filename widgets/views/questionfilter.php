<?php
		use yii\helpers\Html;
		use yii\helpers\Url;
		use yii\bootstrap\ActiveForm;
?>

	<div class="row">
		<?php 
			if($category_url != ''){
					$action = Url::home().'question_category/'.$category_url;
			}else{
					$action = Url::home().'question_category';
			}
			$form = ActiveForm::begin([
				'action' => $action,
				'method' => 'GET',
				'options' => [
					'class' => 'question-form'
				]
			]); ?>
			<div class="input-group">
				<div class="form-group">
					<?php 
							$search_key = '';
							if(isset($_GET['search_key'])){
									$search_key = $_GET['search_key'];
							}else{
									$search_key = null;
							}
					?>
					<input type="text" class="searchInput form-control" value="<?= $search_key; ?>" placeholder="Search" name="search_key">
					<?php 
							$university = '';
							if(isset($_GET['university'])){
									$university = $_GET['university'];
							}else{
									$university= null;
							}
					?>
					<?= Html::dropDownList('university', $university, $universityArray,['prompt' => 'University','class' => 'form-control']); ?>
					<?php 
							$country = '';
							if(isset($_GET['country'])){
									$country = $_GET['country'];
							}else{
									$country= null;
							}
					?>
					<?= Html::dropDownList('country_list', $country, $countryArray,['prompt' => 'Country','class' => 'form-control']); ?>
					<?php 
							$state = '';
							if(isset($_GET['state'])){
									$state = $_GET['state'];
							}else{
									$state= null;
							}
					?>
					<?= Html::dropDownList('states_list', $state,[],['prompt' => 'State','class' => 'form-control']); ?>
				</div>
				<div class="input-group-btn">
					<input type="submit" name="searchButton" value="Search" class="btn btn-search">
				</div>
			</div>
		<?php $form = ActiveForm::end();  ?>
	</div>