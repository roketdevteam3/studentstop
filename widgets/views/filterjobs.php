<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\ActiveForm;
?>
			
	  <!--      
			if(isset($_GET['salary_from'])){
			if(isset($_GET['salary_to'])){

	  -->

<?php $form = ActiveForm::begin(['action' => Url::home().'searchjob',
	'method' => 'GET', 'options' => ['class' => '']
	]); ?>

	<?php
		$jobs_value = '';
		if(isset($_GET['jobs_value'])){
		  $jobs_value = $_GET['jobs_value'];
		}
	?>
	<input type="hidden" name="jobs_value" value="<?=$jobs_value?>">
	<?php
		$type_search = '';
		if(isset($_GET['type_search'])){
		  $type_search = $_GET['type_search'];
		}
	?>
	<input type="hidden" name="jobs_value" value="<?=$type_search?>">
	<?php
		$location = '';
		if(isset($_GET['location'])){
			$location = $_GET['location'];
		} 
	?>
	<div class="form-group">
		<label>Location</label>
	  <?php
	  echo \pudinglabs\tagsinput\TagsinputWidget::widget([
		'name'=>'location',
		'value'=>$location,
		'options' => [],
		'clientOptions' => [],
		'clientEvents' => []
	  ])
	  ?>
	</div>

	
	<?php
	if($role == 'student'){
		$company = '';
		if(isset($_GET['company'])){
			$company = $_GET['company'];
		}else{
			$company = null;
		}
	?>
	<div class="form-group">
		<?php
			echo \kartik\select2\Select2::widget([
				'name' => 'company',
				'value' => $company, // initial value
				'data' => $companyArray,
				'maintainOrder' => true,
				'options' => ['placeholder' => 'Company', 'multiple' => true],
				'pluginOptions' => [
	//          'tags' => true,
				'maximumInputLength' => 10
				],
			]);
		?>
	</div>
	<?php } else { ?>

	<?php 
		$university = '';
		if(isset($_GET['university'])){
			$university = $_GET['university'];
		}else{
			$university= null;
		}
	?>

	<div class="form-group">
		<label>University</label>
		<?php
			echo \kartik\select2\Select2::widget([
				'name' => 'university',
				'value' => $university, // initial value
				'data' => $universityArray,
				'maintainOrder' => true,
				'options' => ['placeholder' => 'University', 'multiple' => true],
				'pluginOptions' => [
		//          'tags' => true,
				'maximumInputLength' => 10
				],
			]);
		?>
	</div>

	<?php } ?>
	<div class="form-group">
		<label>Salary min</label>
		<?php 
			$salary_from = '';
			if(isset($_GET['salary_from'])){
				$salary_from = $_GET['salary_from'];
			}
		?>
		<input type="text" name="salary_from" value="<?= $salary_from; ?>" placeholder="From" class="form-control">
	</div>
	<div class="form-group">
		<label>Salary max</label>
		<?php 
			$salary_to = '';
			if(isset($_GET['salary_to'])){
				$salary_to = $_GET['salary_to'];
			}
		?>
		<input type="text" name="salary_to" value="<?= $salary_to; ?>" placeholder="To" class="form-control">
	</div>

	<?php 
		$job_function = '';
		if(isset($_GET['job_function'])){
			$job_function = $_GET['job_function'];
		}else{
			$job_function = null;
		}
	?>

	<div class="form-group">
		<label>Job function</label>
		<?php
			echo \kartik\select2\Select2::widget([
				'name' => 'job_function',
				'value' => $job_function, // initial value
				'data' => $jobfunctionArray,
				'maintainOrder' => true,
				'options' => ['placeholder' => 'Job function', 'multiple' => true],
				'pluginOptions' => [
	//          'tags' => true,
				'maximumInputLength' => 10
				],
			]);
		?>
	</div>
	<?php 
		$industry = '';
		if(isset($_GET['industry'])){
			$industry = $_GET['industry'];
		}else{
			$industry = null;
		}
	?>

	<div class="form-group">
		<label>Industry</label>
		<?php
			echo \kartik\select2\Select2::widget([
				'name' => 'industry',
				'value' => $industry, // initial value
				'data' => $industryArray,
				'maintainOrder' => true,
				'options' => ['placeholder' => 'Industry', 'multiple' => true],
				'pluginOptions' => [
		//          'tags' => true,
				  'maximumInputLength' => 10
				],
			]);
		?>
	</div>

	<?php if ($role != 'student') { ?>
	<?php
		$date_begin_education = '';
		if(isset($_GET['date_begin_education'])){
		  $date_begin_education = $_GET['date_begin_education'];
		}else{
		  $date_begin_education = null;
		}
	?>

	<div class="form-group">
		<label>Date begin education</label>
	  <?php
	  echo \kartik\date\DatePicker::widget([
		'name' => 'date_begin_education',
		'type' => \kartik\date\DatePicker::TYPE_INPUT,
		'value' => $date_begin_education,
		'options' => ['class' => 'date-picker'],
		'pluginOptions' => [
		  'autoclose'=>true,
		  'format' => 'yyyy-mm-dd'
		]
	  ]);
	  ?>
	</div>


	  <?php
	  $date_end_education = '';
	  if(isset($_GET['date_end_education'])){
		$date_end_education = $_GET['date_end_education'];
	  }else{
		$date_end_education = null;
	  }
	  ?>

	<div class="form-group">
		<label>Date end education</label>
	  <?php
	  echo \kartik\date\DatePicker::widget([
		'name' => 'date_end_education',
		'type' => \kartik\date\DatePicker::TYPE_INPUT,
		'value' => $date_end_education,
		'options' => ['class' => 'date-picker'],
		'pluginOptions' => [
		  'autoclose'=>true,
		  'format' => 'yyyy-mm-dd'
		]
	  ]);
	  ?>
	</div>

<?php } ?>

	<?php
		$skills = '';
		if(isset($_GET['skills'])){
		  $skills = $_GET['skills'];
		}else{
		  $skills = null;
		}
	?>

	<div class="form-group">
	<label>Skills</label>
	<?php
		echo \pudinglabs\tagsinput\TagsinputWidget::widget([
		  'name'=>'skills',
		  'value'=>$skills,
		  'options' => [],
		  'clientOptions' => [],
		  'clientEvents' => []
		])
	?>
	</div>

	<input type="submit" name="searchButton" value="Search" class="btn">
<?php $form = ActiveForm::end();  ?>