<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\ActiveForm;
		use kartik\date\DatePicker;
		use app\assets\PostsAsset;
		PostsAsset::register($this);
?>

	<div class="classified-mr-filter-form-wrap">
		<?php 
			if(isset($_GET['category'])){
				$action = Url::home().'market/'.$parent_first_category.'/'.$_GET['category'];
			}else{
				$action = Url::home().'market/'.$parent_first_category.'/'.$first_category->url_name;
			}
		?>
		<?php $form = ActiveForm::begin(['action' => $action,
		'method' => 'GET',
			'id' => 'form_posts'
		]); ?>
			
		<?php 
		$first_categoryposts_content = '';
			$posts_content = '';
			if(isset($_GET['posts_content'])){
				$posts_content = $_GET['posts_content'];
			}
		?>
		<div class="form-group">
			<label for="">Post title</label>
			<div>
				<input type="text" name="posts_content" value="<?= $posts_content; ?>" class="PlaceholderInput" placeholder="__">
				
			</div>
		</div>
		<div class="form-group">
			<label>Activities</label>
			<div class="">
				<?php 
				$category = '';
					if(isset($_GET['category'])){
						$category = $_GET['category'];
					}
				?>
				<?= Html::dropDownList('category', $category, $categoryArray,['id' => 'category_filter','class' => 'form-control']); ?>
			</div>
		</div>
		<div class="form-group">
			<label for="">University</label>
			<?php 
			 $university = '';
				if(isset($_GET['university'])){
					$university = $_GET['university'];
				}
			?>
			<?= Html::dropDownList('university', $university, $universityArray,['prompt' => 'University','class' => 'form-control']); ?>
		</div>
		<?php 
			$wifi = '';
			if(isset($_GET['wifi'])){
				$wifi = 'checked="checked"';
			}
		?>
		<input type="submit" name="searchButton" value="Search" class="btn">
		<?php $form = ActiveForm::end();  ?>
	</div>