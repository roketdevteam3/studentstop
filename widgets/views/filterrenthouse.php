<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\ActiveForm;
		use kartik\date\DatePicker;
?>
			
	  <!--      
			if(isset($_GET['salary_from'])){
			if(isset($_GET['salary_to'])){

	  -->
<div class="rents-filter-form-wrap">
	<?php 
		if($route == 'posts/rentshouse/index'){
			$action = '';
		}else{
			$action = Url::home().'rentshouse';
		}
	?>
	<?php $form = ActiveForm::begin(['action' => '/rentshouse',
	'method' => 'GET'
	]); ?>



		<?php
		$rent_value = '';
		if(isset($_GET['rent_value'])){
			$rent_value = $_GET['rent_value'];
		}
		?>
		<input type="hidden" name="rent_value" value="<?=$rent_value?>">


		<?php
		$reserve_from = '';
		if(isset($_GET['reserve_from'])){
			$reserve_from = $_GET['reserve_from'];
		}
		?>
		<input type="hidden" name="reserve_from" value="<?=$reserve_from?>">


		<?php
		$reserve_to = '';
		if(isset($_GET['reserve_to'])){
			$reserve_to = $_GET['reserve_to'];
		}
		?>
		<input type="hidden" name="reserve_to" value="<?=$reserve_to?>">

		<?php
		$adults = '';
		if(isset($_GET['adults'])){
			$adults = $_GET['adults'];
		}
		?>
		<input type="hidden" name="adults" value="<?=$adults?>">








		<?php 
			$wifi = '';
			if(isset($_GET['wifi'])){
				if ($_GET['wifi'] == 'on')
					$wifi = 'checked="checked"';
			}
		?>
		<div class="ckecking-wrap">
			<div class="form-group check-group">
				<label class="filter-check wi-fi">
					<input type="checkbox" id="rentshouse-wifi" <?= $wifi; ?> name="wifi">
					<label for="rentshouse-wifi"></label>
				</label>
			</div>
			<?php
			    $parking = '';
				if(isset($_GET['parking'])){
					if ($_GET['parking'] == 'on')
						$parking = 'checked="checked"';
				}
			?>
			<div class="form-group check-group">
				<label class="filter-check parking">
					<input id="parking" type="checkbox" <?= $parking; ?> name="parking">
					<label for="parking"></label>
				</label>
			</div>
	
			<?php
			$smoking = '';
			if(isset($_GET['smoking'])){
				if ($_GET['smoking'] == 'on')
					$smoking = 'checked="checked"';
			}
			?>
			<div class="form-group check-group">
				<label class="filter-check smoking">
					<input id="smoking" type="checkbox" <?= $smoking; ?> name="smoking">
					<label for="smoking"></label>
				</label>
			</div>
			<?php
			$tv = '';
			if(isset($_GET['tv'])){
				if ($_GET['tv'] == 'on')
					$tv = 'checked="checked"';
			}
			?>
			<div class="form-group check-group">
				<label class="filter-check tv">
					<input id="tv" type="checkbox" <?= $tv; ?>  name="tv">
					<label for="tv"></label>
				</label>
			</div>
	
			<?php
			$animal = '';
			if(isset($_GET['animal'])){
				if ($_GET['animal'] == 'on')
					$animal = 'checked="checked"';
			}
			?>
			<div class="form-group check-group">
				<label class="filter-check animal">
					<input id="animal" type="checkbox" <?= $animal; ?> name="animal">
					<label for="animal"></label>
				</label>
			</div>
			<?php
			$conditioner = '';
			if(isset($_GET['conditioner'])){
				if ($_GET['conditioner'] == 'on')
					$conditioner = 'checked="checked"';
			}
			?>
			<div class="form-group check-group">
				<label class="filter-check conditioner">
					<input id="conditioner" type="checkbox"  <?= $conditioner; ?> name="conditioner">
					<label for="conditioner"></label>
				</label>
			</div>
			
		</div>
		<!--<div class="form-group">
			<?php
/*			$min_rent = '';
			if(isset($_GET['min_rent'])){
				$min_rent = $_GET['min_rent'];
			}
			*/?>
			<label>Min. rent </label>
			<?php
/*				echo  Html::dropDownList('min_rent', $min_rent, ['1'=>'day', '7'=>'week', '30'=>'month'],['id'=>'filter_min_rent', 'class'=>'form-control']);
			*/?>
		</div>-->
		<!-- <div class="form-group calendar"> -->
			<?php
				// $reserve_from = '';
				// if(isset($_GET['reserve_from'])){
						// $reserve_from = $_GET['reserve_from'];
				// }
				// echo DatePicker::widget([
					// 'name' => 'reserve_from',
					// 'type' => DatePicker::TYPE_INPUT,
					// 'value' => $reserve_from,
					// 'options' => ['class' => 'PlaceholderInput','placeholder' => 'Check-in','id'=>'filter_reserve_for'],
					// 'pluginOptions' => [
						// 'autoclose'=>true,
						// 'format' => 'yyyy-mm-dd'
					// ]
				// ]);
			?>
		<!-- </div> -->
		<!-- <div class="form-group calendar"> -->
			<?php
				// $reserve_to = '';
				// if(isset($_GET['reserve_to'])){
						// $reserve_to = $_GET['reserve_to'];
				// }
				// echo DatePicker::widget([
					// 'name' => 'reserve_to',
					// 'type' => DatePicker::TYPE_INPUT,
					// 'value' => $reserve_to,
					// 'options' => ['placeholder' => 'Check-out', 'onchange'=>'limitDate()', 'id'=>'filter_reserve_to'],
					// 'pluginOptions' => [
						// 'autoclose'=>true,
						// 'format' => 'yyyy-mm-dd'
					// ]
				// ]);
			?>
		<!-- </div> -->


		<div class="form-group num-filter">
			<label>Room type</label>
			<?php
			$house_type = '';
			if(isset($_GET['house_type'])){
				$house_type = $_GET['house_type'];
			}
				echo  Html::dropDownList('house_type', $house_type, ['Private'=>'Private', 'Shared'=>'Shared'],['id'=>'filter_house_type', 'class'=>'form-control', 'onchange'=>'typeHouse()']);
			?>
		</div>
		<?php 
			$rooms = '';
			if(isset($_GET['room_count'])){
					$rooms = $_GET['room_count'];
			}
		?>
		<div class="form-group num-filter" id="group_input_root_count" style="<?= ($house_type=='Shared') ? '' : 'display: none;'?>">
			<label>Room count</label>
			<input type="number"  name="room_count" class="form-control" id="room_count" value="<?= $rooms; ?>">
		</div>
		<div class="form-group">
			<label>Price min</label>
			<?php 
					$price_from = '';
					if(isset($_GET['price_from'])){
							$price_from = $_GET['price_from'];
					}
			?>
			<input type="number" id="price_min" name="price_from" value="<?= $price_from; ?>" class="form-control">
		</div>
		<div class="form-group">
			<label>Price max</label>
			<?php
					$price_to = '';
					if(isset($_GET['price_to'])){
							$price_to = $_GET['price_to'];
					}
			?>
			<input type="number" id="price_max" name="price_to" value="<?= $price_to; ?>" class="form-control">
		</div>
		<!--<div class="form-group">
			<?php /*
				$adults = '';
				if(isset($_GET['adults'])){
					$adults = $_GET['adults'];
				}
			*/?>
			<label>Adults</label>
			<input type="number"  name="adults" class="form-control" value="<?/*= $adults; */?>">
		</div>-->
	    <div class="form-group">
			<label>Rating</label>
			<?php
			$rating = '';
			if(isset($_GET['rating'])){
				$rating = $_GET['rating'];
			}
			echo \kartik\rating\StarRating::widget([
					'name' => 'rating',
					'value' => $rating,
					'pluginOptions' => [
							'step' => '1',
							'size'=>'xs',
							'showCaption' => false,
							'showClear'=>false,
					],
			]); ?>
		</div>
		<input type="submit" name="searchButton" value="Search" class="btn">
	<?php $form = ActiveForm::end(); ?>
</div>

<script>
	function limitDate() {
		var date_for =  $('#filter_reserve_for').val().split('-');
		var date_to =  $('#filter_reserve_to').val().split('-');
		var min_date =  $('#filter_min_rent').val();
		var day = Math.round((new Date(date_to[0], date_to[1], date_to[2]) - new Date(date_for[0], date_for[1], date_for[2]))/(1000*60*60*24))+1;
		console.log('day',day);
		console.log('date_for',date_for);
		console.log('date_to',new Date(date_to[0], date_to[1], date_to[2]));
		if (day < parseInt(min_date)){
			var messege = '';
			switch (parseInt(min_date)){
				case 7:
				  messege= 'Min limit rent week';
				  break;
				case 30:
					messege= 'Min limit rent month';
					break;
			}
			swal("Warning!", messege, "warning")
			$('#filter_reserve_to').val('');
		}
	}

	function typeHouse(){
		if($('#filter_house_type').val() == 'Shared' ){
			$('#group_input_root_count').show();
		} else {
			$('#group_input_root_count').hide();
			$('#room_count').val('')
		}
	}
</script>