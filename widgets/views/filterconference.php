<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\ActiveForm;
?>

<?php $form = ActiveForm::begin(['action' => Url::home().'searchvirtualclass',
	'method' => 'GET', 'options' => ['class' => 'courseclases-form']
	]); ?>
		<!--<div class="form-group">
			<?php 
                        /*
				$name_conference = '';
				if(isset($_GET['name_conference'])){
					$name_conference = $_GET['name_conference'];
				} */
			?>
			<input type="input" placeholder="..." name="name_conference" value="<?php //= $name_conference; ?>">
		</div> -->

		<div class="form-group">
			<?php 
				$university = '';
				if(isset($_GET['university_conference'])){
						$university = $_GET['university_conference'];
				}else{
						$university= null;
				}
			?>
			<label for="">
				University
			</label>
			<?= Html::dropDownList('university_conference', $university, $universityArray,['prompt' => 'University','class' => 'form-control']); ?>
		</div>
		<div class="form-group">
			<?php 
				$major = '';
				if(isset($_GET['major_conference'])){
						$major = $_GET['major_conference'];
				}else{
						$major= null;
				}
			?>
			<label for="">
				Major
			</label>
			<?= Html::dropDownList('major_conference', $major, $majorArray,['prompt' => '...','class' => 'form-control']); ?>
		</div>
		<div class="form-group">
			<?php 
				$course = '';
				if(isset($_GET['course_conference'])){
					$course = $_GET['course_conference'];
				}else{
					$course= null;
				}
			?>
			<label for="">
				Course
			</label>
			<?= Html::dropDownList('course_conference', $course, $courseArray,['prompt' => '...','class' => 'form-control']); ?>
		</div>
		<div class="form-group">
			<?php
				$tutors = '';
				if(isset($_GET['tutors_conference'])){
						$tutors = $_GET['tutors_conference'];
				}else{
						$tutors = null;
				}
			?>
			<label for="">
				Tutors
			</label>
			<?= Html::dropDownList('tutors_conference', $tutors, $tutorsArray,['prompt' => '...','class' => 'form-control']); ?>
		</div>
		<div class="form-group">
                    <input type="submit" name="searchButton" value="Search" class="btn">
		</div>
	<?php $form = ActiveForm::end();  ?>