<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\ActiveForm;
		use kartik\date\DatePicker;
		use app\assets\RentalAsset;
		RentalAsset::register($this);
?>

<div class="rents-filter-form-wrap">
	<?php 
		if(isset($_GET['category'])){
			$action = Url::home().'rents/'.$_GET['category'];
		}else{
			$action = Url::home().'rents/'.$first_category->url_name;
		}
	?>
	<?php $form = ActiveForm::begin(['action' => $action,
	'method' => 'GET',
		'id' => 'form_rental'
	]); ?>
	<!--<div class="form-group">
		<?php
/*		$min_rent = '';
		if(isset($_GET['min_rent'])){
			$min_rent = $_GET['min_rent'];
		}
		*/?>
		<label>Min. rent </label>
		<?php
/*		echo  Html::dropDownList('min_rent', $min_rent, ['1'=>'day', '7'=>'week', '30'=>'month'],['id'=>'filter_min_rent','class' => 'form-control']);
		*/?>
	</div>-->
	<div class="form-group">
		<label>
			Category
		</label>
		<?php 
		$category = '';
			if(isset($_GET['category'])){
				$category = $_GET['category'];
			}
		?>
		<?= Html::dropDownList('category', $category, $categoryArray,['id' => 'category_filter','class' => 'form-control']); ?>
	</div>
	<!--<div class="form-group calendar">
		--><?php /*
			$reserve_from = '';
			if(isset($_GET['reserve_from'])){
					$reserve_from = $_GET['reserve_from'];
			}
			echo DatePicker::widget([
				'name' => 'reserve_from',
				'type' => DatePicker::TYPE_INPUT,
				'value' => $reserve_from,
				'options' => [ 'placeholder' => 'Check-in', 'onchange'=>'limitDate()', 'id'=>'filter_reserve_for'],
				'pluginOptions' => [
					'autoclose'=>true,
					'format' => 'yyyy-mm-dd'
				]
			]);
		*/?>
	<!--</div>
	<div class="form-group calendar">-->
		<?php
			/*$reserve_to = '';
			if(isset($_GET['reserve_to'])){
					$reserve_to = $_GET['reserve_to'];
			}
			echo DatePicker::widget([
				'name' => 'reserve_to',
				'type' => DatePicker::TYPE_INPUT,
				'value' => $reserve_to,
				'options' => ['placeholder' => 'Check-out','onchange'=>'limitDate()', 'id'=>'filter_reserve_to'],
				'pluginOptions' => [
					'autoclose'=>true,
					'format' => 'yyyy-mm-dd'
				]
			]);*/
		?>
<!--	</div>-->
	<?php 
			$rooms = '';
			if(isset($_GET['room_count'])){
					$rooms = $_GET['room_count'];
			} 
	?>
	<div class="form-group">
		<?php 
			$university = '';
			if(isset($_GET['university'])){
				$university = $_GET['university'];
			} 
		?>
		<?= Html::dropDownList('university', $university, $universityArray,['prompt' => 'University','class' => 'form-control']); ?>
	</div>
	<div class="form-group price-filter num-filter">
		<label>Price min</label>
		<?php 
				$price_from = '';
				if(isset($_GET['price_from'])){
						$price_from = $_GET['price_from'];
				}
		?>
		<input type="number" id="price_min" name="price_from" value="<?= $price_from; ?>" class="form-control">
	</div>
	<div class="form-group">
		<label>Price max</label>
		<?php 
			$price_to = '';
			if(isset($_GET['price_to'])){
					$price_to = $_GET['price_to'];
			}
		?>
		<input type="number" id="price_max" name="price_to" value="<?= $price_to; ?>" class="form-control">
	</div>
	<?php 
		$wifi = '';
		if(isset($_GET['wifi'])){
			$wifi = 'checked="checked"';
		} 
	?>
	<input type="submit" name="searchButton" value="Search" class="btn">
	<?php $form = ActiveForm::end();  ?>
</div>

<script>
	function limitDate() {
		var date_for =  $('#filter_reserve_for').val().split('-');
		var date_to =  $('#filter_reserve_to').val().split('-');
		var min_date =  $('#filter_min_rent').val();
		var day = Math.round((new Date(date_to[0], date_to[1], date_to[2]) - new Date(date_for[0], date_for[1], date_for[2]))/(1000*60*60*24))+1;
		console.log('day',day);
		console.log('date_for',date_for);
		console.log('date_to',new Date(date_to[0], date_to[1], date_to[2]));
		if (day < parseInt(min_date)){
			var messege = '';
			switch (parseInt(min_date)){
				case 7:
					messege= 'Min limit rent week';
					break;
				case 30:
					messege= 'Min limit rent month';
					break;
			}
			swal("Warning!", messege, "warning")
			$('#filter_reserve_to').val('');
		}
	}

	function typeHouse(){
		if($('#filter_house_type').val() == 'Shared' ){
			$('#group_input_root_count').show();
		} else {
			$('#group_input_root_count').hide();
			$('#room_count').val('')
		}
	}
</script>