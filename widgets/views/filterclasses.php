<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\ActiveForm;
?>
			
	  <!--      
			if(isset($_GET['salary_from'])){
			if(isset($_GET['salary_to'])){

	  -->

	<div class="courseclases-form">
		<?php $form = ActiveForm::begin(['action' => Url::home().'searchclasses',
			'method' => 'GET', 'options' => ['class' => '']
			]); ?>
				<?php 
					$university = '';
					if(isset($_GET['university_class'])){
							$university = $_GET['university_class'];
					}else{
							$university= null;
					}
			?>
			<div class="form-group" style='display:none;'>
				<?= Html::dropDownList('university_class', $university, $universityArray,['selection' => $university_id, 'prompt' => 'University','class' => 'form-control']); ?>
					<?php 
						$major = '';
						if(isset($_GET['major_class'])){
								$major = $_GET['major_class'];
						}else{
								$major= null;
						}
				?>
			</div>
			<div class="form-group">
			<?= Html::dropDownList('major_class', $major, $majorArray,['prompt' => 'Major','class' => 'form-control']); ?>
				<?php 
					$course = '';
					if(isset($_GET['course_class'])){
							$course = $_GET['course_class'];
					}else{
							$course= null;
					}
			?>
			</div>
			<div class="form-group">
			<?= Html::dropDownList('course_class', $course, $courseArray,['prompt' => 'Course','class' => 'form-control']); ?>
				<?php /*
					$class = '';
					if(isset($_GET['class_class'])){
							$class = $_GET['class_class'];
					}else{
							$class= null;
					}
			?>
			<?= Html::dropDownList('class_class', $class, $classesArray,['prompt' => 'Class','class' => 'form-control', 'style' => 'color:white;border:1px solid white;border-radius:4px;height:30px;font-size:12px;']); */ ?>
			</div>
			<div class="form-group">
				<?php 
					$professor = '';
					if(isset($_GET['professor_class'])){
							$professor = $_GET['professor_class'];
					}else{
							$professor = null;
					}
			?>
			<?= Html::dropDownList('professor_class', $professor, $professorArray,['prompt' => 'Professor','class' => 'form-control']); ?>
			</div>
			<input type="submit" name="searchButton" value="Search" class="btn">
		<?php $form = ActiveForm::end();  ?>
	</div>