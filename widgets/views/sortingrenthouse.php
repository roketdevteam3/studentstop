<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\ActiveForm;
?>


<?php $form = ActiveForm::begin(['method' => 'GET', 'options' => ['class' => 'top-filter-form']
	]); ?>
	<div class="filter">
		<label>
			Sorting by: 
		</label>
		<div class="form-group">
			<?php 
			 $university = '';
				if(isset($_GET['university'])){
					$university = $_GET['university'];
				} 
			?>
			<?= Html::dropDownList('university', $university, $universityArray,['prompt' => 'University','class' => 'form-control']); ?>
		</div>
		<div class="form-group">
			<?php $distance = '';?>
			<?= Html::dropDownList('distance', $distance, [],['prompt' => 'Distance','class' => 'form-control']); ?>
		</div>
		<div class="form-group">
			<?php $rating = '';?>
			<?= Html::dropDownList('rating', $rating, [],['prompt' => 'Rating','class' => 'form-control']); ?>
		</div>
		<div class="form-group">
			<?php 
			 $price = '';
				if(isset($_GET['price'])){
					$price = $_GET['price'];
				} 
			?>
			<?= Html::dropDownList('price', $price, ['1' => 'lowerst first', '2' => 'highest first'],['prompt' => 'Price','class' => 'form-control']); ?>
		</div>
		<div class="form-group">
			<input type="submit" name="searchButton" value="Sorting" class="btn">
		</div>
	</div>
<?php $form = ActiveForm::end();  ?>