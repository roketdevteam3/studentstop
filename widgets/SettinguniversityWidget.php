<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\University;
use app\models\user\UserInfo;

class SettinguniversityWidget extends Widget{
    //public $class_id;

    public function init(){
        parent::init();
    }

    public function run(){
        $modelUserinfo = UserInfo::find()->where(['id_user' => \Yii::$app->user->id])->one();
        $modelUniversity = University::find()->where(['id' => $modelUserinfo->university_id])->one();
        if($modelUniversity->id_owner == \Yii::$app->user->id){
            return '<li>
                        <a href="../../../../university/settings/'.$modelUniversity->slug.'" style="width:100%;">
                            Setting
                        </a>
                    </li>';        
        }else{
            return '';        
        }
        
    }
    
}
?>
