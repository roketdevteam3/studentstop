<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\posts\models\Userrents;
use app\modules\university\models\University;

class FavoriterentsWidget extends Widget{
	public $rent_id;

	public function init(){
		parent::init();
	}

	public function run(){
		$modelFavoriterents = Userrents::find()->where(['user_id' => \Yii::$app->user->id, 'rent_id' => $this->rent_id])->one();
		if($modelFavoriterents != null){
			$result = '<button class="fav deleteWithFavorite" data-rent_id="'.$this->rent_id.'"><span class="text">Delete with favorite</span><i class="fa fa-star pull-right"></i></button>';
		}else{
			$result = '<button class="fav addToFavorite" data-rent_id="'.$this->rent_id.'"><span class="text">Add to favorite</span><i class="fa fa-star pull-right"></i></button>';
		}
		return $result;
	}
}
?>
