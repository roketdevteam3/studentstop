<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Major;
use app\models\Course;
use app\models\Classes;
use app\models\Tutors;
use app\models\user\User;
use app\modules\university\models\University;

class FilterconferenceWidget extends Widget{
    //public $message;

    public function init(){
        parent::init();
    }

    public function run(){
        $universityArray = ArrayHelper::map(University::find()->all(), 'id', 'name');
        
        $majorArray = [];
        if(isset($_GET['university_conference'])){
            if($_GET['university_conference'] != ''){
                $majorArray = ArrayHelper::map(Major::find()->where(['university_id' => $_GET['university_conference']])->all(), 'id', 'name');
            }
        }
        
        $courseArray = [];
        if(isset($_GET['major_conference'])){
            if($_GET['major_conference'] != ''){
                $courseArray = ArrayHelper::map(Course::find()->where(['id_major' => $_GET['major_conference']])->all(), 'id', 'name');
            }
        }
        
//        $classesArray = [];
//        if(isset($_GET['course_class'])){
//            if($_GET['course_class'] != ''){
//                $classesArray = ArrayHelper::map(Classes::find()->where(['id_course' => $_GET['course_class']])->all(), 'id', 'name');
//            }
//        }
        
        $tutorsArray = [];
        if(isset($_GET['course_conference'])){
            if($_GET['course_conference'] != ''){
                $tutorsModels = Tutors::find()->where(['course_id' => $_GET['course_conference']])->asArray()->all();
                    $tutors_id = [];
                    foreach($tutorsModels as $tutors){
                        $tutors_id[] = $tutors['user_id'];
                    }
                $tutorsArray = ArrayHelper::map(User::find()->where(['id' => $tutors_id])->all(), 'id', 'name');
            }
        }
        return $this->render('filterconference',[
            'tutorsArray' => $tutorsArray,
            'universityArray' => $universityArray,
            'majorArray' => $majorArray,
            'courseArray' => $courseArray,
            //'classesArray' => $classesArray,
        ]);
    }
}
?>
