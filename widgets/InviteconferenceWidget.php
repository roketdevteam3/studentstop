<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Videoconferenceuser;

class InviteconferenceWidget extends Widget{
	public $conference_id;
	public $user_id;

	public function init(){
		parent::init();
	}

	public function run(){
		$modeVideoconferenceUser = Videoconferenceuser::find()->where(['conference_id' => $this->conference_id, 'user_id' => $this->user_id])->one();
		
		if($modeVideoconferenceUser == null){
			return '<a class="btn btn-invite InviteConference" data-conference_id="'.$this->conference_id.'" data-user_id="'.$this->user_id.'"<span class="joinText">Invite</span></a>';
		}else{
			return '';
		}
	}
}
?>
