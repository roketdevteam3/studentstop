<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\jobs\models\Company;
use app\modules\jobs\models\Jobsfunction;
use app\modules\jobs\models\Jobsindustry;
use app\modules\university\models\University;

class FilterjobsWidget extends Widget{
    public $message;
    public $role;

    public function init(){
        parent::init();
    }

    public function run(){
        $companyArray = ArrayHelper::map(Company::find()->all(), 'id', 'name');
        $universityArray = ArrayHelper::map(University::find()->all(), 'id', 'name');
        $jobfunctionArray = ArrayHelper::map(Jobsfunction::find()->all(), 'id', 'name');
        $industryArray = ArrayHelper::map(Jobsindustry::find()->all(), 'id', 'name');
        
        return $this->render('filterjobs',[
            'companyArray' => $companyArray,
            'universityArray' => $universityArray,
            'jobfunctionArray' => $jobfunctionArray,
            'industryArray' => $industryArray,
            'role'=>$this->role,
        ]);
    }
}
?>
