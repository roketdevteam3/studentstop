<?php
namespace app\widgets\rank;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\posts\models\Rentreserve;
use kartik\rating\StarRating;

class RankrentsWidget extends Widget{
    public $rent_id;
    public $type;

    public function init(){
        parent::init();
    }

    public function run(){
        $modelRentreserve = Rentreserve::find()->where(['not', ['rating_count' => 0]])->andWhere(['rent_id' => $this->rent_id, 'rent_type' => $this->type])->all();

        $countUser = 0;
        $countStar = 0;
        foreach($modelRentreserve as $rent){
            $countUser++;
            $countStar = $countStar + $rent->rating_count;
        }
        if($countUser != 0){
            $countStarResult = $countStar/$countUser;
        }else{
            $countStarResult = 0;
        }

        if (5<$countStarResult*2){
            return 'Very good '.round($countStarResult*2 ,1);
        } else {
            return 'Not good '.round($countStarResult*2 ,1);
        }

    }

}
?>
