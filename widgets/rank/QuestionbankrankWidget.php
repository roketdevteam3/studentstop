<?php
namespace app\widgets\rank;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\posts\models\Rentreserve;
use app\models\Ratingrequest;
use kartik\rating\StarRating;

class QuestionbankrankWidget extends Widget{
    public $id;

    public function init(){
        parent::init();
    }

    public function run(){
        $modelRatingrequest = Ratingrequest::find()->where(['to_object_id' => $this->id, 'type' => 'question_bank'])->all();

        $countUser = 0;
        $countStar = 0;
        foreach($modelRatingrequest as $ratingrequest){
            $countUser++;
            $countStar = $countStar + $ratingrequest->rating_count;
        }
        if($countUser != 0){
            $countStarResult = $countStar/$countUser;
        }else{
            $countStarResult = 0;
        }

        return round($countStarResult,1);
    }

}
?>
