<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Follower;
use app\models\Videoconferenceuser;


class VideolistmembersWidget extends Widget{
    public $class_id;

    public function init(){
        parent::init();
    }

    public function run(){
        $membersCount = Videoconferenceuser::find()->where(['conference_id' => $this->class_id, 'status' => '1'])->count();
        
        return $membersCount;        
    }
    
}
?>
