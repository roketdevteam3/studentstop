<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Classes;
use app\models\Follower;
use app\modules\posts\models\Posts;

class MarketcategorycountWidget extends Widget{
    public $category_id;

    public function init(){
        parent::init();
    }

    public function run(){
        $postsCount = Posts::find()->where(['category_id' => $this->category_id])->count();
        
        return $postsCount;        
    }
    
}
?>
