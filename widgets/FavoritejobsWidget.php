<?php
namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\modules\jobs\models\Company;
use app\modules\jobs\models\Favoritejobs;
use app\modules\university\models\University;

class FavoritejobsWidget extends Widget{
	public $jobs_id;

	public function init(){
		parent::init();
	}

	public function run(){
		$modelFavoriteJobs = Favoritejobs::find()->where(['user_id' => \Yii::$app->user->id, 'jobs_id' => $this->jobs_id])->one();
		if($modelFavoriteJobs != null){
			$result = '<button class="fav deleteWithFavorite" data-job_id="'.$this->jobs_id.'"><span class="text">Delete with favorite</span><i class="fa fa-star pull-right"></i></button>';
		}else{
			$result = '<button class="fav addToFavorite" data-job_id="'.$this->jobs_id.'"><span class="text">Add to favorite</span><i class="fa fa-star pull-right"></i></button>';
		}
		return $result;
	}
}
?>
