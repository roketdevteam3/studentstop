<?php
namespace app\widgets;

use app\modules\jobs\models\FavoriteRezume;
use yii\base\Widget;

class FavoriterezumeWidget extends Widget{
	public $rezume_id;
	public $job_id;

	public function init(){
		parent::init();
	}

	public function run(){
		$modelFavoriteRezume = FavoriteRezume::find()->where(['user_id' => \Yii::$app->user->id,'rezume_id'=>$this->rezume_id])->one();
		if($modelFavoriteRezume != null){
			$result = '<button class="fav deleteWithFavoriteRezume" data-rezume_id="'.$this->rezume_id.'" data-job_id="'.$this->job_id.'"><span class="text">Delete with favorite</span><i class="fa fa-star pull-right" ></i></button>';
		}else{
			$result = '<button class="fav addToFavoriteRezume " data-rezume_id="'.$this->rezume_id.'" data-job_id="'.$this->job_id.'"><span class="text">Add to favorite</span><i class="fa fa-star pull-right " ></i></button>';
		}
		return $result;
	}
}
?>
