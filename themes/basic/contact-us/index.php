<?php
/**
 * Created by PhpStorm.
 * User: kuzio
 * Date: 17.12.15
 * Time: 18:25
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>


<div class="container">
    <div class="row">
    <div class="contact-us">
        <p class="page-title">Зв’язатися з нами:</p>
        <div class="divider"></div>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <?php $form = ActiveForm::begin(['id' => 'contact-form', 'action' => Url::to('/contact-us/contact-us', true)]); ?>

                    <?= $form->field($contactForm, 'name',[
                        'template' => "<div class=\"form-group clearfix\">
                        <div class=\"col-md-4\"><label for=\"telnum\">{label} *:</label></div>
                        <div class=\"col-md-8\">{input}</div>
                    </div>{error}",
                    ]) ?>

                    <?= $form->field($contactForm, 'phone',[
                        'template' => "<div class=\"form-group clearfix\">
                        <div class=\"col-md-4\"><label for=\"telnum\">{label} *:</label></div>
                        <div class=\"col-md-8\">{input}</div>
                    </div>{error}",
                    ]) ?>

                    <?= $form->field($contactForm, 'email',[
                        'template' => "<div class=\"form-group clearfix\">
                        <div class=\"col-md-4\"><label for=\"telnum\">{label} *:</label></div>
                        <div class=\"col-md-8\">{input}</div>
                    </div>{error}",
                    ]) ?>

                    <?= $form->field($contactForm, 'text')->textArea(['rows' => 6, 'placeholder' => Yii::t('app', 'Your message ...'),])->label(false) ?>

                    <div class="text-right">
                        <input type="submit" value="<?= Yii::t('app', 'Send') ?>" class="btn btn-success">
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>



