<?php
/**
 * Created by PhpStorm.
 * User: kuzio
 * Date: 17.12.15
 * Time: 18:25
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

?>


<div class="home_background">
    <div class="content_home">
        <h1 class="title wow bounceInDown" data-wow-offset="200" data-wow-delay="0.5s" data-wow-duration="2s">the
            studentstop.</h1>
    </div>
    <div class="wrapper">
        <div class="sub-wrapper">
            <div class="row">
                <div class="col-md-6 wow bounceInLeft" data-wow-offset="200" data-wow-delay="0.5s"
                     data-wow-duration="2s">
                    <?php $form = ActiveForm::begin(['fieldConfig' => [
                        'template' => "{input}{error}",
                        'options' => [
                            'tag' => 'span'
                        ]
                    ],
                        'action' => '/auth/confirm-signup/'
                    ]); ?>

                    <div class="left-m" style="border-top: 2px solid #31B3FF;">
                        <h4>01 FIRST NAME</h4>
                    </div>
                    <div class="right-m" style="border-top: 2px solid #31B3FF;">
                        <?= $form->field($model, 'name')->textInput([
                            'type' => 'text',
                            'class' => 'form-control style-imp',
                        ]) ?>
                    </div>

                    <div class="left-m" style="border-top: 2px solid #31B3FF;">
                        <h4>02 LAST NAME</h4>
                    </div>
                    <div class="right-m" style="border-top: 2px solid #31B3FF;">
                        <?= $form->field($model, 'surname')->textInput([
                            'type' => 'text',
                            'class' => 'form-control style-imp',
                        ]) ?>
                    </div>

                    <div class="left-m" style="border-top: 2px solid #31B3FF;">
                        <h4>03 EMAIL</h4>
                    </div>
                    <div class="right-m" style="border-top: 2px solid #31B3FF;">
                        <?= $form->field($model, 'email')->textInput([
                            'type' => 'text',
                            'class' => 'form-control style-imp',
                        ]) ?>
                    </div>

                    <div class="left-m" style="border-top: 2px solid #31B3FF;">
                        <h4>04 PASSWORD</h4>
                    </div>
                    <div class="right-m" style="border-top: 2px solid #31B3FF;">
                        <?= $form->field($model, 'password')->textInput([
                            'type' => 'password',
                            'class' => 'form-control style-imp',
                        ]) ?>
                    </div>

                </div>
                <div class="col-md-6 wow bounceInRight" data-wow-offset="200" data-wow-delay="0.5s"
                     data-wow-duration="2s">
                    <div class="left-m" style="border-top: 2px solid #31B3FF;">
                        <h4>05 NETWORK</h4>
                    </div>
                    <div class="right-m" style="border-top: 2px solid #31B3FF;">
                        <?= $form->field($model_info, 'network')->dropDownList(ArrayHelper::map($model_university->find()->asArray()->all(),
                            'id', 'name'), [
                            'prompt' => Yii::t('administration', 'Select ...'),
                            'class' => 'form-control style-imp'
                        ])->label(false) ?>

                    </div>

                    <div class="left-m" style="border-top: 2px solid #31B3FF;">
                        <h4>06 STATUS</h4>
                    </div>
                    <div class="right-m" style="border-top: 2px solid #31B3FF;">
                        <?= $form->field($model_info, 'academic_status')->dropDownList(ArrayHelper::map($model_user_role->find()->asArray()->all(),
                            'id', 'name'), [
                            'prompt' => Yii::t('administration', 'Select ...'),
                            'class' => 'form-control style-imp'
                        ])->label(false) ?>
                    </div>

                    <div class="left-m" style="border-top: 2px solid #31B3FF;">
                        <h4>07 CLASS</h4>
                    </div>
                    <div class="right-m" style="border-top: 2px solid #31B3FF;">
                        <?= $form->field($model_info, 'class')->textInput([
                            'type' => 'text',
                            'class' => 'form-control style-imp',
                        ]) ?>
                    </div>
                    <div class="text-right">
                        <input type="submit" value="Sign Up" class="btn btn-info sign">
                    </div>
                    <?php ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </div>

</div>