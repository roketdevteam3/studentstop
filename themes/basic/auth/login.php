<?php
/**
 * Created by PhpStorm.
 * User: kuzio
 * Date: 17.12.15
 * Time: 18:25
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>

<div class="home_background">
    <div class="content_home">
        <h1 class="title wow bounceInDown" data-wow-offset="200" data-wow-delay="0.5s" data-wow-duration="2s">the studentstop.</h1>
    </div>
    <div class="wrapper">
        <div class="sub-wrapper">
            <div class="row">
                <div class="col-md-6 wow bounceInLeft" data-wow-offset="200" data-wow-delay="0.5s" data-wow-duration="2s">
                    <?php $form = ActiveForm::begin(['fieldConfig' => [
                        'template' => "{input}{error}",
                        'options' => [
                            'tag'=>'span'
                        ]
                    ]]); ?>
                    <h3 style="text-align: center; color: #ccc;">SIGN IN</h3>
                    <div class="col-md-5 left-m" style="border-top: 2px solid #31B3FF;">
                        <h4 class="font4">USERNAME</h4>
                    </div>
                    <div class="col-md-7 right-m" style="border-top: 2px solid #31B3FF;">
                        <?= $form->field($model, 'email')->textInput([
                            'type' => 'email',
                            'class' => 'form-control style-imp',
                            'id' => 'InputEmail1',
                            'placeholder' => 'Email'
                        ]) ?>
                    </div>

                    <div class="col-md-5 left-m" style="border-top: 2px solid #31B3FF;">
                        <h4 class="font4">PASSWORD</h4>
                    </div>
                    <div class="col-md-7 right-m" style="border-top: 2px solid #31B3FF;">
                        <?= $form->field($model, 'password')->textInput([
                            'type' => 'password',
                            'class' => 'form-control style-imp',
                            'id' => 'InputPass',
                            'placeholder' => 'Password'
                        ]) ?>
                    </div>

                    <div class="text-right">
                        <input type="submit" value="Sign In" class="btn btn-info sign">
                    </div>

                    <?php ActiveForm::end() ?>
                    <div class="social">
                        <a href="#"><i class="fa fa-facebook-official soc"></i></a>
                        <a href="#"><i class="fa fa-twitter-square soc"></i></a>
                        <a href="#"><i class="fa fa-google-plus-square soc"></i></a>
                        <a href="#"><i class="fa fa-linkedin-square soc"></i></a>
                    </div>
                </div>
                <div class="col-md-6 wow bounceInRight" data-wow-offset="200" data-wow-delay="0.5s" data-wow-duration="2s">
                    <?php $form = ActiveForm::begin([
                        'fieldConfig' => [
                            'template' => "{input}{error}",
                            'options' => [
                                'tag' => 'span'
                            ]
                        ],
                        'action' => '/users/first-step-signup'
                    ]); ?>
                    <h3 style="text-align: center;">SIGN UP</h3>
                    <h4 class="font4">Are you not a member? Join <a href="#">The Student Stop</a> and register to begin your road to academic success</h4>
                    <div class="col-md-5 left-m" style="border-top: 2px solid #31B3FF;">
                        <h4 class="font4">FIRST NAME</h4>
                    </div>
                    <div class="col-md-7 right-m" style="border-top: 2px solid #31B3FF;">
                        <?= $form->field($model_sign_up, 'name')->textInput([
                            'type' => 'text',
                            'class' => 'form-control style-imp',
                            'id' => 'FirstName',
                            'placeholder' => 'First name'
                        ]) ?>
                    </div>

                    <div class="col-md-5 left-m" style="border-top: 2px solid #31B3FF;">
                        <h4 class="font4">LAST NAME</h4>
                    </div>
                    <div class="col-md-7 right-m" style="border-top: 2px solid #31B3FF;">
                        <?= $form->field($model_sign_up, 'surname')->textInput([
                            'type' => 'text',
                            'class' => 'form-control style-imp',
                            'id' => 'LastName',
                            'placeholder' => 'Last name'
                        ]) ?>
                    </div>
                    <div class="text-right">
                        <input type="submit" value="Sign Up" class="btn btn-info sign">
                    </div>
                    <?php ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </div>

</div>