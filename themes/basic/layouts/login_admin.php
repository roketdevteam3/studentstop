<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Modal;


AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="robots" content="noindex">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="bg-dark-blue">

<div class="mr-waper-loader">
    <div class="loader-circle-full-screen"></div>
</div>

<div class="bb-alert alert alert-deng" id="msg-left-alert" style="display: none">
    <a href="javascript:" class="fa fa-times" onclick="$(this).parent().hide()"></a>
    <span></span>
</div>

<?php $this->beginBody() ?>
<div class="wrap ">

    <h1 class="text-center"><?= Yii::t('app','Administration panel'); ?></h1>

    <div class="container">
        <?= $content ?>
    </div>
</div>

<footer class="footer">

</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
