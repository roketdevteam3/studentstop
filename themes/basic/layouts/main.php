<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\models\user\UserFriendRequest;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use app\models\MainMenuItem;
use app\widgets\SettinguniversityWidget;


$main_menu_not_logged = MainMenuItem::find()
	->status(MainMenuItem::STATUS_ON)
	->andWhere(['logged' => MainMenuItem::NOT_LOGGED])
	->orderBy(['order_num' => SORT_DESC])
	->all();

$main_menu_logged = MainMenuItem::find()
	->status(MainMenuItem::STATUS_ON)
	->andWhere(['logged' => MainMenuItem::LOGGED])
	->orderBy(['order_num' => SORT_DESC])
	->all();



AppAsset::register($this);
?>
<?php 
if(!\Yii::$app->user->isGuest){
    $university =  \Yii::$app->user->identity->userInfos->university;     
} ?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<meta name="author" content="Roketdev">

		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
		<meta name="apple-touch-fullscreen" content="yes">
		<meta charset="<?= Yii::$app->charset ?>">
		<?= Html::csrfMetaTags() ?>
		<title><?= Html::encode($this->title) ?></title>
		<script type="text/javascript">
			var site_url = '<?= Url::base(true) ?>';
			var user_id = '<?= \Yii::$app->user->getId() ?>';
		</script>
	   <?php $this->head() ?>


<!--        we need change it Roma Skidan-->
		<?php if(\Yii::$app->user->isGuest): ?>
			<link href="../css/landing-page.css" rel="stylesheet">
			<link href="../css/elements.css" rel="stylesheet">
			<script src="../globals/plugins/modernizr/modernizr.min.js"></script>
<!--            <link rel="stylesheet" href="../css/admin1.css">-->
			<link rel="stylesheet" href="../globals/plugins/bootstrap-social/bootstrap-social.css">
			<link rel="stylesheet" href="../css/plugins.css">
		<?php endif ?>
<!--        we need change it Roma Skidan-->

	</head>

	<?php $this->beginBody() ?>

	<?php if(\Yii::$app->user->isGuest): ?>
                <?php // $this->redirect(''); ?>
                <?= $content ?>
	<?php else: ?>
		<body>
		  <div class="nav-bar-container">

			<!-- BEGIN ICONS -->
			<!--<div class="nav-menu" style="padding: 29px 0 17px 6px;width: 40px;">
			  <div class="hamburger" style="">
				<span class="patty"></span>
				<span class="patty"></span>
				<span class="patty"></span>
				<span class="patty"></span>
				<span class="patty"></span>
				<span class="patty"></span>
			  </div><!--.hamburger
			</div>.nav-menu-->
			<div class="hidden nav-search left-menu-tooltip" style="top:102px;padding: 7px 0 17px 0;width: 40px;" data-toggle="tooltip" data-placement="right" title data-original-title="Search">
			  <span class="search"></span>
			</div>

			<div class="hidden left-menu-tooltip" style="position: fixed; left: 0;width: 40px;z-index:4;padding: 7px 0 17px 0;cursor: pointer;color:white !important;top:152px !important;text-align:center;" data-toggle="tooltip" data-placement="right" title data-original-title="Profile">
				<a href="<?= Url::home().'profile/'.\Yii::$app->user->id; ?>" style="padding:0px;">
					<img src="<?= Url::home(); ?>default_img/header_icon/user.png" style="margin:0 auto;width:15px;height:15px;">
				</a>
			</div>
			<div class="hidden left-menu-tooltip" style="position: fixed; z-index: 4;left: 0px;padding: 7px 0 17px 0;width: 40px;cursor: pointer;color:white !important;top:202px !important;text-align:center" data-toggle="tooltip" data-placement="right" title data-original-title="University">
				<a href="<?= Url::home(); ?>university" style="padding:0px;">
					<img src="<?= Url::home(); ?>default_img/header_icon/university_old.png" style="margin:0 auto;width:15px;height:15px;">
				</a>
			</div><!--.nav-search-->

			<div class="hidden left-menu-tooltip" style="position: fixed; z-index: 4;left: -2px;padding: 7px 0 17px 0;width: 40px;cursor: pointer;color:white !important;top:252px !important;text-align:center" data-toggle="tooltip" data-placement="right" title data-original-title="Cource & Class">
				<a href="<?= Url::home(); ?>courseclases" style="padding:0px;">
					<img src="<?= Url::home(); ?>default_img/header_icon/stats.png" style="margin:0 auto;width:15px;height:15px;">
				</a>
			</div><!--.nav-search-->
			<div class="hidden" style="position: fixed; z-index: 4;left: -2px;padding: 7px 0 17px 0;width: 40px;cursor: pointer;color:white !important;top:302px !important;text-align:center">
				<img src="<?= Url::home(); ?>default_img/header_icon/technology.png" style="margin:0 auto;width:15px;height:15px;">
				<!--<span class="fa fa-calendar fa-2x"  style="font-size: 30px;"></span>-->
			</div>

                        <div class="nav-video">
			  <div class="cross">
				<span class="line"></span>
				<span class="line"></span>
			  </div><!--.cross-->
			</div><!--.nav-user-->
			<!-- END OF ICONS -->

			<div class="nav-bar-border" style="background-color:#525273;width:40px !important;display: none;"></div><!--.nav-bar-border-->

			<!--
                        <div class="overlay" style="">
			  <div class="starting-point">
				<span class="" style="transform: scaleX(0) scaleY(0); height: 3011.08px; width: 3011.08px; top: -1505.54px; left: -1505.54px;"></span>
			  </div>
			  <div class="logo"></div>
			</div>

			<div class="overlay-secondary"></div> -->
			<!-- END OF OVERLAY HELPERS -->

		  </div><!--.nav-bar-container-->

	  <div class="content" style="padding:0;">
		<div class="mr-waper-loader">
			<div class="loader-circle-full-screen"></div>
		</div>


		<!-- notification -->

		<?php if(Yii::$app->getSession()->getFlash('error')): ?>
			<div class="mr-notification danger">

				<?php if(is_array(Yii::$app->getSession()->getFlash('error'))): ?>
					<ul>
						<?php foreach (Yii::$app->getSession()->getFlash('error') as $k => $items): ?>
							<?php if (is_array($items)): ?>
								<?php foreach ($items as $msg): ?>
									<li> <?= $msg ?></li>
								<?php endforeach; ?>
							<?php endif ?>
						<?php endforeach; ?>
					</ul>
					<span class="mr-close" data-mr-remove="true" >&times;</span>
				<?php else: ?>
					<h4><?= Yii::$app->getSession()->getFlash('error') ?></h4>
					<span class="mr-close" data-mr-remove="true" >&times;</span>
				<?php endif ?>
			</div>
		<?php endif ?>

		<?php if(Yii::$app->getSession()->getFlash('success')): ?>
			<div class="mr-notification success">
				<h4><?= Yii::$app->getSession()->getFlash('success') ?></h4>
				<span class="mr-close" data-mr-remove="true" >&times;</span>
			</div>
		<?php endif ?>


		<!-- end -->
		<!--
		<div class="page-header full-content">
			<div class="row">
				<div class="col-sm-6">
					<h1>Dashboard <small>Activity Summary</small></h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb">
						<li><a href="#" class="active"><i class="ion-home"></i> Homepage</a></li>
					</ol>
				</div>
			</div>
		</div>-->
		<div class="wrap">
			<nav class="navbar navbar-top">
				<div class="nav-contents">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarCollapse" aria-expanded="false">
							<span class="sr-only">Menu</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a href="<?= Url::home(); ?>" class="navbar-brand">
							<img src="<?= Url::to('default_img/student_logonewcolor.png',true) ?>" class="mr-nav-first-menu-brand">
						</a>
					</div>
					<ul class="nav navbar-nav navbar-right">
						<?php if (!Yii::$app->user->isGuest): ?>
						<li class="nav-notification notification-count">
                                                    <img style="width: 17px;" src="<?= Url::home().'images/icon/Bell-icon.png'?>">
                                                    <span class="notification_v" data-id="<?= \Yii::$app->user->identity->id ?>" ></span>
                                                    <span class="ntfCount">
                                                        <span class="badge">0</span>
                                                    </span>
						</li>
						<li class="nav-user message-count">
                                                    <img src="<?= Url::home().'images/icon/message-icnewcolor.png'?>">
                                                    <span class="user" data-id="<?= \Yii::$app->user->identity->id ?>" >Message</span>
                                                    <div class="msg">
                                                        <span class="badge"></span>
                                                    </div>
						</li>
						<li>
                                                    <div class="avatar-wrap myHeaderAvatar">
                                                        <?php if(\Yii::$app->user->identity->userInfos->avatar != ''){ ?>
                                                          <img style="height: 28px" class="img-responsive" src="/images/users_images/<?= \Yii::$app->user->identity->userInfos->avatar ?>" alt="avatar">
                                                        <?php }else{ ?>
                                                          <img style="height: 28px" class="img-responsive" src="/images/default_avatar.jpg" alt="avatar" >
                                                        <?php } ?>
                                                    </div>
                                                    <a class="user-name" href="">
                                                        <?= \Yii::$app->user->identity->name ?>
                                                        <?= \Yii::$app->user->identity->surname ?>
                                                    </a>
                                                    <!--<span style="display:block;text-align:right;margin-top:-10px;"><?php /*$university->name;*/ ?></span>-->
						</li>
						<li class="dropdown top-profile-link">
                                                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                                        <div class="cross">
                                                              <span class="line"></span>
                                                              <span class="line"></span>
                                                              <span class="line"></span>
                                                        </div>
                                                    </a>
                                                    <ul role="menu" class="dropdown-menu">
                                                            <li>
                                                                <?= HTML::a('Profile', Url::home().'profile/'.\Yii::$app->user->id); ?>
                                                            </li>
                                                            <li>
                                                                <a href="/tutorspage/<?= \Yii::$app->user->id; ?>">Tutor</a>
                                                            </li>
															<?php
																$maney = '0';
																$credits = 0;
																$user_c_m = \app\models\Usercredits::find()->where(['user_id'=>Yii::$app->user->id])->one();
																if ($user_c_m) {
																	$maney = $user_c_m->many;
																	$credits = $user_c_m->credit_count;
																}
															?>
                                                            <li>
                                                                <?= HTML::a('Credits('.$credits.')', Url::home().'profile/'.\Yii::$app->user->id.'/credits'); ?>
                                                            </li>
															<li>
																<?= HTML::a('Money:'.$maney.'$', Url::home().'profile/'.\Yii::$app->user->id.'/maney'); ?>
															</li>
                                                            <li>
                                                                <?= HTML::a('Photos & Video', Url::home().'profile/'.\Yii::$app->user->id.'/photos'); ?>
                                                            </li>
                                                                <?php 
                                                                    $modelActionCredits = app\models\Creditsactioncount::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                                                if($modelActionCredits){
                                                                    $a = (int)$modelActionCredits->credits_count;
                                                                    $b = $a - 1;
                                                                    $c = substr($b, 0, -1);
                                                                    $level = (int)$c + 1;
                                                                ?>
                                                                    <li>
                                                                       <a> Level <?= $level; ?> - <?= $a; ?> Credits </a>
                                                                    </li>
                                                                <?php } ?>
                                                            <li class="divider"></li>
                                                            <li><a  href="<?= Url::to(['/users/default/logout'], true) ?>" data-method="post">Logout</a></li>
                                                    </ul>
						</li>
						<?php else: ?>
						<li>
							<a href="<?= Url::to(['/users/default/login'], true) ?>" class="bug">LOGIN</a>
						</li>
						<li>
							<a href="<?= Url::to(['/users/default/join'], true) ?>" class="bug">JOIN</a>
						<li>
						<?php endif; ?>
					</ul>
					<div class="header_sviat" >
					<ul class="collapse navbar-collapse nav navbar-nav navbar-left menu-top-wrapper">

						<li class="menu-item-wrap"  style="width:auto;">
							<a class="menu-item" style=" padding-right: 5px;">
								<img src="<?= Url::home(); ?>default_img/header_icon/universitynevcollor.png">
								<span><?= $university->name; ?></span>
							</a>
							<ul class="menu-list">
								<li>
									<a href="<?= Url::to('university/view/'.$university->slug,true) ?>">
										<?= \Yii::t('app','My university'); ?>
									</a>
								</li>
								<li>
									<a href="<?= Url::home();?>users">
										Search
									</a>
								</li>
								<li>
									<a href="<?= Url::home();?>courseclases">
										Course & classes
									</a>
								</li>
								<li>
									<a href="<?= Url::home();?>videoclass/majorlist">
										Virtual Class
									</a>
								</li>
								<!--<li>
                                    <a href="#" style="width:100%;">
                                        Find university
                                    </a>
                                </li>-->
								<?= SettinguniversityWidget::widget(); ?>
							</ul>
						</li>

						<li class="menu-item-wrap">
							<a class="menu-item">
								<img src="<?= Url::home(); ?>default_img/header_icon/star_newcolor.png">
								<span>Service</span>
							</a>
							<ul class="menu-list">
								<li>
									<a href="<?= Url::home();?>market">
										Classified market
									</a>
								</li>
								<!--<li>
                                                                    <a href="<?= Url::home();?>question_bank" style="width:100%;">
                                                                        Question bank
                                                                    </a>
                                                                </li>-->
								<li>
									<a href="<?= Url::home();?>rents">
										Borrow
									</a>
								</li>
								<li>
									<a href="<?= Url::home();?>rentshouse">
										Rent a house
									</a>
								</li>
							</ul>
						</li>
						<li class="menu-item-wrap">
							<a class="menu-item">
								<img src="<?= Url::home(); ?>default_img/header_icon/graphicnewcolor.png">
								<span>Business</span>
							</a>
							<ul class="menu-list">
								<li>
									<a href="<?= Url::home();?>dashboard/job">
										Job & recruitment
									</a>
								</li>
								<li>
									<a href="<?= Url::home();?>question_bank" style="width:100%;">
										Question bank
									</a>
								</li>
							</ul>
						</li>
						<li class="menu-item-wrap">
							<a class="menu-item">
								<img src="<?= Url::home(); ?>default_img/header_icon/arrow_trendingnewcolor.png">
								<span>Trending</span>
							</a>
							<ul class="menu-list">
								<li>
									<a href="<?= Url::home(); ?>trending/rating/university" style="width:100%;">
										Ranking
									</a>
								</li>
								<li>
									<a href="<?= Url::home(); ?>fileshistory/allfiles">
										Files & History
									</a>
								</li>
							</ul>
						</li>
					</ul>
						</div>
					<!-- Collection of nav links, forms, and other content for toggling -->
					<div id="navbarCollapse" class="collapse navbar-collapse nav navbar-nav navbar-left menu-top-wrapper">

					</div>
				</div>
			</nav>

			<?php if(false&&\Yii::$app->user->isGuest): ?>
				<nav role="navigation" class="navbar navbar-default mr-nav-second-menu">
					<div class="navbar-header">
						<button type="button" data-target="#navbarCollapse1" data-toggle="collapse" class="navbar-toggle">
							<span class="sr-only">Menu</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>

					<!-- Collection of nav links, forms, and other content for toggling -->
					<div id="navbarCollapse1" class="collapse navbar-collapse">

						<ul class="nav navbar-nav">



							<?php   // Set default url home menu
							if(isset(\Yii::$app->user->identity->userInfos->network)):
								?>
								<?php $university =  \Yii::$app->user->identity->userInfos->university; ?>
								<li>
									<a href="<?= Url::to('university/view/'.$university->slug,true) ?>">
										<?= \Yii::t('app','Home'); ?>
									</a>
								</li>

							<?php endif; ?>
							<?php /* if (isset($main_menu_logged)): ?>
								<?php foreach ($main_menu_logged as $k => $item): ?>
									<?php if($item['link'] == 'profile'){ ?>
										<li>
											<a href="<?php if ($item['absolute'] > 0){ echo Url::home(true).$item['link'].'/'.\Yii::$app->user->id; }else{ echo $item['link'].'/'.\Yii::$app->user->id; } ?>" onclick="<?php if (!empty($item['onclick'])) echo $item['onclick'];  ?>">
												<?= $item['title'] ?>
											</a>
										</li>
									<?php }else{ ?>
										<li>
											<a href="<?php if ($item['absolute'] > 0) echo Url::home(true).$item['link']; else echo $item['link']; ?>" onclick="<?php if (!empty($item['onclick'])) echo $item['onclick'];  ?>">
												<?= $item['title'] ?>
											</a>
										</li>
									<?php } ?>

								<?php endforeach ?>
							<?php endif */ ?>
						</ul>

					</div>
				</nav>
			<?php endif ?>

			<div class="container-fluid background-block" style="position:relative;">
                                <style>
                                    #remotesVideos > video{
                                            height:250px;
                                    }
                                </style>
                                <div style="position:fixed;background-color:white;z-index:9999999999;right:2px;min-height:100vh;overflow: auto;max-width:350px;">
                                    <div class=" call-box" style="height:350px;display:none">
                                            <div class="col-xs-12">
                                                <video height="250" id="localVideo"></video>
                                            </div>
                                            <div class="col-xs-12">
                                                <div id="remotesVideos" style="height:250px;"></div>
                                            </div>
                                        <div style="clear: both"></div>
                                    </div>
                                    <div style="clear: both"></div>
                                    
                                    <div class="incoming-call-controll" style="display:none">
                                        <div class="sender">
                                        </div>
                                        
                                        <button type="button" class="answer" name="button" data-type="video" style="width:40px;height:40px;border:0px;background-image: url(../../../images/skype_icon/videocall.png);background-color:transparent;background-repeat: no-repeat;background-size:100%;"></button>
                                        <button type="button" class="answer" name="button" data-type="audio" style="width:40px;height:40px;border:0px;background-image: url(../../../images/skype_icon/call.png);background-color:transparent;background-repeat: no-repeat;background-size:100%;"></button>
                                        <button type="button" class="decline" name="button"  style="width:40px;height:40px;border:0px;background-image: url(../../../images/skype_icon/decline.png);background-color:transparent;background-repeat: no-repeat;background-size:100%;"></button>
                                    </div>
                                    <div class="call-controll" style="display:none;text-align:center">
                                            <button type="button" class="mute-video" name="button"  style="width:40px;height:40px;border:0px;background-image: url(../../../images/skype_icon/videocall_1.png);background-color:transparent;background-repeat: no-repeat;background-size:100%;"></button>
                                            <button type="button" class="unmute-video" name="button"  style="width:40px;height:40px;border:0px;background-image: url(../../../images/skype_icon/videocall.png);background-color:transparent;background-repeat: no-repeat;background-size:100%;" ></button>
                                            <button type="button" class="mute-sound" name="button"  style="width:40px;height:40px;border:0px;background-image: url(../../../images/skype_icon/microphone_of.png);background-color:transparent;background-repeat: no-repeat;background-size:100%;"></button>
                                            <button type="button" class="unmute-sound"  style="width:40px;height:40px;border:0px;background-image: url(../../../images/skype_icon/microphone_onn.png);background-color:transparent;background-repeat: no-repeat;background-size:100%;" name="button"></button>
                                            <button type="button" class="cancel-call" name="button" data-type="audio"  style="width:40px;height:40px;border:0px;background-image: url(../../../images/skype_icon/decline.png);background-color:transparent;background-repeat: no-repeat;background-size:100%;"></button>
                                    </div>
                                </div>
				<?= $content ?>
			</div>
		</div>
<!--
		<div class="base-footer" style="min-height: 60px;height:auto;background: #3e50b4">
			<p class="copy">© 2016 StudentStop</p>

			<div class="block-f" style="width:70%;">
				<a href="#">Home</a>
				<a href="#">Forum</a>
				<a href="#">Blog</a>
				<a href="#">About</a>
				<a href="#">Invite friends</a>
			</div>
		</div>-->
		</div>


		<div class="layer-container">
		  <!-- BEGIN MENU LAYER -->
		  <div class="menu-layer">
			<ul>

				<?php if(isset(\Yii::$app->user->identity->userInfos->network)){ ?>
					<?php $university =  \Yii::$app->user->identity->userInfos->university; ?>
					<?php
					$mystring = Yii::$app->request->url;
					$findme   = 'homepage';
					$pos = strpos($mystring, $findme);
					if($pos){ $class = 'open'; }else{ $class = ''; } ?>

					<li class="<?= $class; ?>">
						<a href="<?= Url::to('users/homepage',true) ?>">
							<?= \Yii::t('app','Home'); ?>
						</a>
						<span class="hover-bg"></span>
					</li>

				<?php } ?>

				<?php if (isset($main_menu_logged)){ ?>
					<?php foreach ($main_menu_logged as $k => $item): ?>
					<?php
						$mystring = Yii::$app->request->url;
						$findme   = $item['link'];
						$pos = strpos($mystring, $findme);
						if($pos){ $class = 'open'; }else{ $class = ''; } ?>
					  <?php if($item['link'] == 'profile'){ ?>
						  <li class="<?= $class; ?>">
							  <a href="<?php if ($item['absolute'] > 0){ echo Url::home(true).$item['link'].'/'.\Yii::$app->user->id; }else{ echo $item['link'].'/'.\Yii::$app->user->id; } ?>" onclick="<?php if (!empty($item['onclick'])) echo $item['onclick'];  ?>">
								  <?= $item['title'] ?>
							  </a>
							  <span class="hover-bg"></span>
						  </li>
					  <?php }elseif($item['link'] == 'users'){ ?>
							<?php if(Yii::$app->request->url == 'users'){ $class = 'open'; }else{ $class = ''; } ?>
						  <li class="<?= $class; ?>">
							  <a href="<?php if ($item['absolute'] > 0) echo Url::home(true).$item['link']; else echo $item['link']; ?>" onclick="<?php if (!empty($item['onclick'])) echo $item['onclick'];  ?>">
								  <?= $item['title'] ?>
							  </a>
							  <span class="hover-bg"></span>
						  </li>
					  <?php }else{ ?>
						  <li class="<?= $class; ?>">
							  <a href="<?php if ($item['absolute'] > 0) echo Url::home(true).$item['link']; else echo $item['link']; ?>" onclick="<?php if (!empty($item['onclick'])) echo $item['onclick'];  ?>">
								  <?= $item['title'] ?>
							  </a>
							  <span class="hover-bg"></span>
						  </li>
					  <?php } ?>

					<?php endforeach ?>
				<?php } ?>

			</ul>
		  </div><!--.menu-layer-->
		  <!-- END OF MENU LAYER -->
		  <div class="search-layer">
			  <div class="search">
				<form action="#">
				  <div class="form-group">
					<input type="text" id="input-search" class="form-control" placeholder="type something">
					<button type="submit" class="btn btn-default disabled btn-ripple"><i class="ion-search"></i></button>
				  </div>
				</form>
			  </div><!--.search-->

			  <div class="results">
				<div class="row">

				</div><!--.row-->
			  </div><!--.results-->
			</div><!--.search-layer-->
			<!-- END OF SEARCH LAYER -->
			<!-- END OF v LAYER -->
		<?php if(!Yii::$app->user->isGuest){?>
			<span class="my_user_id" style="display:none;"><?= \Yii::$app->user->id; ?></span>
                        <div class="modalRightPages" style="display:none;">
                        </div>
            <div>
                        <div class="modalNotificationRightBlock" style="display:none;">
                            <select style="    text-align-last: center; width:90%;float:left; height: 35px; color: #8b8b8b;" class="changeNotificationModal form-control">
                                <option value="all">All</option>
                                <option value="university">University</option>
                                <option value="course_and_class">Course & Classes</option>
                                <option value="virtual_class">Virtual classes</option>
                                <option value="rents">Barrow</option>
                                <option value="rentshouse">Rent a house</option>
                                <option value="market">Market</option>
                                <option value="question_bank">Question Bank</option>
                                <option value="all">Job & Recruitment</option>
                            </select>

                            <div style="width:10%;float:left;text-align:center;">
                                <i class="glyphicon glyphicon-refresh refreshNotification" style="cursor:pointer;"></i>
                            </div>
							<div class="row" style=" border-bottom: 2px solid #7364cc;">
							</div>
                            <div class="modalNotificationBody">
                                <table class="table notificationTable" style="font-size: 12px; color: #8b8b8b;"></table>
                                <div style="text-align: center;">
                                    <a class="show_more_ntf btn btn-primary" style="width:100%;cursor:pointer;text-align:center;display:none;">More</a>
                                </div>
                            </div>
                        </div>

			<div class="user-layer" style="display:none;">
				<ul class="nav nav-tabs nav-justified" role="tablist">
					<!--<li class="active"><a href="#messages" class="msg" data-toggle="tab">Messages <span class="badge"></span></a></li>
				<li><a href="#notifications"  class="ntf" data-toggle="tab">Notifications <span class="badge"></span></a></li>-->
				</ul>
				<div class="row no-gutters tab-content">
                    <div class="tab-pane fade in active" id="messages">
                        <div class="correspondence-user-col" style="width:100%;">
							<!--<div class="input-group search-top-group">
							    <input type="text" class="form-control" name="" placeholder="Search...">
							    <span class="input-group-btn">
							        <button class="btn btn-search" type="button">Search</button>
							    </span>
							</div> -->
                            <div class="message-list-overlay"></div>
                            <li class="has-action-left has-action-right template" >
                                    <a href="#" class="visible msg-id" data-message-id="1">
                                            <div class="list-action-left">
                                                    <img  src="/1.jpg" class="face-radius img-responsive" alt="">
                                            </div>
                                            <div class="list-content">
                                                    <span class="badge"></span>
                                                    <span class="title">Pari Subramanium</span>
                                                    <div class="list-action-right">
                                                            <span class="top">15 min</span>
                                                            <!-- <span class="bottom"><i class="ion-android-done"></i></span>
                                                            <i class="ion-android-done bottom"></i> -->
                                                    </div>
                                                    <span class="caption"></span>
                                            </div>
                                    </a>
                            </li>
                            <ul class="list-material message-list" id="conactList"></ul>
                        </div>
                        <!--.col-->
                        <div class="message-send-col" style="padding:0px;display:none;">
                            <div class="user-info">
                                <div class="user-avatar-wrap">
                                    <img class="user-avatar resipienrUserAvatar img-responsive">
                                </div>
                                <h5 class="user-username"></h5>
                                <div class="chat-action-wrap">
                                    <span class="chat-action-btn call" data-id="">
                                        <img  src="<?= Url::home(); ?>images/icon/call-ic.png">
                                        Call
                                    </span>
                                    <span class="chat-action-btn profileSrc">
                                        <img src="<?= Url::home(); ?>images/icon/profile-ic-chat.png">
                                        <?= HTML::a('Profile','#'); ?>
                                    </span>
                                    <button class="chat-action-btn allMassage" style="outline: none; border: 0;background-color: transparent;">
                                        <img src="<?= Url::home(); ?>images/icon/massage-chat-ic.png">
                                        All massage
                                    </button>
                                    <span class="chat-action-btn messageMapButton" data-user_id="">
                                        <img src="<?= Url::home(); ?>images/icon/map-chat-ic.png">
                                        Map
                                    </span>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="massegeMap" class="massegeMap" style="height:500px;display:none;"></div>
                            <div class="message-send-container">
                                    <div class="messages"></div>
                                    <!--.messages-->
                                    <div class="send-message">
                                            <div class="inputer-wrap">
                                                    <div class="inputer inputer-blue">
                                                            <div class="input-wrapper">
                                                                    <textarea rows="2" id="send-message-input" class="form-control js-auto-size" placeholder="Message"></textarea>
                                                            </div>
                                                    </div>
                                                    <!--.inputer-->
                                                    <span class="input-group-btn">
                                                            <button id="send-message-button" class="btn btn-ripple" type="button" style="">Send</button>
                                                    </span>
                                            </div>
                                    </div>
                            </div>
                        </div>
                    </div>
					<!--.tab-pane #messages-->
				<div class="tab-pane fade" id="notifications">

				  <div class="col-md-6 col-md-offset-3">

					<li class="has-action-left has-action-right has-long-story templateN">
					  <a href="#" class="hidden">
						<div class="col-sm-12" style="width:100%">
						  <div class="col-xs-6 deleteN">
							<i class="ion-android-delete pull-right" style=""></i>
						  </div>
						  <div class="col-xs-6 applyN">
							<i class="fa fa-check pull-left"></i>
						  </div>
						</div>
					  </a>
					  <a href="#" class="visible">
						<div class="list-action-left">
						  <img src="" class="face-radius" alt="">
						  <i class="type fa text-indigo" style="position: absolute;top: 0;right: 0;"></i>
						  <!-- <i class="ion-bag icon "></i> -->
						</div>
						<div class="list-content">
						  <span class="title caption"></span>
						  <hr>
						  <span class="caption textD">Collaboratively administrate empowered markets via plug-and-play networks. Dynamically procrastinate B2C users after installed base benefits.</span>
						</div>
						<div class="list-action-right">
						  <span class="top"></span>
						  <!-- <i class="ion-record text-green bottom"></i> -->
						</div>
					  </a>
					</li>
					<ul class="list-material has-hidden">
					</ul>

				  </div>
				  <!--.col-->
				</div>
				<!--.tab-pane #notifications-->


				<!--.tab-pane #settings-->

			  </div>
			  <!--.row-->
			</div>
			<!--.user-layer-->
			<!-- END OF USER LAYER -->

			<?php } ?>
		  </div>

		</body>
	<?php endif ?>
	<?php $this->endBody() ?>
	<?php if(\Yii::$app->user->isGuest): ?>

		<!-- Global Vendors for Pleasure Theme -->
		<script src="/js/global-vendors.js"></script>

		<!-- ScrollMonitor plugin for navigation tracker -->
		<script src="/globals/plugins/scrollMonitor/scrollMonitor.js"></script>

		<!-- Skrollr plugin for parallax elements -->
		<script src="/globals/plugins/skrollr/skrollr.min.js"></script>

		<!-- Typer plugin for retyping text in the hero section -->
		<script src="/globals/plugins/typer/jquery.typer.min.js"></script>

		<!-- Pleasure.js for all necessary functions -->
		<script src="/js/pleasure.js"></script>

		<!-- One page parallax functions and events -->
		<script src="/js/one-page-parallax.js"></script>
	<?php endif ?>
</html>
<?php $this->endPage() ?>
