<?php
use yii\helpers\Url;
?>

<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">

    <li >
        <a href="<?= Url::to(['/administration/dashboard/'],true) ?>">
            <i class="fa fa-dashboard"></i> <span><?= Yii::t('app','Dashboard'); ?></span>
        </a></li>
    <?php if (
        \Yii::$app->user->can('administration/university/index') ||
        \Yii::$app->user->can('administration/university/create')
    )
        : ?>
        <li class=" treeview">
            <a href="#">
                <i class="fa fa-bars"></i> <span><?= Yii::t('app','University'); ?></span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <?php if (\Yii::$app->user->can('administration/university/index')): ?>
                    <li class="active"><a href="<?= Url::to(['/administration/university/index'],true) ?>"><i class="fa fa-list"></i> <?= Yii::t('app','Item list'); ?></a></li>
                <?php endif; ?>
                <?php if (\Yii::$app->user->can('administration/university/create')): ?>
                    <li class="active"><a href="<?= Url::to(['/administration/university/create'],true) ?>"><i class="fa fa-plus"></i> <?= Yii::t('app','Create item'); ?></a></li>
                <?php endif; ?>
            </ul>
        </li>
    <?php endif; ?>



    <?php if (
        \Yii::$app->user->can('administration/page/index') ||
        \Yii::$app->user->can('administration/page/create')
    )
        : ?>
        <li class=" treeview">
            <a href="#">
                <i class="fa fa-bars"></i> <span><?= Yii::t('app','Page'); ?></span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <?php if (\Yii::$app->user->can('administration/page/index')): ?>
                    <li class="active"><a href="<?= Url::to(['/administration/page/index'],true) ?>"><i class="fa fa-list"></i> <?= Yii::t('app','Item list'); ?></a></li>
                <?php endif; ?>
                <?php if (\Yii::$app->user->can('administration/page/create')): ?>
                    <li class="active"><a href="<?= Url::to(['/administration/page/create'],true) ?>"><i class="fa fa-plus"></i> <?= Yii::t('app','Create item'); ?></a></li>
                <?php endif; ?>
            </ul>
        </li>
    <?php endif; ?>


    <?php if (
        \Yii::$app->user->can('administration/main-menu-item/index') ||
        \Yii::$app->user->can('administration/main-menu-item/create')
    )
        : ?>
        <li class=" treeview">
            <a href="#">
                <i class="fa fa-bars"></i> <span><?= Yii::t('app','Main menu'); ?></span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <?php if (\Yii::$app->user->can('administration/main-menu-item/index')): ?>
                    <li class="active"><a href="<?= Url::to(['/administration/main-menu-item/index'],true) ?>"><i class="fa fa-list"></i> <?= Yii::t('app','Item list'); ?></a></li>
                <?php endif; ?>
                <?php if (\Yii::$app->user->can('administration/main-menu-item/create')): ?>
                    <li class="active"><a href="<?= Url::to(['/administration/main-menu-item/create'],true) ?>"><i class="fa fa-plus"></i> <?= Yii::t('app','Create item'); ?></a></li>
                <?php endif; ?>
            </ul>
        </li>
    <?php endif; ?>

    <?php if (
        \Yii::$app->user->can('administration/news/index') ||
        \Yii::$app->user->can('administration/news/create')
    )
        : ?>
        <li class=" treeview">
            <a href="#">
                <i class="fa fa-newspaper-o"></i> <span><?= Yii::t('app','News'); ?></span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <?php if (\Yii::$app->user->can('administration/news/index')): ?>
                    <li class="active"><a href="<?= Url::to(['/administration/news/index'],true) ?>"><i class="fa fa-list"></i> <?= Yii::t('app','Item list'); ?></a></li>
                <?php endif; ?>
                <?php if (\Yii::$app->user->can('administration/news/create')): ?>
                    <li class="active"><a href="<?= Url::to(['/administration/news/create'],true) ?>"><i class="fa fa-plus"></i> <?= Yii::t('app','Create item'); ?></a></li>
                <?php endif; ?>
            </ul>
        </li>
    <?php endif; ?>

    <!-- Users -->
    <?php if (\Yii::$app->user->can('administration/user/index')): ?>
        <li class=" treeview">
            <a href="<?= Url::to(['/administration/user/index'],true) ?>">
                <i class="fa fa-users "></i> <span><?= Yii::t('app','Users'); ?></span>
            </a>

        </li>
    <?php  endif;  ?>
    <?php  if (\Yii::$app->user->can('administration/category/index')): ?>
        <li class=" treeview">
            <a href="<?= Url::to(['/administration/category/index'],true) ?>">
                <i class="fa fa-tasks"></i> <span><?= Yii::t('app','Category'); ?></span>
            </a>
        </li>
    <?php  endif;  ?>
    <?php if (\Yii::$app->user->can('administration/major/index')): ?>
        <li class=" treeview">
            <a href="<?= Url::to(['/administration/major/index'],true) ?>">
                <i class="fa fa-tasks"></i> <span><?= Yii::t('app','Majors'); ?></span>
            </a>
        </li>
    <?php endif;  ?>
    <?php if (\Yii::$app->user->can('administration/course/index')): ?>
        <li class=" treeview">
            <a href="<?= Url::to(['/administration/course/index'],true) ?>">
                <i class="fa fa-tasks"></i> <span><?= Yii::t('app','Courses'); ?></span>
            </a>
        </li>
    <?php endif;  ?>
        
        
        <li class=" treeview">
            <a href="#">
                <i class="fa fa-bars"></i> <span><?= Yii::t('app','Jobs'); ?></span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <?php if (\Yii::$app->user->can('administration/jobs/createindustry')): ?>
                    <li>
                        <a href="<?= Url::to(['/administration/jobs/createindustry'],true) ?>">
                            <i class="fa fa-tasks"></i> <span><?= Yii::t('app','Jobs industry'); ?></span>
                        </a>
                    </li>
                <?php endif;  ?>    
            </ul>
        </li>
        
        
    <?php if (\Yii::$app->user->can('administration/classes/index')): ?>
        <li class=" treeview">
            <a href="<?= Url::to(['/administration/classes/index'],true) ?>">
                <i class="fa fa-tasks"></i> <span><?= Yii::t('app','Classes'); ?></span>
            </a>
        </li>
    <?php endif;  ?>
    <?php if (\Yii::$app->user->can('administration/question/index')): ?>
        <li class=" treeview">
            <a href="<?= Url::to(['/administration/question/index'],true) ?>">
                <i class="fa fa-tasks"></i> <span><?= Yii::t('app','Question category'); ?></span>
            </a>
        </li>
    <?php endif;  ?>
    <?php if (\Yii::$app->user->can('administration/settingadmin/index')): ?>
        <li class=" treeview">
            <a href="<?= Url::to(['/administration/settingadmin/index'],true) ?>">
                <i class="fa fa-tasks"></i> <span><?= Yii::t('app','Setting'); ?></span>
            </a>
        </li>
    <?php endif;  ?>

    <?php if (\Yii::$app->user->can('administration/maney/index')): ?>
        <li class=" treeview">
            <a href="<?= Url::to(['/administration/maney/index'],true) ?>">
                <i class="fa fa-tasks"></i> <span><?= Yii::t('app','Maney pay'); ?></span>
            </a>
        </li>
    <?php endif;  ?>

  <?php if (\Yii::$app->user->can('administration/company/index')): ?>
      <li class=" treeview">
          <a href="<?= Url::to(['/administration/company/index'],true) ?>">
              <i class="fa fa-tasks"></i> <span><?= Yii::t('app','Companies users'); ?></span>
          </a>
      </li>
  <?php endif;  ?>

    <li class="header"><?= Yii::t('app','Settings'); ?></li>

    <!-- Permissions & Roles -->
    <?php if (\Yii::$app->user->can('administration/accesses/assignment/index') ||
        \Yii::$app->user->can('administration/accesses/role/index') ||
        \Yii::$app->user->can('administration/accesses/permission/index') ||
        \Yii::$app->user->can('administration/accesses/rule/index') )
        : ?>
        <li class=" treeview">
            <a href="#">
                <i class="fa fa-unlock-alt "></i> <span><?= Yii::t('app','Permissions and Roles'); ?></span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <?php if (\Yii::$app->user->can('administration/accesses/assignment/index')): ?>
                    <li class="active"><a href="<?= Url::to(['/administration/accesses/assignment'],true) ?>"><i class="fa fa-circle-o"></i> <?= Yii::t('app','Assignment'); ?></a></li>
                <?php endif; ?>
                <?php if (\Yii::$app->user->can('administration/accesses/role/index')): ?>
                    <li class="active"><a href="<?= Url::to(['/administration/accesses/role'],true) ?>"><i class="fa fa-circle-o"></i> <?= Yii::t('app','Role'); ?></a></li>
                <?php endif; ?>
                <?php if (\Yii::$app->user->can('administration/accesses/permission/index')): ?>
                    <li class="active"><a href="<?= Url::to(['/administration/accesses/permission'],true) ?>"><i class="fa fa-circle-o"></i> <?= Yii::t('app','Permission'); ?></a></li>
                <?php endif; ?>
                <?php if (\Yii::$app->user->can('administration/accesses/rule/index')): ?>
                    <li class="active"><a href="<?= Url::to(['/administration/accesses/rule'],true) ?>"><i class="fa fa-circle-o"></i> <?= Yii::t('app','Rule'); ?></a></li>
                <?php endif; ?>
            </ul>
        </li>
    <?php endif; ?>



    <!-- Permissions & Roles -->
    <?php if (\Yii::$app->user->can('administration/setting/index')): ?>
        <li class=" treeview">
            <a href="<?= Url::to(['/administration/setting/index'],true) ?>">
                <i class="fa fa-globe "></i> <span><?= Yii::t('app','Site settings'); ?></span>
            </a>

        </li>
    <?php  endif;  ?>




</ul>