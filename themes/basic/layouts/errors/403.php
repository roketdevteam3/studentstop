<?php
error_reporting(E_ERROR);
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 22.10.15
 * Time: 16:46
 */
 /* @var $this yii\web\View */
/* @var $content string */

 app\assets\Error403Asset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Yii::t('error','Access dined! You dont have premiss!') ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="container">
    <div class="row">
        <div class="site-error">
            <div class="text-center">
                <img src="../default_img/error-icon.png" alt="error">

                <h1 ><?= Yii::t('error','Error 403!'); ?></h1>
                <h2 ><?= Yii::t('error','Access Denied/Forbidden!'); ?></h2>
                <h4><?= Yii::t('error','Forbidden: You don`t have permission to access [directory] on this server.'); ?></h4>
                <h4><?= Yii::t('error','Please sign in the appropriate rights, or leave this page!'); ?></h4>
            </div>
        </div>
    </div>
</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>