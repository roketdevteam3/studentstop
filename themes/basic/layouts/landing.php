<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\MainMenuItem;


$main_menu_not_logged = MainMenuItem::find()
    ->status(MainMenuItem::STATUS_ON)
    ->andWhere(['logged' => MainMenuItem::NOT_LOGGED])
    ->orderBy(['order_num' => SORT_DESC])
    ->all();

$main_menu_logged = MainMenuItem::find()
    ->status(MainMenuItem::STATUS_ON)
    ->andWhere(['logged' => MainMenuItem::LOGGED])
    ->orderBy(['order_num' => SORT_DESC])
    ->all();



\app\assets\LandingAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="author" content="Roketdev">

        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta charset="<?= Yii::$app->charset ?>">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <script type="text/javascript">
            var site_url = '<?= Url::base(true) ?>';
            var user_id = '<?= \Yii::$app->user->getId() ?>';
        </script>
       <?php $this->head() ?>



    </head>

    <?php $this->beginBody() ?>


        <body>
        <div class="mr-waper-loader">
            <div class="loader-circle-full-screen"></div>
        </div>


        <!-- notification -->

        <?php if(Yii::$app->getSession()->getFlash('error')): ?>
            <div class="mr-notification danger">

                <?php if(is_array(Yii::$app->getSession()->getFlash('error'))): ?>
                    <ul>
                        <?php foreach (Yii::$app->getSession()->getFlash('error') as $k => $items): ?>
                            <?php if (is_array($items)): ?>
                                <?php foreach ($items as $msg): ?>
                                    <li> <?= $msg ?></li>
                                <?php endforeach; ?>
                            <?php endif ?>
                        <?php endforeach; ?>
                    </ul>
                    <span class="mr-close" data-mr-remove="true" >&times;</span>
                <?php else: ?>
                    <h4><?= Yii::$app->getSession()->getFlash('error') ?></h4>
                    <span class="mr-close" data-mr-remove="true" >&times;</span>
                <?php endif ?>
            </div>
        <?php endif ?>

        <?php if(Yii::$app->getSession()->getFlash('success')): ?>
            <div class="mr-notification success">
                <h4><?= Yii::$app->getSession()->getFlash('success') ?></h4>
                <span class="mr-close" data-mr-remove="true" >&times;</span>
            </div>
        <?php endif ?>


        <!-- end -->


        <div class="header" style="height:44px !important;">
            <div class="container-fluid">
                <a href="../">
                    <div class="logo landing-logo" style="padding:6px !important;">
                        <img src="<?= Url::to('/default_img/student_logo.png',true) ?>">
                    </div><!--.logo-->
                </a>
                <!--
                * data-nav-link attribute is necessary for data-anchor links.
                * data-anchor="slide1" attribute is useful when user clicks the data-nav-link, the window moves the related slide.
                -->
                <div class="animated-selector">
                    <ul class="navigation">

                        <?php if(\Yii::$app->user->isGuest): ?>
                        <?php if (isset($main_menu_not_logged)): ?>
                                <?php foreach ($main_menu_not_logged as $k => $item): ?>

                                    <li data-anchor="slide<?= $k ?>" data-nav-link>
                                        <a href="<?php if ($item['absolute'] > 0) echo Url::home(true).$item['link']; else echo $item['link']; ?>" onclick="<?php if (!empty($item['onclick'])) echo $item['onclick'];  ?>">
                                            <?= $item['title'] ?>
                                        </a>
                                    </li>

                                <?php endforeach ?>
                        <?php endif ?>
                        <?php endif ?>

                    </ul>
                    <div class="selector"></div>
                </div><!--.animated-selector-->

                <!--<div class="btn-action">
                    <a href="<?= Url::to(['/users/default/login'], true) ?>" class="btn btn-indigo">JOIN or LOGIN</a>-->
                <!--               <a href="--><?//= Url::to(['/users/default/join'], true) ?><!--" class="btn btn-indigo">LOGIN</a>-->
                <!--</div><!--.btn-action-->

                <div class="hamburger-btn"></div><!--.hamburger-btn-->

            </div><!--.container-->
        </div><!--.header-->


        <div class="wrap">
                <?= $content ?>
        </div>

<!--        <div class="base-footer" >-->
<!--            <p class="copy">© 2016 StudentStop</p>-->
<!---->
<!--            <div class="block-f">-->
<!--                <a href="#">Home</a>-->
<!--                <a href="#">Forum</a>-->
<!--                <a href="#">Blog</a>-->
<!--                <a href="#">About</a>-->
<!--                <a href="#">Invite friends</a>-->
<!--            </div>-->
<!--        </div>-->

        </body>
    <?php $this->endBody() ?>

</html>
<?php $this->endPage() ?>
