<?php
/* @var $this \yii\web\View */
/* @var $content string */


use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AdminLTEAsset;
AdminLTEAsset::register($this);
AdminLTEAsset::overrideSystemConfirm();
?>


<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" >
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">



<div class="mr-waper-loader">
    <div class="loader-circle-full-screen"></div>
</div>

<div class="bb-alert alert alert-deng" id="msg-left-alert">
    <a href="javascript:" class="fa fa-times" onclick="$(this).parent().hide()"></a>
    <span></span>
</div>

<?php $this->beginBody() ?>


<div class="wrapper">

<header class="main-header">
<!-- Logo -->
<a href="/" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini">Studentstop</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg">Studentstop</span>
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation">
<!-- Sidebar toggle button-->
<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
    <span class="sr-only">Toggle navigation</span>
</a>



<div class="navbar-custom-menu">

<ul class="nav navbar-nav navbar-right">


<!-- User Account: style can be found in dropdown.less -->
<li class=" user user-menu">
    <a href="<?= Url::to('/administration/logout',true); ?>" class="mr-logout-btn">
        <span class="hidden-xs">
            <i class="fa fa-power-off"></i>
            <?= Yii::t('admin','Logout') ?>
        </span>
    </a>

</li>



</ul>


</div>
</nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="lang-panel">

        </div>

        <?= $this->render('admin_column_left') ?>


    </section>
    <!-- /.sidebar -->
</aside>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= Yii::t('app',Yii::$app->controller->id); ?>
            <small><?= Yii::t('app',Yii::$app->controller->action->id); ?></small>
        </h1>

        <?=
        Breadcrumbs::widget([
            'homeLink' => [
                'template' => "<li><b> <i class='fa fa-dashboard'></i> {link}</b></li>\n",
                'label'    => Yii::t('app', 'Home'),
                'url'      => Yii::$app->homeUrl . 'administration/',
            ],
            'links'    => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],

        ]);
        ?>

    </section>

    <div class="content body">
        <?= $content ?>
        </div>


    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">

    </div>
    <strong>Copyright &copy; 2014-<?= date('Y') ?> <a href="<?= Url::base(true) ?>">Student Stop</a>.</strong> All rights
    reserved.
</footer>


<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
