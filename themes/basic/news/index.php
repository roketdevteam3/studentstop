<?php
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\data\Pagination;

?>

<div class="container">
    <div class="row">
        <div class="news">
            <div class="row">
                <div class="col-md-8">
                    <?php if ($dataProvider->count > 0) : ?>
                        <?php foreach ($dataProvider->models as $item) : ?>
                            <div class="news-list">
                                <div class="news-list-item">
                                    <a href="<?= Url::to("/news/" . $item['slug'], true) ?>" class="page-title">
                                        <?= $item->title ?>
                                    </a>

                                    <div class="divider"></div>
                                    <p class="news-list-item--info">
                                        <?= $item['date_create'] ?><span>|</span> <?= $item->authorInfo['name'] ?>
                                    </p>

                                    <?php if($item['image']): ?>
                                        <div class="news-list-item--image">
                                            <img src="<?= $item['image'] ?>" alt="" class="img-responsive">
                                        </div>
                                    <?php endif; ?>

                                    <p class="news-list-item--descr">
                                        <?= $item->short_desc ?>
                                    </p>

                                    <div class="news-list-item--more">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 text-right">
                                                <a href="<?= Url::to("/news/" . $item['slug'], true) ?>"
                                                   class="btn btn-success">
                                                    Детальніше
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif ?>
                </div>
                <div class="col-md-4 lb">
                    <div class="page-title">НАЙПОПУЛЯРНІШІ НОВИНИ</div>
                    <div class="divider"></div>
                    <div class="popular-news">
                        <?php if (count($model_popular) > 0) : ?>
                            <?php foreach ($model_popular as $item) : ?>
                                <div class="popular-news-item">
                                    <a href="<?= Url::to("/news/" . $item['slug'], true) ?>"
                                       class="popular-news-item--heading">
                                        <?= $item['title'] ?>
                                    </a>
                                    <span class="popular-news-item--date"><?= $item['date_create'] ?></span>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="news-pager">
                        <div class="col-md-12 mr-paginator">
                            <?= yii\widgets\LinkPager::widget([
                                'pagination' => $dataProvider->pagination
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>