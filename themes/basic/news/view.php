<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 11.11.15
 * Time: 10:31
 */

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

\Yii::$app->view->registerMetaTag([
    'name' => 'description',
    'content' => $model->meta_description,
    'id' => "mr_description,"
], "mr_description,"); //this will now replace the default one.

\Yii::$app->view->registerMetaTag([
    'name' => 'keywords',
    'content' => $model->meta_keywords,
    'id' => "mr_keyword"
], "mr_keyword"); //this will now replace the default one.

$this->title = $model->meta_title;

?>

<div class="container">
    <div class="row">
        <div class="news-view">
            <div class="row">
                <div class="col-md-8">
                    <div class="article">
                        <div class="article-heading">
                            <p class="page-title">
                                <?= $model->title ?>
                            </p>

                            <div class="divider"></div>
                            <div class="share-section clearfix">
                                <span>Поділитися:</span>
                                <ul>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                </ul>
                            </div>
                            <p class="article-date"><?= $model['date_create'] ?> <span> | </span> <?= $model['authorInfo']['name'] ?></p>
                        </div>
                        <div class="article-body">

                            <?= $model->short_desc ?>

                            <?php if ($model['image']): ?>
                                <div class="article-body--image">
                                    <img src="<?= $model['image'] ?>" alt="" class="img-responsive">
                                </div>
                            <?php endif; ?>

                            <?= $model->content ?>
                        </div>
                        <div class="article-footer">
                            <div class="share-section clearfix">
                                <span>Поділитися:</span>
                                <ul>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                    <li><a href="#"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="comment-section clearfix">
                        <?php $form = ActiveForm::begin([
                            'id' => 'comments',
                            'action' => Url::to('/comments/create/', true),
                            'enableAjaxValidation' => true,
                            'enableClientValidation' => true,
                            'validateOnSubmit' => true,
                        ]); ?>

                        <?= $form->field($model_comments, 'text')->textarea(['rows' => 6, 'placeholder' => 'Введіть ваш коментар']) ?>

                        <?= $form->field($model_comments, 'type')->hiddenInput(['value' => 'news'])->label(false) ?>

                        <?= $form->field($model_comments, 'id_place')->hiddenInput(['value' => $model['id']])->label(false) ?>

                        <?= $form->field($model_comments, 'reCaptcha')->widget(
                            \himiklab\yii2\recaptcha\ReCaptcha::className(), [
                            'name' => 'reCaptcha',
                            'siteKey' => '6LfRVhUTAAAAALgqmezNyy4YFLwX8Umyxqdpy5BA',
                            'widgetOptions' => ['class' => 'recaptcha-login']
                        ]
                        )->label('reCaptcha') ?>

                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('app', 'Send') , ['class' => 'btn btn-success']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                    <div class="comment-wall">
                        <p class="page-title">Коментарі: </p>
                        <div class="divider"></div>
                        <?php if (count($model_comments_list) > 0): ?>
                            <?php foreach ($model_comments_list as $comment): ?>
                                <div class="comment-wall-item">
                                    <p class="comment-wall-item__date"><?= $comment['date_create'] ?></p>

                                    <div class="comment-wall-item__content">
                                        <?= $comment['text'] ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endif ?>
                    </div>
                </div>
                <div class="col-md-4 lb">
                    <div class="page-title">НАЙПОПУЛЯРНІШІ НОВИНИ</div>
                    <div class="divider"></div>
                    <div class="popular-news">
                        <?php if (count($model_popular) > 0) : ?>
                            <?php foreach ($model_popular as $item) : ?>
                                <div class="popular-news-item">
                                    <a href="<?= Url::to("/news/" . $item['slug'], true) ?>"
                                       class="popular-news-item--heading">
                                        <?= $item['title'] ?>
                                    </a>
                                    <span class="popular-news-item--date"><?= $item['date_create'] ?></span>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
