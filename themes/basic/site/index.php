<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;
?>

<body class="one-page">

<!--
* To change the hover link color and active selector border color,
you can combine the header div with header-*(material-colors).
For ex: <div class="header header-red"> or <div class="header header-teal">
-->
<div class="header">
    <div class="container">

        <div class="logo">
            <a href="../">
                <img src="<?= Url::to('/default_img/one-page-parallax/img/logo.jpg',true) ?>">
            </a>

        </div><!--.logo-->

        <!--
        * data-nav-link attribute is necessary for data-anchor links.
        * data-anchor="slide1" attribute is useful when user clicks the data-nav-link, the window moves the related slide.
        -->
        <div class="animated-selector">
            <ul class="navigation">
                <li data-anchor="slide1" data-nav-link>BENEFITS</li>
                <li data-anchor="slide2" data-nav-link>WE OFFER</li>
                <li data-anchor="slide3" data-nav-link>OVERVIEW</li>
                <li data-anchor="slide4" data-nav-link>TEAM</li>
                <li data-anchor="slide5" data-nav-link>CONTACT</li>
            </ul>
            <div class="selector"></div>
        </div><!--.animated-selector-->

        <div class="btn-action">
            <a href="<?= Url::to(['/users/default/login'], true) ?>" class="btn btn-indigo">JOIN or LOGIN</a>
            <!--            <a href="--><?//= Url::to(['/users/default/join'], true) ?><!--" class="btn btn-indigo">LOGIN</a>-->
        </div><!--.btn-action-->

        <div class="hamburger-btn"></div><!--.hamburger-btn-->

    </div><!--.container-->
</div><!--.header-->

<!--
* Each slide divs can be combined with these classes

** Material colors: such as bg-pink, bg-indigo, bg-teal etc.
** bg-image: you just need to write inline css style in order to path to image.
** bg-image-with-shadow: same for bg-image class but in this time background-gradient floats the image.
** bg-pattern: same for the bg-image but in this time the pattern will be repeated for background.

* data-nav="remove" attribute hides the active navigation border when this slide in viewport.
-->



<div class="slide bg-image" data-nav="remove" style="background-image: url('/default_img/one-page-parallax/img/bg-hero.jpg');">

    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <h1 class="text-indigo text-light-shadow">YOU ARE <span data-typer-targets="STUDENT, PROFESSOR, TEACHER">STUDENT</span>?<br>STUDENTSTOP CREATE FOR YOU</h1>
                <p class="caption text-light-shadow">You want to put the social in learning. The StudentStop gets your teachers and students connected.</p>
            </div><!--.col-->
        </div><!--.row-->

        <div class="row">
            <div class="col-md-12">
                <ul class="store-list">
                    <li><a href="javascript:;"><img src="/default_img/one-page-parallax/img/on-appstore.png" alt=""></a></li>
                    <li><a href="javascript:;"><img src="/default_img/one-page-parallax/img/on-googleplay.png" alt=""></a></li>
                </ul>
            </div><!--.col-->
        </div><!--.row-->

    </div><!--.container-->
    <a class="btn btn-large btn-floating btn-indigo btn-next btn-next-center" data-anchor="slide1" data-nav-link><i class="ion-chevron-down"></i></a>

</div><!--.slide-hero-->

<!--
* data-nav="slide1" attribute is an identifier for data-nav-links.
-->
<div class="slide" data-nav="slide1">

    <div class="container">

        <div class="slide-image-horizontal hide-on-mobile" data-bottom-top="left:-200px" data-center="left:50px"><img src="/default_img/one-page-parallax/img/bg-features.jpg" alt=""></div><!--.slide-image-->

        <div class="row">
            <div class="col-md-6 col-md-offset-6">
                <h2 class="text-black">BENEFITS</h2>
                <p class="caption">
                    Join 88 of the 100 largest school districts in the U.S. already creating safe, collaborative learning environments on the StudentStop. When you activate a school or district account, it's easy to:</p>
                <ul class="list-vertical">
                    <li>
                        <div class="list-icon text-indigo">
                            <i class="ion-locked"></i>
                        </div><!--.list-icon-->
                        <div class="list-info">
                            <h4 class="text-indigo">Secure Network</h4>
                            <span>Bring an entire school or district under one secure network</span>
                        </div><!--.list-info-->
                    </li>
                    <li>
                        <div class="list-icon text-green">
                            <i class="ion-earth"></i>
                        </div><!--.list-icon-->
                        <div class="list-info">
                            <h4 class="text-green">Learning Communities</h4>
                            <span>Build Professional Learning Communities</span>
                        </div><!--.list-info-->
                    </li>
                    <li>
                        <div class="list-icon text-red">
                            <i class="ion-arrow-graph-up-right"></i>
                        </div><!--.list-icon-->
                        <div class="list-info">
                            <h4 class="text-red">Professional Growth</h4>
                            <span>Support professional growth and build teacher capacity</span>
                        </div><!--.list-info-->
                    </li>
                </ul>
            </div><!--.col-->
        </div><!--.row-->

        <a class="btn btn-large btn-floating btn-pink btn-next btn-next-left" data-anchor="slide2" data-nav-link><i class="ion-chevron-down"></i></a>

    </div><!--.container-->

</div><!--.slide-->

<div class="slide bg-light-grey" data-nav="slide2">

    <div class="container">

        <div class="slide-image-vertical hide-on-mobile" data-bottom-top="top:-200px" data-center-bottom="top:100px"><img src="/default_img/one-page-parallax/img/bg-plans.jpg" alt=""></div><!--.slide-image-->

        <div class="row">
            <div class="col-md-6">
                <h2 class="text-black">WE OFFER</h2>
                <ul class="list-vertical">
                    <li>
                        <div class="list-icon text-deep-purple">
                            <i class="ion-images"></i>
                        </div><!--.list-icon-->
                        <div class="list-info">
                            <h4 class="text-deep-purple">Lorem ipsum</h4>
                            <span>Connect with students, Professors and alumni</span>
                        </div><!--.list-info-->
                    </li>
                    <li>
                        <div class="list-icon text-teal">
                            <i class="ion-calendar"></i>
                        </div><!--.list-icon-->
                        <div class="list-info">
                            <h4 class="text-teal">Lorem ipsum</h4>
                            <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
                        </div><!--.list-info-->
                    </li>
                    <li>
                        <div class="list-icon text-amber">
                            <i class="ion-happy-outline"></i>
                        </div><!--.list-icon-->
                        <div class="list-info">
                            <h4 class="text-amber">Lorem ipsum</h4>
                            <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
                        </div><!--.list-info-->
                    </li>
                    <li>
                        <div class="list-icon text-pink">
                            <i class="ion-happy-outline"></i>
                        </div><!--.list-icon-->
                        <div class="list-info">
                            <h4 class="text-pink">Lorem ipsum</h4>
                            <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
                        </div><!--.list-info-->
                    </li>
                </ul>
            </div><!--.col-->
        </div><!--.row-->

        <a class="btn btn-large btn-floating btn-amber btn-next btn-next-right" data-anchor="slide3" data-nav-link><i class="ion-chevron-down"></i></a>

    </div><!--.container-->
</div><!--.slide-->

<div class="slide bg-image-with-shadow" style="background-image: url('/default_img/one-page-parallax/img/bg-overview.jpg'); " data-nav="slide3">
    <div class="container">
        <h3 class="text-white text-center">OVERVIEW</h3>
        <p class="caption text-white text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

        <ul class="row list-horizontal white">
            <li class="col-sm-4" data-bottom-top="top:-50px" data-center="top:0px">
                <div class="list-icon">
                    <i class="ion-radio-waves"></i>
                </div><!--.list-icon-->
                <div class="list-info">
                    <h4>LOREM IPSUM</h4>
                    <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
                </div><!--.list-info-->
            </li>
            <li class="col-sm-4" data-bottom-top="top:-50px" data-center="top:0px">
                <div class="list-icon">
                    <i class="ion-search"></i>
                </div><!--.list-icon-->
                <div class="list-info">
                    <h4>LOREM IPSUM</h4>
                    <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
                </div><!--.list-info-->
            </li>
            <li class="col-sm-4" data-bottom-top="top:-50px" data-center="top:0px">
                <div class="list-icon">
                    <i class="ion-map"></i>
                </div><!--.list-icon-->
                <div class="list-info">
                    <h4>LOREM IPSUM</h4>
                    <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
                </div><!--.list-info-->
            </li>
        </ul>

        <a class="btn btn-large btn-floating btn-teal btn-next btn-next-center" data-anchor="slide4" data-nav-link><i class="ion-chevron-down"></i></a>

    </div><!--.container-->
</div><!--.slide-->

<div class="slide bg-pattern" style="background-image: url('/default_img/globals/img/patterns/school.png');" data-nav="slide4">
    <div class="container">
        <h3 class="text-black text-center">TEAM</h3>
        <p class="caption text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

        <div class="row transform-bug">
            <div class="col-md-3 col-sm-4 col-xs-12" data-bottom-top="bottom:-100px" data-center="bottom:0px">
                <div class="card card-user-new">
                    <img src="/default_img/globals/img/faces/large/1.jpg" class="member-image" alt="">
                    <div class="card-body">
                        <h5>Kathryn Hoffman</h5>
                        <h6>Web Developer</h6>
                        <ul class="social-links list-inline">
                            <li><a href="#"><i class="fa fa-github"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div><!--.card-body-->
                </div><!--.card-->
            </div><!--.col-md-3-->

            <div class="col-md-3 col-sm-4 col-xs-12" data-bottom-top="bottom:-200px" data-center="bottom:0px">
                <div class="card card-user-new">
                    <img src="/default_img/globals/img/faces/large/6.jpg" class="member-image" alt="">
                    <div class="card-body">
                        <h5>James Ramos</h5>
                        <h6>Communications Liason</h6>
                        <ul class="social-links list-inline">
                            <li><a href="#"><i class="fa fa-github"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div><!--.card-body-->
                </div><!--.card-->
            </div><!--.col-md-3-->

            <div class="col-md-3 col-sm-4 col-xs-12" data-bottom-top="bottom:-200px" data-center="bottom:0px">
                <div class="card card-user-new">
                    <img src="/default_img/globals/img/faces/large/7.jpg" class="member-image" alt="">
                    <div class="card-body">
                        <h5>William Romero</h5>
                        <h6>Division Agent</h6>
                        <ul class="social-links list-inline">
                            <li><a href="#"><i class="fa fa-github"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div><!--.card-body-->
                </div><!--.card-->
            </div><!--.col-md-3-->

            <div class="col-md-3 col-sm-4 col-xs-12" data-bottom-top="bottom:-100px" data-center="bottom:0px">
                <div class="card card-user-new">
                    <img src="/default_img/globals/img/faces/large/4.jpg" class="member-image" alt="">
                    <div class="card-body">
                        <h5>Justin Bowman</h5>
                        <h6>Marketing Representative</h6>
                        <ul class="social-links list-inline">
                            <li><a href="#"><i class="fa fa-github"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div><!--.card-body-->
                </div><!--.card-->
            </div><!--.col-md-3-->

        </div><!--.row-->

    </div><!--.container-->

</div><!--.slide-->

<div class="slide" data-nav="slide5">

    <div class="container">
        <h3 class="text-black text-center">CONTACT</h3>
        <p class="caption text-center">We will be glad if you write us.</p>

        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="form-group">
                    <div class="inputer inputer-teal floating-label">
                        <div class="input-wrapper">
                            <input type="text" name="" class="form-control">
                            <label>First name</label>
                        </div><!--.input-wrapper-->
                    </div><!--.inputer-->
                </div><!--.form-group-->
            </div><!--.col-->
            <div class="col-md-6 col-xs-12">
                <div class="form-group">
                    <div class="inputer inputer-teal floating-label">
                        <div class="input-wrapper">
                            <input type="text" name="" class="form-control">
                            <label>Last name</label>
                        </div><!--.input-wrapper-->
                    </div><!--.inputer-->
                </div><!--.form-group-->
            </div><!--.col-->
        </div><!--.row-->

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="inputer inputer-teal floating-label">
                        <div class="input-wrapper">
                            <textarea class="form-control js-auto-size" rows="1"></textarea>
                            <label>Your message</label>
                        </div>
                    </div>
                </div><!--.form-group-->
            </div><!--.col-->
        </div><!--.row-->

        <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-teal pull-right">SEND</button>
            </div><!--.col-->
        </div><!--.row-->

    </div><!--.container-->

</div><!--.slide-->

<div class="footer bg-black">
    <div class="container">

        <ul class="social-list">
            <li><a href="javascript:;" class="facebook"><i class="ion-social-facebook"></i></a></li>
            <li><a href="javascript:;" class="twitter"><i class="ion-social-twitter"></i></a></li>
            <li><a href="javascript:;" class="pinterest"><i class="ion-social-github"></i></a></li>
        </ul>

        <div class="copyright v-text">© 2016 StudentStop</div>

    </div><!--.container-->
</div><!--.footer-->

</body>