<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="container">
    <div class="row">
        <div class="site-error">
            <div class="text-center">
                <img src="../default_img/error-icon.png" alt="error">

                <p class="site-error-type"><?= Html::encode($this->title) ?></p>
                <p class="site-error-descr"><?= nl2br(Html::encode($message)) ?></p>
            </div>
        </div>
    </div>
</div>
