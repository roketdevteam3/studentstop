<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 */

use yii\helpers\Url;

\Yii::$app->view->registerMetaTag([
    'name' => 'description',
    'content' => $model->meta_d,
    'id'=>"mr_description,"
],"mr_description,"); //this will now replace the default one.

\Yii::$app->view->registerMetaTag([
    'name' => 'keywords',
    'content' => $model->meta_k,
    'id'=>"mr_keyword"
],"mr_keyword"); //this will now replace the default one.

$this->title = $model->meta_t;
?>

<?= $model->content ?>

<small style="position: relative; bottom: 0">
    ssss
</small>
