<?php

namespace app\controllers;

use app\modules\university\models\University;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\States;
use app\modules\users\models\UserInfo;
error_reporting( E_ERROR );
class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        if(!\Yii::$app->user->isGuest){
                $user_info = UserInfo::find()->where(['id_user' => (int)\Yii::$app->user->id])->one();
                 if($user_info){
                    $university = University::findOne($user_info->university_id);
                    if($user_info->academic_status != 8){
                        if($university != null){
                            return $this->redirect(Url::to("/university/view/".$university->slug,true));
                        }else{
                            return $this->redirect(Url::to("/users/choiceuniversity",true));
                        }
                    }else{
                        return $this->redirect(Url::to(Url::home()."create_business",true));
                    }
                }else{
                    return $this->redirect(Url::to("/users/choiceuniversity",true));                    
                }
        }else{
            return $this->redirect('/users/login');
        }
    }

        /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelUniversity($id)
    {
        if (($model = University::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetstates()
    {
        $statesArray = ArrayHelper::map(States::find()->where(['country_id' => $_POST['country_id']])->all(), 'id', 'name');
        echo json_encode($statesArray);
    }
    
}
