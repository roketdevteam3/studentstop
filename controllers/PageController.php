<?php
/**
 * Created by PhpStorm.
 * User: minace
 * Date: 2/24/16
 * Time: 10:12 PM
 */


namespace app\controllers;

use app\models\Lang;
use app\models\Pages;
use Yii;
use yii\web\Controller;
use yii\web\BadRequestHttpException;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

class PageController extends Controller
{

    public function actionView($slug){
        $model = $this->findModel($slug);
        return $this->render('view',['model'=>$model]);
    }

    /**
     * Finds the Pages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pages::findBySlug($id)->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

