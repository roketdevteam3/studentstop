<?php
/**
 * Created by PhpStorm.
 * User: minace
 * Date: 2/24/16
 * Time: 10:12 PM
 */


namespace app\controllers;


use Yii;
use yii\web\Controller;
use yii\web\BadRequestHttpException;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\helpers\Json;

class MessagerController extends Controller
{

    public function actionIndex(){
       
    if (Yii::$app->request->post()) {

        $name = Yii::$app->request->post('name');
        $message = Yii::$app->request->post('message');

        return Yii::$app->redis->executeCommand('PUBLISH', [
            'channel' => 'notification',
            'message' => Json::encode(
                [
                    'name' => !\Yii::$app->user->isGuest ? \Yii::$app->user->identity->name : 'Guest',
                    'message' => $message,
                    'user_id' => (int)\Yii::$app->user->getId(),
                ]
            )
        ]);

    }
    return $this->render('index');
    }

   public function actionWho(){
        return Yii::$app->redis->executeCommand('PUBLISH', [
            'channel' => 'who_write',
            'data' => Json::encode(['name' => !\Yii::$app->user->isGuest ? \Yii::$app->user->identity->name : 'Guest'])
        ]);
    }

}