<?php

namespace app\controllers;

use app\modules\university\models\UserInfo;
use app\models\UserRole;
use app\modules\university\models\University;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\modules\users\models\LoginForm;
use app\modules\users\models\SignupForm;

class AuthController extends Controller
{

    public $defaultAction = 'login';

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $model_sign_up = new SignupForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('/');
        }
        return $this->render('login', [
            'model' => $model,
            'model_sign_up' => $model_sign_up,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        $model_info = new UserInfo();

        if ($model->load(Yii::$app->request->post())) {
            $model_university = new University();
            $model_user_role = new UserRole();

            return $this->render('confirm_registration', [
                'model' => $model,
                'model_info' => $model_info,
                'model_university' => $model_university,
                'model_user_role' => $model_user_role
            ]);
        }
        return $this->render('registration', [
            'model' => $model,
        ]);
    }

    public function actionConfirmSignup()
    {
        $model = new SignupForm();
        $model_info = new UserInfo();
        $model_university = new University();
        $model_user_role = new UserRole();

        $data = Yii::$app->request->post();
        if ($model->load($data) && $model_info->load($data)) {
            $isValid = $model->validate();
            $isValid = $model_info->validate() && $isValid;
            if ($isValid) {
                $model->phone = 0;
                $user = $model->signup();
                $model_info->id_user = $user->id;
                $model_info->save();

                $model_university->findOne($model_info->network);

                return $this->redirect(["/u/view/".$model_university->slug]);
            }
        }
        return $this->render('confirm_registration', [
            'model' => $model,
            'model_info' => $model_info,
            'model_university' => $model_university,
            'model_user_role' => $model_user_role
        ]);
    }

}
