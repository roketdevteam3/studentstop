<?php
/**
 * Created by PhpStorm.
 * User: minace
 * Date: 2/24/16
 * Time: 10:12 PM
 */


namespace app\controllers;

use app\models\Lang;
use Yii;
use yii\web\Controller;
use yii\web\BadRequestHttpException;
use yii\helpers\Url;

class RedactorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [

            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url' => Url::to('/images/redactor',true),// Directory URL address, where files are stored.
                'path' => '@webroot/images/redactor' // Or absolute path to directory where files are stored.
            ],
            'images-get' => [
                'class' => 'vova07\imperavi\actions\GetAction',
                'url' => Url::to('/images/redactor',true), // Directory URL address, where files are stored.
                'path' => '@webroot/images/redactor', // Or absolute path to directory where files are stored.
                'type' => \vova07\imperavi\actions\GetAction::TYPE_IMAGES,
            ]
        ];
    }

}

