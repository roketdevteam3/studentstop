var app = require('express')(),

    fs = require('fs'),
/*
 key: fs.readFileSync('D:/web_server/OpenServer/domains/studentstop/nodejs/key/localhost.key'),
 cert: fs.readFileSync('D:/web_server/OpenServer/domains/studentstop/nodejs/key/localhost.crt'),
 */

    options = {
        key: fs.readFileSync('/home/student/web/thestudentstop.com/public_html/nodejs/key/localhost.key'),
        cert: fs.readFileSync('/home/student/web/thestudentstop.com/public_html/nodejs/key/localhost.crt'),
        requestCert: false,
        rejectUnauthorized: false
    },
    server = require('https').Server(options,app),

    //server = require('http').Server(app),
    io = require('socket.io')(server),
    mysql = require('mysql'),
    dbconfig = require("./db/config"),
    unique = require('array-unique'),
    async = require("async"),
    filaments = {},
    online = {};

cMysql = mysql.createPool({
    database: dbconfig.database,
    host: dbconfig.host,
    user: dbconfig.user,
    password: dbconfig.password,
    connectionLimit: dbconfig.connectionLimit
});

server.listen(2700, function () {
    console.info('listening on *:2700');
});

saveMessage = function (data) {

    async.waterfall([
        function(callback) {
            cMysql.getConnection(function (err, connection) {
                if (err) {
                    console.log("MYSQL: can't get connection from pool:", err);
                } else {
                    var post = {
                        user_id: data.user_id,
                        class_id: data.class_id,
                        message: mysql.escape(data.message),
                        created_add: data.created_add
                    };
                    connection.query('INSERT INTO mr_class_messages SET ?', post, function (err, result) {
                        if (err) throw err;
                        //console.log(result.insertId);
                        callback(null, result.insertId);
                    });
                }
                connection.release();
            });

        }
    ], function (err, result) {
        console.log(result.toString());

        data.files.forEach(function(element, index, array) {
            cMysql.getConnection(function (err, connection) {
                if (err) {
                    console.log("MYSQL: can't get connection from pool:", err);
                } else {
                    var post = {
                        user_id: data.user_id,
                        class_id: data.class_id,
                        file_src: element.src,
                        file_type: 'files',
                        class_type: 'class',
                        id_msg_class: result,
                        mime_type: element.type,
                        date_create: data.created_add
                    };
                    connection.query('INSERT INTO mr_files SET ?', post, function (err, result) {
                        if (err) throw err;
                        //files_id.push(result.insertId);
                    });
                }
                connection.release();
            });
        });
        return false;
    });
};

io.on('connection', function (socket) {

    console.log(socket.id + ': user connected');

    socket.on('loginClass', function (data) {
        filaments[socket.id] =  data.class_id;

        async.waterfall([
            function(callback) {
                socket.join(data.class_id);
                if (Array.isArray(online[data.class_id])) {
                    online[data.class_id].push({[socket.id] : data.user_id});
                } else {
                    online[data.class_id] = [];
                    online[data.class_id].push({[socket.id]: data.user_id});
                }
                callback(null, data.class_id);
            }
        ], function (err, result) {
            cMysql.getConnection(function (err, connection) {
                if (err) {
                    console.log("MYSQL: can't get connection from pool:", err);
                } else {
                    var id = data.user_id;
                    connection.query('SELECT u.id, u.name, u.surname, ui.avatar FROM mr_user u LEFT JOIN mr_user_info ui ON ui.id_user = u.id WHERE u.id = ' + id, function (err, rows) {
                        if (err) throw err;
                        socket.to(result).emit('JoinChat', rows);
                    });
                }
                connection.release();
            });
            return false;
        });

    });

    socket.on('classMessage', function (data) {
        data.created_add = getDate() + ', ' + getTime();
        saveMessage(data);

        cMysql.getConnection(function (err, connection) {
            if (err) {
                console.log("MYSQL: can't get connection from pool:", err);
            } else {
                connection.query('SELECT ui.avatar, us.name, us.surname FROM mr_user_info ui \n\
                                        LEFT JOIN mr_user us ON  us.id = '+ data.user_id+ '\n\
                                        WHERE ui.id_user = ' + data.user_id, function (err, rows) {
                    if (err) throw err;
                    data.avatar = rows[0].avatar;
                    data.name = rows[0].name;
                    data.surname = rows[0].surname;
                    socket.emit('reciveClassMessage', data);
                    socket.to(data.class_id).emit('reciveClassMessage', data);
                });
            }
            connection.release();
        });
    });
    socket.on('getListMessage', function(data){
        cMysql.getConnection(function (err, connection) {
            if (err) {
                console.log("MYSQL: can't get connection from pool:", err);
            } else {
                connection.query('SELECT ui.avatar, us.name, us.surname, cm.user_id, cm.message, cm.created_add, \n\
                                f.mime_type, f.file_src, cm.id \n\
                                FROM mr_class_messages cm \n\
                                LEFT JOIN mr_files f ON f.id_msg_class = cm.id AND f.class_type = "class" \n\
                                LEFT JOIN mr_user_info ui ON cm.user_id = ui.id_user \n\
                                LEFT JOIN mr_user us ON cm.user_id = us.id \n\
                                WHERE cm.class_id = ' + data.class_id, function (err, rows) {
                    if (err) throw err;
                    socket.emit('reciveListMessage', rows);
                });
            }
            connection.release();
        });
    });

    socket.on('getOnlineUsers', function(data){
        var users_id = [];
        async.waterfall([
            function(callback) {
                online[data.class_id].forEach(function(element, index, array) {
                    users_id.push(element[Object.keys(element)]);
                });
                callback(null, unique(users_id));
            }
        ], function (err, result) {
            cMysql.getConnection(function (err, connection) {
                if (err) {
                    console.log("MYSQL: can't get connection from pool:", err);
                } else {
                    var id = data.user_id;
                    connection.query('SELECT u.id, u.name, u.surname, ui.avatar FROM mr_user u LEFT JOIN mr_user_info ui ON ui.id_user = u.id WHERE u.id in (' + result + ')', function (err, rows) {
                        if (err) throw err;
                        socket.emit('responseOnlineUsers', rows);
                    });
                }
                connection.release();
            });
            return false;
        });
    });

    socket.on('disconnect', function () {
        console.log(socket.id + ': disconnected');

        async.waterfall([
            function(callback) {
                console.log(online[filaments[socket.id]]);
                online[filaments[socket.id]].forEach(function(element, index, array) {
                    if((socket.id in element)){
                        socket.to(filaments[socket.id]).emit('LeaveChat', element[socket.id]);
                        socket.leave(filaments[socket.id]);
                        array.splice(index,1);
                        socket.emit('LeaveChat', element[socket.id]);
                    }
                    if(array.length == index + 1){
                        callback(null);
                    }
                });


            },
            function(callback) {
                delete(filaments[socket.id]);
                callback(null);
            }
        ], function (err, result) {
            return false;
        });
    });

});

getDate = function () {
    var dateObj = new Date(),
        year = dateObj.getFullYear(),
        month = dateObj.getMonth() + 1,
        date = dateObj.getDate();
    if (month < 10) {
        month = '0' + month;
    }
    if (date < 10) {
        date = '0' + date;
    }
    return year + '-' + month + '-' + date;
};

getTime = function () {
    var dateObj = new Date(),
        hour = dateObj.getHours(),
        minutes = dateObj.getMinutes(),
        seconds = dateObj.getSeconds();
    if (hour < 10) {
        hour = '0' + hour;
    }
    if (minutes < 10) {
        minutes = '0' + minutes;
    }
    if (seconds < 10) {
        seconds = '0' + seconds;
    }
    return hour + ':' + minutes + ':' + seconds;
};