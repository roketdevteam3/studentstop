var app = require('express')();
var app2 = require('express')();
var fs = require('fs');
var options = {
    //    key: fs.readFileSync('D:/web_server/OpenServer/modules/http/Apache-2.4/conf/ssl/server.key'),
    //    cert: fs.readFileSync('D:/web_server/OpenServer/modules/http/Apache-2.4/conf/ssl/server.crt'),

    //stepan
    //        key: fs.readFileSync('C:/wamp/key/ca.key'),
    //        cert: fs.readFileSync('C:/wamp/key/ca.crt'),
    //stepan
    //
    //
    // key: fs.readFileSync('C:/OpenServer/domains/studentstop.dev/nodejs/key/localhost.key'),
    // cert: fs.readFileSync('C:/OpenServer/domains/studentstop.dev/nodejs/key/localhost.crt'),
    key: fs.readFileSync('/home/student/web/thestudentstop.com/public_html/nodejs/key/localhost.key'),
    cert: fs.readFileSync('/home/student/web/thestudentstop.com/public_html/nodejs/key/localhost.crt'),
    requestCert: false,
    rejectUnauthorized: false
};
var server = require('https').Server(options, app),
    rtcServer =  require('https').Server(options, app2)
io = require('socket.io')(server),
    mysql = require('mysql'),
    dbconfig = require("./db/config"),
    users = {},
    filaments = [];

cMysql = mysql.createPool({
    database: dbconfig.database,
    host: dbconfig.host,
    user: dbconfig.user,
    password: dbconfig.password,
    connectionLimit: dbconfig.connectionLimit
});

server.listen(3000, function() {
    console.info('listening on *:3000');
});
rtcServer.listen(3001, function() {
    console.info('listening on *:3000');
});
var sockets = require('signalmaster/sockets')

sockets(rtcServer, {
    "isDev": false,
    "rooms": {
        "maxClients": 0
    },
    "stunservers": [{
        "url": "stun:stun.l.google.com:19302",
        "url":'stun:stun01.sipphone.com',
        "url":'stun:stun.ekiga.net',
        "url":'stun:stun.fwdnet.net',
        "url":'stun:stun.ideasip.com',
        "url":'stun:stun.iptel.org',
        "url":'stun:stun.rixtelecom.se',
        "url":'stun:stun.schlund.de',
        "url":'stun:stun.l.google.com:19302',
        "url":'stun:stun1.l.google.com:19302',
        "url":'stun:stun2.l.google.com:19302',
        "url":'stun:stun3.l.google.com:19302',
        "url":'stun:stun4.l.google.com:19302',
        "url":'stun:stunserver.org',
        "url":'stun:stun.softjoys.com',
        "url":'stun:stun.voiparound.com',
        "url":'stun:stun.voipbuster.com',
        "url":'stun:stun.voipstunt.com',
        "url":'stun:stun.voxgratia.org',
        "url":'stun:stun.xten.com',
    }],
    "turnservers": [{
        "urls": ["turn:your.turn.servers.here"],
        "secret": "turnserversharedsecret",
        "expiry": 86400
    }]
})

var streams = [];
var videoconferences = {};

saveMessage = function(sender, data) {
    cMysql.getConnection(function(err, connection) {
        if (err) {
            console.log("MYSQL: can't get connection from pool:", err);
        } else {
            var post = {
                sender: sender,
                recipient: data.id,
                message: mysql.escape(data.message),
                created_add: getDate() + ', ' + getTime(),
                viewed: 0
            };
            connection.query('INSERT INTO st_messages  SET ?', post, function(err, result) {
                if (err) throw err;
                console.log(result.insertId);
            });
        }
        connection.release();
    });
};

ContactList = function(socket, data, type_user) {
    cMysql.getConnection(function(err, connection) {
        if (err) {
            console.log("MYSQL: can't get connection from pool:", err);
        } else {

            switch (type_user) {
                case 'sender':
                    connection.query('SELECT u.name, u.surname, ui.avatar, u.id AS `id` FROM `mr_user` u LEFT JOIN `mr_user_info` ui ON u.`id` = ui.`id_user` LEFT JOIN `mr_contact_list` cl ON u.`id` = cl.`recipient_id` WHERE cl.`sender_id`=' + data.sender_id + ' ORDER BY cl.`id` DESC', function(err, rows) {
                        if (err) throw err;
                        var users_list = {
                            list: rows,
                            select: {
                                id: data.recipient_id,
                                selected: (data.recipient_id) ? true : false
                            }
                        };

                        if (Array.isArray(users[data.sender_id])) {
                            for (var i = 0; i < users[data.sender_id].length; i++) {
                                if (users[data.sender_id][i].id == socket.id) {
                                    users[data.sender_id][i].emit('responseContactList', users_list);
                                    unreadedCount(users[data.sender_id][i], data);
                                }
                            }
                        } else {
                            users[data.sender_id].emit('responseContactList', users_list);
                            unreadedCount(users[data.sender_id], data);
                        }

                    });
                    connection.release();
                    break;
                case 'recipient':
                    connection.query('SELECT u.name, u.surname, ui.avatar, u.id AS `id` FROM `mr_user` u LEFT JOIN `mr_user_info` ui ON u.`id` = ui.`id_user` LEFT JOIN `mr_contact_list` cl ON u.`id` = cl.`recipient_id` WHERE cl.`sender_id`=' + data.recipient_id + ' ORDER BY cl.`id` DESC', function(err, rows) {
                        if (err) throw err;
                        var users_list = {
                            list: rows,
                            select: {
                                id: '',
                                selected: false
                            }
                        };

                        if (users[data.recipient_id]) {
                            data.sender_id = data.recipient_id;

                            //unreadedCount(users[data.recipient_id], data);
                            console.log('recipient_id - ' + users[data.recipient_id]);
                            if (Array.isArray(users[data.recipient_id])) {
                                for (var i = 0; i < users[data.recipient_id].length; i++) {
                                    users[data.recipient_id][i].emit('responseContactList', users_list);
                                    unreadedCount(users[data.recipient_id][i], data);
                                }
                            } else {
                                users[data.recipient_id].emit('responseContactList', users_list);
                                unreadedCount(users[data.recipient_id], data);
                            }
                        }
                    });
                    connection.release();
                    break;
            }
        }

    });
};

unreadedCount = function(user_socket, data) {
    cMysql.getConnection(function(err, connection) {
        if (err) {
            console.log("MYSQL: can't get connection from pool:", err);
        } else {
            connection.query('SELECT sm.sender, COUNT(*) as unreadedCount FROM st_messages sm WHERE sm.recipient = ' + data.sender_id + ' AND sm.viewed = 0 AND EXISTS (SELECT * FROM mr_contact_list cl WHERE cl.sender_id = ' + data.sender_id + ' OR cl.recipient_id = ' + data.sender_id + ') GROUP BY sm.sender', function(err, rows) {
                if (err) throw err;
                user_socket.emit('responseUnreadedCount', rows);
            });
        }
        connection.release();
    });
};

updateUnread = function(data, socket, user_socket) {
    cMysql.getConnection(function(err, connection) {
        if (err) {
            console.log("MYSQL: can't get connection from pool:", err);
        } else {
            connection.query('UPDATE st_messages SET viewed = 1 WHERE (recipient = ' + filaments[socket.id] + ' AND sender = ' + data.id + ')', function(err, results) {
                if (err) throw err;
                data.sender_id = filaments[socket.id];
                unreadedCount(user_socket, data);
            });
        }
        connection.release();
    });
};
io.on('connection', function(socket) {

    console.log(socket.id + ': user connected');

    socket.on('login', function(data) {
        var user_name = Object.keys(users);
        var tmp_user = '';
        if (user_name.indexOf(data.id.toString()) != -1) {
            if (Array.isArray(users[data.id])) {
                users[data.id].push(socket);
            } else {
                tmp_user = users[data.id];
                users[data.id] = [];
                users[data.id].push(tmp_user, socket);
            }
        } else {
            socket.broadcast.emit('onlineChange', {
                user: data.id,
                status: 1
            });
            users[data.id] = socket;
        }
        filaments[socket.id] = data.id;
        data.sender_id = data.id;
        ContactList(socket, data, 'sender');
        getAllContactReguest(filaments[socket.id]);
        getAllRentReguest(filaments[socket.id]);
    });

    socket.on('getusersmember', function(data) {
        cMysql.getConnection(function(err, connection) {
            if (err) {
                console.log("MYSQL: can't get connection from pool:", err);
            } else {
                var sql_query = '';
                sql_query = "SELECT DISTINCT `mr_user_info`.address AS `address`, `mr_user_info`.birthday AS `birthday`, `mr_user_info`.position_lng AS `position_lng`, `mr_user_info`.position_lat AS `position_lat`, `mr_user_info`.avatar AS `avatar`, `mr_user`.name AS `name`, `mr_user`.surname AS `surname`, `mr_user`.id AS `id`, `mr_user_role`.name AS `userRoleName` , `mr_university`.name AS `universityName`, `mr_user`.name AS `name` " +
                    " FROM `mr_user` " +
                    " LEFT JOIN " +
                    " `mr_user_info` ON `mr_user_info`.id_user = `mr_user`.id " +
                    " LEFT JOIN " +
                    " `mr_user_role` ON `mr_user_role`.id = `mr_user_info`.academic_status " +
                    " LEFT JOIN " +
                    " `mr_university` ON `mr_university`.id = `mr_user_info`.university_id " +
                    " LEFT JOIN " + 
                        " `mr_follower` ON `mr_follower`.id_user = `mr_user`.id AND `mr_follower`.type_object = 'class' "+ 
                    " LEFT JOIN" +
                        " `mr_class` ON `mr_class`.id = `mr_follower`.id_object "+
                    " WHERE NOT(`mr_user_info`.university_id = 0) ";
//                course
                
                if (data.course != '') {
                    sql_query  += " AND `mr_class`.id_course = '"+data.course+"' ";
                }
                
                if (data.age_from != '') {
                    sql_query += " AND " + data.age_from + " <= YEAR(CURDATE()) - YEAR(`mr_user_info`.birthday) ";
                }
                if (data.age_to != '') {
                    sql_query += " AND " + data.age_to + " >= YEAR(CURDATE()) - YEAR(`mr_user_info`.birthday) ";
                }

                if (data.search_by_key != '') {
                    sql_query += " AND (`mr_user`.`name` LIKE '%" + data.search_by_key + "%' OR `mr_user`.`surname` LIKE '%" + data.search_by_key + "%') "
                }
                if (data.university != '') {
                    sql_query += " AND `mr_university`.`id` = '" + data.university + "' ";
                }
                if (data.user_status != '') {
                    sql_query += " AND `mr_user_info`.`academic_status` = " + data.user_status + " ";
                }
                if (data.sex != '') {
                    sql_query += " AND `mr_user_info`.`gender` = '" + data.sex + "' ";
                }
                console.log(sql_query);

                connection.query(sql_query, function(err, rows) {

                    console.log(rows);
                    //connection.query('SELECT *, `mr_user`.id AS `id` FROM `mr_user` LEFT JOIN `mr_user_info` ON `mr_user_info`.id_user = `mr_user`.id WHERE NOT (`mr_user_info`.position_lng = "") ', function (err, rows) {
                    if (err) throw err;
                    if (rows.length == 0) {
                        if (Array.isArray(users[filaments[socket.id]])) {
                            for (var i = 0; i < users[filaments[socket.id]].length; i++) {
                                if (users[filaments[socket.id]][i].id == socket.id) {
                                    users[filaments[socket.id]][i].emit('responseusersmember', "");
                                }
                            }
                        } else {
                            users[filaments[socket.id]].emit('responseusersmember', "");
                        }
                    }
                    rows.forEach(function(el, i, arr) {

                        if (users[el.id]) {
                            arr[i].onlineStatus = 1;
                        } else {
                            arr[i].onlineStatus = 0;
                        }


                        console.log(arr.length);
                        console.log(i + 1);
                        if (arr.length == i + 1) {
                            if (Array.isArray(users[filaments[socket.id]])) {
                                for (var i = 0; i < users[filaments[socket.id]].length; i++) {
                                    if (users[filaments[socket.id]][i].id == socket.id) {
                                        users[filaments[socket.id]][i].emit('responseusersmember', arr);
                                    }
                                }
                            } else {
                                users[filaments[socket.id]].emit('responseusersmember', arr);
                            }
                        }
                    })
                });
            }
            connection.release();
        });
    });



    socket.on('message', function(obj) {
        var sender_id = filaments[socket.id];

        cMysql.getConnection(function(err, connection) {
            if (err) {
                console.log("MYSQL: can't get connection from pool:", err);
            } else {
                connection.query('UPDATE st_messages SET viewed = 1 WHERE (sender = ' + obj.id + ' AND recipient = ' + sender_id + ')', function(err, results) {
                    if (err) throw err;
                    var data = {
                        sender_id: sender_id
                    };
                    ContactList(socket, data, 'sender');
                });
            }
            connection.release();
        });

        if (Array.isArray(users[sender_id])) {
            for (var i = 0; i < users[sender_id].length; i++) {
                obj.created_add = getDate() + ',' + getTime();
                users[sender_id][i].emit('responseMessage', obj);
            }
        } else {
            obj.created_add = getDate() + ',' + getTime();
            users[sender_id].emit('responseMessage', obj);
        }
        if (users[obj.id]) {
            obj.sender_id = sender_id;
            var data = {};
            if (Array.isArray(users[obj.id])) {
                for (var j = 0; j < users[obj.id].length; j++) {
                    users[obj.id][j].emit('reciveMessages', obj);
                    data = {
                        recipient_id: obj.id
                    };
                    ContactList(socket, data, 'recipient');
                }
            } else {
                users[obj.id].emit('reciveMessages', obj);
                data = {
                    recipient_id: obj.id
                };
                ContactList(socket, data, 'recipient');
            }
            console.log('ContactList --- recipient')
        }
        saveMessage(sender_id, obj);
    });

    socket.on('getOnline', function() {
        var id = filaments[socket.id];
        if (Array.isArray(users[id])) {
            for (var i = 0; i < users[id].length; i++) {
                users[id][i].emit('onlineUsers', Object.keys(users));
            }
        } else {
            users[id].emit('onlineUsers', Object.keys(users));
        }
    });
    socket.on('check', function(data) {
        var id = filaments[socket.id];
        console.log(videoconferences);
        if (videoconferences[data.vcId]) {
          if (Array.isArray(users[id])) {
              for (var i = 0; i < users[id].length; i++) {
                  users[id][i].emit('vc-start', {
                    'room': videoconferences[data.vcId],
                    'vcId': data.vcId
                  });
              }
          } else {
              users[id].emit('vc-start', {
                'room': videoconferences[data.vcId],
                'vcId': data.vcId
              });
          }
        }else {
          if (Array.isArray(users[id])) {
              for (var i = 0; i < users[id].length; i++) {
                  users[id][i].emit('vc-f');
              }
          } else {
              users[id].emit('vc-f');
          }
        }
    });
    socket.on('vc-s', function(data) {
        var id = filaments[socket.id];
        videoconferences[data.vcId] = data.room;
        console.log(videoconferences);
        socket.broadcast.emit('vc-start', {
            'room': videoconferences[data.vcId],
            'vcId': data.vcId
        });
        // if (videoconferences[data.vcId]) {
        //   if (Array.isArray(users[id])) {
        //       for (var i = 0; i < users[id].length; i++) {
        //           users[id][i].emit('vc', videoconferences[data.vcId]);
        //       }
        //   } else {
        //       users[id].emit('vc', videoconferences[data.vcId]);
        //   }
        // }else {
        //   if (Array.isArray(users[id])) {
        //       for (var i = 0; i < users[id].length; i++) {
        //           users[id][i].emit('vc-f');
        //       }
        //   } else {
        //       users[id].emit('vc-f');
        //   }
        // }
    });
    socket.on('decline-call-v-server', function(data) {
        var id = filaments[socket.id];
        var vcId = data.vcId
        delete videoconferences[vcId]
        console.log(videoconferences);

        socket.broadcast.emit('vc-end', {
            'vcId': vcId
        });
        // if (videoconferences[data.vcId]) {
        //   if (Array.isArray(users[id])) {
        //       for (var i = 0; i < users[id].length; i++) {
        //           users[id][i].emit('vc', videoconferences[data.vcId]);
        //       }
        //   } else {
        //       users[id].emit('vc', videoconferences[data.vcId]);
        //   }
        // }else {
        //   if (Array.isArray(users[id])) {
        //       for (var i = 0; i < users[id].length; i++) {
        //           users[id][i].emit('vc-f');
        //       }
        //   } else {
        //       users[id].emit('vc-f');
        //   }
        // }
    });
    socket.on('video_message', function(data) {
        // sender_id =
        sender_id = filaments[socket.id]
        recipient_id = data.recipient_id;
        message = data.message;
        sender_id = filaments[socket.id]
        if (Array.isArray(users[recipient_id])) {
            for (var i = 0; i < users[recipient_id].length; i++) {
                users[recipient_id][i].emit('video_message', message);
            }
        } else {
            console.log([sender_id, recipient_id]);
            message.recipient_id = sender_id;
            users[recipient_id].emit('video_message', message);
        }
        // socket.broadcast.emit('video_message', message); // should be room only
    });
    socket.on('streaming', function(data) {
        console.log(filaments[socket.id] + " " + data.reciver_id + " " + data.type);
        // sender_id = filaments[socket.id]
        if (data.type == 'start') {
            streams.push(filaments[socket.id]);
        } else if (data.type == 'connect') {
            console.log(streams);

            if (streams.indexOf(data.owner_id) > -1) {
                if (!Array.isArray(users[data.owner_id])) {
                    console.log(data.owner_id);
                    users[data.owner_id].emit('responseStreaming', {
                        type: "connect_request",
                        reciver_id: data.reciver_id
                    })
                } else {
                    for (var i = 0; i < users[data.owner_id].length; i++) {
                        users[data.owner_id][i].emit('responseStreaming', {
                            type: "connect_request",
                            reciver_id: data.reciver_id
                        })
                    }
                }
            }
        } else if (data.type == "rtc") {
            if (!Array.isArray(users[data.reciver_id])) {
                users[data.reciver_id].emit('responseStreaming', {
                    type: data.message.type,
                    reciver_id: filaments[socket.id],
                    message: data.message
                });
            } else {
                for (var i = 0; i < users[data.reciver_id].length; i++) {
                    users[data.reciver_id][i].emit('responseStreaming', {
                        type: data.message.type,
                        reciver_id: filaments[socket.id],
                        message: data.message
                    });
                }
            }
        }
        // if (Array.isArray(users[recipient_id])) {
        //     for (var i = 0; i < users[recipient_id].length; i++) {
        //         users[recipient_id][i].emit('video_message', message);
        //     }
        // } else {
        //     console.log([sender_id,recipient_id]);
        //     message.recipient_id = sender_id;
        //     users[recipient_id].emit('video_message', message);
        // }
        // socket.broadcast.emit('video_message', message); // should be room only
    });

    socket.on('getAllMessages', function(data) {
        cMysql.getConnection(function(err, connection) {
            if (err) {
                console.log("MYSQL: can't get connection from pool:", err);
            } else {
                connection.query('SELECT * FROM st_messages WHERE (sender = ' + filaments[socket.id] + ' AND recipient = ' + data.id + ') OR (recipient = ' + filaments[socket.id] + ' AND sender = ' + data.id + ') ORDER BY id DESC LIMIT 0,100', function(err, rows) {
                    if (err) throw err;
                    if (Array.isArray(users[filaments[socket.id]])) {
                        for (var i = 0; i < users[filaments[socket.id]].length; i++) {
                            if (users[filaments[socket.id]][i].id == socket.id) {
                                users[filaments[socket.id]][i].emit('responseAllMessages', rows);
                                data.sender_id = filaments[socket.id];
                                updateUnread(data, socket, users[filaments[socket.id]][i]);
                            }
                        }
                    } else {
                        users[filaments[socket.id]].emit('responseAllMessages', rows);
                        data.sender_id = filaments[socket.id];
                        updateUnread(data, socket, users[filaments[socket.id]]);
                    }
                });
            }
            connection.release();
        });
    });
    socket.on('incoming-call-server', function(data) {
        var sId = filaments[socket.id];
        var rId = data.rId;
        console.log(rId);
        cMysql.getConnection(function(err, connection) {
            if (err) {
                console.log("MYSQL: can't get connection from pool:", err);
            } else {
                connection.query('SELECT CONCAT(u.`name`, " ", u.`surname`) as sender  FROM mr_user u WHERE u.id = "'+sId+'"', function(err, result) {
                    if (err) throw err;
                    console.log(result);
                    if(users[rId]){
                      if (Array.isArray(users[data.rId])) {
                        for (var i = 0; i < users[rId].length; i++) {
                          users[rId][i].emit('incoming-call', {
                            sId: sId,
                            roomId: data.room,
                            sender: result[0].sender
                          });
                        }
                      } else {
                        console.log('here');
                        users[rId].emit('incoming-call', {
                          sId: sId,
                          roomId: data.room,
                          sender: result[0].sender
                        });
                      }
                    }
                });
            }
            connection.release();
        });

    });
    socket.on('decline-call-server', function(data) {
        var sId = filaments[socket.id];
        var rId = data.rId;
        if (Array.isArray(users[data.rId])) {
            for (var i = 0; i < users[rId].length; i++) {
                users[rId][i].emit('decline-call', {
                    sId: sId,
                    roomId: data.room
                });
            }
        } else {
            users[rId].emit('decline-call', {
                sId: sId,
                roomId: data.room
            });
        }
    });
    socket.on('newConwersation', function(data) {
        cMysql.getConnection(function(err, connection) {
            if (err) {
                console.log("MYSQL: can't get connection from pool:", err);
            } else {
                connection.query('INSERT INTO mr_contact_list (`sender_id`, `recipient_id`) SELECT "' + data.sender_id + '" as sender_id, "' + data.recipient_id + '" as recipient_id FROM mr_user WHERE NOT EXISTS (SELECT * FROM mr_contact_list WHERE (mr_contact_list.`sender_id` = ' + data.sender_id + ' AND mr_contact_list.`recipient_id` = ' + data.recipient_id + ')) LIMIT 0,1', function(err, result) {
                    if (err) throw err;
                    connection.query('INSERT INTO mr_contact_list (`sender_id`, `recipient_id`) SELECT "' + data.recipient_id + '" as sender_id, "' + data.sender_id + '" as recipient_id FROM mr_user WHERE NOT EXISTS (SELECT * FROM mr_contact_list WHERE (mr_contact_list.`recipient_id` = ' + data.sender_id + ' AND mr_contact_list.`sender_id` = ' + data.recipient_id + ')) LIMIT 0,1', function(err, result) {
                        if (err) throw err;
                        ContactList(socket, data, 'sender');
                        ContactList(socket, data, 'recipient');
                    });
                });

            }
            connection.release();
        });
    });

    socket.on('contactRequest', function(data) {
        cMysql.getConnection(function(err, connection) {
            if (err) {
                console.log("MYSQL: can't get connection from pool:", err);
            } else {
                connection.query('INSERT INTO mr_notifications (`notification_id`, `type`, `from_user_id`, `to_user_id`, `created_add`) VALUE (' + data.post_id + ', "contact_request", ' + data.user_id + ', ' + data.recipient_id + ', "' + getDate() + ' ' + getTime() + '")', function(err, result) {
                    if (err) throw err;
                    sendAllContactReguest(filaments[socket.id], data);
                });
            }
            connection.release();
        });
    });

    socket.on('rentRequest', function(data) {
        cMysql.getConnection(function(err, connection) {
            if (err) {
                console.log("MYSQL: can't get connection from pool:", err);
            } else {
                connection.query('INSERT INTO mr_notifications (`notification_id`, `type`, `from_user_id`, `to_user_id`, `created_add`) VALUE (' + data.rent_id + ', "rent_request", ' + filaments[socket.id] + ', ' + data.recipient_id + ', "' + getDate() + ' ' + getTime() + '")', function(err, result) {
                    if (err) throw err;
                    sendAllRentReguest(filaments[socket.id], data);
                });
            }
            connection.release();
        });
    });

    socket.on('disconnect', function() {
        var id = filaments[socket.id],
            index = -1;
        if (Array.isArray(users[id])) {
            index = users[id].indexOf(socket);
            users[id].splice(index, 1);
            if (users[id].length === 0) {
                delete users[id];
                delete filaments[socket.id];
                socket.broadcast.emit('onlineChange', {
                    'user': id,
                    status: 0
                });
            }
        } else {
            delete users[id];
            delete filaments[socket.id];

            socket.broadcast.emit('onlineChange', {
                'user': id,
                status: 0
            });
        }

        console.log(socket.id + ': user disconnected');
    });

});

getAllContactReguest = function(id) {
    cMysql.getConnection(function(err, connection) {
        if (err) {
            console.log("MYSQL: can't get connection from pool:", err);
        } else {
            connection.query("SELECT nt.created_add, nt.type, nt.id, u.name, u.surname, ui.avatar, p.title FROM mr_notifications nt LEFT JOIN mr_user u ON nt.from_user_id = u.id LEFT JOIN mr_user_info ui ON ui.id_user = u.id LEFT JOIN mr_posts p ON p.id = nt.notification_id WHERE nt.to_user_id = " + id + " AND nt.type = 'contact_request' ORDER BY nt.id DESC", function(err, rows) {
                if (err) throw err;
                var results = {
                    notifications: []
                };
                rows.forEach(function(element, index, array) {
                    results.notifications.push(element);
                });
                if (Array.isArray(users[id])) {
                    for (var i = 0; i < users[id].length; i++) {
                        users[id][i].emit('responseNotifications', results);
                    }
                } else {
                    users[id].emit('responseNotifications', results);
                }
            });
            connection.release();
        }
    });
};
getAllRentReguest = function(id) {
    cMysql.getConnection(function(err, connection) {
        if (err) {
            console.log("MYSQL: can't get connection from pool:", err);
        } else {
            connection.query("SELECT nt.created_add, nt.type, nt.id, u.name, u.surname, ui.avatar, p.title FROM mr_notifications nt LEFT JOIN mr_user u ON nt.from_user_id = u.id LEFT JOIN mr_user_info ui ON ui.id_user = u.id LEFT JOIN mr_posts_rent pr ON pr.id = nt.notification_id LEFT JOIN mr_posts p ON p.id = pr.post_id WHERE nt.to_user_id = " + id + " AND nt.type = 'rent_request' ORDER BY nt.id DESC", function(err, rows) {
                if (err) throw err;
                var results = {
                    notifications: []
                };
                rows.forEach(function(element, index, array) {
                    results.notifications.push(element);
                });
                if (Array.isArray(users[id])) {
                    for (var i = 0; i < users[id].length; i++) {
                        users[id][i].emit('responseNotifications', results);
                    }
                } else {
                    users[id].emit('responseNotifications', results);
                }
            });
            connection.release();
        }
    });
};



sendAllContactReguest = function(id, data) {
    cMysql.getConnection(function(err, connection) {
        if (err) {
            console.log("MYSQL: can't get connection from pool:", err);
        } else {
            connection.query("SELECT nt.created_add, nt.type, nt.id, u.name, u.surname, ui.avatar, p.title FROM mr_notifications nt LEFT JOIN mr_user u ON nt.from_user_id = u.id LEFT JOIN mr_user_info ui ON ui.id_user = u.id LEFT JOIN mr_posts p ON p.id = nt.notification_id WHERE nt.to_user_id = " + data.recipient_id + " AND nt.type = 'contact_request' ORDER BY nt.id DESC", function(err, rows) {
                if (err) throw err;
                var results = {
                    notifications: []
                };
                rows.forEach(function(element, index, array) {
                    console.log(element);
                    results.notifications.push(element);
                });
                if (users[data.recipient_id]) {
                    if (Array.isArray(users[data.recipient_id])) {
                        for (var i = 0; i < users[data.recipient_id].length; i++) {
                            users[data.recipient_id][i].emit('responseNotifications', results);
                        }
                    } else {
                        users[data.recipient_id].emit('responseNotifications', results);
                    }
                }
            });
        }
        connection.release();
    });
};
sendAllRentReguest = function(id, data) {
    cMysql.getConnection(function(err, connection) {
        if (err) {
            console.log("MYSQL: can't get connection from pool:", err);
        } else {
            connection.query("SELECT nt.created_add, nt.type, nt.id, u.name, u.surname, ui.avatar, p.title FROM mr_notifications nt LEFT JOIN mr_user u ON nt.from_user_id = u.id LEFT JOIN mr_user_info ui ON ui.id_user = u.id LEFT JOIN mr_posts_rent pr ON pr.id = nt.notification_id LEFT JOIN mr_posts p ON p.id = pr.post_id WHERE nt.to_user_id = " + data.recipient_id + " AND nt.type = 'rent_request' ORDER BY nt.id DESC", function(err, rows) {
                if (err) throw err;
                var results = {
                    notifications: []
                };
                rows.forEach(function(element, index, array) {
                    console.log(element);
                    results.notifications.push(element);
                });
                if (users[data.recipient_id]) {
                    if (Array.isArray(users[id])) {
                        for (var i = 0; i < users[data.recipient_id].length; i++) {
                            users[data.recipient_id][i].emit('responseNotifications', results);
                        }
                    } else {
                        users[data.recipient_id].emit('responseNotifications', results);
                    }
                }
            });
        }
        connection.release();
    });
};

getDate = function() {
    var dateObj = new Date(),
        year = dateObj.getFullYear(),
        month = dateObj.getMonth() + 1,
        date = dateObj.getDate();
    if (month < 10) {
        month = '0' + month;
    }
    if (date < 10) {
        date = '0' + date;
    }
    return year + '-' + month + '-' + date;
};

getTime = function() {
    var dateObj = new Date(),
        hour = dateObj.getHours(),
        minutes = dateObj.getMinutes(),
        seconds = dateObj.getSeconds();
    if (hour < 10) {
        hour = '0' + hour;
    }
    if (minutes < 10) {
        minutes = '0' + minutes;
    }
    if (seconds < 10) {
        seconds = '0' + seconds;
    }
    return hour + ':' + minutes + ':' + seconds;
};
