<?php
/**
 * Created by PhpStorm.
 * User: Listat
 * Date: 26.07.2016
 * Time: 12:41
 */

namespace app\commands;

use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

class Paypal
{
    public $api;

    function __construct() {
        $this->api = new ApiContext(
            new OAuthTokenCredential(
                'Aexav0b3Y_v8ifdnduatKR4W--LJ1cq12SHqBC8YbXY6Cqtx6Dl4bxvDP9KvU-wT9_x56k9fr0e1sAhJ',
                'EFqPr8zq9XvguNk-dOlKZSXzzts9JRvbL6PrAGxelAecU7Va3zEVmOBQd8qtgn_XDAValDptnQu0aveV'
            )
        );
        $this->api->setConfig([
            'model'=>'sandbox',
            'http.ConnectionTimeOut'=>30,
            'log.LogEnabled'=> 0,
            'log.FileName'=> '',
            'log.LogLevel' => 'FINE',
            'validation.level'=>'log'
        ]);
    }
}