<?php

namespace app\modules\question\models;

use app\models\user\UserBonus;
use app\modules\users\models\User;
use Yii;

class Question extends \yii\db\ActiveRecord
{
    public $category;
    
    public static function tableName()
    {
        return '{{%question}}';
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              $this->date_create = date("Y-m-d H:i:s");
              $this->status = '1';
        }

        return parent::beforeSave($insert);
    }
    
    public function scenarios()
    {
        return [
            'close_question' => ['status'],
            'add_question' => ['question_title', 'university_id', 'country_id', 'states_id', 'city_id','question_text','privacy','user_id','category_id','price', 'class_id','credits'],
            'up_question' => ['question_title', 'university_id', 'country_id', 'states_id', 'city_id','question_text','privacy','user_id','category_id','price', 'class_id','credits'],
	];
    }
    
    public function rules()
    {
        return [
             [['question_title','price','question_text'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id'),
        ];
    }

    public function getQuestioncat()
    {
        return $this->hasOne(Questioncategory::className(), ['id' => 'category_id']);
    }
     public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function afterSave($insert, $changedAttributes) {
        if ($this->isNewRecord) {
            $count = Question::find()->where(['user_id'=>Yii::$app->user->id])->count();
            if ($count == 1){
                $bonus = new UserBonus();
                $bonus->user_id = Yii::$app->user->id;
                $bonus->bonus = 5;
                $bonus->type = '1_question';
                $bonus->save();
            }elseif (is_int($count/10)){
                $bonus = new UserBonus();
                $bonus->user_id = Yii::$app->user->id;
                $bonus->bonus = 10 * ($count/10);
                $bonus->type = '10_question';
                $bonus->save();
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }

}
