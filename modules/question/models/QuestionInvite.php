<?php

namespace app\modules\question\models;

use Yii;

/**
 * This is the model class for table "mr_question_invite".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $question_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $view
 */
class QuestionInvite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mr_question_invite';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'question_id', 'created_at', 'updated_at','view'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'question_id' => 'Question ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
