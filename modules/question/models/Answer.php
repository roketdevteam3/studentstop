<?php

namespace app\modules\question\models;

use app\models\user\UserBonus;
use Yii;
use app\modules\users\models\User;

class Answer extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%answer}}';
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    public function scenarios()
    {
        return [
            'add_answer' => ['answer_text','show_text', 'user_id', 'money', 'question_id','credits'],
            'add_comment' => ['comment'],
            'status_change' => ['status']
	];
    }


    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id'),
        ];
    }
    public function rules()
    {
        return [
            [['money','answer_text','user_id','show_text'], 'required'],
            ['money','integer', 'min' => 0],
            ['credits','integer']
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getQuestion()
    {
        return $this->hasOne(Question::className(), ['id' => 'question_id']);
    }

    public function afterSave($insert, $changedAttributes) {
        if ($this->isNewRecord) {
            $count = Answer::find()->where(['user_id'=>Yii::$app->user->id])->count();
            if ($count == 1){
                $bonus = new UserBonus();
                $bonus->user_id = Yii::$app->user->id;
                $bonus->bonus = 5;
                $bonus->type = '1_answer';
                $bonus->save();
            }elseif (is_int($count/10)){
                $bonus = new UserBonus();
                $bonus->user_id = Yii::$app->user->id;
                $bonus->bonus = 10 * ($count/10);
                $bonus->type = '10_answer';
                $bonus->save();
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }

}