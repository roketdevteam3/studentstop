<?php

namespace app\modules\question\models;

use Yii;

/**
 * This is the model class for table "mr_favorite_question".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $question_id
 */
class FavoriteQuestion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mr_favorite_question';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'question_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'question_id' => 'Question ID',
        ];
    }
}
