<?php

namespace app\modules\question\models;

use Yii;

class Questioncategory extends \yii\db\ActiveRecord
{
    public $categories_child;
    
    public static function tableName()
    {
        return '{{%question_category}}';
    }

//    public function beforeSave($insert)
//    {
//        if ($this->isNewRecord)
//        {
//              $this->date_create = date("Y-m-d H:i:s");
//        }
//
//        return parent::beforeSave($insert);
//    }
    
    public function scenarios()
    {
        return [
            'add_question' => ['name', 'url_name', 'parent_id'],
	];
    }
    
    public function rules()
    {
        return [
            [['name','url_name'], 'required'],
            ['url_name', 'unique'],
            ['url_name', 'match','pattern'=>'/^[a-zA-Z0-9_]+$/'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id'),
        ];
    }
    
}