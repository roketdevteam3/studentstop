<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\Alert;
	use yii\widgets\ActiveForm;
		use kartik\rating\StarRating;
	use app\assets\AnswerAsset;
	AnswerAsset::register($this);
	
	$privacy_array = [1 => 'All User',2 => 'My friend',3 => 'Class mates'];
?>

	<?php
		if(Yii::$app->session->hasFlash('question_closed')):
			echo Alert::widget([
				'options' => [
					'class' => 'alert-info',
				],
				'body' => 'Question closed',
			]);
		endif; 
				
		if(Yii::$app->session->hasFlash('answer_added')):
			echo Alert::widget([
				'options' => [
					'class' => 'alert-info',
				],
				'body' => 'Answer added',
			]);
		endif; 
		if(Yii::$app->session->hasFlash('answer_not_added')):
			echo Alert::widget([
				'options' => [
					'class' => 'alert-error',
				],
				'body' => 'Answer not added',
			]);
		endif; 
		if(Yii::$app->session->hasFlash('question_save')):
			echo Alert::widget([
				'options' => [
					'class' => 'alert-info',
				],
				'body' => 'Question save',
			]);
		endif; 
		if(Yii::$app->session->hasFlash('question_not_save')):
			echo Alert::widget([
				'options' => [
					'class' => 'alert-error',
				],
				'body' => 'Question not save',
			]);
		endif; 
	?>

	<div class="row">
		<div class="question-right-panel questionshow-panel">
			<?= $this->render('/default/right_menu',['userPosted' => $modelUserPosted]); ?>
		</div>
		<div class="question-page">
			<div class="container-fluid background-block">
				<div class="row">
					<?= $this->render('/default/top_filter',['category_url' => null]); ?>
				</div>
				<div class="question-category question-show">
					<div class="question-show-content">
						<h2 class="question-title"><?= $modelQuestion['question_title']; ?></h2>
						<p class="description"><?= $modelQuestion['question_text']; ?></p>
						<p><span>Price for answer - </span> <?= $modelQuestion['price']; ?></p>
						<p><span>Posted - </span> <?= $modelQuestion['date_create']; ?></p>
					</div>
					<div class="btn-wrap">
													<?php if($modelQuestion['status'] == '1'){ ?>
														<button type="button" class="btn add-answer" data-toggle="modal" data-target="#newAnswer">Add answer</button>
													<?php } ?>
						<button type="button" class="btn apply">Apply</button>
						<?php if($modelQuestion['user_id'] == \Yii::$app->user->id){ ?>
							<button type="button" class="btn update-btn" data-toggle="modal" data-target="#questionUpdateModal">Update</button>
															<?php if($modelQuestion['status'] == '1'){ ?>
																<?php $form = ActiveForm::begin(); ?>
																	<input type="submit" class="btn btn-primary btn-xs" name="close_question" value="Close question">
																<?php ActiveForm::end();  ?>
															<?php } ?>
													<?php } ?>
					</div>
				</div>
				<h2 class="answer-heading"><span>Answer</span> to the question</h2>
				<?php if($modelAnswers != null){ ?>
					<?php foreach($modelAnswers as $answer){ ?>
						<div class="col-sm-12 answer<?= $answer['id']; ?>">
							<div class="question-category">
								<div class="user-image">
									<img src="<?= Url::home().'images/users_images/'.$answer['avatar']; ?>" class="img-responsive">
								</div>
								<div class="question-desc">
									<p class="answer-title">Answer from</b></p>
									<p class="user-name"><?= HTML::a($answer['name'].' '.$answer['surname'],Url::home().'profile/'.$answer['user_id']) ?></p>
									<?php if($answer['status'] == 0){ ?>
											<p class="short-answer"><span>Short answer - </span><?= $answer['show_text']; ?></p>
									   <?php }elseif($answer['status'] == 1){ ?>
										   <p class="full-answer"><span>Answer - </span><?= $answer['answer_text']; ?></p>
									   <?php } ?>
								</div>
								<div class="btn-wrap">
									<?php if($answer['status'] != 1){ ?>
										<button class="btn pull-right showFullAnswer" data-money="<?= $answer['money']; ?>" data-answer_id="<?= $answer['id']; ?>">View full answer</button>
									<?php } ?>
								</div>
							</div>
						</div>
					<?php } ?>
				<?php }else{ ?>
					<h2 class="answer-heading">This question has no answer</h2>
				<?php } ?>
			</div>
		</div>
	</div>

	<div id="questionUpdateModal" class="modal big-modal padding-body fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header"  style="clear:both;">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Update question</h4>
				</div>
				<?php $form = ActiveForm::begin(); ?>
					<div class="modal-body"  style="clear:both;">
						<?= $form->field($modelQuestion, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false); ?>
						<?= $form->field($modelQuestion, 'question_title')->textInput(); ?>
						<?= $form->field($modelQuestion, 'question_text')->textarea(); ?>
						<?= $form->field($modelQuestion, 'price')->textInput(['type'=>'number']); ?>
						<?= $form->field($modelQuestion, 'category')->dropDownList($category_array); ?>
						<?= $form->field($modelQuestion, 'category_id')->dropDownList([]); ?>
						<?= $form->field($modelQuestion, 'privacy')->dropDownList($privacy_array); ?>
					</div>
					<div class="modal-footer">
						<?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'change_user_info', 'class' => 'btn btn-action']) ?>
						<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
					</div>
				<?php ActiveForm::end();  ?>
			</div>
		</div>
	</div>

	<div id="newAnswer" class="modal padding-body fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">x</button>
					<h4 class="modal-title">Add answer</h4>
				</div>
				<div class="modal-body">
					<?php $form = ActiveForm::begin(); ?>
					<?= $form->field($modelNewAnswer, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false); ?>
					<?= $form->field($modelNewAnswer, 'question_id')->hiddenInput(['value' => $modelQuestion['id']])->label(false); ?>
					<?= $form->field($modelNewAnswer, 'show_text')->textarea();?>
					<?= $form->field($modelNewAnswer, 'answer_text')->textarea(); ?>
					<?= $form->field($modelNewAnswer, 'money')->textInput(['type' => 'number']); ?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
					<?= Html::submitButton( Yii::t('app', 'Add answer'), ['name'=> 'change_user_info', 'class' => 'btn btn-action', 'data-mr-loader'=>'show']) ?>
					<?php ActiveForm::end();  ?>
				</div>
			</div>
		</div>
	</div>

	<div id="showFullAnswer" class="modal padding-body fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Pay for answer</h4>
				</div>
				<div class="modal-body">
					<div class="modal-body">
						<input type="hidden" class="modalAnswerId" value="">
						<h4 class="pay-answer-head">For a complete answer to pay</h4>
						<h4 class="pay-answer-money"><span class="money_for_answer">0</span>$</h4>
						<div class="choise-pay-wrap">
							<div class="container-fluid text-center">
								<div class="row">
									<div class="col-xs-6">
										<span class="choise-pay">Pay via paypal</span><button class="btn btn-ripple PayWidthPaypal"><span class="fa fa-cc-paypal"></span></button>
									</div>
									<div class="col-xs-6">
										<span class="choise-pay">Pay via credites</span><button class="btn btn-ripple PayWidthCredits"><span class="fa fa-credit-card-alt"></span></button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div id="showAnswerText" class="modal padding-body fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Full Answer</h4>
				</div>
				<div class="modal-body">
					<div class="AnswerText">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success pull-left GoodQuestionRequest">Good</button>
					<button type="button" class="btn btn-danger pull-right BadQuestionRequest">Bad</button>
								</div>
			</div>
		</div>
	</div>

	<div id="show_rating_modal" class="modal padding-body fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
										<?php $form = ActiveForm::begin(); ?>
											<input type="hidden" name="Rating[answer_id]" >
											<?php echo StarRating::widget([
												'name' => 'Rating[rating_count]',
												'pluginOptions' => [
													'step' => '1',
													'size'=>'xs',
													'showCaption' => false,
												],
											]); ?>
											<textarea name="Rating[comment]"> </textarea>
											<?= Html::submitButton( Yii::t('app', 'Add rating'), ['name'=> 'add_rating', 'class' => 'btn btn-action', 'data-mr-loader'=>'show']) ?>
										<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>