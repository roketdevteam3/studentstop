<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\widgets\ActiveForm;
	$this->registerCssFile('/css/ls_style.css');
	$this->registerJsFile('/js/ls_question.js');
?>

	<div class="row">
		<div class="question-right-panel">
			<?= $this->render('/default/right_menu'); ?>
		</div>
		<div class="question-page">
			<div class="container-fluid background-block">
				<div class="row">
					<?= $this->render('/default/top_filter',['category_url' => $category_url]); ?>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<h2 style="text-align:center;"><b>
							<?php
							if($modelCategory != null){
								$modelCategory->name; 
							}
							?></b></h2>
					</div>
					<div class="col-sm-12">
						<div class="question-content">
							<br>
							<?php
							if (Yii::$app->session->get('credits_ok') or Yii::$app->session->get('pay_ok')){
								Yii::$app->session->set('credits_ok',null);
								Yii::$app->session->set('pay_ok',null);
								echo \yii\bootstrap\Alert::widget([
										'options' => [
												'class' => 'alert-info',
										],
										'body' => 'You pay successful response.',
								]);
								\yii\bootstrap\Modal::begin([
										'header' => 'Ful answer',
										'clientOptions' => ['show' => true],
								]);
								$answer_show = \app\modules\question\models\Answer::findOne(Yii::$app->session->get('answer_id'));
								echo $answer_show->answer_text;
								$form = ActiveForm::begin();
									echo Html::input('hidden','answer_id',Yii::$app->session->get('answer_id'));
									echo Html::submitButton('Goot',['name'=>'answer_goot']);
									echo Html::submitButton('Bat',['name'=>'answer_bat']);
								ActiveForm::end();

								\yii\bootstrap\Modal::end();
							}



							if (Yii::$app->session->get('answer_goot')){
								Yii::$app->session->set('answer_goot',null);
								\yii\bootstrap\Modal::begin([
										'header' => 'Renting',
										'clientOptions' => ['show' => true],
								]);
								$form = ActiveForm::begin();
								echo Html::input('hidden','answer_id',Yii::$app->session->get('answer_goot_id'));
								echo \kartik\rating\StarRating::widget([
										'name' => 'rating_user',
										'pluginOptions' => [
												'showClear'=>false,
												'size'=>'xs',
												'showCaption' => false,
										]
								]);
								echo Html::textarea('text');
								echo Html::submitButton('Add renting',['name'=>'add_renting']);
								ActiveForm::end();

								\yii\bootstrap\Modal::end();
								Yii::$app->session->set('answer_goot_id',null);
							}

							if (Yii::$app->session->get('not_credits')){
								Yii::$app->session->set('not_credits',null);
								echo \yii\bootstrap\Alert::widget([
										'options' => [
												'class' => 'alert-warning',
										],
										'body' => 'You do not have credits to pay for answers.',
								]);
							}

							if(Yii::$app->session->get('pay_error')){
								Yii::$app->session->set('pay_error',null);
								echo \yii\bootstrap\Alert::widget([
										'options' => [
												'class' => 'alert-warning',
										],
										'body' => 'You did not pay.',
								]);
							}

							?>
							<table class="table classBlock">
								<thead>
									<tr>
										<td>
											User
										</td>
										<td>
											Question Title
										</td>
										<td>
											Question Text
										</td>
										<td>
											Rank
										</td>
										<td>
											Rating
										</td>
										<td>
											Price
										</td>
										<td>
											Views
										</td>
										<td>
											Answers
										</td>
                                        <td>
                                            Favorite
                                        </td>
										<td>
										</td>
									</tr>
								</thead>
								<?php foreach($modelQuestion as $question){ ?>
										<tr>
											<td>
												<div class="user-img">
													<?php if($question['avatar'] != ''){ ?>
														<img src="<?= Url::home(); ?>images/users_images/<?= $question['avatar']; ?>" style='height:40px;width:40px;'>
													<?php }else{ ?>
														<img src="<?= Url::home(); ?>images/default_images.png"  style='height:40px;width:40px;'>
													<?php } ?>
												</div>
													<span class="name">
														<?= $question['name'].' '.$question['surname']; ?>
													</span>
											</td>
											<td>
												<?= $question['question_title']; ?>
											</td>
											<td>
												<?= substr($question['question_text'], 0, 40).'...'; ?>
											</td>
											<td>
												<?=\app\widgets\rank\QuestionbankrankWidget::widget(['id' => $question['user_id']])?>
											</td>
											<td>
												<?= \app\widgets\rating\QuestionbankratingWidget::widget(['object_id' => $question['user_id']])?>
											</td>
											<td>
												<?= $question['price']; ?>
											</td>
											<td>
												<?= $question['view']; ?>
											</td>
											<td>
												<?= \app\modules\question\models\Answer::find()->where(['question_id'=>$question['id']])->count(); ?>
											</td>
                                            <td>
                                                <?=\app\widgets\FavoritequestionWidget::widget(['question_id'=>$question['id']])?>
                                            </td>
											<td>
												<?= HTML::a('View','#', ['class' => 'btn btn-watch','id'=>'view', 'data-question_id'=>$question['id']]); ?>
												<div class="modalRightPagesLS" id="view_question_<?=$question['id']?>">
													<a class="btn btn-close pull-right" onclick="$('#view_question_<?=$question['id']?>').hide()">
														<span class="" aria-hidden="true">x</span>
													</a>
													<div class="user-info">
														<div class="class-user-av">
															<?php if ($question['avatar']){?>
																<img class="img-responsive" src="<?= Url::home(); ?>images/users_images/<?= $question['avatar']; ?>">
															<?php } else {?>
																<img class="img-responsive" src="<?= Url::home(); ?>images/default_avatar.jpg">
															<?php } ?>
														</div>
														<span class="user-name">
															<?= $question['name']; ?> <?= $question['surname']; ?>
														</span>
														<div class="right-block">
															<?php if ($question['user_id'] <> Yii::$app->user->id ){?>
																<a href="#apply<?=$question['id']?>" aria-controls="apply<?=$question['id']?>" role="tab" data-toggle="tab" class="btn apply btn-ripple">Apply</a>
															<?php }?>
															<span class="price"><?= $question['price']; ?>$</span>
														</div>
														<div class="clearfix"></div>
													</div>
													<div class="panel-content">

														<!-- Nav tabs -->
														<ul class="nav nav-tabs" role="tablist">
															<li role="presentation" ><a href="#info<?=$question['id']?>" aria-controls="info<?=$question['id']?>" role="tab" data-toggle="tab">Info</a></li>
															<li role="presentation" ><a href="#bids<?=$question['id']?>" aria-controls="bids<?=$question['id']?>" role="tab" data-toggle="tab">Bids</a></li>
															<li role="presentation" ><a href="#rating<?=$question['id']?>" aria-controls="rating<?=$question['id']?>" role="tab" data-toggle="tab">Rating</a></li>
														</ul>
														<!-- Tab panes -->
														<div class="tab-content">
															<div role="tabpanel" class="tab-pane active" id="info<?=$question['id']?>">
																<div class="rent-thing">
																	<div class="info">
																		<h4 class="address"><?= $question['question_title']; ?></h4>
																		<p class="content-thing"><?= $question['question_text'] ?></p>
																	</div>
																</div>
															</div>

															<div role="tabpanel" class="tab-pane" id="bids<?=$question['id']?>">
																<table class="table classBlock">
																	<thead>
																	<tr>
																		<td>
																			User
																		</td>
																		<td>
																			Rank
																		</td>
																		<td>
																			Price
																		</td>

																		<td>

																		</td>
																	</tr>
																	</thead>
																	<?php $answeres = \app\modules\question\models\Answer::find()->where(['question_id'=>$question['id']])->all()?>
																	<?php foreach($answeres as $answere){?>
																			<tr>
																				<td>
																					<div class="class-user-av">
																						<?php if ($answere->user->userinfo['avatar']){?>
																							<img class="" src="<?= Url::home(); ?>images/users_images/<?= $answere->user->userinfo['avatar']; ?>">
																						<?php } else {?>
																							<img class="" src="<?= Url::home(); ?>images/default_avatar.jpg">
																						<?php } ?>
																					</div>
																					<span class="user-name">
																						<?= $answere->user['name']; ?> <?= $answere->user['surname']; ?>
																					</span>
																				</td>
																				<td>
																					<?= \app\widgets\rating\QuestionbankratingWidget::widget(['object_id' => $answere->user_id])?>
																				</td>
																				<td><?=$answere->money?></td>
																				<td>
																					<?php if (Yii::$app->user->id == $question['user_id']){?>
																						<?php if ($answere->status == 0){?>
																						<button id="answer_pay" data-text_answer="<?=$answere['show_text']?>" data-money_answer="<?=$answere['money']?>" data-credits_answer="<?=$answere['credits']?>" data-answer_id="<?=$answere['id']?>" >View</button>
																						<?php }else{?>
																							<button id="answer_show" data-text_answer="<?=$answere['answer_text']?>" >View</button>
																						<?php }?>
																					<?php }?>
																				</td>
																			</tr>
																	<?php }?>
																</table>
															</div>
															<div role="tabpanel" class="tab-pane" id="rating<?=$question['id']?>">
																<table class="table classBlock">
																	<thead>
																	<tr>
																		<td>
																			User
																		</td>
																		<td>
																			Rating
																		</td>
																	</tr>
																	</thead>
																	<?php
																	$users = \app\models\Ratingrequest::find()->where(['othe'=>$question['id'], 'type'=> 'question_bank'])->all();
																	foreach($users as $user){
																	?>
																	<tr>
																		<td>
																			<div class="class-user-av">
																				<?php if ($user->user->userinfo['avatar']){?>
																					<img class="img-responsive" src="<?= Url::home(); ?>images/users_images/<?= $user->user->userinfo['avatar']; ?>">
																				<?php } else {?>
																					<img class="img-responsive" src="<?= Url::home(); ?>images/default_avatar.jpg">
																				<?php } ?>
																			</div>
																					<span class="user-name">
																						<?= $user->user['name']; ?> <?= $user->user['surname']; ?>
																					</span>
																		</td>
																		<td>
																			<?=
																			\kartik\rating\StarRating::widget([
																					'name' => 'rating_user',
																					'value' => $user->rating_count,
																					'pluginOptions' => [
																							'disabled'=>true,
																							'showClear'=>false,
																							'size'=>'xs',
																							'showCaption' => false,
																					]
																			]);
																			?>
																		</td>
																	</tr>
																	<?php }?>
																</table>
															</div>
															<div role="tabpanel" class="tab-pane" id="apply<?=$question['id']?>">
																<?php
																$form = ActiveForm::begin(
																		[
																			'options' => [
																				'class' => 'add-answer-form'
																			]
																		]
																	);
																	echo $form->field($answer,'answer_text')->textarea();
																	echo $form->field($answer,'show_text');
																	echo $form->field($answer,'money');
																	echo $form->field($answer,'credits');
																	echo $form->field($answer,'question_id')->hiddenInput(['value'=>$question['id']])->label(false);
																?>
																<button type="submit" class="btn btn-add btn-ripple" name="answer">Add answer</button>
																<?php
																ActiveForm::end();
																?>
															</div>
														</div>
													</div>
												</div>
											</td>
										</tr>
								<?php }?>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="showFullAnswer" class="modal padding-body fade" role="dialog" >
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">x</button>
						<h4 class="modal-title">Pay for answer</h4>
					</div>
					<div class="modal-body">
						<div class="modal-body">
							<h4 class="pay-answer-head" id="text_answer"></h4>
							<!-- <h4 class="pay-answer-head"></h4> -->
							<h4 class="pay-answer-money">For a complete answer to pay <span class="money_for_answer" id="money_answer"></span>$</h4>
							<h4 class="pay-answer-money" id="show_credits" style="display: none"><span class="money_for_answer" id="credits_answer"></span> Credits</h4>
							<?php $form = ActiveForm::begin()?>
								<input type="hidden" name="pay_answer_id" id="pay_answer_id">
								<div class="choise-pay">
									Pay via paypal:
									<button type="submit" name="pay_paypal" class="btn btn-ripple PayWidthPaypal"><span class="fa fa-cc-paypal"></span></button>
								</div>
								<div id="via_credits" style="display: none">
									Pay via credites
									<button type="submit" name="pay_credits" class="btn btn-ripple PayWidthCredits"><span class="fa fa-credit-card-alt"></span></button>
								</div>
							<?php ActiveForm::end()?>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<div id="showFullAnswerPaymend" class="modal padding-body fade" role="dialog" >
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">x</button>
						<h4 class="modal-title">Answer</h4>
					</div>
					<div class="modal-body">
						<div class="modal-body">
							<h4 class="pay-answer-head" id="text_answer_paymend"></h4>
							<!-- <h4 class="pay-answer-head"></h4> -->
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>