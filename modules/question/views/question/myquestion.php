<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\Alert;
	use yii\widgets\ActiveForm;
	use kartik\grid\GridView;
	use app\assets\QuestionAsset;
	QuestionAsset::register($this);
$this->registerJsFile('/js/ls_question.js');

?>
	<div class="row">
		<div class="question-right-panel">
			<?= $this->render('/default/right_menu'); ?>
		</div>
		<div class="question-page">
			<div class="container-fluid background-block">
				<div class="row">
					<?= $this->render('/default/top_filter',['category_url' => null]); ?>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="question-content">
							<?php
								if(Yii::$app->session->hasFlash('question_delete')):
									echo Alert::widget([
										'options' => [
											'class' => 'alert-info',
										],
										'body' => 'Question delete',
									]);
								endif; 
								if(Yii::$app->session->hasFlash('question_delete')):
									echo Alert::widget([
										'options' => [
											'class' => 'alert-error',
										],
										'body' => 'Question not delete',
									]);
								endif; 
							?>
							<?php
							if(Yii::$app->session->hasFlash('question_closed')):
								echo Alert::widget([
										'options' => [
												'class' => 'alert-info',
										],
										'body' => 'Question closed',
								]);
							endif;

							if(Yii::$app->session->hasFlash('answer_added')):
								echo Alert::widget([
										'options' => [
												'class' => 'alert-info',
										],
										'body' => 'Answer added',
								]);
							endif;
							if(Yii::$app->session->hasFlash('question_up')):
								echo Alert::widget([
										'options' => [
												'class' => 'alert-info',
										],
										'body' => 'Question update',
								]);
							endif;

							if(Yii::$app->session->hasFlash('answer_not_added')):
								echo Alert::widget([
										'options' => [
												'class' => 'alert-error',
										],
										'body' => 'Answer not added',
								]);
							endif;
							if(Yii::$app->session->hasFlash('question_save')):
								echo Alert::widget([
										'options' => [
												'class' => 'alert-info',
										],
										'body' => 'Question save',
								]);
							endif;
							if(Yii::$app->session->hasFlash('question_not_save')):
								echo Alert::widget([
										'options' => [
												'class' => 'alert-error',
										],
										'body' => 'Question not save',
								]);
							endif;
							?>
							<?php
							if (Yii::$app->session->get('credits_ok') or Yii::$app->session->get('pay_ok')){
								Yii::$app->session->set('credits_ok',null);
								Yii::$app->session->set('pay_ok',null);
								echo \yii\bootstrap\Alert::widget([
										'options' => [
												'class' => 'alert-info',
										],
										'body' => 'You pay successful response.',
								]);
								\yii\bootstrap\Modal::begin([
										'header' => 'Ful answer',
										'clientOptions' => ['show' => true],
								]);
								$answer_show = \app\modules\question\models\Answer::findOne(Yii::$app->session->get('answer_id'));
								echo $answer_show->answer_text;
								$form = ActiveForm::begin();
								echo Html::input('hidden','answer_id',Yii::$app->session->get('answer_id'));
								echo Html::submitButton('Goot',['name'=>'answer_goot']);
								echo Html::submitButton('Bat',['name'=>'answer_bat']);
								ActiveForm::end();

								\yii\bootstrap\Modal::end();
							}



							if (Yii::$app->session->get('answer_goot')){
								Yii::$app->session->set('answer_goot',null);
								\yii\bootstrap\Modal::begin([
										'header' => 'Renting',
										'clientOptions' => ['show' => true],
								]);
								$form = ActiveForm::begin();
								echo Html::input('hidden','answer_id',Yii::$app->session->get('answer_goot_id'));
								echo \kartik\rating\StarRating::widget([
										'name' => 'rating_user',
										'pluginOptions' => [
												'showClear'=>false,
												'size'=>'xs',
												'showCaption' => false,
										]
								]);
								echo Html::textarea('text');
								echo Html::submitButton('Add renting',['name'=>'add_renting']);
								ActiveForm::end();

								\yii\bootstrap\Modal::end();
								Yii::$app->session->set('answer_goot_id',null);
							}

							if (Yii::$app->session->get('not_credits')){
								Yii::$app->session->set('not_credits',null);
								echo \yii\bootstrap\Alert::widget([
										'options' => [
												'class' => 'alert-warning',
										],
										'body' => 'You do not have credits to pay for answers.',
								]);
							}

							if(Yii::$app->session->get('pay_error')){
								Yii::$app->session->set('pay_error',null);
								echo \yii\bootstrap\Alert::widget([
										'options' => [
												'class' => 'alert-warning',
										],
										'body' => 'You did not pay.',
								]);
							}

							?>
							<?= GridView::widget([
								   'dataProvider' => $modelQuestion,
								   'columns' => [
									   //'question_title',
									   [
											'label' => 'Question name',
											'format' => 'raw',
											'value' => function($modelQuestion){
												return $modelQuestion['question_title'];
											}
										],
									   'question_text',
									   'price',
									   'credits',
									   [
											'label' => 'Question category',
											'format' => 'raw',
											'value' => function($modelQuestion){
												return Html::a(
													$modelQuestion['name'],
													Url::home().'question_category/'.$modelQuestion['url_name']
												);
											}
										],
										[
											'class' => 'yii\grid\ActionColumn',
											'template' => ' {update} {delete} {updateBlock} {view}',
											'buttons' => [
												'delete' => function ($url,$modelQuestion) {
														return Html::a(
														'<span class="fa fa-times"></span>',
														'question/questiondelete?id='.$modelQuestion['id']);
												},
													'update' => function ($url,$modelQuestion) {
														return Html::a(
																'<span class="glyphicon glyphicon-pencil"></span>',
																'question/updatequestion?id='.$modelQuestion['id']);
													},
													'view' => function ($url,$modelQuestion) {
														return Html::a(
																'<span class="glyphicon glyphicon-eye-open"></span>','#',['id'=>'view', 'data-question_id'=>$modelQuestion['id']]).$this->render('/default/_modal',['obj_q'=>$modelQuestion]);
													},
											],
										],
								   ],
							   ]) ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<div id="showFullAnswer" class="modal padding-body fade" role="dialog" >
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4 class="modal-title">Pay for answer</h4>
			</div>
			<div class="modal-body">
				<div class="modal-body">
					<h4 class="pay-answer-head" id="text_answer"></h4>
					<!-- <h4 class="pay-answer-head"></h4> -->
					<h4 class="pay-answer-money">For a complete answer to pay <span class="money_for_answer" id="money_answer"></span>$</h4>
					<h4 class="pay-answer-money" id="show_credits" style="display: none"><span class="money_for_answer" id="credits_answer"></span> Credits</h4>
					<?php $form = ActiveForm::begin()?>
					<input type="hidden" name="pay_answer_id" id="pay_answer_id">
					<div class="choise-pay">
						Pay via paypal:
						<button type="submit" name="pay_paypal" class="btn btn-ripple PayWidthPaypal"><span class="fa fa-cc-paypal"></span></button>
					</div>
					<div id="via_credits" style="display: none">
						Pay via credites
						<button type="submit" name="pay_credits" class="btn btn-ripple PayWidthCredits"><span class="fa fa-credit-card-alt"></span></button>
					</div>
					<?php ActiveForm::end()?>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div id="showFullAnswerPaymend" class="modal padding-body fade" role="dialog" >
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4 class="modal-title">Answer</h4>
			</div>
			<div class="modal-body">
				<div class="modal-body">
					<h4 class="pay-answer-head" id="text_answer_paymend"></h4>
					<!-- <h4 class="pay-answer-head"></h4> -->
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>