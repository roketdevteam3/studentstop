<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\Alert;
	use yii\widgets\ActiveForm;
	use kartik\grid\GridView;
	
	use app\assets\QuestionAsset;
	QuestionAsset::register($this);
?>
	
	<div class="row">
		<div class="question-right-panel">
			<?= $this->render('/default/right_menu'); ?>
		</div>
		<div class="question-page">
			<div class="container-fluid background-block">
				<div class="row">
					<?= $this->render('/default/top_filter',['category_url' => null]); ?>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="question-content">
							<?php
								if(Yii::$app->session->hasFlash('answer_delete')):
									echo Alert::widget([
										'options' => [
											'class' => 'alert-info',
										],
										'body' => 'Answer delete',
									]);
								endif; 
								if(Yii::$app->session->hasFlash('answer_delete')):
									echo Alert::widget([
										'options' => [
											'class' => 'alert-error',
										],
										'body' => 'Answer not delete',
									]);
								endif; 
							?>
							<?= GridView::widget([
								   'dataProvider' => $modelAnswer,
								   'columns' => [
									   'show_text',
									   'answer_text',
									   'money',
									   //'question_id',
									   [
											'label' => 'Question',
											'format' => 'raw',
											'value' => function($modelAnswer){
												return $modelAnswer['question_title'];
											}
										],
									   [
											'class' => 'yii\grid\ActionColumn',
											'template' => '{delete} {update}',
											'buttons' => [
												'delete' => function ($url,$modelAnswer) {
														return Html::a(
														'<span class="fa fa-times"></span>', 
														'question/answerdelete?id='.$modelAnswer['id']);
												},
												'update' => function ($url,$modelAnswer) {
													return '<span data-money="'.$modelAnswer['money'].'" data-answer_text="'.$modelAnswer['answer_text'].'" data-show_text="'.$modelAnswer['show_text'].'" data-answer_id="'.$modelAnswer['id'].'" class="fa fa-pencil-square-o answeredit"></span>';
												},
											],
										],
								   ],
							   ]) ?>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<div id="answerUpdateModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4 class="modal-title">Change answer</h4>
			</div>
			<div class="modal-body">
				<?php $form = ActiveForm::begin(); ?>
				<input type="hidden" name="Answer[answer_id]" class="answer_id form-control">
				<div class="form-group">
					<label class="control-label">Answer Text</label>
					<textarea name="Answer[show_text]" class="show_text form-control"></textarea>
				</div>
				<div class="form-group">
					<label class="control-label">Show Text</label>
					<textarea name="Answer[answer_text]" class="answer_text form-control"></textarea>
				</div>
				<div class="form-group">
					<label class="control-label">Show Text</label>
					<input type="number" name="Answer[money]" class="money form-control">
				</div>
				<div class="form-group">
					<label class="control-label">Show Text</label>
					<input type="number" name="Answer[credits]" class="credits form-control">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
					<?= Html::submitButton( Yii::t('app', 'Save answer'), ['name'=> 'change_user_info', 'class' => 'btn btn-action']) ?>
				<?php ActiveForm::end();  ?>
			</div>
		</div>
	</div>
</div>