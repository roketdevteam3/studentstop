<?php

    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;
    use yii\bootstrap\Alert;
    use app\assets\QuestionAsset;
    QuestionAsset::register($this);
    
    $privacy_array = [1 => 'All User',2 => 'My friend',3 => 'Class mates'];

?>
<div class="col-sm-12">
    <?php
        if(Yii::$app->session->hasFlash('question_added')):
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-info',
                ],
                'body' => 'Question added',
            ]);
        endif; 
        if(Yii::$app->session->hasFlash('question_not_added')):
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-error',
                ],
                'body' => 'Question not added',
            ]);
        endif; 
    ?>
    <?= HTML::a('Question bank',Url::home().'question_bank',['class'=>'btn btn-primary','style'=>'border-radius:0px']); ?>
    
    <?= HTML::a('Post Question','#',['class'=>'btn btn-primary','style'=>'border-radius:0px','data-toggle'=>"modal", 'data-target' => "#postQuestion"]); ?>

    <div id="postQuestion" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">New Question</h4>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin([
                    'method' => 'post',
                    'action' => Url::home().'postquestion'
                ]); ?>
                <?= $form->field($modelNewQuestion, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false); ?>
                <?= $form->field($modelNewQuestion, 'question_title')->textInput(); ?>
                <?= $form->field($modelNewQuestion, 'question_text')->textarea(); ?>
                <?= $form->field($modelNewQuestion, 'price')->textInput(['type'=>'number']); ?>
                <?= $form->field($modelNewQuestion, 'category')->dropDownList($category_array); ?>
                <?= $form->field($modelNewQuestion, 'category_id')->dropDownList([]); ?>
                <?= $form->field($modelNewQuestion, 'privacy')->dropDownList($privacy_array); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <?= Html::submitButton( Yii::t('app', 'Add question'), ['name'=> 'change_user_info', 'class' => 'btn btn-primary', 'data-mr-loader'=>'show']) ?>
                <?php ActiveForm::end();  ?>
            </div>
        </div>
      </div>
    </div>

    
    <?= HTML::a('My Question',Url::home().'myquestion',['class'=>'btn btn-primary','style'=>'border-radius:0px']); ?>
    <?= HTML::a('My answer', Url::home().'myanswer',['class'=>'btn btn-primary','style'=>'border-radius:0px']); ?>
</div>
