<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\widgets\ActiveForm;
	use yii\bootstrap\Alert;
	
	use app\assets\QuestionAsset;
	QuestionAsset::register($this);
	$privacy_array = [1 => 'All User',2 => 'My friend',3 => 'Class mates',4 => 'Invite'];
$this->registerJsFile('/js/ls_question.js');
	
?>
	<div class="row">
		<div class="question-right-panel">
			<?= $this->render('right_menu',['modelNewQuestion' => $modelNewQuestion,'category_array' => $category_array]); ?>
		</div>
		<div class="question-page">
			<div class="container-fluid background-block">
				<div class="row">
					<?= $this->render('top_filter',['category_url' => null]); ?>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="post-question-form-wrap">
							<?php $form = ActiveForm::begin(); ?>
								<?= $form->field($modelNewQuestion, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false); ?>
								<?= $form->field($modelNewQuestion, 'question_title')->textInput(); ?>
								<?= $form->field($modelNewQuestion, 'question_text')->textarea(); ?>
								<?= $form->field($modelNewQuestion, 'price')->textInput(['type'=>'number']); ?>
								<?= $form->field($modelNewQuestion, 'credits')->textInput(['type'=>'number']); ?>
								<?= $form->field($modelNewQuestion, 'category')->dropDownList($category_array); ?>
								<?= $form->field($modelNewQuestion, 'category_id')->dropDownList([]); ?>
								<?= $form->field($modelNewQuestion, 'privacy')->dropDownList($privacy_array,['id'=>'privacy_select']); ?>
								<div id="users" style=" <?= ($modelNewQuestion->privacy == 4)? '': 'display:none;' ?>">
									<?php
									$users = \app\modules\users\models\User::find()->all();
									$data = [];
									foreach($users as $user){
										$data[$user->id] = $user->name.' '.$user->surname;
									}
									$res = null;

									if ($modelNewQuestion->id){
										$question_ins = \app\modules\question\models\QuestionInvite::find()->where(['question_id'=>$modelNewQuestion->id])->all();
										foreach($question_ins as $question_in){
											$res[]=$question_in->user_id;
										}
									}

									echo '<label class="control-label">User</label>';
									echo \kartik\select2\Select2::widget([
											'name' => 'users_invite',
											'value' => $res, // initial value
											'data' => $data,
											'maintainOrder' => true,
											'options' => ['placeholder' => 'Select a user ...', 'multiple' => true],
											'pluginOptions' => [
													'tags' => true,
													'maximumInputLength' => 10
											],
									]);
									?>
								</div>
								<?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'change_user_info', 'class' => 'btn add-question']) ?>
							<?php ActiveForm::end();  ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>