<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\bootstrap\ActiveForm;
    use app\widgets\QuestionfilterWidget;
?>
<div class="col-sm-12">
    <?php if($category_url){ 
        $category = '';
     } ?>
    <?= QuestionfilterWidget::widget(['category_url' => $category_url]) ?>
</div>