<?php
/**
 * Created by PhpStorm.
 * User: Listat
 * Date: 29.12.2016
 * Time: 17:20
 */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$question = \yii\helpers\ArrayHelper::toArray($obj_q);
$question_modal = \app\modules\question\models\Question::findOne($question['id']);
?>
<div class="modalRightPagesLS" id="view_question_<?=$question['id']?>">
    <a class="btn btn-close pull-right" onclick="$('#view_question_<?=$question['id']?>').hide()">
        <span class="" aria-hidden="true">x</span>
    </a>
    <div class="user-info">
        <div class="class-user-av">
            <?php if ($question_modal->user->userinfo['avatar']){?>
                <img class="img-responsive" src="<?= Url::home(); ?>images/users_images/<?= $question_modal->user->userinfo['avatar']; ?>">
            <?php } else {?>
                <img class="img-responsive" src="<?= Url::home(); ?>images/default_avatar.jpg">
            <?php } ?>
        </div>
													<span class="user-name">
														<?= $question_modal->user['name']; ?> <?= $question_modal->user['surname']; ?>
													</span>
        <div class="right-block">
            <?php if ($question['user_id'] <> Yii::$app->user->id ){?>
                <a href="#apply<?=$question['id']?>" aria-controls="apply<?=$question['id']?>" role="tab" data-toggle="tab" class="btn apply btn-ripple">Apply</a>
            <?php }?>
            <span class="price"><?= $question['price']; ?>$</span>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="panel-content">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" ><a href="#info<?=$question['id']?>" aria-controls="info<?=$question['id']?>" role="tab" data-toggle="tab">Info</a></li>
            <li role="presentation" ><a href="#bids<?=$question['id']?>" aria-controls="bids<?=$question['id']?>" role="tab" data-toggle="tab">Bids</a></li>
            <li role="presentation" ><a href="#rating<?=$question['id']?>" aria-controls="rating<?=$question['id']?>" role="tab" data-toggle="tab">Rating</a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="info<?=$question['id']?>">
                <div class="rent-thing">
                    <div class="info">
                        <h4 class="address"><?= $question['question_title']; ?></h4>
                        <p class="content-thing"><?= $question['question_title'] ?></p>
                    </div>
                </div>
            </div>

            <div role="tabpanel" class="tab-pane" id="bids<?=$question['id']?>">
                <table class="table classBlock">
                    <thead>
                    <tr>
                        <td>
                            User
                        </td>
                        <td>
                            Rank
                        </td>
                        <td>
                            Price
                        </td>

                        <td>

                        </td>
                    </tr>
                    </thead>
                    <?php $answeres = \app\modules\question\models\Answer::find()->where(['question_id'=>$question['id']])->all()?>
                    <?php foreach($answeres as $answere){?>
                        <tr>
                            <td>
                                <div class="class-user-av">
                                    <?php if ($answere->user->userinfo['avatar']){?>
                                        <img class="" src="<?= Url::home(); ?>images/users_images/<?= $answere->user->userinfo['avatar']; ?>">
                                    <?php } else {?>
                                        <img class="" src="<?= Url::home(); ?>images/default_avatar.jpg">
                                    <?php } ?>
                                </div>
																				<span class="user-name">
																					<?= $answere->user['name']; ?> <?= $answere->user['surname']; ?>
																				</span>
                            </td>
                            <td>
                                <?= \app\widgets\rating\QuestionbankratingWidget::widget(['object_id' => $answere->user_id])?>
                            </td>
                            <td><?=$answere->money?></td>
                            <td>
                                <?php if (Yii::$app->user->id == $question['user_id']){?>
                                    <?php if ($answere->status == 0){?>
                                        <button id="answer_pay" data-text_answer="<?=$answere['show_text']?>" data-money_answer="<?=$answere['money']?>" data-credits_answer="<?=$answere['credits']?>" data-answer_id="<?=$answere['id']?>" >View</button>
                                    <?php }else{?>
                                        <button id="answer_show" data-text_answer="<?=$answere['answer_text']?>" >View</button>
                                    <?php }?>
                                <?php }?>
                            </td>
                        </tr>
                    <?php }?>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="rating<?=$question['id']?>">
                <table class="table classBlock">
                    <thead>
                    <tr>
                        <td>
                            User
                        </td>
                        <td>
                            Rating
                        </td>
                    </tr>
                    </thead>
                    <?php
                    $users = \app\models\Ratingrequest::find()->where(['othe'=>$question['id'], 'type'=> 'question_bank'])->all();
                    foreach($users as $user){
                        ?>
                        <tr>
                            <td>
                                <div class="class-user-av">
                                    <?php if ($user->user->userinfo['avatar']){?>
                                        <img class="img-responsive" src="<?= Url::home(); ?>images/users_images/<?= $user->user->userinfo['avatar']; ?>">
                                    <?php } else {?>
                                        <img class="img-responsive" src="<?= Url::home(); ?>images/default_avatar.jpg">
                                    <?php } ?>
                                </div>
																				<span class="user-name">
																					<?= $user->user['name']; ?> <?= $user->user['surname']; ?>
																				</span>
                            </td>
                            <td>
                                <?=
                                \kartik\rating\StarRating::widget([
                                    'name' => 'rating_user',
                                    'value' => $user->rating_count,
                                    'pluginOptions' => [
                                        'disabled'=>true,
                                        'showClear'=>false,
                                        'size'=>'xs',
                                        'showCaption' => false,
                                    ]
                                ]);
                                ?>
                            </td>
                        </tr>
                    <?php }?>
                </table>
            </div>
        </div>
    </div>
</div>
