<?php

	use yii\helpers\Html;
	use yii\widgets\ActiveForm;
	use yii\helpers\Url;
	use app\widgets\QuestioncountWidget;
?>

	<div class="row">
		<div class="question-right-panel">
			<?= $this->render('right_menu',['modelNewQuestion' => $modelNewQuestion,'category_array' => $category_array]); ?>
		</div>
		<div class="question-page">
			<div class="container-fluid background-block">
				<div class="row">
					<?= $this->render('top_filter',['category_url' => null]); ?>
				</div>
				<div class="posts-default-index">
					<div class="">
						<div class="row">
							<?php foreach($modelCategory as $category){ ?>

                                                                <?php if($category->categories_child != null){ ?>
                                                                    <div class="col-md-4 col-sm-6">
                                                                            <div class="classified-block">
                                                                                    <h2><?= $category->name; ?></h2>
                                                                                            <?php foreach($category->categories_child as $category_child){
                                                                                                    echo HTML::a('<div class="text_style_span">'.$category_child->name.'</div>'.'<div class="badge">'.QuestioncountWidget::widget(['category_id' => $category_child->id]).'</div>','question_category/'.$category_child->url_name);
                                                                                            } ?>
                                                                            </div>
                                                                    </div>
                                                                <?php } ?>

							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
