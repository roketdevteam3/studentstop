<?php

$module_name = basename(dirname(dirname(__FILE__)));

return [
    'modules' => [
        'question' => [
            'class' => 'app\modules\question\Module',
        ],
    ],

    'components' => [
        'urlManager' => [
            'rules' => [
                'question/pay_answer'=>'question/question/pay_answer',
                'question/pay'=>'question/question/pay',
                'getchildqusetioncategory' => 'question/default/getchildqusetioncategory',
                'postquestion' => 'question/default/postquestion',
                'question_bank' => 'question/default/index',
                'question_category/<category_url:\w+>' => 'question/question/category',
                'question_category' => 'question/question/category',
                'favorite_question' => 'question/question/favorite_question',
                'add_favorite_question' => 'question/question/add_favorite_question',
                'delete_favorite_question' => 'question/question/delete_favorite_question',
                'questionshow/<question_id:\d+>' => 'question/question/questionshow',
                'myquestion' => 'question/question/myquestion',
                'questioncategory/<url_name:\w+>' => 'question/question/questioncategory',
                'question/questiondelete' => 'question/question/questiondelete',
                'myanswer' => 'question/question/myanswer',
                'question/view_count' => 'question/question/view_count',
                'getanswertext' => 'question/question/getanswertext',
                'invite_question' => 'question/question/invite_question',
                $module_name . '/<action:\w+>/<id:\d+>' => $module_name . '/default/<action>',
                $module_name . '/<action:\w+>' => $module_name . '/default/<action>',
            ],
        ],
    ],
];