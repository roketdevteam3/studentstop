<?php

namespace app\modules\question\controllers;
use app\modules\question\models\QuestionInvite;
use Yii;
use yii\web\Controller;
use app\modules\question\models\Questioncategory;
use app\modules\question\models\Question;
use app\modules\users\models\UserInfo;
use app\models\Notificationusers;
use app\models\Notification;
use yii\helpers\ArrayHelper;


class DefaultController extends Controller
{
    
    public function beforeAction($action) {
        if(!\Yii::$app->user->isGuest){
            if(Yii::$app->user->identity->getUsertUniversity()){
                $this->enableCsrfValidation = false;
                return parent::beforeAction($action);
            }else{
                return $this->redirect(Url::to("/users/choiceuniversity",true));
            }
        }else{
            return $this->redirect(Url::to("/users/login",true));
        }
    }
    
    public function actionIndex()
    {
        $modelNewCategory = new Questioncategory();
        $modelNewQuestion = new Question();
        $category_array = ArrayHelper::map(Questioncategory::find()->where(['parent_id' => 0])->all(), 'id', 'name');
        
        $modelCategory = Questioncategory::find()->where(['parent_id' => 0])->all();
        foreach($modelCategory as $category){
            $category->categories_child = Questioncategory::find()->where(['parent_id' => $category->id])->all();
        }
        
        return $this->render('index',[
            'category_array' => $category_array,
            'modelNewQuestion' => $modelNewQuestion,
            'modelNewCategory' => $modelNewCategory,
            'modelCategory' => $modelCategory,
        ]);
    }
    
    public function actionPostquestion()
    {
        $modelNewQuestion = new Question();
        $modelNewQuestion->scenario = 'add_question';
        $category_array = ArrayHelper::map(Questioncategory::find()->where(['parent_id' => 0])->all(), 'id', 'name'); 
        $modelUserInfo = UserInfo::find()->where(['id_user' => \Yii::$app->user->id])->one();
        
        if($_POST){
//            var_dump($_POST);exit;
            $request = Yii::$app->request;
            if($modelNewQuestion->load($request->post())){
                $modelNewQuestion->university_id = $modelUserInfo->university_id;
                $modelNewQuestion->country_id = $modelUserInfo->country_id;
                $modelNewQuestion->states_id = $modelUserInfo->states_id;
                if($modelNewQuestion->save()){
                    
                    $modelFriend = \app\models\user\UserFriend::find()->where(['id_user' => \Yii::$app->user->id])->asArray()->all();
                    $friendId = [];
                    foreach($modelFriend as $friend){
                        $friendId[] = $friend['id_friend'];
                    }
                    
                    $newModelNotification = new Notification();
                    $newModelNotification->scenario = 'add_notification';
                    $newModelNotification->module = 'question_bank';
                    $newModelNotification->from_object_id = \Yii::$app->user->id;
                    $newModelNotification->to_object_id = $modelNewQuestion->id;
                    $newModelNotification->university_id = 0;
                    $newModelNotification->notification_type = 'friend_add_question';
                    if($newModelNotification->save()){
                        foreach($friendId as $user_id){
                            $modelNotificationUser = new Notificationusers();
                            $modelNotificationUser->scenario = 'new_notification';
                            $modelNotificationUser->user_id = $user_id;
                            $modelNotificationUser->notification_id = $newModelNotification->id;
                            $modelNotificationUser->save();
                        }                
                    }
                    $newModelNotification = new Notification();
                    $newModelNotification->scenario = 'add_notification';
                    $newModelNotification->module = 'question_bank';
                    $newModelNotification->from_object_id = \Yii::$app->user->id;
                    $newModelNotification->to_object_id = $modelNewQuestion->id;
                    $newModelNotification->university_id = 0;
                    $newModelNotification->notification_type = 'i_add_question';
                    if($newModelNotification->save()){
                        $modelNotificationUser = new Notificationusers();
                        $modelNotificationUser->scenario = 'new_notification';
                        $modelNotificationUser->user_id = \Yii::$app->user->id;
                        $modelNotificationUser->notification_id = $newModelNotification->id;
                        $modelNotificationUser->save();
                    }
                    
                    if (isset($_POST['users_invite'])){
                        $newModelNotificationI = new Notification();
                        $newModelNotificationI->scenario = 'add_notification';
                        $newModelNotificationI->module = 'question_bank';
                        $newModelNotificationI->from_object_id = $modelNewQuestion->id;
                        $newModelNotificationI->to_object_id = $modelNewQuestion->id;
                        $newModelNotificationI->university_id = 0;
                        $newModelNotificationI->notification_type = 'invite_question';
                        if($newModelNotificationI->save()){
                        }
                        foreach($_POST['users_invite'] as $value){
                            if (!QuestionInvite::find()->where(['user_id'=>$value,'question_id'=>$modelNewQuestion->id])->one()){
                                $user_invite = new QuestionInvite();
                                $user_invite->question_id = $modelNewQuestion->id;
                                $user_invite->user_id = $value;
                                $user_invite->save();
                                
                                $modelNotificationUser = new Notificationusers();
                                $modelNotificationUser->scenario = 'new_notification';
                                $modelNotificationUser->user_id = $value;
                                $modelNotificationUser->notification_id = $newModelNotification->id;
                                $modelNotificationUser->save();
                            }
                        }
                    }
                    Yii::$app->session->setFlash('question_added');
                    return $this->redirect('/myquestion');
                }else{
                    \Yii::$app->session->setFlash('question_not_added');
                }
            }
        }
        return $this->render('post_question',[
            'modelNewQuestion' => $modelNewQuestion,
            'category_array' => $category_array,
        ]);
        
        //return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionUpdatequestion($id)
    {
        $modelNewQuestion = Question::findOne($id);
        $modelNewQuestion->scenario = 'up_question';
        $category_array = ArrayHelper::map(Questioncategory::find()->where(['parent_id' => 0])->all(), 'id', 'name');
        $modelUserInfo = UserInfo::find()->where(['id_user' => \Yii::$app->user->id])->one();

        if($_POST){
            $request = Yii::$app->request;
            if($modelNewQuestion->load($request->post())){
                $modelNewQuestion->university_id = $modelUserInfo->university_id;
                $modelNewQuestion->country_id = $modelUserInfo->country_id;
                $modelNewQuestion->states_id = $modelUserInfo->states_id;
                if($modelNewQuestion->save()){
                    
                    
                    
                    $modelAnswerApplay = \app\modules\question\models\Answer::find()->where(['question_id' => $modelNewQuestion->id])->asArray()->all();
                    $userArray = [];
                    foreach($modelAnswerApplay as $answer_a){
                        $userArray[] = $answer_a['user_id'];                   
                    }
                    $modelFavorite = \app\modules\question\models\FavoriteQuestion::find()->where(['question_id' => $modelNewQuestion->id])->asArray()->all();
                    foreach($modelFavorite as $favorite){
                        $userArray[] = $favorite['user_id'];
                    }
                    $newModelNotification = new Notification();
                    $newModelNotification->scenario = 'add_notification';
                    $newModelNotification->module = 'question_bank';
                    $newModelNotification->from_object_id = \Yii::$app->user->id;
                    $newModelNotification->to_object_id = $modelNewQuestion->id;
                    $newModelNotification->university_id = 0;
                    $newModelNotification->notification_type = 'update_question';
                    if($newModelNotification->save()){
                        foreach($userArray as $user_id){
                            $modelNotificationUser = new Notificationusers();
                            $modelNotificationUser->scenario = 'new_notification';
                            $modelNotificationUser->user_id = $user_id;
                            $modelNotificationUser->notification_id = $newModelNotification->id;
                            $modelNotificationUser->save();
                        }                
                    }
                    
                    
                    if (isset($_POST['users_invite'])){
                        $newModelNotificationI = new Notification();
                        $newModelNotificationI->scenario = 'add_notification';
                        $newModelNotificationI->module = 'question_bank';
                        $newModelNotificationI->from_object_id = $modelNewQuestion->id;
                        $newModelNotificationI->to_object_id = $modelNewQuestion->id;
                        $newModelNotificationI->university_id = 0;
                        $newModelNotificationI->notification_type = 'invite_question';
                        if($newModelNotificationI->save()){
                        }
                            
                                QuestionInvite::deleteAll(['question_id'=>$modelNewQuestion->id]);
                                foreach($_POST['users_invite'] as $value){
                                    if (!QuestionInvite::find()->where(['user_id'=>$value,'question_id'=>$modelNewQuestion->id])->one()){
                                        $user_invite = new QuestionInvite();
                                        $user_invite->question_id = $modelNewQuestion->id;
                                        $user_invite->user_id = $value;
                                        if($user_invite->save()){
                                            $modelNotificationUser = new Notificationusers();
                                            $modelNotificationUser->scenario = 'new_notification';
                                            $modelNotificationUser->user_id = $value;
                                            $modelNotificationUser->notification_id = $newModelNotificationI->id;
                                            $modelNotificationUser->save();
                                        }
                                    }
                                }
                    }
                    Yii::$app->session->setFlash('question_up');
                    return $this->redirect('/myquestion');
                }else{
                    Yii::$app->session->setFlash('question_not_added');
                }
            }
        }
        return $this->render('post_question',[
            'modelNewQuestion' => $modelNewQuestion,
            'category_array' => $category_array,
        ]);

        //return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionGetchildqusetioncategory()
    {
        $category_array = ArrayHelper::map(Questioncategory::find()->where(['parent_id' => $_POST['category_id']])->all(), 'id','url_name', 'name');
        echo json_encode($category_array);
    }
    
    
}