<?php

namespace app\modules\question\controllers;
use app\commands\Paypal;
use app\modules\question\models\FavoriteQuestion;
use app\modules\question\models\PayAnswerUser;
use app\modules\question\models\QuestionInvite;
use app\widgets\FavoritequestionWidget;
use Yii;
use yii\base\UserException;
use yii\helpers\Url;
use yii\web\Controller;
use app\modules\question\models\Questioncategory;
use app\modules\question\models\Question;
use app\modules\question\models\Answer;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\db\Query;
use app\models\Ratingrequest;
use app\models\Creditsrequest;
use app\models\Usercredits;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use app\models\Notificationusers;
use app\models\Notification;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Exception\PayPalConnectionException;
use PayPal\Rest\ApiContext;
use yii\web\HttpException;

class QuestionController extends Controller
{
    
    public function beforeAction($action) {
        if(!\Yii::$app->user->isGuest){
            if(Yii::$app->user->identity->getUsertUniversity()){
                $this->enableCsrfValidation = false;
                return parent::beforeAction($action);
            }else{
                return $this->redirect(Url::to("/users/choiceuniversity",true));
            }
        }else{
            return $this->redirect(Url::to("/users/login",true));
        }
    }
    
    public function actionIndex()
    {
        $modelNewCategory = new Questioncategory();
        $modelNewQuestion = new Question();
        $category_array = ArrayHelper::map(Questioncategory::find()->where(['parent_id' => 0])->all(), 'id', 'name');
        
        $modelCategory = Questioncategory::find()->where(['parent_id' => 0])->all();
        foreach($modelCategory as $category){
            $category->categories_child = Questioncategory::find()->where(['parent_id' => $category->id])->all();
        }
        
        return $this->render('index',[
            'category_array' => $category_array,
            'modelNewQuestion' => $modelNewQuestion,
            'modelNewCategory' => $modelNewCategory,
            'modelCategory' => $modelCategory
        ]);
    }

    public function actionInvite_question()
    {

        QuestionInvite::updateAll(['view'=>1],['user_id'=>Yii::$app->user->id]);

        $modelNewQuestion = new Question();
        $category_array = ArrayHelper::map(Questioncategory::find()->where(['parent_id' => 0])->all(), 'id', 'name');

        $query = new Query;
        $query->select(['*', 'id' => '{{%question}}.id'])
            ->from('{{%question_invite}}')
            ->join('LEFT JOIN',
                '{{%question}}',
                '{{%question}}.id = {{%question_invite}}.question_id'
            )->join('LEFT JOIN',
                '{{%user_info}}',
                '{{%user_info}}.id_user = {{%question}}.user_id'
            )->join('LEFT JOIN',
                '{{%user}}',
                '{{%user}}.id = {{%question}}.user_id')
            ->where(['{{%question}}.status' => 1,'{{%question_invite}}.user_id'=>Yii::$app->user->id]);

        $modelQuestion = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 10]]);

        $answer = new Answer();
        $answer->scenario = 'add_answer';
        $answer->user_id = Yii::$app->user->id;
        if (isset($_POST['answer'])){
            if ($answer->load(Yii::$app->request->post()) && $answer->save()){
                
                $modelAnswerApplay = Answer::find()->where(['question_id' => $answer->question_id])->asArray()->all();
                $applayUserId = [];
                foreach($modelAnswerApplay as $answer_a){
                    $applayUserId[] = $answer_a['user_id'];                   
                }
                $modelQ = Question::find()->where(['id' => $answer->question_id])->asArray()->all();
                $newModelNotification = new Notification();
                $newModelNotification->scenario = 'add_notification';
                $newModelNotification->module = 'question_bank';
                $newModelNotification->from_object_id = \Yii::$app->user->id;
                $newModelNotification->to_object_id = $answer->id;
                $newModelNotification->university_id = 0;
                $newModelNotification->notification_type = 'user_applay_to_question';
                if($newModelNotification->save()){
                    $modelNotificationUser = new Notificationusers();
                    $modelNotificationUser->scenario = 'new_notification';
                    $modelNotificationUser->user_id = $modelQ['user_id'];
                    $modelNotificationUser->notification_id = $newModelNotification->id;
                    $modelNotificationUser->save();
                    
                    foreach($applayUserId as $user_id){
                        $modelNotificationUser = new Notificationusers();
                        $modelNotificationUser->scenario = 'new_notification';
                        $modelNotificationUser->user_id = $user_id;
                        $modelNotificationUser->notification_id = $newModelNotification->id;
                        $modelNotificationUser->save();
                    }
                }
                $newModelNotification = new Notification();
                $newModelNotification->scenario = 'add_notification';
                $newModelNotification->module = 'question_bank';
                $newModelNotification->from_object_id = \Yii::$app->user->id;
                $newModelNotification->to_object_id = $answer->id;
                $newModelNotification->university_id = 0;
                $newModelNotification->notification_type = 'i_applay_to_question';
                if($newModelNotification->save()){
                    $modelNotificationUser = new Notificationusers();
                    $modelNotificationUser->scenario = 'new_notification';
                    $modelNotificationUser->user_id = \Yii::$app->user->id;
                    $modelNotificationUser->notification_id = $newModelNotification->id;
                    $modelNotificationUser->save();
                }
                
                Yii::$app->session->set('answer',true);
                $answer->answer_text = '';
                $answer->show_text = '';
                $answer->money = '';
            }
        }

        if (isset($_POST['pay_paypal'])){
            $this->redirect('/question/pay_answer?id='.$_POST['pay_answer_id'].'&redirect=invite_question');
        }

        if (isset($_POST['pay_credits'])){
            $user_credits = Usercredits::find()->where(['user_id'=>Yii::$app->user->id])->one();
            $answer_pay = Answer::findOne($_POST['pay_answer_id']);
            if ($user_credits->credit_count >= $answer_pay->credits){
                $user_credits->scenario = 'update_credits_count';
                $user_credits->credit_count = $user_credits->credit_count - $answer_pay->credits;
                $user_credits->save();

                $add_credits_user = Usercredits::find()->where(['user_id'=>$answer_pay->user_id])->one();
                $add_credits_user->scenario = 'update_credits_count';
                $add_credits_user->credit_count = $add_credits_user->credit_count + $answer_pay->credits;
                $add_credits_user->save();

                $answer_pay->scenario = 'status_change';
                $answer_pay->status = 1;
                $answer_pay->save();


                $answer_pay_user = new PayAnswerUser();
                $answer_pay_user->user_id = Yii::$app->user->id;
                $answer_pay_user->answer_id = $answer_pay->id;
                $answer_pay_user->status = 1;
                $answer_pay_user->save();

                Yii::$app->session->set('credits_ok',true);
                Yii::$app->session->set('answer_id',$answer_pay->id);

            } else {
                Yii::$app->session->set('not_credits',true);
            }
        }

        if (isset($_POST['answer_goot'])){
            $pay_answer_user = PayAnswerUser::find()
                ->where(['user_id'=>Yii::$app->user->id, 'answer_id'=>$_POST['answer_id']])
                ->one();
            $pay_answer_user->status = 1;
            $pay_answer_user->save();
            Yii::$app->session->set('answer_goot',true);
            Yii::$app->session->set('answer_goot_id',$_POST['answer_id']);
        }

        if (isset($_POST['answer_bat'])){
            $pay_answer_user = PayAnswerUser::find()
                ->where(['user_id'=>Yii::$app->user->id, 'answer_id'=>$_POST['answer_id']])
                ->one();
            $pay_answer_user->status = 2;
            $pay_answer_user->save();
        }

        if (isset($_POST['add_renting'])){
            $ans = Answer::findOne($_POST['answer_id']);
            $add_renting = new Ratingrequest();
            $add_renting->scenario = 'add_rating_request';
            $add_renting->from_user_id = Yii::$app->user->id;
            $add_renting->to_object_id = $ans->user_id;
            $add_renting->rating_count = $_POST['rating_user'];
            $add_renting->type = 'question_bank';
            $add_renting->othe = $ans->question['id'];
            $add_renting->save();
        }

        return $this->render('category',[
            'category_array' => $category_array,
            'category_url' => null,
            'modelNewQuestion' => $modelNewQuestion,
            'modelCategory' => null,
            'modelQuestion' => $modelQuestion->getModels(),
            'pagination' => $modelQuestion->pagination,
            'count' => $modelQuestion->pagination->totalCount,
            'answer' => $answer,
        ]);

    }

    public function actionCategory($category_url=null)
    {

        $modelNewQuestion = new Question();
        $category_array = ArrayHelper::map(Questioncategory::find()->where(['parent_id' => 0])->all(), 'id', 'name');

        $query = new Query;
        $query->select(['*', 'id' => '{{%question}}.id'])
            ->from('{{%question}}')
            ->join('LEFT JOIN',
                '{{%user_info}}',
                '{{%user_info}}.id_user = {{%question}}.user_id'
            )->join('LEFT JOIN',
                '{{%user}}',
                '{{%user}}.id = {{%question}}.user_id')
            ->where(['{{%question}}.status' => 1]);

        if($category_url != null){
            $modelCategory = Questioncategory::find()->where(['url_name' => $category_url])->one();
            $query->andWhere(['{{%question}}.category_id' => $modelCategory->id]);
        }else{
            $modelCategory = null;
        }
        if($_GET){
            if(isset($_GET['search_key'])){
                if($_GET['search_key'] != ''){
                    $query->where(['like','UPPER({{%question}}.question_title)',$_GET['search_key']])
                    ->orWhere(['like','UPPER({{%question}}.question_text)',$_GET['search_key']]);
                }
            }
            if(isset($_GET['university'])){
                if($_GET['university'] != ''){
                    $query->andWhere(['{{%question}}.university_id' => $_GET['university']]);
                }
            }
            if(isset($_GET['country_list'])){
                if($_GET['country_list'] != ''){
                    $query->andWhere(['{{%question}}.country_id' => $_GET['country_list']]);
                }
            }
            if(isset($_GET['states_list'])){
                if($_GET['states_list'] != ''){
                    $query->andWhere(['{{%question}}.states_id' => $_GET['states_list']]);
                }
            }
        }

        $modelQuestion = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 10]]);

        $answer = new Answer();
        $answer->scenario = 'add_answer';
        $answer->user_id = Yii::$app->user->id;
        if (isset($_POST['answer'])){
            if ($answer->load(Yii::$app->request->post()) && $answer->save()){
                Yii::$app->session->set('answer',true);
                $answer->answer_text = '';
                $answer->show_text = '';
                $answer->money = '';
            }
        }

        if (isset($_POST['pay_paypal'])){
            $this->redirect('/question/pay_answer?id='.$_POST['pay_answer_id']);
        }

        if (isset($_POST['pay_credits'])){
            $user_credits = Usercredits::find()->where(['user_id'=>Yii::$app->user->id])->one();
            $answer_pay = Answer::findOne($_POST['pay_answer_id']);
            if ($user_credits->credit_count >= $answer_pay->credits){
                $user_credits->scenario = 'update_credits_count';
                $user_credits->credit_count = $user_credits->credit_count - $answer_pay->credits;
                $user_credits->save();

                $add_credits_user = Usercredits::find()->where(['user_id'=>$answer_pay->user_id])->one();
                $add_credits_user->scenario = 'update_credits_count';
                $add_credits_user->credit_count = $add_credits_user->credit_count + $answer_pay->credits;
                $add_credits_user->save();

                $answer_pay->scenario = 'status_change';
                $answer_pay->status = 1;
                $answer_pay->save();


                $answer_pay_user = new PayAnswerUser();
                $answer_pay_user->user_id = Yii::$app->user->id;
                $answer_pay_user->answer_id = $answer_pay->id;
                $answer_pay_user->status = 1;
                $answer_pay_user->save();

                Yii::$app->session->set('credits_ok',true);
                Yii::$app->session->set('answer_id',$answer_pay->id);

            } else {
                Yii::$app->session->set('not_credits',true);
            }
        }

        if (isset($_POST['answer_goot'])){
            $pay_answer_user = PayAnswerUser::find()
                ->where(['user_id'=>Yii::$app->user->id, 'answer_id'=>$_POST['answer_id']])
                ->one();
            $pay_answer_user->status = 1;
            $pay_answer_user->save();
            Yii::$app->session->set('answer_goot',true);
            Yii::$app->session->set('answer_goot_id',$_POST['answer_id']);
        }

        if (isset($_POST['answer_bat'])){
            $pay_answer_user = PayAnswerUser::find()
                ->where(['user_id'=>Yii::$app->user->id, 'answer_id'=>$_POST['answer_id']])
                ->one();
            $pay_answer_user->status = 2;
            $pay_answer_user->save();
        }

        if (isset($_POST['add_renting'])){
            $ans = Answer::findOne($_POST['answer_id']);
            $add_renting = new Ratingrequest();
            $add_renting->scenario = 'add_rating_request';
            $add_renting->from_user_id = Yii::$app->user->id;
            $add_renting->to_object_id = $ans->user_id;
            $add_renting->rating_count = $_POST['rating_user'];
            $add_renting->type = 'question_bank';
            $add_renting->othe = $ans->question['id'];
            $add_renting->save();
        }

        return $this->render('category',[
            'category_url' => $category_url,
            'category_array' => $category_array,
            'modelNewQuestion' => $modelNewQuestion,
            'modelCategory' => $modelCategory,
            'modelQuestion' => $modelQuestion->getModels(),
            'pagination' => $modelQuestion->pagination,
            'count' => $modelQuestion->pagination->totalCount,
            'answer' => $answer,
        ]);
    }

    public function actionPay_answer($id){
        if (!Yii::$app->user->isGuest) {

            $answer = Answer::findOne($id);

            $dPrice = $answer->money;

//            $paypal = PaypalSetting::find()->where(['user_id'=>$rent->user_id])->one();

            /* if ($paypal){
                 $pay = new ApiContext(
                     new OAuthTokenCredential(
                         $paypal->client_id,
                         $paypal->secret
                     )
                 );
             } else {
                 throw new HttpException(403);
             }*/

            $pay = New Paypal();
            $pay = $pay->api;

            $pay->setConfig([
                'model'=>'sandbox',
                'http.ConnectionTimeOut'=>30,
                'log.LogEnabled'=> YII_DEBUG ? 1 : 0,
                'log.FileName'=> '',
                'log.LogLevel' => 'FINE',
                'validation.level'=>'log'
            ]);

            $payer = new Payer();
            $payer->setPaymentMethod("paypal");

            $item2 = new Item();
            $item2->setName('Payment answer')
                ->setCurrency('USD')
                ->setQuantity(1)
                ->setSku('N-C-'+time()) // Similar to `item_number` in Classic API
                ->setPrice($dPrice);
            $itemList = new ItemList();
            $itemList->setItems(array($item2));


            $details = new Details();
            $details->setShipping(0.00)
                ->setTax(0.00)
                ->setSubtotal($dPrice);


            $amount = new Amount();
            $amount->setCurrency("USD")
                ->setTotal($dPrice+0.00)
                ->setDetails($details);



            $transaction = new Transaction();
            $transaction->setAmount($amount)
                ->setItemList($itemList)
                ->setDescription('Paymend answer ')
                ->setInvoiceNumber(uniqid());

            $redirectUrls = new RedirectUrls();
            if (isset($_GET['redirect'])){
                $redirectUrls->setReturnUrl(Url::home(true) . 'question/pay?approved=true&user_paymend=' . $answer->user_id . '&answer_id=' . $id.'&redirect='.$_GET['redirect'])
                    ->setCancelUrl(Url::home(true) . 'question/pay?approved=false&user_paymend=' . $answer->user_id . '&answer_id=' . $id.'&redirect='.$_GET['redirect']);
            }else {
                $redirectUrls->setReturnUrl(Url::home(true) . 'question/pay?approved=true&user_paymend=' . $answer->user_id . '&answer_id=' . $id)
                    ->setCancelUrl(Url::home(true) . 'question/pay?approved=false&user_paymend=' . $answer->user_id . '&answer_id=' . $id);
            }

            $payment = new Payment();
            $payment->setIntent("sale")
                ->setPayer($payer)
                ->setRedirectUrls($redirectUrls)
                ->setTransactions(array($transaction));

            try{
                $payment->create($pay);
            }catch (PayPalConnectionException $e){

                throw new UserException($e->getMessage());

            }
            $trans= new \app\models\Transaction();
            $trans->user_id= Yii::$app->user->id;
            $trans->price=$dPrice;
            $trans->payment_id=$payment->getId();
            $trans->hash=md5($payment->getId());
            $trans->othe='Paymend answer';
            $trans->project_id=$id;
            $trans->save();
            var_dump( $payment->getApprovalLink());
            Yii::$app->getResponse()->redirect($payment->getApprovalLink());
        } else {
            throw new HttpException(403);
        }
    }

    public function actionPay(){
        /* if ($paypal){
             $pay = new ApiContext(
                 new OAuthTokenCredential(
                     $paypal->client_id,
                     $paypal->secret
                 )
             );
         } else {
             throw new HttpException(403);
         }*/
        $pay = New Paypal();
        $pay = $pay->api;

        if (isset($_GET['approved']) && $_GET['approved'] == 'true') {
            $paymentId = $_GET['paymentId'];
            $payment = Payment::get($paymentId, $pay);
            $execution = new PaymentExecution();
            $execution->setPayerId($_GET['PayerID']);
            try {
                $result = $payment->execute($execution, $pay);
            } catch (PayPalConnectionException $e) {
                throw new UserException($e->getMessage());
            }
            $transaction = \app\models\Transaction::find()->where(['user_id'=>Yii::$app->user->id,'hash'=>md5($paymentId)])->one();
            $transaction->complete=1;
            $transaction->save();



            $user_many = Usercredits::find()->where(['user_id'=>$_GET['user_paymend']])->one();
            if ($user_many) {
                $user_many->scenario = 'update_many_count';
                $user_many->many = $user_many->many + $transaction->price;
                $user_many->save();
            } else {
                $u_m = new Usercredits();
                $u_m->many=$transaction->price;
                $u_m->user_id = $_GET['user_paymend'];
                $u_m->credit_count = 0;
                $u_m->scenario = 'add_credits';
                $u_m->save();
            }

            Yii::$app->session['pay_ok']=true;

            $answer_cat = Answer::findOne($_GET['answer_id']);
            if (isset($_GET['redirect'])) {
                return $this->redirect('/'.$_GET['redirect']);
            } else {
                return $this->redirect('/question_category/' . $answer_cat->question->questioncat['url_name']);
            }
        } else {
            Yii::$app->session['pay_error']=true;
            $answer_cat = Answer::findOne($_GET['answer_id']);
            if (isset($_GET['redirect'])) {
                return $this->redirect('/'.$_GET['redirect']);
            } else {
                return $this->redirect('/question_category/' . $answer_cat->question->questioncat['url_name']);
            }
        }
    }
    
    public function actionQuestionshow($question_id)
    {
        $modelNewQuestion = new Question();
        $category_array = ArrayHelper::map(Questioncategory::find()->where(['parent_id' => 0])->all(), 'id', 'name');
        
        $modelQuestion = Question::find()->where(['id' => $question_id])->one();
        if($modelQuestion != null){
            $modelQuestion->scenario = 'add_question';
            $modelNewAnswer = new Answer();
            $modelNewAnswer->scenario = 'add_answer';
            if($_POST){
                $request = Yii::$app->request;
                if(isset($_POST['Rating'])){
                    $modelAnswer = Answer::find()->where([ 'id' => $_POST['Rating']['answer_id'] ])->one();
                    $modelAnswer->scenario = 'add_comment';
                    $modelRatingRequest = new Ratingrequest();
                    $modelRatingRequest->scenario = 'add_rating_request';
                    $modelRatingRequest->from_user_id = \Yii::$app->user->id;
                    $modelRatingRequest->to_object_id = $modelAnswer->user_id;
                    $modelRatingRequest->rating_count = $_POST['Rating']['rating_count'];
                    $modelRatingRequest->type = 'question_bank';
                    if($modelRatingRequest->save()){
                        $modelAnswer->comment = $_POST['Rating']['comment'];
                        if($modelAnswer->save()){
                            
                        }
                    }
                }
                
                if(isset($_POST['close_question'])){
                    $modelQuestion = Question::find()->where(['id' => $question_id])->one();
                    $modelQuestion->scenario = 'close_question';
                    $modelQuestion->status = 0;
                    if($modelQuestion->save()){
                        Yii::$app->session->setFlash('question_closed');
                    }
                }

                if(isset($_POST['Answer'])){
                    if($modelNewAnswer->load($request->post())){
                        if($modelNewAnswer->save()){
                            Yii::$app->session->setFlash('answer_added');
                        }else{
                            Yii::$app->session->setFlash('answer_not_added');
                        }
                    }
                }

                if(isset($_POST['Question'])){
                    if($modelQuestion->load($request->post())){
                        if($modelQuestion->save()){
                            Yii::$app->session->setFlash('question_save');
                        }else{
                            Yii::$app->session->setFlash('question_not_save');
                        }
                    }
                }
            }
            $query = new Query;
            $query->select(['*', 'id' => '{{%user}}.id'])
                ->from('{{%user}}')
                ->join('LEFT JOIN',
                    '{{%user_info}}',
                    '{{%user_info}}.id_user = {{%user}}.id'
                )->where(['{{%user}}.id' => $modelQuestion->user_id]);

            $command = $query->createCommand();
            $modelUserPosted = $command->queryOne();

            $query = new Query;
            $query->select(['*', 'id' => '{{%answer}}.id', 'status' => '{{%answer}}.status'])
                ->from('{{%answer}}')
                ->join('LEFT JOIN',
                    '{{%user_info}}',
                    '{{%user_info}}.id_user = {{%answer}}.user_id'
                )->join('LEFT JOIN',
                    '{{%user}}',
                    '{{%user}}.id = {{%answer}}.user_id'
                )->where(['{{%answer}}.question_id' => $question_id]);
            $command = $query->createCommand();
            $modelAnswers = $command->queryAll();

            return $this->render('question_show',[
                'category_array' => $category_array,
                'modelNewQuestion' => $modelNewQuestion,
                'modelQuestion' => $modelQuestion,
                'modelNewAnswer' => $modelNewAnswer,
                'modelAnswers' => $modelAnswers,
                'modelUserPosted' => $modelUserPosted,
            ]);
        }else{

        }
    }
    
    public function actionMyquestion()
    {
        $modelNewQuestion = new Question();
        $category_array = ArrayHelper::map(Questioncategory::find()->where(['parent_id' => 0])->all(), 'id', 'name');
        
        //$queryQuestion = Question::find()->where(['user_id' => \Yii::$app->user->id]);
        $queryQuestion = new Query;
        $queryQuestion->select(['*', 'id' => '{{%question}}.id'])
            ->from('{{%question}}')
            ->join('LEFT JOIN',
                '{{%question_category}}',
                '{{%question_category}}.id = {{%question}}.category_id'
            )->where(['{{%question}}.user_id' => \Yii::$app->user->id]);
        $modelQuestion = new ActiveDataProvider(['query' => $queryQuestion, 'pagination' => ['pageSize' => 10]]);



        if (isset($_POST['pay_paypal'])){
            $this->redirect('/question/pay_answer?id='.$_POST['pay_answer_id'].'&redirect=myquestion');
        }

        if (isset($_POST['pay_credits'])){
            $user_credits = Usercredits::find()->where(['user_id'=>Yii::$app->user->id])->one();
            $answer_pay = Answer::findOne($_POST['pay_answer_id']);
            if ($user_credits->credit_count >= $answer_pay->credits){
                $user_credits->scenario = 'update_credits_count';
                $user_credits->credit_count = $user_credits->credit_count - $answer_pay->credits;
                $user_credits->save();

                $add_credits_user = Usercredits::find()->where(['user_id'=>$answer_pay->user_id])->one();
                $add_credits_user->scenario = 'update_credits_count';
                $add_credits_user->credit_count = $add_credits_user->credit_count + $answer_pay->credits;
                $add_credits_user->save();

                $answer_pay->scenario = 'status_change';
                $answer_pay->status = 1;
                $answer_pay->save();


                $answer_pay_user = new PayAnswerUser();
                $answer_pay_user->user_id = Yii::$app->user->id;
                $answer_pay_user->answer_id = $answer_pay->id;
                $answer_pay_user->status = 1;
                $answer_pay_user->save();

                Yii::$app->session->set('credits_ok',true);
                Yii::$app->session->set('answer_id',$answer_pay->id);

            } else {
                Yii::$app->session->set('not_credits',true);
            }
        }

        if (isset($_POST['answer_goot'])){
            $pay_answer_user = PayAnswerUser::find()
                ->where(['user_id'=>Yii::$app->user->id, 'answer_id'=>$_POST['answer_id']])
                ->one();
            $pay_answer_user->status = 1;
            $pay_answer_user->save();
            Yii::$app->session->set('answer_goot',true);
            Yii::$app->session->set('answer_goot_id',$_POST['answer_id']);
        }

        if (isset($_POST['answer_bat'])){
            $pay_answer_user = PayAnswerUser::find()
                ->where(['user_id'=>Yii::$app->user->id, 'answer_id'=>$_POST['answer_id']])
                ->one();
            $pay_answer_user->status = 2;
            $pay_answer_user->save();
        }

        if (isset($_POST['add_renting'])){
            $ans = Answer::findOne($_POST['answer_id']);
            $add_renting = new Ratingrequest();
            $add_renting->scenario = 'add_rating_request';
            $add_renting->from_user_id = Yii::$app->user->id;
            $add_renting->to_object_id = $ans->user_id;
            $add_renting->rating_count = $_POST['rating_user'];
            $add_renting->type = 'question_bank';
            $add_renting->othe = $ans->question['id'];
            $add_renting->save();
        }


        return $this->render('myquestion',[
            'category_array' => $category_array,
            'modelNewQuestion' => $modelNewQuestion,
            'modelQuestion' => $modelQuestion,
        ]);
        
    }
    
    public function actionMyanswer()
    {
        $modelNewQuestion = new Question();
        $category_array = ArrayHelper::map(Questioncategory::find()->where(['parent_id' => 0])->all(), 'id', 'name');
        if($_POST){
            $modelAnswer = Answer::find()->where(['id' => $_POST['Answer']['answer_id']])->one();
            $modelAnswer->scenario = 'add_answer';
            $modelAnswer->show_text = $_POST['Answer']['show_text'];
            $modelAnswer->answer_text = $_POST['Answer']['answer_text'];
            $modelAnswer->money = $_POST['Answer']['money'];
            if($modelAnswer->save()){
                
            }
        }
        //$queryAnswer = Answer::find()->where(['user_id' => \Yii::$app->user->id]);
        $queryAnswer = new Query;
        $queryAnswer->select(['*', 'id' => '{{%answer}}.id'])
            ->from('{{%answer}}')
            ->join('LEFT JOIN',
                '{{%question}}',
                '{{%question}}.id = {{%answer}}.question_id'
            )->where(['{{%answer}}.user_id' => \Yii::$app->user->id]);
        $modelAnswer = new ActiveDataProvider(['query' => $queryAnswer, 'pagination' => ['pageSize' => 10]]);
        
        return $this->render('myanswer',[
            'category_array' => $category_array,
            'modelNewQuestion' => $modelNewQuestion,
            'modelAnswer' => $modelAnswer,
        ]);
    }
    
    public function actionQuestiondelete($id)
    {
        $queryQuestion = Question::find()->where(['id'=>$id])->one();
        if($queryQuestion->delete()){
            Yii::$app->session->setFlash('question_delete');
        }else{
            Yii::$app->session->setFlash('question_not_delete');
        }
        return $this->redirect('/myquestion');
    }
    
    public function actionGetanswertext()
    {
        $modelAnswer = Answer::find()->where(['id' => $_POST['answer_id']])->one();
        $modelUserCredits = Usercredits::find()->where(['user_id' => \Yii::$app->user->id])->one();
        
        $result = [];
        if(floatval($modelUserCredits->credit_count) >= floatval($modelAnswer->money)){
            $modelUserCredits->scenario = 'update_credits_count';
            $modelUserCredits->credit_count = floatval($modelUserCredits->credit_count) - floatval($modelAnswer->money);
            if($modelUserCredits->save()){
                $newModelCreditsrequest = new Creditsrequest();
                $newModelCreditsrequest->scenario = 'create_request';
                $newModelCreditsrequest->recepient_type = 'user';
                $newModelCreditsrequest->recepient_id = $modelAnswer->user_id;
                $newModelCreditsrequest->request_type = 'costs';
                $newModelCreditsrequest->user_id = \Yii::$app->user->id;
                $newModelCreditsrequest->credits_count = floatval($modelAnswer->money);
                $newModelCreditsrequest->modules = 'Question bank';
                $newModelCreditsrequest->save();
                
                $modelRecepientCredits = Usercredits::find()->where(['user_id' => $modelAnswer->user_id])->one();
                if($modelRecepientCredits == null){
                    $modelRecepientCredits = new Usercredits();
                    $modelRecepientCredits->scenario = 'add_credits';
                    $modelRecepientCredits->user_id = $modelAnswer->user_id;
                    $modelRecepientCredits->credit_count = floatval($modelAnswer->money);
                }else{
                    $modelRecepientCredits->scenario = 'update_credits_count';
                    $modelRecepientCredits->credit_count = floatval($modelAnswer->money) + floatval($modelRecepientCredits->credit_count);
                }
                
                if($modelRecepientCredits->save()){
                    $newModelCreditsrequestR = new Creditsrequest();
                    $newModelCreditsrequestR->scenario = 'create_request';
                    $newModelCreditsrequestR->recepient_type = 'user';
                    $newModelCreditsrequestR->recepient_id = \Yii::$app->user->id;
                    $newModelCreditsrequestR->request_type = 'purchase';
                    $newModelCreditsrequestR->user_id = $modelAnswer->user_id;
                    $newModelCreditsrequestR->credits_count = floatval($modelAnswer->money);
                    $newModelCreditsrequestR->modules = 'Question bank';
                    $newModelCreditsrequestR->save();
                }
            }
            $modelAnswer->scenario = 'status_change';
            $modelAnswer->status = '1';
            $modelAnswer->save();
            
            $result['status'] = 'pay';
            $result['answer'] = $modelAnswer->answer_text;
        }else{
            $result['status'] = 'not_pay';
        }
        
        echo json_encode($result);
    }
    
    public function actionQuestioncategory($url_name)
    {
        $categoryModel = Questioncategory::find()->where(['url_name' => $url_name])->one();
        $categoriesModel = Questioncategory::find()->where(['parent_id' => $categoryModel->id])->one();

        return $this->render('questioncategory',[
            'categoryModel' => $categoryModel,
            'categoriesModel' => $categoriesModel,
        ]);
    }

    public function actionView_count($id){
        $question = Question::findOne($id);
        $question->scenario = 'view';
        $question->view = $question->view + 1;
        $question->save();
        echo $question->view;
    }

    public function actionFavorite_question(){
        $modelNewQuestion = new Question();
        $category_array = ArrayHelper::map(Questioncategory::find()->where(['parent_id' => 0])->all(), 'id', 'name');

        $query = new Query;
        $query->select(['*', 'id' => '{{%question}}.id'])
            ->from('{{%favorite_question}}')
            ->join('LEFT JOIN',
                '{{%question}}',
                '{{%favorite_question}}.question_id = {{%question}}.id'
            )->join('LEFT JOIN',
                '{{%user_info}}',
                '{{%user_info}}.id_user = {{%question}}.user_id'
            )->join('LEFT JOIN',
                '{{%user}}',
                '{{%user}}.id = {{%question}}.user_id')
            ->where(['{{%question}}.status' => 1]);

        if($_GET){
            if(isset($_GET['search_key'])){
                if($_GET['search_key'] != ''){
                    $query->where(['like','UPPER({{%question}}.question_title)',$_GET['search_key']])
                        ->orWhere(['like','UPPER({{%question}}.question_text)',$_GET['search_key']]);
                }
            }
            if(isset($_GET['university'])){
                if($_GET['university'] != ''){
                    $query->andWhere(['{{%question}}.university_id' => $_GET['university']]);
                }
            }
            if(isset($_GET['country_list'])){
                if($_GET['country_list'] != ''){
                    $query->andWhere(['{{%question}}.country_id' => $_GET['country_list']]);
                }
            }
            if(isset($_GET['states_list'])){
                if($_GET['states_list'] != ''){
                    $query->andWhere(['{{%question}}.states_id' => $_GET['states_list']]);
                }
            }
        }

        $modelQuestion = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 10]]);

        $answer = new Answer();
        $answer->scenario = 'add_answer';
        $answer->user_id = Yii::$app->user->id;
        if (isset($_POST['answer'])){
            if ($answer->load(Yii::$app->request->post()) && $answer->save()){
                Yii::$app->session->set('answer',true);
                $answer->answer_text = '';
                $answer->show_text = '';
                $answer->money = '';
            }
        }

        if (isset($_POST['pay_paypal'])){
            $this->redirect('/question/pay_answer?id='.$_POST['pay_answer_id']);
        }

        if (isset($_POST['pay_credits'])){
            $user_credits = Usercredits::find()->where(['user_id'=>Yii::$app->user->id])->one();
            $answer_pay = Answer::findOne($_POST['pay_answer_id']);
            if ($user_credits->credit_count >= $answer_pay->credits){
                $user_credits->scenario = 'update_credits_count';
                $user_credits->credit_count = $user_credits->credit_count - $answer_pay->credits;
                $user_credits->save();

                $add_credits_user = Usercredits::find()->where(['user_id'=>$answer_pay->user_id])->one();
                $add_credits_user->scenario = 'update_credits_count';
                $add_credits_user->credit_count = $add_credits_user->credit_count + $answer_pay->credits;
                $add_credits_user->save();

                $answer_pay->scenario = 'status_change';
                $answer_pay->status = 1;
                $answer_pay->save();


                $answer_pay_user = new PayAnswerUser();
                $answer_pay_user->user_id = Yii::$app->user->id;
                $answer_pay_user->answer_id = $answer_pay->id;
                $answer_pay_user->status = 1;
                $answer_pay_user->save();

                Yii::$app->session->set('credits_ok',true);
                Yii::$app->session->set('answer_id',$answer_pay->id);

            } else {
                Yii::$app->session->set('not_credits',true);
            }
        }

        if (isset($_POST['answer_goot'])){
            $pay_answer_user = PayAnswerUser::find()
                ->where(['user_id'=>Yii::$app->user->id, 'answer_id'=>$_POST['answer_id']])
                ->one();
            $pay_answer_user->status = 1;
            $pay_answer_user->save();
            Yii::$app->session->set('answer_goot',true);
            Yii::$app->session->set('answer_goot_id',$_POST['answer_id']);
        }

        if (isset($_POST['answer_bat'])){
            $pay_answer_user = PayAnswerUser::find()
                ->where(['user_id'=>Yii::$app->user->id, 'answer_id'=>$_POST['answer_id']])
                ->one();
            $pay_answer_user->status = 2;
            $pay_answer_user->save();
        }

        if (isset($_POST['add_renting'])){
            $ans = Answer::findOne($_POST['answer_id']);
            $add_renting = new Ratingrequest();
            $add_renting->scenario = 'add_rating_request';
            $add_renting->from_user_id = Yii::$app->user->id;
            $add_renting->to_object_id = $ans->user_id;
            $add_renting->rating_count = $_POST['rating_user'];
            $add_renting->type = 'question_bank';
            $add_renting->othe = $ans->question['id'];
            $add_renting->save();
        }

        return $this->render('category',[
            'category_url' => null,
            'category_array' => $category_array,
            'modelNewQuestion' => $modelNewQuestion,
            'modelCategory' => null,
            'modelQuestion' => $modelQuestion->getModels(),
            'pagination' => $modelQuestion->pagination,
            'count' => $modelQuestion->pagination->totalCount,
            'answer' => $answer,
        ]);
    }


    public function actionAdd_favorite_question()
    {
        $modelQuestion = Question::find()->where(['id' => $_POST['question_id'] ])->asArray()->one();
        
        $model = new FavoriteQuestion();
        $model->user_id = Yii::$app->user->id;
        $model->question_id = $_POST['question_id'];
        

        
        if($model->save()){
            $newModelNotification = new Notification();
            $newModelNotification->scenario = 'add_notification';
            $newModelNotification->module = 'question_bank';
            $newModelNotification->from_object_id = \Yii::$app->user->id;
            $newModelNotification->to_object_id = $_POST['question_id'];
            $newModelNotification->university_id = 0;
            $newModelNotification->notification_type = 'add_question_to_favorite';
            if($newModelNotification->save()){
                $modelNotificationUser = new Notificationusers();
                $modelNotificationUser->scenario = 'new_notification';
                $modelNotificationUser->user_id = $modelQuestion['user_id'];
                $modelNotificationUser->notification_id = $newModelNotification->id;
                $modelNotificationUser->save();
            }
        }

        $result['status'] = 'success';
        echo json_encode($result);
    }
    public function actionDelete_favorite_question()
    {
        $question_id = $_POST['question_id'];
        
        $result['status'] = 'error';
        $modelFavorite = FavoriteQuestion::find()->where(['user_id' => \Yii::$app->user->id,'question_id'=> $question_id])->one();
        if($modelFavorite->delete()){
            $result['status'] = 'success';            
        }
        echo json_encode($result);
    }
}