<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\university\modules\majors\models\Major */

$this->title = Yii::t('app', 'Create Major');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Majors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="major-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>