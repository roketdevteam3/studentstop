<?php

namespace app\modules\university\modules\majors;

/**
 * modules module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\university\modules\courses\controllers';

    /**
     * @var string
     */
    public $defaultRoute = '/university/index';


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}