<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\university\modules\majors\models\Clases */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
        'modelClass' => 'Clases',
    ]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Clases'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="clases-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'list_users'=>$list_users,
        'list_course'=>$list_course,
        'list_majores'=>$list_majores,
    ]) ?>

</div>