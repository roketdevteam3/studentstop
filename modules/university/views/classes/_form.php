<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;

/* @var $this yii\web\View */
/* @var $model app\modules\university\modules\majors\models\Clases */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clases-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>



    <?= $form->field($model, 'professor')->dropDownList($list_users,[]) ?>

    <?= $form->field($model, 'id_course')->dropDownList($list_course,
        [
            'id'=>'mr_course_dropdown',
            'data-mr-url'=> Url::to('university/classes/ajax-get-majors-list',true),
            'data-select-replace'=>'#mr_major_dropdown'
        ]
    ) ?>

    <?= $form->field($model, 'id_major')->dropDownList([],
        [
            'id'=>'mr_major_dropdown',
        ]
    ) ?>


    <?php echo $form->field($model, 'description')->widget(Widget::className(), [
        'settings' => [
            'minHeight'        => 200,
            'imageUpload'      => Url::to(['/redactor/image-upload']),
            'imageManagerJson' => Url::to(['/redactor/images-get']),
            // 'fileManagerJson' => Url::to(['/blog/files-get']),
            // 'fileUpload' => Url::to(['/blog/file-upload']),
            'plugins'          => [
                'clips',
                'fullscreen',
                'table',
                'counter',
                'definedlinks',
                //  'filemanager',
                'fontcolor',
                'fontfamily',
                'fontsize',
                'imagemanager',
                'limiter',
                'textdirection',
                'textexpander',
                'video'
            ]
        ],
        'options'=>[
            'id'=>'pg1'
        ],
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>




</div>

