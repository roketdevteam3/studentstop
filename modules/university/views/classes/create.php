<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\university\modules\majors\models\Clases */

$this->title = Yii::t('app', 'Create Clases');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Clases'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clases-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'list_users'=>$list_users,
        'list_course'=>$list_course,
        'list_majores'=>$list_majores,
    ]) ?>

</div>