<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\university\modules\majors\models\search\ClasesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Clases');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clases-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <div class="row mr-top-pading">

        <nav class="mr-univer-nav-news">'
            <?= Html::a(Yii::t('app', 'Create Clases'), ['create'], ['class' => 'btn btn-success']) ?>
            <a href="<?= Url::to('messager',true) ?>" class="btn btn-primary">Chat</a>
        </nav>

        <?php Pjax::begin(['id' => 'mr_classes']) ?>
        <?php foreach ($dataProvider->models as $k => $item) : ?>

            <?php
            if($item->checkJoin()){
                $class_btn =  'btn-danger';
                $class_icon = 'fa-minus';
            }else{
                $class_btn =  'btn-success';
                $class_icon = 'fa-plus';
            }
            ?>

            <div class="col-lg-2 col-md-6  col-xs-12 mr-icon-group text-center">
                <nav  class="btn-group">
                    <a href="<?= Url::to("university/classes/view/{$item->id}",true) ?>#about" type="button" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                    <a href="<?= Url::to("university/classes/view/{$item->id}",true) ?>#users" type="button" class="btn btn-primary"><i class="fa fa-users "></i></a>
                    <a data-mr-event="ajax_send_click" class="btn <?= $class_btn ?>"
                             data-mr-callb-success = " if(_this.hasClass('btn-danger')){_this.removeClass('btn-danger'); _this.find('i').removeClass().addClass('fa fa-plus'); _this.addClass('btn-success'); $.mr_notification({class:'warning',content: response.msg,close_type: 'remove'}); }else{ if(_this.hasClass('btn-success')){_this.removeClass('btn-success'); _this.addClass('btn-danger'); _this.find('i').removeClass().addClass('fa fa-minus'); $.mr_notification({class:'success',content: response.msg,close_type: 'remove'})  } }"
                             data-mr-callb-error = "bootbox.alert(msg);" data-mr-url="<?= Url::to('/university/classes/join?id='.$item->id, true) ?>">
                        <i class="fa <?= $class_icon ?>"></i>
                    </a>
                </nav>
                <div class="mr-thumb-university-news  ">

                    <a href="<?= Url::to("university/classes/view/{$item->id}",true) ?>" class="img-container center-block">
                        <img src="<?= Url::to('default_img/classes-icon.png',true) ?>"  alt="<?= $item->name ?>" class="center-block">
                    </a>
                </div>

                <h4 class="text-center">
                    <?= $item->name ?>
                </h4>
                <div class="text-center"> <?= StringHelper::truncate(strip_tags($item->description),'100') ; ?>
                    <a href="<?= Url::to("university/classes/view/{$item->id}",true) ?>">(read more)</a>
                </div>
            </div>

        <?php endforeach; ?>

        <div class="col-md-12">
            <?= yii\widgets\LinkPager::widget([
                'pagination' => $dataProvider->pagination
            ]) ?>
        </div>


        <?php Pjax::end() ?>

    </div>


</div>





