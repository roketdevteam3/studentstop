<?php

use app\models\user\UserFriendRequest;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\modules\university\modules\majors\models\Clases */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Clases'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="clases-view">

    <?= $this->render('_head',['model'=>$model]) ?>




    <div class="row">


        <div class="col-lg-9 col-md-9">

            <?php Pjax::begin(['id' => 'mr_chat']) ?>
            <?php foreach ($dataProviderChat->models as $k => $item) : ?>
                <div class="bs-callout bs-callout-warning" >
                    <p><?= $item->message ?></p>
                    <hr>
                    </div>
            <?php endforeach; ?>
            <?php Pjax::end() ?>


            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model_chat, 'message')->textarea(['rows' => 2]) ?>

            <div class="form-group text-center">
                <?= Html::submitButton(Yii::t('app', 'Send'), ['class' =>  'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>

        <div class="col-lg-3 col-md-3">

            <section id="users" class="text-center">


                    <?php Pjax::begin(['id' => 'mr_users']) ?>
                    <?php foreach ($dataProviderUser->models as $k => $item) : ?>

                        <?php
                        $msg_delete = Yii::t('app','Do you want delete request to friend?');
                        $chekUsers = $item->checkFriendRequest();
                        if($chekUsers){
                            $event_friend = 'remove_friend';

                            if($chekUsers->status == UserFriendRequest::STATUS_WAIT){
                                $class_btn =  'btn-success';
                                $class_icon = 'fa-ellipsis-h';
                                $title = UserFriendRequest::getStatus($chekUsers->status);
                            }

                            if($chekUsers->status == UserFriendRequest::STATUS_APPROVED){
                                $class_btn =  'btn-danger';
                                $class_icon = 'fa-user-times';
                                $title = UserFriendRequest::getStatus($chekUsers->status);
                            }

                            if($chekUsers->status == UserFriendRequest::STATUS_DECLINED){
                                $class_btn =  'btn-danger';
                                $class_icon = 'fa-times';
                                $title = UserFriendRequest::getStatus($chekUsers->status);
                            }
                        }else{

                            $event_friend = 'add_friend';
                            $title = Yii::t('app','Add to friend');
                            $class_btn =  'btn-primary';
                            $class_icon = 'fa-user-plus';
                        }
                        ?>

                        <div class=" text-center mr-university-user-block">
                            <nav  class="btn-group">
                                <button type="button" class="btn btn-primary"><i class="fa fa-eye"></i></button>
                                <a  class="btn <?= $class_btn ?>"
                                    data-mr-event="<?= $event_friend ?>"
                                    data-mr-url="<?= Url::to('ajax-set-friend/'.$item->id, true) ?>"
                                    title="<?= $title ?>" data-mr-msg="<?= $msg_delete ?>" >
                                    <i class="fa <?= $class_icon ?>"></i>
                                </a>
                                <button type="button" class="btn btn-primary"><i class="fa fa-envelope"></i></button>
                            </nav>
                            <a href="">
                                <div class="mr-university-user-border">
                                    <i class="fa fa-user"></i>
                                </div>
                                <h3><?= $item->name ?>, <?= $item->surname ?></h3>
                            </a>
                        </div>
                    <?php endforeach; ?>

                    <div class="col-md-12">
                        <?= yii\widgets\LinkPager::widget([
                            'pagination' => $dataProviderUser->pagination
                        ]) ?>
                    </div>


                    <?php Pjax::end() ?>

            </section>

        </div>




    </div>

</div>