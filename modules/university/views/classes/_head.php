<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 27.02.16
 * Email: mackraiscms@gmail.com
 * Site: http://mackrais.tk
 */
use yii\bootstrap\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;


?>

<div class="container-fluid padding-lr mr-university-header" >
    <div class="col-md-2 padding-lr" >

        <img src="<?= Url::to('default_img/classes-icon.png',true) ?>" alt="logo" class="mr-university-logo-view">
    </div>
    <div class="col-md-10 padding-lr" style="height: 100%; ">
            <nav class="navbar navbar-default mr-university-nav-header" role="navigation">
                <div class="container-fluid padding-lr">
                    <!-- Бренд та перемикач згруповані для кращого відображення на мобільних пристроях -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <p class="navbar-brand" style="color: #fff; margin-left: 10px;"><i class="fa fa-home"></i></p>
                    </div>

                    <!-- Зібрано навігаційні лінки, форми, та інший вміст для переключення -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="<?= Url::toRoute(['/university/classes/'.$model->id]) ?>" class="hovmen" style="color: #fff;">Home</a></li>
                            <li><a href="<?= Url::toRoute(['/university/classes/'.$model->id]) ?>" class="hovmen" style="color: #fff;">About</a></li>
                            <li><a href="<?= Url::toRoute(['/university/all-news/'.$model->id]) ?>" class="hovmen" style="color: #fff;">News</a></li>
                            <li><a href="<?= Url::toRoute(['/university/all-events/'.$model->id]) ?>" class="hovmen" style="color: #fff;">Events</a></li>
                            <li><a href="<?= Url::toRoute(['/university/settings/'.$model->id.'/majors']) ?>" class="hovmen" style="color: #fff;">Settings</a></li>
                        </ul>


                        <ul class="nav navbar-nav pull-right">
                            <li>
                                <?= Html::a(Yii::t('app', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                            </li>
                            <li>
                                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                                    'class' => 'btn btn-danger',
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]) ?>
                            </li>
                        </ul>

                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav> 

            <div class="col-md-12 bgtitle">
                <h2><?= $model->name ?></h2>
                <?php if(isset($show_desc)): ?>
                    <p>
                        <?= StringHelper::truncate(strip_tags($model->description),'500') ; ?>
                    </p>
                    <a href="<?= Url::toRoute(['/university/classes/about/'.$model->id]) ?>">Read more</a>
                <?php endif; ?>
            </div>

        </div>
</div>
