<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ClassChat */

$this->title = Yii::t('app', 'Create Class Chat');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Class Chats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="class-chat-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
