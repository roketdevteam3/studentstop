<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 01.03.16
 * Time: 16:47
 * Email: mackraiscms@gmail.com
 * Site: http://mackrais.tk
 */
use yii\helpers\Url;

?>



<?php foreach ($dataProvider->models as $k =>  $item) : ?>

    <?php
    if($model_relate->checkRelate($model_u->id,$item->id,$id_course)){
        $class_btn = 'btn-danger';
        $class_icon = 'fa-minus';
    }else{
        $class_btn = 'btn-success';
        $class_icon = 'fa-plus';
    }
    ?>
    <h4>
        <button class="btn <?= $class_btn ?> mr-major-plus-btn"
                data-mr-event="ajax_send_click"
                data-mr-callb-success = " if(_this.hasClass('btn-danger')){_this.removeClass('btn-danger'); _this.find('i').removeClass().addClass('fa fa-plus'); _this.addClass('btn-success'); }else{ if(_this.hasClass('btn-success')){_this.removeClass('btn-success'); _this.addClass('btn-danger'); _this.find('i').removeClass().addClass('fa fa-minus');  } }"
                data-mr-callb-error = "bootbox.alert(msg);" data-mr-url="<?= Url::to('university/major/add-ajax-course-with-major', true) ?>"
                data-mr-param-id-major="<?= $item->id ?>"
                data-mr-param-id-course="<?= $id_course ?>"
                data-mr-param-id-university="<?= $model_u->id ?>">
            <i class="fa <?= $class_icon ?>"></i>
        </button>
        <?= $item->name ?>
    </h4>
<?php endforeach ?>




