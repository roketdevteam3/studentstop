<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Event */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
	<div id="right-panel">
		<div class="u-av">
			<img class="img-responsive" src="<?= $model->getImg() ?>" alt="logo">
		</div>
		<div class="rating-blok">
			<span class="rating-count">12/1200</span>
			<div class="rating-star">
				<i class="rating-active-star fa fa-star"></i>
				<i class="rating-active-star fa fa-star"></i>
				<i class="rating-active-star fa fa-star"></i>
				<i class="rating-active-star fa fa-star"></i>
				<i class="fa fa-star-o"></i>
			</div>
		</div>
		<div class="menu-wrapper">
			<?= $this->render('/default/left_menu',['model'=>$model_u]) ?>
		</div>
	</div>

	<div class="row university-content update-univer">
		<?= $this->render('/default/_carousel',['events_base'=>$news_carousel,'model' => $model_u]) ?>
		<div class="container-fluid background-block">
			<div class="row">
				<div class="event-news-top-block">
					<div class="btn-wrap">
						<button type="button" class="btn btn-update-univer" data-toggle="modal" data-target="#updateEvent">Update</button>
						<div id="updateEvent" class="modal padding-body big-modal fade" role="dialog">
							<div class="modal-dialog">
							<!-- Modal content-->
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
										<h4 class="modal-title">Update Event</h4>
									</div>
								<?php $form = ActiveForm::begin(
									[
										'options' => [
											'class' => 'userform'
										]
									]
								); ?>
									<div class="modal-body">
	
										<?= $form->field($model, 'place_type')->hiddenInput(['value' => 'event']) ?>
	
										<?= $form->field($model, 'place_id')->textInput() ?>
	
										<?= $form->field($model, 'owner_id')->textInput() ?>
	
										<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
	
										<?= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>
	
										<?php if ($model->slug): ?>
											<?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
										<?php endif ?>
	
										<?= $form->field($model, 'short_desc')->textarea(['rows' => 6]) ?>
	
										<?= $form->field($model, 'date_start')->widget(DateTimePicker::classname(), [
											'options' => ['placeholder' => 'Enter event time ...'],
											'pluginOptions' => [
												'autoclose' => true
											]
										]); ?>
	
										<?= $form->field($model, 'date_end')->widget(DateTimePicker::classname(), [
											'options' => ['placeholder' => 'Enter event time ...'],
											'pluginOptions' => [
												'autoclose' => true
											]
										]); ?>
	
										<?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>
	
										<?= $form->field($model, 'status')->textInput() ?>
	
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
										<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-action' : 'btn btn-action']) ?>
									</div>
									<?php ActiveForm::end(); ?>
								</div>
							</div>
						</div>
						<?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
							'class' => 'btn btn-delete',
							'data' => [
								'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
								'method' => 'post',
							],
						]) ?>
					</div>
					<h1 class="univer-upadate-head"><?= Html::encode($this->title) ?></h1>
					<?php 
						echo '<span class="date-event">'.$model->date_start.' - </span>';
						echo '<span class="date-event">'.$model->date_update.'</span>';
					?>
				</div>
				<div class="col-sm-12">
					<div class="main-content">
						<!-- Modal -->
						
						<?php //= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
						
						<?php 
							// echo $model->id.'<br>';
							// echo $model->place_type.'<br>';
							// echo $model->place_id.'<br>';
							// echo $model->owner_id.'<br>';
							echo '<p class="content">'.$model->content.'</p>';
							// echo $model->date_start.'<br>';
							// echo $model->date_update.'<br>';
							// echo $model->status.'<br>';
						?>
						<?php /* = DetailView::widget([
							'model' => $model,
							'attributes' => [
								'id',
								'place_type',
								'place_id',
								'owner_id',
								'title',
								'image',
								'slug',
								'short_desc:ntext',
								'content:ntext',
								'date_start',
								'date_end',
								'date_create',
								'date_update',
								'status',
							],
						]) ?>
						<?php */ ?>
					</div>
				</div>
			</div>
		</div>
	</div>
