<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <?php $form = ActiveForm::begin([
                'options' => [
                    'enctype' => 'multipart/form-data',
                ]
            ]); ?>

            <?= $form->field($model, 'status', [])->checkbox(['class' => 'checkbox']); ?>

            <?= $form->field($model, 'place_type')->hiddenInput(['value' => 'university'])->label(false) ?>

       <?php if($model->isNewRecord): ?>
            <?= $form->field($model, 'place_id')->hiddenInput(['value' => $model_university->id])->label(false) ?>
        <?php endif; ?>

            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'image', [])->fileInput([
                "data-preview" => "true",
            ]) ?>

            <?php if ($model->slug): ?>
                <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
            <?php endif ?>

            <?= $form->field($model, 'short_desc')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>