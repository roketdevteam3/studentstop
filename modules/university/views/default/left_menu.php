<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 27.02.16
 * Email: mackraiscms@gmail.com
 * Site: http://mackrais.tk
 */
use yii\helpers\StringHelper;
use yii\helpers\Url;

if($model->checkFollow()){
	$text_follow =  'Unfollow';
	$class_follow = 'mr-unfollow';
}else{
	$text_follow =  'Follow';
	$class_follow = '';
}

$allUrl = Yii::$app->request->url;

	if(strpos($allUrl, 'university/view')){$class = 'active';}else{$class = '';}
	if(strpos($allUrl, 'university/toprated')){$class4 = 'active';}else{$class4 = '';}
	if(strpos($allUrl, 'university/all-events')){$class1 = 'active';}else{$class1 = '';}
	if(strpos($allUrl, 'university/courses')){$class2 = 'active';}else{$class2 = '';}
	if(strpos($allUrl, 'university/members')){$class3 = 'active';}else{$class3 = '';}
	if(strpos($allUrl, 'university/about')){$class5 = 'active';}else{$class5 = '';}

?>
	<div class="university-info">
		<span><?= $model->name; ?></span><br />
		<span>Establish: <span><?= $model->year_founded; ?></span></span><br />
		<span>Address: <span><?= $model->location; ?></span></span>
	</div>

        <?php /* ?>
	<nav class="menu-right">
		<ul>
			<li class="<?= $class1; ?>"><a  href="<?= Url::toRoute(['/university/all-events/'.$model->slug]) ?>" >News & Events<span class="menu-counter active">1</span></a></li>
			<li class="<?= $class; ?>"><a href="<?= Url::toRoute(['/university/view/'.$model->slug]) ?>" >Home<span class="menu-counter"></span></a></li>
			<li class="<?= $class4; ?>"><a href="<?= Url::toRoute(['/university/toprated/'.$model->slug]) ?>" >Top rated<span class="menu-counter"></span></a></li>
			<li class="<?= $class2; ?>"><a  href="<?= Url::toRoute(['/university/courses/'.$model->slug]) ?>" >Courses<span class="menu-counter"></span></a></li>
			<li class="<?= $class3; ?>"><a  href="<?= Url::toRoute(['/university/members/'.$model->slug]) ?>" >Members<span class="menu-counter"></span></a></li>
			<li class="<?= $class5; ?>"><a  href="<?= Url::toRoute(['/university/about/'.$model->slug]) ?>" >About<span class="menu-counter"></span></a></li>
			<?php if($model->id_owner == \Yii::$app->user->id){ ?>
				<li><a href="<?= Url::toRoute(['/university/settings/'.$model->slug]) ?>">Settings</a></li>
			<?php } ?>
		</ul>
	</nav>
        <?php */ ?>