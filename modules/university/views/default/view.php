<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use app\widgets\rating\RatinguniversityWidget;
use dosamigos\tinymce\TinyMce;
use vova07\imperavi\Widget;

$this->title = Yii::t('app', 'Universities');
$this->params['breadcrumbs'][] = $this->title;
?>
<div style="display:none;">
    <div id="previews" style="width:100px !important;margin:0px !important;float:left !important;padding:10px;">
        <div class="preview" style="position:relative;width:100%;">
            <img data-dz-thumbnail style="width:100%;"/>
            <i class="fa fa-times" data-dz-remove
               style="position:absolute;top:-7px;right:-7px;font-size:25px;cursor:pointer;"></i>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100"
                 aria-valuenow="0">
                <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
            </div>
            <strong class="error text-danger" data-dz-errormessage></strong>
        </div>
    </div>
</div>
<div class="universityInfoId" style="display:none;"><?= $model->id; ?></div>
<div class="universityInfoName" style="display:none;"><?= $model->name; ?></div>
<div class="universityInfoImage" style="display:none;"><?= $model->getImg() ?></div>
<?php
$adminU = 'no';
if ($model->id_owner == \Yii::$app->user->id) {
    $adminU = 'yes';
} ?>
<div class="universityAdmin" style="display:none;"><?= $adminU; ?></div>
<div id="right-panel" style="max-height:100vh;">
    <div class="u-av" style="height:30vh;">
        <img class="img-responsive" src="<?= $model->getImg() ?>" alt="logo">
    </div>
    <div class="rating-blok" style="height:9vh;">
        <span style="position: relative; top: 5px;">Rank:</span>
        <span class="rating-count universityRatingCount"  style="position: relative; top: 5px;"></span>
        <?= RatinguniversityWidget::widget(['university_id' => $model->id]); ?>
    </div>
    <div class="menu-wrapper" style="height:61vh;">
        <?= $this->render('dashboard_right_block') ?>
    </div>
</div>
<div class="row university-content">
    <div class="container-fluid background-block">
        <div class="content-university" style="margin-top: 15px;">
            <div class="row">
                <div class="col-sm-6">
                    <div class="col-sm-12 ">
                        <div class="style-color-backgroud">
                            <div class="btn-post-wrap">
								<span class="btn-post add-post">
									Add Post
								</span>
                                <span class="btn-post add-post addPostMyPhoto">
									Add photo
								</span>
                                <span class="btn-post add-post addPostMyVideo">
									Add Video
								</span>
                                <span class="btn-post" data-toggle="modal" data-target="#eventCreateModal">
									Add event
								</span>
                                <div class="clearfix"></div>
                            </div>
                            <?php $form = ActiveForm::begin(); ?>
                            <input type='hidden' id="post-avtor_type" name='Post[avtorType]' value="0">

                            <?php $user_avatar = '/default_avatar.jpg'; ?>
                            <?php if ($modelI['avatar'] != '') { ?>
                                <?php $user_avatar = '/users_images/' . $modelI['avatar']; ?>
                            <?php } ?>

                            <?php if ($model->id_owner == \Yii::$app->user->id) { ?>
                                <div style='position:relative;'>
                                    <div class='avtorType'
                                         style='display:none;position:absolute;bottom:-40px;padding:2px;border:1px solid grey;z-index:999;background-color:white;'>
                                        <img data-id="0" src='<?= Url::home(); ?>images<?= $user_avatar; ?>'
                                             style='width:40px;border:1px solid red'>
                                        <img data-id="1" src='<?= $model->getImg() ?>' style='width:40px;'>
                                    </div>
                                </div>
                            <?php } ?>


                            <div class="">
                                <div class='avtorTypeChoise img_message'>
                                    <img src='<?= Url::home(); ?>images<?= $user_avatar; ?>'
                                         style='width:40px; height: 30px;'>
                                </div>
                                <div class="col-sm-8 col-lg-9 no-padding-right no-padding-left ">
                                    <?= $form->field($modelNewPost, 'content')->textarea(['class' => 'contentNews form-control', 'placeholder' => 'Write your news ...', 'rows' => 1])->label(false); ?>
                                    <div class="add-files btn-ripple addFilePost"></div>
                                </div>

                                <input type='hidden' name='filesInput' value="">
                                <div class=" input-group-btn">
                                    <!--style="position: relative; width: 26%;float: right; top: -40px;"-->
                                    <?= Html::submitButton(Yii::t('app', 'Post'), ['class' => 'btn btn-info ButtonAddedPost', 'style' => 'box-shadow: 0 0 10px rgba(0,0,0,0.5);']) ?>
                                </div>
                            </div>
                            <div class="previewFileBlock ">
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                    <div class="col-sm-12 filterContainerPost">
                        <div class="style-color-backgroud">

                            <ul class="nav nav-tabs">
                                <li role="presentation" class="postFilterButton active" data-filter_type="all"
                                    data-university_id="<?= $model['id']; ?>"><a href="#">All</a></li>
                                <li role="presentation" class="postFilterButton" data-filter_type="my"
                                    data-university_id="<?= $model['id']; ?>"><a href="#">My</a></li>
                                <li role="presentation" class="postFilterButton" data-filter_type="friend"
                                    data-university_id="<?= $model['id']; ?>"><a href="#">My Friend</a></li>
                                <li role="presentation" class="postFilterButton" data-filter_type="university"
                                    data-university_id="<?= $model['id']; ?>"><a href="#">University</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-12 containerPost" data-page_type="dashboard">

                    </div>


                </div>
                <div class="col-sm-6" style="padding-right: 20px; padding-left: 0px;}">
                    <?php if ($postSliderArray) { ?>
                        <div id="carousel-example-generic" class="university-carousel carousel slide"
                             data-ride="carousel">
                            <ol class="carousel-indicators">
                                <?php foreach ($postSliderArray as $key => $postslider) {
                                    // var_dump($postslider);?>
                                    <?php if ($key == 0) { ?>
                                        <li data-target="#carousel-example-generic" data-slide-to="<?= $key; ?>"
                                            class="active"></li>
                                    <?php } else { ?>
                                        <li data-target="#carousel-example-generic" data-slide-to="<?= $key; ?>"
                                            class=""></li>
                                    <?php } ?>
                                <?php } ?>
                            </ol>
                            <div class="carousel-inner" role="listbox">
                                <?php foreach ($postSliderArray as $key => $postslider) { ?>

                                    <?php $classActive = ''; ?>
                                    <?php if ($key == 0) { ?>
                                        <?php $classActive = ' active '; ?>
                                    <?php } ?>
                                    <div class="item showPostClick <?= $classActive; ?>"
                                         data-post_id="<?= $postslider['post_info']['id']; ?>">
                                        <img class="img-responsive slider-img"
                                             src="<?= Url::home(); ?>images/users_images/<?= $postslider['files']['file_src']; ?>"
                                             alt="sl1">
                                        <?php if ($postslider['post_info']['content']) { ?>
                                            <div class="carousel-caption style-slider title_in_slider">
                                                <div class="col-sm-12">
                                                    <h4 class="event-title"></h4>
                                                    <h5 class="event-short-desc">
                                                        <?= $postslider['post_info']['content']; ?>
                                                    </h5>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <a class="left carousel-control" href="#carousel-example-generic" role="button"
                               data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" role="button"
                               data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>

                    <?php }  ?>

                    <div class="rating-class-block">
                        <ul class="nav nav-tabs">
                            <li role="presentation" class="active topStudents">
                                <a>Top 5 students</a>
                            </li>
                            <li role="presentation" class="topProfessor">
                                <a>Top 5 professor</a>
                            </li>
                            <li role="presentation" class="topMajor">
                                <a>Top 5 Major</a>
                            </li>
                            <li role="presentation" class="topUniversity">
                                <a>Top 5 University</a>
                            </li>
                        </ul>
                        <div class="topClassContainer top_fix_container">

                        </div>
                    </div>
                    <div class="event-wrap">
                        <div class="col-md-10 no-padding-left">
                            <div id="paginator"></div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn calendarToday">Today</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="eventBlock">
                        <?php if ($modelEvent == null) { ?>
                            <span class="nf-event">sorry but events not found</span>
                        <?php } ?>
                        <?php foreach ($modelEvent as $event) { ?>
                            <div class="event-wrap">
                                <div class="e-img">
                                    <img class="img-responsive" src="<?= $event->image; ?>">
                                    <div class="e-calendar">
                                        <i class="fa fa-calendar"> </i> <span> <?= date('F d', strtotime($event->date_start)); ?> - <?= date('F d', strtotime($event->date_end)); ?> </span>
                                    </div>
                                </div>
                                <div class="e-info">
                                    <h1 class="e-head"><?= $event->title; ?></h1>
                                    <h4 class="e-date">
                                        <span><?= date("l", strtotime($originalDate)); ?></span><?= date("h:i A", strtotime($originalDate)); ?>
                                    </h4>
                                    <p class="e-desc"><?= $event->short_desc; ?></p>
                                    <a class="btn btn-view ViewEventModal" data-event_id="<?= $event->id; ?>">View</a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>

            <div class="modalFriendMassegeBlock col-sm-6"
                 style="display:none;height:100vh;background-color: white;position:absolute;box-shadow: 0 0 10px rgba(0,0,0,0.5);top:0;right:0;padding:10px;">
                <div class="user_info">
                    <div class="col-sm-2">
                        <img class="userAvatar" style="width:100%;">
                    </div>
                    <div class="col-sm-2 ">
                        <h6 class="username"></h6>
                    </div>
                </div>
                <?php //$form = ActiveForm::begin(); ?>
                <div class="col-sm-12" style="box-shadow: 0 0 10px rgba(0,0,0,0.5);padding:10px;">
                    <div class="col-sm-10 no-padding-right">
                        <textarea class="form-control" style="border:1px solid #e4e4e4;" rows="1"
                                  placeholder='Write your news ...'></textarea>
                        <div class="add-files btn-ripple addFilePost"></div>
                    </div>
                    <input type='hidden' name='filesInput' value="">
                    <div class="col-sm-2 no-padding-left">
                        <?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn btn-info ButtonAddedPost', 'style' => 'box-shadow: 0 0 10px rgba(0,0,0,0.5);']) ?>
                    </div>
                </div>
                <?php // ActiveForm::end(); ?>
            </div>

        </div>
    </div>
</div>

<div id="modalAddFileInComment" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color:white;border: 2px dashed #0087F7;">
        </div>
    </div>
</div>

<div class="modal fade" id="eventCreateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">x</span></button>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin([
                    'action' => Url::to('/university/event/create', true),
                    'options' => [
                        'enctype' => 'multipart/form-data',
                        'class' => 'model-form',
                        'data-mr-ajax-form' => 'true',
                        //'data-mr-events'=>'keyup,change',
                    ]
                ]); ?>
                <?= $form->field($model_event, 'type')->hiddenInput(['value' => 'event'])->label(false) ?>
                <?= $form->field($model_event, 'place_type')->hiddenInput(['value' => 'university'])->label(false) ?>
                <?= $form->field($model_event, 'place_id')->hiddenInput(['value' => $model->id])->label(false) ?>
                <?= $form->field($model_event, 'title')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model_event, 'image')->fileInput([
                    "data-preview" => "true",
                ]) ?>
                <?= $form->field($model_event, 'short_desc')->textarea(['rows' => 6]) ?>
                <?= $form->field($model_event, 'date_start')->widget(DateTimePicker::classname(), [
                    'options' => ['placeholder' => 'Enter event time ...'],
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ]); ?>
                <?= $form->field($model_event, 'date_end')->widget(DateTimePicker::classname(), [
                    'options' => ['placeholder' => 'Enter event time ...'],
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ]); ?>
                <?= $form->field($model_event, 'content')->widget(TinyMce::className(), [
                    'options' => ['rows' => 6, 'id' => 'eventsContent'],
                    'language' => 'en_GB',
                    'clientOptions' => [
                        'plugins' => [],
                        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                    ]
                ]);
                ?>
                <div class="form-group text-center">
                    <?= Html::submitButton(Yii::t('app', 'Create'), ['class' => 'btn create-avent']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>


<div style="display:none;">
    <div id="previewsFileModal" class="previews-comment-file">
        <div class="preview">
            <div class="x-button">
                <i class="fa fa-times" data-dz-remove></i>
            </div>
            <img data-dz-thumbnail/>
        </div>
    </div>
</div>
<div class="modal fade" id="modalShowPost" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" style="height: 95%; overflow: auto;" role="document">
        <div class="modal-content" style="height: 100%">
            <!--<div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            </div>-->
            <div class="modal-body modalContent" style="background-color:black;">
                <div class="col-sm-7" style="padding: 0">
                    <div class="postContent" style="background-color: white"></div>
                    <div class="postFilesBlock">
                    </div>
                </div>
                <div class="col-sm-5" style="background-color: white; position: absolute; right:0px; height: 100%">
                    <div style="border-left: 1px solid #ddd;     left: 6px;    height: 100%;  position: absolute;">
                    </div>
                    <div class="modal-header style-modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">x</span></button>
                    </div>
                    <div class="post-header">
                        <div class="avatar">
                        </div>
                        <div class="info">
                            <h4 class="username">
                            </h4>
                            <h6 class="postCreate">
                            </h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <form class="add_comment_to_post_modal style-add-commet" data-post_id="">
                        <div class="comment-area">
                            <textarea class="form-control comment-input" name="newCommentPost" rows="1"
                                      placeholder="Write your comment ..."></textarea>
                            <input type='hidden' name='commentFilesInputModal' value="">
                            <div class="add-files btn-ripple clickCommentFilesAddModal addFileCommentModal"
                                 data-post_id=""></div>
                            <div class="previewFileBlockCommentModal"></div>
                        </div>
                        <div class="btn-comment-wrap">
                            <input type="submit" class="btn add-comment" name="addComment" value="Add">
                        </div>
                    </form>
                    <div class="commentContentBlock" style="margin: 20px 15px 0px;">

                    </div>
                </div>
                <div style="clear:both;"></div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalPostMyFiles" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog"  role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">x</span></button>
            </div>
            <div class="modal-body modalContent">
            </div>
            <div class="modal-footer">
                <button class="btn addMyFileToPost" data-type_action="">Add to post</button>
            </div>
        </div>
    </div>
</div>


<div style="display:none;">
    <div id="previewsUpdate">
        <div class="preview">
            <div class="new-image-wrap">
                <i class="fa fa-times" data-dz-remove></i>
                <img class="new-image" data-dz-thumbnail/>
            </div>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100"
                 aria-valuenow="0">
                <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
            </div>
            <strong class="error text-danger" data-dz-errormessage></strong>
        </div>
    </div>
</div>
<div class="modal fade" id="modalPostUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">x</span></button>
            </div>
            <div class="modal-body modalContent">
                <input type='hidden' name='post_update_id' value="">
                <input type='hidden' name='filesInput' value="">
                <div class="comment-area">
                    <textarea class="form-control contentNews" rows="1"></textarea>
                    <div class="add-files btn-ripple addFileUpdatePost"></div>
                </div>
                <div class="btn-comment-wrap">
                    <button class="btn updatePostButton">Update</button>
                </div>
                <div class="clearfix"></div>
                <div class="previewFilesBlock">
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn SaveUpdatedPost" data-type_action="">Save post</button>
            </div>
        </div>
    </div>
</div>
	
<div class="modal fade" id="modalShowEvent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" style="height: 95%; overflow: auto;" role="document">
        <div class="modal-content" style="height: 100%">

            <div class="modal-body modalContent">
                    <div class="eventTitle"></div>
                    <div class="eventDate"></div>
                    <div class="eventShortContent" style="font-size:14px;"></div>
                    <div class="eventContent" style="font-size:14px;"></div>
            </div>
        </div>
    </div>
</div>