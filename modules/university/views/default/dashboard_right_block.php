<?php

use yii\helpers\StringHelper;
use yii\helpers\Html;
use yii\helpers\Url;

use app\assets\DatepaginatorAsset;
DatepaginatorAsset::register($this);
?>
<div style="height:10%;">
    <input class="search-maket friendSearch" type="text" placeholder="Search"  data-my_id="<?= \Yii::$app->user->id?>">
</div>
<input type="hidden" name="my_input_id" value="<?= \Yii::$app->user->id; ?>">
<div style="height:10%;">

	<ul class="nav nav-tabs fiterBlock">
		<li role="presentation" data-action="friend" class="active filterFriend">
			<a href="#">Friends</a>
		</li>
		<li role="presentation" data-action="classmates" class="filterClassmates">
			<a href="#">Classmates</a>
		</li>
		<li role="presentation" data-action="alumni"  class="filterAlumni">
			<a href="#">Alumni</a>
		</li>
	</ul>
</div>

<div class=" scrollbar" id="style-3" style="height:70%;width:auto;" >
    <div class="friendSearchBlock">
            <?php /* foreach($modelUserFriend as $key => $user){ ?>
                    <div class="row startChat startChatFriend" data-sender-id="<?=\Yii::$app->user->id?>" data-recipient-id="<?= $user['id']?>" style="padding:10px;border-bottom:1px solid #60607d">
                            <div class="col-sm-3" style="padding-right:0px;position:relative;">
                                    <div class="avatar-chat-wrap">
                                            <?php if($user['avatar'] != ''){ ?>
                                                    <img class="" src="<?= Url::home(); ?>images/users_images/<?= $user['avatar']; ?>" class="friendImg" style="">
                                            <?php }else{ ?>
                                                    <img class="" src="<?= Url::home(); ?>images/default_avatar.jpg" class="friendImg" style="">
                                            <?php } ?>
                                    </div>
                            </div>
                            <div class="col-sm-9">
                                    <h6 style="color:white;position: relative; top: 6px;margin-left: 4px;font-size: 13px;">
                                            <?= $user['name'].' '.$user['surname']; ?>
                                    </h6>
                            </div>
                    </div>
            <?php } */ ?>
    </div>
</div>