<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use vova07\imperavi\Widget;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UniversitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Universities');
$this->params['breadcrumbs'][] = $this->title;
?>

	<div id="right-panel">
		<div class="u-av">
			<img class="img-responsive" src="<?= $model->getImg() ?>" alt="logo">
		</div>
		<div class="rating-blok">
			<span class="rating-count">12/1200</span>
			<div class="rating-star">
				<i class="rating-active-star fa fa-star"></i>
				<i class="rating-active-star fa fa-star"></i>
				<i class="rating-active-star fa fa-star"></i>
				<i class="rating-active-star fa fa-star"></i>
				<i class="fa fa-star-o"></i>
			</div>
		</div>
		<div class="menu-wrapper">
			<?= $this->render('left_menu',['model'=>$model]) ?>
		</div>
	</div>
	<div class="row university-content">
		<?= $this->render('_carousel',['events_base'=>$news_carousel,'model' => $model]) ?>
		<div class="container-fluid background-block">
			<div class="row">
				<ul class="nav nav-tabs rated-tab-ul" id="myTab">
					<li class="rated-tab active"><a data-target="#proffessor" data-toggle="tab"><h3 class="rated-heading">Top rated professors</h3></a></li>
					<li class="rated-tab"><a data-target="#major" data-toggle="tab"><h3 class="rated-heading">Top rated class in your major</h3></a></li>
					<li class="rated-tab"><a data-target="#student" data-toggle="tab"><h3 class="rated-heading">Top rated student in your major</h3></a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="proffessor">
						<div class="rated-wrap">
							<ul class="rated-list">
								<li>
									<span class="rated-user">user_1</span>
									<div class="rating-star">
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
									</div>
								</li>
								<li>
									<span class="rated-user">user_1</span>
									<div class="rating-star">
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="fa fa-star"></i>
									</div>
								</li>
								<li>
									<span class="rated-user">user_1</span>
									<div class="rating-star">
										<i class="rating-active-star fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
									</div>
								</li>
								<li>
									<span class="rated-user">user_1</span>
									<div class="rating-star">
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="fa fa-star"></i>
									</div>
								</li>
								<li>
									<span class="rated-user">user_1</span>
									<div class="rating-star">
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="fa fa-star"></i>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="tab-pane" id="major">
						<div class="rated-wrap">
							<ul class="rated-list">
								<li>
									<span class="rated-user">user_1</span>
									<div class="rating-star">
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
									</div>
								</li>
								<li>
									<span class="rated-user">user_1</span>
									<div class="rating-star">
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
									</div>
								</li>
								<li>
									<span class="rated-user">user_1</span>
									<div class="rating-star">
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="fa fa-star"></i>
									</div>
								</li>
								<li>
									<span class="rated-user">user_1</span>
									<div class="rating-star">
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
									</div>
								</li>
								<li>
									<span class="rated-user">user_1</span>
									<div class="rating-star">
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="fa fa-star"></i>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="tab-pane" id="student">
						<div class="rated-wrap">
							<ul class="rated-list">
								<li>
									<span class="rated-user">user_1</span>
									<div class="rating-star">
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="fa fa-star"></i>
									</div>
								</li>
								<li>
									<span class="rated-user">user_1</span>
									<div class="rating-star">
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="fa fa-star"></i>
									</div>
								</li>
								<li>
									<span class="rated-user">user_1</span>
									<div class="rating-star">
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="fa fa-star"></i>
									</div>
								</li>
								<li>
									<span class="rated-user">user_1</span>
									<div class="rating-star">
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="fa fa-star"></i>
									</div>
								</li>
								<li>
									<span class="rated-user">user_1</span>
									<div class="rating-star">
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="rating-active-star fa fa-star"></i>
										<i class="fa fa-star"></i>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

