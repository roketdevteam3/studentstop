<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UniversitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Universities');
$this->params['breadcrumbs'][] = $this->title;
?>

<h2>University list</h2>

    <div class="row">


    <?php Pjax::begin(['id' => 'mr_members']) ?>
    <?php foreach ($dataProvider->models as $k => $item): ?>

        <?php
            if($item->checkFollow()){
                $class_btn =  'btn-danger';
                $class_icon = 'fa-eye-slash';
            }else{
                $class_btn =  'btn-primary';
                $class_icon = 'fa-eye';
            }
        ?>

        <div class="col-lg-3 col-md-6  col-xs-12 text-center mr-university-user-block">
            <nav class="btn-group mr-nav-index-univer text-left">
                <button type="button" class="btn <?= $class_btn ?>"
                        data-mr-event="ajax_send_click"
                        data-mr-callb-success = " if(_this.hasClass('btn-danger')){_this.removeClass('btn-danger'); _this.find('i').removeClass().addClass('fa fa-eye'); _this.addClass('btn-primary'); }else{ if(_this.hasClass('btn-primary')){_this.removeClass('btn-primary'); _this.addClass('btn-danger'); _this.find('i').removeClass().addClass('fa fa-eye-slash');  } }"
                        data-mr-callb-error = "bootbox.alert(msg);" data-mr-url="<?= Url::to('/university/follow', true) ?>"
                        data-mr-param-id="<?= $item->id ?>">
                    <i class="fa <?= $class_icon ?>"></i>
                </button>
            </nav>
            <a href="<?= Url::to("university/view/{$item->slug}",true) ?>">
                <div class="mr-university-user-border">
                    <img src="<?= $item->getImg() ?>" alt="<?= $item->name ?>" class="img-responsive">
                </div>
                <h3><?= $item->name ?></h3>
            </a>
                <div class="text-center">
                    <?= StringHelper::truncate(strip_tags($item->history),'100') ; ?>
                    <a href="<?= Url::to("university/news/view/{$item->slug}",true) ?>">(read more)</a>
                </div>

        </div>

    <?php endforeach ?>
    <div class="col-md-12">
        <?= yii\widgets\LinkPager::widget([
            'pagination' => $dataProvider->pagination
        ]) ?>
    </div>

    <?php Pjax::end() ?>

    </div>