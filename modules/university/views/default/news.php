<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 29.02.16
 * Time: 16:02
 * Email: mackraiscms@gmail.com
 * Site: http://mackrais.tk
 */
use yii\bootstrap\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;

?>



<div class="row">

    <?= $this->render('_head',['model'=>$model, 'show_desc'=>true]) ?>
   <nav class="mr-univer-nav-news">
       <?= Html::a(Yii::t('app', 'Add news'),Url::to('university/news/create?id='.$model->id,true), ['class' => 'btn btn-success']) ?>
   </nav>

    <?php Pjax::begin(['id' => 'mr_groups']) ?>
    <?php foreach ($dataProvider->models as $k => $item) : ?>

        <div class="col-lg-2 col-md-6  col-xs-12 mr-icon-group">
            <div class="mr-thumb-university-news  ">
                <a href="<?= Url::to("university/news/view/{$item->slug}",true) ?>" class="img-container center-block">
                    <img src="<?= $searchModel->getImg($item->id) ?>"  alt="<?= $item->title ?>" class="center-block">
                </a>
            </div>

            <h4 class="text-center">
                <?= $item->title ?>
            </h4>
            <div class="text-center"> <?= StringHelper::truncate(strip_tags($item->short_desc),'100') ; ?>
                <a href="<?= Url::to("university/news/view/{$item->slug}",true) ?>">(read more)</a>
            </div>
        </div>

    <?php endforeach; ?>

    <div class="col-md-12">
        <?= yii\widgets\LinkPager::widget([
            'pagination' => $dataProvider->pagination
        ]) ?>
    </div>


    <?php Pjax::end() ?>

</div>

