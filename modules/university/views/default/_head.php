<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 27.02.16
 * Email: mackraiscms@gmail.com
 * Site: http://mackrais.tk
 */
use yii\helpers\StringHelper;
use yii\helpers\Url;

if($model->checkFollow()){
    $text_follow =  'Unfollow';
    $class_follow = 'mr-unfollow';
}else{
    $text_follow =  'Follow';
    $class_follow = '';
}
?>

<div class="container-fluid padding-lr mr-university-header" >
    <div class="col-md-2 padding-lr" >

        <img src="<?= $model->getImg() ?>" alt="logo" class="mr-university-logo-view">
    </div>
    <div class="col-md-10 padding-lr" style="height: 100%; ">
            <nav class="navbar navbar-default mr-university-nav-header" role="navigation">
                <div class="container-fluid padding-lr">
                    <!-- Бренд та перемикач згруповані для кращого відображення на мобільних пристроях -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <p class="navbar-brand" style="color: #fff; margin-left: 10px;"><i class="fa fa-home"></i></p>
                    </div>

                    <!-- Зібрано навігаційні лінки, форми, та інший вміст для переключення -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="<?= Url::toRoute(['/university/view/'.$model->slug]) ?>" class="hovmen" style="color: #fff;">Home</a></li>
                            <li><a href="<?= Url::toRoute(['/university/about/'.$model->slug]) ?>" class="hovmen" style="color: #fff;">About</a></li>
                            <li><a href="<?= Url::toRoute(['/university/all-news/'.$model->slug]) ?>" class="hovmen" style="color: #fff;">News</a></li>
                            <li><a href="<?= Url::toRoute(['/university/all-events/'.$model->slug]) ?>" class="hovmen" style="color: #fff;">Events</a></li>
                            <li><a href="#" class="hovmen" style="color: #fff;">Top rated</a></li>
                            <li><a href="#" class="hovmen" style="color: #fff;">Courses</a></li>
                            <li><a href="<?= Url::toRoute(['/university/members/'.$model->slug]) ?>" class="hovmen" style="color: #fff;">Members</a></li>
                            <li><a href="<?= Url::toRoute(['/university/settings/'.$model->slug.'/majors']) ?>" class="hovmen" style="color: #fff;">Settings</a></li>
                        </ul>

                          <div class="followerr">

                              <a href="#" class="btn foll mr-university-folower-btn <?= $class_follow ?>"
                                 data-mr-event="ajax_send_click"
                                 data-mr-callb-success = "  var $badge = $('#mr-notiffolowers-count').clone();  _this.toggleClass(''+response.btn_class+''); $badge.text(response.count);  _this.text(response.btn_title);   _this.prepend($badge);"
                                 data-mr-callb-error = "bootbox.alert(msg);" data-mr-url="<?= Url::to('/university/follow', true) ?>"
                                 data-mr-param-id="<?= $model->id ?>">

                                  <span id="mr-notiffolowers-count" class="badge"><?= $model->getCountFollowers() ?></span>

                                  <?= $text_follow ?>
                              </a>
                          </div>

                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav> 

            <div class="col-md-12 bgtitle">
                <h2><?= $model->name ?></h2>
                <?php if(isset($show_desc)): ?>
                    <p>
                        <?= StringHelper::truncate(strip_tags($model->history),'500') ; ?>
                    </p>
                    <a href="<?= Url::toRoute(['/university/about/'.$model->slug]) ?>">Read more</a>
                <?php endif; ?>
            </div>

        </div>
</div>
