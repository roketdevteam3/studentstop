<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 25.02.16
 * Time: 21:44
 * Email: mackraiscms@gmail.com
 * Site: http://mackrais.tk
 */
use yii\helpers\StringHelper;
use yii\helpers\Url;
?>


    <?php if(!empty($news_carousel)): ?>


    <div class="col-md-12 stule-link-news padding-lr">
        <div class="header-news">
            <h4 style="color: #fff;">All news</h4>
        </div>
    </div>
        
          <?php 
                $number_blocks = 6;
                $count_news = count($news_carousel); 
                $count_news_items = ceil($count_news / $number_blocks);
          ?>


    <div class="container-fluid" style="margin-top: 35px; margin-bottom: 35px;">
        <div id="carousel-university-news" class="carousel slide" data-ride="carousel">

             <!-- Indicators -->
            <ol class="carousel-indicators">
            <?php for($i=0; $i < $count_news_items; $i++): ?>
                     <?php if($i == 0) :?>
                    <li data-target="#carousel-university-news" data-slide-to="<?= $i ?>" 
                    class="active"></li>
                <?php else: ?>
                     <li data-target="#carousel-university-news" data-slide-to="<?= $i ?>" ></li>
                <?php endif; ?>
            <?php endfor; ?>
            </ol>


            <!-- Обгортка для слайдів -->
            <div class="carousel-inner" role="listbox">

          <?php for($i=1; $i < $count_news_items + 1; $i++): ?>
              <?php 
                $class_carousel_news = ''; 
                $limit_start = $i > 1 ? ($i * $number_blocks) - $number_blocks : 0;
                $limit_end = $i > 1 ?  $limit_end * 2 : $number_blocks;
                if($i == 1) {$class_carousel_news = 'active';}
                    
                if($i == $count_news_items){
                    $limit_end = $limit_start + ($count_news - (($count_news_items - 1) * $number_blocks));
                }
                
            ?>

                <div class="item <?= $class_carousel_news ?>">
  <?php for($k=$limit_start; $k < $limit_end; $k++): ?>

                    <div class="col-md-2">
                        <img src="<?= $news_carousel[$k]->getImg() ?>" alt="img" style="width: 100%;">
                        <h4 style="word-break: break-all;"><?= $news_carousel[$k]->title ?></h4>
                        <p style="word-break: break-all;">
                        	 <?= StringHelper::truncate(strip_tags($news_carousel[$k]->short_desc),'85'); ?>
                        	  <a href="<?= Url::to('university/news/view/'.$news_carousel[$k]->slug,true); ?>"> (read more)</a>
                        </p>
                    </div>
     
                      <?php endfor; ?>
                </div>

  <?php endfor; ?>


    </div>

          <?php if($count_news_items > 1): ?>
            <!-- Управління -->
            <a class="left carousel-control" href="#carousel-university-news" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Preview</span>
            </a>
            <a class="right carousel-control" href="#carousel-university-news" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
          <?php endif ?>

        </div>
    </div>
<?php endif; ?>

