<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 01.03.16
 * Time: 10:53
 * Email: mackraiscms@gmail.com
 * Site: http://mackrais.tk
 */
use kartik\datetime\DateTimePicker;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;
use vova07\imperavi\Widget;
use dosamigos\tinymce\TinyMce;
use app\assets\DatepaginatorAsset;
DatepaginatorAsset::register($this);

?>
<div style="display:none">
  <div class="table table-striped previewTemplateQ" >

		  <div id="template" class="file-row">
			<!-- This is used as the file preview template -->
			<div>
				<span class="preview"><img data-dz-thumbnail /></span>
			</div>
			<div>
				 <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
				   <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
				 </div>
			 </div>
			<div>
				<strong class="error text-danger" data-dz-errormessage></strong>
			</div>
			<div>
			  <div class="eventButton" style="display:none;">
				  <a data-dz-remove class="btn btn-danger delete">
					<i class="glyphicon glyphicon-trash"></i>
					<span>Delete</span>
				  </a>
			  </div>
			</div>
		  </div>
  </div>
</div>

	<div id="right-panel">
		<div class="u-av">
			<img class="img-responsive" src="<?= $model->getImg() ?>" alt="logo">
		</div>
		<div class="rating-blok">
			<span class="rating-count">12/1200</span>
			<div class="rating-star">
				<i class="rating-active-star fa fa-star"></i>
				<i class="rating-active-star fa fa-star"></i>
				<i class="rating-active-star fa fa-star"></i>
				<i class="rating-active-star fa fa-star"></i>
				<i class="fa fa-star-o"></i>
			</div>
		</div>
		<div class="menu-wrapper">
			<?= $this->render('left_menu',['model'=>$model]) ?>
		</div>
	</div>

	<div class="row university-content">
            <input type="hidden" class="university_slug" value="<?= $model->slug; ?>">
		<?= $this->render('_carousel',['events_base'=>$news_carousel,'model' => $model]) ?>
		<div class="container-fluid background-block">
			<div class="row">
				<div class="container-fluid">
					<div class="event-wrap" style="margin-top:15px;">
						<div class="row">
							<div class="col-md-10">
								<div id="paginator"></div>
							</div>
							<div class="col-md-2">
								<button class="btn calendarToday">Today</button>
							</div>
						</div>
					</div>
					<div class="add-event-wrap">
						<div class="row">
							<div class="col-sm-12">
								<span class="today"><?= date('F d'); ?></span>
								<span class="add-event pull-right">
									<i class="fa fa-plus-circle fa-2x" data-toggle="modal" data-target="#eventCreateModal"></i>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="eventBlock">
				<?php foreach($modelEvent as $event){ ?>
					<div class="event-wrap">
						<div class="e-img">
							<img class="img-responsive" src="<?= $event->image; ?>">
							<div class="e-calendar">
								<i class="fa fa-calendar"> </i> <span> <?= date('F d'); ?> </span>
							</div>
						</div>
						<div class="e-info">
							<h4 class="e-date"><span><?= date("l", strtotime($originalDate));?></span><?= date("h:i A", strtotime($originalDate));?></h4>
							<h1 class="e-head"><?= $event->title; ?></h1>
							<p class="e-desc"><?= $event->short_desc; ?></p>
                            <a href="../../../university/event/view/<?= $event->slug; ?>/<?= $model->slug; ?>" class="btn btn-view">View</a>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>


	<div class="modal fade" id="eventCreateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#event" aria-controls="home" role="tab" data-toggle="tab">Event</a></li>
						<li role="presentation"><a href="#news" aria-controls="profile" role="tab" data-toggle="tab">News</a></li>
					</ul>
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="event">
							<?php $form = ActiveForm::begin([
								'action' => Url::to( '/university/event/create', true),
								'options' => [
									'enctype' => 'multipart/form-data',
									'class' => 'model-form',
									'data-mr-ajax-form'=>'true',
									//'data-mr-events'=>'keyup,change',
								]
							]); ?>
							<?= $form->field($model_event, 'type')->hiddenInput(['value' => 'event'])->label(false) ?>
							<?= $form->field($model_event, 'place_type')->hiddenInput(['value' => 'university'])->label(false) ?>
							<?= $form->field($model_event, 'place_id')->hiddenInput(['value' => $model->id])->label(false) ?>
							<?= $form->field($model_event, 'title')->textInput(['maxlength' => true]) ?>
							<?= $form->field($model_event, 'image')->fileInput([
								"data-preview" => "true",
							]) ?>
							<?= $form->field($model_event, 'short_desc')->textarea(['rows' => 6]) ?>
							<?= $form->field($model_event, 'date_start')->widget(DateTimePicker::classname(), [
								'options' => ['placeholder' => 'Enter event time ...'],
								'pluginOptions' => [
									'autoclose' => true
								]
							]); ?>
							<?= $form->field($model_event, 'date_end')->widget(DateTimePicker::classname(), [
								'options' => ['placeholder' => 'Enter event time ...'],
								'pluginOptions' => [
									'autoclose' => true
								]
							]); ?>
							<?= $form->field($model_event, 'content')->widget(TinyMce::className(), [
									'options' => ['rows' => 6,'id'=>'eventsContent'],
									'language' => 'en_GB',
									'clientOptions' => [
										'plugins' => [],
										'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
									]
								]);
							?>
							<div class="form-group text-center">
								<?= Html::submitButton(Yii::t('app', 'Create'),['class' => 'btn create-avent']) ?>
							</div>
							<?php ActiveForm::end(); ?>
						</div>
						<div role="tabpanel" class="tab-pane" id="news">
							<?php $form = ActiveForm::begin([
									'action' => Url::to( '/university/event/create', true),
									'options' => [
										'enctype' => 'multipart/form-data',
										'class' => 'model-form',
										'data-mr-ajax-form'=>'true',
										//'data-mr-events'=>'keyup,change',
									]
								]); ?>
								<?= $form->field($model_event, 'type')->hiddenInput(['value' => 'news'])->label(false) ?>
								<?= $form->field($model_event, 'place_type')->hiddenInput(['value' => 'university'])->label(false) ?>
								<?= $form->field($model_event, 'place_id')->hiddenInput(['value' => $model->id])->label(false) ?>
								<?= $form->field($model_event, 'title')->textInput(['maxlength' => true]) ?>
								<?= $form->field($model_event, 'image')->fileInput([
									"data-preview" => "true",
								]) ?>
								<?= $form->field($model_event, 'short_desc')->textarea(['rows' => 6]) ?>
								<?= $form->field($model_event, 'date_start')->hiddenInput(['value' => '0000-00-00'])->label(false) ?>
								<?= $form->field($model_event, 'date_end')->hiddenInput(['value' => '0000-00-00'])->label(false) ?>
								<?= $form->field($model_event, 'content')->widget(TinyMce::className(), [
									'options' => ['rows' => 6,'id'=>'newsContent'],
									'language' => 'en_GB',
									'clientOptions' => [
										'plugins' => [],
										'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
									]
								]);
								?>
							<div class="form-group text-center">
								<?= Html::submitButton(Yii::t('app', 'Create'),['class' => 'btn create-avent']) ?>
							</div>
							<?php ActiveForm::end(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /Add events modal -->
