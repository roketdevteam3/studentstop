<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 25.02.16
 * Time: 21:44
 * Email: mackraiscms@gmail.com
 * Site: http://mackrais.tk
 */
use yii\helpers\StringHelper;
use yii\helpers\Url;
?>

<?php if(!empty($news_carousel)): ?>
          <?php $count_news = count($news_carousel) <= 5 ? count($news_carousel) : 5;?>

            <?php foreach ($news_carousel as $i_news => $news): ?>
                <?php if($count_news >= $i_news): ?>

              <div class="sil">
                <div class="textblock">
                    <a href="#" style="color: #fff;">
                        <h5><?= $news->title ?></h5>
                        <p> 
                        <?= StringHelper::truncate(strip_tags($news->short_desc),'300') ; ?>
                        </p>
                    </a>
                </div>
            </div>
            <!--
            <div class="sil2">
                <div class="textblock">
                    <a href="#" style="color: #fff;">
                        <h5>Title</h5>
                        <p>4 users are interesled in buying your texkdalksdjasld;asdlas adaksdt books</p>
                    </a>
                </div>
            </div> -->

                      <?php endif; ?>
            <?php endforeach; ?>
<?php endif; ?>