<?php

	use app\models\user\UserFriendRequest;
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\widgets\Pjax;

?>

	<div id="right-panel">
		<div class="u-av">
			<img class="img-responsive" src="<?= $model->getImg() ?>" alt="logo">
		</div>
		<div class="rating-blok">
			<span class="rating-count">12/1200</span>
			<div class="rating-star">
				<i class="rating-active-star fa fa-star"></i>
				<i class="rating-active-star fa fa-star"></i>
				<i class="rating-active-star fa fa-star"></i>
				<i class="rating-active-star fa fa-star"></i>
				<i class="fa fa-star-o"></i>
			</div>
		</div>
		<div class="menu-wrapper">
			<?= $this->render('left_menu',['model'=>$model]) ?>
		</div>
	</div>
	<div class="row university-content">
		<?= $this->render('_carousel',['events_base'=>$news_carousel,'model' => $model]) ?>
		<div class="container-fluid background-block">
			<div class="row">
				<div class="container-fluid">
					<?php foreach($modelUser as $user){ ?>
						<div class="user-block">
							<?php // var_dump($user); ?>
								<div class="user-img">
									<?php if($user['avatar'] != ''){ ?>
										<img class="img-responsive" src="<?= Url::home().'images/users_images/'.$user['avatar']; ?>">
									<?php }else{ ?>
										<img class="img-responsive" src="<?= Url::home().'images/default_avatar.jpg'; ?>">
									<?php } ?>
								</div>
							<!-- </div> -->
							<div class="user-btn-wrap">
								<div class="container-fluid no-padding">
									<div class="col-xs-9 no-padding">
										<?= HTML::a('Profile',Url::home().'profile/'.\Yii::$app->user->id,['class'=>'btn profile-btn']); ?>
										<button class="btn follow-btn">Follow</button>
									</div>
									<div class="col-xs-3 no-padding">
										<?php if($user['id_user'] != \Yii::$app->user->id){ ?>
											<div class="">
												<span class="startChat" data-sender-id="<?=\Yii::$app->user->id?>" data-recipient-id="<?= $user['id_user']?>"></span>
											</div>
											<div class="">
												<span class="ifonline startVideo" data-sender-id="<?=\Yii::$app->user->id?>" data-recipient-id="<?=$user['id_user']?>"></span>
											</div>
										<?php } ?>
									</div>
								</div>
							</div>
							<div class="user-info">
								<?php //var_dump($user); ?>
								<h5 class="user-name"><?= $user['name'].' '.$user['surname']; ?></h5>
								<p class="user-address"><?= $user['address']; ?></p>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>