<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 29.02.16
 * Time: 18:23
 * Email: mackraiscms@gmail.com
 * Site: http://mackrais.tk
 */
use app\models\user\UserFriendRequest;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
?>

	<div id="right-panel">
		<div class="u-av">
			<img class="img-responsive" src="<?= $model->getImg() ?>" alt="logo">
		</div>
		<div class="rating-blok">
			<span class="rating-count">12/1200</span>
			<div class="rating-star">
				<i class="rating-active-star fa fa-star"></i>
				<i class="rating-active-star fa fa-star"></i>
				<i class="rating-active-star fa fa-star"></i>
				<i class="rating-active-star fa fa-star"></i>
				<i class="fa fa-star-o"></i>
			</div>
		</div>
		<div class="menu-wrapper">
			<?= $this->render('left_menu',['model'=>$model]) ?>
		</div>
	</div>
	<div class="row university-content">
		<?= $this->render('_carousel',['events_base'=>$news_carousel,'model' => $model]) ?>
		<div class="container-fluid background-block">
			<div class="row">
				<div class="courses-form-wrap">
					<?php  $form = ActiveForm::begin(); ?>
					<div class="container-fluid">
						<div class='row'>
							<div class='col-sm-9' style="font-size:14px;">
								<?= $form->field($modelUserInfo, 'major_id')->dropDownList($majorArray,['class'=>'form-control newFormControl','style' => 'font-size:14px;','prompt'=>'Select Your Major'])->label(false) ?>
								<?php //= Html::dropDownList('s_id', null,$majorArray,) ?>
							</div>
							<div class='col-sm-3'>
								<div class="form-group text-center">
									<?= Html::submitButton(Yii::t('app', 'Save'),['class' => 'btn courses-form-btn']) ?>
								</div>
							</div>
						</div>
					</div>
					<?php ActiveForm::end();  ?>
				</div>
				<div class="col-sm-12">
					<div class="row">
						<?php if($modelUserInfo->major_id != 0){ ?>
							<?php if($modelCourses != null){ ?>
								<?php foreach($modelCourses as $courses){ ?>
									<div class='col-md-3 col-sm-4'>
										<div class="panel courses-panel" style="border: 1px solid rgba(217, 217, 217, 0.6);position:relative;">
											<div class="panel-body">
												<img src="<?= Url::home();?>images/course_image_dafault.jpg" style="width:100%">
											</div> 
											<h5 class="courses-name">
												<a href=""><?= $courses->name; ?></a>
											</h5>
										</div>
									</div>
								<?php } ?>
							<?php }else{ ?>
								<div class="col-sm-12">
									<span style="font-size:14px;">Your major not have courses</span>
								</div>
							<?php } ?>
						<?php }else{ ?>
								<div class="col-sm-12">
									<span style="font-size:14px;">You not select major</span>
								</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
