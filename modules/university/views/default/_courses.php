<?php
/**
 * Created by PhpStorm.
 * User: minace
 * Date: 3/1/16
 * Time: 12:03 AM
 */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\modules\university\modules\majors\models\Major */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="major-form">

    <?php $form = ActiveForm::begin([
        'action' => Url::to( 'university/course/create-ajax?id_univesity='.$model_u->id, true),
        'options' => [
            'data-mr-ajax-form'=>'true',
        ]
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php Pjax::begin(['id' => 'mr_courses']) ?>

    <?php foreach ($dataProvider->models as $k =>  $item) : ?>

        <?php
        if($item->checkUniversity($model_u->id)){
            $class_btn = 'btn-danger';
            $class_icon = 'fa-minus';
        }else{
            $class_btn = 'btn-success';
            $class_icon = 'fa-plus';
        }
        ?>
        <h4>
            <button class="btn <?= $class_btn ?> mr-major-plus-btn"
                    data-mr-event="ajax_send_click"
                    data-mr-callb-success = " if(_this.hasClass('btn-danger')){_this.removeClass('btn-danger'); _this.find('i').removeClass().addClass('fa fa-plus'); _this.addClass('btn-success'); }else{ if(_this.hasClass('btn-success')){_this.removeClass('btn-success'); _this.addClass('btn-danger'); _this.find('i').removeClass().addClass('fa fa-minus');  } }"
                    data-mr-callb-error = "bootbox.alert(msg);" data-mr-url="<?= Url::to('university/course/add-ajax-university', true) ?>"
                    data-mr-param-id-course="<?= $item->id ?>"
                    data-mr-param-id-university="<?= $model_u->id ?>">
                <i class="fa <?= $class_icon ?>"></i>
                </button>

                <button class="btn btn-primary mr-major-plus-btn" data-mr-event="dynamic_load_model" data-mr-url="<?= Url::to('university/major/get-ajax-modal-course?id='.$item->id.'&university='.$model_u->id ,true) ?>">
                    <i class="fa fa-bars"></i>
            </button>
            <?= $item->name ?>
        </h4>
    <?php endforeach ?>



    <div class="col-md-12">
        <?= yii\widgets\LinkPager::widget([
            'pagination' => $dataProvider->pagination
        ]) ?>
    </div>

    <?php Pjax::end() ?>


</div>