<?php
/**
 * Created by PhpStorm.
 * User: minace
 * Date: 2/29/16
 * Time: 11:45 PM
 */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use dosamigos\tinymce\TinyMce;
use app\widgets\rating\RatinguniversityWidget;

?>
<div style="display:none">
  <div class="table table-striped previewTemplateQ" >

		  <div id="template" class="file-row">
			<div>
				<span class="preview"><img data-dz-thumbnail  style="width: 248px" /></span>
			</div>
		  </div>
  </div>
</div>
<div class="universityInfoId" style="display:none;"><?= $model->id; ?></div>
<div class="universityInfoName" style="display:none;"><?= $model->name; ?></div>
<div class="universityInfoImage" style="display:none;"><?= $model->getImg() ?></div>
<div id="right-panel">
            <div class="u-av">
                            <img class="img-responsive" src="<?= $model->getImg() ?>" alt="logo">
            </div>
            <div class="rating-blok">
                            <span>Rank:</span>
                            <span class="rating-count universityRatingCount"></span>
                            <?= RatinguniversityWidget::widget(['university_id' => $model->id]);?>
            </div>
            <div class="menu-wrapper">
                    <?= $this->render('dashboard_right_block') ?>
            </div>
    </div>

<div class="row university-content">
    <?php //= $this->render('_carousel',['events_base'=>$news_carousel,'model' => $model]) ?>
    <div class="container-fluid background-block">
        <div class="row">
            <ul class="nav nav-tabs navigation-tabs setting-university-tabs">
                <li role="presentation" class="">
                    <?= HTML::a('Edit university', Url::home().'university/settings/'.$model->slug); ?>
                </li>
                <li role="presentation" class="active">
                    <a href="">Post</a>
                </li>
            </ul>
            <div class="col-xs-12" style="margin-bottom: 23px">
                <div class="col-xs-6 text_style">
                    <g>Add post to slider</g>
                    </div>
                <div class="col-xs-6 text_style1">
                    <g>Here is your slider</g>
                </div>
            </div>

            <div class="col-sm-6 previewSliderPost" style="float: right">
                <div class=" clearfix"></div>
            </div>

            <div class="col-sm-6" style="float: left">
                <div class="col-sm-12 containerPost" data-page_type="setting">
                </div>
            </div>
        </div>
    </div>
</div>



<div style="display:none;">
    <div id="previewsFileModal" class="previews-comment-file">
        <div class="preview" style="position:relative;width:100px;float:left;padding:10px;">
            <img data-dz-thumbnail style="width:100%;"/>
            <div class="x-button" style="position: absolute;top: 0;right: 0;background-color: rgba(29, 32, 34, 0.6);padding: 3px;color: #ffffff;font-size: 14px;cursor: pointer;">
                <i class="fa fa-times" data-dz-remove></i>
            </div>
        </div>
    </div>
</div>
<div class="modal fade no-header" id="modalShowPost" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog"  role="document">
        <div class="modal-content" style="height: 317px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            </div>
            <div class="modal-body modalContent">

                <div class="left-content" style="height:287px;">
                    <div class="post-header">
                        <div class="avatar">
                        </div>
                        <div class="info">
                            <h4 class="username">
                            </h4>
                            <h6 class="postCreate">
                            </h6>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="postContent">
                    </div>
                    <div class="postFilesBlock">
                    </div>
                </div>
                <div class="right-content">
                    <form class="add_comment_to_post_modal " data-post_id="">
                        <div class="comment-area">
                            <textarea class="form-control comment-input" name="newCommentPost" rows="1" placeholder="Write your comment ..." ></textarea>
                            <input type='hidden' name='commentFilesInputModal' value="">
                            <div class="add-files btn-ripple clickCommentFilesAddModal addFileCommentModal" data-post_id="" ></div>
                            <div class="previewFileBlockCommentModal"></div>
                        </div>
                        <div class="btn-comment-wrap">
                            <input type="submit" class="btn add-comment" name="addComment" value="Add">
                        </div>
                    </form>
                    
                    <div class="commentContentBlock">
                    </div>
                </div>
                <div style="clear:both;"></div>
            </div>
        </div>
    </div>
</div>