<?php
/**
 * Created by PhpStorm.
 * User: minace
 * Date: 2/29/16
 * Time: 11:45 PM
 */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use dosamigos\tinymce\TinyMce;
use app\widgets\rating\RatinguniversityWidget;

use app\assets\UniversitysettingAsset;
UniversitysettingAsset::register($this);

?>
<div style="display:none">
  <div class="table table-striped previewTemplateQ" >

		  <div id="template" class="file-row">
			<div>
				<span class="preview"><img data-dz-thumbnail  style="width: 248px" /></span>
			</div>
		  </div>
  </div>
</div>

<div class="universityInfoId" style="display:none;"><?= $model->id; ?></div>
<div class="universityInfoName" style="display:none;"><?= $model->name; ?></div>
<div class="universityInfoImage" style="display:none;"><?= $model->getImg() ?></div>

	<div id="right-panel">
		<div class="u-av">
				<img class="img-responsive" src="<?= $model->getImg() ?>" alt="logo">
		</div>
		<div class="rating-blok">
				<span>Rank:</span>
				<span class="rating-count universityRatingCount"></span>
				<?= RatinguniversityWidget::widget(['university_id' => $model->id]);?>
		</div>
		<div class="menu-wrapper">
			<?= $this->render('dashboard_right_block') ?>
		</div>
	</div>

	<div class="row university-content">
		<?php //= $this->render('_carousel',['events_base'=>$news_carousel,'model' => $model]) ?>
		<div class="container-fluid background-block">
			<div class="row">
                <ul class="nav nav-tabs navigation-tabs setting-university-tabs">
                    <li role="presentation" class="active">
                        <a href="">Edit university</a>
                    </li>
                    <li role="presentation" class="">
                        <?= HTML::a('Post', Url::home().'university/sliderpost/'.$model->slug); ?>
                    </li>
                </ul>
				<div class="col-sm-12">
					<div class="settings-wrap">
						<h2 class="s-head">Edit university</h2>
						<?php $form = ActiveForm::begin([
							'options' => [
								'enctype' => 'multipart/form-data',
								'class' => 's-form',
							]
						]); ?>

						<?= $form->field($model, 'status', [
							'template' => ' {input} <label for="university-status">' . Yii::t('site', 'Status') . '</label> {error}{hint}'
						])->checkbox(['label' => null, 'class' => 'checkbox']); ?>

						<?= $form->field($model, 'id_owner')->dropDownList($arrayUser) ?>
						<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

						<?= $form->field($model, 'history')->widget(TinyMce::className(), [
                                                        'options' => ['rows' => 6,'id'=>'eventsContent'],
                                                        'language' => 'en_GB',
                                                        'clientOptions' => [
                                                                'plugins' => [],
                                                                'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                                                        ]
                                                ]); ?> 

						<?= $form->field($model, 'location')->textInput() ?>
						<?= $form->field($model, 'year_founded')->textInput() ?>
						<?php if ($model->slug): ?>
							<?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
						<?php endif ?>
						<div class="col-sm-12">
							<?php if ($model->image) : ?>
								<img src="<?= $model->image ?>" style="width: 248px">
							<?php endif;  ?>
						</div>
						<div style="clear:both;"></div>
						<div class="col-sm-12 text-center">
							<a class="btn btn-success newPhoto">Change photo</a>
							<div class="newPhotoU">
							</div>
						</div>
						<div style="clear:both;"></div>
						<?= $form->field($model, 'image')->hiddenInput()->label(false);?>

						<?= $form->field($model, 'date')->widget(DateTimePicker::classname(), [
							'options' => ['placeholder' => 'Enter event time ...'],
							'pluginOptions' => [
								'autoclose' => true
							]
						]); ?>

						<div class="form-group text-center">
							<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn update btn-success' : 'btn update']) ?>
						</div>
						<?php ActiveForm::end(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>