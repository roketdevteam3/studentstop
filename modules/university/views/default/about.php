<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use vova07\imperavi\Widget;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UniversitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Universities');
$this->params['breadcrumbs'][] = $this->title;
?>

	<div id="right-panel">
		<div class="u-av">
			<img class="img-responsive" src="<?= $model->getImg() ?>" alt="logo">
		</div>
		<div class="rating-blok">
			<span class="rating-count">12/1200</span>
			<div class="rating-star">
				<i class="rating-active-star fa fa-star"></i>
				<i class="rating-active-star fa fa-star"></i>
				<i class="rating-active-star fa fa-star"></i>
				<i class="rating-active-star fa fa-star"></i>
				<i class="fa fa-star-o"></i>
			</div>
		</div>
		<div class="menu-wrapper">
			<?= $this->render('left_menu',['model'=>$model]) ?>
		</div>
	</div>

	<div class="row university-content">
		<?= $this->render('_carousel',['events_base'=>$news_carousel,'model' => $model]) ?>
		<div class="container-fluid background-block">
			<div class="row">
				<div class="container-fluid">
					<div class="row">
						<div class="col-sm-12">
							<div class="univer-about-block">
								<h3 class="univer-about-header"><?= $model->name; ?></h3>
								<div>
									<?= $model->history; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
