<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 25.02.16
 * Time: 21:44
 * Email: mackraiscms@gmail.com
 * Site: http://mackrais.tk
 */
use yii\helpers\StringHelper;
use yii\helpers\Url;
?>

	<?php if(!empty($events_base)): ?>

	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
		<?php if(count($events_base) <= 5){
			$count = count($events_base);
		} else{
			$count = 5;
		}
		?>
		
		<div class="carousel-inner " role="listbox">
		<!-- Indicators -->
			<ol class="carousel-indicators">
				<?php for($i=0; $i < $count; $i++): ?>
						 <?php if($i == 0) :?>
						<li data-target="#carousel-example-generic" data-slide-to="<?= $i ?>" 
						class="active"></li>
					<?php else: ?>
						 <li data-target="#carousel-example-generic" data-slide-to="<?= $i ?>" ></li>
					<?php endif; ?>
				<?php endfor; ?>
			</ol>
			<?php foreach ($events_base as $i_event => $event): ?>
			<?php if($count >= $i_event): ?>
			<?php 
				$class_carousel = ''; 
				if($i_event == 0) {$class_carousel = 'active';}
			?>
			<div class="item <?= $class_carousel ?>" >
				<img class="img-responsive" src="<?=  $event->getImg();   ?>" alt="sl1">
				<div class="carousel-caption style-slider">
					<div class="col-sm-12">
						<h4 class="event-title"><?= StringHelper::truncate(strip_tags($event->title),'20') ; ?></h4>
						<h5 class="event-short-desc">
							<?= StringHelper::truncate(strip_tags($event->short_desc),'70') ; ?>
						</h5>
						<a href="<?= Url::to('university/event/view/'.$event->slug.'/'.$model->slug,true); ?>" class="btn sl-read-more"> View</a>
						<div></div>
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php endforeach; ?>
		</div>
	</div>

	<?php else: ?>
	<h2 class="mr-empty-university-carousel-h">Now the university don`t have any events!</h2>
	<div class="mr-empty-university-carousel">
		
	</div>
	<?php endif; ?>