<?php

namespace app\modules\university\controllers;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\Event;
use app\models\Follower;
use app\models\News;
use app\modules\users\models\UserImages;
use app\models\search\EventSearch;
use app\models\search\NewsSearch;
use app\modules\users\models\UserInfo;
use app\modules\university\components\UniverseController;
use app\modules\university\models\Course;
use app\modules\university\models\Major;
use app\modules\users\models\User;
use app\models\Post;
use app\models\Postfiles;
use app\models\Postviews;
use app\models\Classes;
use app\models\Classreting;
use app\models\Postlike;
use app\models\Notification;
use app\models\Notificationusers;

use app\models\Creditsactioncount;
use app\models\Creditsaction;

use app\models\Postcomment;
use app\models\Ratingrequest;
use app\models\Postcommentfiles;
use app\modules\university\models\search\CourseSearch;
use app\modules\university\models\Universitynotitfication;
use app\modules\university\models\search\MajorSearch;
use Yii;
use app\modules\university\models\University;
use app\modules\university\models\search\UniversitySearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\db\Query;
error_reporting( E_ERROR );
/**
 * UniversityController implements the CRUD actions for University model.
 */
class DefaultController extends UniverseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action) {
        if(!\Yii::$app->user->isGuest){
            if(Yii::$app->user->identity->getUsertUniversity()){
                $this->enableCsrfValidation = false;
                return parent::beforeAction($action);
            }else{
                return $this->redirect(Url::to("/users/choiceuniversity",true));
            }
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    /**
     * Lists all University models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        $modelUserInfo = \app\models\user\UserInfo::find()->where(['id_user'=>\Yii::$app->user->id])->one();
        $modelUniversity = University::find()->where(['id'=>$modelUserInfo->university_id])->one();
        if(($modelUserInfo != null) && ($modelUserInfo->university_id != '') &&($modelUniversity != null)){
            return $this->redirect(['view', 'slug' => $modelUniversity->slug]);
        }
        $searchModel = new UniversitySearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->setSort([
            'defaultOrder' => [
                'date_create' => SORT_DESC,
            ],
        ]);
        $dataProvider->setPagination(
            [
                'pageSize' => 15,
                'pageParam' => 'page_news'
            ]
        );

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single University model.
     * @param string $slug
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionPostslikes()
    {
        $post_id = $_POST['post_id'];
        $result = [];
        $modelPostLike = Postlike::find()->where(['post_id' => $post_id,'user_like_id' => \Yii::$app->user->id])->one();
        $modelPost = Post::find()->where(['id' => $post_id])->one();
        if($modelPostLike != null){
            $result['action'] = 'delete';
            if($modelPostLike->delete()){
                $result['status'] = 'success';
                $modelPost->scenario = 'change_like_count';
                $modelPost->like_count = intval($modelPost->like_count) - 1;
                $modelPost->save();
                $result['like_count'] = $modelPost->like_count;
            }else{
                $result['status'] = 'error';
            }
        }else{
            $result['action'] = 'add';
            $modelPostLike =  new Postlike();
            $modelPostLike->scenario =  'add_like';
            $modelPostLike->user_like_id =  \Yii::$app->user->id;
            $modelPostLike->post_id =  $post_id;
            if($modelPostLike->save()){
                
                
                
                
                
                            $modelCreditsaction = Creditsaction::find()->where(['object_id' => $post_id, 'action' => 'add_like_in_post'])->one();
                            if($modelCreditsaction){
                                $modelCreditsaction->scenario = 'change_action_count';
                                $modelCreditsaction->action_count = $modelCreditsaction->action_count + 1;
                                if($modelCreditsaction->save()){
                                }
                                
                                
                                if((($modelCreditsaction->action_count % 100) == 0)){
                                    $modelCreditsactionCount = Creditsactioncount::find()->where(['user_id' => $modelPost->user_id])->one();
                                    if($modelCreditsactionCount){
                                        $modelCreditsactionCount->scenario = 'update_credits_count';
                                        $modelCreditsactionCount->credits_count = $newModelCreditsactionCount->credits_count + 5;
                                        $modelCreditsactionCount->date_update = date("Y-m-d H:i:s");
                                        if($modelCreditsactionCount->save()){
                                            
                                        }
                                    }else{
                                        $newModelCreditsactionCount = new Creditsactioncount();
                                        $newModelCreditsactionCount->scenario = 'add_credits_count';
                                        $newModelCreditsactionCount->user_id = $modelPost->user_id;
                                        $newModelCreditsactionCount->credits_count = 5;
                                        if($newModelCreditsactionCount->save()){

                                        }
                                    }
                                    
                                        $modelCredits = \app\models\Usercredits::find()->where(['user_id' => $modelPost->user_id])->one();
                                        if($modelCredits){
                                            $modelCredits->scenario = 'update_credits_count';
                                            $modelCredits->credit_count = $modelCredits->credit_count + 5;
                                            if($modelCredits->save()){
                                            }                            
                                        }else{
                                            $modelNewCredits = new \app\models\Usercredits();
                                            $modelNewCredits->scenario = 'add_credits';
                                            $modelNewCredits->user_id = $modelPost->user_id;
                                            $modelNewCredits->credit_count = 5;
                                            $modelNewCredits->many = 0;
                                            if($modelNewCredits->save()){

                                            }
                                        }
                                    
                                        $modelNotification = new Notification();
                                        $modelNotification->scenario = 'add_notification';
                                        $modelNotification->module = 'university';
                                        $modelNotification->from_object_id = $modelPost->id;
                                        $modelNotification->to_object_id = $modelPost->id;
                                        $modelNotification->notification_type = '100_like_in_post_credits';
                                        $modelNotification->university_id = $modelNewPost->university_id;
                                        if($modelNotification->save()){
                                            $modelNotificationUser = new Notificationusers();
                                            $modelNotificationUser->scenario = 'new_notification';
                                            $modelNotificationUser->user_id = $modelPost->user_id;
                                            $modelNotificationUser->notification_id = $modelNotification->id;
                                            $modelNotificationUser->save();
                                        }
                                }
                                
                                
                                
                            }else{
                                        $newModelCreditsaction = new Creditsaction();
                                        $newModelCreditsaction->scenario = 'add_credits';
                                        $newModelCreditsaction->object_id = $post_id;
                                        $newModelCreditsaction->action = 'add_like_in_post';
                                        $newModelCreditsaction->action_count = 1;
                                        if($newModelCreditsaction->save()){
                                            $modelCreditsactionCount = Creditsactioncount::find()->where(['user_id' => $modelPost->user_id])->one();
                                            if($modelCreditsactionCount){
                                                $modelCreditsactionCount->scenario = 'update_credits_count';
                                                $modelCreditsactionCount->credits_count = $newModelCreditsactionCount->credits_count + 1;
                                                $modelCreditsactionCount->date_update = date("Y-m-d H:i:s");
                                                if($modelCreditsactionCount->save()){
                                                }
                                            }else{
                                                $newModelCreditsactionCount = new Creditsactioncount();
                                                $newModelCreditsactionCount->scenario = 'add_credits_count';
                                                $newModelCreditsactionCount->user_id = $modelPost->user_id;
                                                $newModelCreditsactionCount->credits_count = 1;
                                                if($newModelCreditsactionCount->save()){

                                                }
                                            }
                                        }
                                        $modelCredits = \app\models\Usercredits::find()->where(['user_id' => $modelPost->user_id])->one();
                                        if($modelCredits){
                                            $modelCredits->scenario = 'update_credits_count';
                                            $modelCredits->credit_count = $modelCredits->credit_count + 1;
                                            if($modelCredits->save()){
                                            }                            
                                        }else{
                                            $modelNewCredits = new \app\models\Usercredits();
                                            $modelNewCredits->scenario = 'add_credits';
                                            $modelNewCredits->user_id = $modelPost->user_id;
                                            $modelNewCredits->credit_count = 1;
                                            $modelNewCredits->many = 0;
                                            if($modelNewCredits->save()){

                                            }
                                        }

                                        $modelNotification = new Notification();
                                        $modelNotification->scenario = 'add_notification';
                                        $modelNotification->module = 'university';
                                        $modelNotification->from_object_id = $modelPost->id;
                                        $modelNotification->to_object_id = $modelPost->id;
                                        $modelNotification->notification_type = 'first_like_in_post_credits';
                                        $modelNotification->university_id = $modelPost->university_id;
                                        if($modelNotification->save()){
                                            $modelNotificationUser = new Notificationusers();
                                            $modelNotificationUser->scenario = 'new_notification';
                                            $modelNotificationUser->user_id = $modelPost->user_id;
                                            $modelNotificationUser->notification_id = $modelNotification->id;
                                            $modelNotificationUser->save();
                                        }

                            }
                
                
                
                
                
                $modelUserInfo = UserInfo::find()->where(['university_id' => $modelPost['university_id']])->asArray()->all();
                $userArrayId = [];
                foreach($modelUserInfo as $user){
                    $userArrayId[] = $user['id_user'];                    
                }
                $modelNotification = new Notification();
                $modelNotification->scenario = 'add_notification';
                $modelNotification->module = 'university';
                $modelNotification->from_object_id = \Yii::$app->user->id;
                $modelNotification->to_object_id = $post_id;
                $modelNotification->notification_type = 'post_like';
                $modelNotification->university_id = $modelPost['university_id'];
                if($modelNotification->save()){
                        foreach($userArrayId as $user_id){
                            $modelNotificationUser = new Notificationusers();
                            $modelNotificationUser->scenario = 'new_notification';
                            $modelNotificationUser->user_id = $user_id;
                            $modelNotificationUser->notification_id = $modelNotification->id;
                            $modelNotificationUser->save();
                        }                
                }
                
                
                $result['status'] = 'success';
                $modelPost->scenario = 'change_like_count';
                $modelPost->like_count = intval($modelPost->like_count) + 1;
                $modelPost->save();
                $result['like_count'] = $modelPost->like_count;
            }else{
                $result['status'] = 'error';
            }
        }
        echo json_encode($result);
    }
    
    public function actionGetsliderpost()
    {
        $query = new Query();
        $query->select(['*', 'id' => '{{%post}}.id', 'postCreate' => '{{%post}}.date_create'])
                        ->from('{{%post}}')
                        ->join('LEFT JOIN',
                                    '{{%user}}',
                                    '{{%user}}.id = {{%post}}.user_id')
                        ->join('LEFT JOIN',
                                    '{{%user_info}}',
                                    '{{%user_info}}.id_user = {{%post}}.user_id')
                ->where(['{{%post}}.university_id' => $_POST['university_id'], 'slider' => 1])->orderBy('{{%post}}.date_create DESC');
        
        $command = $query->createCommand();
        $modelPosts = $command->queryAll();
    
        echo json_encode($modelPosts);
    }
    
    public function actionGetmyfiles()
    {
        $user_id = $_POST['user_id'];
        $modelUserFiles =  UserImages::find()->where(['user_id' => \Yii::$app->user->id,'type' => $_POST['type'], 'user_id' => $user_id])->asArray()->all();
        echo json_encode($modelUserFiles);
    }
    
    public function actionDeletepostwithslider()
    {
        $post_id = $_POST['post_id'];
        $result = [];
        $modelPost = Post::find()->where(['id' => $post_id])->one();
        $modelPost->scenario = 'slider';
        $modelPost->slider = 0;
        if($modelPost->save()){
            $result['status'] = 'save';
        }else{
            $result['status'] = 'not_save';
        }
        echo json_encode($result);
    }
    
    public function actionAddpostinslider()
    {
        $post_id = $_POST['post_id'];
        $result = [];
        $modelPost = Post::find()->where(['id' => $post_id])->one();
        $modelPost->scenario = 'slider';
        $modelPost->slider = 1;
        if($modelPost->save()){
            
                        
            $result['status'] = 'save';
                $query = new Query();
                $query->select(['*', 'id' => '{{%post}}.id', 'postCreate' => '{{%post}}.date_create'])
                                ->from('{{%post}}')
                                ->join('LEFT JOIN',
                                            '{{%user}}',
                                            '{{%user}}.id = {{%post}}.user_id')
                                ->join('LEFT JOIN',
                                            '{{%user_info}}',
                                            '{{%user_info}}.id_user = {{%post}}.user_id')
                        ->where(['{{%post}}.id' => $post_id]);

                $command = $query->createCommand();
                $modelPost = $command->queryOne();
                
                $modelUserInfo = UserInfo::find()->where(['university_id' => $modelPost['university_id']])->asArray()->all();
                $userArrayId = [];
                foreach($modelUserInfo as $user){
                    $userArrayId[] = $user['id_user'];                    
                }
                $modelNotification = new Notification();
                $modelNotification->scenario = 'add_notification';
                $modelNotification->module = 'university';
                $modelNotification->from_object_id = $modelPost['id'];
                $modelNotification->to_object_id = $modelPost['id'];
                $modelNotification->notification_type = 'add_post_in_slider';
                $modelNotification->university_id = $modelPost['university_id'];
                $modelNotification->save();
                if($modelNotification->save()){
                        foreach($userArrayId as $user_id){
                            $modelNotificationUser = new Notificationusers();
                            $modelNotificationUser->scenario = 'new_notification';
                            $modelNotificationUser->user_id = $user_id;
                            $modelNotificationUser->notification_id = $modelNotification->id;
                            $modelNotificationUser->save();
                        }                
                }
                
            $result['post'] = $modelPost;
        }else{
            $result['status'] = 'not_save';
        }
        echo json_encode($result);
    }
    
    public function actionGetfilterpost()
    {
        $query = new Query();
        $query->select(['*', 'id' => '{{%post}}.id', 'postCreate' => '{{%post}}.date_create'])
                        ->from('{{%post}}')
                        ->join('LEFT JOIN',
                                    '{{%user}}',
                                    '{{%user}}.id = {{%post}}.user_id')
                        ->join('LEFT JOIN',
                                    '{{%user_info}}',
                                    '{{%user_info}}.id_user = {{%post}}.user_id')
                ->where(['{{%post}}.university_id' => $_POST['university_id']])->orderBy('{{%post}}.date_create DESC')->limit(10);
        switch($_POST['filterType']){
            case 'my':
                $query->andWhere(['{{%post}}.user_id' => \Yii::$app->user->id]);
                break;
            case 'university':
                $query->andWhere(['{{%post}}.user_id' => 0]);
                break;
            case 'friend':
                $queryFriend = new Query();
                $queryFriend->select(['*', 'id' => '{{%user}}.id'])
                                ->from('{{%user_friend}}')
                                ->join('LEFT JOIN',
                                            '{{%user}}',
                                            '{{%user}}.id = {{%user_friend}}.id_friend')
                                ->join('LEFT JOIN',
                                            '{{%user_info}}',
                                            '{{%user_info}}.id_user = {{%user_friend}}.id_friend')
                        ->where(['{{%user_friend}}.id_user' => \Yii::$app->user->id])->orderBy('{{%user_friend}}.date_create DESC')->limit(20);

                $command = $queryFriend->createCommand();
                $modelUserFriend = $command->queryAll();
                $user_id = [];
                foreach($modelUserFriend as $userFriend){
                    $user_id[] = $userFriend['id']; 
                }
                $query->andWhere(['{{%post}}.user_id' => $user_id]);
                break;
        }

        $command = $query->createCommand();
        $modelPost = $command->queryAll();

        $postArray = [];
        foreach($modelPost as $post){
                    $query = new Query();
                    $query->select(['*', 'id' => '{{%post_comment}}.id', 'id_user' => '{{%user}}.id', 'name' => '{{%user}}.name',  'commentCreate' => '{{%post_comment}}.date_create'])
                                    ->from('{{%post_comment}}')
                                    ->join('LEFT JOIN',
                                                '{{%user}}',
                                                '{{%user}}.id = {{%post_comment}}.user_id')
                                    ->join('LEFT JOIN',
                                                '{{%user_info}}',
                                                '{{%user_info}}.id_user = {{%post_comment}}.user_id')
                            ->where(['{{%post_comment}}.post_id' => $post['id']])->orderBy('{{%post_comment}}.date_create DESC');
                    $command = $query->createCommand();
                    $modelCommentCount = Postcomment::find()->where(['post_id' => $post['id']])->count();
                    $modelComments = $command->queryOne();
                        $commentArray = [
                                'comment_info' => $modelComments,
                                'files' => Postcommentfiles::find()->where(['comment_id' => $modelComments['id']])->asArray()->all()
                            ];
            $postArray[] = [
                'post_info' => $post,
                'files' => Postfiles::find()->where(['post_id' => $post['id']])->asArray()->all(),
                'comments' => $commentArray,
                'modelCommentCount' => $modelCommentCount
            ];
        }
            
        echo json_encode($postArray);
    }
    
    public function actionView($slug=null)
    {
        $model = $this->findUniversity($slug);
        if($model == null){
            throw new \yii\web\NotFoundHttpException();
        }else{
            $model_event = new Event();
            $modelNewPost = new Post();
            if($_POST){
                $arrayFormat = ['video/x-flv' => '1',
                    '1' => '1',
                    'video' => '1',
                    'video/mp4' => '1',
                    'video/MP2T' => '1',
                    'application/x-mpegURL' => '1',
                    'video/3gpp' => '1',
                    'video/quicktime' => '1',
                    'video/x-msvideo' => '1',
                    'video/x-ms-wmv' => '1',
                    'audio/x-m4a' => '1',
                    //
                    //images
                    '0' => '0',
                    'image' => '0',
                    'image/png' => '0',
                    'image/png' => '0',
                    'image/jpeg' => '0',
                    'image/jpeg' => '0',
                    'image/jpeg' => '0',
                    'image/gif' => '0',
                    'image/bmp' => '0',
                    'image/vnd.microsoft.icon' => '0',
                    'image/tiff' => '0',
                    'image/tiff' => '0',
                    'image/svg+xml' => '0',
                    'image/svg+xml' => '0'
                    //
                ];
                $modelNewPost->scenario = 'add_post';
                $modelNewPost->content = $_POST['Post']['content'];
                if($_POST['Post']['avtorType'] == '1'){
                    $modelNewPost->user_id = 0;
                }else{
                    $modelNewPost->user_id = \Yii::$app->user->id;
                }
                $modelNewPost->university_id = $model->id;
                $modelNewPost->type = 'user';
                
                if($modelNewPost->save()){
                    
                    $modelUserInfo = UserInfo::find()->where(['university_id' => $modelNewPost->university_id])->asArray()->all();
                    $userArrayId = [];
                    foreach($modelUserInfo as $user){
                        $userArrayId[] = $user['id_user'];                    
                    }
                    
                    //add credits action begin
                            $modelCreditsaction = Creditsaction::find()->where(['object_id' => \Yii::$app->user->id, 'action' => 'add_post'])->one();
                            if($modelCreditsaction){
                                $modelCreditsaction->scenario = 'change_action_count';
                                $modelCreditsaction->action_count = $modelCreditsaction->action_count + 1;
                                if($modelCreditsaction->save()){
                                }
                                
                                
                                if((($modelCreditsaction->action_count % 100) == 0)){
                                    $modelCreditsactionCount = Creditsactioncount::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                    if($modelCreditsactionCount){
                                        $modelCreditsactionCount->scenario = 'update_credits_count';
                                        $modelCreditsactionCount->credits_count = $modelCreditsactionCount->credits_count + 5;
                                        $modelCreditsactionCount->date_update = date("Y-m-d H:i:s");
                                        if($modelCreditsactionCount->save()){
                                            
                                        }
                                    }else{
                                        $newModelCreditsactionCount = new Creditsactioncount();
                                        $newModelCreditsactionCount->scenario = 'add_credits_count';
                                        $newModelCreditsactionCount->user_id = \Yii::$app->user->id;
                                        $newModelCreditsactionCount->credits_count = 5;
                                        if($newModelCreditsactionCount->save()){

                                        }
                                    }
                                    
                                    $modelCredits = \app\models\Usercredits::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                    if($modelCredits){
                                        $modelCredits->scenario = 'update_credits_count';
                                        $modelCredits->credit_count = $modelCredits->credit_count + 5;
                                        if($modelCredits->save()){
                                        }                            
                                    }else{
                                        $modelNewCredits = new \app\models\Usercredits();
                                        $modelNewCredits->scenario = 'add_credits';
                                        $modelNewCredits->user_id = \Yii::$app->user->id;
                                        $modelNewCredits->credit_count = 5;
                                        $modelNewCredits->many = 0;
                                        if($modelNewCredits->save()){

                                        }
                                    }
                                    
                                        $modelNotification = new Notification();
                                        $modelNotification->scenario = 'add_notification';
                                        $modelNotification->module = 'university';
                                        $modelNotification->from_object_id = \Yii::$app->user->id;
                                        $modelNotification->to_object_id = $modelNewPost->id;
                                        $modelNotification->notification_type = 'post_credits';
                                        $modelNotification->university_id = $modelNewPost->university_id;
                                        if($modelNotification->save()){
                                            $modelNotificationUser = new Notificationusers();
                                            $modelNotificationUser->scenario = 'new_notification';
                                            $modelNotificationUser->user_id = \Yii::$app->user->id;
                                            $modelNotificationUser->notification_id = $modelNotification->id;
                                            $modelNotificationUser->save();
                                        }
                                }
                                
                                
                                
                            }else{
                                        $newModelCreditsaction = new Creditsaction();
                                        $newModelCreditsaction->scenario = 'add_credits';
                                        $newModelCreditsaction->object_id = \Yii::$app->user->id;
                                        $newModelCreditsaction->action = 'add_post';
                                        $newModelCreditsaction->action_count = 1;
                                        if($newModelCreditsaction->save()){
                                            $modelCreditsactionCount = Creditsactioncount::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                            if($modelCreditsactionCount){
                                                $modelCreditsactionCount->scenario = 'update_credits_count';
                                                $modelCreditsactionCount->credits_count = $modelCreditsactionCount->credits_count + 2;
                                                $modelCreditsactionCount->date_update = date("Y-m-d H:i:s");
                                                if($modelCreditsactionCount->save()){
                                                }
                                            }else{
                                                $newModelCreditsactionCount = new Creditsactioncount();
                                                $newModelCreditsactionCount->scenario = 'add_credits_count';
                                                $newModelCreditsactionCount->user_id = \Yii::$app->user->id;
                                                $newModelCreditsactionCount->credits_count = 2;
                                                if($newModelCreditsactionCount->save()){

                                                }
                                            }
                                        }
                                        $modelCredits = \app\models\Usercredits::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                        if($modelCredits){
                                            $modelCredits->scenario = 'update_credits_count';
                                            $modelCredits->credit_count = $modelCredits->credit_count + 2;
                                            if($modelCredits->save()){
                                            }                            
                                        }else{
                                            $modelNewCredits = new \app\models\Usercredits();
                                            $modelNewCredits->scenario = 'add_credits';
                                            $modelNewCredits->user_id = \Yii::$app->user->id;
                                            $modelNewCredits->credit_count = 2;
                                            $modelNewCredits->many = 0;
                                            if($modelNewCredits->save()){

                                            }
                                        }

                                        $modelNotification = new Notification();
                                        $modelNotification->scenario = 'add_notification';
                                        $modelNotification->module = 'university';
                                        $modelNotification->from_object_id = \Yii::$app->user->id;
                                        $modelNotification->to_object_id = $modelNewPost->id;
                                        $modelNotification->notification_type = 'first_post_credits';
                                        $modelNotification->university_id = $modelNewPost->university_id;
                                        if($modelNotification->save()){
                                            $modelNotificationUser = new Notificationusers();
                                            $modelNotificationUser->scenario = 'new_notification';
                                            $modelNotificationUser->user_id = \Yii::$app->user->id;
                                            $modelNotificationUser->notification_id = $modelNotification->id;
                                            $modelNotificationUser->save();
                                        }

                            }
                    //add credits action end
                    
                    $modelNotification = new Notification();
                    $modelNotification->scenario = 'add_notification';
                    $modelNotification->module = 'university';
                    $modelNotification->from_object_id = \Yii::$app->user->id;
                    $modelNotification->to_object_id = $modelNewPost->id;
                    $modelNotification->notification_type = 'add_post';
                    $modelNotification->university_id = $modelNewPost->university_id;
                    if($modelNotification->save()){
                            foreach($userArrayId as $user_id){
                                $modelNotificationUser = new Notificationusers();
                                $modelNotificationUser->scenario = 'new_notification';
                                $modelNotificationUser->user_id = $user_id;
                                $modelNotificationUser->notification_id = $modelNotification->id;
                                $modelNotificationUser->save();
                            }                
                    }
                    
                    if(!empty($_POST['filesInput'])){
                        foreach(json_decode($_POST['filesInput']) as $files => $type){
                            $modelFiles = new Postfiles();
                            $modelFiles->post_id = $modelNewPost->id;
                            $modelFiles->file_src = $files;
                            $modelFiles->file_type = $arrayFormat[$type];
                            if($modelFiles->save()){
                                $newModelUserImage =  new UserImages();
                                $newModelUserImage->scenario = 'add_file';
                                $newModelUserImage->user_id = \Yii::$app->user->id;
                                $newModelUserImage->image_name = $files;
                                $newModelUserImage->type = $arrayFormat[$type];
                                if($newModelUserImage->save()){

                                }
                            }
                        }
                    }
//                    $modelNewNotification = new Universitynotitfication();
//                    $modelNewNotification->scenario = 'add_notification';
//                    $modelNewNotification->user_id = \Yii::$app->user->id;
//                    $modelNewNotification->university_id = $model->id;
//                    $modelNewNotification->event_type = 'post';
//                    $modelNewNotification->event_id = $modelNewPost->id;
//                    if($modelNewNotification->save()){
//                    }
                    return $this->redirect('/university/view/'.$slug);
                }
                
            }
            $model_new_event = new Event();

            $date_today = date("Y-m-d");
            $query = Event::find()->where("date_start <= '".$date_today."' and date_end >= '".$date_today."'")
                    ->orWhere(['LIKE', 'date_create', $date_today]);
            
            $modelEvent = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 100]]);
            
            $querySlider = new Query();
            $querySlider->select(['*', 'id' => '{{%post}}.id', 'postCreate' => '{{%post}}.date_create'])
                            ->from('{{%post}}')
                            ->join('LEFT JOIN',
                                        '{{%user}}',
                                        '{{%user}}.id = {{%post}}.user_id')
                            ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%post}}.user_id')
                    ->where(['{{%post}}.university_id' => $model->id, 'slider' => 1])->orderBy('{{%post}}.date_create DESC')->limit(5);

            $command = $querySlider->createCommand();
            $modelSliderPost = $command->queryAll();
            
            
            $postSliderArray = [];
            foreach($modelSliderPost as $post){
                $postSliderArray[] = [
                    'post_info' => $post,
                    'files' => Postfiles::find()->where(['post_id' => $post['id']])->asArray()->one(),
                ];
            }
            $modelI = UserInfo::find()->where(['id_user' => \Yii::$app->user->id])->asArray()->one();
            //$queryNotification = Universitynotitfication::find()->where(['university_id' => $model->id])->orderBy('date_create DESC');
            //$notificationCount = $queryNotification->count();
            //$modelsNotification = new ActiveDataProvider(['query' => $queryNotification, 'pagination' => ['pageSize' => 5]]);
            
            
            if (!is_null($model)){
                return $this->render('view', [
                    'modelI' => $modelI,
                    'postSliderArray' => $postSliderArray,
                    'model_event' => $model_event,
                    'modelEvent'    =>$modelEvent->getModels(),
                    //'modelsNotification' => $modelsNotification->getModels(),
                    //'notificationCount' => $notificationCount,
                    'modelPost'     =>$postArray,
                    'model'         => $model,
                    'modelNewEvent'   => $model_new_event,
                    'modelNewPost'    => $modelNewPost,
                ]);
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
    
    public function actionSaveupdatepost(){
        $result = [];
            $arrayFormat = ['video/x-flv' => '1',
                    '1' => '1',
                    'video' => '1',
                    'video/mp4' => '1',
                    'video/MP2T' => '1',
                    'application/x-mpegURL' => '1',
                    'video/3gpp' => '1',
                    'video/quicktime' => '1',
                    'video/x-msvideo' => '1',
                    'video/x-ms-wmv' => '1',
                    'audio/x-m4a' => '1',
                    //
                    //images
                    '0' => '0',
                    'image' => '0',
                    'image/png' => '0',
                    'image/png' => '0',
                    'image/jpeg' => '0',
                    'image/jpeg' => '0',
                    'image/jpeg' => '0',
                    'image/gif' => '0',
                    'image/bmp' => '0',
                    'image/vnd.microsoft.icon' => '0',
                    'image/tiff' => '0',
                    'image/tiff' => '0',
                    'image/svg+xml' => '0',
                    'image/svg+xml' => '0'
                    //
                ];
            $modelUpdatePost = Post::find()->where(['id' => $_POST['post_id']])->one();
            $modelUpdatePost->scenario = 'add_post';
            $modelUpdatePost->content = $_POST['contentNews'];
            if($modelUpdatePost->save()){
                $result['status'] = 'update';
                Postfiles::deleteAll(['post_id' => $_POST['post_id']]);
                if(!empty($_POST['newFileArray'])){
                    foreach(json_decode($_POST['newFileArray']) as $files => $type){
                        $modelFiles = new Postfiles();
                        $modelFiles->post_id = $modelUpdatePost->id;
                        $modelFiles->file_src = $files;
                        $modelFiles->file_type = $arrayFormat[$type];
                        if($modelFiles->save()){
                            $newModelUserImage =  new UserImages();
                            $newModelUserImage->scenario = 'add_file';
                            $newModelUserImage->user_id = \Yii::$app->user->id;
                            $newModelUserImage->image_name = $files;
                            $newModelUserImage->type = $arrayFormat[$type];
                            if($newModelUserImage->save()){

                            }
                        }
                    }
                }
            }else{
                $result['status'] = 'not_update';
            }
        echo json_encode($result);
    }
    
    public function actionDeletepost(){
        $result = [];
        if(($_POST['post_id'] != '') && ($_POST['post_id'] != 0)){
            $modelPost = Post::find()->where(['id' => $_POST['post_id']])->one();
            if($modelPost != null){
                if($modelPost->delete()){
                    $result['status'] = 'delete';
                }else{
                    $result['status'] = 'not_delete_3';
                }
            }else{
                $result['status'] = 'not_delete_1';
            }
        }else{
                $result['status'] = 'not_delete_2';
        }
        echo json_encode($result);
    }
    
    public function actionGetsearchfriend(){
        $value = $_POST['inputValue'];
        $filter = $_POST['filter'];
        if($filter == 'friend'){
            $queryFriend = new Query();
            $queryFriend->select(['*', 'id' => '{{%user}}.id'])
                            ->from('{{%user_friend}}')
                            ->join('LEFT JOIN',
                                        '{{%user}}',
                                        '{{%user}}.id = {{%user_friend}}.id_friend')
                            ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%user_friend}}.id_friend')
                    ->where(['{{%user_friend}}.id_user' => \Yii::$app->user->id])->orderBy('{{%user_friend}}.date_create DESC');
            $queryFriend->andFilterWhere([
                'or',
                ['LIKE', '{{%user}}.name', $value],
                ['LIKE', '{{%user}}.surname', $value],
            ]);

            $command = $queryFriend->createCommand();
            $modelUser = $command->queryAll();
        }elseif($filter == 'classmates'){
            $modelFollower = Follower::find()->where(['id_user' => \Yii::$app->user->id,'type_object' => 'class','status' => 0])->asArray()->all();
            $class_id = [];
            foreach($modelFollower as $follower){
                $class_id[] = $follower['id_object'];
            }
            $modelFollowers = Follower::find()->where(['id_object' => $class_id, 'type_object' => 'class','status' => 0])->asArray()->all();
            $users_id = [];
            foreach($modelFollowers as $follow){
                $users_id[] = $follow['id_user'];
            }
            $queryUser = new Query();
            $queryUser->select(['*', 'id' => '{{%user}}.id'])
                            ->from('{{%user}}')
                            ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%user}}.id')
                    ->where(['{{%user}}.id' => $users_id])->andWhere(['NOT IN', '{{%user}}.id', \Yii::$app->user->id]);
            $command = $queryUser->createCommand();
            $modelUser = $command->queryAll();
        }else{
            $queryUser = new Query();
            $queryUser->select(['*', 'id' => '{{%user}}.id', 'name' => '{{%user}}.name'])
                            ->from('{{%user}}')
                            ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%user}}.id')
                            ->join('LEFT JOIN',
                                        '{{%user_role}}',
                                        '{{%user_role}}.id = {{%user_info}}.academic_status')
                    ->where(['{{%user_role}}.id' => '6']);
            if(\Yii::$app->user->identity->getUsertUniversity()){
                $queryUser->andWhere(['{{%user_info}}.university_id' => \Yii::$app->user->identity->getUsertUniversity()]);
            }

            $command = $queryUser->createCommand();
            $modelUser = $command->queryAll();
        }
        
        echo json_encode($modelUser);
    }
    
    public function actionGetnotification(){
        $university_id = $_POST['university_id'];
        $limit = 5;
        $offset = $_POST['offest'];
        $result = [];
            $queryNotification = new Query();
            $queryNotification->select(['*', 'id' => '{{%university_notification}}.id'])
                            ->from('{{%university_notification}}')
                            ->join('LEFT JOIN',
                                        '{{%user}}',
                                        '{{%user}}.id = {{%university_notification}}.user_id')
                    ->where(['{{%university_notification}}.university_id' => $university_id])->orderBy('{{%university_notification}}.date_create DESC')->limit(20);
            $queryNotification =  $queryNotification->limit($limit)->offset($offset);

            $notificationCount = $queryNotification->count();
            $command = $queryNotification->createCommand();
            $modalNotification = $command->queryAll();
            
            
        //$queryNotification = Universitynotitfication::find()->where(['university_id' => $university_id])->orderBy('date_create DESC');
        
        $result = ['count' => $notificationCount, 'model' => $modalNotification];
        echo json_encode($result);
    }
    
    public function actionGetpostinformation(){
            $post_id = $_POST['post_id'];
            
            $query = new Query();
            $query->select(['*', 'id' => '{{%post}}.id', 'postCreate' => '{{%post}}.date_create'])
                            ->from('{{%post}}')
                            ->join('LEFT JOIN',
                                        '{{%user}}',
                                        '{{%user}}.id = {{%post}}.user_id')
                            ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%post}}.user_id')
                    ->where(['{{%post}}.id' => $post_id]);
            $command = $query->createCommand();
            $modelPost = $command->queryOne();
            
            $postArray = [];
            if($modelPost != null){
                $query = new Query();
                $query->select(['*', 'id' => '{{%post_comment}}.id', 'commentCreate' => '{{%post_comment}}.date_create'])
                                ->from('{{%post_comment}}')
                                ->join('LEFT JOIN',
                                            '{{%user}}',
                                            '{{%user}}.id = {{%post_comment}}.user_id')
                                ->join('LEFT JOIN',
                                            '{{%user_info}}',
                                            '{{%user_info}}.id_user = {{%post_comment}}.user_id')
                        ->where(['{{%post_comment}}.post_id' => $modelPost['id']])->orderBy('{{%post_comment}}.date_create DESC');
                $command = $query->createCommand();
                $modelComments = $command->queryAll();
                $commentArray = [];
                foreach($modelComments as $comment){
                    $commentArray[] = [
                        'comment_info' => $comment,
                        'files' => Postcommentfiles::find()->where(['comment_id' => $comment['id']])->asArray()->all()
                    ];
                }

                $postArray = [
                    'post_info' => $modelPost,
                    'files' => Postfiles::find()->where(['post_id' => $modelPost['id']])->asArray()->all(),
                    'comments' => $commentArray
                ];
                
                
                $modelPostviews = Postviews::find()->where(['user_id' => \Yii::$app->user->id, 'post_id' => $modelPost['id']])->one();
                if($modelPostviews == null){
                    $modelPostviews = new Postviews();
                    $modelPostviews->scenario = 'add_view';
                    $modelPostviews->user_id = \Yii::$app->user->id;
                    $modelPostviews->post_id = $modelPost['id'];
                    if($modelPostviews->save()){
                        $postArray['views'] = 'add';
                        $modelPos = Post::find()->where(['id' => $modelPost['id']])->one();
                        $modelPos->scenario = 'views';
                        $modelPos->views = $modelPos->views + 1;
                        $modelPos->save();
                    }else{
                        $postArray['views'] = 'not_add';
                    }
                }else{
                    $postArray['views'] = 'not_add';
                }
            }
        echo json_encode($postArray);
    }
    
    public function actionAddcomment()
    {
        $result = [];
        if($_POST['commentContent']){
            $modeComment = new Postcomment();            
            $modeComment->scenario = 'add_comment';
            $modeComment->content = $_POST['commentContent'];            
            $modeComment->user_id = \Yii::$app->user->id;            
            $modeComment->post_id = $_POST['post_id'];            
            if($modeComment->save()){
                if(!empty($_POST['commentFiles'])){
                    $filesArray = json_decode($_POST['commentFiles']);
                    if(is_array($filesArray)){
                        foreach($filesArray as $file){
                            $modelNewCommentFile = new Postcommentfiles();
                            $modelNewCommentFile->comment_id = $modeComment->id;
                            $modelNewCommentFile->file_src = $file;
                            if($modelNewCommentFile->save()){
                            }
                        }
                    }
                }
                $modelPost = Post::find()->where(['id' => $_POST['post_id']])->one();
                $modelNewNotification = new Universitynotitfication();
                $modelNewNotification->scenario = 'add_notification';
                $modelNewNotification->user_id = \Yii::$app->user->id;
                $modelNewNotification->university_id = $modelPost->university_id;
                $modelNewNotification->event_type = 'comment_to_post';
                $modelNewNotification->event_id = $modeComment->id;
                if($modelNewNotification->save()){
                }
                
                
                        $query = new Query();
                        $query->select(['*', 'id' => '{{%post_comment}}.id', 'commentCreate' => '{{%post_comment}}.date_create'])
                                        ->from('{{%post_comment}}')
                                        ->join('LEFT JOIN',
                                                    '{{%user}}',
                                                    '{{%user}}.id = {{%post_comment}}.user_id')
                                        ->join('LEFT JOIN',
                                                    '{{%user_info}}',
                                                    '{{%user_info}}.id_user = {{%post_comment}}.user_id')
                                ->where(['{{%post_comment}}.id' => $modeComment->id]);
                        $command = $query->createCommand();
                        $modComment = $command->queryOne();
                        $result['data'] = ['comment' => $modComment, 'files' => Postcommentfiles::find()->where(['comment_id' => $modeComment->id])->asArray()->all()];
                            
                $result['status'] = 'success';
            }else{
                $result['status'] = 'error';
            }
        }
        echo json_encode($result);
    }
    
    public function actionToprated($slug)
    {
        
        $model = $this->findUniversity($slug);
        $model_event = new Event();
        $model_news = new News();
        
        $events_base = Event::find()
        ->where(['place_type' => 'university', 'place_id' => $model->id])
        ->status(Event::STATUS_ON)
        ->orderBy('date_create DESC')
        ->all();

        $news_carousel = Event::find()
        ->where(['place_type' => 'university', 'place_id' => $model->id])
        ->orderBy('date_create DESC')
        ->status(News::STATUS_ON)
        ->all();

        //echo '<pre>'; print_r($events_base);exit;
        $events = [];
        if ($events_base)
            foreach ($events_base AS $value) {
                $event = new \yii2fullcalendar\models\Event();
                $event->id = $value->id;
                $event->title = $value->title;
                $event->start = date('Y-m-d\TH:i:s\Z', strtotime($value->date_start));
                $event->end = date('Y-m-d\TH:i:s\Z', strtotime($value->date_end));
                $events[] = $event;
            }
        
        $date_today = date("Y-m-d");
        $query = Event::find()->where("date_start <= '".$date_today."' and date_end >= '".$date_today."'")
                ->orWhere(['LIKE', 'date_create', $date_today]);
        $modelEvent = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 100]]);
        
        if (!is_null($model)) {
            return $this->render('top_rated', [
                'modelEvent'=>$modelEvent->getModels(),
                'model' => $model,
                'model_event' => $model_event,
                'model_news' => $model_news,
                'events' => $events,
                'news_carousel' => $news_carousel,
                'events_base'=>$events_base,
                'count_notif'=>count(Follower::find()->all())
            ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    /**
     * Creates a new University model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new University();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing University model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /** Ajax follow and unfollow
     * @return array
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionFollow(){
        if (\Yii::$app->request->isAjax) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            $data = \Yii::$app->request->post();
            $check_exist = Follower::find()
                ->where(
                    [
                        'id_user'=>\Yii::$app->user->getId(),
                        'id_object'=>$data['id'],
                        'type_object'=>'university'
                    ]
                )->one();

            if(!empty($check_exist)){
                if($check_exist->delete()){
                    return ['status'=>'ok','btn_title'=>' Follow', 'btn_class'=>'mr-unfollow', 'count'=>count(Follower::find()->all())];
                }else{
                    return ['status'=>'error','msg'=>'Server error'];
                }
            }else{
                $follower = new Follower();
                $follower->id_object = $data['id'];
                $follower->id_user= \Yii::$app->user->getId();
                $follower->type_object = 'university';
                if($follower->save(false)){
                    return ['status'=>'ok','btn_title'=>' Unfollow', 'btn_class'=>'mr-unfollow', 'count'=>count(Follower::find()->all())];
                }
                return ['status'=>'error','msg'=>'Server error'];
            }

        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }

    }

    /** Show about page
     * @param $slug
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAbout($slug){
        
        $model = $this->findUniversity($slug);
        $model_event = new Event();
        $model_news = new News();
        
        $events_base = Event::find()
        ->where(['place_type' => 'university', 'place_id' => $model->id])
        ->status(Event::STATUS_ON)
        ->orderBy('date_create DESC')
        ->all();

        $news_carousel = Event::find()
        ->where(['place_type' => 'university', 'place_id' => $model->id])
        ->orderBy('date_create DESC')
        ->status(News::STATUS_ON)
        ->all();

        //echo '<pre>'; print_r($events_base);exit;
        $events = [];
        if ($events_base)
            foreach ($events_base AS $value) {
                $event = new \yii2fullcalendar\models\Event();
                $event->id = $value->id;
                $event->title = $value->title;
                $event->start = date('Y-m-d\TH:i:s\Z', strtotime($value->date_start));
                $event->end = date('Y-m-d\TH:i:s\Z', strtotime($value->date_end));
                $events[] = $event;
            }
        
        if (!is_null($model)) {
            return $this->render('about', [
                'model' => $model,
                'model_event' => $model_event,
                'model_news' => $model_news,
                'events' => $events,
                'news_carousel' => $news_carousel,
                'events_base'=>$events_base,
                'count_notif'=>count(Follower::find()->all())
            ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $slug
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAllNews($slug)
    {
        $model = $this->findUniversity($slug);
        $searchModel = new NewsSearch();
        $searchModel->place_type = 'university';
        $searchModel->place_id = $model->id;

      //  print_r($searchModel->getImg(1));exit;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->setSort([
            'defaultOrder' => [
                'date_create' => SORT_DESC,
            ],
        ]);
        $dataProvider->setPagination(
            [
                'pageSize' => 15,
                'pageParam' => 'page_news'
            ]
        );

        return $this->render('news', [
            'dataProvider' => $dataProvider,
            'searchModel'=>$searchModel,
            'university'=>$slug,
            'model'=>$model
        ]);
    }

    /**
     * @param $slug
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionCourses($slug){
        $model = $this->findUniversity($slug);
        
        $news_carousel = Event::find()
        ->where(['place_type' => 'university', 'place_id' => $model->id])
        ->orderBy('date_create DESC')
        ->status(News::STATUS_ON)
        ->all();
        $majorArray = ArrayHelper::map(Major::find()->where(['university_id' => $model->id])->all(), 'id', 'name');
        $modelUserInfo = UserInfo::find()->where(['id_user' => \Yii::$app->user->id])->one();
        if($_POST){
            if(isset($_POST['UserInfo'])){
                $modelUserInfo->scenario = 'update_major';
                $modelUserInfo->major_id = $_POST['UserInfo']['major_id'];
                if($modelUserInfo->save()){
                    
                }
            }
        }
        
        if($modelUserInfo->major_id != 0){
            $queryCourses = Course::find()->where(['id_major' => $modelUserInfo->major_id]);
            $modelCourses = new ActiveDataProvider(['query' => $queryCourses, 'pagination' => ['pageSize' => 10]]);
            
            return $this->render('courses',[
                'modelCourses' => $modelCourses->getModels(),
                'pagination' => $modelCourses->pagination,
                'count' => $modelCourses->pagination->totalCount,
                'model'=>$model,
                'modelUserInfo'=>$modelUserInfo,
                'majorArray'=>$majorArray,
                'news_carousel'=>$news_carousel,
            ]);
        }else{
            $modelCourses = null;
            return $this->render('courses',[
                'modelCourses'=>$modelCourses,
                'model'=>$model,
                'modelUserInfo'=>$modelUserInfo,
                'majorArray'=>$majorArray,
                'news_carousel'=>$news_carousel,
            ]);
        }
        
    }
    
    public function actionMembers($slug){
        $model = $this->findUniversity($slug);
        
        $news_carousel = Event::find()
        ->where(['place_type' => 'university', 'place_id' => $model->id])
        ->orderBy('date_create DESC')
        ->status(News::STATUS_ON)
        ->all();
        
        $query = new Query;
        $query->select(['*', 'id' => '{{%user}}.id'])
            ->from('{{%user}}')
            ->join('LEFT JOIN',
                    '{{%user_info}}',
                    '{{%user_info}}.id_user = {{%user}}.id')
            ->where(['{{%user_info}}.university_id' => $model->id]);
        
        $modelUser = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 10]]);
        
        /*
        $dataProvider = new ActiveDataProvider([
            'query'  => Follower::find()
                ->where('id_user <> '.\Yii::$app->user->getId())
                ->andWhere(
                [
                    'type_object'=>'university',
                    'id_object'=> $model->id
                ]
            ),
            'pagination' => [
                'pageSize' => 10,
                'pageParam' => 'page_members'
            ],
        ]); */
        


        return $this->render('members',[
            //'dataProvider'=>$dataProvider,
            'modelUser' => $modelUser->getModels(),
            'pagination' => $modelUser->pagination,
            'count' => $modelUser->pagination->totalCount,
            'model'=>$model,
            'news_carousel'=>$news_carousel,
        ]);
    }

    /**
     * @param $slug
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionGeteventsajax(){
        $date = $_POST['date'];

        if(isset($_POST['class_id'])){
            $class_id = $_POST['class_id'];
            $modelEvent = Event::find()->where("date_start <= '".$date."' and date_end >= '".$date."'")
                    ->andWhere(['class_id' => $class_id])->orWhere(['LIKE', 'date_create', $date])
                    ->andWhere(['class_id' => $class_id])->asArray()->all();
        }else{
            $modelEvent = Event::find()->where("date_start <= '".$date."' and date_end >= '".$date."'")
                    ->orWhere(['LIKE', 'date_create', $date])->asArray()->all();
        }
        echo \yii\helpers\Json::encode($modelEvent);
    }
    
    public function actionGeteventsdata(){
        $event_id = $_POST['event_id'];
        $modelEvent = Event::find()->where(['id' => $event_id])->asArray()->one();
        echo json_encode($modelEvent);
    }
    
    public function actionAllEvents($slug){
        $model = $this->findUniversity($slug);
        $news_carousel = Event::find()
        ->where(['place_type' => 'university', 'place_id' => $model->id])
        ->orderBy('date_create DESC')
        ->status(News::STATUS_ON)
        ->all();
        
        $searchModel = new EventSearch();
        $model_event = new Event();
        $model_news = new News();
        $searchModel->place_type = 'university';
        $searchModel->place_id = $model->id;
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->setSort([
            'defaultOrder' => [
                'date_create' => SORT_DESC,
            ],
        ]);
        $dataProvider->setPagination(
            [
                'pageSize' => 15,
                'pageParam' => 'page_events'
            ]
        );
        
        $date_today = date("Y-m-d");
        $query = Event::find()->where("date_start <= '".$date_today."' and date_end >= '".$date_today."'")
                ->orWhere(['LIKE', 'date_create', $date_today]);
        $modelEvent = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 100]]);
        
        return $this->render('events',[
            'modelEvent'=>$modelEvent->getModels(),
            'model_event'=>$model_event,
            'model_news'=>$model_news,
            'dataProvider'=>$dataProvider,
            'searchModel'=>$searchModel,
            'university'=>$slug,
            'news_carousel'=>$news_carousel,
            'model'=>$model
        ]);
    }


    /**
     * @param $slug
     * @param $type
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionSettings($slug){
        $model = $this->findUniversity($slug);
        $model_event = new Event();
        $model_news = new News();
        
        $events_base = Event::find()
        ->where(['place_type' => 'university', 'place_id' => $model->id])
        ->status(Event::STATUS_ON)
        ->orderBy('date_create DESC')
        ->all();

        $news_carousel = Event::find()
        ->where(['place_type' => 'university', 'place_id' => $model->id])
        ->orderBy('date_create DESC')
        ->status(News::STATUS_ON)
        ->all();
        
        $arrayUser = [];
        $modelUser = User::find()->all();
        foreach($modelUser as $user){
            $arrayUser[$user->id] = $user->name.' '.$user->surname;
        }
        $data = Yii::$app->request->post();
        if($_POST){
            if($model->load($data)){
                if ($model->save()) {
                     $modelUserInfo = UserInfo::find()->where(['university_id' => $model->id])->asArray()->all();
                    $userArrayId = [];
                    foreach($modelUserInfo as $user){
                        $userArrayId[] = $user['id_user'];                    
                    }
                    
                    $modelNotification = new Notification();
                    $modelNotification->scenario = 'add_notification';
                    $modelNotification->module = 'university';
                    $modelNotification->from_object_id = $model->id;
                    $modelNotification->to_object_id = $model->id;
                    $modelNotification->university_id = $model->id;
                    $modelNotification->notification_type = 'update_university';
                    $modelNotification->save();
                    if($modelNotification->save()){
                        foreach($userArrayId as $user_id){
                            $modelNotificationUser = new Notificationusers();
                            $modelNotificationUser->scenario = 'new_notification';
                            $modelNotificationUser->user_id = $user_id;
                            $modelNotificationUser->notification_id = $modelNotification->id;
                            $modelNotificationUser->save();
                        }
                    }
                    
                }else{
                    //var_dump($model);exit;
                }
            }
        }
        
        $events = [];
        if ($events_base)
            foreach ($events_base AS $value) {
                $event = new \yii2fullcalendar\models\Event();
                $event->id = $value->id;
                $event->title = $value->title;
                $event->start = date('Y-m-d\TH:i:s\Z', strtotime($value->date_start));
                $event->end = date('Y-m-d\TH:i:s\Z', strtotime($value->date_end));
                $events[] = $event;
            }
        
        if (!is_null($model)) {
            return $this->render('settings', [
                'model' => $model,
                'arrayUser' => $arrayUser,
                'model_event' => $model_event,
                'model_news' => $model_news,
                'events' => $events,
                'news_carousel' => $news_carousel,
                'events_base'=>$events_base,
                'count_notif'=>count(Follower::find()->all())
            ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionSliderpost($slug){
        $model = $this->findUniversity($slug);
        
        $arrayUser = [];
        $modelUser = User::find()->all();
        foreach($modelUser as $user){
            $arrayUser[$user->id] = $user->name.' '.$user->surname;
        }
        $data = Yii::$app->request->post();
        if($_POST){
            if($model->load($data)){
                if ($model->save()) {
                     
                }else{
                    var_dump($model);exit;
                }
            }
        }
        
        if (!is_null($model)) {
            return $this->render('sliderpost', [
                'model' => $model,
                'arrayUser' => $arrayUser,
                'news_carousel' => $news_carousel,
            ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = University::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSavedropedfile(){
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/images/users_images/';   //2
        // mkdir($storeFolder);
        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
              mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }
            
            $tempFile = $_FILES['file']['tmp_name'];          //3
            $fileType = $_FILES['file']['type'];
            
            $arrayFormat = [
                //videos
                'video/x-flv' => 'flv',
                'video/mp4' => 'mp4',
                'video/MP2T' => 'ts',
                'application/x-mpegURL' => 'm3u8',
                'video/3gpp' => '3gp',
                'video/quicktime' => 'mov',
                'video/x-msvideo' => 'avi',
                'video/x-ms-wmv' => 'wmv',
                'audio/x-m4a' => 'm4a',
                //
                //images
                'image/png' => 'png',
                'image/jpeg' => 'jpe',
                'image/jpeg' => 'jpeg',
                'image/jpeg' => 'jpg',
                'image/gif' => 'gif',
                'image/bmp' => 'bmp',
                'image/vnd.microsoft.icon' => 'ico',
                'image/tiff' => 'tiff',
                'image/tiff' => 'tif',
                'image/svg+xml' => 'svg',
                'image/svg+xml' => 'svgz'
                //
            ];
            
            $keyStatus = array_key_exists($fileType, $arrayFormat);
            if ($keyStatus == true) {
                $fileType = $arrayFormat[$fileType];
            } else {
                $fileType = 'jpeg';
            }
            
            $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;  //4

            $for_name = time();
            $randNumber = rand(10,99).rand(10,99);
            $targetFile =  $targetPath.$for_name.'_'.$randNumber.'.'.$fileType;  //5

            if(move_uploaded_file($tempFile,$targetFile)){
                return \Yii::$app->user->id.'/'.$for_name.'_'.$randNumber.'.'.$fileType;
            }else{
                var_dump(move_uploaded_file($tempFile,$targetFile));
            }
        }

    }
    
    public function actionSaveduniversityphoto(){
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/uploads/university/';   //2
        // mkdir($storeFolder);
        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
              mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }
              $tempFile = $_FILES['file']['tmp_name'];          //3

              $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;  //4

              $for_name = time();

              $targetFile =  $targetPath.$for_name.'.jpg';  //5

              move_uploaded_file($tempFile,$targetFile); //6
              return '/uploads/university/'.\Yii::$app->user->id.'/'.$for_name.'.jpg';
        }

    }
    
    public function actionSaveuniversitypostfile(){
        $modelUserInfo = UserInfo::find()->where(['id_user' => \Yii::$app->user->id])->one();
        $newModelUserEvents =  new UserEvents();

        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/uploads/university/';   //2
        // mkdir($storeFolder);
        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
              mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }
            $tempFile = $_FILES['file']['tmp_name'];
            
            $fileType = $_FILES['file']['type'];    //3
            $arrayFormat = [
                //videos
                'video/x-flv' => 'flv',
                'video/mp4' => 'mp4',
                'video/MP2T' => 'ts',
                'application/x-mpegURL' => 'm3u8',
                'video/3gpp' => '3gp',
                'video/quicktime' => 'mov',
                'video/x-msvideo' => 'avi',
                'video/x-ms-wmv' => 'wmv',
                'audio/x-m4a' => 'm4a',
                //
                //images
                'png' => 'image/png',
                'image/jpeg' => 'jpe',
                'image/jpeg' => 'jpeg',
                'image/jpeg' => 'jpg',
                'image/gif' => 'gif',
                'image/bmp' => 'bmp',
                'image/vnd.microsoft.icon' => 'ico',
                'image/tiff' => 'tiff',
                'image/tiff' => 'tif',
                'image/svg+xml' => 'svg',
                'image/svg+xml' => 'svgz'
                //
            ];
            
            $keyStatus = array_key_exists($fileType, $arrayFormat);
            if ($keyStatus == true) {
                $fileType = $arrayFormat[$fileType];
            } else {
                $fileType = 'jpeg';
            }
//3

            $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;  //4

            $for_name = time();
            $randNumber = rand(10,99).rand(10,99);
            $targetFile =  $targetPath.$for_name.'_'.$randNumber.'.'.$fileType;  //5

            move_uploaded_file($tempFile,$targetFile); //6
            return '/uploads/university/'.\Yii::$app->user->id.'/'.$for_name.'_'.$randNumber.'.'.$fileType;
        }
    }
    
    public function actionGetuniversityratinglevel(){
        $university_id = $_POST['university_id'];
        $modelUniversities = University::find()->orderBy('date_create DESC')->asArray()->all();
            $universityRatingArray = [];
            $universityCount = 0;
            foreach($modelUniversities as $university){
                    $universityCount++;
                    $modelClasses = Classes::find()->where(['id_university' => $university['id']])->asArray()->all();
                    $count_entry = 0;
                    $count_star = 0;
                    foreach($modelClasses as $class){
                        $modelRatingClass = Classreting::find()->where(['class_id' => $class['id']])->all();
                        $countUser = 0;
                        $countStar = 0;
                        foreach($modelRatingClass as $request){
                            $countUser++;
                            $countStar = $countStar + $request->rate_professor;
                        }
                        $result = '';
                        if($countUser == 0){
                            $countStarClassResult = 0;
                        }else{
                            $count_entry++;
                            $countStarClassResult = round($countStar/$countUser,1); 
                            $count_star = $count_star + $countStarClassResult;
                        }
                    }
                    if($count_entry == 0){
                        $countStarResult = 0;
                    }else{
                        $countStarResult = round($count_star/$count_entry,1); 
                    }
                    $universityRatingArray[$university['id']] = $countStarResult;
            }
            arsort($universityRatingArray);
            $k = 0;
            $result_rat = '';
            foreach($universityRatingArray as $un_id => $univerRat){
                $k++;
                if($un_id == $university_id){
                    $result_rat = $k;
                }
            }
            echo json_encode(['k' => $result_rat, 'c' => $universityCount]);
            //var_dump($universityRatingArray);
            
    }
    
    public function actionGetclasstopuser()
    {
        $university_id = $_POST['university_id'];
        $queryUser = new Query();
        $queryUser->select(['*', 'id' => '{{%user}}.id'])
                            ->from('{{%user}}')
                            ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%user}}.id')
                    ->where(['{{%user_info}}.university_id' => $university_id, 'academic_status' => '2']);

        $command = $queryUser->createCommand();
        $modelUsers = $command->queryAll();
        $usersId = [];
        foreach($modelUsers as $user){
            $usersId[] = $user['id'];
        }
        
        $userRating = [];
        foreach($usersId as $user_id){
            $modelRatingUser = Ratingrequest::find()->where(['to_object_id' => $user_id])->all();
            $countUser = 0;
            $countStar = 0;
            foreach($modelRatingUser as $request){
                $countUser++;
                $countStar = $countStar + $request['rating_count'];
            }
            $result = '';
            if($countUser == 0){
                $countStarResult = 0;
            }else{
                $countStarResult = round($countStar/$countUser,1);
            }
            $userRating[$user_id] =  $countStarResult;
            
        }

        $userArray = [];
        foreach($userRating as $user_id => $rating){
            $k = $k + 1;
            if($k < 6){
                $query = new Query();
                $query->select(['*', 'id' => '{{%user}}.id', 'username' => '{{%user}}.name', 'user_role_name' => '{{%user_role}}.name'])
                                ->from('{{%user}}')
                                ->join('LEFT JOIN',
                                            '{{%user_info}}',
                                            '{{%user_info}}.id_user = {{%user}}.id')
                                ->join('LEFT JOIN',
                                            '{{%user_role}}',
                                            '{{%user_role}}.id = {{%user_info}}.academic_status'
                                        )->where(['{{%user}}.id' => $user_id]);
                $command = $query->createCommand();
                $modelUser = $command->queryOne();
                
                $userArray[$user_id]['user_info'] = $modelUser;
                $userArray[$user_id]['user_rating'] = $rating;
            }else{
                break;
            }
        }
            
        echo json_encode($userArray);
        
    }
    
    public function actionGetclasstopprofessor()
    {
        $university_id = $_POST['university_id'];
        $modelClasses = Classes::find()->where(['id_university' => $university_id])->asArray()->all();
        $professor_id = [];
        foreach($modelClasses as $class){
            $professor_id[] = $class['professor'];
        }
        
        $professorRating = [];
        foreach($professor_id as $id){
            $modelRatingrequest = Ratingrequest::find()->where(['to_object_id' => $id, 'type' => 'professor'])->asArray()->all();
            
            $countUser = 0;
            $countStar = 0;
            foreach($modelRatingrequest as $rating){
                $countUser++;
                $countStar = $countStar + $rating['rating_count'];
            }
            $result = '';
            if($countUser == 0){
                $countStarResult = 0;
            }else{
                $countStarResult = round($countStar/$countUser,1); 
            }
            
            $professorRating[$id] =  $countStarResult;
        }
        arsort($professorRating);
        $k = 0;
        $professorArray = [];
        foreach($professorRating as $professor_id => $rating){
            $k = $k + 1;
            if($k < 6){
                $query = new Query();
                $query->select(['*', 'id' => '{{%user}}.id', 'username' => '{{%user}}.name', 'user_role_name' => '{{%user_role}}.name'])
                                ->from('{{%user}}')
                                ->join('LEFT JOIN',
                                            '{{%user_info}}',
                                            '{{%user_info}}.id_user = {{%user}}.id')
                                ->join('LEFT JOIN',
                                            '{{%user_role}}',
                                            '{{%user_role}}.id = {{%user_info}}.academic_status'
                                        )->where(['{{%user}}.id' => $professor_id]);
                $command = $query->createCommand();
                $modelUser = $command->queryOne();
        
                $professorArray[$professor_id]['professor_info'] = $modelUser;
                $professorArray[$professor_id]['professor_rating'] = $rating;
            }else{
                break;
            }
//            ksort($professorArray);
        }
        echo json_encode($professorArray);
    }
    
    public function actionGettopuniversity()
    {
        $modelUniversity = University::find()->asArray()->all();
        $universityId = [];
        foreach($modelUniversity as $university){
            $universityId[] = $university['id'];
        }
        
        $universityRating = [];
        foreach($universityId as $id){
            $modelClasses = Classes::find()->where(['id_university' => $id])->asArray()->all();
            $count_entry = 0;
            $count_star = 0;
            foreach($modelClasses as $class){
                $modelRatingClass = Classreting::find()->where(['class_id' => $class['id']])->all();
                $countUser = 0;
                $countStar = 0;
                foreach($modelRatingClass as $request){
                    $countUser++;
                    $countStar = $countStar + $request->rate_professor;
                }
                $result = '';
                if($countUser == 0){
                    $countStarClassResult = 0;
                }else{
                    $count_entry++;
                    $countStarClassResult = round($countStar/$countUser,1); 
                    $count_star = $count_star + $countStarClassResult;
                }
            }
            if($count_entry == 0){
                $countStarResult = 0;
            }else{
                $countStarResult = round($count_star/$count_entry,1); 
            }
            
            $universityRating[$id] =  $countStarResult;
        }
        arsort($universityRating);
        
        $universityArray = [];
        foreach($universityRating as $university_id => $rating){
            $k = $k + 1;
            if($k < 6){
                $modelUniversity = University::find()->where(['id' => $university_id])->asArray()->one();
                
                $universityArray[$university_id]['university_info'] = $modelUniversity;
                $universityArray[$university_id]['university_rating'] = $rating;
            }else{
                break;
            }
        }
        echo json_encode($universityArray);
        
    }
    
    public function actionGettopmajor()
    {
        $university_id = $_POST['university_id'];
        $modelMajors = Major::find()->where(['university_id' => $university_id])->asArray()->all();
        
        $majorId = [];
        foreach($modelMajors as $major){
            $majorId[] = $major['id'];
        }
        
        $majorRating = [];
        foreach($majorId as $id){
            $modelClasses = Classes::find()->where(['id_major' => $id])->asArray()->all();
            $count_entry = 0;
            $count_star = 0;
            foreach($modelClasses as $class){
                $modelRatingClass = Classreting::find()->where(['class_id' => $class['id']])->all();
                $countUser = 0;
                $countStar = 0;
                foreach($modelRatingClass as $request){
                    $countUser++;
                    $countStar = $countStar + $request->rate_professor;
                }
                $result = '';
                if($countUser == 0){
                    $countStarClassResult = 0;
                }else{
                    $count_entry++;
                    $countStarClassResult = round($countStar/$countUser,1); 
                    $count_star = $count_star + $countStarClassResult;
                }
            }
            if($count_entry == 0){
                $countStarResult = 0;
            }else{
                $countStarResult = round($count_star/$count_entry,1); 
            }
            
            $majorRating[$id] =  $countStarResult;
        }
        arsort($majorRating);
        
        $majorArray = [];
        foreach($majorRating as $major_id => $rating){
            $k = $k + 1;
            if($k < 6){
                $modelMajor = Major::find()->where(['id' => $major_id])->asArray()->one();
                
                $majorArray[$major_id]['major_info'] = $modelMajor;
                $majorArray[$major_id]['major_rating'] = $rating;
            }else{
                break;
            }
        }
        echo json_encode($majorArray);
        
    }

}
