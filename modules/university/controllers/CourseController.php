<?php

namespace app\modules\university\controllers;

use app\models\UniversityCourse;
use Yii;
use app\modules\university\models\Course;
use app\modules\university\models\search\CourseSearch;
use yii\bootstrap\ActiveForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * CourseController implements the CRUD actions for Course model.
 */
class CourseController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Course models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CourseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Course model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Course model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Course();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Course model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Course model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }



    /**
     * Creates a new course ajax model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionCreateAjax($id_univesity = 0)
    {
        $model = new Course();
        $data = Yii::$app->request->post();

        if(!Yii::$app->request->isAjax){
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if($data['ajax_submit']){
            if ($model->load($data)) {
                if ($model->save()) {
                    Yii::$app->response->format = Response::FORMAT_JSON;

                    if(!empty($id_univesity)){
                        $university_major = new UniversityCourse();
                        $university_major->id_university = (int)$id_univesity;
                        $university_major->id_course = $model->id;
                        $university_major->save(false);
                    }

                    return ['status'=>'ok'];
                }else{
                    if (Yii::$app->request->isAjax) {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return   ['status'=>'valid_error','messages'=>ActiveForm::validate($model)];
                    }
                }
            }else{
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return   ['status'=>'valid_error','messages'=>ActiveForm::validate($model)];
                }
            }
        }else{
            if (Yii::$app->request->isAjax) {
                $model->load($data);
                Yii::$app->response->format = Response::FORMAT_JSON;
                return   ['status'=>'valid_error','messages'=>ActiveForm::validate($model)];
            }
        }
    }

    /** Ajax  addd major to university
     * @throws NotFoundHttpException
     */
    public function actionAddAjaxUniversity(){

        if(!Yii::$app->request->isAjax){
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $data = Yii::$app->request->post();
        Yii::$app->response->format = Response::FORMAT_JSON;

        $major = new Course();
        $check = $major->checkUniversity($data['iduniversity'],$data['idcourse']);

        if($check){
            if($check->delete()){
                return ['status'=>'ok'];
            }else{
                return ['status'=>'error','msg'=>'Server error!'];
            }
        }else{
            $model = new UniversityCourse();
            $model->id_course = $data['idcourse'];
            $model->id_university = $data['iduniversity'];

            if($model->save(false)){
                return ['status'=>'ok'];
            }else{
                return ['status'=>'error','msg'=>'Server error!'];
            }
        }
    }

    /**
     * Finds the Course model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Course the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Course::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}