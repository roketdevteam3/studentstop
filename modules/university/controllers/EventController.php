<?php

namespace app\modules\university\controllers;

use Yii;
use app\models\Event;
use app\models\News;
use app\models\search\EventSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\modules\university\components\UniverseController;
use app\modules\university\models\University;
use yii\filters\VerbFilter;

use app\models\Creditsactioncount;
use app\models\Creditsaction;
use app\models\Notification;
use app\models\Notificationusers;

use yii\widgets\ActiveForm;
use app\modules\university\models\Universitynotitfication;
use \yii\web\Response;
use yii\web\UploadedFile;
use app\helpers\Image;

/**
 * EventController implements the CRUD actions for Event model.
 */
class EventController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Event models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Event model.
     * @param integer $slug
     * @return mixed
     */
    public function actionView($slug,$slug_u)
    {
        $model_u = University::find()->where("slug = '{$slug_u}' ")->one();
        //$model_u = $this->findOne($slug_u);
        
        $news_carousel = Event::find()
        ->where(['place_type' => 'university', 'place_id' => $model_u->id])
        ->orderBy('date_create DESC')
        ->status(News::STATUS_ON)
        ->all();
        
        $model = $this->findModelBySlug($slug);
        if($_POST){
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                
            }
        }
        return $this->render('view', [
            'model' => $model,
            'model_u' => $model_u,
            'news_carousel' => $news_carousel,
        ]);
    }

    /**
     * Creates a new Event model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        //exit;
        if(isset($_POST['Event'])){
            $model = new Event();
        }elseif(isset($_POST['News'])){
            $model = new News();
        }
        $data = Yii::$app->request->post();
       
            if($data['ajax_submit']){

                if ($model->load($data)) {
                    $fileInstanse = UploadedFile::getInstance($model, 'image');
                    $model->image = $fileInstanse;

                    if (isset($fileInstanse))
                        if ($model->validate(['image'])) {
                            $model->image = Image::upload($model->image, 'event');
                        } else {
                            Yii::$app->getSession()->setFlash('error', $model->getErrors());
                        }
                    $model->status = 1;
                    if ($model->save()) {
                        
                        
                        
                        
                        
                        //add credits action begin
                            $modelCreditsaction = Creditsaction::find()->where(['object_id' => \Yii::$app->user->id, 'action' => 'add_event'])->one();
                            if($modelCreditsaction){
                                $modelCreditsaction->scenario = 'change_action_count';
                                $modelCreditsaction->action_count = $modelCreditsaction->action_count + 1;
                                if($modelCreditsaction->save()){
                                }
                                
                                
                                if((($modelCreditsaction->action_count % 10) == 0)){
                                    $modelCreditsactionCount = Creditsactioncount::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                    if($modelCreditsactionCount){
                                        $modelCreditsactionCount->scenario = 'update_credits_count';
                                        $modelCreditsactionCount->credits_count = $newModelCreditsactionCount->credits_count + 2;
                                        $modelCreditsactionCount->date_update = date("Y-m-d H:i:s");
                                        if($modelCreditsactionCount->save()){
                                            
                                        }
                                    }else{
                                        $newModelCreditsactionCount = new Creditsactioncount();
                                        $newModelCreditsactionCount->scenario = 'add_credits_count';
                                        $newModelCreditsactionCount->user_id = \Yii::$app->user->id;
                                        $newModelCreditsactionCount->credits_count = 2;
                                        if($newModelCreditsactionCount->save()){

                                        }
                                    }
                                    
                                    $modelCredits = \app\models\Usercredits::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                    if($modelCredits){
                                        $modelCredits->scenario = 'update_credits_count';
                                        $modelCredits->credit_count = $modelCredits->credit_count + 2;
                                        if($modelCredits->save()){
                                        }                            
                                    }else{
                                        $modelNewCredits = new \app\models\Usercredits();
                                        $modelNewCredits->scenario = 'add_credits';
                                        $modelNewCredits->user_id = \Yii::$app->user->id;
                                        $modelNewCredits->credit_count = 2;
                                        $modelNewCredits->many = 0;
                                        if($modelNewCredits->save()){

                                        }
                                    }
                                    
                                        $modelNotification = new Notification();
                                        $modelNotification->scenario = 'add_notification';
                                        $modelNotification->module = 'university';
                                        $modelNotification->from_object_id = \Yii::$app->user->id;
                                        $modelNotification->to_object_id = $model->id;
                                        $modelNotification->notification_type = 'event_credits';
                                        $modelNotification->university_id = $model->place_id;
                                        if($modelNotification->save()){
                                            $modelNotificationUser = new Notificationusers();
                                            $modelNotificationUser->scenario = 'new_notification';
                                            $modelNotificationUser->user_id = \Yii::$app->user->id;
                                            $modelNotificationUser->notification_id = $modelNotification->id;
                                            $modelNotificationUser->save();
                                        }
                                }
                                
                                
                                
                            }else{
                                        $newModelCreditsaction = new Creditsaction();
                                        $newModelCreditsaction->scenario = 'add_credits';
                                        $newModelCreditsaction->object_id = \Yii::$app->user->id;
                                        $newModelCreditsaction->action = 'add_event';
                                        $newModelCreditsaction->action_count = 1;
                                        if($newModelCreditsaction->save()){
                                            $modelCreditsactionCount = Creditsactioncount::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                            if($modelCreditsactionCount){
                                                $modelCreditsactionCount->scenario = 'update_credits_count';
                                                $modelCreditsactionCount->credits_count = $modelCreditsactionCount->credits_count + 2;
                                                $modelCreditsactionCount->date_update = date("Y-m-d H:i:s");
                                                if($modelCreditsactionCount->save()){
                                                }
                                            }else{
                                                $newModelCreditsactionCount = new Creditsactioncount();
                                                $newModelCreditsactionCount->scenario = 'add_credits_count';
                                                $newModelCreditsactionCount->user_id = \Yii::$app->user->id;
                                                $newModelCreditsactionCount->credits_count = 2;
                                                if($newModelCreditsactionCount->save()){

                                                }
                                            }
                                        }
                                        $modelCredits = \app\models\Usercredits::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                        if($modelCredits){
                                            $modelCredits->scenario = 'update_credits_count';
                                            $modelCredits->credit_count = $modelCredits->credit_count + 2;
                                            if($modelCredits->save()){
                                            }                            
                                        }else{
                                            $modelNewCredits = new \app\models\Usercredits();
                                            $modelNewCredits->scenario = 'add_credits';
                                            $modelNewCredits->user_id = \Yii::$app->user->id;
                                            $modelNewCredits->credit_count = 2;
                                            $modelNewCredits->many = 0;
                                            if($modelNewCredits->save()){

                                            }
                                        }

                                        $modelNotification = new Notification();
                                        $modelNotification->scenario = 'add_notification';
                                        $modelNotification->module = 'university';
                                        $modelNotification->from_object_id = \Yii::$app->user->id;
                                        $modelNotification->to_object_id = $model->id;
                                        $modelNotification->notification_type = 'first_event_credits';
                                        $modelNotification->university_id = $model->place_id;
                                        if($modelNotification->save()){
                                            $modelNotificationUser = new Notificationusers();
                                            $modelNotificationUser->scenario = 'new_notification';
                                            $modelNotificationUser->user_id = \Yii::$app->user->id;
                                            $modelNotificationUser->notification_id = $modelNotification->id;
                                            $modelNotificationUser->save();
                                        }

                            }
                    //add credits action end
                        
                        
                        
                        
                        $modelUserInfo = \app\modules\users\models\UserInfo::find()->where(['university_id' => $model->place_id])->asArray()->all();
                        $userArrayId = [];
                        foreach($modelUserInfo as $user){
                            $userArrayId[] = $user['id_user'];                    
                        }
                        
                        $modelNotification = new Notification();
                        $modelNotification->scenario = 'add_notification';
                        $modelNotification->module = 'university';
                        $modelNotification->from_object_id = $model->place_id;
                        $modelNotification->to_object_id = $model->id;
                        $modelNotification->notification_type = 'add_event';
                        $modelNotification->university_id = $model->place_id;
                        if($modelNotification->save()){
                            foreach($userArrayId as $user_id){
                                $modelNotificationUser = new Notificationusers();
                                $modelNotificationUser->scenario = 'new_notification';
                                $modelNotificationUser->user_id = $user_id;
                                $modelNotificationUser->notification_id = $modelNotification->id;
                                $modelNotificationUser->save();
                            }                
                        }   
                        
                        Yii::$app->response->format = Response::FORMAT_JSON;
//                            $modelNewNotification = new Universitynotitfication();
//                            $modelNewNotification->scenario = 'add_notification';
//                            $modelNewNotification->user_id = \Yii::$app->user->id;
//                            $modelNewNotification->university_id = $model->place_id;
//                            $modelNewNotification->event_type = 'event';
//                            $modelNewNotification->event_id = $model->id;
//                            if($modelNewNotification->save()){
//                            }
                        return ['status'=>'ok'];
                    }else{
                        if (Yii::$app->request->isAjax) {
                            Yii::$app->response->format = Response::FORMAT_JSON;
                            return   ['status'=>'valid_error','messages'=>ActiveForm::validate($model)];
                        }
                    }
                }else{
                    if (Yii::$app->request->isAjax) {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return   ['status'=>'valid_error','messages'=>ActiveForm::validate($model)];
                    }
                }
            }else{
                if (Yii::$app->request->isAjax) {
                    $model->load($data);
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return   ['status'=>'valid_error','messages'=>ActiveForm::validate($model)];
                }
            }
    }

    /**
     * Updates an existing Event model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Event model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Event::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    protected function findModelBySlug($slug)
    {
        if (($model = Event::find()->where("slug = '{$slug}' ")->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}