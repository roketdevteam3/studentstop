<?php

namespace app\modules\university\controllers;

use app\modules\university\components\UniverseController;
use app\modules\university\models\University;
use Yii;
use app\models\News;
use app\models\search\NewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\helpers\Image;
use yii\web\UploadedFile;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends UniverseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action) {
     $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
//        $model_university = $this->findUniversity($slug);
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($slug)
    {
        return $this->render('view', [
            'model' => $this->findModelBySlug($slug),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateforuniversity()
    {
        $model = new News();
        $data = Yii::$app->request->post();
        if ($model->load($data)) {
            if($model->save()) {
                $modelUniversity = \app\models\University::find()->where(['id' => $model->place_id])->one();
                return $this->redirect(['/university/all-events/'.$modelUniversity->slug]);
            }
        }    
    }
    
    public function actionCreate()
    {
        $model = new News();
        $data = Yii::$app->request->post();
        $model_university = University::findOne($id);
        if (!$model_university)
            return $this->redirect(Yii::$app->request->referrer);

        $data = Yii::$app->request->post();

        if ($model->load($data)) {
            if (($fileInstanse1 = UploadedFile::getInstance($model, 'image'))) {

                $model->image = $fileInstanse1;
                if ($model->validate(['image'])) {
                    $model->image = Image::upload($model->image, 'news',200,200,true);
                }
                $model->owner_id = yii::$app->user->id;

                $model->status = 1;
                if ($model->save()) {

                    return $this->redirect(['index']);
                }
                return $this->refresh();
            }else{
                if ($model->save()) {
                    $model->owner_id = yii::$app->user->id;
                    return $this->redirect(['index']);
                }
            }

        } else {
            return $this->render('create', [
                'model' => $model,
                'model_university' => $model_university
            ]);
        }
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelBySlug($slug)
    {
        if (($model = News::find()->where("slug = '{$slug}' ")->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}