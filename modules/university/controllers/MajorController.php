<?php

namespace app\modules\university\controllers;

use app\models\UniversityCourseWithMajor;
use app\models\UniversityMajor;
use app\modules\university\models\University;
use Yii;
use app\modules\university\models\Major;
use app\modules\university\models\search\MajorSearch;
use yii\bootstrap\ActiveForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * MajorController implements the CRUD actions for Major model.
 */
class MajorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Major models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MajorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Major model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Major model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Major();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new majors ajax model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionCreateAjax($id_univesity = 0)
    {
        $model = new Major();
        $data = Yii::$app->request->post();

        if(!Yii::$app->request->isAjax){
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if($data['ajax_submit']){
            if ($model->load($data)) {
                if ($model->save()) {
                    Yii::$app->response->format = Response::FORMAT_JSON;

                    if(!empty($id_univesity)){
                        $university_major = new UniversityMajor();
                        $university_major->id_university = (int)$id_univesity;
                        $university_major->id_major = $model->id;
                        $university_major->save(false);
                    }

                    return ['status'=>'ok'];
                }else{
                    if (Yii::$app->request->isAjax) {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return   ['status'=>'valid_error','messages'=>ActiveForm::validate($model)];
                    }
                }
            }else{
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return   ['status'=>'valid_error','messages'=>ActiveForm::validate($model)];
                }
            }
        }else{
            if (Yii::$app->request->isAjax) {
                $model->load($data);
                Yii::$app->response->format = Response::FORMAT_JSON;
                return   ['status'=>'valid_error','messages'=>ActiveForm::validate($model)];
            }
        }
    }

    /** Ajax  addd major to university
     * @throws NotFoundHttpException
     */
    public function actionAddAjaxUniversity(){

        if(!Yii::$app->request->isAjax){
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $data = Yii::$app->request->post();
        Yii::$app->response->format = Response::FORMAT_JSON;

        $major = new Major();
        $check = $major->checkUniversity($data['iduniversity'],$data['idmajor']);

       if($check){
           if($check->delete()){
               return ['status'=>'ok'];
           }else{
               return ['status'=>'error','msg'=>'Server error!'];
           }
       }else{
           $model = new UniversityMajor();
           $model->id_major = $data['idmajor'];
           $model->id_university = $data['iduniversity'];

           if($model->save(false)){
               return ['status'=>'ok'];
           }else{
               return ['status'=>'error','msg'=>'Server error!'];
           }
       }
    }


    /** Ajax  add major to university
     * @throws NotFoundHttpException
     */
    public function actionAddAjaxCourseWithMajor(){

        if(!Yii::$app->request->isAjax){
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $data = Yii::$app->request->post();
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new UniversityCourseWithMajor();
        $check = $model->checkRelate($data['iduniversity'],$data['idmajor'],$data['idcourse']);

        if($check){
            if($check->delete()){
                return ['status'=>'ok'];
            }else{
                return ['status'=>'error','msg'=>'Server error!'];
            }
        }else{

            $model->id_major = $data['idmajor'];
            $model->id_university = $data['iduniversity'];
            $model->id_course = $data['idcourse'];

            if($model->save(false)){
                return ['status'=>'ok'];
            }else{
                return ['status'=>'error','msg'=>'Server error!'];
            }
        }
    }

    public function actionGetAjaxModalCourse($id,$university){
        if(!Yii::$app->request->isAjax){
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $data = Yii::$app->request->post();
        Yii::$app->response->format = Response::FORMAT_JSON;

        $searchModelMajor = new MajorSearch();
        $dataProviderMajor = $searchModelMajor->search(Yii::$app->request->queryParams);

        $university = University::findOne($university);
        $model_relate = new UniversityCourseWithMajor();


        $dataProviderMajor->setSort([
            'defaultOrder' => [
                'id' => SORT_DESC,
            ],
        ]);
        $dataProviderMajor->setPagination(
            [
                'pageSize' => count(MajorSearch::find()->all()),
                'pageParam' => 'page_major'
            ]
        );

        return [
            'status'=>'ok',
            'title'=>'Add majors',
            'content' => $this->renderPartial('_ajax_modal_majors',
                [
                    'dataProvider'=>$dataProviderMajor,
                    'searchModel'=>$searchModelMajor,
                    'model_u'=>$university,
                    'model_relate'=>$model_relate,
                    'id_course'=>$id,
                ]
            )
        ];

    }

    /**
     * Updates an existing Major model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Major model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Major model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Major the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Major::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}