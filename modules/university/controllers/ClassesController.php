<?php

namespace app\modules\university\controllers;

use app\models\ClassChat;
use app\models\ClassUser;
use app\models\search\ClassChatSearch;
use app\models\user\User;
use app\modules\university\models\Course;
use app\modules\university\models\Major;
use Yii;
use app\modules\university\models\Classes;
use app\modules\university\models\search\ClassesSearch;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * ClasesController implements the CRUD actions for Clases model.
 */
class ClassesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Clases models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ClassesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Clases model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if($model->checkJoin()){

            $searchModelChat = new ClassChatSearch();
            $dataProviderChat = $searchModelChat->search(Yii::$app->request->queryParams);

            $dataProviderUser = new ActiveDataProvider([
                'query'  =>User::find()
                    ->joinWith('class')
                    ->where('{{%class_user}}.id_class ='.$id)
                    ->andWhere('{{%class_user}}.id_user <> '.\Yii::$app->user->getId()),
                'pagination' => [
                    'pageSize' => 10,
                    'pageParam' => 'page_members'
                ],
            ]);

            $model_chat = new ClassChat();
            $model_chat->data_create = date('Y-m-d H:i:s');
            $model_chat->type = 1;
            $model_chat->id_class = $id;
            $model_chat->id_user = Yii::$app->user->getId();

            if ($model_chat->load(Yii::$app->request->post()) && $model_chat->save()) {
                return $this->redirect(['view', 'id' => $id]);
            }

            return $this->render('view', [
                'model' => $model,
                'dataProviderUser'=>$dataProviderUser,
                'searchModelChat'=>$searchModelChat,
                'dataProviderChat'=>$dataProviderChat,
                'model_chat'=>$model_chat,

            ]);
        }else{
            return $this->redirect('about');
        }



    }

    /**
     * Creates a new Clases model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Classes();


        $list_users = ArrayHelper::map(
            User::find()
                ->select("concat_ws(' ',`surname`,`name`) as surname_name, id")
                ->asArray()->all(),
            'id',
            'surname_name'
        );

        $list_course = ArrayHelper::map(
            Course::find()->asArray()->all(),
            'id',
            'name'
        );

        $list_majores = ArrayHelper::map(
            Major::find()->asArray()->all(),
            'id',
            'name'
        );

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'list_users'=>$list_users,
                'list_course'=>$list_course,
                'list_majores'=>$list_majores,
            ]);
        }
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionAjaxGetMajorsList($id){
        if(!Yii::$app->request->isAjax){
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $data = Yii::$app->request->post();
        Yii::$app->response->format = Response::FORMAT_JSON;

        $list_majores = ArrayHelper::map(
            Major::find()
                ->joinWith('courses')
                ->where('{{%university_course_with_major}}.id_course = '.$id)
                ->orderBy('{{%university_course_with_major}}.id_major')
                ->asArray()->all(),
            'id',
            'name'
        );

        return $list_majores;
    }

    /**
     * Updates an existing Clases model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);

        $list_users = ArrayHelper::map(
            User::find()
                ->select("concat_ws(' ',`surname`,`name`) as surname_name, id")
                ->asArray()->all(),
            'id',
            'surname_name'
        );

        $list_course = ArrayHelper::map(
            Course::find()->asArray()->all(),
            'id',
            'name'
        );

        $list_majores = ArrayHelper::map(
            Major::find()->asArray()->all(),
            'id',
            'name'
        );




        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'list_users'=>$list_users,
                'list_course'=>$list_course,
                'list_majores'=>$list_majores,
            ]);
        }
    }




    /**
     * Deletes an existing Clases model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionJoin($id){
        $model = new ClassUser();
        $data = Yii::$app->request->post();

        if(!Yii::$app->request->isAjax){
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        $check = $model->checkJoin($id);

        if($check){
            if ($check->delete()) {
                return ['status' => 'ok', 'msg'=>\Yii::t('app','You have successfully left the class!')];
            } else {
                return ['status'=>'error','msg'=>'Server error!'];
            }
        }else{
            $model->id_class = (int)$id;
            $model->id_user= \Yii::$app->user->getId();

            if ($model->save()) {
                return ['status' => 'ok', 'msg'=>\Yii::t('app','You are successfully joined to class')];
            } else {
                return ['status'=>'error','msg'=>'Server error!'];
            }
        }
    }

    /**
     * Finds the Clases model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Clases the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Classes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}