<?php

namespace app\modules\university\models;

use app\models\Follower;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use app\components\ActiveRecord;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%university}}".
 *
 * @property integer $id
 * @property integer $id_owner
 * @property string $name
 * @property string $history
 * @property integer $status
 * @property string $slug
 * @property string $date
 * @property string $date_create
 * @property string $date_update
 *
 * @property UniversityInfo[] $universityInfos
 */
class University extends ActiveRecord
{

    const STATUS_OFF = 0;
    const STATUS_ON = 1;
    const DEFAULT_IMG = 'uploads/university/default.jpg';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%university}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_owner', 'status','country_id', 'region_id'], 'integer'],
            [['name'], 'required'],
            [['history'], 'string'],
            [['location'], 'string'],
            [['year_founded'], 'string'],
            [['facebook_link'], 'string'],
            [['twitter_link'], 'string'],
            [['linkedin_link'], 'string'],
            [['date', 'date_create', 'date_update'], 'safe'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['image'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_owner' => Yii::t('app', 'Id Owner'),
            'name' => Yii::t('app', 'Name'),
            'history' => Yii::t('app', 'History'),
            'status' => Yii::t('app', 'Status'),
            'slug' => Yii::t('app', 'Slug'),
            'date' => Yii::t('app', 'Date'),
            'country_id' => Yii::t('app', 'Country'),
            'region_id' => Yii::t('app', 'States'),
            'date_create' => Yii::t('app', 'Date Create'),
            'date_update' => Yii::t('app', 'Date Update'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUniversityInfos()
    {
        return $this->hasMany(UniversityInfo::className(), ['id_university' => 'id']);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_create',
                'updatedAtAttribute' => 'date_update',
                'value' => new Expression('NOW()'),
            ],
            'slug' => [
                'class'         => 'app\behaviors\Slug',
                'in_attribute'  => 'name',
                'out_attribute' => 'slug',
                'translit'      => true
            ]
        ];
    }

    public function getCountFollowers(){
        return count(Follower::find()->all());
    }

    /** Check follow
     * @param int $id
     * @return array|null|\yii\db\ActiveRecord
     */
    public function checkFollow($id = 0){
        if(empty($id)){
            $id = $this->id;
        }
        $check_exist = Follower::find()
            ->where(
                [
                    'id_user'=>\Yii::$app->user->getId(),
                    'id_object'=>$id,
                    'type_object'=>'university'
                ]
            )->one();
        return $check_exist;
    }

    /** Get image
     * @param int $id
     * @return mixed
     */
    public function getImg($id = 0){
        if((int)$id){
            $model = $this->findOne($id);
        }else{
            $model = $this->findOne($this->id);
        }

        if(!empty($model)){
            $path = \Yii::getAlias('@webroot'.$model->image);

            if(!file_exists($path) || !is_file($path)){
                return Url::to(self::DEFAULT_IMG,true);
            }
            return Url::to($model->image,true);

        }else{
            return Url::to(self::DEFAULT_IMG,true);
        }
    } 
}
