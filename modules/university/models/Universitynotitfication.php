<?php

namespace app\modules\university\models;

use Yii;

/**
 * This is the model class for table "{{%course}}".
 *
 * @property integer $id
 * @property integer $id_major
 * @property string $name
 * @property string $description
 * @property string $date_create
 * @property string $date_update
 * @property integer $status
 *
 * @property Clases[] $classes
 * @property Major $idMajor
 */
class Universitynotitfication extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%university_notification}}';
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    public function scenarios()
    {
        return [
            'add_notification' => ['user_id', 'university_id','event_type', 'event_id'],
        ];
    }
    
    public function rules()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
        ];
    }

}