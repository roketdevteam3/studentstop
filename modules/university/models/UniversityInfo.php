<?php

namespace app\modules\university\models;

use Yii;

/**
 * This is the model class for table "{{%university_info}}".
 *
 * @property integer $id
 * @property integer $id_university
 *
 * @property University $idUniversity
 */
class UniversityInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%university_info}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_university'], 'integer'],
            [['id_university'], 'exist', 'skipOnError' => true, 'targetClass' => University::className(), 'targetAttribute' => ['id_university' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_university' => Yii::t('app', 'Id University'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUniversity()
    {
        return $this->hasOne(University::className(), ['id' => 'id_university']);
    }
}
