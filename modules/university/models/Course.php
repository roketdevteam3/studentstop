<?php

namespace app\modules\university\models;

use app\models\UniversityCourse;
use Yii;

/**
 * This is the model class for table "{{%course}}".
 *
 * @property integer $id
 * @property integer $id_major
 * @property string $name
 * @property string $description
 * @property string $date_create
 * @property string $date_update
 * @property integer $status
 *
 * @property Clases[] $classes
 * @property Major $idMajor
 */
class Course extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%course}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'status', 'creator'], 'integer'],
            [['name'], 'required'],
            [['description'], 'string'],
            [['date_create', 'date_update'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['id_major'], 'exist', 'skipOnError' => true, 'targetClass' => Major::className(), 'targetAttribute' => ['id_major' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_major' => Yii::t('app', 'Id Major'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'date_create' => Yii::t('app', 'Date Create'),
            'date_update' => Yii::t('app', 'Date Update'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClasses()
    {
        return $this->hasMany(Clases::className(), ['id_course' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMajor()
    {
        return $this->hasOne(Major::className(), ['id' => 'id_major']);
    }

    public function checkUniversity($university_id, $id = 0){
        return  UniversityCourse::find()->where(
            [
                'id_course'=>!empty($id) ? $id : $this->id,
                'id_university'=>$university_id,
            ]
        )->one();
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // Place your custom code here
             $this->creator = \Yii::$app->user->getId();
            return true;
        } else {
            return false;
        }
    }
}