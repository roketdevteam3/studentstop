<?php

namespace app\modules\university\models;

use app\models\UniversityCourseWithMajor;
use app\models\UniversityMajor;
use Yii;

/**
 * This is the model class for table "{{%major}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $date_create
 * @property string $date_update
 * @property integer $status
 *
 * @property Course[] $courses
 */
class Major extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%major}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['date_create', 'date_update'], 'safe'],
            [['status','creator'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'date_create' => Yii::t('app', 'Date Create'),
            'date_update' => Yii::t('app', 'Date Update'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasOne(UniversityCourseWithMajor::className(), ['id_major' => 'id']);
    }

    public function checkUniversity($university_id, $id = 0){
       return  UniversityMajor::find()->where(
            [
                'id_major'=>!empty($id) ? $id : $this->id,
                'id_university'=>$university_id,
            ]
        )->one();
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            $this->creator = \Yii::$app->user->getId();
            return true;
        } else {
            return false;
        }
    }
}