<?php

namespace app\modules\university\models;

use app\models\ClassUser;
use Yii;

/**
 * This is the model class for table "{{%class}}".
 *
 * @property integer $id
 * @property integer $id_course
 * @property string $name
 * @property string $description
 * @property string $date_create
 * @property string $date_update
 * @property integer $status
 * @property integer $creator
 * @property integer $professor
 * @property integer $id_major
 * @property integer $id_university
 *
 * @property Course $idCourse
 */
class Classes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%class}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_course', 'status', 'creator', 'professor', 'id_major', 'id_university'], 'integer'],
            [['name'], 'required'],
            [['description'], 'string'],
            [['date_create', 'date_update'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['id_course'], 'exist', 'skipOnError' => true, 'targetClass' => Course::className(), 'targetAttribute' => ['id_course' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_course' => Yii::t('app', 'Course'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'date_create' => Yii::t('app', 'Date Create'),
            'date_update' => Yii::t('app', 'Date Update'),
            'status' => Yii::t('app', 'Status'),
            'creator' => Yii::t('app', 'Creator'),
            'professor' => Yii::t('app', 'Professor'),
            'id_major' => Yii::t('app', 'Major'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'id_course']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(ClassUser::className(), ['id_class' => 'id']);
    }



    /**
     * @param $id_class
     * @param $id_user
     * @return array|null|\yii\db\ActiveRecord
     */
    public function checkJoin($id_class = 0,$id_user = 0){
        return ClassUser::find()->where(
            [
                'id_class'=>$id_class ? $id_class : $this->id,
                'id_user'=>$id_user ? $id_user : \Yii::$app->user->getId(),
            ]
        )->one();

    }


    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // Place your custom code here
            $this->creator = \Yii::$app->user->getId();
            return true;
        } else {
            return false;
        }
    }
}