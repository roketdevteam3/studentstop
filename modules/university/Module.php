<?php

namespace app\modules\university;

/**
 * modules module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\university\controllers';

    /**
     * @var string
     */
    public $defaultRoute = '/default/index';

    public $modules = ['majors'];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}