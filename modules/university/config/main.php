<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 28.07.15
 * Time: 10:02
 */

$module_name = basename(dirname(dirname(__FILE__)));

return [
    'modules' => [
        $module_name => [
            'class' => 'app\modules\\'.$module_name.'\\Module'
       ],
    ],

    'components' => [
        'urlManager' => [
            'rules' => [
                'university/default/addcomment' => 'university/default/addcomment',
                'university/view/<slug>' => 'university/default/view',
                'university/saveduniversityphoto' => 'university/default/saveduniversityphoto',
                'university/toprated/<slug>' => 'university/default/toprated',
                'university/follow/<id:\d+>' => 'university/default/follow',
                'university/about/<slug>' => 'university/default/about',
                'university/settings/<slug>' => 'university/default/settings',
                'university/sliderpost/<slug>' => 'university/default/sliderpost',
                'university/members/<slug>' => 'university/default/members',
                'university/courses/<slug>' => 'university/default/courses',
                'university/all-news/<slug>' => 'university/default/all-news',
                'university/all-events/<slug>' => 'university/default/all-events',
                'university/event/view/<slug>/<slug_u>' => 'university/event/view',

                'university/default/geteventsajax' => 'university/default/geteventsajax',
                'university/default/savedropedfile' => 'university/default/savedropedfile',
                'university/news/create' => 'university/news/create',
                'university/news/view/<slug>' => 'university/news/view',
                'university/classes/view/<id:\d+>' => 'university/classes/view',
                'university/classes/a/<id:\d+>' => 'university/classes/view',
                'university/classes/view/<id:\d+>' => 'university/classes/view',
                $module_name . '/<action:\w+>/<id:\d+>' => $module_name . '/default/<action>',
                $module_name . '/<action:\w+>' => $module_name . '/default/<action>',
            ],
        ],
    ],
];