<?php

/**
 * Created by PhpStorm.
 * User: kuzio
 * Date: 06.02.16
 * Time: 18:04
 */

namespace app\modules\university\components;

use Yii;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use \yii\web\Controller;
use app\modules\university\models\University;
use yii\web\NotFoundHttpException;

class UniverseController extends Controller
{
    public function init()
    {
        parent::init();
    }

//    public function beforeAction($action)
//    {
//        if (parent::beforeAction($action)) {
//            if($action->id !== 'error'){
//
//                $module  = Yii::$app->controller->module->id;
//                $action_id = Yii::$app->controller->action->id;
//                $controller  = Yii::$app->controller->id;
//                $route  = "$module/$controller/$action_id";
//
//                if (!\Yii::$app->user->can(trim($route))) {
//                    return $this->error403();
//                }
//            }
//            return true;
//        } else {
//            return false;
//        }
//    }


    /**
     * @param $slug
     * @return bool
     * @throws NotFoundHttpException
     */
    public function findUniversity($slug)
    {
        $model = University::find()->status(1)->andWhere(['slug' => $slug])->one();
        if ($model !== null){
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function error403()
    {
        $this->layout = '//errors/403';
        Yii::$app->response->setStatusCode(403, Yii::t('error', 'Access denied'));
        return $this->renderContent(null);
    }
}