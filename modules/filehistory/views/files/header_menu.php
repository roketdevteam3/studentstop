<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use yii\helpers\StringHelper;
    use yii\helpers\Url;
    
    $allUrl = Yii::$app->request->url;
    
    if($this->context->getRoute() == 'filehistory/files/myfiles'){$class = 'active';}else{$class = '';}
    if($this->context->getRoute() == 'filehistory/files/allfiles'){$class2 = 'active';}else{$class2 = '';}
    if($this->context->getRoute() == 'filehistory/files/myproducts'){$class3 = 'active';}else{$class3 = '';}
    if($this->context->getRoute() == 'filehistory/files/allproducts'){$class4 = 'active';}else{$class4 = '';}
    
?>
    <ul class="nav nav-tabs header_myclass" >
        <li role="presentation" class="<?= $class2; ?>">
            <a href="<?= Url::home(); ?>fileshistory/allfiles">Files</a>
        </li>

        <li role="presentation" class="<?= $class4; ?>">
            <a href="<?= Url::home(); ?>fileshistory/allproducts">Marketplace</a>
        </li>

        <li role="presentation" class="<?= $class; ?>">
            <a href="<?= Url::home(); ?>fileshistory/myfiles">Personal Files</a>
        </li>

        <li role="presentation" class="<?= $class3; ?>">
            <a href="<?= Url::home(); ?>fileshistory/myproducts">Selling</a>
        </li>
    </ul>
