<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\StringHelper;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use app\widgets\FileclasstypeWidget;

use app\assets\FilehistoryAsset;
FilehistoryAsset::register($this);

?>
	<div class="row university-content">
		<div class="container-fluid background-block">
                    <div class="content-university" style="background-color: white;padding-top: 0px">
                        <?= $this->render('header_menu') ?>                        
                    </div>
                    <?php
                        if(\Yii::$app->session->hasFlash('product_update')):
                            echo Alert::widget([
                                'options' => [
                                    'class' => 'alert-info',
                                ],
                                'body' => \Yii::t('app','Product update'),
                            ]);
                        endif;
                    ?>
                    <?php
                        if(\Yii::$app->session->hasFlash('product_delete')):
                            echo Alert::widget([
                                'options' => [
                                    'class' => 'alert-warning',
                                ],
                                'body' => \Yii::t('app','Product delete'),
                            ]);
                        endif;
                    ?>
                    <h2 style="text-align:center;"><b>My product</b></h2>
                    <div class="filehistory-default-index">
                        <table class="table" style="background-color: white">
                                <tr>
                                    <?php 
                                        $href_id = '?sort=file_id';
                                        if(isset($_GET['sort']) && ($_GET['sort'] == 'file_id')){ 
                                            if(isset($_GET['type']) && ($_GET['type'] == 'asc')){ 
                                                $href_id .= '&type=desc';
                                            }else{ 
                                                $href_id .= '&type=asc'; 
                                            } 
                                        }else{
                                            $href_id .= '&type=asc';
                                        } 
                                    ?>
                                    <td>
                                        <b><a href="<?= $href_id; ?>">#</a></b>
                                    </td>
                                    
                                    <?php 
                                        $href_user = '?sort=file_user';
                                        if(isset($_GET['sort']) && ($_GET['sort'] == 'file_user')){ 
                                            if(isset($_GET['type']) && ($_GET['type'] == 'asc')){ 
                                                $href_user .= '&type=desc';
                                            }else{ 
                                                $href_user .= '&type=asc'; 
                                            } 
                                        }else{
                                            $href_user .= '&type=asc';
                                        }
                                    ?>
                                    <td><b><a href="<?= $href_user; ?>">Member</a></b></td>
                                    
                                    <?php 
                                        $href_type = '?sort=file_type';
                                        if(isset($_GET['sort']) && ($_GET['sort'] == 'file_type')){ 
                                            if(isset($_GET['type']) && ($_GET['type'] == 'asc')){ 
                                                $href_type .= '&type=desc';
                                            }else{ 
                                                $href_type .= '&type=asc'; 
                                            } 
                                        }else{
                                            $href_type .= '&type=asc';
                                        }
                                    ?>
                                    <td><b><a href="<?= $href_type; ?>">Type</a></b></td>
                                    
                                    <?php
                                        $href_title = '?sort=file_title';
                                        if(isset($_GET['sort']) && ($_GET['sort'] == 'file_title')){ 
                                            if(isset($_GET['type']) && ($_GET['type'] == 'asc')){ 
                                                $href_title .= '&type=desc';
                                            }else{ 
                                                $href_title .= '&type=asc'; 
                                            } 
                                        }else{
                                            $href_title .= '&type=asc';
                                        }
                                    ?>
                                    <td><b><a href="<?= $href_title; ?>">Title</a></b></td>
                                    
                                    <?php 
                                        $href_class = '?sort=file_class';
                                        if(isset($_GET['sort']) && ($_GET['sort'] == 'file_class')){ 
                                            if(isset($_GET['type']) && ($_GET['type'] == 'asc')){ 
                                                $href_class .= '&type=desc';
                                            }else{ 
                                                $href_class .= '&type=asc'; 
                                            } 
                                        }else{
                                            $href_class .= '&type=asc';
                                        }
                                    ?>
                                    <td><b><a href="<?= $href_class; ?>">Class</a></b></td>
                                    
                                    <?php 
                                        $href_date = '?sort=file_date';
                                        if(isset($_GET['sort']) && ($_GET['sort'] == 'file_date')){ 
                                            if(isset($_GET['type']) && ($_GET['type'] == 'asc')){ 
                                                $href_date .= '&type=desc';
                                            }else{ 
                                                $href_date .= '&type=asc'; 
                                            } 
                                        }else{
                                            $href_date .= '&type=asc';
                                        }
                                    ?>
                                    <td><b><a href="<?= $href_date; ?>">Date added</a></b></td>
                                    <td><b>File src</b></td>
                                    
                                    <?php 
                                        $href_price = '?sort=file_price';
                                        if(isset($_GET['sort']) && ($_GET['sort'] == 'file_price')){ 
                                            if(isset($_GET['type']) && ($_GET['type'] == 'asc')){ 
                                                $href_price .= '&type=desc';
                                            }else{ 
                                                $href_price .= '&type=asc'; 
                                            } 
                                        }else{
                                            $href_price .= '&type=asc';
                                        }
                                    ?>
                                    <td><b><a href="<?= $href_price; ?>">Price</a></b></td>
                                    <td></td>
                                </tr>
                            <?php foreach($modelProducts as $product){ ?>
                                <tr>
                                    <td><?= $product['id']; ?></td>
                               
                                    <td><?= $product['name']; ?> 
                                    <?= $product['surname']; ?></td>
                                    <td><?= $product['class_type']; ?></td>
                                    <td><?= $product['title']; ?></td>
                                    <td><?= FileclasstypeWidget::widget(['type' => $product['class_type'], 'class_id' => $product['class_id']]); ?></td>
                                    <td><?= $product['date_create']; ?></td>
                                    <td>
                                            <?php if($product['mime_type'] == 'application'){ ?>
                                                <a href="<?= Url::home().'files/'.$product['file_class_id'].'/'.$product['file_src']; ?>">
                                                    <img style="width:40px;" src="<?= Url::home(); ?>images/files_default_image.png">
                                                </a>
                                            <?php }elseif($product['mime_type'] == 'image'){ ?>
                                                <img class="fileImageShow" style="cursor:pointer;width:40px;" src="<?= Url::home().'files/'.$product['file_class_id'].'/'.$product['file_src']; ?>">
                                            <?php } ?>
                                    </td>
                                    <td><?= $product['price']; ?>$</td>
                                    <td>
                                        <a href="<?= Url::home(); ?>fileshistory/product/edit/<?= $product['id']; ?>">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a href="<?= Url::home(); ?>fileshistory/product/delete/<?= $product['id']; ?>"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                    <div class="col-sm-12">
                        <?= LinkPager::widget(['pagination'=>$pagination]); ?>
                    </div>
                </div>
        </div>

<div id="myModalImageFile" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <img src="" style="max-width: 100%">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </div>
    </div>
</div>
