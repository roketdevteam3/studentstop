<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\widgets\FileclasstypeWidget;

use app\assets\FilehistoryAsset;
FilehistoryAsset::register($this);

?>
 	<div class="row university-content">
		<div class="container-fluid background-block">
                    <div class="content-university" style="background-color: white;padding-top: 0px">
                        <?= $this->render('header_menu') ?>                        
                    </div>
                    <h2 style="text-align:center;"><b>Marketplace</b></h2>
                    <div class="filehistory-default-index">
                        <table class="table" style="background-color: white">
                                <tr>
                                    <?php 
                                        $href_id = '?sort=file_id';
                                        if(isset($_GET['sort']) && ($_GET['sort'] == 'file_id')){ 
                                            if(isset($_GET['type']) && ($_GET['type'] == 'asc')){ 
                                                $href_id .= '&type=desc';
                                            }else{ 
                                                $href_id .= '&type=asc'; 
                                            } 
                                        }else{
                                            $href_id .= '&type=asc';
                                        } 
                                    ?>
                                    <td>
                                        <b><a href="<?= $href_id; ?>">#</a></b>
                                    </td>
                                    
                                    <td>
                                        <b>
                                            <a data-type="product" class="getMembers">Member</a>
                                        </b>
                                        <div style="position:relative;">
                                            <div style="background-color:white;position:absolute;top:0;left:0;width:200px;">
                                                <?php 
                                                    $user_id = '';
                                                    if(isset($_GET['sort']) && ($_GET['sort'] == 'file_user')){
                                                        $user_id = $_GET['user_id'];
                                                    }
                                                ?>
                                                <div style="display:none;" class="userFilesChoisen"><?= $user_id; ?></div>
                                                <ul class="userFilesChoise" style="border:1px solid black;display:none;"></ul>
                                            </div>
                                        </div>
                                    </td>
                                    
                                    <?php 
                                        $href_type = '?sort=file_type';
                                        if(isset($_GET['sort']) && ($_GET['sort'] == 'file_type')){ 
                                            if(isset($_GET['type']) && ($_GET['type'] == 'asc')){ 
                                                $href_type .= '&type=desc';
                                            }else{ 
                                                $href_type .= '&type=asc'; 
                                            } 
                                        }else{
                                            $href_type .= '&type=asc';
                                        }
                                    ?>
                                    <td><b><a href="<?= $href_type; ?>">Type</a></b></td>
                                    
                                    <?php
                                        $href_title = '?sort=file_title';
                                        if(isset($_GET['sort']) && ($_GET['sort'] == 'file_title')){ 
                                            if(isset($_GET['type']) && ($_GET['type'] == 'asc')){ 
                                                $href_title .= '&type=desc';
                                            }else{ 
                                                $href_title .= '&type=asc'; 
                                            } 
                                        }else{
                                            $href_title .= '&type=asc';
                                        }
                                    ?>
                                    <td><b><a href="<?= $href_title; ?>">Title</a></b></td>
                                    
                                    <td>
                                        <b>
                                            <a data-type="product" class="getClass">Class</a>
                                        </b>
                                        <div style="position:relative;">
                                            <?php 
                                                $class_id = '';
                                                if(isset($_GET['sort']) && ($_GET['sort'] == 'file_class')){
                                                    $class_id = $_GET['class_id'];
                                                }
                                            ?>
                                            <div style="display:none;" class="classFilesChoisen"><?= $class_id; ?></div>
                                            <div style="background-color:white;position:absolute;top:0;left:0;width:200px;">
                                                <ul class="classFilesChoise" style="border:1px solid black;display:none;"></ul>
                                            </div>
                                        </div>
                                    </td>
                                    
                                    <?php 
                                        $href_date = '?sort=file_date';
                                        if(isset($_GET['sort']) && ($_GET['sort'] == 'file_date')){ 
                                            if(isset($_GET['type']) && ($_GET['type'] == 'asc')){ 
                                                $href_date .= '&type=desc';
                                            }else{ 
                                                $href_date .= '&type=asc'; 
                                            } 
                                        }else{
                                            $href_date .= '&type=asc';
                                        }
                                    ?>
                                    <td><b><a href="<?= $href_date; ?>">Date added</a></b></td>
                                    <?php /* ?>
                                    <td><b>File src</b></td>
                                    <?php */ ?>
                                    
                                    <?php 
                                        $href_price = '?sort=file_price';
                                        if(isset($_GET['sort']) && ($_GET['sort'] == 'file_price')){ 
                                            if(isset($_GET['type']) && ($_GET['type'] == 'asc')){ 
                                                $href_price .= '&type=desc';
                                            }else{ 
                                                $href_price .= '&type=asc'; 
                                            } 
                                        }else{
                                            $href_price .= '&type=asc';
                                        }
                                    ?>
                                    <td><b><a href="<?= $href_price; ?>">Price</a></b></td>
                                    <td></td>
                                </tr>
                            <?php foreach($modelProducts as $product){ ?>
                                <tr>
                                    <td><?= $product['id']; ?></td>
                               
                                    <td><?= $product['name']; ?> 
                                    <?= $product['surname']; ?></td>
                                    <td><?= $product['class_type']; ?></td>
                                    <td><?= $product['title']; ?></td>
                                    <td><?= FileclasstypeWidget::widget(['type' => $product['class_type'], 'class_id' => $product['class_id']]); ?></td>
                                    <td><?= $product['date_create']; ?></td>
                                    <?php /* ?>
                                    <td>
                                            <?php if($product['mime_type'] == 'application'){ ?>
                                                <a href="<?= Url::home().'files/'.$product['file_class_id'].'/'.$product['file_src']; ?>">
                                                    <img style="width:40px;" src="<?= Url::home(); ?>images/files_default_image.png">
                                                </a>
                                            <?php }elseif($product['mime_type'] == 'image'){ ?>
                                                <img class="fileImageShow" style="cursor:pointer;width:40px;" src="<?= Url::home().'files/'.$product['file_class_id'].'/'.$product['file_src']; ?>">
                                            <?php } ?>
                                    </td>
                                    <?php */ ?>
                                    <td><?= $product['price']; ?>$</td>
                                    <td>
                                        <?php if($product['user_id'] != \Yii::$app->user->id){ ?>
                                            <button class="btn btn-primary">Pay</button>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                    <div class="col-sm-12">
                        <?= LinkPager::widget(['pagination'=>$pagination]); ?>
                    </div>
                </div>
        </div>

<div id="myModalImageFile" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <img src="" style="max-width: 100%">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </div>
    </div>
</div>