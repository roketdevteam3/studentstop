<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\widgets\FileclasstypeWidget;

use app\assets\FilehistoryAsset;
FilehistoryAsset::register($this);

?>
	<div class="row university-content">
		<div class="container-fluid background-block">
                    <div class="content-university" style="background-color: white;padding-top: 0px">
                        <?= $this->render('header_menu') ?>                        
                    <?php //var_dump($_GET); ?>
                    </div>
                    <h2 style="text-align:center;"><b><?= $modelProduct->title; ?></b></h2>
                    <div class="filehistory-default-index">
                        <?php $form = ActiveForm::begin(); ?>
                                <?php //= $form->field($modelProduct, 'class_id')->hiddenInput(['value'=>$modelClass->id])->label(false); ?>
                                <?= $form->field($modelProduct, 'title')->textInput(); ?>
                                <?= $form->field($modelProduct, 'description')->textarea(['rows' => '6']); ?>
                                <?= $form->field($modelProduct, 'price')->textInput(); ?>
                                <?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'change_user_info', 'class' => 'btn btn-action']) ?>
                        <?php ActiveForm::end();  ?>
                    </div>
                </div>
        </div>
