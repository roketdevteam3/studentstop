<?php
/**
 * Created by PhpStorm.
 * User: Listat
 * Date: 06.02.2017
 * Time: 18:23
 */

?>
<div id="right-panel">
  <?= $this->render('right_menu') ?>
</div>
<div class="row university-content">
  <div class="container-fluid background-block">
    <div class="content-university" style="margin-top:60px;">
      <?= $this->render('header_menu') ?>
    </div>
    <div class="filehistory-default-index">
      <?= \kartik\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
          ['class' => 'yii\grid\SerialColumn'],

//          'user_id',
           'othe',
           'price',
          // 'created_at',
           'updated_at:date',
        ],
      ]); ?>
    </div>
  </div>
</div>


