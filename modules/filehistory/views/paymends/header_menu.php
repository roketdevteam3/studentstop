<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use yii\helpers\StringHelper;
    use yii\helpers\Url;
    
    $allUrl = Yii::$app->request->url;

    if($allUrl == '/paymends/credits'){$class = 'active';}else{$class = '';}
    if($allUrl == '/paymends/rent_house'){$class2 = 'active';}else{$class2 = '';}
    if($allUrl == '/paymends/borrow'){$class3 = 'active';}else{$class3 = '';}
    if($allUrl == '/paymends/classes'){$class4 = 'active';}else{$class4 = '';}
    
?>
    <ul class="nav nav-tabs">
        <li role="presentation" class="<?= $class; ?>">
            <a href="/paymends/credits">Credits</a>
        </li>

        <li role="presentation" class="<?= $class2; ?>">
            <a href="<?= Url::home(); ?>paymends/rent_house">Rent a house</a>
        </li>

        <li role="presentation" class="<?= $class3; ?>">
            <a href="<?= Url::home(); ?>paymends/borrow">Borrow</a>
        </li>
        
        <li role="presentation" class="<?= $class4; ?>">
            <a href="<?= Url::home(); ?>paymends/classes">Virtual class</a>
        </li>

    </ul>
