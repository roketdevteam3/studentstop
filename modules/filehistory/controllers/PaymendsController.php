<?php

namespace app\modules\filehistory\controllers;

use app\models\TransactionSearch;
use yii\web\Controller;

class PaymendsController extends Controller
{
  public function actionCredits()
  {
    $searchModel = new TransactionSearch([
      'user_id'=>\Yii::$app->user->id,
      'othe'=>'Paymend credits',
      'complete'=>1
    ]);
    $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

    return $this->render('credits', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);

  }
  public function actionBorrow()
  {
    $searchModel = new TransactionSearch(['user_id'=>\Yii::$app->user->id, 'othe'=>'Paymend rent borrow','complete'=>1]);
    $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

    return $this->render('credits', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);

  }
  public function actionClasses()
  {
    $searchModel = new TransactionSearch(['user_id'=>\Yii::$app->user->id, 'othe'=>'Paymend virtual class','complete'=>1]);
    $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

    return $this->render('credits', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);

  }
  public function actionRent_house()
  {
    $searchModel = new TransactionSearch(['user_id'=>\Yii::$app->user->id,'othe'=>'Paymend rent house','complete'=>1]);
    $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

    return $this->render('credits', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);

  }
}