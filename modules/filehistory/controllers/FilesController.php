<?php

namespace app\modules\filehistory\controllers;

use yii\web\Controller;
use app\models\Files;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use app\models\Follower;
use app\models\Videoconferenceuser;
use app\models\Product;
/**
 * Default controller for the `filehistory` module
 */
class FilesController extends Controller
{
    
    public function beforeAction($action) {
        if(!\Yii::$app->user->isGuest){
            if(\Yii::$app->user->identity->getUsertUniversity()){
                $this->enableCsrfValidation = false;
                return parent::beforeAction($action);
            }else{
                return $this->redirect(Url::to("/users/choiceuniversity",true));
            }
        }else{
            return $this->redirect(Url::to("/users/login",true));
        }
    }
    
    public function actionIndex()
    {
        
    }
    
    public function actionMyfiles()
    {
        $queryFiles = new Query();
        $queryFiles->select(['*', 'id' => '{{%files}}.id', 'date_create' => '{{%files}}.date_create'])
                        ->from('{{%files}}')
                        ->join('LEFT JOIN',
                                    '{{%user}}',
                                    '{{%user}}.id = {{%files}}.user_id')
                        ->join('LEFT JOIN',
                                    '{{%user_info}}',
                                    '{{%user_info}}.id_user = {{%files}}.user_id')
                        ->where(['{{%files}}.user_id' => \Yii::$app->user->id]);
        if(isset($_GET['type'])){
            if($_GET['type'] == 'asc'){
                $sort_type = 'ASC';
            }else{
                $sort_type = 'DESC';
            }
        }else{
            $sort_type = 'ASC';
        }
        
        if(isset($_GET['sort'])){
            switch ($_GET['sort']) {
                case 'file_id':
                    $queryFiles->orderBy('{{%files}}.id '.$sort_type);
                    break;
                case 'file_user':
                    $queryFiles->orderBy('{{%user}}.name '.$sort_type);
                    break;
                case 'file_class':
                    $queryFiles->orderBy('{{%user}}.name '.$sort_type);
                    break;
                case 'file_type':
                    $queryFiles->orderBy('{{%files}}.class_type '.$sort_type);
                    break;
                case 'file_date':
                    $queryFiles->orderBy('{{%files}}.date_create '.$sort_type);
                    break;
            }
        }
        $modelFiles = new ActiveDataProvider(['query' => $queryFiles, 'pagination' => ['pageSize' => 10]]);        
        return $this->render('files',[
            'modelFiles' => $modelFiles->getModels(),
            'pagination' => $modelFiles->pagination,
            'count' => $modelFiles->pagination->totalCount,
        ]);
        
    }
    
    public function actionAllfiles()
    {
        $modelFollower = Follower::find()->where(['id_user' => \Yii::$app->user->id, 'type_object' => 'class' ])->asArray()->all();
        
        $class_id = [];
        foreach($modelFollower as $follower){
            $class_id[] = $follower['id_object'];
        }
        
        $modelVirtualClass = Videoconferenceuser::find()->where(['user_id' => \Yii::$app->user->id])->asArray()->all();
        $videoclass_id = [];
        foreach($modelVirtualClass as $videoclass){
            $videoclass_id[] = $videoclass['conference_id'];
        }
        
        $queryFiles = new Query();
        $queryFiles->select(['*', 'id' => '{{%files}}.id', 'date_create' => '{{%files}}.date_create'])
                        ->from('{{%files}}')
                        ->join('LEFT JOIN',
                                    '{{%user}}',
                                    '{{%user}}.id = {{%files}}.user_id')
                        ->join('LEFT JOIN',
                                    '{{%user_info}}',
                                    '{{%user_info}}.id_user = {{%files}}.user_id')
                        ->where(['{{%files}}.class_id' => $class_id, '{{%files}}.class_type' => 'class'])
                        ->orWhere(['{{%files}}.class_id' => $videoclass_id, '{{%files}}.class_type' => 'virtualclass']);

        if(isset($_GET['type'])){
            if($_GET['type'] == 'asc'){
                $sort_type = 'ASC';
            }else{
                $sort_type = 'DESC';
            }
        }else{
            $sort_type = 'ASC';
        }
        
        if(isset($_GET['sort'])){
            switch ($_GET['sort']) {
                case 'file_id':
                    $queryFiles->orderBy('{{%files}}.id '.$sort_type);
                    break;
                case 'file_user':
                    if(isset($_GET['user_id'])){
                        $user_id = $_GET['user_id'];
                        $queryFiles->andWhere(['{{%files}}.user_id' => $user_id]);
                    }
                    break;
                case 'file_class':
                    if(isset($_GET['class_id'])){
                        $user_id = $_GET['class_id'];
                        $queryFiles->andWhere(['{{%files}}.class_id' => $user_id]);
                    }
                    break;
                case 'file_type':
                    $queryFiles->orderBy('{{%files}}.class_type '.$sort_type);
                    break;
                case 'file_date':
                    $queryFiles->orderBy('{{%files}}.date_create '.$sort_type);
                    break;
            }
        }
        
        $modelFiles = new ActiveDataProvider(['query' => $queryFiles, 'pagination' => ['pageSize' => 10]]);        
        return $this->render('allfiles',[
            'modelFiles' => $modelFiles->getModels(),
            'pagination' => $modelFiles->pagination,
            'count' => $modelFiles->pagination->totalCount,
        ]);
        
    }
    
    public function actionMyproducts()
    {
        $queryProduct = new Query();
        $queryProduct->select(['*', 'id' => '{{%product}}.id', 'file_class_id' => '{{%files}}.class_id',  'class_id' => '{{%product}}.class_id', 'date_create' => '{{%product}}.date_create', 'title' => '{{%product}}.title', 'description' => '{{%product}}.description'])
                        ->from('{{%product}}')
                        ->join('LEFT JOIN',
                                    '{{%files}}',
                                    '{{%files}}.id = {{%product}}.file_id')
                        ->join('LEFT JOIN',
                                    '{{%user}}',
                                    '{{%user}}.id = {{%product}}.user_id')
                        ->join('LEFT JOIN',
                                    '{{%user_info}}',
                                    '{{%user_info}}.id_user = {{%product}}.user_id')
                        ->where(['{{%product}}.user_id' => \Yii::$app->user->id]);
        if(isset($_GET['type'])){
            if($_GET['type'] == 'asc'){
                $sort_type = 'ASC';
            }else{
                $sort_type = 'DESC';
            }
        }else{
            $sort_type = 'ASC';
        }
        
        if(isset($_GET['sort'])){
            switch ($_GET['sort']) {
                case 'file_id':
                    $queryProduct->orderBy('{{%files}}.id '.$sort_type);
                    break;
                case 'file_user':
                    $queryProduct->orderBy('{{%user}}.name '.$sort_type);
                    break;
                case 'file_class':
                    $queryProduct->orderBy('{{%user}}.name '.$sort_type);
                    break;
                case 'file_title':
                    $queryProduct->orderBy('{{%product}}.title '.$sort_type);
                    break;
                case 'file_type':
                    $queryProduct->orderBy('{{%files}}.class_type '.$sort_type);
                    break;
                case 'file_date':
                    $queryProduct->orderBy('{{%product}}.date_create '.$sort_type);
                    break;
                case 'file_price':
                    $queryProduct->orderBy('{{%product}}.price '.$sort_type);
                    break;
            }
        }
        
        
        $modelProducts = new ActiveDataProvider(['query' => $queryProduct, 'pagination' => ['pageSize' => 10]]);
//        var_dump($modelProducts->getModels());exit;
        return $this->render('myproducts',[
            'modelProducts' => $modelProducts->getModels(),
            'pagination' => $modelProducts->pagination,
            'count' => $modelProducts->pagination->totalCount,
        ]);
    }
    
    public function actionAllproducts(){
        $modelFollower = Follower::find()->where(['id_user' => \Yii::$app->user->id, 'type_object' => 'class' ])->asArray()->all();
        
        $class_id = [];
        foreach($modelFollower as $follower){
            $class_id[] = $follower['id_object'];
        }
        
        $modelVirtualClass = Videoconferenceuser::find()->where(['user_id' => \Yii::$app->user->id])->asArray()->all();
        $videoclass_id = [];
        foreach($modelVirtualClass as $videoclass){
            $videoclass_id[] = $videoclass['conference_id'];
        }
        
        $queryProduct = new Query();
        $queryProduct->select(['*', 'id' => '{{%product}}.id', 'user_id' => '{{%product}}.user_id', 'file_class_id' => '{{%files}}.class_id',  'class_id' => '{{%product}}.class_id', 'date_create' => '{{%product}}.date_create', 'title' => '{{%product}}.title', 'description' => '{{%product}}.description'])
                        ->from('{{%product}}')
                        ->join('LEFT JOIN',
                                    '{{%files}}',
                                    '{{%files}}.id = {{%product}}.file_id')
                        ->join('LEFT JOIN',
                                    '{{%user}}',
                                    '{{%user}}.id = {{%product}}.user_id')
                        ->join('LEFT JOIN',
                                    '{{%user_info}}',
                                    '{{%user_info}}.id_user = {{%product}}.user_id')
                        ->where(['{{%product}}.class_id' => $class_id]);
        if(isset($_GET['type'])){
            if($_GET['type'] == 'asc'){
                $sort_type = 'ASC';
            }else{
                $sort_type = 'DESC';
            }
        }else{
            $sort_type = 'ASC';
        }
        
        if(isset($_GET['sort'])){
            switch ($_GET['sort']) {
                case 'file_id':
                    $queryProduct->orderBy('{{%files}}.id '.$sort_type);
                    break;
                
                case 'file_user':
                    if(isset($_GET['user_id'])){
                        $user_id = $_GET['user_id'];
                        $queryProduct->andWhere(['{{%product}}.user_id' => $user_id]);
                    }
                    break;
                case 'file_class':
                    if(isset($_GET['class_id'])){
                        $user_id = $_GET['class_id'];
                        $queryProduct->andWhere(['{{%product}}.class_id' => $user_id]);
                    }
                    break;
                    
                case 'file_title':
                    $queryProduct->orderBy('{{%product}}.title '.$sort_type);
                    break;
                case 'file_type':
                    $queryProduct->orderBy('{{%files}}.class_type '.$sort_type);
                    break;
                case 'file_date':
                    $queryProduct->orderBy('{{%product}}.date_create '.$sort_type);
                    break;
                case 'file_price':
                    $queryProduct->orderBy('{{%product}}.price '.$sort_type);
                    break;
            }
        }
        
        
        
        $modelProducts = new ActiveDataProvider(['query' => $queryProduct, 'pagination' => ['pageSize' => 10]]);        
//        var_dump($modelProducts->getModels());exit;
//        var_dump($modelProducts->getModels());
        return $this->render('allproducts',[
            'modelProducts' => $modelProducts->getModels(),
            'pagination' => $modelProducts->pagination,
            'count' => $modelProducts->pagination->totalCount,
        ]);
        
    }
    
    public function actionProductdelete($id=null){
        $modelProduct = Product::find()->where(['id' => $id])->one();
        if($modelProduct != null){
            if($modelProduct->delete()){
                \Yii::$app->session->setFlash('product_delete');
                return $this->redirect('/fileshistory/myproducts');
            }else{
                return $this->redirect('/fileshistory/myproducts');                
            }
        }else{
            return $this->redirect('/fileshistory/myproducts');                
        }
    }
    
    public function actionProductedit($id=null){
        $modelProduct = Product::find()->where(['id' => $id])->one();
        if($modelProduct != null){
            if($_POST){
                $modelProduct->scenario = 'update_product';
                $request = \Yii::$app->request;
                if($modelProduct->load($request->post())){
                    if($modelProduct->save()){
                        \Yii::$app->session->setFlash('product_update');
                        return $this->redirect('/fileshistory/myproducts');
                    }else{
                        return $this->redirect('/fileshistory/myproducts');
                    }
                }
            }
            return $this->render('productedit',[
                'modelProduct' => $modelProduct
            ]);
        }        
    }
    
    public function actionGetuserforsort(){
        if($_POST['type'] == 'files'){
            $modelFollower = Follower::find()->where(['id_user' => \Yii::$app->user->id, 'type_object' => 'class'])->asArray()->all();

            $class_id = [];
            foreach($modelFollower as $follower){
                $class_id[] = $follower['id_object'];
            }

            $modelVirtualClass = Videoconferenceuser::find()->where(['user_id' => \Yii::$app->user->id])->asArray()->all();
            $videoclass_id = [];
            foreach($modelVirtualClass as $videoclass){
                $videoclass_id[] = $videoclass['conference_id'];
            }

            $modelFiles = Files::find()->where(['class_id' => $videoclass_id, 'class_type' => 'virtualclass'])->
                    orWhere(['class_id' => $class_id, 'class_type' => 'class'])->asArray()->all();

            $user_id = [];
            foreach($modelFiles as $files){
                $user_id[] = $files['user_id'];
            }
        }else{
            $modelFollower = Follower::find()->where(['id_user' => \Yii::$app->user->id, 'type_object' => 'class'])->asArray()->all();

            $class_id = [];
            foreach($modelFollower as $follower){
                $class_id[] = $follower['id_object'];
            }

            $modelProduct = Product::find()->where(['class_id' => $class_id])->asArray()->all();

            $user_id = [];
            foreach($modelProduct as $product){
                $user_id[] = $product['user_id'];
            }            
        }
        
        $modelUser = \app\modules\users\models\User::find()->where(['id' => $user_id])->asArray()->all();
        
        echo json_encode($modelUser);        
    }
    
    public function actionGetclassforsort(){
        if($_POST['type'] == 'files'){
            $modelFollower = Follower::find()->where(['id_user' => \Yii::$app->user->id, 'type_object' => 'class'])->asArray()->all();

            $class_id = [];
            foreach($modelFollower as $follower){
                $class_id[] = $follower['id_object'];
            }

            $modelVirtualClass = Videoconferenceuser::find()->where(['user_id' => \Yii::$app->user->id])->asArray()->all();
            $videoclass_id = [];
            foreach($modelVirtualClass as $videoclass){
                $videoclass_id[] = $videoclass['conference_id'];
            }

            $modelFiles = Files::find()->where(['class_id' => $videoclass_id, 'class_type' => 'virtualclass'])->
                    orWhere(['class_id' => $class_id, 'class_type' => 'class'])->asArray()->all();

            $class_id = [];
            foreach($modelFiles as $files){
                $class_id[] = $files['class_id'];
            }
        }else{
            $modelFollower = Follower::find()->where(['id_user' => \Yii::$app->user->id, 'type_object' => 'class'])->asArray()->all();

            $class_id = [];
            foreach($modelFollower as $follower){
                $class_id[] = $follower['id_object'];
            }
            $modelProduct = Product::find()->where(['class_id' => $class_id])->asArray()->all();

            $class_id = [];
            foreach($modelProduct as $product){
                $class_id[] = $product['class_id'];
            }
            
        }
        $modelClass = \app\models\Classes::find()->where(['id' => $class_id])->asArray()->all();
        
        echo json_encode($modelClass);        
    }
    
}
