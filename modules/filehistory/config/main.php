<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 28.07.15
 * Time: 10:02
 */

$module_name = basename(dirname(dirname(__FILE__)));

return [
    'modules' => [
        'filehistory' => [
            'class' => 'app\modules\filehistory\Module',
        ],
    ],

    'components' => [
        'urlManager' => [
            'rules' => [
                'fileshistory/product/delete/<id:\d+>' => 'filehistory/files/productdelete',
                'fileshistory/product/edit/<id:\d+>' => 'filehistory/files/productedit',
                'fileshistory/allproducts' => 'filehistory/files/allproducts',
                'fileshistory/myproducts' => 'filehistory/files/myproducts',
                'fileshistory/myfiles' => 'filehistory/files/myfiles',
                'fileshistory/allfiles' => 'filehistory/files/allfiles',
                'paymends/credits' => 'filehistory/paymends/credits',
                'paymends/rent_house' => 'filehistory/paymends/rent_house',
                'paymends/borrow' => 'filehistory/paymends/borrow',
                'paymends/classes' => 'filehistory/paymends/classes',
                $module_name . '/<action:\w+>/<id:\d+>' => $module_name . '/default/<action>',
                $module_name . '/<action:\w+>' => $module_name . '/default/<action>',
            ],
        ],
    ],
];