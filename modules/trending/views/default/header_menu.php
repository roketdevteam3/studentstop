<?php

    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use yii\helpers\StringHelper;
    use yii\helpers\Url;
    use yii\widgets\LinkPager;
    
    $allUrl = Yii::$app->request->url;
    if($this->context->getRoute() == 'trending/rating/university'){$class = 'active';}else{$class = '';}
    if($this->context->getRoute() == 'trending/rating/classes'){$class2 = 'active';}else{$class2 = '';}
    if($this->context->getRoute() == 'trending/rating/virtualclass'){$class3 = 'active';}else{$class3 = '';}
    if($this->context->getRoute() == 'trending/rating/users'){$class4 = 'active';}else{$class4 = '';}
    if($this->context->getRoute() == 'trending/rating/questionbank'){$class5 = 'active';}else{$class5 = '';}

?>
<ul class="nav nav-tabs navigation-tabs" style='margin-top:50px;'>
    <li class="<?= $class; ?>">
        <?= HTML::a('Universities', Url::home().'trending/rating/university'); ?>
    </li>
    <li class="<?= $class2; ?>">
        <?= HTML::a('Classes', Url::home().'trending/rating/classes'); ?>
    </li>
    <li class="<?= $class4; ?>">
        <?= HTML::a('Users', Url::home().'trending/rating/users'); ?>
    </li>
    <li class="<?= $class3; ?>">
        <?= HTML::a('Virtual classes', Url::home().'trending/rating/virtualclass'); ?>
    </li>
    <li class="<?= $class5; ?>">
        <?= HTML::a('Question Bank', Url::home().'trending/rating/questionbank'); ?>
    </li>
</ul>