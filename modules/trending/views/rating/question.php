<?php 
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;
	use yii\helpers\Url;
	use app\widgets\trending\QuestionratingWidget;
?>
<style>
    .user-layer{
        right:0px !important;
    }
    .modalNotificationRightBlock{
        right:0px !important;        
    }
</style>
<div class="row background-block">
	<div class="rating-page">
		<div class="rating-wrap">
			<?= $this->render('/default/header_menu'); ?>
			<div class="trending-default-index">
				<table class="table table-striped table-history">
					<thead>
						<tr>
							<td>
								№
							</td>
							<td>
								Title
							</td>
							<td>
                                                                Member
							</td>
							<td>
								University
							</td>
							<td>
								Rating
							</td>
						</tr>
					</thead>
					<?php $k = 1;?>
					<?php foreach($questionRatingArray as $question_id => $question_rating){ ?>
						<tr>
							<td>
								<?= $k++;?>
							</td>
							<?= QuestionratingWidget::widget(['question_id' => $question_id, 'rating_count' => $question_rating]); ?>
						</tr>
					<?php } ?>
				</table>
			</div>
		</div>
	</div>
	
</div>
