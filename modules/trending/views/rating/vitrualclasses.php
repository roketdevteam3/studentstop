<?php 
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;
	use yii\helpers\Url;
	use app\widgets\trending\VirtualclassinfoWidget;
?>
<style>
    .user-layer{
        right:0px !important;
    }
    .modalNotificationRightBlock{
        right:0px !important;        
    }
</style>
<div class="row background-block">
	<div class="rating-page">
		<div class="rating-wrap">
			<?= $this->render('/default/header_menu'); ?>
			<div class="trending-default-index">
				<table class="table table-striped table-history">
					<thead>
						<tr>
							<td>
								№
							</td>
							<td>
								Name
							</td>
							<td>
                                                                Professor
							</td>
							<td>
								University
							</td>
							<td>
								User count
							</td>
							<td>
								Rating
							</td>
						</tr>
					</thead>
					<?php $k = 1;?>
					<?php foreach($classRatingArray as $class_id => $class_rating){ ?>
						<tr>
							<td>
								<?= $k++;?>
							</td>
							<?= VirtualclassinfoWidget::widget(['class_id' => $class_id, 'rating_count' => $class_rating]); ?>
						</tr>
					<?php } ?>
				</table>
			</div>
		</div>
	</div>
	
</div>
