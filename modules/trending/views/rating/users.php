<?php 
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;
	use yii\helpers\Url;
	use app\widgets\trending\UserinfoWidget;
?>
<style>
    .user-layer{
        right:0px !important;
    }
    .modalNotificationRightBlock{
        right:0px !important;        
    }
</style>
<div class="row background-block">
	<div class="rating-page">
		<div class="rating-wrap">
			<?= $this->render('/default/header_menu'); ?>
			<div class="trending-default-index">
				<table class="table table-striped table-history">
					<thead>
						<tr>
							<td>
								#
							</td>
							<td>
								Name
							</td>
							<td>
                                                                Major
							</td>
							<td>
								University
							</td>
							<td>
								<b>Class count</b>
							</td>
							<td>
								<b>Virtual class count</b>
							</td>
							<td>
								Rating
							</td>
						</tr>
					</thead>
					<?php $k = 1;?>
					<?php foreach($arrayRatingResult as $user_id => $user_rating){ ?>
						<tr>
							<td>
								<?= $k++;?>
							</td>
							<?= UserinfoWidget::widget(['user_id' => $user_id, 'rating_count' => $user_rating]); ?>
						</tr>
					<?php } ?>
				</table>
			</div>
		</div>
	</div>
</div>
