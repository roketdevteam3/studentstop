<?php

namespace app\modules\trending\controllers;

use yii\web\Controller;

/**
 * Default controller for the `trending` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
