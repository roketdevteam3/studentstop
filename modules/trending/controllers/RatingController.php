<?php

namespace app\modules\trending\controllers;

use yii\web\Controller;
use app\models\Classes;
use app\models\user\User;
use app\models\University;
use app\models\Classreting;
use app\models\Ratingrequest;
use app\models\Virtualclassreting;
use app\models\Videoconference;
use app\modules\question\models\Question;
/**
 * Default controller for the `trending` module
 */
class RatingController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionUniversity()
    {
        $modelUniversities = University::find()->asArray()->all();
            $universityRatingArray = [];
            $universityCount = 0;
            foreach($modelUniversities as $university){
                    $universityCount++;
                    $modelClasses = Classes::find()->where(['id_university' => $university['id']])->asArray()->all();
                    $count_entry = 0;
                    $count_star = 0;
                    foreach($modelClasses as $class){
                        $modelRatingClass = Classreting::find()->where(['class_id' => $class['id']])->all();
                        $countUser = 0;
                        $countStar = 0;
                        foreach($modelRatingClass as $request){
                            $countUser++;
                            $countStar = $countStar + $request->rate_professor;
                        }
                        $result = '';
                        if($countUser == 0){
                            $countStarClassResult = 0;
                        }else{
                            $count_entry++;
                            $countStarClassResult = round($countStar/$countUser,1); 
                            $count_star = $count_star + $countStarClassResult;
                        }
                    }
                    if($count_entry == 0){
                        $countStarResult = 0;
                    }else{
                        $countStarResult = round($count_star/$count_entry,1); 
                    }
                    $universityRatingArray[$university['id']] = $countStarResult;
            }
            arsort($universityRatingArray);
            //var_dump($universityRatingArray);exit;
            
            $k = 0;
            $result_rat = '';
//            foreach($universityRatingArray as $un_id => $univerRat){
//                $k++;
//                if($un_id == $university_id){
//                    $result_rat = $k;
//                }
//            }
            //echo json_encode(['k' => $result_rat, 'c' => $universityCount]);
            
        return $this->render('university',[
            'universityRatingArray' => $universityRatingArray,
            'universityCount' => $universityCount
        ]);
    }
    
    public function actionClasses()
    {
            $modelClasses = Classes::find()->asArray()->all();
            $count_entry = 0;
            $count_star = 0;
            $classRatingArray = [];
            $class_count = 0;
            foreach($modelClasses as $class){
                $class_count++;
                $modelRatingClass = Classreting::find()->where(['class_id' => $class['id']])->all();
                $countUser = 0;
                $countStar = 0;
                foreach($modelRatingClass as $request){
                    $countUser++;
                    $countStar = $countStar + $request->rate_professor;
                }
                $result = '';
                if($countUser == 0){
                    $countStarClassResult = 0;
                }else{
                    $count_entry++;
                    $countStarClassResult = round($countStar/$countUser,1); 
                }
                $classRatingArray[$class['id']] = $countStarClassResult;
            }
            arsort($classRatingArray);
        
        return $this->render('classes',[
            'classRatingArray' => $classRatingArray,
            'classCount'  => $class_count,
        ]);
    }
    
    public function actionUsers()
    {
        $modelUsers = User::find()->asArray()->all();
        $arrayRatingResult = [];
        $userCount = 0;
        foreach($modelUsers as $user){
            $userCount++;
            $modelRatingrequest = Ratingrequest::find()->where(['to_object_id' => $user['id']])->asArray()->all();
            $countUser = 0;
            $countStar = 0;
                foreach($modelRatingrequest as $rating){
                    $countUser++;
                    $countStar = $countStar + $rating['rating_count'];
                }
                $result = '';
                if($countUser == 0){
                    $countStarResult = 0;
                }else{
                    $countStarResult = round($countStar/$countUser,1); 
                }
            $arrayRatingResult[$user['id']] = $countStarResult;
        }
        
        arsort($arrayRatingResult);
        
        return $this->render('users',[
            'arrayRatingResult' => $arrayRatingResult,
            'userCount' => $userCount,
        ]);
    }
    
    public function actionVirtualclass()
    {
        $modelClasses = Videoconference::find()->asArray()->all();
        $count_entry = 0;
        $count_star = 0;
        $classRatingArray = [];
        $class_count = 0;
        foreach($modelClasses as $class){
            $class_count++;
            $modelRatingClass = Virtualclassreting::find()->where(['class_id' => $class['id']])->all();
            $countUser = 0;
            $countStar = 0;
            foreach($modelRatingClass as $request){
                $countUser++;
                $countStar = $countStar + $request->rate_professor;
            }
            $result = '';
            if($countUser == 0){
                $countStarResult = 0;
            }else{
                $countStarResult = round($countStar/$countUser,1); 
            }
            
            $classRatingArray[$class['id']] = $countStarResult;
        }
        arsort($classRatingArray);
        //var_dump($classRatingArray);exit;
        
        return $this->render('vitrualclasses',[
            'classRatingArray' => $classRatingArray,
            'classCount'  => $class_count,
        ]);
        
    }
    
    public function actionQuestionbank()
    {
        $modelQuestion = Question::find()->asArray()->all();
        $count_entry = 0;
        $count_star = 0;
        $questionRatingArray = [];
        $class_count = 0;
        foreach($modelQuestion as $question){
            $class_count++;
            $modelRatingrequest = Ratingrequest::find()->where(['to_object_id' => $question['user_id'], 'type' => 'question_bank'])->all();
            $countUser = 0;
            $countStar = 0;
            foreach($modelRatingrequest as $ratingrequest){
                $countUser++;
                $countStar = $countStar + $ratingrequest->rating_count;
            }
            if($countUser != 0){
                $countStarResult = $countStar/$countUser;
            }else{
                $countStarResult = 0;
            }
            
            $questionRatingArray[$question['id']] = $countStarResult;
        }
        arsort($questionRatingArray);
        //var_dump($classRatingArray);exit;
        
        return $this->render('question',[
            'questionRatingArray' => $questionRatingArray,
            'classCount'  => $class_count,
        ]);
        
    }
    
}
