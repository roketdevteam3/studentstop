<?php

namespace app\modules\users\models;

use Yii;

/**
 * This is the model class for table "{{%user_role}}".
 *
 * @property integer $id
 * @property integer $name
 */
class UserEvents extends \yii\db\ActiveRecord
{
    public $event_model;
    public $usernameInfo;
    
    public static function tableName()
    {
        return '{{%user_events}}';
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
//    public function scenarios()
//    {
//        return [
//            'add_image' => ['user_id','image_name'],
//        ];
//    }
    
    public function rules()
    {
        return [
        ];
    }

}
