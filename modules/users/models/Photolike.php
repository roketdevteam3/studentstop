<?php

namespace app\modules\users\models;

use Yii;

/**
 * This is the model class for table "{{%user_role}}".
 *
 * @property integer $id
 * @property integer $name
 */
class Photolike extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%photos_like}}';
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    public function scenarios()
    {
        return [
            'add_like' => ['user_like_id','photo_id'],
        ];
    }
    
    public function rules()
    {
        return [
 //           ['image_name', 'unique'],
        ];
    }

}
