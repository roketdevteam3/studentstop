<?php

namespace app\modules\users\models;

use Yii;

/**
 * This is the model class for table "{{%user_role}}".
 *
 * @property integer $id
 * @property integer $name
 */
class Photocomment extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%photos_comment}}';
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    public function scenarios()
    {
        return [
            'add_comment' => ['user_comment_id', 'photo_id',' comment_content'],
        ];
    }
    
    public function rules()
    {
        return [
 //           ['image_name', 'unique'],
        ];
    }

}
