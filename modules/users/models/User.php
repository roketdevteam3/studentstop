<?php

namespace app\modules\users\models;


use app\modules\jobs\models\Company;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use app\modules\users\models\UserInfo;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    const ROLE_ADMIN = 20;
    const ROLE_USER = 10;
    public $old_password;
    public $new_password;
    public $confirm_password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
//    public function behaviors()
//    {
////        return [
////            TimestampBehavior::className(),
////        ];
//    }

    
    public function scenarios()
    {
        return [
            'change_password' => ['old_password', 'new_password', 'confirm_password','password'],
            'default' => ['*'],
            'update' => ['name', 'surname', 'email'],
	];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            ['group', 'default', 'value' => 'user'],
            
            ['old_password', 'checkedOldPassword'],
            ['old_password', 'required'],
            ['old_password', 'string', 'min' => 6],
            
            ['new_password', 'required'],
            ['new_password', 'string', 'min' => 6],
            
            
            ['confirm_password', 'required'],
            ['confirm_password', 'compare', 'compareAttribute'=>'new_password', 'message'=>"Passwords don't match" ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function checkedOldPassword()
    {
            if (\Yii::$app->security->generatePasswordHash($this->old_password) !== $this->password) {
                $this->addError('old_password',  'Incorrectly entered old password!');
            }
    }
    
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $email
     * @return static|null
     */
    public static function findByUsername($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }



    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Generates new password reset token
     */
    public function generateToken()
    {
        $this->token = Yii::$app->security->generateRandomString() . '_' . time();
    }


    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /* check admin role */

    public function getIsAdmin()
    {
        return $this->status == self::ROLE_ADMIN;
    }

    /* check user role */

    public function getIsUser()
    {
        return $this->status == self::ROLE_USER;
    }

    static function getUsername($user_id){
        $userModel = static::find()->where(['id' => $user_id])->one();
        $userInfo = UserInfo::find()->where(['id_user' => $user_id])->one();
        
        $user['name'] = $userModel->name;
        $user['avatar'] = $userInfo->avatar;
        $user['surname'] = $userModel->surname;
        return $user;
    }

    public function getUserinfo()
    {
        return $this->hasOne(UserInfo::className(), ['id_user' => 'id']);
    }

  public function getCompany()
  {
    return $this->hasOne(Company::className(), ['user_id' => 'id']);
  }
}
