<?php

namespace app\modules\users\models;

use Yii;

/**
 * This is the model class for table "{{%user_role}}".
 *
 * @property integer $id
 * @property integer $name
 */
class UserNews extends \yii\db\ActiveRecord
{
    public $event_model;
    
    public static function tableName()
    {
        return '{{%user_news}}';
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    public function scenarios()
    {
        return [
            'add_news' => ['title','content','user_id'],
        ];
    }
    
    public function rules()
    {
        return [
            ['title', 'required'],
            ['content', 'required'],
        ];
    }
}