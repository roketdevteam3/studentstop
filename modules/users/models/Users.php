<?php

namespace app\modules\users\models;


use app\models\user\UserFriendRequest;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $auth_key
 * @property string $token
 * @property integer $status
 * @property string $email
 * @property string $created_at
 * @property string $updated_at
 */
class Users extends \yii\db\ActiveRecord
{

    public $confirm_password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_create',
                'updatedAtAttribute' => 'date_update',
                'value' => new Expression('NOW()'),
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        if($this->isNewRecord)
        return [
            [['surname', 'name', 'email','password'], 'required'],
            ['email','email'],
            ['auth_key', 'default', 'value' => Yii::$app->security->generateRandomString()],
            ['token', 'default', 'value' => Yii::$app->security->generateRandomString() . '_' . time()],
            ['group', 'default', 'value' => 'user'],
            [['confirm_password'], 'confirmPassword'],
            [['date_create','date_update'],'safe'],
            ['email', 'unique', 'targetClass' => '\app\modules\users\models\User', 'message' => Yii::t('site','This username has already been taken.')],
        ];
        else
            return [
                [['surname', 'name', 'email'], 'required'],
                ['password','string'],
                ['email','email'],
                [['date_create','date_update'],'safe'],
                ['auth_key', 'default', 'value' => Yii::$app->security->generateRandomString()],
                ['token', 'default', 'value' => Yii::$app->security->generateRandomString() . '_' . time()],
                [['confirm_password'], 'confirmPassword'],
                ['email', 'unique', 'targetClass' => '\app\modules\users\models\User', 'message' => Yii::t('site','This username has already been taken.')],
            ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('site', 'ID'),
            'name' => Yii::t('site', 'Name'),
            'password' => Yii::t('site', 'Password'),
            'auth_key' => Yii::t('site', 'Auth Key'),
            'token' => Yii::t('site', 'Token'),
            'status' => Yii::t('site', 'Status'),
            'email' => Yii::t('site', 'Email'),
            'date_create' => Yii::t('site', 'Created At'),
            'date_update' => Yii::t('site', 'Updated At'),
            'confirm_password' => Yii::t('site', 'Confirm password'),
            'surname' => Yii::t('site', 'Surname'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->auth_key = Yii::$app->security->generateRandomString();
            $this->token = Yii::$app->security->generateRandomString() . '_' . time();

            if(isset($this->password) && !empty($this->password)){
                $this->password = Yii::$app->security->generatePasswordHash($this->password);
            }else{
                $this->password = self::findOne($this->id)->password;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function confirmPassword($attribute, $params)
    {
        if (!$this->hasErrors()) {

            if ($this->password !== $this->confirm_password) {
                $this->addError($attribute,  Yii::t('site','Passwords do not match!'));
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManagers()
    {
        return $this->hasMany(Manager::className(), ['id_user' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserInfos()
    {
        return $this->hasOne(UserInfo::className(), ['id_user' => 'id']);
    }

    public function checkFriendRequest($whom_send = 0, $who_send = 0){
        return UserFriendRequest::find()->where(
            [
                'whom_send'=>$whom_send ? $whom_send : $this->id,
                'who_send'=>$who_send ? $who_send : \Yii::$app->user->getId(),
            ]
        )
            ->one();
    }
}
