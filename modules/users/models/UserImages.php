<?php

namespace app\modules\users\models;

use Yii;

/**
 * This is the model class for table "{{%user_role}}".
 *
 * @property integer $id
 * @property integer $name
 * type: 0-image, 1-video
 */
class UserImages extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%user_images}}';
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    public function scenarios()
    {
        return [
            'add_image' => ['user_id','image_name'],
            'add_file' => ['user_id','image_name', 'type'],
        ];
    }
    
    public function rules()
    {
        return [
            ['image_name', 'unique'],
        ];
    }

}
