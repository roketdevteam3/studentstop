<?php
namespace app\modules\users\models;


use app\modules\users\models\User;
use yii\base\Model;
use Yii;
use yii\db\ActiveRecord;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $name;
    public $surname;
    public $email;
    public $password;
    public $verifyCode;
    public $password_confirm;




    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['name', 'surname'], 'filter', 'filter' => 'trim'],
            [['name', 'surname', 'email'], 'required'],
            ['email', 'unique', 'targetClass' => '\app\modules\users\models\User', 'message' => 'This username has already been taken.'],
            [['name', 'surname'], 'string', 'min' => 2, 'max' => 255],
            ['email', 'email'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            //['password_confirm', 'required'],

            ['password_confirm', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match"],
         //   ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }


    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->name = $this->name;
            $user->surname = $this->surname;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
}
