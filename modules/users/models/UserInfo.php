<?php

namespace app\modules\users\models;

use app\modules\jobs\models\Jobsfunction;
use app\modules\jobs\models\Jobsindustry;
use app\modules\university\models\University;
use Yii;

/**
 * This is the model class for table "{{%user_info}}".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $network
 * @property string $academic_status
 * @property string $class
 * @property string $birthday
 *
 * @property User $idUser
 */
class UserInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_info}}';
    }

    /**
     * @inheritdoc
     */
    
    public function scenarios()
    {
        return [
            'update_user_info' => ['birthday', 'mobile', 'address', 'website', 'facebook','twitter', 'gender' ],
            'user_avatar_wrapper' => ['avatar_wrapper' ],
            'user_avatar' => ['avatar'],
            'user_position' => ['position_lat','position_lng'],
            'resume' => ['birthday','mobile','gender','industry_id',
                'academic_status', 'address','website','facebook',
                'twitter', 'linkedin', 'university_id', 'major_id',
                'date_end_education','date_begin_education', 'jobs_function_id', 'about'],
            'register' => ['network','academic_status','university_id', 'id_user'],
            'update_major' => ['major_id'],
            'default' => '*',
        ];
    }
    
    public function rules()
    {
        return [
            [['id_user', 'network'], 'integer'],
            [['academic_status', 'class', 'birthday'], 'string', 'max' => 255],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user' => Yii::t('app', 'Id User'),
            'network' => Yii::t('app', 'Network'),
            'academic_status' => Yii::t('app', 'Academic Status'),
            'class' => Yii::t('app', 'Class'),
            'birthday' => Yii::t('app', 'Birthday'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    public function getJobs_function()
    {
        return $this->hasOne(Jobsfunction::className(), ['id' => 'jobs_function_id']);
    }

  public function getJobfunction()
  {
    return $this->hasOne(Jobsfunction::className(), ['id' => 'jobs_function_id']);
  }

    public function getUniversity()
    {
        return $this->hasOne(University::className(), ['id' => 'university_id']);
    }
 public function getIndustry()
    {
        return $this->hasOne(Jobsindustry::className(), ['id' => 'industry_id']);
    }


}
