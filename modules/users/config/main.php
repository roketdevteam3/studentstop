<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 28.07.15
 * Time: 10:02
 */

$module_name = basename(dirname(dirname(__FILE__)));

return [
    'modules' => [
        $module_name => [
            'class' => 'app\modules\\'.$module_name.'\\Module'
        ],
    ],

    'components' => [
        'urlManager' => [
            'rules' => [
                'users/choiceuniversity' => 'users/default/choiceuniversity',
                'addlikephoto' => 'users/profile/addlikephoto',
                'addcomentphoto' => 'users/profile/addcomentphoto',
                'showphotoinformation' => 'users/profile/showphotoinformation',
                'getcreditscount' => 'users/profile/getcreditscount',
                'paycredits' => 'users/profile/paycredits',
                'users/homepage' => 'users/default/homepage',
                'saveavatar' => 'users/profile/saveavatar',
                'saveavatarwrapper' => 'users/profile/saveavatarwrapper',
                'savedropedfile' => 'users/profile/savedropedfile',
                'deletefile' => 'users/profile/deletefile',

                'profile/pay' => 'users/profile/pay',
                'profile/timeline' => 'users/profile/timeline',
                'profile/<user_id:\w+>/timeline' => 'users/profile/timeline',
                
                'profile/docs' => 'users/profile/docs',
                'profile/<user_id:\w+>/docs' => 'users/profile/docs',
                
                'profile/market' => 'users/profile/market',
                'profile/<user_id:\w+>/market' => 'users/profile/market',
                
                'profile/courses' => 'users/profile/courses',
                'profile/<user_id:\w+>/courses' => 'users/profile/courses',
                
                'profile/rating' => 'users/profile/rating',
                'profile/<user_id:\w+>/rating' => 'users/profile/rating',

                'profile/about' => 'users/profile/about',
                'profile/<user_id:\w+>/about' => 'users/profile/about',

                'profile/photos' => 'users/profile/photos',
                'profile/<user_id:\w+>/photos' => 'users/profile/photos',


                'mylocationsave' => 'users/profile/mylocation',
                'profile/credits' => 'users/profile/credits',
                'profile/<user_id:\w+>/credits' => 'users/profile/credits',

                'profile/maney' => 'users/profile/maney',
                'profile/<user_id:\w+>/maney' => 'users/profile/maney',

                'profile/streaming' => 'users/profile/streaming',
                'profile/<user_id:\w+>/streaming' => 'users/profile/streaming',

                'profile/<user_id:\w+>' => 'users/profile/index',

                'getfriends' => 'users/profile/friendsajax',
                'users/default/getcourse' => 'users/default/getcourse',

                'friendrequest' => 'users/profile/friendrequest',
                'users/profile/getusersmember' => 'users/profile/getusersmember',
                'profile/friends' => 'users/profile/friends',
                'profile/<user_id:\w+>/friends' => 'users/profile/friends',
                'profile/<user_id:\w+>/friends/<friends_type:\w+>' => 'users/profile/friends',

                'ajax-set-friend/<id:\d+>' => 'users/default/ajax-set-friend',
                'ajax-accept-friend/<id:\d+>' => 'users/default/ajax-accept-friend',
                $module_name . '/<action:\w+>/<id:\d+>' => $module_name . '/default/<action>',
                $module_name . '/<action:\w+>' => $module_name . '/default/<action>',
            ],
        ],
    ],
];
