<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 04.03.16
 * Time: 17:04
 * Email: mackraiscms@gmail.com
 * Site: http://mackrais.tk
 */
use yii\helpers\Url;
use yii\widgets\Pjax;



$tab_class_friends = '';

if($dataProviderRequest->count == 0){
    $tab_class_friends = 'active';
}

?>


<ul class="nav nav-tabs" id="myTab">
    <?php if($dataProviderRequest->count > 0): ?>
        <li class="active"><a data-target="#request" data-toggle="tab">
                Requests
                <span class="badge  "><?= $dataProviderRequest->count ?></span>
            </a>
        </li>
    <?php endif; ?>

    <li class="<?= $tab_class_friends ?>">
        <a data-target="#friends" data-toggle="tab">
            Friends
            <span class="badge  "><?= $dataProviderFriends->count ?></span>
        </a>
    </li>
</ul>

<div class="tab-content">
    <?php if($dataProviderRequest->count > 0): ?>
    <div class="tab-pane active" id="request">

        <?php Pjax::begin(['id' => 'mr_users']) ?>
        <?php foreach ($dataProviderRequest->models as $k => $item) : ?>


            <div class="col-lg-2 col-md-6  col-xs-12 text-center mr-university-user-block">
                <nav  class="btn-group">

                        <a  class="btn btn-success"
                            data-mr-event="accept_friend"
                            data-mr-url="<?= Url::to('ajax-accept-friend/'.$item->id, true) ?>"
                             >
                        <i class="fa fa-check"></i>
                       </a>

                    <button type="button" class="btn btn-danger"><i class="fa fa-times"></i></button>
                    <button type="button" class="btn btn-primary"><i class="fa fa-envelope"></i></button>
                </nav>
                <a href="">
                    <div class="mr-university-user-border">
                        <i class="fa fa-user"></i>
                    </div>
                    <h3><?= $item->name ?>, <?= $item->surname ?></h3>
                </a>
            </div>
        <?php endforeach; ?>

        <div class="col-md-12">
            <?= yii\widgets\LinkPager::widget([
                'pagination' => $dataProviderRequest->pagination
            ]) ?>
        </div>

        <?php Pjax::end() ?>
    </div>

    <?php endif; ?>
    <div class="tab-pane <?= $tab_class_friends ?>" id="friends">

        <?php Pjax::begin(['id' => 'mr_users']) ?>
        <?php foreach ($dataProviderFriends->models as $k => $item) : ?>


            <div class="col-lg-2 col-md-6  col-xs-12 text-center mr-university-user-block">
                <nav  class="btn-group">
                    <button type="button" class="btn btn-primary"><i class="fa fa-eye"></i></button>
                    <button type="button" class="btn btn-primary"><i class="fa fa-envelope"></i></button>
                    <button type="button" class="btn btn-danger"><i class="fa fa-user-times"></i></button>
                </nav>
                <a href="">
                    <div class="mr-university-user-border">
                        <i class="fa fa-user"></i>
                    </div>
                    <h3><?= $item->name ?>, <?= $item->surname ?></h3>
                </a>
            </div>
        <?php endforeach; ?>

        <div class="col-md-12">
            <?= yii\widgets\LinkPager::widget([
                'pagination' => $dataProviderRequest->pagination
            ]) ?>
        </div>

    </div>

</div>
</div>