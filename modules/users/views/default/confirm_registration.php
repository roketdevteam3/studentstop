<?php
/**
 * Created by PhpStorm.
 * User: kuzio
 * Date: 17.12.15
 * Time: 18:25
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

?>


<div class="home_background">
	<div class="content_home">
		<h1 class="title wow bounceInDown" data-wow-offset="200" data-wow-delay="0.5s" data-wow-duration="2s">the studentstop.</h1>
	</div>
	<div class="wrapper">
		<div class="sub-wrapper">
			<div id="heder-title">
				<h3 style="text-align:center;" id="header-text">LOG IN AND SIGN UP</h3>
			</div>
			<div class="row">
                <?php $form = ActiveForm::begin(['fieldConfig' => [
                    'template' => "{input}{error}",
                    'options' => [
                        'tag' => 'span'
                    ]
                ],
                    'action' => '/users/signup'
                ]); ?>
				<div class="col-md-6 wow bounceInLeft" data-wow-offset="200" data-wow-delay="0.5s" data-wow-duration="2s">
					<div class="col-md-5 left-m" style="border-top: 2px solid #31B3FF;">
						<h4 class="font4">FIRST NAME</h4>
					</div>
					<div class="col-md-7 right-m" style="border-top: 2px solid #31B3FF;">
                        <div class="right-m" style="border-top: 2px solid #31B3FF;">
                            <?= $form->field($model, 'name')->textInput([
                                'type' => 'text',
                                'class' => 'form-control style-imp',
                                'id' => 'InputFirstName',
                                'placeholder' => 'First Name'
                            ]) ?>
                        </div>
					</div>
					<div class="col-md-5 left-m" style="border-top: 2px solid #31B3FF;">
						<h4 class="font4">LAST NAME</h4>
					</div>
					<div class="col-md-7 right-m" style="border-top: 2px solid #31B3FF;">
                        <?= $form->field($model, 'surname')->textInput([
                            'type' => 'text',
                            'class' => 'form-control style-imp',
                            'id' => 'InputLastName',
                            'placeholder' => 'Last Name'
                        ]) ?>
					</div>
					<div class="col-md-5 left-m" style="border-top: 2px solid #31B3FF;">
						<h4 class="font4">EMAIL</h4>
					</div>
					<div class="col-md-7 right-m" style="border-top: 2px solid #31B3FF;">
                        <?= $form->field($model, 'email')->textInput([
                            'type' => 'email',
                            'class' => 'form-control style-imp',
                            'id' => 'InputEmail',
                            'placeholder' => 'Email'
                        ]) ?>
					</div>
					<div class="col-md-5 left-m" style="border-top: 2px solid #31B3FF;">
						<h4 class="font4">PASSWORD</h4>
					</div>
					<div class="col-md-7 right-m" style="border-top: 2px solid #31B3FF;">
                        <?= $form->field($model, 'password')->textInput([
                            'type' => 'password',
                            'class' => 'form-control style-imp',
                            'id' => 'InputPassword',
                        ]) ?>
					</div>
				</div>
				<div class="col-md-6 wow bounceInRight" data-wow-offset="200" data-wow-delay="0.5s" data-wow-duration="2s">
					<div class="col-md-5 left-m" style="border-top: 2px solid #31B3FF;">
						<h4 class="font4">NETWORK</h4>
					</div>
					<div class="col-md-7 right-m" style="border-top: 2px solid #31B3FF;">
						<div class="form-group">
                            <?= $form->field($model_info, 'network')->dropDownList(ArrayHelper::map($model_university->find()->asArray()->all(),
                                'id', 'name'), [
                                //'prompt' => Yii::t('administration', 'Select ...'),
                                'class' => 'form-control style-imp'
                            ])->label(false) ?>
						</div>
					</div>

					<div class="col-md-5 left-m" style="border-top: 2px solid #31B3FF;">
                                            <h4 class="font4">ACADEMIC STATUS</h4>
					</div>
					<div class="col-md-7 right-m" style="border-top: 2px solid #31B3FF;">
						<div class="form-group">
                                                    <?= $form->field($model_info, 'academic_status')->dropDownList(ArrayHelper::map($model_user_role->find()->where(['status' => '1'])->asArray()->all(),
                                                        'id', 'name'), [
                                                        //'prompt' => Yii::t('administration', 'Select ...'),
                                                        'class' => 'form-control style-imp'
                                                    ])->label(false) ?>
						</div>
					</div>
					<!--<div class="col-md-5 left-m" style="border-top: 2px solid #31B3FF;">
						<h4 class="font4">CLASS</h4>
					</div>
					<div class="col-md-7 right-m" style="border-top: 2px solid #31B3FF;">
						<div class="form-group">
						  <select class="form-control" id="sel3">
						    <option>Senior</option>
						    <option>Senior</option>
						    <option>Senior</option>
						    <option>Senior</option>
						  </select>
						</div>
					</div>-->
                    <input type="submit" value="Sign Up" class="btn btn-info sign">
				</div>
			</div>
		</div>
	</div>
