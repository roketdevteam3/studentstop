<?php

/**
 * Created by PhpStorm.
 * User: kuzio
 * Date: 17.12.15
 * Time: 18:25
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\authclient\widgets\AuthChoice;

$this->registerJsFile('/js/login.js');
?>

<video autoplay loop muted class="bgvideo" id="bgvideo" style="position: fixed;
    top:-100px;
    right: 0;
    bottom: 0;
    min-width: 100%;
    min-height: 100%;
    width: auto;
    height: auto;
    z-index: -9999;">
    <source src="../../default_img/Good_Mood.mp4" type="video/mp4"></source>
</video>

<div class="row bg-login printable" style="position: relative;">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="store-btn-wrap">
                            <a class="apple-btn" href="#">
                                <img src="../../../images/icon/apple-ic.png" alt="">
                                <p>Available on the<br/><span>App Store</span></p>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="store-btn-wrap">
                            <a class="play-btn" href="#">
                                <img src="../../../images/icon/play-ic.png" alt="">
                                <p>Get it on<br/><span>Google Play</span></p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="login-screen">
                    <div class="panel-login">
                        <div id="pane-login" class="panel-body active">
                            <?php $form = ActiveForm::begin(
                                [
                                    'id' => 'login-form',
                                    'fieldConfig' => [
                                        'template' => "{input}{error}{hint}"
                                    ],
                                    'action' => Url::base(true) . '/users/default/login'
                                ]); ?>
                            <h4>You are student?</h4>
                            <p class="form-subtitle">Studentstop created for you</p>

                            <?= $form->field($model, 'email')->textInput([
                                'type' => 'email',
                                'class' => 'form-control ',
                                'placeholder' => 'Email'
                            ]) ?>

                            <?= $form->field($model, 'password')->passwordInput([
                                'class' => 'form-control ',
                                'placeholder' => 'Password'
                            ]) ?>

                            <div class="form-buttons clearfix" style="margin-top: 0px;display:none;height:0px;">
                                <label class="pull-left" style="margin-top: 0px"><?= $form->field($model, 'rememberMe')->checkbox()->label(false) ?><span>Remember me</span></label>
                            </div>
                            <div class="form-group button-wrap">
                                <button type="submit" class="btn login-btn">Login</button>
                                <p>or</p>
                                <a class="btn sign-in-btn show-pane-create-account">Sign up</a>
                            </div>

                            <?php $authAuthChoice = AuthChoice::begin([
                                'baseAuthUrl' => ['/users/default/auth'],
                                'autoRender' => false
                            ]); ?>

                            <div class="social-accounts">
                                <?php foreach ($authAuthChoice->getClients() as $client): ?>
                                    <?= Html::a('<img src="'.Url::home().'images/icon/'.$client->name.'.png">', ['auth', 'authclient'=> $client->name, ]) ?>
                                <?php endforeach; ?>
                            </div>

                            <?php AuthChoice::end(); ?>
                            <!--
                            <ul class="extra-links">
                                <li><a href="#" class="show-pane-forgot-password">Forgot your password</a></li>
                                <li><a href="#" class="show-pane-create-account">Create a new account</a></li>
                            </ul> -->
                            <?php ActiveForm::end() ?>
                        </div><!--#login.panel-body-->

                        <div id="pane-forgot-password" class="panel-body">
                            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
                            <h2>Forgot Your Password</h2>
                            <div class="form-group">
                                <div class="inputer">
                                    <div class="input-wrapper">
                                        <?= Html::activeTextInput($model, 'email', ['placeholder' => 'Enter your email address', 'type' => 'email', 'class' => 'form-control']); ?>
                                    </div>
                                </div>
                            </div><!--.form-group-->
                            <div class="form-buttons clearfix">
                                <button type="submit" class="btn btn-white pull-left show-pane-login">Cancel</button>
                                <button type="submit" class="btn btn-success pull-right">Send</button>
                            </div><!--.form-buttons-->
                            <?php ActiveForm::end(); ?>
                        </div><!--#pane-forgot-password.panel-body-->

                        <div id="pane-create-account" class="panel-body">
                            <h4>Create a new account</h4>
                            <?php $form = ActiveForm::begin([
                                'fieldConfig' => [
                                    'template' => "{input}{error}{hint}",
                                    'options' => [
                                        'tag' => 'span'
                                    ]
                                ],
                                'action' => Url::to('/users/signup',true)
                            ]); ?>
                            <div class="row full-name-wrap">
                                <div class="col-md-6">
                                    <?= $form->field($model_sign_up, 'name')->textInput([
                                        'type' => 'text',
                                        'class' => 'form-control style-imp',
                                        'placeholder' => 'Enter your name'
                                    ]) ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($model_sign_up, 'surname')->textInput([
                                        'type' => 'text',
                                        'class' => 'form-control ',
                                        'placeholder' => 'Enter your surname'
                                    ]) ?>
                                </div>
                            </div>

                            <?= $form->field($model_sign_up, 'email')->textInput([
                                'type' => 'email',
                                'class' => 'form-control ',
                                'placeholder' => 'Enter your email'
                            ]) ?>

                            <?= $form->field($model_sign_up, 'password')->passwordInput([
                                'class' => 'form-control ',
                                'placeholder' => 'Enter your password'
                            ]) ?>
                            <?= $form->field($model_sign_up, 'password_confirm')->passwordInput([
                                'class' => 'form-control ',
                                'placeholder' => 'Enter your password again'
                            ]) ?>
                            
                            <?= $form->field($model_info, 'academic_status')->dropDownList(ArrayHelper::map($model_user_role->find()->where(['status' => '1'])->asArray()->all(),
                                'id', 'name'), [
                                //'prompt' => Yii::t('administration', 'Select ...'),
                                'class' => 'form-control style-imp'
                            ])->label(false) ?>
                            
                            <?= $form->field($model_info, 'network')->dropDownList(ArrayHelper::map($model_university->find()->asArray()->all(),
                                'id', 'name'), [
                                //'prompt' => Yii::t('administration', 'Select ...'),
                                'class' => 'form-control style-imp'
                            ])->label(false) ?>

                            <!-- <div class="form-group">
                                <label><input type="checkbox" name="remember" value="1"> I have read and agree to the term of use.</label>
                            </div> -->

                            <button type="submit" class="btn sign-up-btn">Sign Up</button>
                            <!-- <a class="btn btn-white pull-left show-pane-login" style="width:100%;font-size:14px;">Back to login</a> -->
                            
                            <?php  $authAuthChoice = AuthChoice::begin([
                                'baseAuthUrl' => ['/users/default/auth'],
                                'autoRender' => false
                            ]); ?>
                                <div class="social-accounts">
                                    <?php foreach ($authAuthChoice->getClients() as $client): ?>
                                        <?= Html::a('<img src="'.Url::home().'images/icon/'.$client->name.'.png">', ['auth', 'authclient'=> $client->name, ]) ?>
                                    <?php endforeach; ?>
                                </div>
                            <?php AuthChoice::end(); ?>

                            <?php ActiveForm::end() ?>


                        </div><!--#login.panel-body-->

                    </div><!--.blur-content-->
                </div>
            </div>
        </div>
    </div>


    
    
    
<!--
<div class="bg-blur dark">
    <div class="overlay"></div><!--.overlay-->
<!--</div>.bg-blur-->

<!--<svg version="1.1" xmlns='http://www.w3.org/2000/svg'>
    <filter id='blur'>
        <feGaussianBlur stdDeviation='7' />
    </filter>
</svg> -->
<div class="clear:both;"></div>
</div>




<?php
  $this->registerJsFile('/scripts/user-pages.js');
  $this->registerJsFile('/js/pleasure.js');
  $this->registerJsFile('/js/layout.js');
  $this->registerJs("    $(document).ready(function () {
        Pleasure.init();
        Layout.init();
        UserPages.login();
    });")
?>

