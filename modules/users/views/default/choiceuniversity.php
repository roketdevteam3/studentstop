<?php

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Url;
?>

<div class="row">
    <div class="container-fluid background-block">
        <div class="modal-dialog" style="width:90%;">
            <div class="modal-content" style="background-color:rgba(255, 255, 255, 0.3);">
            <?php $form = ActiveForm::begin(); ?>
                <div class="modal-body">
                    <h3 style="text-align: center"><b>Welcome <?= \Yii::$app->user->identity->name; ?>!</b></h3><br>
                    <h4 style="text-align: center">You are going to use StudentStop platform.</h4>
                    <h4 style="text-align: center">Pleace read terms and condition and confirm you promary network University</h4>
                        <div class="row">
                            <div style="    padding: 15px 1px 70px;">
                            <div class=" col-sm-6">
                                <div class="col-sm-5">
                                    University
                                </div>
                                <div class="col-sm-7">
                                    <?= Html::dropDownList('university', 'university', $universityArray,['class' => 'form-control','style' => 'width:100%;background-color:white;color: #8b8b8b;    text-align-last: center;']); ?>
                                </div>
                            </div>
                            <div class=" col-sm-6">
                                <div class="col-sm-5">
                                    Userrole
                                </div>
                                <div class="col-sm-7">
                                    <?= Html::dropDownList('userrole', 'Userrole', $userRoleArray,['class' => 'form-control','style' => 'width:100%;background-color:white;color: #8b8b8b;    text-align-last: center;']); ?>
                                </div>
                            </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <iframe width="480" height="360" src="https://www.youtube.com/embed/xFa2_PVMeDQ" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="col-sm-6">
                                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <input type="checkbox" name="option2" required="required" value="a2"> terms and conditions of Student Stop LLC
                                </div>
                                <div class="col-sm-6">
                                    <?= Html::submitButton('Accept & Continue',['class' => 'btn btn-info pull-right']); ?>
                                </div>
                            </div>
                        </div>
                </div>
            <?php $form = ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>