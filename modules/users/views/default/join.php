<?php
/**
 * Created by PhpStorm.
 * User: kuzio
 * Date: 17.12.15
 * Time: 18:25
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

?>

<div class="home_background">
    <div class="content_home">
        <h1 class="title wow bounceInDown" data-wow-offset="200" data-wow-delay="0.5s" data-wow-duration="2s">the
            studentstop.</h1>
    </div>
    <div class="wrapper">
        <div class="sub-wrapper">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 wow bounceInRight" data-wow-offset="200" data-wow-delay="0.5s"
                     data-wow-duration="2s">
                    <?php $form = ActiveForm::begin([
                        'fieldConfig' => [
                            'template' => "{input}{error}",
                            'options' => [
                                'tag' => 'span'
                            ]
                        ],
                        'action' => '/users/default/confirm-signup'
                    ]); ?>
                    <h3 style="text-align: center;">SIGN UP</h3>
                    <h4>Are you not a member? Join <a href="#">The Student Stop</a> and register to begin your road to
                        academic success</h4>

                    <div class="left-m" style="border-top: 2px solid #31B3FF;">
                        <h4>FIRST NAME</h4>
                    </div>
                    <div class="right-m" style="border-top: 2px solid #31B3FF;">
                        <?= $form->field($model_sign_up, 'name')->textInput([
                            'type' => 'text',
                            'class' => 'form-control style-imp',
                            'id' => 'FirstName',
                            'placeholder' => 'First name'
                        ]) ?>
                    </div>

                    <div class="left-m" style="border-top: 2px solid #31B3FF;">
                        <h4>LAST NAME</h4>
                    </div>
                    <div class="right-m" style="border-top: 2px solid #31B3FF;">
                        <?= $form->field($model_sign_up, 'surname')->textInput([
                            'type' => 'text',
                            'class' => 'form-control style-imp',
                            'id' => 'LastName',
                            'placeholder' => 'Last name'
                        ]) ?>
                    </div>
                    <div class="text-right">
                        <input type="submit" value="Sign Up" class="btn btn-info sign">
                    </div>
                    <?php ActiveForm::end() ?>
                </div>
            </div>
        </div>
    </div>
