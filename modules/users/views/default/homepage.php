<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\components\comments_tree_mackrais\CommentTreeWidget;
use app\modules\comments\models\Comments;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\models\Users */

/* data for comments widget */


?>

    <?php foreach ($main_menu_logged as $k => $item): ?>
        <div class="col-md-3">
            <?php if($item['link'] == 'profile'){ ?>
                <div class="panel" style="border: 1px solid rgba(217, 217, 217, 0.6);">
                    <div class="panel-heading" style="">
                        <div class="panel-title">
                            <h4>
                                <a href="<?php if ($item['absolute'] > 0){ echo Url::home(true).$item['link'].'/'.\Yii::$app->user->id; }else{ echo $item['link'].'/'.\Yii::$app->user->id; } ?>" onclick="<?php if (!empty($item['onclick'])) echo $item['onclick'];  ?>" style="color:black;">
                                    <?= $item['title'] ?>
                                </a>
                            </h4><br>
                            <h4>
                                <?= \Yii::$app->user->identity->name; ?>
                                <?= \Yii::$app->user->identity->surname; ?>
                            </h4>
                        </div>
                    </div> 
                    <div class="panel-body" style="padding:0px;">
                        <img src="../../default_img/news1.jpg" style="width:100%;height:70px;">
                    </div> 
                </div> 
            <?php }elseif($item['link'] == 'university'){ ?>
                <div class="panel" style="border: 1px solid rgba(217, 217, 217, 0.6);">
                    <div class="panel-heading" style="background: #F7F7F7;">
                        <div class="panel-title">
                            <h4>
                                <a href="<?php if ($item['absolute'] > 0) echo Url::home(true).$item['link']; else echo $item['link']; ?>" onclick="<?php if (!empty($item['onclick'])) echo $item['onclick'];  ?>"  style="color:black;">
                                    <?= $item['title'] ?>
                                </a>
                            </h4>
                        </div>
                    </div> 
                    <div class="panel-body" style="padding:0px;">
                        <img src="../../default_img/news1.jpg" style="width:100%;height:100px;">
                    </div> 
                </div> 
            <?php }else{ ?>
                <div class="panel" style="border: 1px solid rgba(217, 217, 217, 0.6);">
                    <div class="panel-heading" style="background: #F7F7F7;">
                        <div class="panel-title">
                            <h4>
                                <a href="<?php if ($item['absolute'] > 0) echo Url::home(true).$item['link']; else echo $item['link']; ?>" onclick="<?php if (!empty($item['onclick'])) echo $item['onclick'];  ?>"  style="color:black;">
                                    <?= $item['title'] ?>
                                </a>
                            </h4>
                        </div>
                    </div> 
                    <div class="panel-body" style="padding:0px;">
                        <img src="../../default_img/news1.jpg" style="width:100%;height:100px;">
                    </div> 
                </div> 
            <?php } ?>
        </div>
    <?php endforeach ?>