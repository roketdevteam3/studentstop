<?php

use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use app\assets\MembersAsset;
MembersAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Members');
$this->params['breadcrumbs'][] = $this->title;
?>

	<div class="members-right-panel">
		<?= $this->render('right_menu',[
			'arrayUniversity' => $arrayUniversity,
			'arrayUserRole' => $arrayUserRole,
			'arrayMajors' => $arrayMajors,
			'arrayCourse' => $arrayCourse,
                ]); ?>
	</div>
	<div class="row members-content">
		<div class="container-fluid background-block">
			<div class="row">
				<div class="col-sm-12" style="display:none;">
					<div class="row">
	                    <div class="input-group search-top-group">
	                        <input type="text" class="form-control getSearchInput" data-tabs_type="members" name="search_key" placeholder="Search by address or keywords">
	                        <span class="input-group-btn">
	                            <button class="btn btn-default topSearchClick" type="button">Search</button>
	                        </span>
	                    </div>
					</div>
                </div>
				<div class="col-sm-12">
					<div class="row">
						<input type="hidden" class="sender_id" value="<?= \Yii::$app->user->id?>">
						<input type="hidden" class="user_university_id" value="<?= $modelUsersInfo->university_id; ?>">
						<div class="map-wrap">
							<div id="map" class="map"></div>
							<div class="col-sm-12">
								<ul class="nav nav-pills userButtonsInfo">
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="container-fluid ">
                    <ul class="nav nav-tabs navigation-tabs">
                        <li class="active"><a data-toggle="tab" class="membersButton" href="#members">Members</a></li>
                        <li><a data-toggle="tab" class="universityButton" href="#university">University</a></li>
                    </ul>

                    <div class="tab-content members-tab-content" style="height: auto;">
                      <div id="members" class="tab-pane fade in active " >
                            <div class="usersClass">
                                
                            </div>
                      </div>
                      <div id="university" class="tab-pane fade">
                            <div class="universityClass">
                                
                            </div>
                      </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
