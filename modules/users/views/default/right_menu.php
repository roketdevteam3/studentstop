<?php
	use yii\bootstrap\Html;
	use yii\helpers\Url;
?>

<div class="members-form">
	<!--  -->
	<div class="membersFilter">
		<!--<input type="text" class="form-control getCountryOrCity" name="country_city" placeholder="Country or City">-->
		<div class="form-group">
			<label for="">
				University
			</label>
			<?= Html::dropDownList('university', null, $arrayUniversity, ['prompt' => 'University','class' => 'form-control']); ?>
		</div>
		<?php /* = Html::dropDownList('country', null, ['1' => 'country 1', '2' => 'country 2' ], ['prompt' => 'Country','class' => 'form-control','style'=>'max-width:160px;border:1px solid white;font-size:12px;padding: 0px;height:24px;border-radius:4px;padding-left:5px;color:white;margin-top: 10px']); ?>
		<?= Html::dropDownList('state', null, ['1' => 'state 1', '2' => 'state 2' ], ['prompt' => 'State','class' => 'form-control','style'=>'max-width:160px;border:1px solid white;font-size:12px;padding: 0px;height:24px;border-radius:4px;padding-left:5px;color:white;margin-top: 10px']); */ ?>
		<div class="form-group">
			<label for="">
				Course
			</label>
			<?= Html::dropDownList('course', null, [], ['prompt' => ['label' => 'Course', 'value' => ''],'class' => 'form-control']); ?>
		</div>
		<div class="form-group">
			<label for="">
				User status
			</label>
			<?= Html::dropDownList('user_status', null, $arrayUserRole, ['prompt' => 'User status','class' => 'form-control']); ?>
		</div>
		<div class="form-group">
			<label for="">
				Sex
			</label>
			<?= Html::dropDownList('sex', null, ['0' => 'Male','1' => 'Female'], ['prompt' => 'Sex','class' => 'form-control']); ?>
		</div>
		<div class="form-group age-filter">
			<label>Age min</label>
			<input type="text" name="age_from" placeholder="0" class="form-control">
		</div>
		<div class="form-group age-filter">
			<label>Age max</label>
			<input type="text" name="age_to" placeholder="99" class="form-control">
		</div>
		<!--<input type="submit" name="searchButton" value="Search" class="btn searchButton">-->
	</div>
	<div class="universityFilter" style="display:none;">
		<div class="form-group">
			<label for="">
				Majors
			</label>
			<?= Html::dropDownList('major_ufilter', null, $arrayMajors, ['prompt' => 'Majors','class' => 'form-control']); ?>
		</div>
		<div class="form-group">
			<label for="">
				Course
			</label>
			<?= Html::dropDownList('course_ufilter', null, $arrayCourse, ['prompt' => 'Course','class' => 'form-control']); ?>
		</div>
		<div class="form-group">
			<label for="">
				Rating
			</label>
			<?= Html::dropDownList('rating_ufilter', null, [1 => 1,  2 => 2,  3 => 3,  4 => 4,  5 => 5], ['prompt' => 'Rating', 'class' => 'form-control']); ?>
		</div>
		<input type="submit" name="searchUniversityButton" value="Search" class="btn searchUniversityButton">
	</div>
</div>
