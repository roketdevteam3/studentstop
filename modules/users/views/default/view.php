<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\components\comments_tree_mackrais\CommentTreeWidget;
use app\modules\comments\models\Comments;


/* @var $this yii\web\View */
/* @var $model app\models\Users */

/* data for comments widget */


$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="university-index">
    <div class="container-fluid padding-lr" style="height: 100%;">
        <div class="col-md-2 padding-lr" style="border: 1px solid #000;">
            <img src="../../../../web/default_imXVg/logo.png" alt="logo" style="width: 100%;">
        </div>
        <div class="col-md-10 padding-lr" style="border: 1px solid #000; background-color: rgba(0, 0, 0, 0.5);">
            <nav class="navbar navbar-default" role="navigation" style="margin-bottom: 0px">
                <div class="container-fluid padding-lr">
                    <!-- Бренд та перемикач згруповані для кращого відображення на мобільних пристроях -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <p class="navbar-brand" style="color: #337ab7; margin-left: 10px;"><i class="fa fa-home"></i></p>
                    </div>


<div class="container">
    <div class="row users-view">


        <div class="col-md-2">
            <div class="thumbnail-avatar-block">
                <i class="avatar-icon glyphicon glyphicon-user" style=""></i>
            </div>

        </div>

        <div class="col-md-3">
            <h3>First Name: <em><?= $model->name; ?></em></h3>
            <h3>Last Name: <em><?= $model->surname; ?></em></h3>
            <h3>Email: <em><?= $model->email; ?></em></h3>

            <?= Html::a('Edit', ['//users/default/update','id'=>$model->id]
            ); ?>

        </div>


    </div>
</div>
