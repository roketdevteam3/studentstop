<?php
/**
 * Created by PhpStorm.
 * User: minace
 * Date: 2/29/16
 * Time: 10:50 PM
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use app\widgets\ClassmembersWidget;
use app\widgets\JoinclassWidget;
use app\widgets\RatingstarWidget;
use app\widgets\rating\RatingclassWidget;
use app\assets\CreditsprofileAsset;
CreditsprofileAsset::register($this);
   use app\assets\ClassrightmenuAssets;
    ClassrightmenuAssets::register($this);
?>
	<div class="row background-block">
		<?php echo $this->render('right_menu',['modelUser' => $modelUser, 'modelUserInfo' => $modelUserInfo]); ?>
		<div class="profile-content">
                    <?php foreach($modelClass as $class){ ?>
                        <div class="col-sm-1-5 col-md-1-5 col-lg-1-5">

                                <div class="class-block">
                                        <h4 class="class-name"><?= $class['class_name']; ?></h4>
                                        <p class="proffesor-name">Proffessor:<?= $class['name'].' '.$class['surname'];?></p>
                                        <div class="class-image">
                                                <img class="img-responsive" src="<?= Url::home().'images/classes/'.$class['img_src']; ?>">
                                        </div>
                                        <div class="class-rating" style="float: left">
                                                <span>Rating: </span>
                                        </div>
                                    <div  class="mystar" style=" float: left">
                                        <?= RatingclassWidget::widget(['class_id' => $class['id'], 'type_show' => 'without_rat']); ?>
                                    </div>
                                        <div class="class-info">
                                                <table>
                                                        <tr>
                                                                <td style="padding-right: 8px">Members: </td>
                                                                <td> <?= ClassmembersWidget::widget(['class_id' =>  $class['id']]) ?></td>
                                                        </tr>
                                                        <tr>
                                                                <td>Major: </td>
                                                                <td><?= $class['major_name']; ?></td>
                                                        </tr>
                                                        <tr>
                                                                <td>Course: </td>
                                                                <td><?= $class['course_name']; ?></td>
                                                        </tr>
                                                        <tr>
                                                                <td>Status: </td>
                                                                <td>
                                                                        <?php if($class['class_status'] == '1'){ ?>
                                                                                <span class="active-status">
                                                                                        Active
                                                                                </span>
                                                                        <?php }else{ ?>
                                                                                <span class="no-active-status">
                                                                                        Not started
                                                                                </span>
                                                                        <?php } ?>
                                                                </td>
                                                        </tr>
                                                </table>
                                        </div>
                                        <div class="">
                                                <a href="<?= Url::home().'class/'.$class['class_url_name']; ?>" class="btn btn-watch">
                                                        Watching
                                                </a>
                                                <?= JoinclassWidget::widget(['class_id' =>  $class['id']]) ?>
                                        </div>
                                        <div style="clear: both"></div>
                                </div>
                        </div>
                <?php } ?>
                </div>
	</div>
