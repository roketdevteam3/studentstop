<?php
/**
 * Created by PhpStorm.
 * User: minace
 * Date: 2/29/16
 * Time: 10:50 PM
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use app\assets\CreditsprofileAsset;
CreditsprofileAsset::register($this);

?>
	<div class="row">
		<?php echo $this->render('right_menu',['modelUser' => $modelUser, 'modelUserInfo' => $modelUserInfo]); ?>
		<div class="profile-content">
			<div class="container-fluid background-block">
				<div class="profile-container">
					<div class="users-form">
					</div>
				</div>
			</div>
		</div>
	</div>
