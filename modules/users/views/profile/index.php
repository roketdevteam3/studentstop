<?php
/**
 * Created by PhpStorm.
 * User: minace
 * Date: 2/29/16
 * Time: 10:50 PM
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
$model->password = '';
?>

<div class="users-form">
    <div class="header">
        <?php echo $this->render('header_profile',[]); ?>
    </div>
    <?php /* $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <h2><?= Yii::t('app','Change password') ?></h2>
    <hr />

    <?= $form->field($model, 'old_password')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'confirm_password')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton( Yii::t('app', 'Save'), ['class' => 'btn btn-primary', 'data-mr-loader'=>'show']) ?>
    </div>

    <?php ActiveForm::end(); */ ?>
</div>
