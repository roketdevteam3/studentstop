<?php
/**
 * Created by PhpStorm.
 * User: minace
 * Date: 2/29/16
 * Time: 10:50 PM
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use app\assets\StreamingownerAsset;
use app\assets\StreamingreciverAsset;
?>

<div class="users-form">
    <div class="row">
        <div class="col-sm-12" style="padding:0px;">
            <?php //echo $this->render('header_profile',['modelUser' => $modelUser,'modelUserInfo' => $modelUserInfo]); ?>
        </div>
    </div>
    <div class="row">
        <h1>Streaming</h1>


          <?php if($modelUser->id == \Yii::$app->user->id){ ?>
            <?php StreamingownerAsset::register($this); ?>
            <video id="owner" data-owner-id="<?= $modelUser->id ?>" autoplay muted></video>
          <?php }else{ ?>
            <?php StreamingreciverAsset::register($this); ?>
            <video id="reciver" data-owner-id="<?= $modelUser->id ?>" data-reciver-id="<?=\Yii::$app->user->id ?>" autoplay></video>
          <?php } ?>
    </div>

</div>
