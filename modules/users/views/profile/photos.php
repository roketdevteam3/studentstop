<?php
/**
 * Created by PhpStorm.
 * User: minace
 * Date: 2/29/16
 * Time: 10:50 PM
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

use app\assets\ImageuploadAsset;
ImageuploadAsset::register($this);

?>
<div style="display:none;" class="userPhotosId"><?= $modelUser->id; ?></div>
	<div class="row">
		<?php echo $this->render('right_menu',['modelUser' => $modelUser, 'modelUserInfo' => $modelUserInfo]); ?>
		<div class="profile-content">
			<div class="container-fluid background-block">
				<div class="profile-container gallery">
					<div class="users-form">
						<?php if($user_id == \Yii::$app->user->id){ ?>
							<div class="row">
								<div class="col-sm-12">
									<div class="drag-and-drop user_photos" action="../../users/profile/savedropedfile" class="">
										<div class="dz-message needsclick">
											<h4>Drop files here or click to upload.</h4>
										</div>
									</div>
								<?php $form = ActiveForm::begin([
										'id' => 'formSavePhotos',
									]); ?>
									<div></div>
									<?= Html::submitButton('Save', ['class' => 'btn btn-save', 'name' => 'savePhotos']) ?>
								<?php ActiveForm::end();  ?>
								</div>
							</div>
						<?php } ?>
						<div class="row">
							<div class="col-sm-5">
								<ul class="nav nav-tabs navigation-tabs">
									<li class="active my_photo_profile">
										<a href="#">My photos</a>
									</li>
									<li class="my_video_profile">
										<a href="#">My video</a>
									</li>
								</ul>
							</div>
							<div class="col-sm-12 fileContainer">
								<?php /* ?>
									<div class="user-image-list">
											<?php foreach($modelUserImages as $images){ ?>
															<a href="<?= $storeFolder.$images->image_name;?>" rel="prettyPhoto" title="This is the description">
													<?php */ ?>
													<?php /* ?>
															<div class="profileImageClick img-thumbnail" data-photo_id='<?= $images->id; ?>' style="cursor:pointer;width:160px;height:160px;background-image: url('<?= $storeFolder.$images->image_name;?>');background-size:cover;background-repeat:no-repeat;" /></div>
															</a>
											<?php } ?>
									</div>
								<?php */ ?>
							</div>
							<div class="col-sm-12">
								<?= LinkPager::widget(['pagination'=>$pagination]); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="imagesProfile" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<div class='col-sm-12'>
						<img class="img-responsive modalImagesShow" src='<?= Url::home(); ?>images/default_wrapper.png'>
					</div>
					<div class="col-sm-12">
						<i class="fa fa-heart pull-right photoLike"> Like <span class="like_count">0</span></i>
					</div>
					<div class="col-sm-12">
						<h5 class="comment">Comment</h5>
						<textarea class="form-control newComment"></textarea>
						<button class="btn btn-action pull-right saveComment">Save</button>
					</div>
				</div>
				<div class="modal-footer commentList">
				
				</div>
			</div>
		</div>
	</div>
	<div id="videoProfile" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<div class='col-sm-12 videoContainer'>
					</div>
					<!--<div class="col-sm-12">
						<i class="fa fa-heart pull-right photoLike"> Like <span class="like_count">0</span></i>
					</div>
					<div class="col-sm-12">
						<h5 class="comment">Comment</h5>
						<textarea class="form-control newComment"></textarea>
						<button class="btn btn-action pull-right saveComment">Save</button>
					</div> -->
				</div>
							<!--
				<div class="modal-footer commentList">
				
				</div> -->
			</div>
		</div>
	</div>
