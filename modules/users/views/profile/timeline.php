
<?php
/**
 * Created by PhpStorm.
 * User: minace
 * Date: 2/29/16
 * Time: 10:50 PM
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use dosamigos\tinymce\TinyMce;
?>
	<?php /* ?>
	<div class="row">
		<div class="col-sm-12" style="padding:0px;">
			<?php echo $this->render('header_profile',['modelUser' => $modelUser, 'modelUserInfo' => $modelUserInfo]); ?>
		</div>
	</div>
	<?php */ ?>
	<div class="row">
		<?php echo $this->render('right_menu',['modelUser' => $modelUser, 'modelUserInfo' => $modelUserInfo]); ?>
		<div class="profile-content">
			<div class="container-fluid background-block">

					<?php if($modelUser->id == \Yii::$app->user->id){ ?>
					<div class="row">
						<div class="profile-top-line">
							<button class="btn btn-add-post" data-toggle="modal" data-target="#newPost">
								+
							</button>
						</div>
					</div>
					<div class="modal fade" id="newPost" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
															<?php $form = ActiveForm::begin(); ?>
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">x</button>
									<h4 class="modal-title">Add new post</h4>
								</div>
								<div class="modal-body">
									<?= $form->field($modelNewsNew, 'title')->textInput(); ?>
									<?= $form->field($modelNewsNew, 'content')->textArea(['rows' => '6']); ?>
								</div>
								<div class="modal-footer">
									<div class="form-group">
										<button type="button" class="btn btn-close" data-dismiss="modal">Cancel</button>
										<?= Html::submitButton( Yii::t('app', 'Share a post.'), ['name'=> 'add_user_news', 'class' => 'btn btn-action']) ?>
									</div>
								</div>
															<?php ActiveForm::end();  ?>
							</div>
						</div>
					</div>
				<?php } ?>
				<div id="masonry_hybrid_demo1">
								<?php foreach($modelUserEvents as $events){ ?>
									<div class="grid-item">
										<div class="grid-sizer"></div>
										<div class="gutter-sizer"></div>

											<div style="padding:10px;">
											<div class="card tile card-post" style="margin:0px;">
												<div class="card-heading">
													<div class="author-avatar">
														<img src="<?= Url::home().'images/users_images/'.$events->usernameInfo['avatar']; ?>" class="user-image" alt="">
													</div>
														<div class="post-info">
															<p class="author-post">
																<?= HTML::a($events->usernameInfo['name'].' '.$events->usernameInfo['surname'],Url::home().'profile/'.$events->user_id); ?>

																										<?php
																										switch ($events->event_type){
																												case 'image': ?>
																												added a new photo
																											</p>
																											<p class="time"><?= $events->date_create; ?></p>
																										</div>
																										<div class="card-body">
																											<img class="img-responsive" src="<?= Url::home(); ?>images/users_images/<?= $events->event_model['image_name']; ?>" class="post-single-image" alt="">
																										</div>
																								<?php break;
																											case 'news': ?>
																											shared a post
																											</p>
																											<p class="time"><?= $events->date_create; ?></p>
																										</div>
																										<div class="card-body">
																											<h4 class="post-title"><?= $events->event_model['title'].'<br>'; ?></h4>
																											<div class="post-content">
																												<?= $events->event_model['content'].'<br>'; ?>
																											</div>
																										</div>
																										<?php break;
																										case 'profile': ?>
																										<?php var_dump($events->event_model); ?>
																										<?php break;
																										case 'friends':?>
																										<?php var_dump($events->event_model); ?>
																										<?php break;
																										case 'avatar':?>
																										change avatar
																											</p>
																											<p class="time"><?= $events->date_create; ?></p>
																										</div>
																										<div class="card-body">
																											<img class="img-responsive" src="<?= Url::home(); ?>images/users_images/<?= $events->event_model['avatar']; ?>" class="post-single-image" alt="">
																										</div>
																					<?php break;
																										case 'avatar_wrapper':?>
																										change avatar wrapper
																										</p>
																										<p class="time"><?= $events->date_create; ?></p>
																									</div>
																									<div class="card-body">
																										<img class="img-responsive" src="<?= Url::home(); ?>images/users_images/<?= $events->event_model['avatar_wrapper']; ?>" class="post-single-image" alt="">
																									</div>
																				<?php break;
																						} ?>
												</div>
											</div>
											</div>


									</div>
								<?php } ?>
						</div>
			</div>
		</div>
	</div>
