<?php
/**
 *
 */
use yii\bootstrap\ActiveForm;
?>
<div class="row">
    <?php echo $this->render('right_menu',['modelUser' => $modelUser, 'modelUserInfo' => $modelUserInfo]); ?>
    <div class="profile-content">
        <div class="container-fluid background-block">
            <div class="credits-container">
                <div class="users-form">
                    <div class="credits-header">
                        <h4>Total maney - <?= $maney; ?> $</h4>
                        <button type="button" class="btn btn-action btn-pay-credits" data-toggle="modal" data-target="#myModal">Withdraw</button>
                    </div>
                    <?php
                    if(Yii::$app->session->hasFlash('Error')) {
                        echo \yii\bootstrap\Alert::widget([
                            'options' => [
                                'class' => 'alert-warning',
                            ],
                            'body' => \Yii::t('app', 'You do not have the money!'),
                        ]);
                    }
                    ?>
                    <div id="myModal" class="modal padding-body pay-credits fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">x</button>
                                    <h4 class="modal-title">Pay credits</h4>
                                </div>
                                <?php $form = ActiveForm::begin()?>
                                <div class="modal-body text-center">
                                    <?= $form->field($output_maney, 'maney')?>
                                    <?= $form->field($output_maney, 'email_paypal')?>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
                                    <button class="btn btn-action" type="submit">Send</button>
                                </div>
                                <?php ActiveForm::end();?>
                            </div>
                        </div>
                    </div>
                    <?= \kartik\grid\GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'email_paypal',
                            'maney',
//                            'user_id',
                             'status:boolean',
                            'created_at:datetime',
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
