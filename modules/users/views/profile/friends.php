<?php
/**
 * Created by PhpStorm.
 * User: minace
 * Date: 2/29/16
 * Time: 10:50 PM
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use app\widgets\FollowbuttonWidget;
?>
	<div class="row">
		<?php echo $this->render('right_menu',['modelUser' => $modelUser, 'modelUserInfo' => $modelUserInfo]); ?>
		<div class="profile-content">
			<div class="container-fluid background-block">
				<div class="profile-container friends">
					<div class="users-form">
						<div class="row">
							<div class="col-sm-12">
								<?php  
									$url = Url::to('');
									$class   = 'class';
									$all = '';$clas ='';
									$pos = strpos($url, $class);
										if ($pos === false) {
											$all = 'active';
										}else{
											$clas = 'active';
										}     
								?>
								<ul class="nav nav-tabs navigation-tabs">
									<li role="presentation" class="<?= $all; ?>">
										<a href="<?= '/profile/'.$user_id.'/friends'; ?>">All</a>
									</li>
									<li role="presentation" class="<?= $clas; ?>">
										<a href="<?= '/profile/'.$user_id.'/friends/class'; ?>">Class mates</a>
									</li>
								</ul>
							</div>
							<div class='col-sm-12'>
								<div class="row usersContainer">
									<?php foreach($modelFriends as $friends){ ?>
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <div class="userClickClass user-block user-map userBlock userClass<?= $friends['id']; ?>" data-user_id="<?= $friends['id']; ?>">
                                                                                        <div class="user-img">
                                                                                            <?php if($friends['avatar'] != ''){ ?>
                                                                                                <img class="img-responsive" src="<?= Url::home(); ?>images/users_images/<?= $friends['avatar']; ?>" alt="">
                                                                                            <?php }else{ ?>
                                                                                                <img class="img-responsive" src="<?= Url::home(); ?>images/default_avatar.jpg" alt="">
                                                                                            <?php } ?>
                                                                                        </div>
                                                                                        <div class="center-block">
                                                                                            <div class="user-data">
                                                                                                <input type="hidden" class="userlatitude" value="<?= $friends['position_lat']; ?>">
                                                                                                <input type="hidden" class="userlongitude" value="<?= $friends['position_lng']; ?>">
                                                                                                <p  class="userNameSurname"><?= $friends['username']; ?> <?= $friends['surname']; ?></p>
                                                                                                <p class="post">(<?= $friends['user_role']; ?>)</p>
                                                                                                <div class="rating-star userRating  mystar">
                                                                                                </div>
                                                                                                <p class="userStatus">online</p>
                                                                                            </div>
                                                                                            <table class="table">
                                                                                                <tr>
                                                                                                    <td class="table-label">Status:</td>
                                                                                                    <td class="table-value"><?= $friends['user_role']; ?></td>
                                                                                                    <td class="table-label">University:</td>
                                                                                                    <td class="table-value universityName"><?= $friends['id']; ?></td>
                                                                                                    <td class="table-label">Classes:</td>
                                                                                                    <td class="table-value classCount">enrolled in 0</td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td class="table-label">Age:</td>
                                                                                                    <td class="table-value"><?= $friends['birthday']; ?></td>
                                                                                                    <td class="table-label">Friends:</td>
                                                                                                    <td class="table-value friendCount">20 members</td>
                                                                                                </tr>
                                                                                            </table>
                                                                                            <div class="address">
                                                                                                Address: 
                                                                                                <span><?= $friends['address']; ?></span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="user-btn-wrap">
                                                                                                <!--<div class="ranking">Ranking: <span class="rankingCount">70/100</span></div>-->
                                                                                                <a href="../../../profile/<?= $friends['id']; ?>" data-user_id="<?= $friends['id']; ?>" class="buttonAboutRightPages btn profile-btn">Profile</a>
                                                                                                <?= FollowbuttonWidget::widget(['friend_id' => $friends['id']]); ?>
                                                                                                <!--<div class="location-wrap"><span class="location">1 mile away</span><img src="../../../images/icon/map-ic.png" alt="" /></div>-->
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                    
									<?php } ?>
								</div>
							</div>
							<div class='col-sm-12'>
								<?= LinkPager::widget(['pagination'=>$pagination]); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
