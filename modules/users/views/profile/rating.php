<?php
/**
 * Created by PhpStorm.
 * User: minace
 * Date: 2/29/16
 * Time: 10:50 PM
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use kartik\rating\StarRating;
use app\assets\CreditsprofileAsset;
CreditsprofileAsset::register($this);

?>
	<div class="row">
		<?php echo $this->render('right_menu',['modelUser' => $modelUser, 'modelUserInfo' => $modelUserInfo]); ?>
		<div class="profile-content">
			<div class="userInfoId" style="display:none;"><?= $modelUser['id']; ?></div>
			<div class="container-fluid background-block">
				<br>
				<!--<div class="col-sm-12">
					<ul class="nav nav-tabs navigation-tabs ratingProfileMenu">
						<li class="active" data-action="rental">
							<a >
								Rental
							</a>
						</li>
						<li  data-action="rental_a_house">
							<a >
								Rental a house
							</a>
						</li>
						<li data-action="question_bank">
							<a >
								Question bank
							</a>
						</li>
						<li data-action="professor">
							<a >
								Professor
							</a>
						</li>
					</ul>
				</div>-->
                <div class="col-sm-6" style="margin: 0 auto; float: none;">
                    <div class="col-sm-12">
                    <table class="td_ratind">
                        <tr>
                            <td style="text-align: center"><p style="font-size: 23px; ">Ranking & Rating</p>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
				<div class="col-sm-6 blockViewProfileRating" style="margin: 0 auto; float: none;">
					<?php $arrayTypeRating = ['rental' => 'Rental',
						'rental_a_house' => 'Rental a house', 
						'question_bank' => 'Question bank', 
						'professor' => 'Professor']; ?>
					<?php foreach($arrayRatingResult as $type => $rating_result){ ?>
						<div class="col-sm-12">
                            <table class="td_ratind">

                                <tr>
                                    <td>
							<h4 >
								<?= $arrayTypeRating[$type]; ?>
							</h4>
                                    </td>
                                    <td>
							<div class="rating-blok" >
								<span>Rank:</span>
								<span class="rating-count ">1/100</span>
							</div>
                                    </td>
                                    <td>
                            <div class="mystar"style="position: relative; bottom: 5px;">
							<?= StarRating::widget([
									'name' => $type,
									'value' => $rating_result,
									'pluginOptions' => [
										'disabled'=>true,
										'showClear'=>false,
										'size'=>'xs',
										'showCaption' => false,
									]
								]); 
							?>
                            </div>
                                    </td>
                                </tr>
                            </table>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>

	</div>
