<?php
/**
 * Created by PhpStorm.
 * User: minace
 * Date: 2/29/16
 * Time: 10:50 PM
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use kartik\datetime\DateTimePicker;

?>

	<div class="row">
		<?php echo $this->render('right_menu',['modelUser' => $modelUser, 'modelUserInfo' => $modelUserInfo]); ?>
		<div class="profile-content">
			<div class="users-form">
				<?php echo $this->render('header_profile',['modelUser' => $modelUser,'modelUserInfo' => $modelUserInfo]); ?>
				<div class="container-fluid background-block">
					<div class="profile-container">
						<div class="row">
							<div class="col-md-5">
								<div class="user-info-table">
									<span class="edit-icon" type="button" data-toggle="modal" data-target="#update_profile"></span>
									<div id="update_profile" class="modal big-modal padding-body fade" role="dialog">
										<div class="modal-dialog">
											<!-- Modal content-->
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">Modal Header</h4>
												</div>
												<div class="modal-body">
													<?php $form = ActiveForm::begin(); ?>
													<?= $form->field($modelUser, 'name')->textInput(['maxlength' => true]) ?>
													<?= $form->field($modelUser, 'surname')->textInput(['maxlength' => true]) ?>
													<?= $form->field($modelUser, 'email')->textInput(['maxlength' => true])  ?>
													<?= $form->field($modelUserInfo, 'birthday')->widget(DateTimePicker::classname(), [
														'options' => ['placeholder' => 'Enter event time ...'],
														'pluginOptions' => [
															'format' => 'd-M-Y',
															'todayHighlight' => true
														]
													]);  ?>
													<?= $form->field($modelUserInfo, 'university_id')->dropDownList($universityArray); ?>
													<?= $form->field($modelUserInfo, 'address')->textInput(); ?>
													<?= $form->field($modelUserInfo, 'gender')->radioList(array('1'=>'Male', '2' =>'female'))->label('Gender'); ?>
													<?= $form->field($modelUserInfo, 'website')->textInput(); ?>
													<?= $form->field($modelUserInfo, 'facebook')->textInput(); ?>
													<?= $form->field($modelUserInfo, 'twitter')->textInput(); ?>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
													<?= Html::submitButton( Yii::t('app', 'Save change'), ['name'=> 'change_user_info', 'class' => 'btn btn-action', 'data-mr-loader'=>'show']) ?>
													<?php ActiveForm::end();  ?>
												</div>
											</div>
										</div>
									</div>
									<table class="table">
										<tr>
											<td>First name: </td>
											<td><?= $modelUser->name; ?></td>
										</tr>
										<tr>
											<td>Last name: </td>
											<td><?= $modelUser->surname; ?></td>
										</tr>
										<tr>
											<td>Date of birth: </td>
											<td><?= $modelUserInfo->birthday; ?></td>
										</tr>
										<tr>
											<td>Network: </td>
											<td><?= $modelUserInfo->university_id; ?></td>
										</tr>
										<tr>
											<td>Address: </td>
											<td><?= $modelUserInfo->address; ?></td>
										</tr>
										<tr>
											<td>Mobile: </td>
											<td><?= $modelUserInfo->mobile; ?></td>
										</tr>
										<tr>
											<td></td>
											<td></td>
										</tr>
										<?php if($modelUserInfo['id_user'] == \Yii::$app->user->id){ ?>
										<tr>
											<td>
												<button class="btn btn-action" data-toggle="modal" data-target="#myLocation">Change location <i class="fa fa-map-marker " aria-hidden="true"></i></button>
											</td>
										</tr>
										<?php } ?>
									</table>
									<div id="myLocation" class="modal big-modal padding-body fade" role="dialog">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">My location</h4>
												</div>
												<div class="modal-body">
													<input type="hidden" name="location_lat" id="location_lat" value="<?= $modelUserInfo->position_lat; ?>">
													<input type="hidden" name="location_lng" id="location_lng" value="<?= $modelUserInfo->position_lng; ?>">
													<button class="btn btn-action datermine_my_location">Determine my location</button>
													<label>Enter your location</label>
													<input type="text" name="" class="form-control enter_your_location" placeholder="enter your location">
													<div id="map" class="map"></div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
													<?= Html::submitButton('Save', ['class' => 'btn btn-action saveMyLocation']); ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-7">
								<!--<div id="map2" class="map2"></div>-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
