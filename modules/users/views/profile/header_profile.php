<?php
/**
 * Created by PhpStorm.
 * User: minace
 * Date: 2/29/16
 * Time: 10:50 PM
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use app\widgets\FollowbuttonWidget;

?>
	<div class="profile-header">
		<div class="prifile-cover-wrap">
			<?php $dataTarget = '';?>
			<?php $dataTargetAvatar = ''?>
			<?php if($modelUserInfo['id_user'] == \Yii::$app->user->id){ ?>
				<?php $dataTarget = '#modalImageChange';?>
				<?php $dataTargetAvatar = '#modalAvatarChange'?>
			<?php } ?>
			<?php if($modelUserInfo->avatar_wrapper != ''){
					$avatar_wrapper = '/users_images/'.$modelUserInfo->avatar_wrapper; 
				}else{
					$avatar_wrapper = '/default_wrapper.png'; 
				} ?>
				<div class="profile-cover" style="background-image:url('<?= Url::home(); ?>images<?= $avatar_wrapper; ?>');background-repeat:no-repeat;background-size:100%;">
					<?php if($modelUserInfo->id_user == \Yii::$app->user->id){ ?>
						<span class="edit-icon" data-toggle="modal" data-target="<?= $dataTarget; ?>" ></span>
					<?php } ?>
				</div>
				<div style='position:absolute;bottom:0px'>
					<?php /* if($modelUserInfo->avatar != ''){ ?>
						<img src='<?= Url::home(); ?>images/users_images/<?= $modelUserInfo->avatar; ?>'   style='width:100px;height:100px;border-radius:50%;float:left;'>
					<?php }else{ ?>
						<img src='<?= Url::home(); ?>images/default_avatar.png'  data-toggle="modal" data-target="<?= $dataTargetAvatar; ?>" style='width:100px;height:100px;border-radius:50%;float:left;'>
					<?php } */ ?>
				</div>
		</div>
		<div class="profile-name-line">
			<?php
				$class1 = ($this->context->getRoute() == 'users/profile/timeline')?'active':'';
				$class2 = ($this->context->getRoute() == 'users/profile/about')?'active':'';
				$class3 = ($this->context->getRoute() == 'users/profile/photos')?'active':'';
				$class4 = ($this->context->getRoute() == 'users/profile/friends')?'active':'';
				$class5 = ($this->context->getRoute() == 'users/profile/credits')?'active':'';
			?>
			<ul class="nav nav-pills">
				<li role="presentation" class="user-name <?= $class1; ?>">
					<?= $modelUser->name; ?>
					<?= $modelUser->surname; ?>
				</li>
				<?php /* ?>
				<li role="presentation" class="<?= $class1; ?>">
					<?php echo HTML::a(\Yii::t('app', 'Timeline'), 'timeline'); ?>
				</li>
				<li role="presentation" class="<?= $class2; ?>">
					<?php echo HTML::a(\Yii::t('app', 'About'), 'about'); ?>
				</li>
				<li role="presentation" class="<?= $class3; ?>">
					<?php echo HTML::a(\Yii::t('app', 'Photos'), 'photos'); ?>
				</li>
				<li role="presentation" class="<?= $class4; ?>">
					<?php echo HTML::a(\Yii::t('app', 'Friends'), 'friends'); ?>
				</li>
			  <?php if($modelUserInfo['id_user'] == \Yii::$app->user->id){ ?>
				<li role="presentation" class="<?= $class5; ?>">
					<?php echo HTML::a(\Yii::t('app', 'Credits'), 'credits'); ?>
				</li>
			  <?php } ?>
				<?php */ ?>
				<?php if (!\Yii::$app->user->isGuest): ?>
				<?php if($modelUserInfo['id_user'] != \Yii::$app->user->id){ ?>
					<li role="presentation" class="pull-right hidden">
						<button class="btn ifonline startVideo" data-sender-id="<?=\Yii::$app->user->id?>" data-recipient-id="<?=$modelUserInfo['id_user']?>"></button>
					</li>
					<li role="presentation" class="pull-right">
						<button class="btn startChat" data-sender-id="<?=\Yii::$app->user->id?>" data-recipient-id="<?=$modelUserInfo['id_user']?>"></button>
					</li>
					<li role="presentation" class="pull-right">
                                            <?= FollowbuttonWidget::widget(['friend_id' => $modelUserInfo['id_user']]); ?>
					</li>
					<?php }else{ ?>
						<li role="presentation" class="pull-right">
							
						</li>
					<?php } ?>
				<?php endif; ?>
			</ul>
		</div>
	</div>
