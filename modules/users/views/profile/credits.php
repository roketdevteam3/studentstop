<?php
/**
 * Created by PhpStorm.
 * User: minace
 * Date: 2/29/16
 * Time: 10:50 PM
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use app\assets\CreditsprofileAsset;
CreditsprofileAsset::register($this);

?>

	<div class="row">
		<?php echo $this->render('right_menu',['modelUser' => $modelUser, 'modelUserInfo' => $modelUserInfo]); ?>
		<div class="profile-content">
			<div class="container-fluid background-block">
				<div class="credits-container">
					<div class="users-form">
						<span style="display:none;" class="CreditsForMoney"><?= $modelSettings->value; ?></span>
						<div class="credits-header">
							<h4>Total Credits - <?= $countCredits; ?> credits</h4>
							<button type="button" class="btn btn-action btn-pay-credits" data-toggle="modal" data-target="#myModal">Buy Credits</button>
						</div>
						<div id="myModal" class="modal padding-body pay-credits fade" role="dialog">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">x</button>
										<h4 class="modal-title">Pay credits</h4>
									</div>
									<div class="modal-body text-center">
										<?php $form = ActiveForm::begin(['action' => ['pay_credits'],])?>
										<input type="number" name="credit_pay_count" value="0">
										<br />
										<span class="credits-sum">
											<span class="money_for_credits">0</span><span>$</span>
										</span>
<!--										<button class="btn btn-action pay_credits">pay</button>-->
										<button class="btn btn-action" type="submit">pay</button>
										<?php ActiveForm::end();?>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
						<table class="table credits-table">
							<thead>
								<tr>
									<td>
										Service
									</td>
									<td>
										Recepient
									</td>
									<td>
										Recepient type
									</td>
									<td>
										Credit
									</td>
								</tr>
							</thead>
							<?php foreach($modelCreditsrequest as $credits){ ?>
								<tr>
									<td>
										<?= $credits['modules']; ?>
									</td>
									<td>
										<?= $credits['recepient_id']; ?>
									</td>
									<td>
										<?= $credits['recepient_type']; ?>
									</td>
									<td>
										<?php if($credits['request_type'] == 'costs'){ ?>
											-<?= $credits['credits_count']; ?>
										<?php }else{ ?>
											+<?= $credits['credits_count']; ?>
										<?php } ?>
									</td>
								</tr>
							<?php } ?>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	
