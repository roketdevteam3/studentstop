<?php

	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\widgets\ActiveForm;
	use yii\bootstrap\Alert;
        use app\widgets\rating\UserratingWidget;
        use app\assets\ImagesheaderAsset;
        ImagesheaderAsset::register($this);
?>
        <?php if($modelUserInfo->id_user == \Yii::$app->user->id){ 
                $user_role = 'My ';
        }else{
                $user_role = 'User ';
        }
?>

	<div style="display:none">
		<div class="table table-striped previewTemplateQ" >
			<div id="template" class="file-row">
				<!-- This is used as the file preview template -->
				<div class="new-avatar">
					<span class="preview">
						<img data-dz-thumbnail />
					</span>
				</div>
				<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
					<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
				</div>
				<span class="error text-danger" data-dz-errormessage></span>
				<div class="eventButton" style="display:none;">
					<?php $form = ActiveForm::begin([
						'id' => 'formSaveAavatar',
						'action' => '/saveavatar'
						]); ?>
						<input type='hidden' name='type' value="avatar">
						<input type='hidden' name='userAvatar' id='userAvatar'>
						<?= Html::submitButton('Save', ['class' => 'btn btn-save pull-right', 'name' => 'userAvata']) ?>
					<?php ActiveForm::end();  ?>
					<a data-dz-remove class="btn btn-danger delete">
						<i class="glyphicon glyphicon-trash"></i>
						<span>Delete</span>
					</a>
				</div>
			</div>
		</div>
	</div>

	<?php if($modelUserInfo->id_user == \Yii::$app->user->id){ ?>
		<div class="modal fade" id="modalAvatarChange" role="dialog">
			<div class="modal-dialog" >
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">x</button>
					</div>
					<div class="modal-body">
						<div class="avatar-change">
							<?php if($modelUserInfo->avatar != ''){ ?>
								<img class="img-responsive" src='<?= Url::home(); ?>images/users_images/<?= $modelUserInfo->avatar; ?>'>
							<?php }else{ ?>
								<img class="img-responsive" src='<?= Url::home(); ?>images/default_avatar.jpg'>
							<?php } ?>
						</div>
						<span class="btn btn-action newAvatar">
							<span>new Avatar</span>
						</span>
						<div class='user_header_avatar'>
						</div>
						<?php /* $form = ActiveForm::begin([
							'id' => 'formSaveAavatarWrapper',
							'action' => '/saveavatar'
						]); ?>
						<input type='hidden' name='type' value="avatar">
						<input type='hidden' name='userAvatar' id='userAvatar' value="<?= $modelUserInfo->avatar; ?>">
						<?= Html::submitButton('Save', ['class' => 'btn btn-primary pull-right', 'name' => 'userAvatar']) ?>
						
						<?php ActiveForm::end();  */ ?>
						<div style='clear:both;'></div>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="modalImageChange" role="dialog">
			<div class="modal-dialog" style="width:90%;">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
						<div class="user_header_photo" action="../../users/profile/savedropedfile"  style="background-image:url('/images/users_images/<?= $modelUserInfo->avatar_wrapper; ?>');background-repeat:no-repeat;background-size:100%;height:300px;">
						</div>
						<?php $form = ActiveForm::begin([
							'id' => 'formSaveAavatarWrapper',
							'action' => '/saveavatar'
						]); ?>
						<input type='hidden' name='type' value="wrapper">
						<input type='hidden' name='userAvatarWrapper' id='userAvatarWrapper' value="<?= $modelUserInfo->avatar_wrapper; ?>">
						<?= Html::submitButton('Save', ['class' => 'btn btn-action pull-right', 'name' => 'userAvataW']) ?>
						<?php ActiveForm::end();  ?>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>

	<?php
		$class1 = ($this->context->getRoute() == 'users/profile/timeline')?'active':'';
		$class2 = ($this->context->getRoute() == 'users/profile/about')?'active':'';
		$class3 = ($this->context->getRoute() == 'users/profile/photos')?'active':'';
		$class4 = ($this->context->getRoute() == 'users/profile/friends')?'active':'';
		$class5 = ($this->context->getRoute() == 'users/profile/credits')?'active':'';
	?>
	<div id="profile-right-panel">
		<div class="profile-avatar-wrap">
			<?php $dataTarget = '';?>
			<?php $dataTargetAvatar = ''?>
			<?php $my = 'no'?>
			<?php if($modelUserInfo['id_user'] == \Yii::$app->user->id){ ?>
                                <?php $my = 'yes'?>
				<?php $dataTarget = '#modalImageChange';?>
				<?php $dataTarget = '#modalImageChange';?>
				<?php $dataTargetAvatar = '#modalAvatarChange'?>
			<?php } ?>
			<?php if($modelUserInfo->avatar != ''){ ?>
				<img class="img-responsive" src='<?= Url::home(); ?>images/users_images/<?= $modelUserInfo->avatar; ?>'>
			<?php }else{ ?>
				<img class="img-responsive" src='<?= Url::home(); ?>images/default_avatar.jpg'>
			<?php } ?>
			<?php if($modelUserInfo->id_user == \Yii::$app->user->id){ ?>
				<span class="edit-icon" data-toggle="modal" data-target="<?= $dataTargetAvatar; ?>" ></span>
			<?php } ?>
		</div>
        
		<div class="profile-rating">
            <div id="userInfoId" style="display:none;"><?= $modelUserInfo['id_user']; ?></div>
			<span style="font-size: 13px;">Rank:</span>
			<span class="rating-count ratingCountUser"></span>
			<?= UserratingWidget::widget(['user_id' => $modelUserInfo['id_user'] ])?>
		</div>

        <input type="hidden" name="location_lat" id="location_lat" value="<?= $modelUserInfo['position_lat']; ?>">
        <input type="hidden" name="location_lng" id="location_lng" value="<?= $modelUserInfo['position_lng']; ?>">
            
        <nav class="profile-menu">
			<li role="presentation" class="<?= $class1; ?>">
				<?php echo HTML::a(\Yii::t('app', 'Dashboard'), 'timeline'); ?>
			</li>
			<li role="presentation" class="<?= $class2; ?>">
				<?php echo HTML::a(\Yii::t('app', 'About'), '',['class' => 'buttonAboutRightPages','data-user_id' => $modelUserInfo['id_user'], 'data-my' => $my]); ?>
			</li>
			<li role="presentation" class="<?= $class3; ?>">
				<?php echo HTML::a('Photos & Videos', 'photos'); ?>
			</li>
			<li role="presentation" class="<?= $class4; ?>">
				<?php echo HTML::a(\Yii::t('app', 'Friends'), 'friends'); ?>
			</li>
			<li role="presentation" class="<?= $class3; ?>">
				<?php echo HTML::a(\Yii::t('app', 'Marketplace'), 'market'); ?>
			</li>
			<li role="presentation" class="<?= $class3; ?>">
				<?php echo HTML::a('Courses & Classes', 'courses'); ?>
			</li>
            <li role="presentation" class="<?= $class3; ?>">
                    <?php echo HTML::a('Ranking and Rating', 'rating'); ?>
            </li>
			<?php if($modelUserInfo['id_user'] == \Yii::$app->user->id){ ?>
				<li role="presentation" class="<?= $class5; ?>">
					<?php echo HTML::a(\Yii::t('app', $user_role.' docs'), 'docs'); ?>
				</li>
			<?php } ?>
			<?php if($modelUserInfo['id_user'] == \Yii::$app->user->id){ ?>
				<li role="presentation" class="<?= $class5; ?>">
					<?php echo HTML::a(\Yii::t('app', 'Credits'), 'credits'); ?>
				</li>
			<?php } ?>
		</nav>
		<?php //= QuestioncategoryWidget::widget(); ?>
	</div>