<?php
namespace app\modules\users\controllers;

use app\models\user\User;
use app\models\user\UserFriendRequest;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 04.03.16
 * Time: 16:55
 * Email: mackraiscms@gmail.com
 * Site: http://mackrais.tk
 */



class FriendsController extends Controller{


    public function actionIndex(){

        $dataProviderRequest = new ActiveDataProvider(array(
            'query'  =>User::find()
                ->joinWith('friendsRequest')
                ->where(
                    [
                        '{{%user_friend_request}}.whom_send' => \Yii::$app->user->getId(),
                        '{{%user_friend_request}}.status' => UserFriendRequest::STATUS_WAIT
                    ]
                )
                ->andWhere('{{%user_friend_request}}.who_send <> '.\Yii::$app->user->getId()),
            'pagination' => array(
                'pageSize' => 10,
                'pageParam' => 'page_friends_request'
            ),
        ));

        $dataProviderFriends = new ActiveDataProvider(array(
            'query'  =>User::find()
                ->joinWith('friends')
                ->where(
                    [
                        '{{%user_friend}}.id_user' => \Yii::$app->user->getId(),
                    ]
                )
                ->andWhere('{{%user_friend}}.id_friend <> '.\Yii::$app->user->getId()),
            'pagination' => array(
                'pageSize' => 10,
                'pageParam' => 'page_friends'
            ),
        ));

        return $this->render('index',['dataProviderRequest'=>$dataProviderRequest, 'dataProviderFriends'=>$dataProviderFriends]);
    }


    public function actionRequests(){
        return $this->render('requests');
    }

}


