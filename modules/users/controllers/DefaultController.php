<?php

namespace app\modules\users\controllers;

use app\models\ClassUser;
use app\models\Follower;
use app\models\user\UserFriend;
use app\models\user\UserFriendRequest;
use app\models\user\UserFriendType;
use app\modules\university\models\Classes;
use app\modules\university\models\Major;
use app\modules\university\models\Course;
use app\modules\users\models\User;
use app\modules\users\models\Users;
use app\modules\users\models\UserInfo;
use app\modules\users\models\UserRole;
use app\modules\university\models\University;
use app\modules\users\models\UsersSearch;
use Yii;
use app\modules\users\models\LoginForm;
use app\modules\users\models\SignupForm;
use app\models\Notification;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use app\components\MrCmsController;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Ratingrequest;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use app\components\AuthHandler;
use app\models\MainMenuItem;
use yii\db\Query;
error_reporting( E_ERROR );

class DefaultController extends Controller
{

    public function behaviors()
    {
        return [

            // ...
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            // ...

        ];
    }

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                //'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'successCallback'],
            ],
        ];
    }

    /**
     * @param $client
     */
    
    
    public function successCallback($client)
    {
        //var_dump($client->getUserAttributes());exit;
        (new AuthHandler($client))->handle();
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionHomepage()
    {
        $user_info = UserInfo::find()->where(['id_user' => Yii::$app->user->id])->one();
        if($user_info){
            $university = University::findOne($user_info->university_id);
            if($university == null){
                if($user_info->university_id){
                    return $this->redirect(Url::to("/university/view/".$university->slug,true));
                }
            }else{
                return $this->redirect(Url::to("/users/choiceuniversity",true));
            }
        }
                
        return $this->redirect();
        $main_menu_logged = MainMenuItem::find()
            ->status(MainMenuItem::STATUS_ON)
            ->andWhere(['logged' => MainMenuItem::LOGGED])
            ->orderBy(['order_num' => SORT_DESC])
            ->all();
        
        return $this->render('homepage', [
            'main_menu_logged' => $main_menu_logged
        ]);
    }
    
    public function actionIndex()
    {
        if(!\Yii::$app->user->isGuest){
            $arrayUniversity = ArrayHelper::map(University::find()->all(), 'id', 'name');
            $arrayMajors = ArrayHelper::map(Major::find()->all(), 'id', 'name');
            $arrayCourse = ArrayHelper::map(Course::find()->all(), 'id', 'name');

            $arrayUserRole = ArrayHelper::map(UserRole::find()->where(['status' => '1'])->all(), 'id', 'name');
            return $this->render('index', [
                'arrayUniversity' => $arrayUniversity,
                'arrayUserRole' => $arrayUserRole,
                'arrayMajors' => $arrayMajors,
                'arrayCourse' => $arrayCourse,
            ]);
        }else{
            return $this->redirect(Url::to("/users/login",true));
        }
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionUserupdateprofile()
    {
        $result = [];
        $modelUser = User::find()->where(['id' => 'user_id'])->one();
        $modelUserInfo = UserInfo::find()->where(['id_user' => ''])->one();
        
        //
        //'update_user_info' => ['birthday', 'mobile', 'address', 'website', 'facebook','twitter', 'gender' ],
        //'update' => ['name', 'surname', 'email'],
        //
        
        //first_name:first_name, surname:surname, birth:birth, address:address, mobile:mobile, about:about

        echo json_encode($result);
    }
    
    public function actionChoiceuniversity()
    {
        if($_POST){
            $modelUserInfo = UserInfo::find()->where(['id_user' => \Yii::$app->user->id])->one();
            if($modelUserInfo == null){
                $modelUserInfo = new UserInfo();
                $modelUserInfo->id_user = \Yii::$app->user->id;
            }
            $modelUserInfo->scenario = 'register';
            $modelUserInfo->network = $_POST['university'];
            $modelUserInfo->academic_status = $_POST['userrole'];
            $modelUserInfo->university_id = $_POST['university'];
            
            if($modelUserInfo->save()){
                $university = University::findOne($modelUserInfo->university_id);
                return $this->redirect(Url::to("/university/view/".$university->slug,true));
            }else{
            }
        }
        $universityArray = ArrayHelper::map(University::find()->all(), 'id', 'name');
        $userRoleArray = ArrayHelper::map(UserRole::find()->where(['NOT IN', 'name', ['admin']])->all(), 'id', 'name');
        return $this->render('choiceuniversity', ['universityArray' => $universityArray, 'userRoleArray' => $userRoleArray]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @throws ForbiddenHttpException
     * @return mixed
     */
    public function actionCreate()
    {

        if (!\Yii::$app->user->can('create') || \Yii::$app->user->getIsGuest()) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @return mixed
     */
    public function actionUpdate($id)
    {

        if ( \Yii::$app->user->getIsGuest()) {
            if(!\Yii::$app->user->group != 'admin' &&  !\Yii::$app->user->group  !== $id )
            throw new ForbiddenHttpException('Access denied'.\Yii::$app->user->id);
        }

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Users model after login.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @return mixed
     */
    public function actionUpdateAfterLogin($id)
    {
        if (!\Yii::$app->user->can('updateOwnProfile', ['profileId' => $id])  || \Yii::$app->user->getIsGuest()) {
            throw new ForbiddenHttpException('Access denied');
        }

        $model = $this->findModel($id);
        $form_name = $model->formName();
        $data = Yii::$app->request->post();

        if ($model->load($data) && $model->save()){
            $data[$form_name]['auth_key'] =  Yii::$app->security->generateRandomString();
            $data[$form_name]['token'] = Yii::$app->security->generateRandomString() . '_' . time();
            $data[$form_name]['updated_at'] = date('Y-m-d H:i:s');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update_after_login', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \Exception
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!\Yii::$app->user->can('create') || \Yii::$app->user->getIsGuest()) {
            throw new ForbiddenHttpException('Access denied');
        }

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        $this->layout = '//landing';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $model_sign_up = new SignupForm();
        $modelUserInfo = new UserInfo();
        $model_university = new University();
        $model_user_role = new UserRole();
        
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $user_info = UserInfo::find()->where(['id_user' => Yii::$app->user->id])->one();
            if($user_info){
                $university = University::findOne($user_info->university_id);
                //var_dump($user_info->academic_status);exit;
                if($user_info->academic_status == 8){
                    return $this->redirect(Url::to(Url::home()."create_business",true));
                }else{
                    return $this->redirect(Url::to("/university/view/".$university->slug,true));
                }
            }
            
            return $this->goBack();
        }else{
            return $this->render('login',[
                'model' => $model,
                'model_sign_up' => $model_sign_up,
                'model_info' => $modelUserInfo,
                'model_university' => $model_university,
                'model_user_role' => $model_user_role,
            ]);
        }
    }

    public function actionJoin()
    {

        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model_sign_up = new SignupForm();

        return $this->render('join', [
            'model_sign_up' => $model_sign_up,
        ]);

    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionConfirmSignup()
    {

        $model = new SignupForm();
        $model_info = new UserInfo();
        $model_university = new University();
        $model_user_role = new UserRole();

        if ($model->load(Yii::$app->request->post())) {
            $model_university = new University();
            $model_user_role = new UserRole();
            
            return $this->render('confirm_registration', [
                'model' => $model,
                'model_info' => $model_info,
                'model_university' => $model_university,
                'model_user_role' => $model_user_role
            ]);
        }
        return $this->render('confirm_registration', [
            'model' => $model,
            'model_info' => $model_info,
            'model_university' => $model_university,
            'model_user_role' => $model_user_role
        ]);
    }

    public function actionSignup()
    {
        if(!\Yii::$app->user->isGuest){
            //var_dump(Yii::$app->user->identity->getUsertUniversity());
                $user_info = UserInfo::find()->where(['id_user' => (int)\Yii::$app->user->id])->one();
                 if($user_info){
                    $university = University::findOne($user_info->university_id);
                    if($university != null){
                        return $this->redirect(Url::to("/university/view/".$university->slug,true));
                    }else{
                        return $this->redirect(Url::to("/users/choiceuniversity",true));
                    }
                }
        }
        
        $model = new SignupForm();
        $model_info = new UserInfo();
        $model_info->scenario = 'register';
        $model_university = new University();
        $model_user_role = new UserRole();

        $data = Yii::$app->request->post();
        if ($model->load($data) && $model_info->load($data)) {
//            $model_info->id_user = /Yii::$app->user->id;
            $model_info->university_id = $model_info->network;
            $isValid = $model->validate();
            $isValid = $model_info->validate() && $isValid;
            if ($isValid) {
                $user = $model->signup();
                $model_info->id_user = $user->id;
                $model_info->save();

                $modelNotification = new Notification();
                $modelNotification->scenario = 'add_notification';
                $modelNotification->module = 'university';
                $modelNotification->from_object_id = $model_info->university_id;
                $modelNotification->to_object_id = $user->id;
                $modelNotification->university_id = $model_info->university_id;
                $modelNotification->notification_type = 'register';
                $modelNotification->save();
                
                $model_university= University::findOne($model_info->network);

                //added default role User
                $authManager = \Yii::$app->authManager;
                $role = $authManager->getRole('user');
                $authManager->assign($role,$user->id);

                if (Yii::$app->getUser()->login($user)) {

                    // Insert follower
                    $follower = new Follower();
                    $follower->id_object = $model_info->network;
                    $follower->id_user= $model_info->id_user;
                    $follower->type_object = 'university';
                    $follower->save(false);
                    if($model_info->academic_status == 8){
                        return $this->redirect(Url::home()."dashboard/job");
                    }else{
                        return $this->redirect(["/university/view/".$model_university->slug]);
                    }

                }
            }
            
        }
        return $this->render('confirm_registration', [
            'model' => $model,
            'model_info' => $model_info,
            'model_university' => $model_university,
            'model_user_role' => $model_user_role
        ]);
    }


    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
        public function actionAjaxSetFriend($id){

        $model = new UserFriendRequest();
        $data = Yii::$app->request->post();

        if(!Yii::$app->request->isAjax){
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        $check = $model->checkFriend($id);

        if($check){
            if ($check->delete()) {
                return [
                    'status' => 'ok',
                    'msg'=>\Yii::t('app','You have successfully removed request be friend!'),
                    'title' =>\Yii::t('app','Add to friend'),
                ];
            } else {
                return ['status'=>'error','msg'=>'Server error!'];
            }
        }else{
            $model->who_send = \Yii::$app->user->getId();
            $model->whom_send= (int)$id;
            $model->status= UserFriendRequest::STATUS_WAIT;

            if ($model->save()) {
                return [
                    'status' => 'ok',
                    'msg'=>\Yii::t('app','You are successfully  send request be friend!'),
                    'title' => UserFriendRequest::getStatus($model->status),
                ];
            } else {
                return ['status'=>'error','msg'=>'Server error!'];
            }
        }
    }

    /** TODO add checker type friends by model UserTypeFriend
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionAjaxAcceptFriend($id){

        $model = new UserFriendRequest();
        $model_user = new UserFriend();
        $model_friend = new UserFriend();
        $data = Yii::$app->request->post();

        if(!Yii::$app->request->isAjax){
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        $check = $model->checkFriend(\Yii::$app->user->getId(), $id);



        if($check){
            $check->status = UserFriendRequest::STATUS_APPROVED;

            $model_user->id_user = $check->who_send;
            $model_user->id_friend = $check->whom_send;

            $model_friend->id_user = $check->whom_send;
            $model_friend->id_friend = $check->who_send;



            $type = UserFriend::TYPE_FRIEND_OTHER;

            $user_class  =  ClassUser::find()->where(['id_user'=>$model_user->id_user])->asArray()->all();
            $user_class = ArrayHelper::map($user_class,'id_class','id_class');
            $friend_class =  ClassUser::find()
                ->where(['id_user'=>$model_user->id_friend])
                ->andFilterWhere(['IN','id_class',$user_class]);

            if(!empty($user_class) && !empty($friend_class) ){
                $type = UserFriend::TYPE_FRIEND_CLASS;
            }else{
                $model_class = Classes::find()
                    ->where(['professor'=>$model_user->id_friend])
                    ->andFilterWhere(['IN','id',$user_class])->one();

                if(!empty($model_class)){
                    $type = UserFriend::TYPE_FRIEND_PROFESSOR;
                }else{

                    $model_univer = Follower::find()
                        ->where(
                            [
                                'type_object'=>'university',
                                'id_user' => $model_user->id_user,
                            ]
                        )->asArray()->all();

                    $model_univer = ArrayHelper::map($model_univer,'id','id');

                    $model_univer_univ = Follower::find()
                        ->where(
                            [
                                'type_object'=>'university',
                                'id_user' => $model_user->id_friend,
                            ]
                        )
                        ->andFilterWhere(['IN','id',$model_univer])->one();
                    if(!empty($model_univer_univ)){
                        $type = UserFriend::TYPE_FRIEND_UNIVERSITY;
                    }
                }

            }
            $model_user->type = $type;
            $model_friend->type = $type;

            if ($check->save() && $model_user->save() && $model_friend->save()) {
                return [
                    'status' => 'ok',
                    'msg'=>\Yii::t('app','You have successfully accept friend!'),
                    'title' =>\Yii::t('app','Add to friend'),
                ];
            } else {
                $errors = ArrayHelper::merge($model_user->getErrors(),$model_friend->getErrors());
                return ['status'=>'error','msg'=>'Server error!', 'errors'=>$errors];
            }
        }
    }

    public function actionGetcourse()
    {
        $university_id = $_POST['university_id'];
        $majorsModel = Major::find()->where(['university_id' => $university_id])->all();
        $major_id = [];
        foreach($majorsModel as $major){
            $major_id[] = $major->id;
        }
        $modelCourse = ArrayHelper::map(Course::find()->where(['id_major' => $major_id])->all(), 'id', 'name');
        echo json_encode($modelCourse);
    }
    
    public function actionGetuserinfo()
    {
        $user_id ='';
        if(isset($_POST['user_id']) && ($_POST['user_id'] != '') && ($_POST['user_id']!= 0)){
            $user_id = $_POST['user_id'];
        }else{
            $user_id = \Yii::$app->user->id;
        }
        $queryUser = new Query();
        $queryUser->select(['*', 'id' => '{{%user}}.id'])
                            ->from('{{%user}}')
                            ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%user}}.id')
                    ->where(['{{%user}}.id' => $user_id]);

        $command = $queryUser->createCommand();
        $modelUser = $command->queryOne();
        
        echo json_encode($modelUser);
    }
    
    public function actionMessagemapdata()
    {
        $user_id = $_POST['user_id'];
        $result = [];
        
        $modelUser = User::find()->where(['id' => $user_id])->one();
        $modeUserInfo = UserInfo::find()->where(['id_user' => $user_id])->one();
        $modeUniversity = University::find()->where(['id' => $modeUserInfo->university_id])->asArray()->one();
        $result['university_info'] = $modeUniversity;
        if($modeUniversity != null){
            $queryFriend = new Query();
            $queryFriend->select(['*', 'id' => '{{%user}}.id', 'position_lat' => '{{%user_info}}.position_lat', 'position_lng' => '{{%user_info}}.position_lng'])
                                ->from('{{%user}}')
                                ->join('LEFT JOIN',
                                            '{{%user_info}}',
                                            '{{%user_info}}.id_user = {{%user}}.id')
                        ->where(['{{%user_info}}.university_id' => $modeUniversity['id']]);

            $command = $queryFriend->createCommand();
            $modelUniversityUser = $command->queryAll();
            $result['university_users'] = $modelUniversityUser;
        }else{
            $result['university_users'] = null;
        }
        
        echo json_encode($result);
    }
    
    public function actionGetuniversitiesmembers(){
        
        $major_id = $_POST['major_id'];
        $course_id = $_POST['course_id'];
        $rating_id = $_POST['rating_id'];
        $search_key = $_POST['search_key'];
        
        
        $states = \app\models\States::find()->where(['country_id' => '231'])->asArray()->all();
        $result = [];
        $univetsityA = [];
        //state foreach begin
        foreach($states as $state){
            $queryUniversity = new Query();
            $queryUniversity->select(['*', 'id' => '{{%university}}.id', 'university_name' => '{{%university}}.name', 'username' => '{{%user}}.name'])
                    ->from('{{%university}}')
                    ->join('LEFT JOIN',
                                '{{%user_info}}',
                                '{{%user_info}}.id_user = {{%university}}.id_owner')
                    ->join('LEFT JOIN',
                                '{{%user}}',
                                '{{%user}}.id = {{%university}}.id_owner')
                    ->where(['LIKE', '{{%university}}.name', $search_key])->andWhere(['{{%university}}.region_id' => $state['id']])
                    ->orWhere(['LIKE', '{{%university}}.history', $search_key])->andWhere(['{{%university}}.region_id' => $state['id']])
                    ->orWhere(['LIKE', '{{%university}}.location', $search_key])->andWhere(['{{%university}}.region_id' => $state['id']]);

                if($major_id != ''){
                    $majorModel = Major::find()->where(['id' => $major_id])->one();
                    $queryUniversity->andWhere(['{{%university}}.id' => $majorModel->university_id]);
                }
                if($course_id != ''){
                    $courseModel = Course::find()->where(['id' => $course_id])->one();
                    $majorModel = Major::find()->where(['id' => $courseModel->id_major])->one();
                    $queryUniversity->andWhere(['{{%university}}.id' => $majorModel->university_id]);
                }
            $command = $queryUniversity->createCommand();
            $modelUniversity = $command->queryAll();
            $univetsityA = array_merge($univetsityA, $modelUniversity);
            
            $friendCount = 0;
            $univeristyArray = [];
            foreach($modelUniversity as $key =>  $university){
                $univeristyArray[$university['id']]['university_id'] = $university['id'];
                $univeristyArray[$university['id']]['university_name'] = $university['university_name'];
                $univeristyArray[$university['id']]['university_avatar'] = ($uniUniver = University::find()->where(['id' => $university['id']])->one())? $uniUniver->getImg() : '';
                $univeristyArray[$university['id']]['email'] = $university['email'];
                $univeristyArray[$university['id']]['website'] = $university['website'];
                $univeristyArray[$university['id']]['telefone'] = $university['telefone'];
                $univeristyArray[$university['id']]['year_founded'] = $university['year_founded'];
                $univeristyArray[$university['id']]['history'] =  substr(strip_tags($university['history']),0, 250);
                $univeristyArray[$university['id']]['professor_name'] = $university['username'].' '.$university['surname'];
                $univeristyArray[$university['id']]['professor_avatar'] = $university['avatar'];
                $Location = \app\models\States::find()->where(['id' => $university['region_id']])->asArray()->one();
                if($Location){
                    $univeristyArray[$university['id']]['location'] = $Location['name'].' '.$university['location'];
                }else{
                    $univeristyArray[$university['id']]['location'] = '';
                }
    //            $univeristyArray[$university['id']]['users_count'] = UserInfo::find()->where(['university_id' => $university['id']])->count();
                $queryfriend = new Query();
                $queryfriend->select(['*', 'id' => '{{%user_friend}}.id_user'])
                        ->from('{{%user_friend}}')
                        ->join('LEFT JOIN',
                                    '{{%user_info}}',
                                    '{{%user_info}}.id_user = {{%user_friend}}.id_friend')
                        ->where(['{{%user_info}}.university_id' => $university['id'], '{{%user_friend}}.id_user' => \Yii::$app->user->id ]);
                $count =    $queryfriend->count();
                $univeristyArray[$university['id']]['users_count'] = $count;
                $friendCount = $friendCount + (int)$count;
            }
            if(count($univeristyArray) == 0){
                $result[$state['id']]['university'] = 'null';
            }else{
                $result[$state['id']]['university'] = $univeristyArray;                
            }
            
            $result[$state['id']]['state_name'] = $state['name'];
            $result[$state['id']]['state_lat'] = $state['lat'];
            $result[$state['id']]['state_lng'] = $state['lng'];
            $result[$state['id']]['state_friend_count'] = $friendCount;
        }
        //state foreach end
        $result['univetsities'] = $univetsityA;
        echo json_encode($result);
    }
    
    public function actionGetfilternameusers(){
        $value = $_POST['value'];
        $pieces = explode(" ", $value);
        $modelUsers = [];
        if(!$pieces[3]){
            $queryUser = new Query();
            $queryUser->select(['*', 'id' => '{{%user}}.id', 'position_lat' => '{{%user_info}}.position_lat', 'position_lng' => '{{%user_info}}.position_lng'])
                                ->from('{{%user}}')
                                ->join('LEFT JOIN',
                                            '{{%user_info}}',
                                            '{{%user_info}}.id_user = {{%user}}.id');
            if($pieces[1]){
                $queryUser->where(['LIKE', '{{%user}}.name', $pieces[0]]);
                $queryUser->andWhere(['LIKE', '{{%user}}.surname', $pieces[1]]);

                $queryUser->orWhere(['LIKE', '{{%user}}.name', $pieces[1]]);
                $queryUser->andWhere(['LIKE', '{{%user}}.surname', $pieces[0]]);
            }elseif($pieces[0]){
                $queryUser->where(['LIKE', '{{%user}}.name', $pieces[0]]);
                $queryUser->orWhere(['LIKE', '{{%user}}.surname', $pieces[0]]);
            }
            
            $command = $queryUser->createCommand();
            $modelUsers = $command->queryAll();
        }
        
        echo json_encode($modelUsers);
    }
    
    public function actionGetanoutheruserinformation(){
        $user_id = $_POST['user_id'];
        $university_id = $_POST['university_id'];
        $modelUniversity = University::find()->where(['id' => $university_id])->one();
        $userArray = [];
        $userInfoArray['university_name'] = $modelUniversity->name;
            
            $modelRatingUser = Ratingrequest::find()->where(['to_object_id' => $user_id])->all();
            $countUser = 0;
            $countStar = 0;
            foreach($modelRatingUser as $request){
                $countUser++;
                $countStar = $countStar + $request['rating_count'];
            }
            $result = '';
            if($countUser == 0){
                $countStarResult = 0;
            }else{
                $countStarResult = round($countStar/$countUser,1);
            }
        $userInfoArray['star_count'] = $countStarResult;
        $follow_status = '';
        if($user_id != \Yii::$app->user->id){
            $modelFriend = UserFriend::find()->where(['id_user' => \Yii::$app->user->id, 'id_friend' => $user_id])->one();
            if($modelFriend == null){
                $modelFriendConfirm = UserFriend::find()->where(['id_user' => $user_id, 'id_friend' => \Yii::$app->user->id])->one();
                if($modelFriendConfirm == null){
                    $follow_status = '<button class="btn btn-info followFriend" data-friend_id="'.$user_id.'" data-my_id="'.\Yii::$app->user->id.'">Follow</button>';
                }else{
                    $follow_status = '<button class="btn btn-info confirmFriend" data-friend_id="'.$user_id.'" data-my_id="'.\Yii::$app->user->id.'">Confirm</button>';
                }
            }else{
                $follow_status = '<button class="btn btn-info unfollowFriend" data-friend_id="'.$user_id.'" data-my_id="'.\Yii::$app->user->id.'">Unfollow</button>';
            }
            
        }
        
        $userInfoArray['follow_status'] = $follow_status;
        $userInfoArray['friend_count'] = UserFriend::find()->where(['id_user' => $user_id])->count();
        $userInfoArray['class_count'] = Follower::find()->where(['id_user' => $user_id, 'type_object' => 'class'])->count();
        
        echo json_encode($userInfoArray);
    }
    
    public function actionGetfrienduniversity(){
        $university_id = $_POST['university_id'];
        $modelUniversity = University::find()->where(['id' => $university_id])->one();
        $queryfriend = new Query();
        $queryfriend->select(['*', 'id' => '{{%user_friend}}.id_friend'])
                ->from('{{%user_friend}}')
                ->join('LEFT JOIN',
                            '{{%user_info}}',
                            '{{%user_info}}.id_user = {{%user_friend}}.id_friend')
                ->where(['{{%user_info}}.university_id' => $university_id, '{{%user_friend}}.id_user' => \Yii::$app->user->id ]);
        
        $command = $queryfriend->createCommand();
        $modelFriend = $command->queryAll();

        foreach($modelFriend as $friend){
            $friend_id = $friend['id_friend'];
            $userInfoArray[$friend_id]['user_info'] = UserInfo::find()->where(['id_user' => $friend_id])->asArray()->one();
            $userInfoArray[$friend_id]['user'] = User::find()->where(['id' => $friend_id])->asArray()->one();
            
            $userInfoArray[$friend_id]['university_name'] = $modelUniversity->name;

                $modelRatingUser = Ratingrequest::find()->where(['to_object_id' => $friend_id])->all();
                $countUser = 0;
                $countStar = 0;
                foreach($modelRatingUser as $request){
                    $countUser++;
                    $countStar = $countStar + $request['rating_count'];
                }
                $result = '';
                if($countUser == 0){
                    $countStarResult = 0;
                }else{
                    $countStarResult = round($countStar/$countUser,1);
                }
            $userRole = UserRole::find()->where(['id' => $userInfoArray[$friend_id]['user_info']['academic_status']])->asArray()->one();
            $userInfoArray[$friend_id]['user_role'] = $userRole['name'];
            $userInfoArray[$friend_id]['star_count'] = $countStarResult;
            $userInfoArray[$friend_id]['friend_count'] = UserFriend::find(['id_user' => $friend_id])->where()->count();
            $userInfoArray[$friend_id]['class_count'] = Follower::find(['id_user' => $friend_id, 'type_object' => 'class'])->where()->count();            
        }
        
        echo json_encode($userInfoArray);
    }
    
}
