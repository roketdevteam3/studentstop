<?php

namespace app\modules\users\controllers;
use app\commands\Paypal;
use app\models\OutputManey;
use app\models\OutputManeySearch;
use yii\base\UserException;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use app\models\user\User;
use app\models\user\UserInfo;
use app\models\Creditsrequest;
use app\models\Usercredits;
use app\models\user\UserFriend;
use yii\helpers\ArrayHelper;
use app\models\user\UserFriendRequest;
use Yii;
use app\models\user\Event;
use app\models\user\UserEdit;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use app\models\Settingadmin;
use app\models\Ratingrequest;
use app\models\Notificationusers;
use app\modules\users\models\UserImages;
use app\modules\users\models\UserNews;
use app\modules\users\models\UserEvents;
use app\modules\university\models\University;
use app\modules\users\models\Photocomment;
use app\modules\users\models\Photolike;
use yii\web\HttpException;

class NotificationController extends \yii\web\Controller
{

    public function actionGetnotification()
    {
        $result = [];
        if($_POST['user_id'] != ''){
            $user_id = $_POST['user_id'];
            $limit = $_POST['limit'];
            $offset = $_POST['offset'];
            
            $nfCountUnread = Notificationusers::find()->where(['user_id' => $user_id, 'status' => '0'])->count();
            $query = new Query;
            $query->select(['*', 'id' => '{{%notification}}.id'])
                ->from('{{%notification_users}}')
                ->join('LEFT JOIN',
                        '{{%notification}}',
                        '{{%notification}}.id = {{%notification_users}}.notification_id')
                ->where(['{{%notification_users}}.user_id' => $user_id]);
                        
            if(($_POST['module_type'] != '') && ($_POST['module_type'] != 'all')){
                $query->andWhere(['{{%notification}}.module' => $_POST['module_type']]);
            }
            $query->limit($limit);
            $query->offset($offset);

            $query->orderBy('{{%notification}}.date_create DESC');
            $command = $query->createCommand();
            $modelNotification = $command->queryAll();
            $result['info'] = $modelNotification;
            $result['unread_count'] = $nfCountUnread;
            $result['status'] = 'success';
        }else{
            $result['status'] = 'error';
        }
        
        echo json_encode($result);
    }
    
    public function actionGetnotificationunread()
    {
        $nfCountUnread = Notificationusers::find()->where(['user_id' => \Yii::$app->user->id, 'status' => '0'])->count();
        echo json_encode($nfCountUnread);        
    }
    
    public function actionNotificationunreaded()
    {
        $result = [];
        $notification_id = $_POST['notification_id'];
        $modelNotificationUser = Notificationusers::find()->where(['user_id' => \Yii::$app->user->id, 'notification_id' => $notification_id])->one();
        $modelNotificationUser->scenario = 'change_status';
        $modelNotificationUser->status = 1;
        if($modelNotificationUser->save()){
            $nfCountUnread = Notificationusers::find()->where(['user_id' => \Yii::$app->user->id, 'status' => '0'])->count();
            $result['status'] = 'success';
            $result['ntf_count'] = $nfCountUnread;
        }else{
            $result['status'] = 'error';
        }
        echo json_encode($result);
    }
    
}
