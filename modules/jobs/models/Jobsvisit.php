<?php

namespace app\modules\jobs\models;

use Yii;

class Jobsvisit extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%last_jobs_visit}}';
    }

//    public function beforeSave($insert)
//    {
//        if ($this->isNewRecord)
//        {
//              $this->date_create = date("Y-m-d H:i:s");
//        }
//
//        return parent::beforeSave($insert);
//    }
    
    public function scenarios()
    {
        return [
            'add_last_visit' => ['user_id', 'date_visit'],
	];
    }
    
    public function rules()
    {
        return [
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id'),
        ];
    }
    
}