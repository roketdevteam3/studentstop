<?php

namespace app\modules\jobs\models;

use Yii;

class Jobsindustry extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%jobs_industry}}';
    }

//    public function beforeSave($insert)
//    {
//        if ($this->isNewRecord)
//        {
//              $this->date_create = date("Y-m-d H:i:s");
//        }
//
//        return parent::beforeSave($insert);
//    }
    
    public function scenarios()
    {
        return [
            'add_industry' => ['name'],
	];
    }
    
    public function rules()
    {
        return [
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id'),
        ];
    }
    
}