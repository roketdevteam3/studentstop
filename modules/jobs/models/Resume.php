<?php

namespace app\modules\jobs\models;

use app\modules\users\models\User;
use app\modules\users\models\UserInfo;
use Yii;

class Resume extends \yii\db\ActiveRecord
{
    public $status;
    public $count_company_favorite;

    public static function tableName()
    {
        return '{{%user_resume}}';
    }

//    public function beforeSave($insert)
//    {
//        if ($this->isNewRecord)
//        {
//              $this->date_create = date("Y-m-d H:i:s");
//        }
//
//        return parent::beforeSave($insert);
//    }
    
    public function scenarios()
    {
        return [
            'add_resume' => ['avatar', 'about','skills','current_gpa','overall_gpa'],
	];
    }
    
    public function rules()
    {
        return [
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id'),
        ];
    }

  public function getUser()
  {
    return $this->hasOne(User::className(), ['id' => 'user_id']);
  }
  public function getUserinfo()
  {
    return $this->hasOne(UserInfo::className(), ['id_user' => 'user_id']);
  }
    
}