<?php

namespace app\modules\jobs\models;

use app\modules\university\models\University;
use Yii;

class Jobs extends \yii\db\ActiveRecord
{
    public $status;
    public $company_name;
    public $count_user_apple;

    public static function tableName()
    {
        return '{{%jobs}}';
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    public function scenarios()
    {
        return [
            'add_jobs' => ['title', 'vip_status', 'about', 'logo_src', 
            'company_id', 'function_id', 'industry_id', 'salary', 'address', 
            'gender', 'academic_status', 'university_id', 'major_id', 'salary_date','skills'],
	];
    }
    
    public function rules()
    {
        return [
            [['title','salary','salary','address'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id'),
        ];
    }

  public function getCompany()
  {
    return $this->hasOne(Company::className(), ['id' => 'company_id']);
  }

  public function getUniversity()
  {
    return $this->hasOne(University::className(), ['id' => 'university_id']);
  }

  public function getIndustry()
  {
    return $this->hasOne(Jobsindustry::className(), ['id' => 'industry_id']);
  }

  public function getJobfunction()
  {
    return $this->hasOne(Jobsfunction::className(), ['id' => 'function_id']);
  }

  public function getJobview()
  {
    return $this->hasOne(JobView::className(), ['job_id' => 'id']);
  }
    
}