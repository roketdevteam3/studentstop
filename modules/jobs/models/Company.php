<?php

namespace app\modules\jobs\models;

use app\modules\users\models\User;
use Yii;

/**
 * This is the model class for table "{{%user_info}}".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $network
 * @property string $academic_status
 * @property string $class
 * @property string $birthday
 * @property integer $status
 *
 * @property User $idUser
 */
class Company extends \yii\db\ActiveRecord
{
    public $jobs_count;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company}}';
    }

    /**
     * @inheritdoc
     */
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    public function scenarios()
    {
        return [
            'add_business' => ['name', 'about', 'logo_src', 'user_id',
                'phone','email','fax','address',
                'linkedin', 'facebook', 'twitter',
                'industry_id', 'company_create', 'website'],
          'status' => ['status'],
	];
    }
    
    public function rules()
    {
        return [
            [['name','company_create', 'address'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
  public function getUser()
  {
    return $this->hasOne(User::className(), ['id' => 'user_id']);
  }

  public function getIndustrial()
  {
    return $this->hasOne(Jobsindustry::className(), ['id' => 'industry_id']);
  }
}
