<?php

namespace app\modules\jobs\models;

use app\modules\users\models\UserInfo;
use Yii;

class Favoritejobs extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%favorite_jobs}}';
    }
    
    public function scenarios()
    {
        return [
            'add_favorite_job' => ['user_id','jobs_id']
        ];
    }
    
    public function rules()
    {
        return [
        //    [['title','salary','salary','address'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id'),
        ];
    }

  public function getJob()
  {
    return $this->hasOne(Jobs::className(), ['id' => 'jobs_id']);
  }

  public function getRezume()
  {
    return $this->hasOne(Resume::className(), ['user_id' => 'user_id']);
  }

  public function getUserinfo()
  {
    return $this->hasOne(UserInfo::className(), ['id_user' => 'user_id']);
  }
}