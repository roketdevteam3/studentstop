<?php

namespace app\modules\jobs\models;

use Yii;

class Jobsinvite extends \yii\db\ActiveRecord
{
    public $status;
    
    public static function tableName()
    {
        return '{{%user_jobs_invite}}';
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    public function scenarios()
    {
        return [
            'add_invite' => ['user_id', 'jobs_id'],
	];
    }
    
    public function rules()
    {
        return [
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id'),
        ];
    }
    
}