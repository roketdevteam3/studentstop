<?php

namespace app\modules\jobs\models;

use app\modules\users\models\User;
use app\modules\users\models\UserInfo;
use Yii;

/**
 * This is the model class for table "mr_favorite_rezume".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $rezume_id
 * @property integer $job_id
 */
class FavoriteRezume extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mr_favorite_rezume';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'rezume_id', 'job_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'rezume_id' => 'Rezume ID',
            'job_id' => 'Job ID',
        ];
    }
  public function getRezume()
  {
    return $this->hasOne(Resume::className(), ['id' => 'rezume_id']);
  }
  public function getUser()
  {
    return $this->hasOne(User::className(), ['id' => 'user_id']);
  }

  public function getUserinfo()
  {
    return $this->hasOne(UserInfo::className(), ['id_user' => 'user_id']);
  }
}
