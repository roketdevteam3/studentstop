<?php

namespace app\modules\jobs\models;

use app\modules\users\models\User;
use app\modules\users\models\UserInfo;
use Yii;

class Userjobs extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return '{{%user_jobs}}';
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    public function scenarios()
    {
        return [
            'add_user_jobs' => ['jobs_id', 'user_id','status'],
	];
    }
    
    public function rules()
    {
        return [
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id'),
        ];
    }

  public function getUser()
  {
    return $this->hasOne(User::className(), ['id' => 'user_id']);
  }
  public function getUserinfo()
  {
    return $this->hasOne(UserInfo::className(), ['id_user' => 'user_id']);
  }

  public function getRezume()
  {
    return $this->hasOne(Resume::className(), ['user_id' => 'user_id']);
  }

  public function getJob()
  {
    return $this->hasOne(Jobs::className(), ['id' => 'jobs_id']);
  }
}