<?php

namespace app\modules\jobs\models;

use app\modules\users\models\User;
use app\modules\users\models\UserInfo;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "mr_commit_news_company".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $company_id
 * @property string $commit
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $new_id
 */
class CommitNewsCompany extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mr_commit_news_company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'company_id', 'created_at', 'updated_at', 'new_id'], 'integer'],
            [['commit'], 'required'],
            [['commit'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'new_id' => 'New ID',
            'company_id' => 'Company ID',
            'commit' => 'Commit',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
  public function behaviors()
  {
    return [
      TimestampBehavior::className(),
    ];
  }
  public function getUser()
  {
    return $this->hasOne(User::className(), ['id' => 'user_id']);
  }

  public function getUserinfo()
  {
    return $this->hasOne(UserInfo::className(), ['id_user' => 'user_id']);
  }
}
