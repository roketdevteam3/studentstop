<?php

namespace app\modules\jobs\models;

use app\modules\users\models\User;
use Yii;

/**
 * This is the model class for table "mr_offer_job".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $job_id
 * @property string $salary
 * @property string $type
 */
class OfferJob extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mr_offer_job';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'job_id'], 'integer'],
            [['salary', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'job_id' => 'Job',
            'salary' => 'Salary',
            'type' => 'Type',
        ];
    }

  public function getUser()
  {
    return $this->hasOne(User::className(), ['id' => 'user_id']);
  }

  public function getJob()
  {
    return $this->hasOne(Jobs::className(), ['id' => 'job_id']);
  }
}
