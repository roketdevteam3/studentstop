<?php

namespace app\modules\jobs\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "mr_company_news".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $desc
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $like
 */
class CompanyNews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mr_company_news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'created_at', 'updated_at','like'], 'integer'],
            [['desc'], 'required'],
            [['desc'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'desc' => 'Desc',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

  public function behaviors()
  {
    return [
      TimestampBehavior::className(),
    ];
  }

    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

}
