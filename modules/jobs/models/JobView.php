<?php

namespace app\modules\jobs\models;

use Yii;

/**
 * This is the model class for table "mr_job_view".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $job_id
 * @property integer $status
 */
class JobView extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mr_job_view';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'job_id', 'status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'job_id' => 'Job ID',
        ];
    }
}
