<?php

namespace app\modules\jobs\models;

use app\modules\users\models\UserInfo;
use Yii;

/**
 * This is the model class for table "mr_favorite_company".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $company_id
 */
class FavoriteCompany extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mr_favorite_company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'company_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'company_id' => 'Company ID',
        ];
    }

  public function getCompany()
  {
    return $this->hasOne(Company::className(), ['id' => 'company_id']);
  }

  public function getRezume()
  {
    return $this->hasOne(Resume::className(), ['user_id' => 'user_id']);
  }
  public function getUserinfo()
  {
    return $this->hasOne(UserInfo::className(), ['id_user' => 'user_id']);
  }
}
