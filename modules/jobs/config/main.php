<?php

$module_name = basename(dirname(dirname(__FILE__)));

return [
    'modules' => [
        'jobs' => [
            'class' => 'app\modules\jobs\Module',
        ],
    ],

    'components' => [
        'urlManager' => [
            'rules' => [
                'job_recruitment' => 'jobs/default/job_recruitment',
                'rezume/<id:\w+>' => 'jobs/default/rezume_view',
                'create_resume' => 'jobs/default/create_rezume',
                'create_business' => 'jobs/default/create_business',
                'dashboard/job' => 'jobs/default/dashboard_job',
                'mycompany/candidates' => 'jobs/default/candidates',
                'mycompany/friends' => 'jobs/default/friends',
                'mycompany/<type:\w+>' => 'jobs/default/mycompany',
                'companylogosave' => 'jobs/default/companylogosave',
                'jobslogosave' => 'jobs/default/jobslogosave',
                'addbusiness' => 'jobs/default/addbusiness',
                'company/<company_id:\d+>' => 'jobs/default/company',
                'company' => 'jobs/default/company',
                'delete_company_new' => 'jobs/default/delete_company_new',
                'like_company_new' => 'jobs/default/like_company_new',
                'companies/<type:\w+>' => 'jobs/default/companies',
                'job/<job_id:\d+>' => 'jobs/default/job',
                
                'jobs/favorite' => 'jobs/default/favoritejobs',
                
                'applyjob' => 'jobs/default/applyjob',
                'unapplyjob' => 'jobs/default/unapplyjob',
                'company_view/<id:\d+>' => 'jobs/default/company_view',

                'addjobtofavorite' => 'jobs/default/addjobtofavorite',
                'deletejobwithfavorite' => 'jobs/default/deletejobwithfavorite',

                'addrezumetofavorite' => 'jobs/default/addrezumetofavorite',
                'deleterezumewithfavorite' => 'jobs/default/deleterezumewithfavorite',

                'addcompanytofavorite' => 'jobs/default/addcompanytofavorite',

                'deletecompanywithfavorite' => 'jobs/default/deletecompanywithfavorite',

                'apply_job_status' => 'jobs/default/apply_job_status',
                'offer_job_status' => 'jobs/default/offer_job_status',

                'searchjob' => 'jobs/default/searchjob',
                'getmajorwithuniversity' => 'jobs/default/getmajorwithuniversity',

                'get_address_university' => 'jobs/default/get_address_university',

                'jobs/<jobs_id:\d+>/apply_student' => 'jobs/default/jobs',
                'jobs/<jobs_id:\d+>/select_student' => 'jobs/default/jobsselectuser',
                
                'dashboard/myapplyjob' => 'jobs/default/myapplyjob',
                'dashboard/myresume' => 'jobs/default/myresume',
                'dashboard/myalertjobs' => 'jobs/default/myalertjobs',
                
                'userdataresume' => 'jobs/default/userdataresume',
                'userinvitejob' => 'jobs/default/userinvitejob',
                
                $module_name . '/<action:\w+>/<id:\d+>' => $module_name . '/default/<action>',
                $module_name . '/<action:\w+>' => $module_name . '/default/<action>',
            ],
        ],
    ],
];