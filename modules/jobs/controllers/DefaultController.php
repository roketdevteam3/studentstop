<?php

namespace app\modules\jobs\controllers;
use yii\helpers\Url;
use app\models\Event;
use app\modules\jobs\models\CommitNewsCompany;
use app\modules\jobs\models\CompanyNews;
use app\modules\jobs\models\FavoriteCompany;
use app\modules\jobs\models\FavoriteRezume;
use app\modules\jobs\models\JobView;
use app\modules\jobs\models\OfferJob;
use Yii;
use yii\web\Controller;
use app\modules\jobs\models\Company;
use app\modules\jobs\models\Jobs;
use app\modules\users\models\User;
use app\modules\users\models\UserRole;
use app\modules\users\models\UserInfo;
use app\modules\university\models\University;
use app\models\Major;
use app\modules\jobs\models\Favoritejobs;
use app\modules\jobs\models\Jobsinvite;
use app\modules\jobs\models\Jobsindustry;
use app\modules\jobs\models\Jobsfunction;
use app\modules\jobs\models\Jobsvisit;
use app\modules\jobs\models\Userjobs;
use app\modules\jobs\models\Resume;
use yii\web\ForbiddenHttpException;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\db\Query;
 error_reporting( E_ERROR );
/**
 * Default controller for the `jobs` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    
    public function beforeAction($action) {
        if(!\Yii::$app->user->isGuest){
            if(Yii::$app->user->identity->getUsertUniversity()){
                $this->enableCsrfValidation = false;
                return parent::beforeAction($action);
            }else{
                return $this->redirect(Url::to("/users/choiceuniversity",true));
            }
        }else{
            return $this->redirect(Url::to("/users/login",true));
        }
    }

    
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionCompanies($type)
    {
        $modelCompany = Company::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        $role = '';
        if($modelCompany == null){
            $role = 'student';
        }else{
            $role = 'company';
        }
        
        if($type == 'company'){
            $queryCompanies = Company::find();
            $modelCompanies = new ActiveDataProvider(['query' => $queryCompanies, 'pagination' => ['pageSize' => 20]]);

            foreach($modelCompanies->getModels() as $company){
                $company->jobs_count = Jobs::find()->where(['company_id' => $company->id])->count();
            }
            return $this->render('companies',[
                'modelCompanies' => $modelCompanies->getModels(),
                'pagination' => $modelCompanies->pagination,
                'count' => $modelCompanies->pagination->totalCount,
                'role' => $role,            
            ]);
            
        }else{
            throw new ForbiddenHttpException('Access denied');
        }
        
    }
    
    public function actionCompany($company_id=null)
    {
        $modelCompany = Company::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        $role = '';
        if($modelCompany == null){
            $role = 'student';
        }else{
            $role = 'company';
        }
        
        if($company_id == null){
            $queryAllJobs = Jobs::find();
            $modelAllJobs = new ActiveDataProvider(['query' => $queryAllJobs, 'pagination' => ['pageSize' => 20]]);
            $modelCompany = null;
        }else{
            $modelCompany = Company::find()->where(['id' => $company_id])->one();
            $queryAllJobs = Jobs::find()->where(['company_id' => $company_id]);
            $modelAllJobs = new ActiveDataProvider(['query' => $queryAllJobs, 'pagination' => ['pageSize' => 20]]);
        }
        
        foreach($modelAllJobs->getModels() as $jobs){
            $jobs->status = Userjobs::find()->where(['jobs_id' => $jobs->id,'user_id' => \Yii::$app->user->id])->one();
//            $companyModel = Company::find()->where(['id' => $jobs->company_id])->one();
//            $jobs->company_name = $companyModel->name;
        }
        
        return $this->render('company',[
            'modelAllJobs' => $modelAllJobs->getModels(),
            'pagination' => $modelAllJobs->pagination,
            'count' => $modelAllJobs->pagination->totalCount,
            'modelCompany' => $modelCompany,
            'role' => $role,
        ]);
    }
    
    public function actionMycompany($type=null)
    {
        $modelCompany = Company::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        $role = '';
        if($modelCompany == null){
            $role = 'student';
        }else{
            $role = 'company';
        }
    
        if($type == 'edit'){
            
            $modelCompany->scenario = 'add_business';
        
            if($_POST){
                $request = Yii::$app->request;
                if($modelCompany->load($request->post())){
                    if($modelCompany->save()){
                        Yii::$app->session->setFlash('company_update');
                    }else{
                        Yii::$app->session->setFlash('company_not_update');
                    }
                }
            }
            
            $jobIndustryArray = ArrayHelper::map(Jobsindustry::find()->all(),'id','name');
            return $this->render('mycompany',[
                'modelCompany' => $modelCompany, 
                'jobIndustryArray' => $jobIndustryArray, 
                'role' => $role, 
            ]);
            
        }elseif($type == 'jobs'){
            $modelNewJobs = new Jobs();
            $modelNewJobs->scenario = 'add_jobs';
            
            if($_POST){
                $request = Yii::$app->request;

                if($modelNewJobs->load($request->post())){


                    if($modelNewJobs->save()){
                        $this->redirect('/job/'.$modelNewJobs->id);
                        Yii::$app->session->setFlash('jobs_update');
                    }else{
                        Yii::$app->session->setFlash('jobs_not_update');
                    }
                }
            }
            
            $jobFunctionArray = ArrayHelper::map(Jobsfunction::find()->all(),'id','name');
            $jobIndustryArray = ArrayHelper::map(Jobsindustry::find()->all(),'id','name');
            $universityArray  = ArrayHelper::map(University::find()->all(),'id','name');
            $universityArray  = ArrayHelper::map(University::find()->all(),'id','name');
            $userRoleArray    = ArrayHelper::map(UserRole::find()->where(['status' => 1])->all(),'id','name');
          $majorsArray  = ArrayHelper::map(Major::find()->all(),'id','name');
            
            
            $query = new Query;
            $query->select(['*', 'id' => '{{%jobs}}.id', 'jobsCreate' => '{{%jobs}}.date_create', 'jobsLogo' => '{{%jobs}}.logo_src', 'jobsAddress' => '{{%jobs}}.address', 'jobsAbout' => '{{%jobs}}.about'])
                ->from('{{%jobs}}')
                ->join('LEFT JOIN',
                    '{{%company}}',
                    '{{%company}}.id = {{%jobs}}.company_id'
                )->where(['{{%jobs}}.company_id' => $modelCompany->id]);
            
            //$queryAllJobs = Jobs::find()->where(['company_id' => $modelCompany->id]);
            $modelAllJobs = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
            
            return $this->render('jobs',[
                'modelAllJobs' => $modelAllJobs->getModels(),
                'pagination' => $modelAllJobs->pagination,
                'count' => $modelAllJobs->pagination->totalCount,
                'jobFunctionArray' => $jobFunctionArray,
                'jobIndustryArray' => $jobIndustryArray,
                'userRoleArray' => $userRoleArray,
                'universityArray' => $universityArray,
                'modelNewJobs' => $modelNewJobs,
                'modelCompany' => $modelCompany,
                'majorsArray' => $majorsArray,
                'role' => $role,
            ]);
        }else{
            throw new ForbiddenHttpException('Access denied');
        }
        
    }
    
    public function actionCandidates()
    {
        $modelCompany = Company::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        $role = '';
        if($modelCompany == null){
            $role = 'student';
        }else{
            $role = 'company';
        }
        
        
        $query = new Query;
        $query->select(['*', 'id' => '{{%user}}.id', 'name' => '{{%user}}.name', 'academic_status' => '{{%user_role}}.name'])
            ->from('{{%user}}')
            ->join('LEFT JOIN',
                '{{%user_info}}',
                '{{%user_info}}.id_user = {{%user}}.id')
            ->join('LEFT JOIN',
                '{{%user_role}}',
                '{{%user_role}}.id = {{%user_info}}.academic_status');
        if($_GET){
            if(isset($_GET['username'])){
                if($_GET['username'] != ''){
                    $query->where(['like','{{%user}}.name',$_GET['username']])
                        ->orWhere(['like','{{%user}}.surname',$_GET['username']]);
                }
            }
            if(isset($_GET['university'])){
                if($_GET['university'] != ''){
                    $query->andWhere(['{{%user_info}}.university_id' => $_GET['university']]);
                }
            }
            if(isset($_GET['job_function'])){
                if($_GET['job_function'] != ''){
                    $query->andWhere(['{{%user_info}}.jobs_function_id' => $_GET['job_function']]);
                }
            }
            if(isset($_GET['industry'])){
                if($_GET['industry'] != ''){
                    $query->andWhere(['{{%user_info}}.industry_id' => $_GET['industry']]);
                }
            }
        }
        
        $companyArray = ArrayHelper::map(Company::find()->all(), 'id', 'name');
        $universityArray = ArrayHelper::map(University::find()->all(), 'id', 'name');
        $jobfunctionArray = ArrayHelper::map(Jobsfunction::find()->all(), 'id', 'name');
        $industryArray = ArrayHelper::map(Jobsindustry::find()->all(), 'id', 'name');
        $majorsArray  = ArrayHelper::map(Major::find()->all(),'id','name');

        $modelUsers = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
        return $this->render('candidates',[
            'modelUsers' => $modelUsers->getModels(),
            'pagination' => $modelUsers->pagination,
            'count' => $modelUsers->pagination->totalCount,
            'companyArray' => $companyArray,
            'universityArray' => $universityArray,
            'jobfunctionArray' => $jobfunctionArray,
            'industryArray' => $industryArray,
            'majorsArray' => $majorsArray,
            'role' => $role
        ]);
    }
    
    public function actionJobs($jobs_id)
    {
        $modelCompany = Company::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        $role = '';
        if($modelCompany == null){
            $role = 'student';
        }else{
            $role = 'company';
        }
        
        $query = new Query;
        $query->select(['*', 'id' => '{{%user_jobs}}.id','dateApply' => '{{%user_jobs}}.date_create'])
            ->from('{{%user_jobs}}')
            ->join('LEFT JOIN',
                '{{%user_info}}',
                '{{%user_info}}.id_user = {{%user_jobs}}.user_id'
            )->join('LEFT JOIN',
                '{{%user}}',
                '{{%user}}.id = {{%user_jobs}}.user_id'
            )->where(['{{%user_jobs}}.jobs_id' => $jobs_id])->orderBy('{{%user_jobs}}.date_create DESC');

        $modelUserJobs = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
        
        $modelJobs = Jobs::find()->where(['id' => $jobs_id])->one();
        $modelJobs->scenario = 'add_jobs';
        if($_POST){
            $request = Yii::$app->request;
            if($modelJobs->load($request->post())){
                if($modelJobs->save()){
                    Yii::$app->session->setFlash('jobs_update');
                }else{
                    Yii::$app->session->setFlash('jobs_not_update');
                }
            }
        }
      $jobFunctionArray = ArrayHelper::map(Jobsfunction::find()->all(),'id','name');
      $jobIndustryArray = ArrayHelper::map(Jobsindustry::find()->all(),'id','name');
      $universityArray  = ArrayHelper::map(University::find()->all(),'id','name');
      $majorsArray  = ArrayHelper::map(Major::find()->all(),'id','name');
      $userRoleArray    = ArrayHelper::map(UserRole::find()->where(['status' => 1])->all(),'id','name');

            
        return $this->render('one_jobs',[
            'modelUserJobs' => $modelUserJobs->getModels(),
            'pagination' => $modelUserJobs->pagination,
            'count' => $modelUserJobs->pagination->totalCount,
            'jobFunctionArray' => $jobFunctionArray,
            'jobIndustryArray' => $jobIndustryArray,
            'userRoleArray' => $userRoleArray,
            'universityArray' => $universityArray,
            'majorsArray' => $majorsArray,
            'modelJobs' => $modelJobs,
            'role' => $role
        ]);
    }
    
    public function actionJobsselectuser($jobs_id)
    {
        $modelCompany = Company::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        $role = '';
        if($modelCompany == null){
            $role = 'student';
        }else{
            $role = 'company';
        }
        
        $query = new Query;
        $query->select(['*', 'id' => '{{%user}}.id'])
            ->from('{{%user}}')
            ->join('LEFT JOIN',
                '{{%user_info}}',
                '{{%user_info}}.id_user = {{%user}}.id'
            );

        $modelUser = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
        
        $modelJobs = Jobs::find()->where(['id' => $jobs_id])->one();
        $modelJobs->scenario = 'add_jobs';
        if($_POST){
            $request = Yii::$app->request;
            if($modelJobs->load($request->post())){
                if($modelJobs->save()){
                    Yii::$app->session->setFlash('jobs_update');
                }else{
                    Yii::$app->session->setFlash('jobs_not_update');
                }
            }
        }
        
        return $this->render('one_jobs_select',[
            'modelUser' => $modelUser->getModels(),
            'pagination' => $modelUser->pagination,
            'count' => $modelUser->pagination->totalCount,
            
            'modelJobs' => $modelJobs,
            'role' => $role
        ]);
    }
    
    public function actionJob($job_id)
    {
        $modelCompany = Company::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        $role = '';
        if($modelCompany == null){
            $role = 'student';
        }else{
            $role = 'company';
        }
        
        $modelJobs = Jobs::find()->where(['id' => $job_id])->one();
        $modelJobs->scenario = 'add_jobs';
        if($_POST){
            $request = Yii::$app->request;
            if($modelJobs->load($request->post())){
                if($modelJobs->save()){
                    Yii::$app->session->setFlash('jobs_update');
                }else{
                    Yii::$app->session->setFlash('jobs_not_update');
                }
            }
        }

        $job_view = JobView::findOne(['job_id'=>$job_id, 'user_id'=>Yii::$app->user->id]);
        if(!$job_view){
          $job_view_new = new JobView();
          $job_view_new->user_id = Yii::$app->user->id;
          $job_view_new->job_id = $job_id;
          $job_view_new->save();
        }

        
        $query = new Query;
        $query->select(['*', 'id' => '{{%jobs}}.id',
            'jobFunctionName' => '{{%jobs_function}}.name',
            'jobIndustryName' => '{{%jobs_industry}}.name',
            'jobsAbout' => '{{%jobs}}.about',
            'jobsAddress' => '{{%jobs}}.address',
            'jobsLogo' => '{{%jobs}}.logo_src',
            'jobDateCreate' => '{{%jobs}}.date_create',
        ])
            ->from('{{%jobs}}')
            ->join('LEFT JOIN',
                '{{%company}}',
                '{{%company}}.id = {{%jobs}}.company_id')
            ->join('LEFT JOIN',
                '{{%jobs_function}}',
                '{{%jobs_function}}.id = {{%jobs}}.function_id')
            ->join('LEFT JOIN',
                '{{%jobs_industry}}',
                '{{%jobs_industry}}.id = {{%jobs}}.industry_id')
            ->where(['{{%jobs}}.id' => $job_id])->orderBy('{{%jobs}}.date_create DESC');
        $command = $query->createCommand();
        $modelJobsAll = $command->queryOne();
        
        $jobFunctionArray = ArrayHelper::map(Jobsfunction::find()->all(),'id','name');
        $jobIndustryArray = ArrayHelper::map(Jobsindustry::find()->all(),'id','name');
        
        $modelUserJobs = Userjobs::find()->where(['user_id' => \Yii::$app->user->id, 'jobs_id' => $job_id])->all();
        $modelUserJob1 = Userjobs::find()->where(['user_id' => \Yii::$app->user->id, 'jobs_id' => $job_id])->one();


      $modelUserFavorite = Favoritejobs::findAll(['jobs_id'=>$job_id]);
        
        $query = new Query;
        $query->select(['*', 'id' => '{{%user_jobs}}.id','dateApply' => '{{%user_jobs}}.date_create'])
            ->from('{{%user_jobs}}')
            ->join('LEFT JOIN',
                '{{%user_info}}',
                '{{%user_info}}.id_user = {{%user_jobs}}.user_id'
            )->join('LEFT JOIN',
                '{{%user}}',
                '{{%user}}.id = {{%user_jobs}}.user_id'
            )->where(['{{%user_jobs}}.jobs_id' => $job_id])->orderBy('{{%user_jobs}}.date_create DESC');

        $modelUserJob = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
        //var_dump($modelUserJob->getModels());exit;

        $companyJob = Company::findOne($modelJobs->company_id);

        $modelUserJobsFavorite = FavoriteRezume::find()->where(['job_id'=>$job_id])->all();
        $modelUserJobsMatches = Resume::find()
          ->joinWith('userinfo')
          ->where(['mr_user_info.university_id'=>$modelJobs->university_id])
          ->orWhere(['mr_user_info.jobs_function_id'=>$modelJobs->function_id])
          ->orWhere(['mr_user_info.industry_id'=>$modelJobs->industry_id])
          ->orWhere(['mr_user_info.major_id'=>$modelJobs->major_id])
          ->all();

        $offerJob = new OfferJob();
        $offerJob->job_id=$job_id;

      if(isset($_POST['offer_bt'])){
        if ($offerJob->load(Yii::$app->request->post())){
          $offerJob->save();
        }
      }

      $jobFunctionArray = ArrayHelper::map(Jobsfunction::find()->all(),'id','name');
      $jobIndustryArray = ArrayHelper::map(Jobsindustry::find()->all(),'id','name');
      $universityArray  = ArrayHelper::map(University::find()->all(),'id','name');
      $userRoleArray    = ArrayHelper::map(UserRole::find()->where(['status' => 1])->all(),'id','name');
      $majorsArray  = ArrayHelper::map(Major::find()->all(),'id','name');

      $userRezume = Resume::findOne(['user_id'=>Yii::$app->user->id]);

        return $this->render('job',[
            'modelUserJob' => $modelUserJob->getModels(),
            'pagination' => $modelUserJob->pagination,
            'count' => $modelUserJob->pagination->totalCount,
            'jobIndustryArray' => $jobIndustryArray,
            'jobFunctionArray' => $jobFunctionArray,
            'modelJobsAll' => $modelJobsAll,
            'modelJobs' => $modelJobs,
            'userRezume' => $userRezume,
            'companyJob' => $companyJob,
            'modelCompany' => $modelCompany,
            'modelUserJobs' => $modelUserJobs,
            'modelUserJob1' => $modelUserJob1,
            'modelUserJobsFavorite' => $modelUserJobsFavorite,
            'modelUserJobsMatches' => $modelUserJobsMatches,
            'role' => $role,
            'modelUserFavorite' => $modelUserFavorite,
          'jobFunctionArray' => $jobFunctionArray,
          'jobIndustryArray' => $jobIndustryArray,
          'majorsArray' => $majorsArray,
          'userRoleArray' => $userRoleArray,
          'universityArray' => $universityArray,
            'offerJob' => $offerJob
        ]);
    }
    
    public function actionAddbusiness()
    {
        $modelCompany = Company::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        $role = '';
        if($modelCompany == null){
            $role = 'student';
        }else{
            $role = 'company';
        }
        
        $modelNewCompany = new Company();
        $modelNewCompany->scenario = 'add_business';
        
        if($_POST){
            $request = Yii::$app->request;
            if($modelNewCompany->load($request->post())){
                if($modelNewCompany->save()){
                    return $this->redirect('companies/company');
                }
            }
        }
        $jobIndustryArray = ArrayHelper::map(Jobsindustry::find()->all(),'id','name');
        return $this->render('add_business',[
            'modelNewCompany' => $modelNewCompany,
            'jobIndustryArray' => $jobIndustryArray,
            'role' => $role, 
        ]);
    }


    public function actionMyapplyjob()
    {
        $modelCompany = Company::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        $role = '';
        if($modelCompany == null){
            $role = 'student';
        }else{
            $role = 'company';
        }
        $modelUserInfo = UserInfo::find()->where(['id_user' => \Yii::$app->user->id])->one();
        $queryUserJobs = Userjobs::find()->where(['user_id' => \Yii::$app->user->id]);
        
        $modelLastVisit = Jobsvisit::find()->where(['user_id' => \Yii::$app->user->id])->one();
        if($modelLastVisit == null){
            $newModelLastVisit = new Jobsvisit();
            $newModelLastVisit->scenario = 'add_last_visit';
            $newModelLastVisit->user_id = \Yii::$app->user->id;
            $newModelLastVisit->date_visit = date("Y-m-d H:i:s");
            $newModelLastVisit->save();
        }
        $countJobsLastVisit = Jobs::find()->where("date_create > '".$modelLastVisit->date_visit."'")->count();
        $countJobsTargetVisit = Jobs::find()
            ->where(' date_create > "'.$modelLastVisit->date_visit.'" and function_id  = "'.$modelUserInfo->jobs_function_id.'" ')
            ->orWhere('date_create > "'.$modelLastVisit->date_visit.'" and industry_id  = "'.$modelUserInfo->industry_id.'"')
            ->count();
        //$modelUserInfo
        $query = new Query;
        $query->select(['*', 'id' => '{{%user_jobs}}.id','dateApply' => '{{%user_jobs}}.date_create'])
            ->from('{{%user_jobs}}')
            ->join('LEFT JOIN',
                '{{%jobs}}',
                '{{%jobs}}.id = {{%user_jobs}}.jobs_id')
            ->join('LEFT JOIN',
                '{{%company}}',
                '{{%company}}.id = {{%jobs}}.company_id')
            ->where(['{{%user_jobs}}.user_id' => \Yii::$app->user->id])->orderBy('{{%user_jobs}}.date_create DESC');
        $modelUserJobs = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
        
        $queryVipjobs = new Query;
        $queryVipjobs->select(['*', 'id' => '{{%jobs}}.id', 'jobsLogo' => '{{%jobs}}.logo_src', 'jobsAddress' => '{{%jobs}}.address', 'jobsAbout' => '{{%jobs}}.about'])
            ->from('{{%jobs}}')
            ->join('LEFT JOIN',
                '{{%company}}',
                '{{%company}}.id = {{%jobs}}.company_id'
            )->where(['{{%jobs}}.vip_status' => 1])->orderBy('{{%jobs}}.date_create DESC');
        $modelVipJobs = new ActiveDataProvider(['query' => $queryVipjobs, 'pagination' => ['pageSize' => 20]]);
        
        return $this->render('applyjobs',[
            'modelUserJobs' => $modelUserJobs->getModels(),
            'pagination' => $modelUserJobs->pagination,
            'count' => $modelUserJobs->pagination->totalCount,
            'modelVipJobs' => $modelVipJobs->getModels(), 
            'countJobsLastVisit' => $countJobsLastVisit, 
            'countJobsTargetVisit' => $countJobsTargetVisit, 
            'role' => $role, 
        ]);
    }
    
    public function actionMyalertjobs()
    {
        $modelCompany = Company::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        $role = '';
        if($modelCompany == null){
            $role = 'student';
        }else{
            $role = 'company';
        }
        $query = new Query;
        $query->select(['*', 'id' => '{{%user_jobs_invite}}.id','dateApply' => '{{%user_jobs_invite}}.date_create'])
            ->from('{{%user_jobs_invite}}')
            ->join('LEFT JOIN',
                '{{%jobs}}',
                '{{%jobs}}.id = {{%user_jobs_invite}}.jobs_id')
            ->join('LEFT JOIN',
                '{{%company}}',
                '{{%company}}.id = {{%jobs}}.company_id')
            ->where(['{{%user_jobs_invite}}.user_id' => \Yii::$app->user->id])->orderBy('{{%user_jobs_invite}}.date_create DESC');

        $modelJobs = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
        
        return $this->render('alertjobs',[
            'modelJobs' => $modelJobs->getModels(),
            'pagination' => $modelJobs->pagination,
            'count' => $modelJobs->pagination->totalCount,
            
            'role' => $role, 
        ]);
    }
    
    public function actionMyresume()
    {
        $modelCompany = Company::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        $role = '';
        if($modelCompany == null){
            $role = 'student';
        }else{
            $role = 'company';
        }
        $modelUserInfo = UserInfo::find()->where(['id_user' => \Yii::$app->user->id])->one();
        $modelUser = User::find()->where(['id' => \Yii::$app->user->id])->one();
        $modelResume = Resume::find()->where(['user_id' => \Yii::$app->user->id])->one();
        if($modelResume == null){
            $modelResume = new Resume();
            $modelResume->scenario = 'add_resume';
            $modelResume->avatar = $modelUserInfo->avatar;
            $modelResume->user_id = Yii::$app->user->id;
        }
      $modelResume->scenario = 'add_resume';

        if($_POST){

            $request = Yii::$app->request;
//            if($modelResume->load($request->post())){
//                $modelResume->user_id = \Yii::$app->user->id;
//                $modelResume->save();
//            }
            $modelUser->scenario = 'update';
            if($modelUser->load($request->post())){
                if($modelUser->save()){
                    
                }
            }
            
            $modelUserInfo->scenario = 'resume';
            if($modelUserInfo->load($request->post())){
                $modelUserInfo->save();
            }

          if (isset($_FILES['avatar'])) {
            $file = \yii\web\UploadedFile::getInstanceByName('avatar');
            if($file){
              if ($file->saveAs(Yii::$app->basePath.'/web/images/users_images/' . Yii::$app->user->id.'/rezume_'. $file->name)) {
                //Now save file data to database
                $modelResume->avatar = Yii::$app->user->id.'/rezume_'. $file->name;
                $modelResume->save();

              }
            }else{
              $modelResume->save();
            }
          } else {
            $modelResume->save();
          }

        }
        $userRoleArray    = ArrayHelper::map(UserRole::find()->where(['status' => 1])->all(),'id','name');
        $jobFunctionArray = ArrayHelper::map(Jobsfunction::find()->all(),'id','name');
        $jobIndustryArray = ArrayHelper::map(Jobsindustry::find()->all(),'id','name');
        $universityArray  = ArrayHelper::map(University::find()->all(),'id','name');
        $majorArray = ArrayHelper::map(Major::find()->where(['university_id' => $modelUserInfo->university_id])->all(),'id','name');
        return $this->render('resume',[
            'universityArray' => $universityArray, 
            'jobIndustryArray' => $jobIndustryArray, 
            'jobFunctionArray' => $jobFunctionArray, 
            'modelUserInfo' => $modelUserInfo, 
            'modelUser' => $modelUser, 
            'majorArray' => $majorArray, 
            'modelResume' => $modelResume, 
            'userRoleArray' => $userRoleArray, 
            
            'role' => $role, 
        ]);
    }
    
    public function actionFavoritejobs()
    {
        $modelCompany = Company::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        $role = '';
        if($modelCompany == null){
            $role = 'student';
        }else{
            $role = 'company';
        }
        $query = new Query;
        $query->select(['*', 'id' => '{{%jobs}}.id', 'jobsCreate' => '{{%jobs}}.date_create', 'jobsLogo' => '{{%jobs}}.logo_src', 'jobsAddress' => '{{%jobs}}.address', 'jobsAbout' => '{{%jobs}}.about'])
            ->from('{{%favorite_jobs}}')
            ->join('LEFT JOIN',
                '{{%jobs}}',
                '{{%jobs}}.id = {{%favorite_jobs}}.jobs_id')
            ->join('LEFT JOIN',
                '{{%company}}',
                '{{%company}}.id = {{%jobs}}.company_id'
            )
        ->where(['{{%favorite_jobs}}.user_id' => \Yii::$app->user->id]);
        //$queryFavoritejobs = Favoritejobs::find()->where(['user_id' => \Yii::$app->user->id]);
        $modelFavoritejobs = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
        return $this->render('favorite',[
            'modelFavoritejobs' => $modelFavoritejobs->getModels(),
            'pagination' => $modelFavoritejobs->pagination,
            'count' => $modelFavoritejobs->pagination->totalCount,
            'role' => $role
        ]);
    }
    
    public function actionCompanylogosave()
    {
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/images/company_logo/';   //2
        // mkdir($storeFolder);
        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
              mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }
              $tempFile = $_FILES['file']['tmp_name'];          //3

              $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;  //4

              $for_name = time();

              $targetFile =  $targetPath.$for_name.'.jpg';  //5

              move_uploaded_file($tempFile,$targetFile); //6
              return \Yii::$app->user->id.'/'.$for_name.'.jpg';
        }
    }
    
    public function actionJobslogosave()
    {
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/images/jobs_logo/';   //2
        // mkdir($storeFolder);
        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
              mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }
              $tempFile = $_FILES['file']['tmp_name'];          //3

              $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;  //4

              $for_name = time();

              $targetFile =  $targetPath.$for_name.'.jpg';  //5

              move_uploaded_file($tempFile,$targetFile); //6
              return \Yii::$app->user->id.'/'.$for_name.'.jpg';
        }
    }
    
    public function actionApplyjob()
    {
        $result = [];
        $modalUserJobs = new Userjobs();
        $modalUserJobs->scenario = 'add_user_jobs';
        $modalUserJobs->user_id = \Yii::$app->user->id;
        $modalUserJobs->jobs_id = $_POST['jobs_id'];
        if($modalUserJobs->save()){
            $result['status'] = 'success';
        }else{
            $result['status'] = 'error';
        }
        
        echo json_encode($result);
    }
    
    public function actionAddjobtofavorite()
    {
        $result = [];
        $modalUserJobs = new Favoritejobs();
        $modalUserJobs->scenario = 'add_favorite_job';
        $modalUserJobs->user_id = \Yii::$app->user->id;
        $modalUserJobs->jobs_id = $_POST['jobs_id'];
        if($modalUserJobs->save()){
            $result['status'] = 'success';
        }else{
            $result['status'] = 'error';
        }
        
        echo json_encode($result);
    }
    
    public function actionUnapplyjob()
    {
        $result = [];
        $modalUserJobs = Userjobs::find()->where(['user_id' => \Yii::$app->user->id, 'jobs_id' => $_POST['jobs_id']])->one();

        if($modalUserJobs->delete()){
            $result['status'] = 'success';
        }else{
            $result['status'] = 'error';
        }
        
        echo json_encode($result);
    }
    
    public function actionDeletejobwithfavorite()
    {
        $result = [];


        if(Favoritejobs::find()->where(['user_id' => \Yii::$app->user->id, 'jobs_id' => $_POST['jobs_id']])->one()->delete()){
            $result['status'] = 'success';
        }else{
            $result['status'] = 'error';
        }
        
        echo json_encode($result);
    }
    
    public function actionUserdataresume()
    {
        $userId = $_POST['user_id'];
        $result = [];
        
        $modelUserInfo = Userinfo::find()->where(['id_user' => $userId])->one();
        $result['user_info'] = [];
        foreach($modelUserInfo as $key => $user_info){
            $result['user_info'][$key] = $user_info;
        }
        
        $modelUser = User::find()->where(['id' => $userId])->one();
        $result['user'] = [];
        foreach($modelUser as $key => $user){
            $result['user'][$key] = $user;
        }
        
        $modelResume = Resume::find()->where(['user_id' => $userId])->one();
        if($modelResume != null){
            $result['resume'] = [];
            foreach($modelResume as $key => $resume){
                $result['resume'][$key] = $resume;
            }
        }else{
            $result['resume'] = null;
        }
        echo json_encode($result);
    }
    
    public function actionUserinvitejob()
    {
        $user_id = $_POST['user_id'];
        $jobs_id = $_POST['jobs_id'];
        
        $modelInvite = Jobsinvite::find()->where(['user_id' => $user_id,'jobs_id' => $jobs_id])->one();
        $result = [];
        if($modelInvite == null){
            $modelNewInvite = new Jobsinvite();
            $modelNewInvite->scenario = 'add_invite';
            $modelNewInvite->user_id = $user_id;
            $modelNewInvite->jobs_id = $jobs_id;
            if($modelNewInvite->save()){
                $result['status'] = 'added';
            }else{
                $result['status'] = 'falsee';
            }
        }else{
            $result['status'] = 'not_added';
        }
        
        echo json_encode($result);
    }
    
    public function actionGetmajorwithuniversity()
    {
        $majorArray = ArrayHelper::map(Major::find()->where(['university_id' => $_POST['university_id']])->all(),'id','name');
        
        echo json_encode($majorArray);
    }

    public function actionGet_address_university()
    {
        $majorArray = University::findOne($_POST['university_id']);

      echo json_encode(['location'=>$majorArray->location]);
    }

    public function actionCompany_view($id){

      $modelCompany = Company::find()->where(['user_id'=>\Yii::$app->user->id])->one();
      $role = '';
      if($modelCompany == null){
        $role = 'student';
      }else{
        $role = 'company';
      }

      $company = Company::findOne($id);

      $jobs_q = Jobs::find()->where(['company_id'=>$id]);

      $countQuery = clone $jobs_q;
      $pages_jobs = new Pagination(['totalCount' => $countQuery->count(),
        'pageSize'=>5, 'defaultPageSize'=>5, 'pageParam'=>'job_page']);

      $jobs = $jobs_q->offset($pages_jobs->offset)
        ->limit($pages_jobs->limit)
        ->all();

      $friends  = FavoriteCompany::findAll(['company_id'=>$company->id]);

      $news_q = CompanyNews::find()->where(['company_id'=>$company->id])->orderBy(['id'=>SORT_DESC]);

      $countQuery = clone $news_q;
      $pages_news = new Pagination(['totalCount' => $countQuery->count(),
        'pageSize'=>5, 'defaultPageSize'=>5, 'pageParam'=>'news_page']);

      $news = $news_q->offset($pages_news->offset)
        ->limit($pages_news->limit)
        ->all();

      $array_jobs = [];
      $array_users = [];

      $jobs_company = Jobs::findAll(['company_id'=>$modelCompany->id]);

      foreach ($jobs_company as $item){
        $array_jobs[]=$item->id;
      }

      $offersJob = OfferJob::find()->where(['job_id'=>$array_jobs])->orderBy(['id'=>SORT_DESC])->all();

      foreach ($offersJob as $item){
        $array_users[]=$item->user_id;
      }

      $offersUsers = Resume::find()->where(['user_id'=>$array_users])->all();


      $applyUsers = Userjobs::findAll(['jobs_id'=>$array_jobs]);

      $rezume_favorites = FavoriteRezume::find()->where(['user_id'=>Yii::$app->user->id])->all();

      return $this->render('company_view',[
        'role' => $role,
        'jobs' => $jobs,
        'friends' => $friends,
        'company' => $company,
        'pages_jobs' => $pages_jobs,
        'news' => $news,
        'pages_news' => $pages_news,
        'offersUsers' => $offersUsers,
        'applyUsers' => $applyUsers,
        'rezume_favorites' => $rezume_favorites,
      ]);

    }
    
    /*public function actionSearchjob()
    {
    
        $modelCompany = Company::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        $role = '';
        if($modelCompany == null){
            $role = 'student';
        }else{
            $role = 'company';
        }
        $modelUserInfo = UserInfo::find()->where(['id_user' => \Yii::$app->user->id])->one();
        
            $modelLastVisit = Jobsvisit::find()->where(['user_id' => \Yii::$app->user->id])->one();
            if($modelLastVisit == null){
                $newModelLastVisit = new Jobsvisit();
                $newModelLastVisit->scenario = 'add_last_visit';
                $newModelLastVisit->user_id = \Yii::$app->user->id;
                $newModelLastVisit->date_visit = date("Y-m-d H:i:s");
                $newModelLastVisit->save();
            }else{
                $modelLastVisit->scenario = 'add_last_visit';
                $modelLastVisit->date_visit = date("Y-m-d H:i:s");
                $modelLastVisit->save();
            }
        
        $query = new Query;
        $query->select(['*', 'id' => '{{%jobs}}.id', 'jobsCreate' => '{{%jobs}}.date_create', 'jobsLogo' => '{{%jobs}}.logo_src', 'jobsAddress' => '{{%jobs}}.address', 'jobsAbout' => '{{%jobs}}.about'])
            ->from('{{%jobs}}')
            ->join('LEFT JOIN',
                '{{%company}}',
                '{{%company}}.id = {{%jobs}}.company_id'
            );
        if(isset($_GET['jobs_value'])){
            $query->where(['like','{{%jobs}}.about',$_GET['jobs_value']])
                ->orWhere(['like','{{%jobs}}.title',$_GET['jobs_value']])
                ->orWhere(['like','{{%company}}.name',$_GET['jobs_value']]);
        }
        if(isset($_GET['last_visit'])){
            $query->andWhere("{{%jobs}}.date_create > '".$modelLastVisit->date_visit."'")->count();
        }
        if(isset($_GET['target'])){
            $query->andWhere('{{%jobs}}.date_create > "'.$modelLastVisit->date_visit.'" and {{%jobs}}.function_id  = "'.$modelUserInfo->jobs_function_id.'" ')
            ->orWhere('{{%jobs}}.date_create > "'.$modelLastVisit->date_visit.'" and {{%jobs}}.industry_id  = "'.$modelUserInfo->industry_id.'" ');
        }
        if(isset($_GET['searchButton'])){
            if(isset($_GET['location'])){
             //   echo 'location'.'<br />';
            }
            if(isset($_GET['company'])){
                if($_GET['company'] != ''){
                    $query->andWhere(['{{%jobs}}.company_id' => $_GET['company']]);
                }
            }
            if(isset($_GET['university'])){
                if($_GET['university'] != ''){
                    $query->andWhere(['{{%jobs}}.university_id' => $_GET['university']]);
                }
            }
            if(isset($_GET['salary_from'])){
                if($_GET['salary_from'] != ''){
                    $query->andWhere('{{%jobs}}.salary > '.$_GET['salary_from']);
                }
            }
            if(isset($_GET['salary_to'])){
                if($_GET['salary_to'] != ''){
                    $query->andWhere('{{%jobs}}.salary < '.$_GET['salary_to']);
                }
            }
            if(isset($_GET['job_function'])){
                if($_GET['job_function'] != ''){
                    $query->andWhere(['{{%jobs}}.function_id' => $_GET['job_function']]);
                }
            }
            if(isset($_GET['industry'])){
                if($_GET['industry'] != ''){
                    $query->andWhere(['{{%jobs}}.industry_id' => $_GET['industry']]);
                }
            }
        }
        
        $query->orderBy('{{%jobs}}.date_create DESC');
        $modelAllJobs = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
        
        return $this->render('searchjob',[
            'modelAllJobs' => $modelAllJobs->getModels(),
            'pagination' => $modelAllJobs->pagination,
            'count' => $modelAllJobs->pagination->totalCount,
            
            'modelCompany' => $modelCompany,
            'role' => $role
        ]);
    }*/


    //andrew_ls

  public function actionSearchjob()
  {

    $modelCompany = Company::find()->where(['user_id'=>\Yii::$app->user->id])->one();
    $role = '';
    if($modelCompany == null){
      $role = 'student';
    }else{
      $role = 'company';
    }


    //search get


    $jobs_q = Jobs::find();

    $companies_q = Company::find();

    $users_q = Resume::find()->joinWith('userinfo');


    if (Yii::$app->request->get()){
      if (isset($_GET['university'])){
        foreach ($_GET['university'] as $item){
          $jobs_q->orWhere(['university_id'=>$_GET['university']]);
          $users_q->orWhere(['university_id'=>$_GET['university']]);
        }
      }


      if (isset($_GET['company'])){
        foreach ($_GET['company'] as $item){
          $jobs_q->orWhere(['company_id'=>$_GET['company']]);
        }
      }

      if (isset($_GET['job_function'])){
        foreach ($_GET['job_function'] as $item){
          $jobs_q->orWhere(['function_id'=>$_GET['job_function']]);
          $users_q->orWhere(['jobs_function_id'=>$_GET['job_function']]);
        }
      }


      if (isset($_GET['industry'])){
        foreach ($_GET['industry'] as $item){
          $jobs_q->orWhere(['industry_id'=>$_GET['industry']]);
          $companies_q->orWhere(['industry_id'=>$_GET['industry']]);
          $users_q->orWhere(['industry_id'=>$_GET['industry']]);
        }
      }

      if (isset($_GET['date_end_education'])){
        $users_q->orWhere(['<','date_end_education',$_GET['date_end_education']]);
      }

      if (isset($_GET['date_begin_education'])){
        $users_q->orWhere(['>','date_begin_education',$_GET['date_begin_education']]);
      }

      if (isset($_GET['skills'])){
        $array = explode(",", $_GET['skills']);

        foreach ($array as $item){
          $jobs_q->orWhere(['like', 'skills', $item]);
          $users_q->orWhere(['like', 'skills', $item]);
        }
      }

      if (isset($_GET['location'])){
        $array = explode(",", $_GET['location']);

        foreach ($array as $item){
          $jobs_q->orWhere(['like', 'address', $item]);
          $users_q->orWhere(['like', 'address', $item]);
        }
      }

      if(isset($_GET['type_search'])){
        if(isset($_GET['jobs_value'])){
          $array = explode(" ", $_GET['jobs_value']);
          switch ($_GET['type_search']){
            case 'name':
              foreach ($array as $item){
                $jobs_q->orWhere(['like', 'title', $item]);
                $companies_q->orWhere(['like', 'name', $item]);
              }
              break;
            case 'tags':
              foreach ($array as $item){
                $jobs_q->orWhere(['like', 'skills', $item]);
                $users_q->orWhere(['like', 'skills', $item]);
              }
              break;
            case 'industry':
              $industr = Jobsindustry::find();
              foreach ($array as $item){
                $industr->orWhere(['like', 'name', $item]);
              }
              $industr->all();

              $array_in = [];
              foreach ($industr as $value){
                $array_in[]=$value->id;
              }
              $jobs_q->orWhere(['industry_id'=>$array_in]);
              $companies_q->orWhere(['industry_id'=>$array_in]);
              $users_q->orWhere(['industry_id'=>$array_in]);
              break;
          }
        }
      }

    }



    $countQuery = clone $jobs_q;
    $pages_jobs = new Pagination(['totalCount' => $countQuery->count(),
      'pageSize'=>5, 'defaultPageSize'=>5, 'pageParam'=>'job_page']);

    $jobs = $jobs_q->offset($pages_jobs->offset)
      ->limit($pages_jobs->limit)
      ->all();


    $countQuery = clone $companies_q;
    $pages_companies = new Pagination(['totalCount' => $countQuery->count(),
      'pageSize'=>5, 'defaultPageSize'=>5, 'pageParam'=>'job_page']);

    $companies = $companies_q->offset($pages_companies->offset)
      ->limit($pages_companies->limit)
      ->all();




    $countQuery = clone $users_q;
    $pages_users = new Pagination(['totalCount' => $countQuery->count(),
      'pageSize'=>5, 'defaultPageSize'=>5, 'pageParam'=>'job_page']);

    $users = $users_q->offset($pages_users->offset)
      ->limit($pages_users->limit)
      ->all();

    $offerJob = new OfferJob();

    if (isset($_POST['offer_bt'])){
      if ($offerJob->load(Yii::$app->request->post())){
        $offerJob->save();
      }
    }

    $userRezume = Resume::findOne(['user_id'=>Yii::$app->user->id]);

    return $this->render('search_job',[
      'role' => $role,
      'userRezume' => $userRezume,
      'users' => $users,
      'companies' => $companies,
      'pages_jobs' => $pages_jobs,
      'pages_users' => $pages_users,
      'pages_companies' => $pages_companies,
      'offerJob' => $offerJob,
      'jobs' => $jobs
    ]);
  }

  public function actionJob_recruitment()
  {
    return $this->render('job_recruitment');
  }

  public function actionRezume_view($id){
    $modelCompany = Company::find()->where(['user_id'=>\Yii::$app->user->id])->one();
    $role = '';
    if($modelCompany == null){
      $role = 'student';
    }else{
      $role = 'company';
    }

    $modelResume = Resume::find()->where(['id' => $id])->one();
    $modelUserInfo = UserInfo::find()->where(['id_user' => $modelResume->user_id])->one();
    $modelUser = User::find()->where(['id' => $modelResume->user_id])->one();
    $userRoleArray    = ArrayHelper::map(UserRole::find()->where(['status' => 1])->all(),'id','name');
    $jobFunctionArray = ArrayHelper::map(Jobsfunction::find()->all(),'id','name');
    $jobIndustryArray = ArrayHelper::map(Jobsindustry::find()->all(),'id','name');
    $universityArray  = ArrayHelper::map(University::find()->all(),'id','name');
    $majorArray = ArrayHelper::map(Major::find()->where(['university_id' => $modelUserInfo->university_id])->all(),'id','name');
    return $this->render('rezume_view',[
      'role'=>$role,
      'universityArray' => $universityArray,
      'jobIndustryArray' => $jobIndustryArray,
      'jobFunctionArray' => $jobFunctionArray,
      'modelUserInfo' => $modelUserInfo,
      'modelUser' => $modelUser,
      'majorArray' => $majorArray,
      'modelResume' => $modelResume,
      'userRoleArray' => $userRoleArray,
    ]);
  }

  public function actionCreate_rezume()
    {
      $modelCompany = Company::find()->where(['user_id'=>\Yii::$app->user->id])->one();
      $role = '';
      if($modelCompany == null){
        $role = 'student';
      }else{
        $role = 'company';
      }
      $modelUserInfo = UserInfo::find()->where(['id_user' => \Yii::$app->user->id])->one();
      $modelUser = User::find()->where(['id' => \Yii::$app->user->id])->one();
      $modelResume = Resume::find()->where(['user_id' => \Yii::$app->user->id])->one();
      if($modelResume == null){
        $modelResume = new Resume();
        $modelResume->scenario = 'add_resume';
        $modelResume->avatar = $modelUserInfo->avatar;
        $modelResume->user_id = Yii::$app->user->id;
      }
      $modelResume->scenario = 'add_resume';

      if($_POST){
        $request = Yii::$app->request;
//            if($modelResume->load($request->post())){
//                $modelResume->user_id = \Yii::$app->user->id;
//                $modelResume->save();
//            }
        $modelUser->scenario = 'update';
        if($modelUser->load($request->post())){
          if($modelUser->save()){

          }
        }

        $modelUserInfo->scenario = 'resume';
        if($modelUserInfo->load($request->post())){
          $modelUserInfo->save();
        }

        if($modelResume->load($request->post())){
          $modelResume->save();
        }

        if (isset($_FILES['avatar'])) {

          $file = \yii\web\UploadedFile::getInstanceByName('avatar');
          if($file){
            if ($file->saveAs(Yii::$app->basePath.'/web/images/users_images/' . Yii::$app->user->id.'/rezume_'. $file->name)) {
              //Now save file data to database
              $modelResume->avatar = Yii::$app->user->id.'/rezume_'. $file->name;
              $modelResume->save();

            }
          }

        } else {
          $modelResume->save();
        }

        $this->redirect('/dashboard/job');
      }
      $userRoleArray    = ArrayHelper::map(UserRole::find()->where(['status' => 1])->all(),'id','name');
      $jobFunctionArray = ArrayHelper::map(Jobsfunction::find()->all(),'id','name');
      $jobIndustryArray = ArrayHelper::map(Jobsindustry::find()->all(),'id','name');
      $universityArray  = ArrayHelper::map(University::find()->all(),'id','name');
      $majorArray = ArrayHelper::map(Major::find()->where(['university_id' => $modelUserInfo->university_id])->all(),'id','name');
      return $this->render('create_rezume',[
        'universityArray' => $universityArray,
        'jobIndustryArray' => $jobIndustryArray,
        'jobFunctionArray' => $jobFunctionArray,
        'modelUserInfo' => $modelUserInfo,
        'modelUser' => $modelUser,
        'majorArray' => $majorArray,
        'modelResume' => $modelResume,
        'userRoleArray' => $userRoleArray,

        'role' => $role,
      ]);
    }

  public function actionDashboard_job(){

    $event = new Event();
    $event->place_type='job';
    $event->place_id=Yii::$app->user->id;

    if (isset($_POST['event'])){
      $event->load(Yii::$app->request->post());
      $event->save();
    }

    $offerJob = new OfferJob();

    if (isset($_POST['offer_bt'])){
      if ($offerJob->load(Yii::$app->request->post())){
        $offerJob->save();
      }
    }

    $modelCompany = Company::find()->where(['user_id'=>\Yii::$app->user->id])->one();
    $role = '';
    $new = '';
    $rezumes = '';
    $matches = '';
    $offersUsers = '';
    $applyUsers = '';
    $applyUsersNew = '';
    if($modelCompany == null){
      if (Resume::find()->where(['user_id'=>\Yii::$app->user->id])->one()){
        $role = 'student';
        $offersUsers = OfferJob::findAll(['user_id'=>Yii::$app->user->id]);
      } else {
            $modelUserInfo = UserInfo::find()->where(['id_user' => \Yii::$app->user->id])->one();
            if($modelUserInfo->academic_status == 8){
                $this->redirect('/create_business');
            }else{
                $this->redirect('/job_recruitment');
            }
      }

    }else{
      $role = 'company';
      $rezumes = Resume::find()->all();
//      $matches = Resume::find()->all();
      $matches = Resume::find()->joinWith('userinfo')->where(['industry_id'=>$modelCompany->industry_id])->all();

      $array_jobs = [];
      $array_users = [];

      $jobs_company = Jobs::findAll(['company_id'=>$modelCompany->id]);

      foreach ($jobs_company as $item){
        $array_jobs[]=$item->id;
      }

      $offersJob = OfferJob::find()->where(['job_id'=>$array_jobs])->orderBy(['id'=>SORT_DESC])->all();

      foreach ($offersJob as $item){
        $array_users[]=$item->user_id;
      }

      $offersUsers = Resume::find()->where(['user_id'=>$array_users])->all();


      $applyUsers = Userjobs::findAll(['jobs_id'=>$array_jobs]);

      $applyUsersNew = Userjobs::find()->where(['jobs_id'=>$array_jobs,'status'=>0])->count();

      $new = new CompanyNews();
      $new->company_id = $modelCompany->id;

      if (isset($_POST['new_send'])){
        if ($new->load(Yii::$app->request->post())){
          $new->save();
          $new->desc = null;
        }
      }

      $news_q = CompanyNews::find()->where(['company_id'=>$modelCompany->id])->orderBy(['id'=>SORT_DESC]);

      $countQuery = clone $news_q;
      $pages_news = new Pagination(['totalCount' => $countQuery->count(),
        'pageSize'=>5, 'defaultPageSize'=>5, 'pageParam'=>'news_page']);

      $news = $news_q->offset($pages_news->offset)
        ->limit($pages_news->limit)
        ->all();



    }


    $rezume_favorites = FavoriteRezume::find()->where(['user_id'=>Yii::$app->user->id])->all();

    $company_favorites = FavoriteCompany::find()->where(['user_id'=>Yii::$app->user->id])->all();

    $userRezume = Resume::findOne(['user_id'=>Yii::$app->user->id]);


    if ( $role == 'student'){


      $array_company = [];

      foreach ($company_favorites as $item){
        $array_company[]=$item->company_id;
      }

      $applyUsers = Userjobs::findAll(['user_id'=>Yii::$app->user->id]);

      $userInfo = UserInfo::findOne(['id_user'=>Yii::$app->user->id]);

      if (Yii::$app->request->get('new')){
        $jobs = Jobs::find()
          ->joinWith('jobview')
          ->where(['industry_id'=>$userInfo->industry_id,'status'=>1])
          ->all();

        $jobsAll = Jobs::find()
          ->joinWith('jobview')
          ->where(['industry_id'=>$userInfo->industry_id,'status'=>1])
          ->orWhere(['company_id'=>$array_company])
          ->all();
      } else {
        $jobs = Jobs::find()
          ->where(['industry_id'=>$userInfo->industry_id])
          ->all();
        $jobsAll = Jobs::find()
          ->where(['industry_id'=>$userInfo->industry_id])
          ->orWhere(['company_id'=>$array_company])
          ->all();
      }


      $jobs_favorites = Favoritejobs::find()->where(['user_id'=>Yii::$app->user->id])->all();
      $jobs_top = Jobs::find()
        ->where([
          'industry_id'=>$userInfo->industry_id,
          'vip_status' => 1,
        ])
        ->all();

      $news_q = CompanyNews::find()->where(['company_id'=>$array_company])->orderBy(['id'=>SORT_DESC]);

      $countQuery = clone $news_q;
      $pages_news = new Pagination(['totalCount' => $countQuery->count(),
        'pageSize'=>5, 'defaultPageSize'=>5, 'pageParam'=>'news_page']);

      $news = $news_q->offset($pages_news->offset)
        ->limit($pages_news->limit)
        ->all();

    } else {
      $jobs = null;
      $jobsAll = null;
      $jobs_favorites = null;
      $jobs_top = null;
      $commitNew = null;
    }



    $commitNew = new CommitNewsCompany();
    $commitNew->user_id = Yii::$app->user->id;
    if (isset($_POST['commit_send'])){
      if ($commitNew->load(Yii::$app->request->post())){
        $commitNew->save();
        $commitNew->commit = null;
      }
    }



    $countJobsLastVisit = 0;
    $countJobsTargetVisit = 0;

    foreach ($jobs as $item){
      if (!$item->jobview['status']){
        $countJobsLastVisit++;
      }
      $countJobsTargetVisit++;
    }


    $top_5_student = Resume::find()->select(['*','count_company_favorite'=>'(SELECT COUNT(*) FROM mr_favorite_company fc WHERE fc.user_id = mr_user_resume.user_id )'])->limit(5)->orderBy(['count_company_favorite'=>SORT_DESC])->all();

    $top_5_company = Company::find()->select(['*','count_company_favorite'=>'(SELECT COUNT(*) FROM mr_favorite_company fc WHERE fc.company_id = mr_company.id )'])->limit(5)->orderBy(['count_company_favorite'=>SORT_DESC])->all();

    $top_5_job = Jobs::find()->select(['*','count_user_apple'=>'(SELECT COUNT(*) FROM mr_user_jobs uj WHERE uj.jobs_id = mr_jobs.id )'])->limit(5)->orderBy(['count_user_apple'=>SORT_DESC])->all();





    return $this->render('dashboard_job',[
      'type' => 'rezume',
      'role' => $role,
      'applyUsersNew' => $applyUsersNew,
      'countJobsLastVisit' => $countJobsLastVisit,
      'countJobsTargetVisit' => $countJobsTargetVisit,
      'jobs' => $jobs,
      'top_5_student' => $top_5_student,
      'top_5_company' => $top_5_company,
      'top_5_job' => $top_5_job,
      'jobs_top' => $jobs_top,
      'eventsM' => $event,
      'rezumes' => $rezumes,
      'matches' => $matches,
      'offerJob' => $offerJob,
      'jobsAll' => $jobsAll,
      'userRezume' => $userRezume,
      'rezume_favorites' => $rezume_favorites,
      'company_favorites' => $company_favorites,
      'jobs_favorites' => $jobs_favorites,
      'modelCompany' => $modelCompany,
      'offersUsers' => $offersUsers,
      'applyUsers' => $applyUsers,
      'new'=>$new,
      'news'=>$news,
      'pages_news'=>$pages_news,
      'commitNew'=>$commitNew,
    ]);
  }


  public function actionEvents_job()
  {
//    Yii::$app->response->format =
//    return true;
    $date = $_POST['date'];

      $modelEvent = Event::find()->where("date_start <= '".$date."' and date_end >= '".$date."'")->orWhere(['LIKE', 'date_create', $date])
        ->andWhere(['place_type'=>'job','place_id'=>Yii::$app->user->id])
        ->asArray()->all();

    echo \yii\helpers\Json::encode($modelEvent);
  }

  public function actionEvents_job_student()
  {
//    Yii::$app->response->format =
//    return true;
    $date = $_POST['date'];

    $favoriteCompany = FavoriteCompany::find()->where(['user_id'=>Yii::$app->user->id])->all();

    $array = [];
    foreach ($favoriteCompany as $item){
      $array[] =  $item->company_id;
    }

    $modelEvent = Event::find()->where("date_start <= '".$date."' and date_end >= '".$date."'")->orWhere(['LIKE', 'date_create', $date])
      ->andWhere(['place_type'=>'job','place_id'=>$array])
      ->asArray()->all();

    echo \yii\helpers\Json::encode($modelEvent);
  }

  public function actionCreate_business()
    {

      $modelCompany = Company::find()->where(['user_id'=>\Yii::$app->user->id])->one();
      $role = '';
      
      if($modelCompany == null){
        $modelCompany = new Company();
        $modelCompany->user_id =\Yii::$app->user->id;
        $role = 'student';
      }else{
        $role = 'company';
          $modelUserInfo = UserInfo::find()->where(['id_user' => \Yii::$app->user->id])->one();
          if($modelUserInfo->academic_status == 8){
              return $this->redirect('/dashboard/job');

          }
      }

      $modelCompany->scenario = 'add_business';

      if($_POST){
        $request = Yii::$app->request;
        if($modelCompany->load($request->post())){
          if($modelCompany->save()){
            $this->redirect('/dashboard/job');
          }else{
            Yii::$app->session->setFlash('company_not_update');
          }
        }

      }

      $jobIndustryArray = ArrayHelper::map(Jobsindustry::find()->all(),'id','name');
      return $this->render('create_business',[
        'modelCompany' => $modelCompany,
        'jobIndustryArray' => $jobIndustryArray,
        'role' => $role,
      ]);

    }

  public function actionDeleterezumewithfavorite()
  {
    $result = [];
    $modalUserJobs = FavoriteRezume::find()->where(['user_id' => \Yii::$app->user->id, 'rezume_id' => $_POST['rezume_id']])->one();
    if($modalUserJobs->delete()){
      $result['status'] = 'success';
    }else{
      $result['status'] = 'error';
    }

    echo json_encode($result);
  }

  public function actionDeletecompanywithfavorite()
  {
    $result = [];
    $modalUserJobs = FavoriteCompany::find()->where(['user_id' => \Yii::$app->user->id, 'company_id' => $_POST['company_id']])->one();
    if($modalUserJobs->delete()){
      $result['status'] = 'success';
    }else{
      $result['status'] = 'error';
    }

    echo json_encode($result);
  }

  public function actionAddrezumetofavorite()
  {
    $result = [];
    $modalUserJobs = new FavoriteRezume();
    $modalUserJobs->user_id = \Yii::$app->user->id;
    $modalUserJobs->rezume_id = $_POST['rezume_id'];
    $modalUserJobs->job_id = $_POST['job_id'];
    if($modalUserJobs->save()){
      $result['status'] = 'success';
    }else{
      $result['status'] = 'error';
    }

    echo json_encode($result);
  }

  public function actionAddcompanytofavorite()
  {
    $result = [];
    $modalUserJobs = new FavoriteCompany();
    $modalUserJobs->user_id = \Yii::$app->user->id;
    $modalUserJobs->company_id = $_POST['company_id'];
    if($modalUserJobs->save()){
      $result['status'] = 'success';
    }else{
      $result['status'] = 'error';
    }

    echo json_encode($result);
  }

  public function actionApply_job_status()
  {

    $userJob = Userjobs::findOne($_POST['user_job_id']);

    if ($userJob){
      if($_POST['status']==1){
        $userJob->status = $_POST['status'];

        $userJob->scenario = 'add_user_jobs';
        $userJob->save();

        $company = Company::findOne(['user_id'=>Yii::$app->user->id]);

        $fav_com = new FavoriteCompany();
        $fav_com->user_id=$_POST['user_id'];
        $fav_com->company_id = $company->id;
        $fav_com->save();
      }else{
        $userJob->status = $_POST['status'];

        $userJob->scenario = 'add_user_jobs';
        $userJob->save();
      }
    }
    return true;
  }

  public function actionOffer_job_status()
  {

    $offerJob = OfferJob::findOne($_POST['user_job_id']);

    if ($offerJob){
      if($_POST['status']==1){
        $offerJob->status = $_POST['status'];

        $offerJob->save();

      }else{
        $offerJob->status = $_POST['status'];
        $offerJob->save();
      }
    }
    return true;
  }

  public function actionFriends()
  {

    $company = Company::find()->where(['user_id'=>\Yii::$app->user->id])->one();
    $role = 'company';
    $friends = FavoriteCompany::find()->where(['company_id'=>$company->id])->all();

    $array = [];

    foreach ($friends as $friend){
      $array[]=$friend->id;
    }

    $users_q = Resume::find()->where(['user_id'=>$array]);

    $countQuery = clone $users_q;
    $pages_users = new Pagination(['totalCount' => $countQuery->count(),
      'pageSize'=>5, 'defaultPageSize'=>5, 'pageParam'=>'job_page']);

    $users = $users_q->offset($pages_users->offset)
      ->limit($pages_users->limit)
      ->all();

    $offerJob = new OfferJob();

    if (isset($_POST['offer_bt'])){
      if ($offerJob->load(Yii::$app->request->post())){
        $offerJob->save();
      }
    }

    $userRezume = Resume::findOne(['user_id'=>Yii::$app->user->id]);


    return $this->render('friends',[
      'users' => $users,
      'role'=> $role,
      'offerJob' => $offerJob,
      'pages_users' => $pages_users,
      'userRezume' => $userRezume,
    ]);
  }

  public function actionDelete_company_new(){
      if(CompanyNews::findOne($_POST['id'])->delete())
          return true;
      else
          return false;

  }
    public function actionLike_company_new(){
      $model = CompanyNews::findOne($_POST['id']);
      $model->like = $model->like+1;
        if($model->save())
            return true;
        else
            return false;

    }
}
