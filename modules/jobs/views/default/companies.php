<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

?>
<div class="col-sm-12" style="background: url(<?= Url::home(); ?>images/univesity_fon.png) center;background-size:cover;background-repeat:no-repeat;padding:0px;min-height:93vh;">
    
    <div class="col-sm-10" style='padding:0px;'>
        <div class="col-sm-12">
            <div class="col-sm-12" style='padding:0px;background-color:white;'>
                <?= $this->render('menu',['role'=>$role]); ?>
            </div>
        </div>
        <div class="col-sm-12" style="padding-top: 20px;">
            <div class="col-sm-12">
                <div class="col-sm-12" style='box-shadow: 0 0 10px rgba(0,0,0,0.1);background-color:white;padding:0px;'>
                    <ul class="nav nav-tabs">
                        <li role="presentation" class="active">
                            <?= HTML::a('All companies',Url::home().'companies/company'); ?>
                        </li>
                        <li role="presentation">
                            <?= HTML::a('All jobs',Url::home().'company'); ?>
                        </li>
                    </ul>
                </div>
            </div>
            <?php foreach($modelCompanies as $company){ ?>
                <div class="col-sm-12" style='margin-top:10px;margin-bottom:10px;'>
                    <div class="col-sm-12" style='max-height:100vh;background-color:white;box-shadow: 0 0 10px rgba(0,0,0,0.2);padding:10px;'>
                        <div class="col-sm-1" style="font-size:12px;">
                            <i class="fa fa-star-o"></i><br>
                            <i class="fa fa-star" style="color:yellow;"></i><br>
                            <i class="fa fa-star" style="color:yellow;"></i><br>
                            <i class="fa fa-star" style="color:yellow;"></i><br>
                            <i class="fa fa-star" style="color:yellow;"></i><br>
                        </div>
                        <div class="col-sm-2">
                            <img src="<?= Url::home().'images/company_logo/'.$company->logo_src; ?>" style="width:100%;max-height:150px;">
                        </div>
                        <div class="col-sm-2">
                            <h4 style="font-size:14px;"><?= $company->name; ?></h4>
                            <p style="font-size:11px;"><i><?= $company->address; ?></i></p>
                            <p style="font-size:12px;"><?= substr($company->about, 0, 60).'...'; ?></p>
                        </div>
                        <div class="col-sm-2">
                            <h4 style="text-align:center;font-size:14px;">Vacancies</h4>
                            <p style="text-align:center;font-size:25px;color:#5ec6ed;"><?= $company->jobs_count; ?></p>
                        </div>
                        <div class="col-sm-3">
                            <h4 style="font-size:14px;">Contact info</h4>
                            <p  style="font-size:12px;"><b>Phone:</b> <?= $company->phone; ?></p>
                            <p style="font-size:12px;"><b>Email:</b> <?= $company->email; ?></p>
                            <p style="font-size:12px;"><b>Website:</b> <?= $company->website; ?></p>
                            <p style="font-size:12px;"><b>Fax:</b> <?= $company->fax; ?></p>
                        </div>
                        <div class="col-sm-2">
                            <button class="btn" style="font-size:12px;padding-top:0px; padding-bottom:0px;margin-top:10px;width:100%;background-color:transparent;">Add to favorite<i class="fa fa-star pull-right" style="color:#f1f1f1;"></i></button>
                            <?= HTML::a('<img src="'.Url::home().'images/icon/eye.png" class="pull-left" style="margin-top:5px;width:18px;">View',Url::home().'company/'.$company->id,['class' => 'btn', 'style' => 'font-size:12px;margin-top:10px;padding-top:0px; padding-bottom:0px;width:100%;background-color:#5ec6ed;color:white;']); ?>
                            <button class="btn" style="font-size:12px;padding-top:0px; padding-bottom:0px;margin-top:10px;width:100%;background-color:#525273;color:white;" ><img src="<?= Url::home(); ?>images/icon/ok.png" class="pull-left" style="margin-top:5px;width:12px;">Follow</button>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="col-sm-12">
                <?= LinkPager::widget(['pagination'=>$pagination]); ?>
            </div>
        </div>
    </div>
    <div class="col-sm-2" style='padding: 0px;background-color:#525274;position:relative;'>
        <div class="col-sm-12" style="padding:0px;position:fixed;">
            <div class="col-sm-12" style="padding:0px;background-color:#525274;min-height:100vh">
                <?= $this->render('right_menu',['role'=>$role]); ?>
            </div>
        </div>
    </div>
</div> 