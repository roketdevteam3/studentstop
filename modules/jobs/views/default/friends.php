<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use app\assets\BusinessAsset;
use app\widgets\UserjobsWidget;
use app\widgets\FavoritejobsWidget;

BusinessAsset::register($this);

use app\assets\ApplyjobsAsset;
ApplyjobsAsset::register($this);

//$this->registerCssFile('/css/search_block.css');
//$this->registerJsFile('/js/search_block.js');

$array_skills_user = explode(",", $userRezume->skills);

?>

	<div class="row">
		<?= $this->render('right_menu',['role'=>$role]); ?>
		<div class="jobs-content">
			<div class="container-fluid background-block">
				<div class="row">
					<?= $this->render('menu',['role'=>$role]); ?>
					<div class="col-sm-12">
                        <ul class="nav nav-tabs navigation-tabs no-icons" role="tablist">
                            <ul class="nav nav-tabs view-tabs">
                                <span class="choice">View</span>
                                <li>
                                    <a id="actionViewGrid" data-toggle="tab" class="grid-appearance" aria-hidden="true" href=""></a>
                                </li>
                                <li class="active">
                                    <a id="actionViewList" data-toggle="tab" class="list-appearance active" aria-hidden="true" href=""></a>
                                </li>
                            </ul>
                        </ul>
                        <div id="view_list_student" class="top-vacancies">
                          <?php foreach($users as $rezume){ ?>
                              <div class="top-v-item ">
                                  <div class="jobs-logo">
                                    <?php if($rezume->avatar != ''){ ?>
                                        <img class="img-responsive" src="<?= Url::home().'images/users_images/'.$rezume->avatar; ?>">
                                    <?php }else{ ?>
                                        <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                                    <?php } ?>
                                  </div>
                                  <div class="centerrer-block">
                                      <h4><?= $rezume->user['name'].' '.$rezume->user['surname']; ?></h4>
                                      <table class="jobs-skills">
                                          <tr>
                                              <td>Addres</td>
                                              <td> <?= $rezume->userinfo['address']; ?></td>
                                          </tr>
                                          <tr>
                                              <td>University</td>
                                              <td><?=$rezume->userinfo->university['name']?></td>
                                          </tr>
                                          <tr>
                                              <td>Industry</td>
                                              <td><?=$rezume->userinfo->industry['name']?></td>
                                          </tr>
                                          <tr>
                                              <td>Job function</td>
                                              <td><?=$rezume->userinfo->jobfunction['name']?></td>
                                          </tr>
                                          <tr>
                                              <td>GPA overall</td>
                                              <td><?=$rezume->overall_gpa?></td>
                                          </tr>
                                          <tr>
                                              <td>GPA current</td>
                                              <td><?=$rezume->current_gpa?></td>
                                          </tr>
                                          <tr>
                                              <td>Matches vacancies</td>
                                            <?php
                                            $matches_vacancies = \app\modules\jobs\models\Jobs::find()
                                              ->where(['company_id'=>$modelCompany->id,'industry_id'=>$rezume->userinfo['industry_id']])
                                              ->orWhere(['function_id'=>$rezume->userinfo['jobs_function_id']])
                                              ->orWhere([ 'major_id'=>$rezume->userinfo['major_id']])
                                              ->all();
                                            ?>
                                              <td><a onclick="$('#view_user_<?=$rezume->user_id?>').show();"><?=count($matches_vacancies)?></a></td>
                                          </tr>
                                          <tr>
                                              <td>Favorite company</td>
                                              <td><a onclick="$('#view_user_<?=$rezume->user_id?>').show();"><?=\app\modules\jobs\models\FavoriteCompany::find()->where(['user_id'=>$rezume->user_id])->count()?></a></td>
                                          </tr>
                                          <tr>
                                              <td>Favorite jobs</td>
                                              <td><a onclick="$('#view_user_<?=$rezume->user_id?>').show();"><?=\app\modules\jobs\models\Favoritejobs::find()->where(['user_id'=>$rezume->user_id])->count()?></a></td>
                                          </tr>
                                          <tr>
                                              <td>Apply</td>
                                              <td><a onclick="$('#view_user_<?=$rezume->user_id?>').show();"><?=\app\modules\jobs\models\Userjobs::find()->where(['user_id'=>$rezume->user_id])->count()?></a></td>
                                          </tr>
                                          <tr>
                                              <td>Offer</td>
                                              <td>
                                                  <a onclick="$('#view_user_<?=$rezume->user_id?>').show();">
                                                    <?=\app\modules\jobs\models\OfferJob::find()->where(['user_id'=>$rezume->user_id])->count()?>
                                                  </a>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td>About</td>
                                              <td> <?= substr($rezume['about'], 0, 60).'...'; ?></td>
                                          </tr>
                                          <tr>
                                              <td>Tags</td>
                                              <td>
                                                <?php $array_skills_user = explode(",", $rezume->skills);
                                                foreach ($array_skills_user as $value) {
                                                  echo '<span class="label label-default">'.$value.'</span> ';
                                                }
                                                ?>
                                              </td>
                                          </tr>
                                      </table>
                                  </div>
                                  <div class="btn-wrap text-right">
                                    <?=\app\widgets\MakeOfferWidget::widget(['user_id'=>$rezume->user_id,'offer_id'=>0])?>
                                      <button class="view-job startChat" data-sender-id="<?=Yii::$app->user->id?>" data-recipient-id="<?=$rezume->user_id?>">Chat</button>
                                    <?= HTML::a('View',Url::home().'rezume/'.$rezume->id,['class' => 'view-job']); ?>
                                    <?= \app\widgets\FavoriterezumeWidget::widget(['rezume_id'=>$rezume->id])?>
                                  </div>
                              </div>
                            <?php
                          }
                          ?>
                        </div>
                        <div id="view_grid_student" style="display: none" class="top-vacancies">
                          <?php foreach($users as $rezume){ ?>
                              <div class="top-v-item col-sm-4" style="padding: 10px">
                                  <div class="jobs-logo">
                                    <?php if($rezume->avatar != ''){ ?>
                                        <img class="img-responsive" src="<?= Url::home().'images/users_images/'.$rezume->avatar; ?>">
                                    <?php }else{ ?>
                                        <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                                    <?php } ?>
                                  </div>
                                  <div class="centerrer-block">
                                      <h4><?= $rezume->user['name'].' '.$rezume->user['surname']; ?></h4>
                                      <table class="jobs-skills">
                                          <tr>
                                              <td>Addres</td>
                                              <td> <?= $rezume->userinfo['address']; ?></td>
                                          </tr>
                                          <tr>
                                              <td>University</td>
                                              <td><?=$rezume->userinfo->university['name']?></td>
                                          </tr>
                                          <tr>
                                              <td>Industry</td>
                                              <td><?=$rezume->userinfo->industry['name']?></td>
                                          </tr>
                                          <tr>
                                              <td>Job function</td>
                                              <td><?=$rezume->userinfo->jobfunction['name']?></td>
                                          </tr>
                                          <tr>
                                              <td>GPA overall</td>
                                              <td><?=$rezume->overall_gpa?></td>
                                          </tr>
                                          <tr>
                                              <td>GPA current</td>
                                              <td><?=$rezume->current_gpa?></td>
                                          </tr>
                                          <tr>
                                              <td>Matches vacancies</td>
                                            <?php
                                            $matches_vacancies = \app\modules\jobs\models\Jobs::find()
                                              ->where(['company_id'=>$modelCompany->id,'industry_id'=>$rezume->userinfo['industry_id']])
                                              ->orWhere(['function_id'=>$rezume->userinfo['jobs_function_id']])
                                              ->orWhere([ 'major_id'=>$rezume->userinfo['major_id']])
                                              ->all();
                                            ?>
                                              <td><a onclick="$('#view_user_<?=$rezume->user_id?>').show();"><?=count($matches_vacancies)?></a></td>
                                          </tr>
                                          <tr>
                                              <td>Favorite company</td>
                                              <td><a onclick="$('#view_user_<?=$rezume->user_id?>').show();"><?=\app\modules\jobs\models\FavoriteCompany::find()->where(['user_id'=>$rezume->user_id])->count()?></a></td>
                                          </tr>
                                          <tr>
                                              <td>Favorite jobs</td>
                                              <td><a onclick="$('#view_user_<?=$rezume->user_id?>').show();"><?=\app\modules\jobs\models\Favoritejobs::find()->where(['user_id'=>$rezume->user_id])->count()?></a></td>
                                          </tr>
                                          <tr>
                                              <td>Apply</td>
                                              <td><a onclick="$('#view_user_<?=$rezume->user_id?>').show();"><?=\app\modules\jobs\models\Userjobs::find()->where(['user_id'=>$rezume->user_id])->count()?></a></td>
                                          </tr>
                                          <tr>
                                              <td>Offer</td>
                                              <td>
                                                  <a onclick="$('#view_user_<?=$rezume->user_id?>').show();">
                                                    <?=\app\modules\jobs\models\OfferJob::find()->where(['user_id'=>$rezume->user_id])->count()?>
                                                  </a>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td>About</td>
                                              <td> <?= substr($rezume['about'], 0, 60).'...'; ?></td>
                                          </tr>
                                          <tr>
                                              <td>Tags</td>
                                              <td>
                                                <?php $array_skills_user = explode(",", $rezume->skills);
                                                foreach ($array_skills_user as $value) {
                                                  echo '<span class="label label-default">'.$value.'</span> ';
                                                }
                                                ?>
                                              </td>
                                          </tr>
                                      </table>
                                  </div>
                                  <div class="btn-wrap text-right">
                                    <?=\app\widgets\MakeOfferWidget::widget(['user_id'=>$rezume->user_id,'offer_id'=>0])?>
                                      <button class="view-job startChat" data-sender-id="<?=Yii::$app->user->id?>" data-recipient-id="<?=$rezume->user_id?>">Chat</button>
                                    <?= HTML::a('View',Url::home().'rezume/'.$rezume->id,['class' => 'view-job']); ?>
                                    <?= \app\widgets\FavoriterezumeWidget::widget(['rezume_id'=>$rezume->id])?>
                                  </div>
                              </div>
                            <?php
                          }
                          ?>
                        </div>
                      <?php
                      echo \yii\widgets\LinkPager::widget([
                        'pagination' => $pages_users,
                      ]);
                      ?>

					</div>
				</div>
			</div>
		</div>
	</div>

<div class="search_block search-job-block" id="close_s">
	<div class="search_panel">
	  <?= \app\widgets\FilterjobsWidget::widget(['message' => ' Yii2.0','role'=>$role]) ?>
	</div>
</div>

<?php if($role != 'student' ){?>
	<div class="modal fade" id="offer_job" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
					<h4 class="modal-title" id="myModalLabel">Modal title</h4>
				</div>
			  <?php $form = ActiveForm::begin(); ?>
				<div class="modal-body">
					<input type="hidden" name="OfferJob[user_id]" value="" id="user_id_offer">
				  <?= $form->field($offerJob,'salary');?>
				  <?= $form->field($offerJob,'type')->dropDownList(['day'=>'day','week'=>'week','month'=>'month']);?>
				  <?= $form->field($offerJob,'job_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\modules\jobs\models\Jobs::findAll(['company_id'=>$modelCompany->id]),'id','title'));?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
					<button type="submit" name="offer_bt" class="btn btn-action">Save changes</button>
				</div>
			  <?php ActiveForm::end() ?>
			</div>
		</div>
	</div>


  <?php foreach ($users as $item){

    $offersUsers = \app\modules\jobs\models\OfferJob::findAll(['user_id'=>$item->id]);

    $followersUsers = \app\modules\jobs\models\FavoriteRezume::findAll(['rezume_id'=>$item->id]);

    $followCompany = \app\modules\jobs\models\FavoriteCompany::findAll(['user_id'=>$item->user_id]);

    $followJob = \app\modules\jobs\models\Favoritejobs::findAll(['user_id'=>$item->user_id]);

    $appliedUsers = \app\modules\jobs\models\Userjobs::findAll(['user_id'=>$item->user_id]);


    $matchesVacancies = \app\modules\jobs\models\Jobs::find()
      ->where(['company_id'=>$modelCompany->id,'industry_id'=>$item->userinfo['industry_id']])
      ->orWhere(['function_id'=>$item->userinfo['jobs_function_id']])
      ->orWhere([ 'major_id'=>$item->userinfo['major_id']])
      ->all();

    ?>
        <div class="modalRightPagesLS" id="view_user_<?=$item->user_id?>">

            <a class="btn btn-close pull-right closeModal" onclick="$('#view_user_<?=$item->user_id?>').hide();" >
                <span class="" aria-hidden="true">x</span>
            </a>
            <div class="user-info">
                <div class="class-user-av">
                  <?php if($item->avatar != ''){ ?>
                      <img class="img-responsive" src="<?= Url::home().'images/users_images/'.$item->avatar; ?>">
                  <?php }else{ ?>
                      <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                  <?php } ?>
                </div>
                <span class="user-name">
					<?= $item->user['name'].' '.$item->user['surname']; ?>
				</span>
                <div class="right-block">

                </div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-content">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#offers_user<?=$item->user_id?>" aria-controls="offers_user<?=$item->user_id?>" role="tab" data-toggle="tab">Offers</a></li>
                    <li role="presentation"><a href="#followers_user<?=$item->user_id?>" aria-controls="followers_user<?=$item->user_id?>" role="tab" data-toggle="tab">Followers</a></li>
                    <li role="presentation"><a href="#follow_user<?=$item->user_id?>" aria-controls="follow_user<?=$item->user_id?>" role="tab" data-toggle="tab">Follow</a></li>
                    <li role="presentation"><a href="#applied_user<?=$item->user_id?>" aria-controls="applied_user<?=$item->user_id?>" role="tab" data-toggle="tab">Applied</a></li>
                    <li role="presentation"><a href="#matches_vacancies<?=$item->user_id?>" aria-controls="matches_vacancies<?=$item->user_id?>" role="tab" data-toggle="tab">Matches vacancies</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="offers_user<?=$item->user_id?>">
                        <table class="table classBlock">
                            <thead>
                            <tr>
                                <td>Name jobs</td>
                                <td>Salaty</td>
                                <td>Type</td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($offersUsers as $value){?>
                                <tr>
                                    <td>
                                        <div class="class-user-av">
                                            <img class="" src="<?=$value->job['logo_src']?>">
                                        </div>
                                        <span class="user-name"><?=$value->job['title']?></span>
                                    </td>
                                    <td>
                                      <?=$value->salary?>
                                    </td>
                                    <td>
                                      <?=$value->type?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="followers_user<?=$item->user_id?>">
                        <table class="table classBlock">
                            <thead>
                            <tr>
                                <td>Name comapny</td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($followersUsers as $value){?>
                                <tr>
                                    <td>
                                        <div class="class-user-av">
                                            <img class="" src="<?=$value->user->company['logo_src']?>">
                                        </div>
                                        <span class="user-name"><?=$value->user->company['name']?></span>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="follow_user<?=$item->user_id?>">
                        <table class="table classBlock">
                            <thead>
                            <tr>
                                <td>Name comapny</td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($followCompany as $value){?>
                                <tr>
                                    <td>
                                        <div class="class-user-av">
                                            <img class="" src="<?=$value->company['logo_src']?>">
                                        </div>
                                        <span class="user-name"><?=$value->company['name']?></span>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <table class="table classBlock">
                            <thead>
                            <tr>
                                <td>Name jobs</td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($followJob as $value){?>
                                <tr>
                                    <td>
                                        <div class="class-user-av">
                                            <img class="" src="<?=$value->job['logo_src']?>">
                                        </div>
                                        <span class="user-name"><?=$value->job['title']?></span>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="applied_user<?=$item->user_id?>">
                        <table class="table classBlock">
                            <thead>
                            <tr>
                                <td>Name jobs</td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($appliedUsers as $value){?>
                                <tr>
                                    <td>
                                        <div class="class-user-av">
                                            <img class="" src="<?=$value->job['logo_src']?>">
                                        </div>
                                        <span class="user-name"><?=$value->job['title']?></span>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="matches_vacancies<?=$item->user_id?>">
                        <table class="table classBlock">
                            <thead>
                            <tr>
                                <td>Name vacansies</td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($matchesVacancies as $value){?>
                                <tr>
                                    <td>
                                        <div class="class-user-av">
                                            <img class="" src="<?=$value->logo_src?>">
                                        </div>
                                        <span class="user-name"><?=$value->title?></span>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
  <?php } ?>

<?php } ?>