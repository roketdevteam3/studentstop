<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\ActiveForm;
	use app\widgets\FilterjobsWidget;
	
	use app\assets\FilterAsset;
	FilterAsset::register($this);
	
	$allUrl = Yii::$app->request->url;
		
	if(strpos($allUrl, 'mycompany/jobs')){$class1 = 'active';}else{$class1 = '';}
	if(strpos($allUrl, 'mycompany/edit')){$class2 = 'active';}else{$class2 = '';}
	if(strpos($allUrl, 'addbusiness')){$class3 = 'active';}else{$class3 = '';}
	if(strpos($allUrl, 'dashboard/myapplyjob')){$class4 = 'active';}else{$class4 = '';}
	if(strpos($allUrl, 'dashboard/myalertjobs')){$class5 = 'active';}else{$class5 = '';}
	if(strpos($allUrl, 'jobs/favorite')){
			$class6 = 'active';
		}else{
			$class6 = '';
		}
	if(strpos($allUrl, 'searchjob')){$class7 = 'active';}else{$class7 = '';}
	if(strpos($allUrl, 'dashboard/myresume')){$class8 = 'active';}else{$class8 = '';}
?>
	<div class="jobs-right-panel">
		<nav class="menu-right">
			<ul class="">
				<li class="<?= $class4; ?>">
						<?= HTML::a('Dashboard',Url::home().'dashboard/job',['class' => ""]);?>
				</li>
				<?php if($role == 'company'){ ?>
							<!--	<li class="<?/*= $class3; */?>" >
										<?/*= HTML::a('Add company',Url::home().'addbusiness',['class' => ""]); */?>
								</li>
				--><?php /*}else{ */?>
								<li class="<?= $class2; ?>">
										<?= HTML::a('My company',Url::home().'mycompany/edit',['class' => ""]); ?>
								</li>
				<?php } ?>
				<?php if($role == 'company'){ ?>
						<li class="<?= $class1; ?>">
								<?= HTML::a('My vacancies',Url::home().'mycompany/jobs',['class' => ""]); ?>
						</li>
                    <li class="<?= $class1; ?>">
                      <?= HTML::a('My Followers',Url::home().'mycompany/friends',['class' => ""]); ?>
                    </li>
				<?php } ?>
				<?php /* ?>
				<li class="<?= $class5; ?>">
						<?= HTML::a('Message <span class="menu-counter active">1</span>',Url::home().'dashboard/myalertjobs',['class' => ""]); ?>
				</li>
				<?php */ ?>

				<li class="<?= $class7; ?>">
						<?= HTML::a('Search',Url::home().'searchjob',['class' => ""]); ?>
				</li>
              <?php if($role != 'company'){ ?>
				<li class="<?= $class8; ?>">
						<?= HTML::a('My resume',Url::home().'dashboard/myresume',['class' => ""]); ?>
				</li>
                <?php }?>
			</ul>
		</nav>

	</div>
