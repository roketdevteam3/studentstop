<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Alert;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\assets\JobAsset;
use app\widgets\UserjobsWidget;
use app\widgets\FavoritejobsWidget;
use app\assets\ApplyjobsAsset;

ApplyjobsAsset::register($this);
JobAsset::register($this);
?>
<div class="row">
	<?= $this->render('right_menu',['role'=>$role]); ?>
	<div class="jobs-content">
		<div class="container-fluid background-block">
			<div class="row">
				<?= $this->render('menu',['role'=>$role]); ?>
				<div class="col-sm-12">
					<div class="main-content">
						<div class="jobs-news">
							<h5>
								<span>
									<?= HTML::a($countJobsLastVisit,Url::home().'searchjob?last_visit='); ?>
								</span> 
								new jobs since alast visit
							</h5>
							<h5>
								<span>
									<?= HTML::a($countJobsTargetVisit,Url::home().'searchjob?target='); ?>
								</span> 
								jobs targered to your profile
							</h5>
						</div>
						<table class="table">
							<thead>
								<tr>
									<td>Job</td>
									<td>Company</td>
									<td>Area</td>
									<td>Expected Salary</td>
									<td>Data listed</td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							</thead>
							<tbody>
								<?php foreach($modelUserJobs as $jobs){?>
									<tr>
										<td><?= HTML::a($jobs['title'],Url::home().'job/'.$jobs['jobs_id']); ?></td>
										<td><?= HTML::a($jobs['name'],Url::home().'company/'.$jobs['company_id']); ?></td>
										<td><?= $jobs['address']; ?></td>
										<td><?= $jobs['salary']; ?>/Month</td>
										<td><?= $jobs['dateApply']; ?></td>
										<td>
											<?php if($jobs['user_id'] != \Yii::$app->user->id){ ?>
												<?= UserjobsWidget::widget(['jobs_id' => $jobs['jobs_id'],'page_type' => 'dashboard']) ?>
											<?php } ?>
										</td>
										<td><?= HTML::a('View',Url::home().'job/'.$jobs['jobs_id'],['class' => 'btn view-job']); ?></td>
									</tr>
								<?php }?>
							</tbody>
						</table>
	
						<h5 class="top-v-head"><span>TOP </span> vacancies</h5>
						<div class="top-vacancies">
							<?php foreach($modelVipJobs as $jobs){ ?>
								<div class="top-v-item">
									<!-- <div class="jobs-rating">
										<i class="fa fa-star-o"></i><br>
										<i class="jobs-rating-active fa fa-star"></i><br>
										<i class="jobs-rating-active fa fa-star"></i><br>
										<i class="jobs-rating-active fa fa-star"></i><br>
										<i class="jobs-rating-active fa fa-star"></i><br>
									</div> -->
									<div class="jobs-logo">
										<?php if($jobs['jobsLogo'] != ''){ ?>
											<img class="img-responsive" src="<?= Url::home().'images/jobs_logo/'.$jobs['jobsLogo']; ?>">
										<?php }else{ ?>
											<img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
										<?php } ?>
									</div>
									<div class="job-info">
										<h4><?= $jobs['title']; ?></h4>
										<p class="job-address"><i><?= $jobs['jobsAddress']; ?></i></p>
										<p class="job-desc"><?= substr($jobs['jobsAbout'], 0, 60).'...'; ?></p>
									</div>
									<div class="ex-salary">
										<h4>Expected salary</h4>
										<p><?= $jobs['salary']; ?>/month</p>
									</div>
									<div class="btn-wrap text-center">
										<?php if($jobs['user_id'] != \Yii::$app->user->id){ ?>
											<?= FavoritejobsWidget::widget(['jobs_id' => $jobs['id']]) ?>
										<?php } ?>
											<?= HTML::a('View',Url::home().'job/'.$jobs['id'],['class' => 'view-job']); ?>
										<?php if($jobs['user_id'] != \Yii::$app->user->id){ ?>
											<?= UserjobsWidget::widget(['jobs_id' => $jobs['id'],'page_type' => 'dashboard']) ?>
										<?php } ?>
									</div>
								</div>
							<?php }?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
