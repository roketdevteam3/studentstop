<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Alert;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\assets\JobAsset;
use app\widgets\UserjobsWidget;
use app\widgets\FavoritejobsWidget;
use app\assets\ApplyjobsAsset;

ApplyjobsAsset::register($this);
JobAsset::register($this);

$this->registerJsFile('/js/user_job_apply_statys.js');
?>
<?php if($role == 'student'){ ?>
    <script>
        window.role = 1;
    </script>
<?php }else{?>
    <script>
        window.role = 2;
    </script>
<?php } ?>
<div class="row">
    <?= $this->render('right_menu',['role'=>$role]); ?>
    <div class="jobs-content">
        <div class="container-fluid background-block">
            <div class="row">
                <?= $this->render('menu',['role'=>$role]); ?>
            </div>
            <div class="row">
                
                <?php if($role == 'student'){?>
                <div class="col-sm-6">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs navigation-tabs no-icons" role="tablist">
                        <li role="presentation" class="active"><a href="#matches" aria-controls="matches" role="tab" data-toggle="tab">Matches <span class="badge"><?=count($jobs)?>(<?=$countJobsLastVisit?>)</span></a></li>
                        <!--<li role="presentation" class=""><a href="#all" aria-controls="all" role="tab" data-toggle="tab">All <span class="badge"><?/*=count($jobsAll)*/?></span></a></li>-->
                        <li role="presentation"><a href="#favorite" aria-controls="favorite" role="tab" data-toggle="tab">Favorite <span class="badge"><?=count($jobs_favorites)?></span></a></li>
                        <li role="presentation"><a href="#companies" aria-controls="companies" role="tab" data-toggle="tab">Companies <span class="badge"><?=count($company_favorites)?></span></a></li>
                        <li role="presentation"><a href="#offers" aria-controls="offers" role="tab" data-toggle="tab">Offers <span class="badge"><?=count($offersUsers)?>(<?=\app\modules\jobs\models\OfferJob::find()->where(['user_id'=>Yii::$app->user->id, 'status'=>0])->count()?>)</span></a></li>
                        <li role="presentation"><a href="#applied" aria-controls="applied" role="tab" data-toggle="tab">Applied <span class="badge"><?=count($applyUsers)?></span></a></li>
                        <li role="presentation"><a href="#news" aria-controls="applied" role="tab" data-toggle="tab">News </a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="matches">
                            <div class="top-vacancies">
                                <?php foreach($jobs as $job){ ?>
                                    <div class="top-v-item column">
                                        <div class="jobs-logo" style="display: inline-block">
                                            <?php if($job->logo_src != ''){ ?>
                                                <img class="img-responsive"  style="display: inline-block; width: 60%" src="<?= Url::home().'images/jobs_logo/'.$job->logo_src; ?>">
                                            <?php }else{ ?>
                                                <img class="img-responsive" style="display: inline-block; width: 60%"  src="<?= Url::home(); ?>images/noavatar.png">
                                            <?php } ?>
                                            <div class="btn-wrap text-right">
                                                <?= HTML::a('View',Url::home().'job/'.$job->id,['class' => 'view-job']); ?>
                                                <?php if($job->company['user_id'] != \Yii::$app->user->id){ ?>
                                                    <?= UserjobsWidget::widget(['jobs_id' => $job->id,'page_type' => 'dashboard']) ?>
                                                <?php } ?>
                                                <?php if($job->company['user_id'] != \Yii::$app->user->id){ ?>
                                                    <?= FavoritejobsWidget::widget(['jobs_id' => $job->id]) ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <h4><?= $job->title; ?> <?=(\app\modules\jobs\models\JobView::findOne(['user_id'=>Yii::$app->user->id, 'job_id'=>$job->id])) ? '' : '<span class="label label-danger">New</span>'?></h4>
                                            <table class="jobs-skills">
                                                <tr>
                                                    <td>Addres</td>
                                                    <td><?= $job->address; ?></td>
                                                </tr>
                                                <tr <?=($job->university_id == $userRezume->userinfo['university_id'])?'style="color:green"':''?>>
                                                    <td>University</td>
                                                    <td><?= $job->university['name']; ?></td>
                                                </tr>
                                                <tr <?=($job->industry_id == $userRezume->userinfo['industry_id'])?'style="color:green"':''?>>
                                                    <td>Industry</td>
                                                    <td><?= $job->industry['name']; ?></td>
                                                </tr>
                                                <tr <?=($job->function_id == $userRezume->userinfo['jobs_function_id'])?'style="color:green"':''?>>
                                                    <td>Job function</td>
                                                    <td><?= $job->jobfunction['name']; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Favorite</td>
                                                    <td><?=\app\modules\jobs\models\Favoritejobs::find()->where(['jobs_id'=>$job->id])->count()?></td>
                                                </tr>
                                                <tr>
                                                    <td>Apply</td>
                                                    <td><?=\app\modules\jobs\models\Userjobs::find()->where(['jobs_id'=>$job->id])->count()?></td>
                                                </tr>
                                                <tr>
                                                    <td>About</td>
                                                    <td><?= substr($job->about, 0, 60).'...'; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Tags</td>
                                                    <td>
                                                        <?php $array_skills_vak = explode(",", $job->skills);
                                                        foreach ($array_skills_vak as $value) {
                                                            if (in_array($value, $array_skills_user)) {
                                                                echo '<span class="label label-success">'.$value.'</span> ';
                                                            } else
                                                                echo '<span class="label label-default">'.$value.'</span> ';
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="ex-salary">
                                            <h4>Expected salary</h4>
                                            <p><?= $job->salary; ?>/month</p>
                                        </div>
                                    </div>
                                <?php }?>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="all">
                            <div class="top-vacancies">
                                <?php foreach($jobsAll as $job){ ?>
                                    <div class="top-v-item">
                                        <div class="jobs-logo col-sm-5">
                                            <?php if($job->logo_src != ''){ ?>
                                                <img class="img-responsive" src="<?= Url::home().'images/jobs_logo/'.$job->logo_src; ?>">
                                            <?php }else{ ?>
                                                <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                                            <?php } ?>
                                        </div>
                                        <div class="col-sm-5">
                                            <h4><?= $job->title; ?></h4>
                                            <table class="jobs-skills">
                                                <tr>
                                                    <td>Addres</td>
                                                    <td><?= $job->address; ?></td>
                                                </tr>
                                                <tr <?=($job->university_id == $userRezume->userinfo['university_id'])?'style="color:green"':''?>>
                                                    <td>University</td>
                                                    <td><?= $job->university['name']; ?></td>
                                                </tr>
                                                <tr <?=($job->industry_id == $userRezume->userinfo['industry_id'])?'style="color:green"':''?>>
                                                    <td>Industry</td>
                                                    <td><?= $job->industry['name']; ?></td>
                                                </tr>
                                                <tr <?=($job->function_id == $userRezume->userinfo['jobs_function_id'])?'style="color:green"':''?>>
                                                    <td>Job function</td>
                                                    <td><?= $job->jobfunction['name']; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Favorite</td>
                                                    <td><?=\app\modules\jobs\models\Favoritejobs::find()->where(['jobs_id'=>$job->id])->count()?></td>
                                                </tr>
                                                <tr>
                                                    <td>Apply</td>
                                                    <td><?=\app\modules\jobs\models\Userjobs::find()->where(['jobs_id'=>$job->id])->count()?></td>
                                                </tr>
                                                <tr>
                                                    <td>About</td>
                                                    <td><?= substr($job->about, 0, 60).'...'; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Tags</td>
                                                    <td>
                                                        <?php $array_skills_vak = explode(",", $job->skills);
                                                        foreach ($array_skills_vak as $value) {
                                                            if (in_array($value, $array_skills_user)) {
                                                                echo '<span class="label label-success">'.$value.'</span> ';
                                                            } else
                                                                echo '<span class="label label-default">'.$value.'</span> ';
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="ex-salary">
                                            <h4>Expected salary</h4>
                                            <p><?= $job->salary; ?>/month</p>
                                        </div>
                                        <div class="btn-wrap text-right">
                                            <?= HTML::a('View',Url::home().'job/'.$job->id,['class' => 'view-job']); ?>
                                            <?php if($job->company['user_id'] != \Yii::$app->user->id){ ?>
                                                <?= UserjobsWidget::widget(['jobs_id' => $job->id,'page_type' => 'dashboard']) ?>
                                            <?php } ?>
                                            <?php if($job->company['user_id'] != \Yii::$app->user->id){ ?>
                                                <?= FavoritejobsWidget::widget(['jobs_id' => $job->id]) ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php }?>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="favorite">
                            <div class="top-vacancies">
                                <?php foreach($jobs_favorites as $job){ ?>
                                    <div class="top-v-item">
                                        <div class="jobs-logo col-sm-4">
                                            <?php if($job->job['logo_src'] != ''){ ?>
                                                <img class="img-responsive" src="<?= Url::home().'images/jobs_logo/'.$job->job['logo_src']; ?>">
                                            <?php }else{ ?>
                                                <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                                            <?php } ?>
                                        </div>
                                        <div class="col-sm-5">
                                            <h4><?= $job->job['title']; ?></h4>
                                            <table class="jobs-skills">
                                                <tr>
                                                    <td>Addres</td>
                                                    <td><?= $job->job['address']; ?></td>
                                                </tr>
                                                <tr <?=($job->job['university_id'] == $userRezume->userinfo['university_id'])?'style="color:green"':''?>>
                                                    <td>University</td>
                                                    <td><?= $job->job->university['name']; ?></td>
                                                </tr>
                                                <tr <?=($job->job['industry_id'] == $userRezume->userinfo['industry_id'])?'style="color:green"':''?>>
                                                    <td>Industry</td>
                                                    <td><?= $job->job->industry['name']; ?></td>
                                                </tr>
                                                <tr <?=($job->job['function_id'] == $userRezume->userinfo['jobs_function_id'])?'style="color:green"':''?>>
                                                    <td>Job function</td>
                                                    <td><?= $job->job->jobfunction['name']; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Favorite</td>
                                                    <td><?=\app\modules\jobs\models\Favoritejobs::find()->where(['jobs_id'=>$job->jobs_id])->count()?></td>
                                                </tr>
                                                <tr>
                                                    <td>Apply</td>
                                                    <td><?=\app\modules\jobs\models\Userjobs::find()->where(['jobs_id'=>$job->jobs_id])->count()?></td>
                                                </tr>
                                                <tr>
                                                    <td>About</td>
                                                    <td><?= substr($job->job['about'], 0, 60).'...'; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Tags</td>
                                                    <td>
                                                        <?php $array_skills_vak = explode(",", $job->job['skills']);
                                                        foreach ($array_skills_vak as $value) {
                                                            echo '<span class="label label-default">'.$value.'</span> ';
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="ex-salary">
                                            <h4>Expected salary</h4>
                                            <p><?= $job->job->salary; ?>/month</p>
                                        </div>
                                        <div class="btn-wrap text-right">
                                            <?= HTML::a('View',Url::home().'job/'.$job->job['id'],['class' => 'view-job']); ?>
                                            <?= UserjobsWidget::widget(['jobs_id' => $job->job['id'],'page_type' => 'dashboard']) ?>
                                            <?= FavoritejobsWidget::widget(['jobs_id' => $job->job['id']]) ?>
                                        </div>
                                    </div>
                                <?php }?>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="companies">
                            <div class="top-vacancies">
                                <?php
                                foreach($company_favorites as $company){
                                    ?>
                                    <div class="top-v-item">
                                        <div class="jobs-logo">
                                            <?php if($company->company['logo_src'] != ''){ ?>
                                                <img class="img-responsive" src="<?= Url::home().'images/company_logo/'.$company->company['logo_src']; ?>">
                                            <?php }else{ ?>
                                                <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                                            <?php } ?>
                                        </div>
                                        <div class="col-sm-5">
                                            <h4><?= $company->company['name']; ?></h4>
                                            <table class="jobs-skills">
                                                <tr>
                                                    <td>Addres</td>
                                                    <td> <?= $company->company['address']; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Industrial</td>
                                                    <td><?=$company->company->industrial['name']?></td>
                                                </tr>
                                                <tr>
                                                    <td>Jobs</td>
                                                    <td><?=\app\modules\jobs\models\Jobs::find()->where(['company_id'=>$company->company_id])->count()?></td>
                                                </tr>
                                                <tr>
                                                    <td>Favorite</td>
                                                    <td><?=\app\modules\jobs\models\FavoriteCompany::find()->where(['company_id'=>$company->company_id])->count()?></td>
                                                </tr>
                                                <tr>
                                                    <td>About</td>
                                                    <td> <?= substr($company->company['about'], 0, 60).'...'; ?></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="btn-wrap text-right">
                                            <?= \app\widgets\FavoritecompanyWidget::widget(['company_id'=>$company->company['id']])?>
                                            <?= HTML::a('View',Url::home().'company_view/'.$company->company['id'],['class' => 'view-job']); ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="offers">
                            <div class="top-vacancies">
                                <?php foreach($offersUsers as $job){ ?>
                                    <div class="top-v-item">
                                        <div class="jobs-logo">
                                            <?php if($job->job['logo_src'] != ''){ ?>
                                                <img class="img-responsive" src="<?= Url::home().'images/jobs_logo/'.$job->job['logo_src']; ?>">
                                            <?php }else{ ?>
                                                <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                                            <?php } ?>
                                        </div>
                                        <div class="col-sm-5">
                                            <h4><?= $job->job['title']; ?></h4>
                                            <table class="jobs-skills">
                                                <tr>
                                                    <td>Addres</td>
                                                    <td><?= $job->job['address']; ?></td>
                                                </tr>
                                                <tr <?=($job->job['university_id'] == $userRezume->userinfo['university_id'])?'style="color:green"':''?>>
                                                    <td>University</td>
                                                    <td><?= $job->job->university['name']; ?></td>
                                                </tr>
                                                <tr <?=($job->job['industry_id'] == $userRezume->userinfo['industry_id'])?'style="color:green"':''?>>
                                                    <td>Industry</td>
                                                    <td><?= $job->job->industry['name']; ?></td>
                                                </tr>
                                                <tr <?=($job->job['function_id'] == $userRezume->userinfo['jobs_function_id'])?'style="color:green"':''?>>
                                                    <td>Job function</td>
                                                    <td><?= $job->job->jobfunction['name']; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Favorite</td>
                                                    <td><?=\app\modules\jobs\models\Favoritejobs::find()->where(['jobs_id'=>$job->job_id])->count()?></td>
                                                </tr>
                                                <tr>
                                                    <td>Apply</td>
                                                    <td><?=\app\modules\jobs\models\Userjobs::find()->where(['jobs_id'=>$job->job_id])->count()?></td>
                                                </tr>
                                                <tr>
                                                    <td>About</td>
                                                    <td><?= substr($job->job['about'], 0, 60).'...'; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Tags</td>
                                                    <td>
                                                        <?php $array_skills_vak = explode(",", $job->job['skills']);
                                                        foreach ($array_skills_vak as $value) {
                                                            echo '<span class="label label-default">'.$value.'</span> ';
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="ex-salary">
                                            <h4>Offer salary</h4>
                                            <p><?= $job->salary; ?>/<?= $job->type; ?></p>
                                        </div>
                                        <div class="btn-wrap text-right">
                                            <?= HTML::a('View',Url::home().'job/'.$job->job['id'],['class' => 'view-job']); ?>
                                            <?= FavoritejobsWidget::widget(['jobs_id' => $job->job['id']]) ?>
                                        </div>
                                    </div>
                                <?php }?>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="applied">
                            <div class="top-vacancies">
                                <?php foreach($applyUsers as $job){ ?>
                                    <div class="top-v-item">
                                        <div class="jobs-logo">
                                            <?php if($job->job['logo_src'] != ''){ ?>
                                                <img class="img-responsive" src="<?= Url::home().'images/jobs_logo/'.$job->job['logo_src']; ?>">
                                            <?php }else{ ?>
                                                <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                                            <?php } ?>
                                        </div>
                                        <div class="col-sm-5">
                                            <h4><?= $job->job['title']; ?></h4>
                                            <table class="jobs-skills">
                                                <tr>
                                                    <td>Addres</td>
                                                    <td><?= $job->job['address']; ?></td>
                                                </tr>
                                                <tr <?=($job->job['university_id'] == $userRezume->userinfo['university_id'])?'style="color:green"':''?>>
                                                    <td>University</td>
                                                    <td><?= $job->job->university['name']; ?></td>
                                                </tr>
                                                <tr <?=($job->job['industry_id'] == $userRezume->userinfo['industry_id'])?'style="color:green"':''?>>
                                                    <td>Industry</td>
                                                    <td><?= $job->job->industry['name']; ?></td>
                                                </tr>
                                                <tr <?=($job->job['function_id'] == $userRezume->userinfo['jobs_function_id'])?'style="color:green"':''?>>
                                                    <td>Job function</td>
                                                    <td><?= $job->job->jobfunction['name']; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Favorite</td>
                                                    <td><?=\app\modules\jobs\models\Favoritejobs::find()->where(['jobs_id'=>$job->jobs_id])->count()?></td>
                                                </tr>
                                                <tr>
                                                    <td>Apply</td>
                                                    <td><?=\app\modules\jobs\models\Userjobs::find()->where(['jobs_id'=>$job->jobs_id])->count()?></td>
                                                </tr>
                                                <tr>
                                                    <td>About</td>
                                                    <td><?= substr($job->job['about'], 0, 60).'...'; ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Tags</td>
                                                    <td>
                                                        <?php $array_skills_vak = explode(",", $job->job['skills']);
                                                        foreach ($array_skills_vak as $value) {
                                                            echo '<span class="label label-default">'.$value.'</span> ';
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="ex-salary">
                                            <h4>Expected salary</h4>
                                            <p><?= $job->job->salary; ?>/month</p>
                                        </div>
                                        <div class="btn-wrap text-right">
                                            <?= HTML::a('View',Url::home().'job/'.$job->job['id'],['class' => 'view-job']); ?>
                                            <?= UserjobsWidget::widget(['jobs_id' => $job->job['id'],'page_type' => 'dashboard']) ?>
                                            <?= FavoritejobsWidget::widget(['jobs_id' => $job->job['id']]) ?>
                                        </div>
                                    </div>
                                <?php }?>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane" id="news">
                            <div class="top-vacancies">

                                <?php foreach ($news as $item){?>
                                <div class="top-v-item university-content">
                                    <div class="post-header row">
                                        <div class="user-img">
                                            <img src="/images/company_logo/<?=$item->company['logo_src']?>" style="width:100%;position:relative;z-index:6;">
                                        </div>
                                        <div class="post-header-info">
                                            <h4 class="post-author"><?= $item->company['name'] ?></h4>
                                            <h6 class="post-date"> <?=Yii::$app->formatter->asDatetime($item->created_at)?></h6>
                                        </div>
                                        <div class="post-like-comment-count">
                                                        <span class="count like-count clickLike likePost33" onclick="like_new(<?=$item->id?>)" data-post_id="33">
                                                            <img src="../../../images/icon/like-ic.png"><span id="like_count" data-count="<?=$item->like?>"><?=$item->like?></span>
                                                        </span>
                                            <span class="count post-count">
                                                            <img src="../../../images/icon/comment-ic.png"><?=\app\modules\jobs\models\CommitNewsCompany::find()->where(['new_id'=>$item->id])->count()?>
                                                        </span>
                                            <span class="count views-count viewsCount33">
                                                            <img src="../../../images/icon/views-count-ic.png"><span>1</span>
                                                        </span>
                                            <!--                                                        <span class="postUpdate"  data-post_id="33"><i class="fa fa-edit"></i></span>-->
                                        </div>
                                        <?=$item->desc?>
                                        <?php
                                        $commits = \app\modules\jobs\models\CommitNewsCompany::find()->where(['new_id'=>$item->id])->orderBy(['id'=>SORT_DESC])->all();
                                        ?>
                                        <div>
                                            <?php foreach ($commits as $commit){?>
                                                <div class="row">
                                                    <div class="">
                                                        <div class="user-img">
                                                            <img style="width:40px;" class="" src="/images/users_images/<?=$commit->userinfo['avatar']?>">
                                                        </div>
                                                        <span class="user-name"><?=$commit->user['surname']?> <?=$commit->user['name']?></span>
                                                        <span><?=Yii::$app->formatter->asDatetime($commit->created_at)?></span>
                                                    </div>
                                                    <div class="">
                                                        <?=$commit->commit?>
                                                    </div>
                                                </div>
                                            <?php }?>
                                        </div>
                                        <?php $form2 = ActiveForm::begin();?>
                                        <div class="col-sm-8">
                                            <?= $form2->field($commitNew,'commit')->label(false)?>
                                            <?= $form2->field($commitNew,'new_id')->hiddenInput(['value'=>$item->id])->label(false)?>
                                        </div>
                                        <div class="col-sm-4">
                                            <button type="submit" name="commit_send" class="btn btn-create btn-ripple">Add</button>
                                        </div>
                                        <?php ActiveForm::end();?>
                                    </div>

                                    <?php } ?>
                                    <?php
                                    echo \yii\widgets\LinkPager::widget([
                                        'pagination' => $pages_news,
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php }else{?>
                        <div class="col-sm-6">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs navigation-tabs no-icons" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#matches" aria-controls="matches" role="tab" data-toggle="tab">
                                        Matches <span class="badge"><?=count($matches)?></span>
                                    </a></li>
                                <li role="presentation">
                                    <a href="#favorite" aria-controls="favorite" role="tab" data-toggle="tab">
                                        Favorite <span class="badge"><?=count($rezume_favorites)?></span>
                                    </a>
                                </li>
                                <!--<li role="presentation" >
									<a href="#all" aria-controls="all" role="tab" data-toggle="tab">
										All <span class="badge"><?/*=count($rezumes)*/?></span>
									</a>
								</li>-->
                                <li role="presentation" >
                                    <a href="#offers" aria-controls="offers" role="tab" data-toggle="tab">
                                        Offers <span class="badge"><?=count($offersUsers)?></span>
                                    </a>
                                </li>
                                <li role="presentation" >
                                    <a href="#apply" aria-controls="apply" role="tab" data-toggle="tab">
                                        Apply <span class="badge"><?=count($applyUsers)?></span> <span class="badge"><?=$applyUsersNew?></span>
                                    </a>
                                </li>
                                <li role="presentation" >
                                    <a href="#news" aria-controls="news" role="tab" data-toggle="tab">
                                        News
                                    </a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="matches">
                                    <div class="top-vacancies">
                                        <?php if($matches){ ?>
                                        <?php foreach($matches as $item){ ?>
                                            <div class="top-v-item" style="padding-top: 0px; padding-left: 0px;    margin-top: 10px; height: 420px;">
                                                <div class=" col-sm-5" style="padding: 0px;">
                                                <div class="jobs-logo ">
                                                    <?php if($item->avatar != ''){ ?>
                                                        <img class="img-responsive" src="<?= Url::home().'images/users_images/'.$item->avatar; ?>">
                                                    <?php }else{ ?>
                                                        <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                                                    <?php } ?>
                                                </div>
                                                    <div class="btn-wrap text-right" style="   float: left; text-align: center;   position: relative; top: 15px;">
                                                        <?=\app\widgets\MakeOfferWidget::widget(['user_id'=>$item->user_id,'offer_id'=>0])?>
                                                        <button class="view-job startChat" data-sender-id="<?=Yii::$app->user->id?>" data-recipient-id="<?=$item->user_id?>">Chat</button>
                                                        <?= HTML::a('View',Url::home().'rezume/'.$item->id,['class' => 'view-job']); ?>
                                                    </div>
                                                </div>
                                                <div class="centerrer-block col-xs-7">
                                                    <h4 style="display: inline-block"><?= $item->user['name'].' '.$item->user['surname']; ?></h4>
                                                    <?= \app\widgets\FavoriterezumeWidget::widget(['rezume_id'=>$item->id, 'job_id'=>0])?>
                                                    <?=\app\widgets\rating\UserratingWidget::widget(['user_id'=>$item->user_id])?>
                                                    <table class="jobs-skills">
                                                        <tr>
                                                            <td>Addres</td>
                                                            <td> <?= $item->userinfo['address']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>University</td>
                                                            <td><?=$item->userinfo->university['name']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Industry</td>
                                                            <td><?=$item->userinfo->industry['name']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Job function</td>
                                                            <td><?=$item->userinfo->jobfunction['name']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>GPA overall</td>
                                                            <td><?=$item->overall_gpa?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>GPA current</td>
                                                            <td><?=$item->current_gpa?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Matches vacancies</td>
                                                            <?php
                                                            $matches_vacancies = \app\modules\jobs\models\Jobs::find()
                                                                ->where(['company_id'=>$modelCompany->id,'industry_id'=>$item->userinfo['industry_id']])
                                                                ->orWhere(['function_id'=>$item->userinfo['jobs_function_id']])
                                                                ->orWhere([ 'major_id'=>$item->userinfo['major_id']])
                                                                ->all();
                                                            ?>
                                                            <td><a onclick="$('#view_user_<?=$item->user_id?>').show();"><?=count($matches_vacancies)?></a></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Favorite company</td>
                                                            <td>
                                                                <a onclick="$('#view_user_<?=$item->user_id?>').show();"><?=\app\modules\jobs\models\FavoriteCompany::find()->where(['user_id'=>$item->user_id])->count()?></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Favorite jobs</td>
                                                            <td>
                                                                <a onclick="$('#view_user_<?=$item->user_id?>').show();"><?=\app\modules\jobs\models\Favoritejobs::find()->where(['user_id'=>$item->user_id])->count()?></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Apply</td>
                                                            <td>
                                                                <a onclick="$('#view_user_<?=$item->user_id?>').show();">
                                                                    <?=\app\modules\jobs\models\Userjobs::find()->where(['user_id'=>$item->user_id])->count()?>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Offer</td>
                                                            <td>
                                                                <a onclick="$('#view_user_<?=$item->user_id?>').show();">
                                                                    <?=\app\modules\jobs\models\OfferJob::find()->where(['user_id'=>$item->user_id])->count()?>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>About</td>
                                                            <td> <?= substr($item->about, 0, 60).'...'; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tags</td>
                                                            <td>
                                                                <?php $array_skills_user = explode(",", $item->skills);
                                                                foreach ($array_skills_user as $value) {
                                                                    echo '<span class="label label-default">'.$value.'</span> ';
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>

                                            </div>
                                        <?php }?>
                                        <?php }?>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="all">
                                    <div class="top-vacancies">
                                        <?php if($rezumes){; ?>
                                        <?php foreach($rezumes as $item){ ?>
                                            <div class="top-v-item">
                                                <div class="jobs-logo col-sm-4">
                                                    <?php if($item->avatar != ''){ ?>
                                                        <img class="img-responsive" src="<?= Url::home().'images/users_images/'.$item->avatar; ?>">
                                                    <?php }else{ ?>
                                                        <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                                                    <?php } ?>
                                                </div>
                                                <div class="col-sm-5">
                                                    <h4><?= $item->user['name'].' '.$item->user['surname']; ?></h4>
                                                    <?=\app\widgets\rating\UserratingWidget::widget(['user_id'=>$item->user_id])?>
                                                    <table class="jobs-skills">
                                                        <tr>
                                                            <td>Addres</td>
                                                            <td> <?= $item->userinfo['address']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>University</td>
                                                            <td><?=$item->userinfo->university['name']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Industry</td>
                                                            <td><?=$item->userinfo->industry['name']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Job function</td>
                                                            <td><?=$item->userinfo->jobfunction['name']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>GPA overall</td>
                                                            <td><?=$item->overall_gpa?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>GPA current</td>
                                                            <td><?=$item->current_gpa?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Matches vacancies</td>
                                                            <?php
                                                            $matches_vacancies = \app\modules\jobs\models\Jobs::find()
                                                                ->where(['company_id'=>$modelCompany->id,'industry_id'=>$item->userinfo['industry_id']])
                                                                ->orWhere(['function_id'=>$item->userinfo['jobs_function_id']])
                                                                ->orWhere([ 'major_id'=>$item->userinfo['major_id']])
                                                                ->all();
                                                            ?>
                                                            <td><a onclick="$('#view_user_<?=$item->user_id?>').show();"><?=count($matches_vacancies)?></a></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Favorite company</td>
                                                            <td>
                                                                <a onclick="$('#view_user_<?=$item->user_id?>').show();"><?=\app\modules\jobs\models\FavoriteCompany::find()->where(['user_id'=>$item->user_id])->count()?></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Favorite jobs</td>
                                                            <td>
                                                                <a onclick="$('#view_user_<?=$item->user_id?>').show();"><?=\app\modules\jobs\models\Favoritejobs::find()->where(['user_id'=>$item->user_id])->count()?></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Apply</td>
                                                            <td>
                                                                <a onclick="$('#view_user_<?=$item->user_id?>').show();">
                                                                    <?=\app\modules\jobs\models\Userjobs::find()->where(['user_id'=>$item->user_id])->count()?>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Offer</td>
                                                            <td>
                                                                <a onclick="$('#view_user_<?=$item->user_id?>').show();">
                                                                    <?=\app\modules\jobs\models\OfferJob::find()->where(['user_id'=>$item->user_id])->count()?>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>About</td>
                                                            <td> <?= substr($item->about, 0, 60).'...'; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tags</td>
                                                            <td>
                                                                <?php $array_skills_user = explode(",", $item->skills);
                                                                foreach ($array_skills_user as $value) {
                                                                    echo '<span class="label label-default">'.$value.'</span> ';
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col-sm-3">
                                                    <?=\app\widgets\MakeOfferWidget::widget(['user_id'=>$item->user_id,'offer_id'=>0])?>
                                                    <button class="view-job startChat" data-sender-id="<?=Yii::$app->user->id?>" data-recipient-id="<?=$item->user_id?>">Chat</button>

                                                    <?= HTML::a('View',Url::home().'rezume/'.$item->id,['class' => 'view-job']); ?>
                                                    <?= \app\widgets\FavoriterezumeWidget::widget(['rezume_id'=>$item->id, 'job_id'=>0])?>
                                                </div>
                                            </div>
                                        <?php }?>
                                    <?php }?>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="offers">
                                    <div class="top-vacancies">
                                        <?php if($offersUsers){ ?>
                                        <?php foreach($offersUsers as $item){ ?>
                                            <div class="top-v-item">
                                                <div class="jobs-logo col-sm-4">
                                                    <?php if($item->avatar != ''){ ?>
                                                        <img class="img-responsive" src="<?= Url::home().'images/users_images/'.$item->avatar; ?>">
                                                    <?php }else{ ?>
                                                        <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                                                    <?php } ?>
                                                </div>
                                                <div class="col-sm-5">
                                                    <h4><?= $item->user['name'].' '.$item->user['surname']; ?></h4>
                                                    <?=\app\widgets\rating\UserratingWidget::widget(['user_id'=>$item->user_id])?>
                                                    <table class="jobs-skills">
                                                        <tr>
                                                            <td>Addres</td>
                                                            <td> <?= $item->userinfo['address']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>University</td>
                                                            <td><?=$item->userinfo->university['name']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Industry</td>
                                                            <td><?=$item->userinfo->industry['name']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Job function</td>
                                                            <td><?=$item->userinfo->jobfunction['name']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>GPA overall</td>
                                                            <td><?=$item->overall_gpa?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>GPA current</td>
                                                            <td><?=$item->current_gpa?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Matches vacancies</td>
                                                            <?php
                                                            $matches_vacancies = \app\modules\jobs\models\Jobs::find()
                                                                ->where(['company_id'=>$modelCompany->id,'industry_id'=>$item->userinfo['industry_id']])
                                                                ->orWhere(['function_id'=>$item->userinfo['jobs_function_id']])
                                                                ->orWhere([ 'major_id'=>$item->userinfo['major_id']])
                                                                ->all();
                                                            ?>
                                                            <td><a onclick="$('#view_user_<?=$item->user_id?>').show();"><?=count($matches_vacancies)?></a></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Favorite company</td>
                                                            <td>
                                                                <a onclick="$('#view_user_<?=$item->user_id?>').show();"><?=\app\modules\jobs\models\FavoriteCompany::find()->where(['user_id'=>$item->user_id])->count()?></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Favorite jobs</td>
                                                            <td>
                                                                <a onclick="$('#view_user_<?=$item->user_id?>').show();"><?=\app\modules\jobs\models\Favoritejobs::find()->where(['user_id'=>$item->user_id])->count()?></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Apply</td>
                                                            <td>
                                                                <a onclick="$('#view_user_<?=$item->user_id?>').show();">
                                                                    <?=\app\modules\jobs\models\Userjobs::find()->where(['user_id'=>$item->user_id])->count()?>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Offer</td>
                                                            <td>
                                                                <a onclick="$('#view_user_<?=$item->user_id?>').show();">
                                                                    <?=\app\modules\jobs\models\OfferJob::find()->where(['user_id'=>$item->user_id])->count()?>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>About</td>
                                                            <td> <?= substr($item->about, 0, 60).'...'; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tags</td>
                                                            <td>
                                                                <?php $array_skills_user = explode(",", $item->skills);
                                                                foreach ($array_skills_user as $value) {
                                                                    echo '<span class="label label-default">'.$value.'</span> ';
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col-sm-3">
                                                    <?=\app\widgets\MakeOfferWidget::widget(['user_id'=>$item->user_id,'offer_id'=>$item->id])?>
                                                    <button class="view-job startChat" data-sender-id="<?=Yii::$app->user->id?>" data-recipient-id="<?=$item->user_id?>">Chat</button>

                                                    <?= HTML::a('View',Url::home().'rezume/'.$item->id,['class' => 'view-job']); ?>
                                                    <?= \app\widgets\FavoriterezumeWidget::widget(['rezume_id'=>$item->id, 'job_id'=>0])?>
                                                </div>
                                            </div>
                                        <?php }?>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="favorite">
                                    <div class="top-vacancies">
                                        <?php if($rezume_favorites){ ?>
                                        <?php foreach($rezume_favorites as $item){?>
                                            <div class="top-v-item btn-xl-hide">
                                                <div class="jobs-logo col-sm-4">
                                                    <?php if($item->rezume['avatar'] != ''){ ?>
                                                        <img class="img-responsive" src="<?= Url::home().'images/users_images/'.$item->rezume['avatar']; ?>">
                                                    <?php }else{ ?>
                                                        <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                                                    <?php } ?>
                                                </div>
                                                <div class="col-sm-5">
                                                    <h4><?= $item->rezume->user['name'].' '.$item->rezume->user['surname']; ?></h4>
                                                    <?=\app\widgets\rating\UserratingWidget::widget(['user_id'=>$item->rezume['user_id']])?>
                                                    <table class="jobs-skills">
                                                        <tr>
                                                            <td>Addres</td>
                                                            <td> <?= $item->rezume->userinfo['address']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>University</td>
                                                            <td><?=$item->rezume->userinfo->university['name']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Industry</td>
                                                            <td><?=$item->userinfo->industry['name']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Job function</td>
                                                            <td><?=$item->userinfo->jobfunction['name']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>GPA overall</td>
                                                            <td><?=$item->rezume['overall_gpa']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>GPA current</td>
                                                            <td><?=$item->rezume['current_gpa']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Matches vacancies</td>
                                                            <?php
                                                            $matches_vacancies = \app\modules\jobs\models\Jobs::find()
                                                                ->where(['company_id'=>$modelCompany->id,'industry_id'=>$item->userinfo['industry_id']])
                                                                ->orWhere(['function_id'=>$item->userinfo['jobs_function_id']])
                                                                ->orWhere([ 'major_id'=>$item->userinfo['major_id']])
                                                                ->all();
                                                            ?>
                                                            <td><a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();"><?=count($matches_vacancies)?></a></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Favorite company</td>
                                                            <td>
                                                                <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();"><?=\app\modules\jobs\models\FavoriteCompany::find()->where(['user_id'=>$item->rezume['user_id']])->count()?></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Favorite jobs</td>
                                                            <td>
                                                                <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();"><?=\app\modules\jobs\models\Favoritejobs::find()->where(['user_id'=>$item->rezume['user_id']])->count()?></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Apply</td>
                                                            <td>
                                                                <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();">
                                                                    <?=\app\modules\jobs\models\Userjobs::find()->where(['user_id'=>$item->rezume['user_id']])->count()?>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Offer</td>
                                                            <td>
                                                                <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();">
                                                                    <?=\app\modules\jobs\models\OfferJob::find()->where(['user_id'=>$item->rezume['user_id']])->count()?>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>About</td>
                                                            <td> <?= substr($item->rezume['about'], 0, 60).'...'; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tags</td>
                                                            <td>
                                                                <?php $array_skills_user = explode(",", $item->rezume['skills']);
                                                                foreach ($array_skills_user as $value) {
                                                                    echo '<span class="label label-default">'.$value.'</span> ';
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col-sm-3">
                                                    <?=\app\widgets\MakeOfferWidget::widget(['user_id'=>$item->user_id,'offer_id'=>0])?>
                                                    <button class="view-job startChat" data-sender-id="<?=Yii::$app->user->id?>" data-recipient-id="<?=$item->rezume['user_id']?>">Chat</button>

                                                    <?= HTML::a('View',Url::home().'rezume/'.$item->rezume['id'],['class' => 'view-job']); ?>
                                                    <?= \app\widgets\FavoriterezumeWidget::widget(['rezume_id'=>$item->rezume['id'], 'job_id'=>0])?>
                                                </div>
                                            </div>
                                            <div class="top-v-item no-padding-style-m btn-lg-hide">
                                                <div class="col-sm-4 no-padding-style">
                                                    <div class="jobs-logo dashboard-logo-div-style">
                                                        <?php if($item->rezume['avatar'] != ''){ ?>
                                                            <img class="img-responsive" src="<?= Url::home().'images/users_images/'.$item->rezume['avatar']; ?>">
                                                        <?php }else{ ?>
                                                            <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                                                        <?php } ?>
                                                    </div>
                                                    <div class="jobs-logo btn-lg-hide search-jobs-center-margin dashboard-btn-div-style btn-width-style">
                                                        <?=\app\widgets\MakeOfferWidget::widget(['user_id'=>$item->user_id,'offer_id'=>0])?>
                                                        <button class="view-job startChat" data-sender-id="<?=Yii::$app->user->id?>" data-recipient-id="<?=$item->rezume['user_id']?>">Chat</button>
                                                        <?= HTML::a('View',Url::home().'rezume/'.$item->rezume['id'],['class' => 'view-job']); ?>
                                                    </div>
                                                </div>
                                                <div class="col-sm-5 dashboard-info-div-style">
                                                    <div class="row search-jobs-center-margin">
                                                        <div class="col-lg-6">
                                                            <h4><?= $item->rezume->user['name'].' '.$item->rezume->user['surname']; ?></h4>
                                                        </div>
                                                        <div class="col-lg-6 text-right">
                                                            <?=\app\widgets\rating\UserratingWidget::widget(['user_id'=>$item->rezume['user_id']])?>
                                                        </div>
                                                    </div>
                                                    <div class="row btn-lg-hide dashboard-info-fonts-style">
                                                        <div class="col-lg-6">
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    University
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <?=$item->rezume->userinfo->university['name']?>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    Industry
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <?=$item->userinfo->industry['name']?>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    Job function
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <?=$item->userinfo->jobfunction['name']?>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    GPA overall
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <?=$item->rezume['overall_gpa']?>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    GPA current
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <?=$item->rezume['current_gpa']?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    Matches vacancies
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <?php
                                                                    $matches_vacancies = \app\modules\jobs\models\Jobs::find()
                                                                        ->where(['company_id'=>$modelCompany->id,'industry_id'=>$item->userinfo['industry_id']])
                                                                        ->orWhere(['function_id'=>$item->userinfo['jobs_function_id']])
                                                                        ->orWhere([ 'major_id'=>$item->userinfo['major_id']])
                                                                        ->all();
                                                                    ?>
                                                                    <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();"><?=count($matches_vacancies)?></a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    Favorite company
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();"><?=\app\modules\jobs\models\FavoriteCompany::find()->where(['user_id'=>$item->rezume['user_id']])->count()?></a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    Favorite jobs
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();"><?=\app\modules\jobs\models\Favoritejobs::find()->where(['user_id'=>$item->rezume['user_id']])->count()?></a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    Apply
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();">
                                                                        <?=\app\modules\jobs\models\Userjobs::find()->where(['user_id'=>$item->rezume['user_id']])->count()?>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    Offer
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();">
                                                                        <?=\app\modules\jobs\models\OfferJob::find()->where(['user_id'=>$item->rezume['user_id']])->count()?>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 dashboard-info-last-style">
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                    Addres
                                                                </div>
                                                                <div class="col-lg-10">
                                                                    <?= $item->rezume->userinfo['address']; ?>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-2">
                                                                    Abou
                                                                </div>
                                                                <div class="col-lg-10">
                                                                    <?= substr($item->rezume['about'], 0, 60).'...'; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row div-fv-style">
                                                            <div class="col-lg-2">
                                                                Tags
                                                            </div>
                                                            <div class="col-lg-10 div-fv-margin-style text-right fv-div-padding">
                                                                <?= \app\widgets\FavoriterezumeWidget::widget(['rezume_id'=>$item->rezume['id'], 'job_id'=>0])?>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        <?php }?>
                                        <?php }?>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="apply">
                                    <div class="top-vacancies">
                                    <?php if($rezume_favorites){ ?>
                                        <?php foreach($applyUsers as $item){?>
                                            <div class="top-v-item">
                                                <div class="jobs-logo col-sm-4">
                                                    <?php if($item->rezume['avatar'] != ''){ ?>
                                                        <img class="img-responsive" src="<?= Url::home().'images/users_images/'.$item->rezume['avatar']; ?>">
                                                    <?php }else{ ?>
                                                        <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                                                    <?php } ?>
                                                </div>
                                                <div class="col-sm-5">
                                                    <h4><?= $item->rezume->user['name'].' '.$item->rezume->user['surname']; ?></h4>
                                                    <?=\app\widgets\rating\UserratingWidget::widget(['user_id'=>$item->user_id])?>
                                                    <table class="jobs-skills">
                                                        <tr>
                                                            <td>Addres</td>
                                                            <td> <?= $item->rezume->userinfo['address']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>University</td>
                                                            <td><?=$item->rezume->userinfo->university['name']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Industry</td>
                                                            <td><?=$item->userinfo->industry['name']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Job function</td>
                                                            <td><?=$item->userinfo->jobfunction['name']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>GPA overall</td>
                                                            <td><?=$item->rezume['overall_gpa']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>GPA current</td>
                                                            <td><?=$item->rezume['current_gpa']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Matches vacancies</td>
                                                            <?php
                                                            $matches_vacancies = \app\modules\jobs\models\Jobs::find()
                                                                ->where(['company_id'=>$modelCompany->id,'industry_id'=>$item->userinfo['industry_id']])
                                                                ->orWhere(['function_id'=>$item->userinfo['jobs_function_id']])
                                                                ->orWhere([ 'major_id'=>$item->userinfo['major_id']])
                                                                ->all();
                                                            ?>
                                                            <td><a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();"><?=count($matches_vacancies)?></a></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Favorite company</td>
                                                            <td>
                                                                <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();"><?=\app\modules\jobs\models\FavoriteCompany::find()->where(['user_id'=>$item->rezume['user_id']])->count()?></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Favorite jobs</td>
                                                            <td>
                                                                <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();"><?=\app\modules\jobs\models\Favoritejobs::find()->where(['user_id'=>$item->rezume['user_id']])->count()?></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Apply</td>
                                                            <td>
                                                                <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();">
                                                                    <?=\app\modules\jobs\models\Userjobs::find()->where(['user_id'=>$item->rezume['user_id']])->count()?>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Offer</td>
                                                            <td>
                                                                <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();">
                                                                    <?=\app\modules\jobs\models\OfferJob::find()->where(['user_id'=>$item->rezume['user_id']])->count()?>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>About</td>
                                                            <td> <?= substr($item->rezume['about'], 0, 60).'...'; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tags</td>
                                                            <td>
                                                                <?php $array_skills_user = explode(",", $item->rezume['skills']);
                                                                foreach ($array_skills_user as $value) {
                                                                    echo '<span class="label label-default">'.$value.'</span> ';
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Status</td>
                                                            <td id="job_status_<?=$item->id?>">
                                                                <?php if ($item->status == 0){?>
                                                                    <button class="view-job " id="apply_job_status_accept" data-job_id="<?=$item->jobs_id?>" data-user_job_id="<?=$item->id?>" data-user_id="<?=$item->user_id?>">Accept</button>
                                                                    <button class="view-job " id="apply_job_status_maybe" data-job_id="<?=$item->jobs_id?>" data-user_job_id="<?=$item->id?>" data-user_id="<?=$item->user_id?>">Maybe</button>
                                                                    <button class="view-job " id="apply_job_status_decline" data-job_id="<?=$item->jobs_id?>" data-user_job_id="<?=$item->id?>" data-user_id="<?=$item->user_id?>">Decline</button>
                                                                <?php }else{
                                                                    switch ($item->status){
                                                                        case 1:
                                                                            echo 'Accept';
                                                                            break;
                                                                        case 2:
                                                                            echo 'Maybe';
                                                                            break;
                                                                        case 3:
                                                                            echo 'Not suitable';
                                                                            break;
                                                                    }
                                                                }?>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col-sm-3">
                                                    <?=\app\widgets\MakeOfferWidget::widget(['user_id'=>$item->user_id,'offer_id'=>$item->id])?>
                                                    <button class="view-job startChat" data-sender-id="<?=Yii::$app->user->id?>" data-recipient-id="<?=$item->rezume['user_id']?>">Chat</button>
                                                    <?= HTML::a('View',Url::home().'rezume/'.$item->rezume['id'],['class' => 'view-job']); ?>
                                                    <?= \app\widgets\FavoriterezumeWidget::widget(['rezume_id'=>$item->rezume['id'], 'job_id'=>0])?>
                                                </div>
                                            </div>
                                        <?php }?>
                                        <?php }?>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="news">
                                    <div class="top-vacancies">
                                        
                                    <?php if($new){ ?>
                                        <div class="top-v-item">
                                            <?php $form = ActiveForm::begin();?>
                                            <?= $form->field($new, 'desc')->widget(\dosamigos\tinymce\TinyMce::className(), [
                                                'options' => ['rows' => 3],
                                                'clientOptions' => [
                                                    'plugins' => [
                                                        "advlist autolink lists link charmap print preview anchor",
                                                        "searchreplace visualblocks code fullscreen",
                                                        "insertdatetime media table contextmenu paste"
                                                    ],
                                                    'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                                                ]
                                            ])->label(false);?>
                                            <button type="submit" name="new_send" class="btn btn-create btn-ripple">Save</button>
                                            <?php ActiveForm::end();?>
                                        </div>

                                        <?php foreach ($news as $item){?>
                                            <div class="top-v-item university-content" id="company_new_id_<?=$item->id?>">
                                                <div class="post-header row">
                                                    <div class="user-img">
                                                        <img src="/images/company_logo/<?=$item->company['logo_src']?>" style="width:100%;position:relative;z-index:6;">
                                                    </div>
                                                    <div class="post-header-info">
                                                        <h4 class="post-author"><?= $item->company['name'] ?></h4>
                                                        <h6 class="post-date"> <?=Yii::$app->formatter->asDatetime($item->created_at)?></h6>
                                                    </div>
                                                    <div class="post-like-comment-count">
                                                        <span class="count like-count clickLike likePost33" onclick="like_new(<?=$item->id?>)" data-post_id="33">
                                                            <img src="../../../images/icon/like-ic.png"><span id="like_count" data-count="<?=$item->like?>"><?=$item->like?></span>
                                                        </span>
                                                        <span class="count post-count">
                                                            <img src="../../../images/icon/comment-ic.png"><?=\app\modules\jobs\models\CommitNewsCompany::find()->where(['new_id'=>$item->id])->count()?>
                                                        </span>
                                                        <span class="count views-count viewsCount33">
                                                            <img src="../../../images/icon/views-count-ic.png"><span>1</span>
                                                        </span>
                                                        <!--                                                        <span class="postUpdate"  data-post_id="33"><i class="fa fa-edit"></i></span>-->
                                                        <span class="postDelete" onclick="delete_new(<?=$item->id?>)" data-post_id="33"><i class="fa fa-times"></i></span>
                                                    </div>
                                                </div>
                                                <?=$item->desc?>
                                                <?php
                                                $commits = \app\modules\jobs\models\CommitNewsCompany::find()->where(['new_id'=>$item->id])->orderBy(['id'=>SORT_DESC])->all();
                                                ?>
                                                <div>
                                                    <?php foreach ($commits as $commit){?>
                                                        <div class="row">
                                                            <div class="">
                                                                <div class="user-img">
                                                                    <img style="width:40px;" class="" src="/images/users_images/<?=$commit->userinfo['avatar']?>">
                                                                </div>
                                                                <span class="user-name"><?=$commit->user['surname']?> <?=$commit->user['name']?></span>
                                                                <span><?=Yii::$app->formatter->asDatetime($commit->created_at)?></span>
                                                            </div>
                                                            <div class="">
                                                                <?=$commit->commit?>
                                                            </div>
                                                        </div>
                                                    <?php }?>
                                                </div>
                                                <?php $form2 = ActiveForm::begin();?>
                                                <div class="col-sm-8">
                                                    <?= $form2->field($commitNew,'commit')->label(false)?>
                                                    <?= $form2->field($commitNew,'new_id')->hiddenInput(['value'=>$item->id])->label(false)?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <button type="submit" name="commit_send" class="btn btn-create btn-ripple">Add</button>
                                                </div>
                                                <?php ActiveForm::end();?>
                                            </div>
                                        <?php } ?>
                                        <?php
                                        echo \yii\widgets\LinkPager::widget([
                                            'pagination' => $pages_news,
                                        ]);
                                        ?>
                                    <?php  } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="col-sm-6">
                        <?php
                        if($role == 'student') {

                            $items = [];

                            foreach ($jobs_top as $job) {
                                $logo = ($job->logo_src != '') ? '<img class="img-responsive" src="' . Url::home() . 'images/jobs_logo/' . $job->logo_src . '">' : '<img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">';

                                $favorite = ($job->company['user_id'] != \Yii::$app->user->id) ?
                                    FavoritejobsWidget::widget(['jobs_id' => $job->id]) : '';

                                $aply =($job->company['user_id'] != \Yii::$app->user->id) ?
                                    UserjobsWidget::widget(['jobs_id' => $job->id, 'page_type' => 'dashboard']) : '';
                                $items[] = [
                                    //'content' => '',
                                    'content' => '<div class="top-v-item"><div class="jobs-logo  col-sm-4">' . $logo . '</div>' .
                                        '<div class="col-sm-5">' .
                                        '<h4>' . $job->title . '</h4>' .
                                        '<table>'.
                                        '<tr><td>Adress</td><td>' . $job->address . '</td></tr>' .
                                        '<tr><td>Favorite</td><td>' . \app\modules\jobs\models\Favoritejobs::find()->where(['jobs_id'=>$job->id])->count() . '</td></tr>' .
                                        '<tr><td>Apply</td><td>' . \app\modules\jobs\models\Userjobs::find()->where(['jobs_id'=>$job->id])->count() . '</td></tr>' .
                                        '<tr><td>About</td><td>' . substr($job->about, 0, 60) . '...</td></tr>' .
                                        '</table>'.
                                        '</div>' .
                                        '<div class="ex-salary">' .
                                        '<h4>Expected salary</h4>' .
                                        '<p>' . $job->salary . '/month</p>' .
                                        '</div>' .
                                        '<div class="btn-wrap text-right">' .
                                        $favorite .
                                        HTML::a('View', Url::home() . 'job/' . $job->id, ['class' => 'view-job']) .
                                        $aply .
                                        '</div>' .
                                        '</div>',

                                ];

                            }

                            echo \yii\bootstrap\Carousel::widget([
                                'items' => $items,
                                'options' => [
                                    'class'=>'top-vacancies'
                                ]
                            ]);
                        }
                        ?>
                        <div class="rating-class-block">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <?php if($role =='student'){?>
                                    <li role="presentation" class="active"><a href="#5_jobs" aria-controls="5_jobs" role="tab" data-toggle="tab">Top 5 jobs</a></li>

                                    <li role="presentation"><a href="#5_company" aria-controls="5_company" role="tab" data-toggle="tab">Top 5 company</a></li>
                                    <li role="presentation" ><a href="#5_students" aria-controls="5_students" role="tab" data-toggle="tab">Top 5 students</a></li>
                                <?php }else{?>
                                    <li role="presentation" class="active"><a href="#5_students" aria-controls="5_students" role="tab" data-toggle="tab">Top 5 students</a></li>
                                    <li role="presentation"><a href="#5_jobs" aria-controls="5_jobs" role="tab" data-toggle="tab">Top 5 jobs</a></li>
                                    <li role="presentation"><a href="#5_company" aria-controls="5_company" role="tab" data-toggle="tab">Top 5 company</a></li>
                                <?php }?>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content ">
                                <div role="tabpanel" class="tab-pane <?=($role=='student')? '':'active'?>" id="5_students">
                                    <?php foreach ($top_5_student as $item){?>
                                        <table class="table class-table job-table-style">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <div class="user-img">
                                                        <img style="width:40px;" class="" src="/images/users_images/<?=$item->userinfo['avatar']?>">
                                                    </div>
                                                    <span class="user-name"><?=$item->user['surname']?> <?=$item->user['name']?></span>
                                                </td>
                                                <td>
                                                    <?=\app\widgets\rating\UserratingWidget::widget(['user_id'=>$item->user_id])?>
                                                </td>
                                                <td>
                                                    <a class="view-job" href="/rezume/<?=$item->id?>">View</a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    <?php } ?>
                                </div>
                                <div role="tabpanel" class="tab-pane <?=($role=='student')? 'active':''?>" id="5_jobs">
                                    <?php foreach ($top_5_job as $item){?>
                                        <table class="table class-table job-table-style">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <div class="user-img">
                                                        <img style="width:40px;" class="" src="/images/jobs_logo/<?=$item->logo_src?>">
                                                    </div>
                                                    <span class="user-name"><?=$item->title?></span>
                                                </td>
                                                <td>
                                                    <a class="view-job" href="/job/<?=$item->id?>">View</a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    <?php } ?>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="5_company">
                                    <?php foreach ($top_5_company as $item){?>
                                        <table class="table class-table job-table-style">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <div class="user-img">
                                                        <img style="width:40px;" class="" src="/images/jobs_logo/<?=$item->logo_src?>">
                                                    </div>
                                                    <span class="user-name"><?=$item->name?></span>
                                                </td>
                                                <td>
                                                    <a class="view-job" href="/company_view/<?=$item->id?>">View</a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    <?php } ?>
                                </div>
                            </div>

                        </div>
                        <?php if($role != 'student'){?>
                            <button type="button" class="btn btn-create" data-toggle="modal" data-target="#createEvent">
                                Create event
                            </button>
                        <?php } ?>
                        <div class="event-wrap">
                            <div class="col-md-10 no-padding-left">
                                <div id="paginator"></div>
                            </div>
                            <div class="col-md-2">
                                <button class="btn calendarToday">Today</button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="eventBlock">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if($role != 'student'){?>
        <!-- Modal -->
        <div class="modal fade" id="createEvent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                        <h4 class="modal-title" id="myModalLabel">Create new event</h4>
                    </div>
                    <?php $form = ActiveForm::begin();?>
                    <div class="modal-body width_textarea">
                        <?= $form->field($eventsM,'title')?>
                        <?= $form->field($eventsM,'content')->textarea(['row'=>6])?>
                        <?= $form->field($eventsM,'date_start')->widget(\kartik\date\DatePicker::className(),[
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ])?>
                        <?= $form->field($eventsM,'date_end')->widget(\kartik\date\DatePicker::className(),[
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ])?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
                        <button type="submit" name="event" class="btn btn-action">Save event</button>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>

        <div class="modal fade" id="offer_job" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                    </div>
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="modal-body">
                        <input type="hidden" name="OfferJob[user_id]" value="" id="user_id_offer">
                        <?= $form->field($offerJob,'salary');?>
                        <?= $form->field($offerJob,'type')->dropDownList(['day'=>'day','week'=>'week','month'=>'month']);?>
                        <?= $form->field($offerJob,'job_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\modules\jobs\models\Jobs::findAll(['company_id'=>$modelCompany->id]),'id','title'));?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
                        <button type="submit" name="offer_bt" class="btn btn-action">Save changes</button>
                    </div>
                    <?php ActiveForm::end() ?>
                </div>
            </div>
        </div>

        <?php foreach ($rezumes as $item){

            $offersUsers = \app\modules\jobs\models\OfferJob::findAll(['user_id'=>$item->id]);

            $followersUsers = \app\modules\jobs\models\FavoriteRezume::findAll(['rezume_id'=>$item->id]);

            $followCompany = \app\modules\jobs\models\FavoriteCompany::findAll(['user_id'=>$item->user_id]);

            $followJob = \app\modules\jobs\models\Favoritejobs::findAll(['user_id'=>$item->user_id]);

            $appliedUsers = \app\modules\jobs\models\Userjobs::findAll(['user_id'=>$item->user_id]);


            $matchesVacancies = \app\modules\jobs\models\Jobs::find()
                ->where(['company_id'=>$modelCompany->id,'industry_id'=>$item->userinfo['industry_id']])
                ->orWhere(['function_id'=>$item->userinfo['jobs_function_id']])
                ->orWhere([ 'major_id'=>$item->userinfo['major_id']])
                ->all();

            ?>
            <div class="modalRightPagesLS" id="view_user_<?=$item->user_id?>">

                <a class="btn btn-close pull-right closeModal" onclick="$('#view_user_<?=$item->user_id?>').hide();" >
                    <span class="" aria-hidden="true">x</span>
                </a>
                <div class="user-info">
                    <div class="class-user-av">
                        <?php if($item->avatar != ''){ ?>
                            <img class="img-responsive" src="<?= Url::home().'images/users_images/'.$item->avatar; ?>">
                        <?php }else{ ?>
                            <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                        <?php } ?>
                    </div>
                    <span class="user-name">
					<?= $item->user['name'].' '.$item->user['surname']; ?>
				</span>
                    <div class="right-block">

                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-content">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#offers_user<?=$item->user_id?>" aria-controls="offers_user<?=$item->user_id?>" role="tab" data-toggle="tab">Offers</a></li>
                        <li role="presentation"><a href="#followers_user<?=$item->user_id?>" aria-controls="followers_user<?=$item->user_id?>" role="tab" data-toggle="tab">Followers</a></li>
                        <li role="presentation"><a href="#follow_user<?=$item->user_id?>" aria-controls="follow_user<?=$item->user_id?>" role="tab" data-toggle="tab">Follow</a></li>
                        <li role="presentation"><a href="#applied_user<?=$item->user_id?>" aria-controls="applied_user<?=$item->user_id?>" role="tab" data-toggle="tab">Applied</a></li>
                        <li role="presentation"><a href="#matches_vacancies<?=$item->user_id?>" aria-controls="matches_vacancies<?=$item->user_id?>" role="tab" data-toggle="tab">Matches vacancies</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="offers_user<?=$item->user_id?>">
                            <table class="table classBlock">
                                <thead>
                                <tr>
                                    <td>Name jobs</td>
                                    <td>Salaty</td>
                                    <td>Type</td>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($offersUsers as $value){?>
                                    <tr>
                                        <td>
                                            <div class="class-user-av">
                                                <img class="" src="<?=$value->job['logo_src']?>">
                                            </div>
                                            <span class="user-name"><?=$value->job['title']?></span>
                                        </td>
                                        <td>
                                            <?=$value->salary?>
                                        </td>
                                        <td>
                                            <?=$value->type?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="followers_user<?=$item->user_id?>">
                            <table class="table classBlock">
                                <thead>
                                <tr>
                                    <td>Name comapny</td>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($followersUsers as $value){?>
                                    <tr>
                                        <td>
                                            <div class="class-user-av">
                                                <img class="" src="<?=$value->user->company['logo_src']?>">
                                            </div>
                                            <span class="user-name"><?=$value->user->company['name']?></span>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="follow_user<?=$item->user_id?>">
                            <table class="table classBlock">
                                <thead>
                                <tr>
                                    <td>Name comapny</td>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($followCompany as $value){?>
                                    <tr>
                                        <td>
                                            <div class="class-user-av">
                                                <img class="" src="<?=$value->company['logo_src']?>">
                                            </div>
                                            <span class="user-name"><?=$value->company['name']?></span>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            <table class="table classBlock">
                                <thead>
                                <tr>
                                    <td>Name jobs</td>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($followJob as $value){?>
                                    <tr>
                                        <td>
                                            <div class="class-user-av">
                                                <img class="" src="<?=$value->job['logo_src']?>">
                                            </div>
                                            <span class="user-name"><?=$value->job['title']?></span>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="applied_user<?=$item->user_id?>">
                            <table class="table classBlock">
                                <thead>
                                <tr>
                                    <td>Name jobs</td>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($appliedUsers as $value){?>
                                    <tr>
                                        <td>
                                            <div class="class-user-av">
                                                <img class="" src="<?=$value->job['logo_src']?>">
                                            </div>
                                            <span class="user-name"><?=$value->job['title']?></span>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="matches_vacancies<?=$item->user_id?>">
                            <table class="table classBlock">
                                <thead>
                                <tr>
                                    <td>Name vacansies</td>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($matchesVacancies as $value){?>
                                    <tr>
                                        <td>
                                            <div class="class-user-av">
                                                <img class="" src="<?=$value->logo_src?>">
                                            </div>
                                            <span class="user-name"><?=$value->title?></span>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        <?php } ?>

    <?php } else {?>
        <?php foreach ($offersUsers as $item){
            $offersJobUser = \app\modules\jobs\models\OfferJob::findAll(['job_id'=>$item->job_id]);
            ?>
            <div class="modalRightPagesLS" id="view_offer_<?=$item->id?>">

                <a class="btn btn-close pull-right closeModal" onclick="$('#view_offer_<?=$item->id?>').hide();" >
                    <span class="" aria-hidden="true">x</span>
                </a>
                <div class="user-info">
                    <div class="class-user-av">
                        <?php if($item->job->company['logo_src'] != ''){ ?>
                            <img class="img-responsive" src="<?= Url::home().'images/company_logo/'.$item->job->company['logo_src']; ?>">
                        <?php }else{ ?>
                            <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                        <?php } ?>
                    </div>
                    <span class="user-name">
					<?= $item->job->company['name']; ?>
				</span>
                    <div class="right-block">

                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-content">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#offers_<?=$item->id?>" aria-controls="offers_<?=$item->id?>" role="tab" data-toggle="tab">Offers</a></li>

                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="offers_<?=$item->id?>">
                            <table class="table classBlock">
                                <thead>
                                <tr>
                                    <td>User</td>
                                    <td>Salaty</td>
                                    <td>Type</td>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($offersJobUser as $value){?>
                                    <tr>
                                        <td>
                                            <div class="class-user-av">
                                                <img class="" src="<?=$value->user->userinfo['avatar']?>">
                                            </div>
                                            <span class="user-name"><?=$value->user['surname'].' '.$value->user['name']?></span>
                                        </td>
                                        <td>
                                            <?=$value->salary?>
                                        </td>
                                        <td>
                                            <?=$value->type?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        <?php } ?>
    <?php } ?>
    <div class="search_block search-job-block" id="close_s">
        <div class="search_panel">
            <?= \app\widgets\FilterjobsWidget::widget(['message' => ' Yii2.0','role'=>$role]) ?>
        </div>
    </div>

    <script>
        function delete_new(id) {
            $.post('/delete_company_new',{id:id},function (data) {
                if(data)
                    $('#company_new_id_'+id).remove()
//               location.reload();
            })
        }

        function like_new(id) {
            $.post('/like_company_new',{id:id},function (data) {
                if(data)
                    $('#like_count').text($('#like_count').data('count')+1);
            })
        }
    </script>