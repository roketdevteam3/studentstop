<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Alert;
use yii\helpers\Url;
use yii\widgets\LinkPager;

use app\assets\BusinessAsset;
BusinessAsset::register($this);

use app\assets\DashboardAsset;
DashboardAsset::register($this);

?>
<div class="row">
    <?php
        if(Yii::$app->session->hasFlash('jobs_update')):
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-info',
                ],
                'body' => 'Jobs added',
            ]);
        endif; 
        if(Yii::$app->session->hasFlash('jobs_not_update')):
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-error',
                ],
                'body' => 'Jobs not added',
            ]);
        endif; 
    ?>
    <div style="display:none;">
        <div class="table table-striped" class="files" id="previews">
            <div id="template" class="file-row">
              <!-- This is used as the file preview template -->
              <div>
                  <span class="preview"><img data-dz-thumbnail /></span>
              </div>
              <div>
                  <p class="name" data-dz-name></p>
                  <strong class="error text-danger" data-dz-errormessage></strong>
              </div>
              <div>
                  <p class="size" data-dz-size></p>
                  <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                    <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                  </div>
              </div>
              <div>
                <button class="btn btn-primary start">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
                <button data-dz-remove class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
                <button data-dz-remove class="btn btn-danger delete">
                  <i class="glyphicon glyphicon-trash"></i>
                  <span>Delete</span>
                </button>
              </div>
            </div>
        </div>
    </div>
    
    
    <div class="col-sm-12">
        <?= $this->render('menu',['role'=>$role]); ?>
    </div>
    <div class="col-sm-12">
        <?= HTML::a('Back to all jobs', Url::home().'mycompany/jobs',['class'=>'btn btn-default']);?>
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#update_jobs">Update job</button>

        <div id="update_jobs" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update job</h4>
              </div>
              <div class="modal-body">
                        <?php $form = ActiveForm::begin(); ?>
                            <div class="form-content">
                                <div class="form-group">
                                    <label class="control-label">Title</label>
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <?= $form->field($modelJobs, 'title')->textInput(['class'=>'form-control'])->label(false); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">About</label>
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <?= $form->field($modelJobs, 'about')->textarea(['class'=>'form-control'])->label(false); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Salary</label>
                                    <div class="inputer">
                                        <div class="input-wrapper">
                                            <?= $form->field($modelJobs, 'salary')->textInput(['class'=>'form-control'])->label(false); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <a class="new_logo_photo btn btn-success">new logo</a>
                            </div>
                            <div class="jobs_logo_photo">
                            </div>

                            <?= $form->field($modelJobs, 'logo_src')->hiddenInput(['class'=>'form-control'])->label(false); ?>
                            <div class="form-group">
                                <?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'add_business', 'class' => 'btn btn-primary pull-right']) ?>
                            </div>
                        <?php $form = ActiveForm::end(); ?>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-12" style="margin-top: 20px">
            <div class="col-sm-3">
                <img src="<?= Url::home().'images/jobs_logo/'.$modelJobs->logo_src; ?>" style="width:100%;">
            </div>
            <div class="col-sm-9">
                <h2><?= $modelJobs->title; ?></h2>
                <div><b>Salary - </b><?= $modelJobs->salary; ?></div>
                <div>
                    <?= $modelJobs->about; ?>
                </div>
            </div>
        </div>
        
        <div class="col-sm-12">
            <ul class="nav nav-tabs" style="padding:20px;">
            <li role="presentation">
                <a href="<?= Url::home(); ?>jobs/<?= $modelJobs->id; ?>/apply_student">Users apply</a>
            </li>
            <li role="presentation" class="active">
                <a href="<?= Url::home(); ?>jobs/<?= $modelJobs->id; ?>/select_student">Select student</a>
            </li>
            </ul>
        </div>
        <div class="col-sm-12">
            <h2 style="text-align: center"><b>Select student</b></h2>
        </div>
        <div class="col-sm-12">
            <?php foreach($modelUser as $user){ ?>
                <div class="col-sm-12" style="padding:10px;">
                    <?php // var_dump($user); ?>
                    <div class="col-sm-3">
                        <?php  if($user['avatar'] != ''){ ?>
                            <img src="<?= Url::home().'images/users_images/'.$user['avatar']; ?>" style="width:100%;">
                        <?php }else{ ?>
                            <img src="<?= Url::home().'images/default_avatar.jpg'; ?>" style="width:100%;">
                        <?php } ?>
                    </div>
                    <div class="col-sm-9">
                        <h2><?= $user['name']; ?> <?= $user['surname']; ?></h2>
                        <button class="btn btn-success showResume" data-user_id="<?= $user['id']; ?>">Show resume</button>
                        <button class="btn btn-primary inviteUser" data-job_id="<?= $modelJobs->id; ?>" data-user_id="<?= $user['id']; ?>">Invite user</button>
                        <button class="btn btn-primary startChat" data-sender-id="<?=\Yii::$app->user->id?>" data-recipient-id="<?= $user['id']?>">Write <i class="fa fa-envelope"></i></button>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="col-sm-12">
            <?= LinkPager::widget(['pagination'=>$pagination]); ?>
        </div>
        <div id="user_resume" class="modal fade" role="dialog">
            <div class="modal-dialog" style="width:90%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Resume</h4>
                    </div>
                    <div class="modal-body">
                        <div class="modalBodyResume">
                            <div class="col-sm-12">
                                <div class="col-sm-3">
                                    <img src="" style="width:100%;" class="UserAvatar">
                                </div>
                                <div class="col-sm-9 UserAbout">
                                    
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-3">
                                </div>
                                <div class="col-sm-9">
                                    <table class="tableAbout">
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 