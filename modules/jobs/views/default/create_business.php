<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap\Alert;
use kartik\date\DatePicker;
use app\assets\BusinessAsset;
BusinessAsset::register($this);

?>



<div style="display:none;">
  <div class="table table-striped" class="files" id="previews">
    <div id="template" class="file-row">
      <!-- This is used as the file preview template -->
      <div class="col-sm-4">
        <span class="preview"><img data-dz-thumbnail /></span>
      </div>
      <div class="col-sm-5">
        <p class="name" data-dz-name></p>
        <strong class="error text-danger" data-dz-errormessage></strong>
        <p class="size" data-dz-size></p>
        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
          <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
        </div>
      </div>
      <div class="col-sm-3">
        <button data-dz-remove class="btn btn-danger delete">
          <i class="glyphicon glyphicon-trash"></i>
          <span>Delete</span>
        </button>
      </div>
      <div style="clear:both;"></div>
    </div>
  </div>
</div>


<div class="col-sm-12" style="background: url(<?= Url::home(); ?>images/univesity_fon.png) center;background-size:cover;background-repeat:no-repeat;padding:0px;min-height:93vh;">

  <div class="col-sm-10" style='padding:0px;'>
    <div class="col-sm-12" style="padding-top: 20px;">
      <?php
      if(Yii::$app->session->hasFlash('company_update')):
        echo Alert::widget([
          'options' => [
            'class' => 'alert-info',
          ],
          'body' => 'Company update',
        ]);
      endif;
      if(Yii::$app->session->hasFlash('company_not_update')):
        echo Alert::widget([
          'options' => [
            'class' => 'alert-error',
          ],
          'body' => 'Company not update',
        ]);
      endif;
      ?>
      <div class="col-sm-12">
        <?php /* ?>
                <div class="col-sm-12" style="box-shadow: 0 0 10px rgba(0,0,0,0.1);margin-bottom:20px;background-color:white;border-radius:5px;padding:0px">

                    <ul class="nav nav-tabs" style="padding:0px;">
                        <li role="presentation" class="active">
                            <?= HTML::a('My company edit',Url::home().'mycompany/edit'); ?>
                        </li>
                        <li role="presentation">
                            <?= HTML::a('jobs',Url::home().'mycompany/jobs'); ?>
                        </li>
                    </ul>
                </div>
                <?php */ ?>
        <div class="col-sm-12" style="box-shadow: 0 0 10px rgba(0,0,0,0.1);margin-bottom:20px;background-color:white;border-radius:5px;padding:20px;">
          <h2 style="text-align: center"><b>My company</b></h2>
          <div class="col-sm-12">
            <?php $form = ActiveForm::begin(); ?>
            <div class="form-content">
            <div class="form_style_business">
                    <?= $form->field($modelCompany, 'name')->textInput(['class'=>'form-control']); ?>


                    <?= $form->field($modelCompany, 'about')->textarea(['class'=>'form-control']); ?>



                    <?= $form->field($modelCompany, 'phone')->textInput(['class'=>'form-control']); ?>



                    <?= $form->field($modelCompany, 'fax')->textInput(['class'=>'form-control']); ?>



                    <?= $form->field($modelCompany, 'email')->textInput(['class'=>'form-control']); ?>



                    <?= $form->field($modelCompany, 'website')->textInput(['class'=>'form-control']); ?>


                    <?= $form->field($modelCompany, 'facebook')->textInput(['class'=>'form-control']); ?>


                    <?= $form->field($modelCompany, 'twitter')->textInput(['class'=>'form-control']); ?>



                    <?= $form->field($modelCompany, 'linkedin')->textInput(['class'=>'form-control']); ?>



                    <?= $form->field($modelCompany, 'address')->textInput(['class'=>'form-control']); ?>



                    <?php //= $form->field($modelCompany, 'company_create')->textInput(['class'=>'form-control'])->label(false); ?>
                    <?php echo $form->field($modelCompany, 'company_create')->widget(DatePicker::classname(), [
                      'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd'
                      ]
                    ]);
                    ?>



                    <?= $form->field($modelCompany, 'industry_id')->dropDownList($jobIndustryArray); ?>

            </div>
            </div>

            <div class="company_logo_photo_preview">
              <?php if($modelCompany->logo_src != ''){ ?>
                <img src="<?= Url::home().'images/company_logo/'.$modelCompany->logo_src; ?>" style="width:5%;">
              <?php } ?>
            </div>

            <div class="" style="display: inline-block">
              <a class="new_logo_photo btn btn-success">change logo</a>
            </div>
            <div class="company_logo_photo">

            </div>

            <?= $form->field($modelCompany, 'logo_src')->hiddenInput(['class'=>'form-control'])->label(false); ?>
            <?= $form->field($modelCompany, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false); ?>
            <div class="form-group">
              <?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'add_business', 'class' => 'btn btn-primary pull-right']) ?>
            </div>
            <?php $form = ActiveForm::end(); ?>
          </div>
          <div style="clear:both;"></div>
        </div>
      </div>
    </div>
  </div>
</div>