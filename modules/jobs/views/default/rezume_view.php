<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Alert;
use yii\helpers\Url;

use app\assets\ResumeAsset;
ResumeAsset::register($this);

use kartik\date\DatePicker;

?>
<div class="row">
  <?= $this->render('right_menu',['role'=>$role]); ?>
  <div class="jobs-content">
    <div class="container-fluid background-block">
      <div class="row">
        <?= $this->render('menu',['role'=>$role]); ?>
        <div class="col-sm-12">
          <div class="my-resume-form-wrap">
            <?php $form = ActiveForm::begin(); ?>
            <div class="row">
              <div class="col-md-3">
                <div class="resume-avatar">
                  <?php if($modelResume->avatar != ''){ ?>
                    <img class="img-responsive" src="<?= Url::home(); ?>images/users_images/<?=  $modelResume->avatar; ?>">
                  <?php }else{ ?>
                    <img src="<?= Url::home(); ?>images/default_avatar.jpg" style="width:100%;">
                  <?php } ?>
                  <a href="/dashboard/job" class="btn change-avatar">Back</a>
                </div>
              </div>
              <div class="col-md-9 col-lg-8">
                  <div id="info-rm" class="my-resume resume-info-div-style">
                      <div class="row">
                          <div class="col-md-6 col-lg-6">
                              <div class="row">
                                  <div class="col-md-6 title-input-style">
                                      <label>Name:</label>
                                  </div>
                                  <div class="col-md-6">
                                      <?= $modelUser->name; ?>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-md-6 title-input-style">
                                      <label>Surname:</label>
                                  </div>
                                  <div class="col-md-6">
                                      <?= $modelUser->surname; ?>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-md-6 title-input-style">
                                      <label>Birthday:</label>
                                  </div>
                                  <div class="col-md-6">
                                      <?= $modelUserInfo->birthday; ?>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-md-6 title-input-style">
                                      <label>Gender:</label>
                                  </div>
                                  <div class="col-md-6">
                                      <?=  ($modelUserInfo->gender == 1)? 'Male' : 'Female'; ?>
                                  </div>
                              </div>
                              <div class="row">
                              <div class="col-md-6 title-input-style">
                                  <label>Address:</label>
                              </div>
                              <div class="col-md-6">
                                  <?= $modelUserInfo->address; ?>
                              </div>
                          </div>
                          </div>
                          <div class="col-md-6 col-lg-6">
                              <div class="row">
                                  <div class="col-md-6 title-input-style">
                                      <label>Mobile phone:</label>
                                  </div>
                                  <div class="col-md-6">
                                      <?= $modelUserInfo->mobile; ?>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-md-6 title-input-style">
                                      <label>Email:</label>
                                  </div>
                                  <div class="col-md-6">
                                      <?= $modelUser->email; ?>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-md-6 title-input-style">
                                      <label>Facebook:</label>
                                  </div>
                                  <div class="col-md-6">
                                      <?= $modelUserInfo->facebook; ?>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-md-6 title-input-style">
                                      <label>Twitter:</label>
                                  </div>
                                  <div class="col-md-6">
                                      <?= $modelUserInfo->twitter; ?>
                                  </div>
                              </div>
                              <div class="row">
                          <div class="col-md-6 title-input-style">
                              <label>Linkedin:</label>
                          </div>
                          <div class="col-md-6">
                              <?= $modelUserInfo->linkedin; ?>
                          </div>
                      </div>
                          </div>
                          <div id="rm-info" class="read-more-btn-style">
                              <span>Reead more</span>
                          </div>
                      </div>
                  </div>
                  <?php
                  if(strlen($modelResume->about) == null) {

                      echo "";
                  }
                  else {?>
                  <div id="about-mr" class="my-resume resume-info-div-style">
                      <div class="row">
                              <div class="col-md-12 text-center title-info-style">
                          ABOUT
                          </div>
                          <div id="rm-text-about" class="col-md-12 about-text-div-style">
                              <?= $modelResume->about; ?>
                          </div>
                      </div>
                      <?php if(strlen($modelResume->about) > 211) {?>
                          <div id="rm-about" class="read-more-btn-style">
                              <span>Reead more</span>
                          </div>
                      <?php } ?>
                  </div>
                  <?php } ?>
                  <div class="my-resume resume-info-div-style">
                      <div class="row">
                          <div class="col-md-12 col-lg-12 text-center title-info-style">
                              EDUCATION
                          </div>
                          <div class="col-md-6 col-lg-6">
                              <div class="row">
                                <div class="col-md-6 title-input-style">
                                    <label>University:</label>
                                </div>
                                <div class="col-md-6">
                                    <?= $universityArray[$modelUserInfo->university_id]; ?>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-6 title-input-style">
                                    <label>Major:</label>
                                </div>
                                <div class="col-md-6">
                                    <?= $majorArray[$modelUserInfo->major_id]; ?>
                                </div>
                            </div>
                              <div class="row">
                                <div class="col-md-6 title-input-style">
                                    <label>Acedemic status:</label>
                                </div>
                                <div class="col-md-6">
                                    <?= $userRoleArray[$modelUserInfo->academic_status]; ?>
                                </div>
                            </div>
                          </div>
                          <div class="col-md-6 col-lg-6">
                              <div class="row">
                                  <div class="col-md-6 title-input-style">
                                      <label>Date end education:</label>
                                  </div>
                                  <div class="col-md-6">
                                      <?= $modelUserInfo->date_end_education; ?>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-md-6 title-input-style">
                                      <label>Industry:</label>
                                  </div>
                                  <div class="col-md-6">
                                      <?= $jobIndustryArray[$modelUserInfo->industry_id]; ?>
                                  </div>
                              </div>
                              <div class="row">
                          <div class="col-md-6 title-input-style">
                              <label>Major:</label>
                          </div>
                          <div class="col-md-6">
                              <?= $jobFunctionArray[$modelUserInfo->jobs_function_id]; ?>
                          </div>
                      </div>
                          </div>
                      </div>
                  </div>
              </div>
            </div>
            <?php $form = ActiveForm::end(); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

