<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use kartik\date\DatePicker;
use app\assets\BusinessAsset;

BusinessAsset::register($this);

?>
	<div style="display:none;">
		<div class="table table-striped" class="files" id="previews">
			<div id="template" class="file-row">
				<!-- This is used as the file preview template -->
				<div>
					<span class="preview"><img data-dz-thumbnail /></span>
				</div>
				<div>
					<p class="name" data-dz-name></p>
					<strong class="error text-danger" data-dz-errormessage></strong>
				</div>
				<div>
					<p class="size" data-dz-size></p>
					<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
						<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
					</div>
				</div>
				<div>
					<button data-dz-remove class="btn delete">
						<i class="glyphicon glyphicon-trash"></i>
						<span>Delete</span>
					</button>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<?= $this->render('right_menu',['role'=>$role]); ?>
		<div class="jobs-content">
			<div class="container-fluid background-block">
				<div class="row">
					<?= $this->render('menu',['role'=>$role]); ?>
					<div class="col-sm-12">
						<div class="add-company-wrap">
							<h3 class="add-company-header">Add business</h3>
							<?php $form = ActiveForm::begin(); ?>
								<div class="form-content">
									<?= $form->field($modelNewCompany, 'name')->textInput(['class'=>'form-control'])->label('Name'); ?>
									<?= $form->field($modelNewCompany, 'about')->textarea(['class'=>'form-control'])->label('About'); ?>
									<?= $form->field($modelNewCompany, 'phone')->textInput(['class'=>'form-control'])->label('Phone'); ?>
									<?= $form->field($modelNewCompany, 'fax')->textInput(['class'=>'form-control'])->label('Fax'); ?>
									<?= $form->field($modelNewCompany, 'email')->textInput(['class'=>'form-control'])->label('Email'); ?>
									<?= $form->field($modelNewCompany, 'website')->textInput(['class'=>'form-control'])->label('Website'); ?>
									<?= $form->field($modelNewCompany, 'facebook')->textInput(['class'=>'form-control'])->label('Facebook'); ?>
									<?= $form->field($modelNewCompany, 'twitter')->textInput(['class'=>'form-control'])->label('Twitter'); ?>
									<?= $form->field($modelNewCompany, 'linkedin')->textInput(['class'=>'form-control'])->label('Linkedin'); ?>
									<?= $form->field($modelNewCompany, 'address')->textInput(['class'=>'form-control'])->label('Address'); ?>
									 <?php  //$form->field($modelUserInfo, 'date_end_education')->textInput(['class'=>'form-control','style'=>'border:1px solid #f0f0f0 !important;'])->label(false); 
										echo $form->field($modelNewCompany, 'company_create')->widget(DatePicker::classname(), [
											'pluginOptions' => [
												'autoclose'=>true,
                                              'format' => 'yyyy-mm-dd'
											]
										])->label('Company create');
									?>
									<?= $form->field($modelNewCompany, 'industry_id')->dropDownList($jobIndustryArray)->label(false); ?>
								</div>
								<a class="new_logo_photo btn">New logo</a>
								<div class="company_logo_photo">
								</div>
								<?= $form->field($modelNewCompany, 'logo_src')->hiddenInput(['class'=>'form-control'])->label(false); ?>
								<?= $form->field($modelNewCompany, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false); ?>
								<div class="form-group">
									<?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'add_business', 'class' => 'btn save-job']) ?>
								</div>
							<?php $form = ActiveForm::end(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>