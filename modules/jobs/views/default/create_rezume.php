<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Alert;
use yii\helpers\Url;

use app\assets\ResumeAsset;
ResumeAsset::register($this);

use kartik\date\DatePicker;

?>
<div class="row">
  <div class="jobs-content" style="padding-right:0;">
    <div class="container-fluid background-block">
      <div class="row">
        <div class="col-sm-12">
            <div class="my-resume-form-wrap">
              <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                <div class="row">
                    <div class="col-md-3">
                        <div class="resume-avatar">
                          <?php if($modelUserInfo->avatar != ''){ ?>
                              <img class="img-responsive" src="<?= Url::home(); ?>images/users_images/<?=  $modelResume->avatar; ?>">
                          <?php }else{ ?>
                              <img src="<?= Url::home(); ?>images/default_avatar.jpg" style="width:100%;">
                          <?php } ?>
                          <?php

                          echo \kartik\file\FileInput::widget([
                            'name' => 'avatar',
                            'pluginOptions' => [
                              'showCaption' => false,
                              'showRemove' => false,
                              'showUpload' => false,
                              'browseClass' => 'btn btn-primary btn-block',
                              'browseLabel' =>  'Select Photo'
                            ],
                            'options' => ['accept' => 'image/*'],
                          ]);

                          /*echo $form->field($modelResume, 'avatar')->widget(\kartik\file\FileInput::classname(), [
                            'pluginOptions' => [
                              'showCaption' => false,
                              'showRemove' => false,
                              'showUpload' => false,
                              'browseClass' => 'btn btn-primary btn-block',
                              'browseLabel' =>  'Select Photo'
                            ],
                            'options' => ['accept' => 'image/*']
                          ]);*/
                          ?>
                        </div>
                    </div>
                    <div class="col-md-9 col-lg-8">
                        <div class="my-resume">
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Name</label>
                                </div>
                                <div class="col-md-9">
                                  <?= $form->field($modelUser, 'name')->textInput(['class'=>'form-control'])->label(false); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Surname</label>
                                </div>
                                <div class="col-md-9">
                                  <?= $form->field($modelUser, 'surname')->textInput(['class'=>'form-control'])->label(false); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Birthday</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="col-md-9">
                                      <?php  //$form->field($modelUserInfo, 'date_end_education')->textInput(['class'=>'form-control','style'=>'border:1px solid #f0f0f0 !important;'])->label(false);
                                      echo $form->field($modelUserInfo, 'birthday')->widget(DatePicker::classname(), [
                                        'pluginOptions' => [
                                          'autoclose'=>true,
                                          'format' => 'yyyy-mm-dd'
                                        ]
                                      ])->label(false);
                                      ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Gender</label>
                                </div>
                                <div class="col-md-9">
                                  <?= $form->field($modelUserInfo, 'gender')->dropDownList(['1' => 'Male','2' => 'Female'],['class'=>'form-control'])->label(false); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Acedemic status</label>
                                </div>
                                <div class="col-md-9">
                                  <?= $form->field($modelUserInfo, 'academic_status')->dropDownList($userRoleArray,['class'=>'form-control'])->label(false); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>About</label>
                                </div>
                                <div class="col-md-9">
                                  <?= $form->field($modelUserInfo, 'about')->textarea(['class'=>'form-control'])->label(false); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Mobile phone</label>
                                </div>
                                <div class="col-md-9">
                                  <?= $form->field($modelUserInfo, 'mobile')->textInput(['class'=>'form-control'])->label(false); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Email</label>
                                </div>
                                <div class="col-md-9">
                                  <?= $form->field($modelUser, 'email')->textInput(['class'=>'form-control'])->label(false); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Facebook</label>
                                </div>
                                <div class="col-md-9">
                                  <?= $form->field($modelUserInfo, 'facebook')->textInput(['class'=>'form-control'])->label(false); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Twitter</label>
                                </div>
                                <div class="col-md-9">
                                  <?= $form->field($modelUserInfo, 'twitter')->textInput(['class'=>'form-control'])->label(false); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Linkedin</label>
                                </div>
                                <div class="col-md-9">
                                  <?= $form->field($modelUserInfo, 'linkedin')->textInput(['class'=>'form-control'])->label(false); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>University</label>
                                </div>
                                <div class="col-md-9">
                                  <?= $form->field($modelUserInfo, 'university_id')->dropDownList($universityArray,['class'=>'form-control'])->label(false); ?>
                                </div>
                            </div>
                            <?php /* ?>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Major</label>
                                </div>
                                <div class="col-md-9">
                                  <?php //= $form->field($modelUserInfo, 'major_id')->dropDownList($majorArray,['class'=>'form-control'])->label(false); ?>
                                </div>
                            </div>
                            <?php */ ?>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>address</label>
                                </div>
                                <div class="col-md-9">
                                  <?= $form->field($modelUserInfo, 'address')->textInput(['class'=>'form-control','style'=>'border:1px solid #f0f0f0 !important;'])->label(false); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Date begin education</label>
                                </div>
                                <div class="col-md-9">
                                  <?php  //$form->field($modelUserInfo, 'date_end_education')->textInput(['class'=>'form-control','style'=>'border:1px solid #f0f0f0 !important;'])->label(false);
                                  echo $form->field($modelUserInfo, 'date_begin_education')->widget(DatePicker::classname(), [
                                    'pluginOptions' => [
                                      'autoclose'=>true,
                                      'format' => 'yyyy-mm-dd'
                                    ]
                                  ])->label(false);
                                  ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Date end education</label>
                                </div>
                                <div class="col-md-9">
                                  <?php  //$form->field($modelUserInfo, 'date_end_education')->textInput(['class'=>'form-control','style'=>'border:1px solid #f0f0f0 !important;'])->label(false);
                                  echo $form->field($modelUserInfo, 'date_end_education')->widget(DatePicker::classname(), [
                                    'pluginOptions' => [
                                      'autoclose'=>true,
                                      'format' => 'yyyy-mm-dd'
                                    ]
                                  ])->label(false);
                                  ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <label>Industry</label>
                                </div>
                                <div class="col-md-9">
                                  <?= $form->field($modelUserInfo, 'industry_id')->dropDownList($jobIndustryArray,['class'=>'form-control'])->label(false); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Major</label>
                                </div>
                                <div class="col-md-9">
                                  <?= $form->field($modelUserInfo, 'jobs_function_id')->dropDownList($majorArray,['class'=>'form-control'])->label(false); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Skills</label>
                                </div>
                                <div class="col-md-9">
                                  <?=
                                  $form->field($modelResume, 'skills')->widget(\pudinglabs\tagsinput\TagsinputWidget::classname(), [
                                    'options' => [],
                                    'clientOptions' => [],
                                    'clientEvents' => []
                                  ])->label(false);
                                  ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Overall GPA</label>
                                </div>
                                <div class="col-md-9">
                                  <?=
                                  $form->field($modelResume, 'overall_gpa')->label(false);
                                  ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label>Current GPA</label>
                                </div>
                                <div class="col-md-9">
                                  <?=
                                  $form->field($modelResume, 'current_gpa')->label(false);
                                  ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                          <?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'add_business', 'class' => 'btn save-resume']) ?>
                        </div>
                    </div>
                </div>
              <?php $form = ActiveForm::end(); ?>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

