<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Alert;
use yii\helpers\Url;
use yii\widgets\LinkPager;

?>
<div class="col-sm-12" style="background: url(<?= Url::home(); ?>images/univesity_fon.png) center;background-size:cover;background-repeat:no-repeat;padding:0px;min-height:93vh;">
    
    <div class="col-sm-10" style='padding:0px;'>
        <div class="col-sm-12" style='padding:0px;background-color:white;'>
            <?= $this->render('menu',['role'=>$role]); ?>
        </div>
        <div class="col-sm-12" style="padding-top: 20px;">
            <div class="col-sm-12" style='max-height:100vh;background-color:white;box-shadow: 0 0 10px rgba(0,0,0,0.2);padding:0px;'>
                    <table class='table' style="font-size:14px;padding:0px;margin:0px;padding-bottom:15px;">
                        <tr>
                            <td>Job</td>
                            <td>Company</td>
                            <td>Area</td>
                            <td>Expected Salary</td>
                            <td>Data listed</td>
                            <!--<td>Apply</td>-->
                            <td></td>
                        </tr>
                        <?php foreach($modelJobs as $jobs){?>
                            <tr>
                                <!--<td>
                                    <?php /* if($jobs['logo_src'] != ''){ ?>
                                        <img src="<?= Url::home().'images/jobs_logo/'.$jobs['logo_src']; ?>" style="width:70px;">
                                    <?php }else{ ?>
                                        <img src="<?= Url::home(); ?>images/noavatar.png" style="width:70px;">
                                    <?php } */ ?>
                                </td> -->
                                <td><?= HTML::a($jobs['title'],Url::home().'job/'.$jobs['id']); ?></td>
                                <td><?= HTML::a($jobs['name'],Url::home().'company/'.$jobs['company_id']); ?></td>
                                <td><?= $jobs['address']; ?></td>
                                <td><?= $jobs['salary']; ?>/Month</td>
                                <td><?= $jobs['date_create']; ?></td>
                                <!--<td>
                                    <a class="unapplyJob" data-job_id="<?php //= $jobs['id']; ?>" style="cursor:pointer;">Unapply</a>
                                </td> -->
                                <td><?= HTML::a('<img src="'.Url::home().'images/icon/eye.png" class="pull-left" style="margin-top:5px;width:14px;"> View',Url::home().'job/'.$jobs['jobs_id'],['class' => 'btn', 'style' => 'font-size:12px;width:100%;background-color:#5ec6ed;color:white;padding-top:0px;padding-bottom:0px;']); ?></td>
                            </tr>
                        <?php }?>
                    </table>
                </div>
        </div>
    </div>
    <div class="col-sm-2" style='padding: 0px;background-color:#525274;position:relative;'>
        <div class="col-sm-12" style="padding:0px;position:fixed;">
            <div class="col-sm-12" style="padding:0px;background-color:#525274;min-height:100vh">
                <?= $this->render('right_menu',['role'=>$role]); ?>
            </div>
        </div>
    </div>
</div>
