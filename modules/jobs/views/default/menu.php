<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\ActiveForm;
	
	$class1 = ($this->context->getRoute() == 'admin/stormer')?'active':''; 
	$class2 = ($this->context->getRoute() == 'admin/customer')?'active':''; 
?>

	<div class="col-sm-12">
		<div class="row">
			<?php $form = ActiveForm::begin([
				'method' => 'get',
				'action' => ['searchjob'],
				'options' => ['class' => 'top-search-form']
			]); ?>
				<?php
					$type_search = '';
					if(isset($_GET['type_search'])){
					$type_search = $_GET['type_search'];
				}
				?>
				<div class="input-group">
					<div class="form-group top-input-size-style">
						<?php if(isset($_GET['jobs_value'])){ ?>
							<input type="text" name="jobs_value" class="form-control searchInput " placeholder="Enter search text..."  value="<?= $_GET['jobs_value']; ?>">
						<?php }else{ ?>
							<input type="text" name="jobs_value" class="form-control searchInput" placeholder="Enter search text..." >
						<?php } ?>
						<?= Html::dropDownList('type_search',$type_search,['name'=>'Name','tags'=>'Tags','industry'=>'Industry'],['class'=>'form-control']) ?>
					</div>
				</div>

			<?php $form = ActiveForm::end(); ?>
			<button id="search_bt" class="filter-btn-style" style="position: absolute; top: 1px; right: 0;">Fiter</button>
		</div>
	</div>