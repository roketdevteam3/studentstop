<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Alert;
use yii\helpers\Url;
use app\widgets\UserjobsWidget;

use app\assets\BusinessAsset;
BusinessAsset::register($this);

?>
<div style="display:none;">
    <div class="table table-striped" class="files" id="previews">
        <div id="template" class="file-row">
            <!-- This is used as the file preview template -->
            <div>
                <span class="preview"><img data-dz-thumbnail /></span>
            </div>
            <div>
                <p class="name" data-dz-name></p>
                <strong class="error text-danger" data-dz-errormessage></strong>
            </div>
            <div>
                <button data-dz-remove class="btn btn-danger delete">
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="row">
  <?= $this->render('right_menu',['role'=>$role]); ?>
    <div class="jobs-content">
        <div class="container-fluid background-block">
            <div class="row">
              <?= $this->render('menu',['role'=>$role]); ?>

              <?php //= HTML::a('Back to company jobs', Url::home().'company/'.$modelJobs->company_id,['class'=>'btn btn-default']);?>
                <div class="col-sm-12">
                    <div class="col-sm-12">
                        <div class="top-vacancies">
                            <div class="top-v-item">
                                <div class="jobs-logo col-sm-4">
                                  <?php if($company->logo_src != ''){ ?>
                                      <img class="img-responsive" src="<?= Url::home().'images/company_logo/'.$company->logo_src; ?>">
                                  <?php }else{ ?>
                                      <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                                  <?php } ?>
                                </div>
                                <div class="col-sm-8">
                                    <h4><?= $company->name; ?></h4>
                                    <table class="jobs-skills">
                                        <tr>
                                            <td>Addres</td>
                                            <td> <?= $company->address; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Industrial</td>
                                            <td><?=$company->industrial['name']?></td>
                                        </tr>
                                        <tr>
                                            <td>Website</td>
                                            <td><?=$company->website?></td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td><?=$company->email?></td>
                                        </tr>
                                        <tr>
                                            <td>Phone</td>
                                            <td><?=$company->phone?></td>
                                        </tr>
                                        <tr>
                                            <td>Frends</td>
                                            <td><?=count($friends)?></td>
                                        </tr>
                                        <tr>
                                            <td>Vacansies</td>
                                            <td><?=count($jobs)?></td>
                                        </tr>
                                        <tr>
                                            <td>Connect</td>
                                            <td><?=\app\widgets\FavoritecompanyWidget::widget(['company_id'=>$company->id])?></td>
                                        </tr>
                                        <tr>
                                            <td>About</td>
                                            <td> <?=$company->about?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div>

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs color_style_li" style="    margin: 0px auto;   max-width: 1170px;     background-color: white;    margin-bottom: 10px;" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#vacansies" aria-controls="vacansies" role="tab" data-toggle="tab">
                                    Vacansies <span class="badge"><?=count($jobs)?></span>
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#friends" aria-controls="friends" role="tab" data-toggle="tab">
                                    Friends <span class="badge"><?=count($friends)?></span>
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#news" aria-controls="news" role="tab" data-toggle="tab">
                                    News <span class="badge"><?=\app\modules\jobs\models\CompanyNews::find()->where(['company_id'=>$company->id])->count()?></span>
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#favorite" aria-controls="favorite" role="tab" data-toggle="tab">
                                    Favorite <span class="badge"><?=count($rezume_favorites)?></span>
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#offers" aria-controls="offers" role="tab" data-toggle="tab">
                                    Offers <span class="badge"><?=count($offersUsers)?></span>
                                </a>
                            </li>

                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="vacansies">
                                <div class="top-vacancies">
                                  <?php foreach($jobs as $job){ ?>
                                      <div class="top-v-item">
                                          <div class="jobs-logo col-sm-4">
                                            <?php if($job->logo_src != ''){ ?>
                                                <img class="img-responsive" src="<?= Url::home().'images/jobs_logo/'.$job->logo_src; ?>">
                                            <?php }else{ ?>
                                                <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                                            <?php } ?>
                                          </div>
                                          <div class="col-sm-8">
                                              <h4><?= $job->title; ?></h4>
                                              <table class="jobs-skills">
                                                  <tr>
                                                      <td>Addres</td>
                                                      <td><?= $job->address; ?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>Salary</td>
                                                      <td><?= $job->salary; ?></td>
                                                  </tr>
                                                  <tr <?=($job->university_id == $userRezume->userinfo['university_id'])?'style="color:green"':''?>>
                                                      <td>University</td>
                                                      <td><?= $job->university['name']; ?></td>
                                                  </tr>
                                                  <tr <?=($job->industry_id == $userRezume->userinfo['industry_id'])?'style="color:green"':''?>>
                                                      <td>Industry</td>
                                                      <td><?= $job->industry['name']; ?></td>
                                                  </tr>
                                                  <tr <?=($job->function_id == $userRezume->userinfo['jobs_function_id'])?'style="color:green"':''?>>
                                                      <td>Job function</td>
                                                      <td><?= $job->jobfunction['name']; ?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>Favorite</td>
                                                      <td><?=\app\modules\jobs\models\Favoritejobs::find()->where(['jobs_id'=>$job->id])->count()?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>Apply</td>
                                                      <td><?=\app\modules\jobs\models\Userjobs::find()->where(['jobs_id'=>$job->id])->count()?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>About</td>
                                                      <td><?= substr($job->about, 0, 60).'...'; ?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>Tags</td>
                                                      <td>
                                                        <?php $array_skills_vak = explode(",", $job->skills);
                                                        foreach ($array_skills_vak as $value) {
                                                          if (in_array($value, $array_skills_user)) {
                                                            echo '<span class="label label-success">'.$value.'</span> ';
                                                          } else
                                                            echo '<span class="label label-default">'.$value.'</span> ';
                                                        }
                                                        ?>
                                                      </td>
                                                  </tr>
                                              </table>
                                          </div>
                                          <div class="btn-wrap text-center">
                                            <?php if($job->company['user_id'] != \Yii::$app->user->id){ ?>
                                              <?= \app\widgets\FavoritejobsWidget::widget(['jobs_id' => $job->id]) ?>
                                            <?php } ?>
                                            <?= HTML::a('View',Url::home().'job/'.$job->id,['class' => 'view-job']); ?>
                                            <?php if($job->company['user_id'] != \Yii::$app->user->id){ ?>
                                              <?= UserjobsWidget::widget(['jobs_id' => $job->id,'page_type' => 'dashboard']) ?>
                                            <?php } ?>
                                          </div>
                                      </div>
                                  <?php }
                                  echo \yii\widgets\LinkPager::widget([
                                    'pagination' => $pages_jobs,
                                  ]);?>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="friends">
                                <div class="top-vacancies">
                                  <?php foreach($friends as $item){?>
                                      <div class="top-v-item">
                                          <div class="jobs-logo col-sm-4">
                                            <?php if($item->rezume['avatar'] != ''){ ?>
                                                <img class="img-responsive" src="<?= Url::home().'images/users_images/'.$item->rezume['avatar']; ?>">
                                            <?php }else{ ?>
                                                <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                                            <?php } ?>
                                          </div>
                                          <div class="col-sm-5">
                                              <h4><?= $item->rezume->user['name'].' '.$item->rezume->user['surname']; ?></h4>
                                            <?=\app\widgets\rating\UserratingWidget::widget(['user_id'=>$item->rezume['user_id']])?>
                                              <table class="jobs-skills">
                                                  <tr>
                                                      <td>Addres</td>
                                                      <td> <?= $item->rezume->userinfo['address']; ?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>University</td>
                                                      <td><?=$item->rezume->userinfo->university['name']?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>Industry</td>
                                                      <td><?=$item->userinfo->industry['name']?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>Job function</td>
                                                      <td><?=$item->userinfo->jobfunction['name']?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>GPA overall</td>
                                                      <td><?=$item->rezume['overall_gpa']?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>GPA current</td>
                                                      <td><?=$item->rezume['current_gpa']?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>Matches vacancies</td>
                                                    <?php
                                                    $matches_vacancies = \app\modules\jobs\models\Jobs::find()
                                                      ->where(['company_id'=>$modelCompany->id,'industry_id'=>$item->userinfo['industry_id']])
                                                      ->orWhere(['function_id'=>$item->userinfo['jobs_function_id']])
                                                      ->orWhere([ 'major_id'=>$item->userinfo['major_id']])
                                                      ->all();
                                                    ?>
                                                      <td><a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();"><?=count($matches_vacancies)?></a></td>
                                                  </tr>
                                                  <tr>
                                                      <td>Favorite company</td>
                                                      <td>
                                                          <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();"><?=\app\modules\jobs\models\FavoriteCompany::find()->where(['user_id'=>$item->rezume['user_id']])->count()?></a>
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td>Favorite jobs</td>
                                                      <td>
                                                          <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();"><?=\app\modules\jobs\models\Favoritejobs::find()->where(['user_id'=>$item->rezume['user_id']])->count()?></a>
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td>Apply</td>
                                                      <td>
                                                          <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();">
                                                            <?=\app\modules\jobs\models\Userjobs::find()->where(['user_id'=>$item->rezume['user_id']])->count()?>
                                                          </a>
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td>Offer</td>
                                                      <td>
                                                          <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();">
                                                            <?=\app\modules\jobs\models\OfferJob::find()->where(['user_id'=>$item->rezume['user_id']])->count()?>
                                                          </a>
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td>About</td>
                                                      <td> <?= substr($item->rezume['about'], 0, 60).'...'; ?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>Tags</td>
                                                      <td>
                                                        <?php $array_skills_user = explode(",", $item->rezume['skills']);
                                                        foreach ($array_skills_user as $value) {
                                                          echo '<span class="label label-default">'.$value.'</span> ';
                                                        }
                                                        ?>
                                                      </td>
                                                  </tr>
                                              </table>
                                          </div>
                                          <?php if($role!='student'){?>
                                          <div class="col-sm-3">
                                           <button class="view-job startChat" data-sender-id="<?=Yii::$app->user->id?>" data-recipient-id="<?=$item->rezume['user_id']?>">Chat</button>

                                            <?= HTML::a('View',Url::home().'rezume/'.$item->rezume['id'],['class' => 'view-job']); ?>
                                            <?= \app\widgets\FavoriterezumeWidget::widget(['rezume_id'=>$item->rezume['id'], 'job_id'=>0])?>
                                          </div>
                                          <?php } ?>
                                      </div>
                                  <?php }?>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="news">
                                <div class="top-vacancies">

                                  <?php foreach ($news as $item){?>
                                      <div class="top-v-item">
                                        <?=$item->desc?>
                                        <?php
                                        $commits = \app\modules\jobs\models\CommitNewsCompany::find()->where(['new_id'=>$item->id])->orderBy(['id'=>SORT_DESC])->all();
                                        ?>
                                          <div>
                                            <?php foreach ($commits as $commit){?>
                                                <div class="row">
                                                    <div class="">
                                                        <div class="user-img">
                                                            <img style="width:40px;" class="" src="/images/users_images/<?=$commit->userinfo['avatar']?>">
                                                        </div>
                                                        <span class="user-name"><?=$commit->user['surname']?> <?=$commit->user['name']?></span>
                                                        <span><?=Yii::$app->formatter->asDatetime($commit->created_at)?></span>
                                                    </div>
                                                    <div class="">
                                                      <?=$commit->commit?>
                                                    </div>
                                                </div>
                                            <?php }?>
                                          </div>
                                       <!-- <?php /*$form2 = ActiveForm::begin();*/?>
                                          <div class="col-sm-8">
                                            <?/*= $form2->field($commitNew,'commit')->label(false)*/?>
                                            <?/*= $form2->field($commitNew,'new_id')->hiddenInput(['value'=>$item->id])->label(false)*/?>
                                          </div>
                                          <div class="col-sm-4">
                                              <button type="submit" name="commit_send" class="btn btn-create btn-ripple">Add</button>
                                          </div>
                                        --><?php /*ActiveForm::end();*/?>
                                      </div>

                                  <?php } ?>
                                  <?php
                                  echo \yii\widgets\LinkPager::widget([
                                    'pagination' => $pages_news,
                                  ]);
                                  ?>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="offers">
                                <div class="top-vacancies">
                                  <?php foreach($offersUsers as $item){ ?>
                                      <div class="top-v-item">
                                          <div class="jobs-logo col-sm-4">
                                            <?php if($item->avatar != ''){ ?>
                                                <img class="img-responsive" src="<?= Url::home().'images/users_images/'.$item->avatar; ?>">
                                            <?php }else{ ?>
                                                <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                                            <?php } ?>
                                          </div>
                                          <div class="col-sm-5">
                                              <h4><?= $item->user['name'].' '.$item->user['surname']; ?></h4>
                                            <?=\app\widgets\rating\UserratingWidget::widget(['user_id'=>$item->user_id])?>
                                              <table class="jobs-skills">
                                                  <tr>
                                                      <td>Addres</td>
                                                      <td> <?= $item->userinfo['address']; ?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>University</td>
                                                      <td><?=$item->userinfo->university['name']?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>Industry</td>
                                                      <td><?=$item->userinfo->industry['name']?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>Job function</td>
                                                      <td><?=$item->userinfo->jobfunction['name']?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>GPA overall</td>
                                                      <td><?=$item->overall_gpa?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>GPA current</td>
                                                      <td><?=$item->current_gpa?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>Matches vacancies</td>
                                                    <?php
                                                    $matches_vacancies = \app\modules\jobs\models\Jobs::find()
                                                      ->where(['company_id'=>$modelCompany->id,'industry_id'=>$item->userinfo['industry_id']])
                                                      ->orWhere(['function_id'=>$item->userinfo['jobs_function_id']])
                                                      ->orWhere([ 'major_id'=>$item->userinfo['major_id']])
                                                      ->all();
                                                    ?>
                                                      <td><a onclick="$('#view_user_<?=$item->user_id?>').show();"><?=count($matches_vacancies)?></a></td>
                                                  </tr>
                                                  <tr>
                                                      <td>Favorite company</td>
                                                      <td>
                                                          <a onclick="$('#view_user_<?=$item->user_id?>').show();"><?=\app\modules\jobs\models\FavoriteCompany::find()->where(['user_id'=>$item->user_id])->count()?></a>
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td>Favorite jobs</td>
                                                      <td>
                                                          <a onclick="$('#view_user_<?=$item->user_id?>').show();"><?=\app\modules\jobs\models\Favoritejobs::find()->where(['user_id'=>$item->user_id])->count()?></a>
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td>Apply</td>
                                                      <td>
                                                          <a onclick="$('#view_user_<?=$item->user_id?>').show();">
                                                            <?=\app\modules\jobs\models\Userjobs::find()->where(['user_id'=>$item->user_id])->count()?>
                                                          </a>
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td>Offer</td>
                                                      <td>
                                                          <a onclick="$('#view_user_<?=$item->user_id?>').show();">
                                                            <?=\app\modules\jobs\models\OfferJob::find()->where(['user_id'=>$item->user_id])->count()?>
                                                          </a>
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td>About</td>
                                                      <td> <?= substr($item->about, 0, 60).'...'; ?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>Tags</td>
                                                      <td>
                                                        <?php $array_skills_user = explode(",", $item->skills);
                                                        foreach ($array_skills_user as $value) {
                                                          echo '<span class="label label-default">'.$value.'</span> ';
                                                        }
                                                        ?>
                                                      </td>
                                                  </tr>
                                              </table>
                                          </div>
                                          <div class="col-sm-3">
                                            <button class="view-job startChat" data-sender-id="<?=Yii::$app->user->id?>" data-recipient-id="<?=$item->user_id?>">Chat</button>

                                            <?= HTML::a('View',Url::home().'rezume/'.$item->id,['class' => 'view-job']); ?>
                                            <?= \app\widgets\FavoriterezumeWidget::widget(['rezume_id'=>$item->id, 'job_id'=>0])?>
                                          </div>
                                      </div>
                                  <?php }?>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="favorite">
                                <div class="top-vacancies">
                                  <?php foreach($rezume_favorites as $item){?>
                                      <div class="top-v-item">
                                          <div class="jobs-logo col-sm-4">
                                            <?php if($item->rezume['avatar'] != ''){ ?>
                                                <img class="img-responsive" src="<?= Url::home().'images/users_images/'.$item->rezume['avatar']; ?>">
                                            <?php }else{ ?>
                                                <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                                            <?php } ?>
                                          </div>
                                          <div class="col-sm-5">
                                              <h4><?= $item->rezume->user['name'].' '.$item->rezume->user['surname']; ?></h4>
                                            <?=\app\widgets\rating\UserratingWidget::widget(['user_id'=>$item->rezume['user_id']])?>
                                              <table class="jobs-skills">
                                                  <tr>
                                                      <td>Addres</td>
                                                      <td> <?= $item->rezume->userinfo['address']; ?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>University</td>
                                                      <td><?=$item->rezume->userinfo->university['name']?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>Industry</td>
                                                      <td><?=$item->userinfo->industry['name']?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>Job function</td>
                                                      <td><?=$item->userinfo->jobfunction['name']?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>GPA overall</td>
                                                      <td><?=$item->rezume['overall_gpa']?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>GPA current</td>
                                                      <td><?=$item->rezume['current_gpa']?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>Matches vacancies</td>
                                                    <?php
                                                    $matches_vacancies = \app\modules\jobs\models\Jobs::find()
                                                      ->where(['company_id'=>$modelCompany->id,'industry_id'=>$item->userinfo['industry_id']])
                                                      ->orWhere(['function_id'=>$item->userinfo['jobs_function_id']])
                                                      ->orWhere([ 'major_id'=>$item->userinfo['major_id']])
                                                      ->all();
                                                    ?>
                                                      <td><a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();"><?=count($matches_vacancies)?></a></td>
                                                  </tr>
                                                  <tr>
                                                      <td>Favorite company</td>
                                                      <td>
                                                          <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();"><?=\app\modules\jobs\models\FavoriteCompany::find()->where(['user_id'=>$item->rezume['user_id']])->count()?></a>
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td>Favorite jobs</td>
                                                      <td>
                                                          <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();"><?=\app\modules\jobs\models\Favoritejobs::find()->where(['user_id'=>$item->rezume['user_id']])->count()?></a>
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td>Apply</td>
                                                      <td>
                                                          <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();">
                                                            <?=\app\modules\jobs\models\Userjobs::find()->where(['user_id'=>$item->rezume['user_id']])->count()?>
                                                          </a>
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td>Offer</td>
                                                      <td>
                                                          <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();">
                                                            <?=\app\modules\jobs\models\OfferJob::find()->where(['user_id'=>$item->rezume['user_id']])->count()?>
                                                          </a>
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td>About</td>
                                                      <td> <?= substr($item->rezume['about'], 0, 60).'...'; ?></td>
                                                  </tr>
                                                  <tr>
                                                      <td>Tags</td>
                                                      <td>
                                                        <?php $array_skills_user = explode(",", $item->rezume['skills']);
                                                        foreach ($array_skills_user as $value) {
                                                          echo '<span class="label label-default">'.$value.'</span> ';
                                                        }
                                                        ?>
                                                      </td>
                                                  </tr>
                                              </table>
                                          </div>
                                          <div class="col-sm-3">

                                              <button class="view-job startChat" data-sender-id="<?=Yii::$app->user->id?>" data-recipient-id="<?=$item->rezume['user_id']?>">Chat</button>

                                            <?= HTML::a('View',Url::home().'rezume/'.$item->rezume['id'],['class' => 'view-job']); ?>
                                            <?= \app\widgets\FavoriterezumeWidget::widget(['rezume_id'=>$item->rezume['id'], 'job_id'=>0])?>
                                          </div>
                                      </div>
                                  <?php }?>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>
</div>