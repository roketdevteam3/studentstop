<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    
?>
<?php 
    $class1 = ($this->context->getRoute() == 'jobs/default/myapplyjob')?'active':''; 
    $class2 = ($this->context->getRoute() == 'jobs/default/myalertjobs')?'active':''; 
    $class3 = ($this->context->getRoute() == 'jobs/default/myresume')?'active':''; 
?>
<div class="col-sm-12">
    <ul class="nav nav-tabs">
        <li role="presentation" class="<?= $class1; ?>">
            <?= HTML::a('Applied jobs',Url::home().'dashboard/myapplyjob'); ?>
        </li>
        <li role="presentation" class="<?= $class2; ?>">
            <?= HTML::a('Job alerts',Url::home().'dashboard/myalertjobs'); ?>
        </li>
        
    </ul>
</div>
            