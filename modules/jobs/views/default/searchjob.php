<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use app\assets\BusinessAsset;
use app\widgets\UserjobsWidget;
use app\widgets\FavoritejobsWidget;

BusinessAsset::register($this);

use app\assets\ApplyjobsAsset;
ApplyjobsAsset::register($this);

?>

	<div class="row">
		<?= $this->render('right_menu',['role'=>$role]); ?>
		<div class="jobs-content">
			<div class="container-fluid background-block">
				<div class="row">
					<?= $this->render('menu',['role'=>$role]); ?>
					<div class="col-sm-12">
						<div class="main-content">
							<table class="table">
								<thead>
									<tr>
										<td>Job</td>
										<td>Company</td>
										<td>Area</td>
										<td>Expected Salary</td>
										<td>Data listed</td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
								</thead>
								<tbody>
									<?php foreach($modelAllJobs as $jobs){ ?>
										<?php  //var_dump($jobs); ?>
										<tr>
											<td>
												<?= HTML::a($jobs['title'],Url::home().'job/'.$jobs['id']); ?>
											</td>
											<td>
												<?= HTML::a($jobs['name'],Url::home().'company/'.$jobs['company_id']); ?>
											</td>
											<td>
												<?= $jobs['jobsAddress']; ?>
											</td>
											<?php $salaryD = [0 => 'none', 1 => 'day', 2 => 'week', 3 => 'month', 4 => 'year']; ?>
											<td>
												<?= $jobs['salary'].'/'.$salaryD[$jobs['salary_date']]; ?>
											</td>
											<td>
												<?php $date = date("d.m.Y", strtotime($jobs['jobsCreate']));
													  echo $date; ?>
											</td>
											<td>
												<?php if($jobs['user_id'] != \Yii::$app->user->id){ ?>
													<?= UserjobsWidget::widget(['jobs_id' => $jobs['id'],'page_type' => 'search']) ?>
												<?php } ?>
												<?php /* ?>
												<a class="unapplyJob" data-job_id="<?= $jobs['id']; ?>" style="cursor:pointer;">Unapply</a>
												<?php */ ?>
											</td>
											<td>
												<?= HTML::a('View',Url::home().'job/'.$jobs['id'], ['class' => 'btn view-job']); ?>
											</td>
											<td>
												<?php if($jobs['user_id'] != \Yii::$app->user->id){ ?>
													<?= FavoritejobsWidget::widget(['jobs_id' => $jobs['id']]) ?>
												<?php } ?>
												<?php /* ?>
												<button class="btn addToFavorite" data-job_id="<?= $jobs['id']; ?>" style="padding:0px;font-size:11px;padding-top:0px; padding-bottom:0px;width:100px;background-color:transparent;">Add to favorite<i class="fa fa-star pull-right" style="color:#f1f1f1;"></i></button>-->
												<?php */ ?>
											</td>
										</tr>
									<?php }?>
								<tbody>
							</table>
							<div class="col-sm-12">
								<?= LinkPager::widget(['pagination'=>$pagination]); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>