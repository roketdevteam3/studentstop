<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use app\assets\BusinessAsset;
BusinessAsset::register($this);

use app\assets\ApplyjobsAsset;
ApplyjobsAsset::register($this);

?>

	<div class="row">
		<?= $this->render('right_menu',['role'=>$role]); ?>
		<div class="jobs-content">
			<div class="container-fluid background-block">
				<div class="row">
					<?= $this->render('menu',['role'=>$role]); ?>
					<div class="col-sm-12">
						<div class="row">
							<div class="padding-block">
								<?php if($modelCompany != null){ ?>
									<div class="employer-company">
										<div class="employer-company-logo">
											<?php if($modelCompany->logo_src == ''){ ?>
												<img class="img-responsive" src="<?= Url::home(); ?>images/default_company_logo.jpg" style="width:100%;">
											<?php }else{ ?>
												<img class="img-responsive" src="<?= Url::home().'images/company_logo/'.$modelCompany->logo_src; ?>" style="width:100%;">
											<?php } ?>
										</div>
										<div class="employer-company-info">
											<h4 class="employer-company-name">Company name - <span><?= $modelCompany->name; ?></span></h4>
											<p class="employer-company-about"><?= $modelCompany->about; ?></p>
										</div>
									</div>
								<?php }else{ ?>
									<ul class="nav nav-tabs" style="padding:0px;">
										<li role="presentation">
											<?= HTML::a('All companies',Url::home().'companies/company'); ?>
										</li>
										<li role="presentation" class="active">
											<?= HTML::a('All jobs',Url::home().'companies/jobs'); ?>
										</li>
									</ul>
								<?php } ?>
								<div class="top-vacancies">
									<?php foreach($modelAllJobs as $jobs){ ?>
										<div class="top-v-item">
											<div class="jobs-rating">
												<i class="fa fa-star-o"></i><br>
												<i class="jobs-rating-active fa fa-star"></i><br>
												<i class="jobs-rating-active fa fa-star"></i><br>
												<i class="jobs-rating-active fa fa-star"></i><br>
												<i class="jobs-rating-active fa fa-star"></i><br>
											</div>
											<div class="jobs-logo">
												<?php if($jobs->logo_src != ''){ ?>
													<img class="img-responsive" src="<?= Url::home().'images/jobs_logo/'.$jobs->logo_src; ?>">
												<?php }else{ ?>
													<img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
												<?php } ?>
											</div>
											<div class="job-info">
												<h4><?= $jobs->title; ?></h4>
												<p class="job-address"><i><?= $jobs->address; ?></i></p>
												<p class="job-desc"><?= substr($jobs->about, 0, 60).'...'; ?></p>
											</div>
											<div class="ex-salary">
												<h4>Expected salary</h4>
												<p><?= $jobs->salary; ?>/month</p>
											</div>
											<div class="btn-wrap text-center">
												<?= HTML::a('View',Url::home().'job/'.$jobs->id,['class' => 'btn view-job']); ?>
												<?php if($jobs->status == null){ ?>
													<button class="btn applyJob" data-status="" data-job_id="<?= $jobs->id; ?>">Apply</button>
												<?php } ?>
											</div>
										</div>
									<?php } ?>
									<div class="col-sm-12">
										<?= LinkPager::widget(['pagination'=>$pagination]); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>