<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use app\assets\BusinessAsset;
use app\widgets\UserjobsWidget;
use app\widgets\FavoritejobsWidget;

BusinessAsset::register($this);

use app\assets\ApplyjobsAsset;
ApplyjobsAsset::register($this);

?>
   
<div class="col-sm-12" style="background: url(<?= Url::home(); ?>images/univesity_fon.png) center;background-size:cover;background-repeat:no-repeat;padding:0px;min-height:93vh;">
    <div class="col-sm-10" style='padding:0px;'>
        <div class="col-sm-12" style="padding:0px;background-color: white">
            <?= $this->render('menu',['role'=>$role]); ?>
        </div>
        <div class="col-sm-12" style="margin-top:10px;">
            
            <div class="col-sm-12">
                <div class="col-sm-12" style="box-shadow: 0 0 10px rgba(0,0,0,0.1);margin-bottom:20px;background-color:white;border-radius:5px;padding:0px">
                    <form>
                            <?php $form = ActiveForm::begin(['method' => 'GET']); ?>
                                    <div class="col-sm-2">
                                        <?php 
                                                $username = '';
                                                if(isset($_GET['username'])){
                                                        $username = $_GET['username'];
                                                } 
                                        ?>
                                        <input type='text' name='username' class='jobsFilterUser' value='<?= $username; ?>' placeholder='name or surname'>
                                    </div>
                                    <div class="col-sm-2">
                                        
                                        <?php 
                                            $university = '';
                                            if(isset($_GET['university'])){
                                                    $university = $_GET['university'];
                                            }else{
                                                    $university= null;
                                            }
                                        ?>
                                        <?= Html::dropDownList('university', $university, $universityArray,['prompt' => 'University','class' => 'jobsFilterUser']); ?>
                                    </div>
                                    
                                    <div class="col-sm-2">
                                        <?php 
                                            $job_function = '';
                                            if(isset($_GET['job_function'])){
                                                    $job_function = $_GET['job_function'];
                                            }else{
                                                    $job_function = null;
                                            }
                                        ?>
                                        <?= Html::dropDownList('job_function', $job_function,$jobfunctionArray,['prompt' => 'Job function','class' => 'jobsFilterUser']); ?>
                                    </div>

                                    <div class="col-sm-2">
                                        <?php 
                                            $industry = '';
                                            if(isset($_GET['industry'])){
                                                    $industry = $_GET['industry'];
                                            }else{
                                                    $industry = null;
                                            }
                                        ?>
                                        <?= Html::dropDownList('industry', $industry,$industryArray,['prompt' => 'Industry','class' => 'jobsFilterUser']); ?>
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="submit" name="searchButton" value="Search" class="jobsFilterUser">
                                    </div>
                                    
                            <?php $form = ActiveForm::end();  ?>
                    </form>
                </div>
            </div>
            
            <div class="col-sm-12">
                    <?php foreach($modelUsers as $users){ ?>
                        <div class="col-sm-12" style='margin-bottom:20px;background-color:white;box-shadow: 0 0 10px rgba(0,0,0,0.2);padding:10px;'>
                            <div class="col-sm-2" style="padding-left:5px;">
                                <a href='<?= Url::home().'profile/'.$users['id']; ?>'>
                                    <img src="<?= Url::home(); ?>images/users_images/<?= $users['avatar']; ?>" style="width:100%;" alt="">
                                </a>
                            </div>
                            <div class="col-sm-3">
                                <p  class="title userNameSurname" style="text-align:left;color:#3e50b4 !important;font-size:14px;">
                                    <a href='<?= Url::home().'profile/'.$users['id']; ?>'>
                                        <?= $users['name'].' '.$users['surname']; ?>
                                    </a>
                                </p>
                            </div>
                            <div class="col-sm-2">
                                <p style="font-size:12px;"><?= $users['address']; ?></p>
                                <p style="font-size:12px;"><?= $users['academic_status']; ?></p>
                            </div>
                            <div class="col-sm-2">

                            </div>
                            <div class="col-sm-3">
                                <div class="col-xs-9 no-padding">
                                    <?= HTML::a('<span class="fa fa-user"></span> Profile',Url::home().'profile/'.\Yii::$app->user->id,['class'=>'btn profileJobs-btn', 'style' => 'font-size:12px;']); ?>
                                    <button class="btn followJobs-btn" style='font-size:14px;'><i class="fa fa-check"></i> Follow</button>
                                </div>
                                <div class="col-xs-3 no-padding">
                                    <?php if($user['id_user'] != \Yii::$app->user->id){ ?>
                                        <div class="">
                                            <span class="startChat startChatJobs" data-sender-id="<?=\Yii::$app->user->id?>" data-recipient-id="<?= $user['id_user']?>"></span>
                                        </div>
                                        <div class="">
                                            <span class="ifonline startVideo  startVideoJobs" data-sender-id="<?=\Yii::$app->user->id?>" data-recipient-id="<?=$user['id_user']?>"></span>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
            </div>
            <div class="col-sm-12">
                <?= LinkPager::widget(['pagination'=>$pagination]); ?>
            </div>
        </div>
    </div>
    <div class="col-sm-2" style='padding: 0px;background-color:#525274;position:relative;'>
        <div class="col-sm-12" style="padding:0px;position:fixed;">
            <div class="col-sm-12" style="padding:0px;background-color:#525274;min-height:100vh">
                <?= $this->render('right_menu',['role'=>$role]); ?>
            </div>
        </div>
    </div>
</div>