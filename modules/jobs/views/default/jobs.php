<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use app\assets\BusinessAsset;
use app\widgets\UserjobsWidget;
use app\widgets\FavoritejobsWidget;

BusinessAsset::register($this);

use app\assets\ApplyjobsAsset;
ApplyjobsAsset::register($this);

?>
	<?php
		if(Yii::$app->session->hasFlash('jobs_update')):
			echo Alert::widget([
				'options' => [
					'class' => 'alert-info',
				],
				'body' => 'Jobs added',
			]);
		endif; 
		if(Yii::$app->session->hasFlash('jobs_not_update')):
			echo Alert::widget([
				'options' => [
					'class' => 'alert-error',
				],
				'body' => 'Jobs not added',
			]);
		endif; 
	?>
	<div style="display:none;">
		<div class="table table-striped" class="files" id="previews">
			<div id="template" class="file-row">
				<!-- This is used as the file preview template -->
				<div>
					<span class="preview"><img data-dz-thumbnail /></span>
				</div>
				<div>
					<p class="name" data-dz-name></p>
					<strong class="error text-danger" data-dz-errormessage></strong>
				</div>
				<div>
					<p class="size" data-dz-size></p>
					<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
						<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
					</div>
				</div>
				<div>
					<button data-dz-remove class="btn btn-danger delete">
						<i class="glyphicon glyphicon-trash"></i>
						<span>Delete</span>
					</button>
				</div>
			</div>
		</div>
	</div>
	

<div class="row">
	<?= $this->render('right_menu',['role'=>$role]); ?>
	<div class="jobs-content">
		<div class="container-fluid background-block">
			<div class="row">
				<?= $this->render('menu',['role'=>$role]); ?>
				<div class="col-sm-12">
					<div class="vacancy-wrap">
						<button type="button" class="btn btn-add-job" data-toggle="modal" data-target="#addJobs">Add Jobs</button>
						<h3 class="vacancy-head">
							<span>Company</span> vacancies
						</h3>
						<?php if($modelAllJobs == null){ ?>
							<h6 class="vacancy-head">Companies not have vacancies</h6>
						<?php }else{ ?>
							<table class='table'>
								<thead>
									<tr>
										<td>Job</td>
										<td>Company</td>
										<td>Area</td>
										<td>Expected Salary</td>
										<td>Data listed</td>
										<td></td>
										<td>Favorite</td>
										<td></td>
										<td></td>
									</tr>
								</thead>
								<tbody>
									<?php foreach($modelAllJobs as $jobs){ ?>
										<tr>
											<td><?= HTML::a($jobs['title'],Url::home().'job/'.$jobs['id']); ?></td>
											<td><?= HTML::a($jobs['name'],Url::home().'company/'.$jobs['company_id']); ?></td>
											<td><?= $jobs['jobsAddress']; ?></td>
											<?php $salaryD = [0 => 'none', 1 => 'day', 2 => 'week', 3 => 'month', 4 => 'year']; ?>
											<td><?= $jobs['salary'].'/'.$salaryD[$jobs['salary_date']]; ?></td>
											<td>
												<?php $date = date("d.m.Y", strtotime($jobs['jobsCreate']));
													  echo $date; ?>
											</td>
											<td>
												<?php if($jobs['user_id'] != \Yii::$app->user->id){ ?>
													<?= UserjobsWidget::widget(['jobs_id' => $jobs['id'],'page_type' => 'search']) ?>
												<?php } ?>
											</td>
											<td>
											  <?=\app\modules\jobs\models\Favoritejobs::find()->where(['jobs_id'=>$jobs['id']])->count()?>
											</td>
											<td>
												<?= HTML::a('View',Url::home().'job/'.$jobs['id'], ['class' => 'btn btn-view']); ?>
											</td>
											<td>
												<?php if($jobs['user_id'] != \Yii::$app->user->id){ ?>
													<?= FavoritejobsWidget::widget(['jobs_id' => $jobs['id']]) ?>
												<?php } ?>
											</td>
										</tr>
									<?php }?>
								</tbody>
							</table>
						<?php } ?>
						<?= LinkPager::widget(['pagination'=>$pagination]); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="addJobs" class="modal fade" role="dialog" tabindex="-1" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4 class="modal-title">Add job</h4>
			</div>
			<?php $form = ActiveForm::begin(); ?>
				<div class="modal-body">
					<div class="form-content">
						<?= $form->field($modelNewJobs, 'title')->textInput(['class'=>'form-control'])->label('Name'); ?>
						<?= $form->field($modelNewJobs, 'gender')->dropDownList(['0' => 'Male', '1' => 'Female'],['prompt' =>'All Gender'])->label(false); ?>
						<?= $form->field($modelNewJobs, 'academic_status')->dropDownList($userRoleArray,['prompt' => 'All Academic Status'])->label(false); ?>
						<?= $form->field($modelNewJobs, 'about')->textarea(['class'=>'form-control'])->label('About'); ?>
						<?= $form->field($modelNewJobs, 'university_id')->dropDownList($universityArray,['prompt' => 'All University'])->label(false); ?>
						<?= $form->field($modelNewJobs, 'major_id')->dropDownList($majorsArray,['prompt' => 'All Majors'])->label(false); ?>
						<?= $form->field($modelNewJobs, 'address')->textInput(['class'=>'form-control'])->label('Address'); ?>
						<?= $form->field($modelNewJobs, 'industry_id')->dropDownList($jobIndustryArray)->label(false); ?>
						<?= $form->field($modelNewJobs, 'function_id')->dropDownList($jobFunctionArray)->label('Job function'); ?>
						<?= $form->field($modelNewJobs, 'salary')->textInput(['class'=>'form-control'])->label('Salary'); ?>
						<?= $form->field($modelNewJobs, 'salary_date')->dropDownList(['1' => 'day', '2' => 'week', '3' => 'month', '4' => 'year'])->label(false); ?>
						<?= $form->field($modelNewJobs, 'industry_level')->dropDownList(['1' => 'Junior', '2' =>  'Middle', '3' => 'Sinior'])->label('Industry level'); ?>
						<?php
						//	$form->field($modelNewJobs, 'skills')->textInput(['class'=>'form-control'])->label(false);
						?>
						<?=
						$form->field($modelNewJobs, 'skills')->widget(\pudinglabs\tagsinput\TagsinputWidget::classname(), [
						  'options' => [],
						  'clientOptions' => [],
						  'clientEvents' => []
						])->label('Skills');
						?>
						<?= $form->field($modelNewJobs, 'vip_status',['template' => ' {input}{label} {error}{hint}'])->checkbox(['label' => null, 'class'=>'checkbox'],false) ?>
					</div>
					<a class="new_logo_photo btn btn-action">Add logo</a>
					<div class="jobs_logo_photo">
					</div>

					<?= $form->field($modelNewJobs, 'logo_src')->hiddenInput(['class'=>'form-control'])->label(false); ?>
					<?= $form->field($modelNewJobs, 'company_id')->hiddenInput(['value' => $modelCompany->id])->label(false); ?>
					<div class="form-group">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
					<?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'add_business', 'class' => 'btn btn-action']) ?>
				</div>
			<?php $form = ActiveForm::end(); ?>
		</div>
	</div>
</div>