<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap\Alert;
use kartik\date\DatePicker;
use app\assets\BusinessAsset;
BusinessAsset::register($this);

?>

	
	
	<div style="display:none;">
		<div class="table table-striped" class="files" id="previews">
			<div id="template" class="file-row">
			  <!-- This is used as the file preview template -->
			  <div class="col-sm-4">
				  <span class="preview"><img data-dz-thumbnail /></span>
			  </div>
			  <div class="col-sm-5">
				  <p class="name" data-dz-name></p>
				  <strong class="error text-danger" data-dz-errormessage></strong>
				  <p class="size" data-dz-size></p>
				  <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
					<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
				  </div>
			  </div>
			  <div class="col-sm-3">
				<button data-dz-remove class="btn btn-danger delete">
				  <i class="glyphicon glyphicon-trash"></i>
				  <span>Delete</span>
				</button>
			  </div>
			  <div style="clear:both;"></div>
			</div>
		</div>
	</div>
	
<div class="row">
	<?= $this->render('right_menu',['role'=>$role]); ?>
	<div class="jobs-content">
		<div class="container-fluid background-block">
			<div class="row">
				<?= $this->render('menu',['role'=>$role]); ?>
				<div class="col-sm-12">
					<div class="company-form-wrap">
						<h2>My company</h2>
						<?php $form = ActiveForm::begin(); ?>
							<div class="form-content">
								<?= $form->field($modelCompany, 'name')->textInput(['class'=>'form-control'])->label('Name'); ?>
								<?= $form->field($modelCompany, 'about')->textarea(['class'=>'form-control'])->label('About'); ?>
								<?= $form->field($modelCompany, 'phone')->textInput(['class'=>'form-control'])->label('Phone'); ?>
								<?= $form->field($modelCompany, 'fax')->textInput(['class'=>'form-control'])->label('Fax'); ?>
								<?= $form->field($modelCompany, 'email')->textInput(['class'=>'form-control'])->label('Email'); ?>
								<?= $form->field($modelCompany, 'website')->textInput(['class'=>'form-control'])->label('Website'); ?>
								<?= $form->field($modelCompany, 'facebook')->textInput(['class'=>'form-control'])->label('Facebook'); ?>
								<?= $form->field($modelCompany, 'twitter')->textInput(['class'=>'form-control'])->label('Twitter'); ?>
								<?= $form->field($modelCompany, 'linkedin')->textInput(['class'=>'form-control'])->label('Linkedin'); ?>
								<?= $form->field($modelCompany, 'address')->textInput(['class'=>'form-control'])->label('Address'); ?>
								<?php //= $form->field($modelCompany, 'company_create')->textInput(['class'=>'form-control'])->label(false); ?>
								<?php echo $form->field($modelCompany, 'company_create')->widget(DatePicker::classname(), [
										'pluginOptions' => [
											'autoclose'=>true
										]
									])->label('Company create');;
								?>
								<?= $form->field($modelCompany, 'industry_id')->dropDownList($jobIndustryArray)->label(false); ?>
							</div>
							<div class="company_logo_photo_preview">
								<?php if($modelCompany->logo_src != ''){ ?>
									<img src="<?= Url::home().'images/company_logo/'.$modelCompany->logo_src; ?>" style="width:100px;">
								<?php } ?>
							</div>
							<a class="new_logo_photo btn btn-action">change logo</a>
							<div class="company_logo_photo">
								
							</div>
							<?= $form->field($modelCompany, 'logo_src')->hiddenInput(['class'=>'form-control'])->label(false); ?>
							<?= $form->field($modelCompany, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false); ?>
							<?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'add_business', 'class' => 'btn btn-action pull-right']) ?>
							<div style="clear:both;"></div>
						<?php $form = ActiveForm::end(); ?>
					</div>
				</div>
				<div style="clear:both;"></div>
                <div class="col-sm-12 company-alert-style">
                    <?php
                    if(Yii::$app->session->hasFlash('company_update')):
                        echo Alert::widget([
                            'options' => [
                                'class' => 'alert-info',
                            ],
                            'body' => 'Company update',
                        ]);
                    endif;
                    if(Yii::$app->session->hasFlash('company_not_update')):
                        echo Alert::widget([
                            'options' => [
                                'class' => 'alert-error',
                            ],
                            'body' => 'Company not update',
                        ]);
                    endif;
                    ?>
                </div>
			</div>
		</div>
	</div>
</div> 