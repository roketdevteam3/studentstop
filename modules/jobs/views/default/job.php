<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Alert;
use yii\helpers\Url;
use app\widgets\UserjobsWidget;

use app\assets\BusinessAsset;
BusinessAsset::register($this);
$array_skills_jobs = explode(",", $modelJobs->skills);
?>
<div style="display:none;">
	<div class="table table-striped" class="files" id="previews">
		<div id="template" class="file-row">
		  <!-- This is used as the file preview template -->
		  <div>
			  <span class="preview"><img data-dz-thumbnail /></span>
		  </div>
		  <div>
			  <p class="name" data-dz-name></p>
			  <strong class="error text-danger" data-dz-errormessage></strong>
		  </div>
		  <div>
			<button data-dz-remove class="btn btn-danger delete">
			  <i class="glyphicon glyphicon-trash"></i>
			  <span>Delete</span>
			</button>
		  </div>
		</div>
	</div>
</div>

<div id="update_jobs" class="modal fade" role="dialog">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Update job</h4>
		  </div>
			<?php $form = ActiveForm::begin(); ?>
				<div class="modal-body">
					<div class="form-content">
                        <div class="form-content">
                          <?= $form->field($modelJobs, 'title')->textInput(['class'=>'form-control'])->label('Name'); ?>
                          <?= $form->field($modelJobs, 'gender')->dropDownList(['0' => 'Male', '1' => 'Female'],['prompt' =>'All Gender'])->label(false); ?>
                          <?= $form->field($modelJobs, 'academic_status')->dropDownList($userRoleArray,['prompt' => 'All Academic Status'])->label(false); ?>
                          <?= $form->field($modelJobs, 'about')->textarea(['class'=>'form-control'])->label('About'); ?>
                          <?= $form->field($modelJobs, 'university_id')->dropDownList($universityArray,['prompt' => 'All University'])->label(false); ?>
                          <?= $form->field($modelJobs, 'major_id')->dropDownList([],['prompt' => 'All Majors'])->label(false); ?>
                          <?= $form->field($modelJobs, 'address')->textInput(['class'=>'form-control'])->label('Address'); ?>
                          <?= $form->field($modelJobs, 'industry_id')->dropDownList($jobIndustryArray)->label(false); ?>
                          <?= $form->field($modelJobs, 'function_id')->dropDownList($jobFunctionArray)->label('Job function'); ?>
                          <?= $form->field($modelJobs, 'salary')->textInput(['class'=>'form-control'])->label('Salary'); ?>
                          <?= $form->field($modelJobs, 'salary_date')->dropDownList(['1' => 'day', '2' => 'week', '3' => 'month', '4' => 'year'])->label(false); ?>
                          <?= $form->field($modelJobs, 'industry_level')->dropDownList(['1' => 'Junior', '2' =>  'Middle', '3' => 'Sinior'])->label('Industry level'); ?>
                          <?php
                          //	$form->field($modelNewJobs, 'skills')->textInput(['class'=>'form-control'])->label(false);
                          ?>
                          <?=
                          $form->field($modelJobs, 'skills')->widget(\pudinglabs\tagsinput\TagsinputWidget::classname(), [
                            'options' => [],
                            'clientOptions' => [],
                            'clientEvents' => []
                          ])->label('Skills');
                          ?>
                          <?= $form->field($modelJobs, 'vip_status',['template' => ' {input}{label} {error}{hint}'])->checkbox(['label' => null, 'class'=>'checkbox'],false) ?>
                        </div>
					</div>
					<div class="jobs_logo_photo" style="max-width:200px;">
						<?php if($modelJobsAll['jobsLogo'] != ''){ ?>
							<img src="<?= Url::home().'images/jobs_logo/'.$modelJobsAll['jobsLogo']; ?>" class="img_logo_preview" style="width:100%;">
						<?php }else{ ?>
							<img src="<?= Url::home(); ?>images/noavatar.png" class="img_logo_preview" style="width:100%;">
						<?php } ?>
					</div>
					<div class="col-sm-12">
						<a class="new_logo_photo btn btn-success">new logo</a>
					</div>

					<?= $form->field($modelJobs, 'logo_src')->hiddenInput(['class'=>'form-control'])->label(false); ?>
				</div>
		  <div class="modal-footer">
			<?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'add_business', 'class' => 'btn btn-primary pull-right']) ?>
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		  </div>
		<?php $form = ActiveForm::end(); ?>
		</div>
	  </div>
	</div>
	
	<div class="row">
		<?= $this->render('right_menu',['role'=>$role]); ?>
		<div class="jobs-content">
			<div class="container-fluid background-block">
				<div class="row">
					<?= $this->render('menu',['role'=>$role]); ?>
					<?php
						if(Yii::$app->session->hasFlash('jobs_update')):
							echo Alert::widget([
								'options' => [
									'class' => 'alert-info',
								],
								'body' => 'Jobs added',
							]);
						endif; 
						if(Yii::$app->session->hasFlash('jobs_not_update')):
							echo Alert::widget([
								'options' => [
									'class' => 'alert-error',
								],
								'body' => 'Jobs not added',
							]);
						endif; 
					?>
					<?php //= HTML::a('Back to company jobs', Url::home().'company/'.$modelJobs->company_id,['class'=>'btn btn-default']);?>
					<div class="col-sm-6">
                        <?php if($role == 'company'){?>
                            <div>

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" style="background-color: white;" role="tablist">
                                    <li role="presentation" class="active"><a href="#matches" aria-controls="matches" role="tab" data-toggle="tab">Matches <span class="badge"><?=count($modelUserJobsMatches)?></span></a></li>
                                    <li role="presentation"><a href="#favorite" aria-controls="favorite" role="tab" data-toggle="tab">Favorite <span class="badge"><?=count($modelUserJobsFavorite)?></span></a></li>
                                    <li role="presentation"><a href="#applied" aria-controls="applied" role="tab" data-toggle="tab">Applied <span class="badge"><?=count($modelUserJobs)?></span></a></li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="matches">
                                        <div class="top-vacancies">
                                          <?php
                                          foreach($modelUserJobsMatches as $item){
                                            ?>
                                              <div class="top-v-item" style="padding-top: 0px; padding-left: 0px;    margin-top: 10px; height: 374px;">
                                                  <div class=" col-sm-5" style="padding: 0px" >
                                                  <div class="jobs-logo" >
                                                    <?php if($item->avatar != ''){ ?>
                                                        <img class="img-responsive" src="<?= Url::home().'images/users_images/'.$item->avatar; ?>">
                                                    <?php }else{ ?>
                                                        <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                                                    <?php } ?>
                                                  </div>
                                                    <div style="text-align: center; padding-top: 15px; position: relative; top:25px">  <?= HTML::a('Invite','#',[
                                                          'class' => 'view-job',
                                                          'data-toggle'=>"modal",
                                                          'data-target'=>"#offer_job",
                                                          'onclick'=>'$("#user_id_offer").val('.$item->user_id.')'
                                                      ]); ?>
                                                      <?= HTML::a('View',Url::home().'rezume/'.$item->id,['class' => 'view-job']); ?>
                                                  </div>
                                                  </div>
                                                  <div class="col-sm-7">
                                                      <h4 style="display: inline-block;"><?= $item->user['name'].' '.$item->user['surname']; ?></h4>
                                                      <?= \app\widgets\FavoriterezumeWidget::widget(['rezume_id'=>$item->id, 'job_id'=>$modelJobs->id])?>
                                                      <table class="jobs-skills">
                                                          <tr>
                                                              <td>Addres</td>
                                                              <td> <?= $item->userinfo['address']; ?></td>
                                                          </tr>
                                                          <tr  <?=($item->userinfo['university_id'] == $modelJobs->university_id) ? 'style="color: green"':''?> >
                                                              <td>University</td>
                                                              <td><?=$item->userinfo->university['name']?></td>
                                                          </tr>
                                                          <tr  <?=($item->userinfo['industry_id'] == $modelJobs->industry_id) ? 'style="color: green"':''?>>
                                                              <td>Industry</td>
                                                              <td><?=$item->userinfo->industry['name']?></td>
                                                          </tr>
                                                          <tr  <?=($item->userinfo['jobs_function_id'] == $modelJobs->function_id) ? 'style="color: green"':''?>>
                                                              <td>Job function</td>
                                                              <td><?=$item->userinfo->jobfunction['name']?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>GPA overall</td>
                                                              <td><?=$item->overall_gpa?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>GPA current</td>
                                                              <td><?=$item->current_gpa?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>Matches vacancies</td>
                                                            <?php
                                                            $matches_vacancies = \app\modules\jobs\models\Jobs::find()
                                                              ->where(['company_id'=>$modelCompany->id,'industry_id'=>$item->userinfo['industry_id']])
                                                              ->orWhere(['function_id'=>$item->userinfo['jobs_function_id']])
                                                              ->orWhere([ 'major_id'=>$item->userinfo['major_id']])
                                                              ->all();
                                                            ?>
                                                              <td><a onclick="$('#view_user_<?=$item->user_id?>').show();"><?=count($matches_vacancies)?></a></td>
                                                          </tr>
                                                          <tr>
                                                              <td>Favorite company</td>
                                                              <td>
                                                                  <a onclick="$('#view_user_<?=$item->user_id?>').show();"><?=\app\modules\jobs\models\FavoriteCompany::find()->where(['user_id'=>$item->user_id])->count()?></a>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>Favorite jobs</td>
                                                              <td>
                                                                  <a onclick="$('#view_user_<?=$item->user_id?>').show();"><?=\app\modules\jobs\models\Favoritejobs::find()->where(['user_id'=>$item->user_id])->count()?></a>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>Apply</td>
                                                              <td>
                                                                  <a onclick="$('#view_user_<?=$item->user_id?>').show();">
                                                                    <?=\app\modules\jobs\models\Userjobs::find()->where(['user_id'=>$item->user_id])->count()?>
                                                                  </a>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>Offer</td>
                                                              <td>
                                                                  <a onclick="$('#view_user_<?=$item->user_id?>').show();">
                                                                    <?=\app\modules\jobs\models\OfferJob::find()->where(['user_id'=>$item->user_id])->count()?>
                                                                  </a>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>About</td>
                                                              <td> <?= substr($item->about, 0, 60).'...'; ?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>Tags</td>
                                                              <td>
                                                                <?php $array_skills_user = explode(",", $item->skills);
                                                                foreach ($array_skills_user as $value) {
                                                                  if (in_array($value, $array_skills_jobs)) {
                                                                    echo '<span class="label label-success">'.$value.'</span> ';
                                                                  } else
                                                                    echo '<span class="label label-default">'.$value.'</span> ';
                                                                }
                                                                ?>
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </div>

                                              </div>
                                          <?php }?>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="favorite">
                                        <div class="top-vacancies">
                                          <?php
                                          foreach($modelUserJobsFavorite as $item){
                                            ?>
                                              <div class="top-v-item">
                                                  <div class="jobs-logo col-sm-4">
                                                    <?php if($item->rezume['avatar'] != ''){ ?>
                                                        <img class="img-responsive" src="<?= Url::home().'images/users_images/'.$item->rezume['avatar']; ?>">
                                                    <?php }else{ ?>
                                                        <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                                                    <?php } ?>
                                                  </div>
                                                  <div class="col-sm-5">
                                                      <h4><?= $item->rezume->user['name'].' '.$item->rezume->user['surname']; ?></h4>
                                                      <table class="jobs-skills">
                                                          <tr>
                                                              <td>Addres</td>
                                                              <td> <?= $item->rezume->userinfo['address']; ?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>University</td>
                                                              <td><?=$item->rezume->userinfo->university['name']?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>GPA overall</td>
                                                              <td><?=$item->rezume['overall_gpa']?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>GPA current</td>
                                                              <td><?=$item->rezume['current_gpa']?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>Matches vacancies</td>
                                                            <?php
                                                            $matches_vacancies = \app\modules\jobs\models\Jobs::find()
                                                              ->where(['company_id'=>$modelCompany->id,'industry_id'=>$item->userinfo['industry_id']])
                                                              ->orWhere(['function_id'=>$item->userinfo['jobs_function_id']])
                                                              ->orWhere([ 'major_id'=>$item->userinfo['major_id']])
                                                              ->all();
                                                            ?>
                                                              <td><a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();"><?=count($matches_vacancies)?></a></td>
                                                          </tr>
                                                          <tr>
                                                              <td>Favorite company</td>
                                                              <td>
                                                                  <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();"><?=\app\modules\jobs\models\FavoriteCompany::find()->where(['user_id'=>$item->rezume['user_id']])->count()?></a>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>Favorite jobs</td>
                                                              <td>
                                                                  <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();"><?=\app\modules\jobs\models\Favoritejobs::find()->where(['user_id'=>$item->rezume['user_id']])->count()?></a>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>Apply</td>
                                                              <td>
                                                                  <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();">
                                                                    <?=\app\modules\jobs\models\Userjobs::find()->where(['user_id'=>$item->rezume['user_id']])->count()?>
                                                                  </a>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>Offer</td>
                                                              <td>
                                                                  <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();">
                                                                    <?=\app\modules\jobs\models\OfferJob::find()->where(['user_id'=>$item->rezume['user_id']])->count()?>
                                                                  </a>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>About</td>
                                                              <td> <?= substr($item->rezume['about'], 0, 60).'...'; ?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>Tags</td>
                                                              <td>
                                                                <?php $array_skills_user = explode(",", $item->rezume['skills']);
                                                                foreach ($array_skills_user as $value) {
                                                                  if (in_array($value, $array_skills_jobs)) {
                                                                    echo '<span class="label label-success">'.$value.'</span> ';
                                                                  } else
                                                                    echo '<span class="label label-default">'.$value.'</span> ';
                                                                }
                                                                ?>
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </div>
                                                  <div class="col-sm-3">
                                                    <?= HTML::a('Invite','#',[
                                                      'class' => 'view-job',
                                                      'data-toggle'=>"modal",
                                                      'data-target'=>"#offer_job",
                                                      'onclick'=>'$("#user_id_offer").val('.$item->rezume['user_id'].')'
                                                    ]); ?>
                                                    <?= HTML::a('View',Url::home().'rezume/'.$item->rezume['id'],['class' => 'view-job']); ?>
                                                    <?= \app\widgets\FavoriterezumeWidget::widget(['rezume_id'=>$item->rezume['id'], 'job_id'=>$modelJobs->id])?>
                                                  </div>
                                              </div>
                                          <?php }?>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="applied">
                                        <div class="top-vacancies">
                                          <?php
                                          foreach($modelUserJobs as $item){
                                            ?>
                                              <div class="top-v-item">
                                                  <div class="jobs-logo col-sm-4">
                                                    <?php if($item->rezume['avatar'] != ''){ ?>
                                                        <img class="img-responsive" src="<?= Url::home().'images/users_images/'.$item->rezume['avatar']; ?>">
                                                    <?php }else{ ?>
                                                        <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                                                    <?php } ?>
                                                  </div>
                                                  <div class="col-sm-4">
                                                      <h4><?= $item->rezume->user['name'].' '.$item->rezume->user['surname']; ?></h4>
                                                      <table class="jobs-skills">
                                                          <tr>
                                                              <td>Addres</td>
                                                              <td> <?= $item->rezume->userinfo['address']; ?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>University</td>
                                                              <td><?=$item->rezume->userinfo->university['name']?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>GPA overall</td>
                                                              <td><?=$item->rezume['overall_gpa']?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>GPA current</td>
                                                              <td><?=$item->rezume['current_gpa']?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>Matches vacancies</td>
                                                            <?php
                                                            $matches_vacancies = \app\modules\jobs\models\Jobs::find()
                                                              ->where(['company_id'=>$modelCompany->id,'industry_id'=>$item->userinfo['industry_id']])
                                                              ->orWhere(['function_id'=>$item->userinfo['jobs_function_id']])
                                                              ->orWhere([ 'major_id'=>$item->userinfo['major_id']])
                                                              ->all();
                                                            ?>
                                                              <td><a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();"><?=count($matches_vacancies)?></a></td>
                                                          </tr>
                                                          <tr>
                                                              <td>Favorite company</td>
                                                              <td>
                                                                  <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();"><?=\app\modules\jobs\models\FavoriteCompany::find()->where(['user_id'=>$item->rezume['user_id']])->count()?></a>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>Favorite jobs</td>
                                                              <td>
                                                                  <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();"><?=\app\modules\jobs\models\Favoritejobs::find()->where(['user_id'=>$item->rezume['user_id']])->count()?></a>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>Apply</td>
                                                              <td>
                                                                  <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();">
                                                                    <?=\app\modules\jobs\models\Userjobs::find()->where(['user_id'=>$item->rezume['user_id']])->count()?>
                                                                  </a>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>Offer</td>
                                                              <td>
                                                                  <a onclick="$('#view_user_<?=$item->rezume['user_id']?>').show();">
                                                                    <?=\app\modules\jobs\models\OfferJob::find()->where(['user_id'=>$item->rezume['user_id']])->count()?>
                                                                  </a>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>About</td>
                                                              <td> <?= substr($item->rezume['about'], 0, 60).'...'; ?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>Tags</td>
                                                              <td>
                                                                <?php $array_skills_user = explode(",", $item->rezume['skills']);
                                                                foreach ($array_skills_user as $value) {
                                                                  if (in_array($value, $array_skills_jobs)) {
                                                                    echo '<span class="label label-success">'.$value.'</span> ';
                                                                  } else
                                                                    echo '<span class="label label-default">'.$value.'</span> ';
                                                                }
                                                                ?>
                                                              </td>
                                                          </tr>
                                                      </table>
                                                  </div>
                                                  <div class="col-sm-3">
                                                    <?= HTML::a('Invite','#',[
                                                      'class' => 'view-job',
                                                      'data-toggle'=>"modal",
                                                      'data-target'=>"#offer_job",
                                                      'onclick'=>'$("#user_id_offer").val('.$item->rezume['user_id'].')'
                                                    ]); ?>
                                                  </div>
                                              </div>
                                          <?php }?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        <?php } else {?>
                            <br>
                            <div>

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active">
                                        <a href="#apply" aria-controls="apply" role="tab" data-toggle="tab">
                                            Apply <span class="badge"><?=count($modelUserJobs)?></span>
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#favorite" aria-controls="favorite" role="tab" data-toggle="tab">
                                            Favorite <span class="badge"><?=count($modelUserFavorite)?></span>
                                        </a>
                                    </li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="apply">
                                        <div class="top-vacancies">
                                          <?php
                                          foreach($modelUserJobs as $item){
                                            ?>
                                              <div class="top-v-item">
                                                  <div class="jobs-logo col-sm-4">
                                                    <?php if($item->rezume['avatar'] != ''){ ?>
                                                        <img class="img-responsive" src="<?= Url::home().'images/users_images/'.$item->rezume['avatar']; ?>">
                                                    <?php }else{ ?>
                                                        <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                                                    <?php } ?>
                                                  </div>
                                                  <div class="col-sm-8">
                                                      <h4><?= $item->rezume->user['name'].' '.$item->rezume->user['surname']; ?></h4>
                                                      <table class="jobs-skills">
                                                          <tr>
                                                              <td>Addres</td>
                                                              <td> <?= $item->rezume->userinfo['address']; ?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>University</td>
                                                              <td><?=$item->rezume->userinfo->university['name']?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>Favorite company</td>
                                                              <td><?=\app\modules\jobs\models\FavoriteCompany::find()->where(['user_id'=>$item->rezume['user_id']])->count()?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>Favorite jobs</td>
                                                              <td><?=\app\modules\jobs\models\Favoritejobs::find()->where(['user_id'=>$item->rezume['user_id']])->count()?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>Apply</td>
                                                              <td><?=\app\modules\jobs\models\Userjobs::find()->where(['user_id'=>$item->rezume['user_id']])->count()?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>Offer</td>
                                                              <td><?=\app\modules\jobs\models\OfferJob::find()->where(['user_id'=>$item->rezume['user_id']])->count()?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>About</td>
                                                              <td> <?= substr($item->rezume['about'], 0, 60).'...'; ?></td>
                                                          </tr>
                                                      </table>
                                                  </div>
                                              </div>
                                          <?php }?>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="favorite">
                                        <div class="top-vacancies">
                                          <?php
                                          foreach($modelUserFavorite as $item){
                                            ?>
                                              <div class="top-v-item">
                                                  <div class="jobs-logo col-sm-4">
                                                    <?php if($item->rezume['avatar'] != ''){ ?>
                                                        <img class="img-responsive" src="<?= Url::home().'images/users_images/'.$item->rezume['avatar']; ?>">
                                                    <?php }else{ ?>
                                                        <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                                                    <?php } ?>
                                                  </div>
                                                  <div class="col-sm-8">
                                                      <h4><?= $item->rezume->user['name'].' '.$item->rezume->user['surname']; ?></h4>
                                                      <table class="jobs-skills">
                                                          <tr>
                                                              <td>Addres</td>
                                                              <td> <?= $item->rezume->userinfo['address']; ?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>University</td>
                                                              <td><?=$item->rezume->userinfo->university['name']?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>Favorite company</td>
                                                              <td><?=\app\modules\jobs\models\FavoriteCompany::find()->where(['user_id'=>$item->rezume['user_id']])->count()?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>Favorite jobs</td>
                                                              <td><?=\app\modules\jobs\models\Favoritejobs::find()->where(['user_id'=>$item->rezume['user_id']])->count()?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>Apply</td>
                                                              <td><?=\app\modules\jobs\models\Userjobs::find()->where(['user_id'=>$item->rezume['user_id']])->count()?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>Offer</td>
                                                              <td><?=\app\modules\jobs\models\OfferJob::find()->where(['user_id'=>$item->rezume['user_id']])->count()?></td>
                                                          </tr>
                                                          <tr>
                                                              <td>About</td>
                                                              <td> <?= substr($item->rezume['about'], 0, 60).'...'; ?></td>
                                                          </tr>
                                                      </table>
                                                  </div>
                                              </div>
                                          <?php }?>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        <?php } ?>
					</div>
					<div class="col-sm-6">


                            <div class="jobs-block">
                              <div class="row">
                                  <div class="col-sm-2">
                                      <div class="jobs-logo">
                                        <?php if($companyJob->logo_src != ''){ ?>
                                            <img class="img-responsive" src="<?= Url::home().'images/company_logo/'.$companyJob->logo_src; ?>">
                                        <?php }else{ ?>
                                            <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                                        <?php } ?>
                                      </div>
                                  </div>
                                  <div class="col-sm-3">
                                      <h4 class="title-job">Name: <a href="/company_view/<?=$companyJob->id?>"> <?= $companyJob->name; ?> </a></h4>
                                  </div>
                                  <div class="col-sm-3">
                                      <h4 class="title-job"> Vacancies: <?= \app\modules\jobs\models\Jobs::find()->where(['company_id'=>$companyJob->id])->count() ?></h4>
                                  </div>
                                  <div class="col-sm-3">
                                      <h4 class="title-job"> Followers: <?= \app\modules\jobs\models\FavoriteCompany::find()->where(['company_id'=>$companyJob->id])->count() ?></h4>
                                  </div>
                                  <?php if($role != 'company'){?>
                                      <div class="col-sm-1">
                                          <?=\app\widgets\FavoritecompanyWidget::widget(['company_id'=>$companyJob->id])?>
                                      </div>
                                  <?php } ?>
                              </div>
                          </div>
                            <div class="jobs-block">
                                <div class="row">
                                <div class="col-sm-4">
                                    <div class="jobs-logo">
                                      <?php if($modelJobsAll['jobsLogo'] != ''){ ?>
                                          <img class="img-responsive" src="<?= Url::home().'images/jobs_logo/'.$modelJobsAll['jobsLogo']; ?>">
                                      <?php }else{ ?>
                                          <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
                                      <?php } ?>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <h4 class="title-job"><?= $modelJobsAll['title']; ?></h4>
                                    <?php $job = \app\modules\jobs\models\Jobs::findOne($modelJobsAll['id']) ?>
                                    <table class="jobs-skills">
                                        <tr>
                                            <td>Addres</td>
                                            <td><?= $job->address; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Salary</td>
                                            <td><?= $job->salary; ?></td>
                                        </tr>
                                        <tr <?=($job->university_id == $userRezume->userinfo['university_id'])?'style="color:green"':''?>>
                                            <td>University</td>
                                            <td><?= $job->university['name']; ?></td>
                                        </tr>
                                        <tr <?=($job->industry_id == $userRezume->userinfo['industry_id'])?'style="color:green"':''?>>
                                            <td>Industry</td>
                                            <td><?= $job->industry['name']; ?></td>
                                        </tr>
                                        <tr <?=($job->function_id == $userRezume->userinfo['jobs_function_id'])?'style="color:green"':''?>>
                                            <td>Job function</td>
                                            <td><?= $job->jobfunction['name']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Favorite</td>
                                            <td><?=\app\modules\jobs\models\Favoritejobs::find()->where(['jobs_id'=>$job->id])->count()?></td>
                                        </tr>
                                        <tr>
                                            <td>Apply</td>
                                            <td><?=\app\modules\jobs\models\Userjobs::find()->where(['jobs_id'=>$job->id])->count()?></td>
                                        </tr>
                                        <tr>
                                            <td>About</td>
                                            <td><?= substr($job->about, 0, 60).'...'; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Tags</td>
                                            <td>
                                              <?php $array_skills_vak = explode(",", $job->skills);
                                              foreach ($array_skills_vak as $value) {
                                                if (in_array($value, $array_skills_user)) {
                                                  echo '<span class="label label-success">'.$value.'</span> ';
                                                } else
                                                  echo '<span class="label label-default">'.$value.'</span> ';
                                              }
                                              ?>
                                            </td>
                                        </tr>
                                    </table>
                                  <?php if($role != 'company'){?>
                                  <?php

                                  if($modelJobsAll['user_id'] == \Yii::$app->user->id){ ?>
                                      <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#update_jobs">Update job</button>
                                  <?php }else{ ?>
                                      <?= UserjobsWidget::widget(['jobs_id' => $modelJobsAll['id'],'page_type' => 'search']) ?>
                                          <!--<button class="btn applyJob pull-right" data-status="" style="font-size:12px;padding-top:0px; padding-bottom:0px;margin-top:10px;background-color:#525273;color:white;" data-job_id="<?php //= $modelJobsAll['id']; ?>">Apply</button> -->
                                  <?php } ?>

                                      <div class="col-sm-1">
                                        <?=\app\widgets\FavoritejobsWidget::widget(['jobs_id'=>$modelJobsAll['id']])?>
                                      </div>
                                  <?php } ?>
                                </div>
                            </div>
                            </div>

					</div>
				</div>
			</div>
		</div>
	</div>
<div class="modal fade" id="offer_job" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
          <?php $form = ActiveForm::begin(); ?>
            <div class="modal-body">
                <input type="hidden" name="OfferJob[user_id]" value="" id="user_id_offer">
                <?= $form->field($offerJob,'salary');?>
                <?= $form->field($offerJob,'type')->dropDownList(['day'=>'day','week'=>'week','month'=>'month']);?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" name="offer_bt" class="btn btn-primary">Save changes</button>
            </div>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>

<?php foreach ($modelUserJobsMatches as $item){

  $offersUsers = \app\modules\jobs\models\OfferJob::findAll(['user_id'=>$item->id]);

  $followersUsers = \app\modules\jobs\models\FavoriteRezume::findAll(['rezume_id'=>$item->id]);

  $followCompany = \app\modules\jobs\models\FavoriteCompany::findAll(['user_id'=>$item->user_id]);

  $followJob = \app\modules\jobs\models\Favoritejobs::findAll(['user_id'=>$item->user_id]);

  $appliedUsers = \app\modules\jobs\models\Userjobs::findAll(['user_id'=>$item->user_id]);


  $matchesVacancies = \app\modules\jobs\models\Jobs::find()
    ->where(['company_id'=>$modelCompany->id,'industry_id'=>$item->userinfo['industry_id']])
    ->orWhere(['function_id'=>$item->userinfo['jobs_function_id']])
    ->orWhere([ 'major_id'=>$item->userinfo['major_id']])
    ->all();

  ?>
    <div class="modalRightPagesLS" id="view_user_<?=$item->user_id?>">

        <a class="btn btn-close pull-right closeModal" onclick="$('#view_user_<?=$item->user_id?>').hide();" >
            <span class="" aria-hidden="true">x</span>
        </a>
        <div class="user-info">
            <div class="class-user-av">
              <?php if($item->avatar != ''){ ?>
                  <img class="img-responsive" src="<?= Url::home().'images/users_images/'.$item->avatar; ?>">
              <?php }else{ ?>
                  <img class="img-responsive" src="<?= Url::home(); ?>images/noavatar.png">
              <?php } ?>
            </div>
            <span class="user-name">
					<?= $item->user['name'].' '.$item->user['surname']; ?>
				</span>
            <div class="right-block">

            </div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-content">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#offers_user<?=$item->user_id?>" aria-controls="offers_user<?=$item->user_id?>" role="tab" data-toggle="tab">Offers</a></li>
                <li role="presentation"><a href="#followers_user<?=$item->user_id?>" aria-controls="followers_user<?=$item->user_id?>" role="tab" data-toggle="tab">Followers</a></li>
                <li role="presentation"><a href="#follow_user<?=$item->user_id?>" aria-controls="follow_user<?=$item->user_id?>" role="tab" data-toggle="tab">Follow</a></li>
                <li role="presentation"><a href="#applied_user<?=$item->user_id?>" aria-controls="applied_user<?=$item->user_id?>" role="tab" data-toggle="tab">Applied</a></li>
                <li role="presentation"><a href="#matches_vacancies<?=$item->user_id?>" aria-controls="matches_vacancies<?=$item->user_id?>" role="tab" data-toggle="tab">Matches vacancies</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="offers_user<?=$item->user_id?>">
                    <table class="table classBlock">
                        <thead>
                        <tr>
                            <td>Name jobs</td>
                            <td>Salaty</td>
                            <td>Type</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($offersUsers as $value){?>
                            <tr>
                                <td>
                                    <div class="class-user-av">
                                        <img class="" src="<?=$value->job['logo_src']?>">
                                    </div>
                                    <span class="user-name"><?=$value->job['title']?></span>
                                </td>
                                <td>
                                  <?=$value->salary?>
                                </td>
                                <td>
                                  <?=$value->type?>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane" id="followers_user<?=$item->user_id?>">
                    <table class="table classBlock">
                        <thead>
                        <tr>
                            <td>Name comapny</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($followersUsers as $value){?>
                            <tr>
                                <td>
                                    <div class="class-user-av">
                                        <img class="" src="<?=$value->user->company['logo_src']?>">
                                    </div>
                                    <span class="user-name"><?=$value->user->company['name']?></span>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane" id="follow_user<?=$item->user_id?>">
                    <table class="table classBlock">
                        <thead>
                        <tr>
                            <td>Name comapny</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($followCompany as $value){?>
                            <tr>
                                <td>
                                    <div class="class-user-av">
                                        <img class="" src="<?=$value->company['logo_src']?>">
                                    </div>
                                    <span class="user-name"><?=$value->company['name']?></span>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                    <table class="table classBlock">
                        <thead>
                        <tr>
                            <td>Name jobs</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($followJob as $value){?>
                            <tr>
                                <td>
                                    <div class="class-user-av">
                                        <img class="" src="<?=$value->job['logo_src']?>">
                                    </div>
                                    <span class="user-name"><?=$value->job['title']?></span>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane" id="applied_user<?=$item->user_id?>">
                    <table class="table classBlock">
                        <thead>
                        <tr>
                            <td>Name jobs</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($appliedUsers as $value){?>
                            <tr>
                                <td>
                                    <div class="class-user-av">
                                        <img class="" src="<?=$value->job['logo_src']?>">
                                    </div>
                                    <span class="user-name"><?=$value->job['title']?></span>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane" id="matches_vacancies<?=$item->user_id?>">
                    <table class="table classBlock">
                        <thead>
                        <tr>
                            <td>Name vacansies</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($matchesVacancies as $value){?>
                            <tr>
                                <td>
                                    <div class="class-user-av">
                                        <img class="" src="<?=$value->logo_src?>">
                                    </div>
                                    <span class="user-name"><?=$value->title?></span>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
<?php } ?>