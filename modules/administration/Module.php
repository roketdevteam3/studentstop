<?php

namespace app\modules\administration;

use app\components\MrCmsModule;
use Yii;

class Module extends MrCmsModule
{
    public $controllerNamespace = 'app\modules\administration\controllers';
    public $layout = '//administration';
    public $defaultRoute = '/dashboard/index';

    public function init()
    {

        parent::init();
        // custom initialization code goes here
    }


}
