<?php

/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 02.11.15
 * Time: 13:00
 */
namespace app\modules\administration;
use lajax\translatemanager\Module;

class TranslateManagerModule extends Module {
    const CACHE_KEY_THEME = 'setting_site_theme';
    public function init(){
//        parent::init();
        $theme = \Yii::$app->cache->get(self::CACHE_KEY_THEME) ? \Yii::$app->cache->get(self::CACHE_KEY_THEME) : 'basic';
        $this->viewPath = $this->viewPath.'/'.$theme;
    }

}

