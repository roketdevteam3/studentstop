<?php

namespace app\modules\administration\controllers;

use PayPal\Api\Currency;
use PayPal\Api\Payout;
use PayPal\Api\PayoutItem;
use PayPal\Api\PayoutSenderBatchHeader;
use Yii;
use app\models\OutputManey;
use app\models\OutputManeySearch;
use yii\base\Exception;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ManeyController implements the CRUD actions for OutputManey model.
 */
class ManeyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OutputManey models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OutputManeySearch(['status'=>0]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $searchModelOk = new OutputManeySearch(['status'=>1]);
        $dataProviderOk = $searchModelOk->search(Yii::$app->request->queryParams);

        $dataProvider->pagination->pageParam = 'pay';
        $dataProviderOk->pagination->pageParam = 'payment';
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProviderOk' => $dataProviderOk,
        ]);
    }

    /**
     * Displays a single OutputManey model.
     * @param integer $id
     * @return mixed
     */
    public function actionPay_maney($id)
    {
        $pay_user = OutputManey::findOne($id);

        $pay = new \app\commands\Paypal();
        $payouts = new Payout();
        $senderBatchHeader = new PayoutSenderBatchHeader();
        $senderBatchHeader->setSenderBatchId(uniqid())
            ->setEmailSubject("You have a payment");

        $senderItem1 = new PayoutItem();
        $senderItem1->setRecipientType('Email')
            ->setNote('Thanks you.')
            ->setReceiver($pay_user->email_paypal)
            ->setSenderItemId("item_1" . uniqid())
            ->setAmount(new Currency('{
                        "value":"'.$pay_user->maney.'",
                        "currency":"USD"
                    }'));

        $payouts->setSenderBatchHeader($senderBatchHeader)
            ->addItem($senderItem1);

        $request = clone $payouts;


        try {
            $output = $payouts->create(null, $pay->api);
        } catch (Exception $ex) {
            Yii::$app->session->set('pay_error',true);
            Yii::$app->session->set('msg',$ex);
            return $this->redirect('index');
        }

        $payoutBatch = $output;

        $payoutBatchId = $payoutBatch->getBatchHeader()->getPayoutBatchId();

        try {
            $output = Payout::get($payoutBatchId, $pay->api);
        } catch (Exception $ex) {
            Yii::$app->session->set('pay_error',true);
            Yii::$app->session->set('msg',$ex);
            return $this->redirect('index');
        }

        $pay_user->status = 1;
        $pay_user->save();

        Yii::$app->session->set('pay_ok',true);
        return $this->redirect('index');
    }

    /**
     * Creates a new OutputManey model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OutputManey();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing OutputManey model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing OutputManey model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the OutputManey model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OutputManey the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OutputManey::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
