<?php

namespace app\modules\administration\controllers;

use app\components\MrCmsController;

use app\modules\users\models\UsersSearch;
use Yii;
use app\modules\users\models\Users;
use app\modules\users\models\SignupForm;
use app\models\Setting;



/**
 * DefaultController implements the CRUD actions for Comments model.
 */
class UserController extends MrCmsController
{

    /**
     * Lists all users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Users();
        $supportEmail = Setting::find()->where(['key' => 'setting_support_email'])->asArray()->one();

        if ($model->load(Yii::$app->request->post())) {

            $pass = substr(Yii::$app->security->generateRandomString(), 0, 8);

            $model->password = $pass;
            $model->group = 'moderator';
            if ($user = $model->save()) {
                //added default role User
                $authManager = \Yii::$app->authManager;
                $role = $authManager->getRole('moderator');
                $authManager->assign($role, $model->id);

                \Yii::$app->mailer->compose('@app/mail/create_moderator',
                    [
                        'model' => $model,
                        'password' => $pass,
                    ])
                    ->setTo($model->email)
                    ->setFrom([$supportEmail['value'] => \Yii::$app->name . ' support'])
                    ->setSubject('Create organization')
                    ->send();

                return $this->redirect(['index']);

            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }


    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
