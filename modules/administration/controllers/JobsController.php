<?php

namespace app\modules\administration\controllers;

use app\components\MrCmsController;

use app\modules\users\models\UsersSearch;
use Yii;
use app\modules\users\models\Users;
use app\modules\users\models\SignupForm;
use app\models\Setting;
use app\modules\jobs\models\Jobsindustry;
use app\models\CategoriesType;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\db\Query;


class JobsController extends MrCmsController
{

    public function actionIndex()
    {
        
    }

    public function actionCreateindustry()
    {
        $modelNewIndustry = new Jobsindustry();
        $modelNewIndustry->scenario = 'add_industry';
        
        if($_POST){
            $request = Yii::$app->request;
            if($modelNewIndustry->load($request->post())){
                if($modelNewIndustry->save()){
                    Yii::$app->session->setFlash('industry_create');
                }else{
                    Yii::$app->session->setFlash('industry_notcreate');
                }
            }
        }
        
        $queryIndustry = Jobsindustry::find();
        $modelIndustry = new ActiveDataProvider(['query' => $queryIndustry, 'pagination' => ['pageSize' => 30]]);

        return $this->render('createindustry',[
            'modelIndustry' => $modelIndustry,
            'modelNewIndustry' => $modelNewIndustry,
        ]);
    }
    
    public function actionDeleteindustry($id)
    {
        $modelIndustry = Jobsindustry::find()->where(['id'=>$id])->one();
        if($modelIndustry->delete()){
            Yii::$app->session->setFlash('industry_delete');
        }else{
            Yii::$app->session->setFlash('industry_notdelete');
        }
        return $this->redirect('createindustry');
    }
}
