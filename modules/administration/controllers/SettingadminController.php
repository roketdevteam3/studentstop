<?php

namespace app\modules\administration\controllers;

use app\components\MrCmsController;

use app\modules\users\models\UsersSearch;
use Yii;
use app\modules\users\models\Users;
use app\modules\users\models\SignupForm;
use app\models\Settingadmin;
use app\models\Categories;
use app\models\CategoriesType;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\db\Query;

class SettingadminController extends MrCmsController
{

    public function actionIndex()
    {
        //$modelSetting->scenario = 'update_setting';
        if($_POST){
            if(!isset($_POST['date_one'])){
                    if(isset($_POST['Settingadmin'])){
                        $modelSetting = Settingadmin::find()->where(['key' => $_POST['Settingadmin']['key']])->one();
                        $modelSetting->scenario = 'update_setting';
                        $modelSetting->key = $_POST['Settingadmin']['key'];
                        $modelSetting->value = $_POST['Settingadmin']['value'];
                        $modelSetting->date_update = date("Y-m-d H:i:s");
                        if($modelSetting->save()){
                            Yii::$app->session->setFlash('setting_update');
                        }else{
                            Yii::$app->session->setFlash('setting_not_update');
                        }
                    }                
            }else{
                $arrayDate = [];
                $arrayDate['date_one'] = $_POST['date_one'];
                $arrayDate['date_two'] = $_POST['date_two'];
                
                $modelSetting = Settingadmin::find()->where(['key' => 'beginning of the semester'])->one();
                $modelSetting->scenario = 'update_setting';
                $modelSetting->value = json_encode($arrayDate);
                $modelSetting->date_update = date("Y-m-d H:i:s");
                if($modelSetting->save()){
                    Yii::$app->session->setFlash('setting_update');
                }else{
                    Yii::$app->session->setFlash('setting_not_update');
                }
            }
        }
        $modelSettings = Settingadmin::find()->all();
        return $this->render('index',[
            'modelSetting' => $modelSettings
        ]);
    }
//
//    public function actionDelete($id)
//    {
//        $modelCategory = Categories::find()->where(['id'=>$id])->one();
//        $modelCategory->delete();
//        return $this->redirect('index');
//    }
    
}