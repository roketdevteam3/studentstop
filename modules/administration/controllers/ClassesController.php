<?php

namespace app\modules\administration\controllers;

use app\components\MrCmsController;

use app\modules\users\models\UsersSearch;
use Yii;
use app\modules\users\models\Users;
use app\modules\users\models\User;
use app\modules\users\models\SignupForm;
use app\models\Setting;
use app\models\Categories;
use app\models\Major;
use app\models\Course;
use app\models\Classes;
use app\models\University;
use app\models\CategoriesType;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\db\Query;


class ClassesController extends MrCmsController
{

    public function actionIndex()
    {
        $modelNewClass = new Classes;
        $modelNewClass->scenario = 'add_class';
        
        if($_POST){
            $request = Yii::$app->request;
            if($modelNewClass->load($request->post())){
                if($modelNewClass->save()){
                    Yii::$app->session->setFlash('class_added');
                }else{
                    Yii::$app->session->setFlash('class_notadded');
                }
            }
        }
        
        $courseArray = ArrayHelper::map(Course::find()->all(), 'id', 'name');
        $userArray = ArrayHelper::map(User::find()->all(), 'id', 'name');
        $queryClass = Classes::find();
        $modelClass = new ActiveDataProvider(['query' => $queryClass, 'pagination' => ['pageSize' => 30]]);

        return $this->render('index',[
            'userArray' => $userArray,
            'modelNewClass' => $modelNewClass,
            'modelClass' => $modelClass,
            'courseArray' => $courseArray,
        ]);
    }

    public function actionDelete($id)
    {
        $queryClass = Classes::find()->where(['id'=>$id])->one();
        if($queryClass->delete()){
            Yii::$app->session->setFlash('class_delete');
        }else{
            Yii::$app->session->setFlash('class_notdelete');
        }
        return $this->redirect('index');
    }
    
}
