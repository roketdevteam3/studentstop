<?php

namespace app\modules\administration\controllers;


use app\components\MrCmsController;

use Yii;
use app\models\Setting;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;


/**
 * DefaultController implements the CRUD actions for Comments model.
 */
class SettingController extends MrCmsController
{

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Setting::find()->where('type_input <> "hidden"'),
        ]);

            return $this->render('index', [
                'dataProvider' => $dataProvider,
            ]);

    }

  public function actionUpdate()
  {
      // Validate if there is an editable input saved via AJAX
      if (Yii::$app->request->post('hasEditable')) {

          $data = Yii::$app->request->post();
          $model = $this->findModel($data['editableKey']);
          $model->value = $data[$model->formName()][$data['editableIndex']]['value'];

          // Store a default JSON response as desired by editable
          $out = Json::encode(['output' => 'error', 'message' => Yii::t('app','Error update!')]);


          // Load model like any single model validation
          if ($model->save()) {

              Yii::$app->cache->set($model->key,  $model->value);
              $out = Json::encode(['output' => '', 'message' => '']);
          }
          return $out;
      }
      throw new NotFoundHttpException('The requested page does not exist.');
  }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Setting::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
