<?php

namespace app\modules\administration\controllers;

use app\components\MrCmsController;

use app\modules\users\models\UsersSearch;
use Yii;
use app\modules\users\models\Users;
use app\modules\users\models\SignupForm;
use app\models\Setting;
use app\models\Categories;
use app\models\Major;
use app\models\University;
use app\models\CategoriesType;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\db\Query;


class MajorController extends MrCmsController
{

    public function actionIndex()
    {
        $modelNewMajor = new Major;
        $modelNewMajor->scenario = 'add_major';
        
        if($_POST){
            $request = Yii::$app->request;
            if($modelNewMajor->load($request->post())){
                if($modelNewMajor->save()){
                    Yii::$app->session->setFlash('major_added');
                }else{
                    Yii::$app->session->setFlash('major_notadded');
                }
            }
        }
        
        $universityArray = ArrayHelper::map(University::find()->all(), 'id', 'name');
        $queryMajor = Major::find();
        $modelMajor = new ActiveDataProvider(['query' => $queryMajor, 'pagination' => ['pageSize' => 30]]);

        return $this->render('index',[
            'modelNewMajor' => $modelNewMajor,
            'modelMajor' => $modelMajor,
            'universityArray' => $universityArray,
        ]);
    }

    public function actionDelete($id)
    {
        $queryMajor = Major::find()->where(['id'=>$id])->one();
        if($queryMajor->delete()){
            Yii::$app->session->setFlash('major_delete');
        }else{
            Yii::$app->session->setFlash('major_notdelete');
        }
        return $this->redirect('index');
    }
}
