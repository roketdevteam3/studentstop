<?php

namespace app\modules\administration\controllers;

use app\components\MrCmsController;

use app\modules\users\models\UsersSearch;
use Yii;
use app\modules\users\models\Users;
use app\modules\users\models\SignupForm;
use app\models\Setting;
use app\modules\question\models\Questioncategory;
use app\models\CategoriesType;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\db\Query;


class QuestionController extends MrCmsController
{

    public function actionIndex()
    {
        $modelNewCategory = new Questioncategory();
        $modelNewCategory->scenario = 'add_question';
        
        if($_POST){
            $request = Yii::$app->request;
            if($modelNewCategory->load($request->post())){
                if($modelNewCategory->parent_id == ''){
                   $modelNewCategory->parent_id = 0; 
                }
                if($modelNewCategory->save()){
                    Yii::$app->session->setFlash('category_added');
                }else{
                    Yii::$app->session->setFlash('category_notadded');
                }
            }
        }
        
        $categoryArray = ArrayHelper::map(Questioncategory::find()->where(['parent_id' => 0])->all(), 'id', 'name');
        $queryCategory = Questioncategory::find();
        $modelCategory = new ActiveDataProvider(['query' => $queryCategory, 'pagination' => ['pageSize' => 30]]);

        return $this->render('index',[
            'modelNewCategory' => $modelNewCategory,
            'modelCategory' => $modelCategory,
            'categoryArray' => $categoryArray,
        ]);
    }

    public function actionDelete($id)
    {
        $queryCategory = Questioncategory::find()->where(['id'=>$id])->one();
        if($queryCategory->delete()){
            Yii::$app->session->setFlash('category_delete');
        }else{
            Yii::$app->session->setFlash('category_notdelete');
        }
        return $this->redirect('index');
    }
}
