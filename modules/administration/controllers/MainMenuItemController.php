<?php

namespace app\modules\administration\controllers;

use Yii;
use app\models\MainMenuItem;
use app\models\search\MainMenuItemSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\behaviors\SortableController;
use app\components\MrCmsController;
use app\behaviors\StatusController;


/**
 * MainMenuItemController implements the CRUD actions for MainMenuItem model.
 */
class MainMenuItemController extends MrCmsController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            [
                'class' => SortableController::className(),
                'model' => MainMenuItem::className()
            ],
            [
                'class' => StatusController::className(),
                'model' => MainMenuItem::className()
            ]
        ];
    }

    /**
     * Lists all MainMenuItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MainMenuItemSearch();

        $dataProviderLogged = $searchModel->getMenuListLogged();
        $dataProviderNonLogin = $searchModel->getMenuListNonLogin();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProviderLogged' => $dataProviderLogged,
            'dataProviderNonLogin' => $dataProviderNonLogin,
        ]);
    }

    /**
     * Displays a single MainMenuItem model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MainMenuItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MainMenuItem();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MainMenuItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MainMenuItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionUp($id)
    {
        return $this->move($id, 'up');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionDown($id)
    {
        return $this->move($id, 'down');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionOn($id)
    {
        return $this->changeStatus($id, MainMenuItem::STATUS_ON);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionOff($id)
    {
        return $this->changeStatus($id, MainMenuItem::STATUS_OFF);
    }

    /**
     * Finds the MainMenuItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MainMenuItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MainMenuItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}