<?php

namespace app\modules\administration\controllers;

use app\components\MrCmsController;
use Yii;
use app\modules\university\models\University;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\helpers\Image;
use yii\helpers\ArrayHelper;
use app\modules\users\models\User;

/**
 * UniversityController implements the CRUD actions for University model.
 */
class UniversityController extends MrCmsController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all University models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => University::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single University model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new University model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new University();
        $data = Yii::$app->request->post();
        
        $arrayUser = [];
        $modelUser = User::find()->all();
        $regionArray = ArrayHelper::map(\app\models\States::find()->where(['country_id' => 231])->asArray()->all(), 'id', 'name');

        foreach($modelUser as $user){
            $arrayUser[$user->id] = $user->name.' '.$user->surname;
        }
//        $arrayUser = ArrayHelper::map(User::find()->asArray()->all(),'id', 'name');
        if ($model->load($data)) {
            //$fileInstanse = UploadedFile::getInstance($model, 'image');
            //$model->image = $fileInstanse;
            /*if (isset($fileInstanse))
                if ($model->validate(['image'])) {
                    $model->image = Image::upload($model->image, 'university');
                } else {
                    return $this->render('create', [
                        'model' => $model,
                        'arrayUser' => $arrayUser,
                    ]);
                } */
            if ($model->save()) {
                return $this->redirect('index');
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'regionArray' => $regionArray,
                'arrayUser' => $arrayUser,
            ]);
        }
    }

    /**
     * Updates an existing University model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $arrayUser = [];
        $regionArray = ArrayHelper::map(\app\models\States::find()->where(['country_id' => 231])->asArray()->all(), 'id', 'name');
        
        $modelUser = User::find()->all();
        foreach($modelUser as $user){
            $arrayUser[$user->id] = $user->name.' '.$user->surname;
        }
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['administration/university/view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'regionArray' => $regionArray,
                'arrayUser' => $arrayUser,
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing University model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the University model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return University the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = University::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
