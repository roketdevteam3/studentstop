<?php

namespace app\modules\administration\controllers;

use app\behaviors\SortableController;
use app\behaviors\StatusController;
use app\components\MrCmsController;
use app\models\MainMenuItem;
use Yii;
use app\models\Pages;
use app\models\search\PagesSearch;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PageController implements the CRUD actions for Pages model.
 */
class PageController extends MrCmsController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            [
                'class' => SortableController::className(),
                'model' => Pages::className()
            ],
            [
                'class' => StatusController::className(),
                'model' => Pages::className()
            ]
        ];
    }

    /**
     * Lists all Pages models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pages();

        if ($model->load(Yii::$app->request->post()) ) {
            if($model->save()){

                if($model->create_menu){
                    $menu = new MainMenuItem();
                    $menu->title = $model->title;
                    $menu->status = $model->status;
                    $menu->link = Url::to('page/'.$model->slug);
                    $menu->absolute = 1;
                    $menu->save(false);
                }

                return $this->redirect(['index']);
            }else{
                return $this->render('create', [
                    'model' => $model,
                ]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Pages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Pages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * @param $id
     * @return mixed
     */
    public function actionUp($id)
    {
        return $this->move($id, 'up');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionDown($id)
    {
        return $this->move($id, 'down');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionOn($id)
    {
        return $this->changeStatus($id, MainMenuItem::STATUS_ON);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionOff($id)
    {
        return $this->changeStatus($id, MainMenuItem::STATUS_OFF);
    }

    /**
     * Finds the Pages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}