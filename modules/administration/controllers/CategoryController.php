<?php

namespace app\modules\administration\controllers;

use app\components\MrCmsController;

use app\modules\users\models\UsersSearch;
use Yii;
use app\modules\users\models\Users;
use app\modules\users\models\SignupForm;
use app\models\Setting;
use app\models\Categories;
use app\models\CategoriesType;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\db\Query;

/**
 * DefaultController implements the CRUD actions for Comments model.
 */
class CategoryController extends MrCmsController
{

    /**
     * Lists all users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $modelNewCategories = new Categories;
        $modelNewCategories->scenario = 'add_category'; 
        if($_POST){
            $request = Yii::$app->request;
                if($modelNewCategories->load($request->post())){
                    if($modelNewCategories->parent_id != ''){
                        $modelNewCategories->category_type = 0;
                    }else{
                        $modelNewCategories->parent_id = 0;
                    }
                    if($modelNewCategories->save()){
                        $modelNewCategories = new Categories;
                        $modelNewCategories->scenario = 'add_category';
                    }
                }
        }
        
        //$query = Categories::find();
        $query = new Query;
        $query	->select(['*, {{%post_categories}}.id as id'])
                        ->from('{{%post_categories}}')
                        ->join(	'LEFT JOIN',
                                    '{{%post_categories_type}}',
                                    '{{%post_categories_type}}.id = {{%post_categories}}.category_type'
                            );
        $command = $query->createCommand();
        $modelC = $command->queryAll();
        $arrayCategories = [];
        foreach($modelC as $category){
            $category['parent_id'] = Categories::getCategoryName($category['parent_id']);
            $arrayCategories[] = $category;
        }
        
        $modelCategories = [];
        $modelCategories = new ArrayDataProvider(['allModels' => $arrayCategories, 'pagination' => ['pageSize' => 30]]);
        
        $categoriesTypeArray = ArrayHelper::map(CategoriesType::find()->where(['id' => 3])->orWhere(['id' => 1])->all(), 'id', 'type_name');
        $parentCategoriesArray = ArrayHelper::map(Categories::find()->where(['parent_id' => 0])->all(), 'id', 'category_name');
        
        return $this->render('index',[
            'modelNewCategories' => $modelNewCategories,
            'modelCategories' => $modelCategories,
            'categoriesTypeArray' => $categoriesTypeArray,
            'parentCategoriesArray' => $parentCategoriesArray,
        ]);
    }

    public function actionDelete($id)
    {
        $modelCategory = Categories::find()->where(['id'=>$id])->one();
        $modelCategory->delete();
        return $this->redirect('index');
    }
    
}
