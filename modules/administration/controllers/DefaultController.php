<?php

namespace app\modules\administration\controllers;


use app\components\MrCmsController;
use app\modules\users\models\LoginForm;
use app\modules\users\models\Users;
use app\models\UserChangePassw;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\Html;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
error_reporting(E_ERROR);

/**
 * DefaultController implements the CRUD actions for Comments model.
 */
class DefaultController extends MrCmsController
{
    public function behaviors()
    {
        return [

            // ...
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['view'],
                'rules' => [
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            // ...

        ];
    }


    public function actionLogin()
    {
        $this->layout = '//login_admin';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(Url::to('/administration/', true));
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionChangePassword()
    {

        $currentId = Yii::$app->user->identity->getId();
        $model = UserChangePassw::findOne($currentId);
        if (Yii::$app->request->post()) {
            $aData = Yii::$app->request->post()['UserChangePassw'];
            if ($aData['password'] == $aData['reNewPass']) {
//                $model->password = Yii::$app->security->generatePasswordHash($aData['password']);

                if($model->load(Yii::$app->request->post()) ){
                    $model->password = Yii::$app->security->generatePasswordHash($model->password);
                    $model->reNewPass = Yii::$app->security->generatePasswordHash($model->reNewPass);
                    if($model->save()){
                        return $this->redirect(Url::to('/administration/', true));
                    }else {
                        $this->flash('error', Yii::t('admin', 'Create error. {0}', $model->formatErrors()));
                    }
                }else {
                    $this->flash('error', Yii::t('admin', 'Create error. {0}', $model->formatErrors()));
                }

            } else {
                $this->flash('error', Yii::t('admin', 'Create error. {0}', $model->formatErrors()));
            }


        } else {
            return $this->render('change-pass', [
                'model' => $model,
            ]);
        }

    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

}
