<?php

namespace app\modules\administration\controllers;

use app\components\MrCmsController;
use app\models\News;
use app\modules\users\models\Users;
use Yii;




/**
 * DefaultController implements the CRUD actions for Comments model.
 */
class DashboardController extends MrCmsController
{

    /**
     * Lists all statistic models.
     * @return mixed
     */
    public function actionIndex()
    {


        return $this->render('index', [

        ]);
    }



}
