<?php

namespace app\modules\administration\controllers;

use app\components\MrCmsController;

use app\modules\users\models\UsersSearch;
use Yii;
use app\modules\users\models\Users;
use app\modules\users\models\SignupForm;
use app\models\Setting;
use app\models\Categories;
use app\models\Major;
use app\models\Course;
use app\models\University;
use app\models\CategoriesType;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\db\Query;


class CourseController extends MrCmsController
{

    public function actionIndex()
    {
        $modelNewCourse = new Course;
        $modelNewCourse->scenario = 'add_course';
        
        if($_POST){
            $request = Yii::$app->request;
            if($modelNewCourse->load($request->post())){
                if($modelNewCourse->save()){
                    Yii::$app->session->setFlash('course_added');
                }else{
                    Yii::$app->session->setFlash('course_notadded');
                }
            }
        }
        
        $majorArray = ArrayHelper::map(Major::find()->all(), 'id', 'name');
        $queryCourse = Course::find();
        $modelCourse = new ActiveDataProvider(['query' => $queryCourse, 'pagination' => ['pageSize' => 30]]);

        return $this->render('index',[
            'modelNewCourse' => $modelNewCourse,
            'modelCourse' => $modelCourse,
            'majorArray' => $majorArray,
        ]);
    }

    public function actionDelete($id)
    {
        $queryCourse = Course::find()->where(['id'=>$id])->one();
        if($queryCourse->delete()){
            Yii::$app->session->setFlash('course_delete');
        }else{
            Yii::$app->session->setFlash('course_notdelete');
        }
        return $this->redirect('index');
    }
    
}
