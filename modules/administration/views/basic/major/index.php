<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;

?>
<h1>Major</h1>
<?php
    if(Yii::$app->session->hasFlash('major_added')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => 'Major added',
        ]);
    endif; 
    if(Yii::$app->session->hasFlash('major_delete')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => 'Major deleted',
        ]);
    endif; 
    if(Yii::$app->session->hasFlash('major_notadded')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-error',
            ],
            'body' => 'Major not added',
        ]);
    endif; 
?>


<?php $form = ActiveForm::begin(); ?>
        <?= $form->field($modelNewMajor, 'name')->textInput(); ?>
        <?= $form->field($modelNewMajor, 'description')->textarea(); ?>
        <?= $form->field($modelNewMajor, 'url_name')->textInput(); ?>
        <?= $form->field($modelNewMajor, 'university_id')->dropDownList($universityArray); ?>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('app', 'Add Major'), ['name'=> 'change_user_info', 'class' => 'btn btn-primary', 'data-mr-loader'=>'show']) ?>
    </div>
<?php ActiveForm::end();  ?>

<?= GridView::widget([
           'dataProvider' => $modelMajor,
           'columns' => [
               'name',
               'description',
               'url_name',
               'university_id',
               'date_create',
               [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}',
                    'buttons' => [
                        'delete' => function ($url,$modelMajor) {
                                return Html::a(
                                '<span class="fa fa-times"></span>', 
                                'delete?id='.$modelMajor['id']);
                            
                        },
                    ],
                ],
           ],
       ]) ?>