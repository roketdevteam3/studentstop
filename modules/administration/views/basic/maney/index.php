<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OutputManeySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Output Maneys';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="output-maney-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    //					if(Yii::$app->request->get('message')=='false'):
    if(Yii::$app->session['pay_error']):
        Yii::$app->session['pay_error']=null;
        echo \yii\bootstrap\Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => \Yii::t('app',Yii::$app->session['msg']),
        ]);
    endif;
    if(Yii::$app->session['pay_ok']):
        Yii::$app->session['pay_ok']=null;
        echo \yii\bootstrap\Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => \Yii::t('app','You payment pay!'),
        ]);
    endif;
    ?>
    <div>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#pay" aria-controls="pay" role="tab" data-toggle="tab">Pay</a></li>
            <li role="presentation"><a href="#payment" aria-controls="payment" role="tab" data-toggle="tab">Payment</a></li>
       </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="pay">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'user.name',
                        'user.surname',
                        'email_paypal',
                        'maney',
                        'status:boolean',
                        'created_at:datetime',
//            'updated_at',
                        ['class' => 'yii\grid\ActionColumn',
                            'template'=>'{pay}',
                            'buttons' => [
                                'pay' => function ($url, $model, $key) {
                                    if (!$model->status)
                                        return Html::a('Pay','pay_maney?id='.$model->id,['class'=>'btn btn-primary']);
                                },
                            ],
                        ],
                    ],
                ]); ?>
            </div>
            <div role="tabpanel" class="tab-pane" id="payment">
                <?= GridView::widget([
                    'dataProvider' => $dataProviderOk,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'user.name',
                        'user.surname',
                        'email_paypal',
                        'maney',
                        'status:boolean',
                        'created_at:datetime',
//            'updated_at',
                    ],
                ]); ?>
            </div>
        </div>

    </div>

</div>
