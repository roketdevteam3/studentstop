<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;



$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $form = ActiveForm::begin(); ?>
        <?= $form->field($modelNewCategories, 'category_name')->textInput(); ?>
        <?= $form->field($modelNewCategories, 'url_name')->textInput(); ?>
        <?= $form->field($modelNewCategories, 'parent_id')->dropDownList($parentCategoriesArray,['prompt' => ' -- Select Parent Category--']); ?>
        <?= $form->field($modelNewCategories, 'category_type')->dropDownList($categoriesTypeArray); ?>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('app', 'Add Category'), ['name'=> 'change_user_info', 'class' => 'btn btn-primary', 'data-mr-loader'=>'show']) ?>
    </div>
<?php ActiveForm::end();  ?>
 

        <?= GridView::widget([
           'dataProvider' => $modelCategories,
           'columns' => [
               'category_name',
               'parent_id',
               'type_name',
               'url_name',
               'date_create',
               [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}',
                    'buttons' => [
                        'delete' => function ($url,$modelCategories) {
                            if($modelCategories['change_status'] != 1){
                                return Html::a(
                                '<span class="fa fa-times"></span>', 
                                'delete?id='.$modelCategories['id']);
                            }
                        },
                    ],
                ],
           ],
       ]) ?>