<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;

?>
<h1>Course</h1>
<?php 
    if(Yii::$app->session->hasFlash('course_added')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => 'Course added',
        ]);
    endif; 
    if(Yii::$app->session->hasFlash('course_delete')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => 'Course deleted',
        ]);
    endif; 
    if(Yii::$app->session->hasFlash('course_notadded')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-error',
            ],
            'body' => 'Course not added',
        ]);
    endif; 
?>

<?php $form = ActiveForm::begin(); ?>
        <?= $form->field($modelNewCourse, 'name')->textInput(); ?>
        <?= $form->field($modelNewCourse, 'description')->textarea(); ?>
        <?= $form->field($modelNewCourse, 'url_name')->textInput(); ?>
        <?= $form->field($modelNewCourse, 'id_major')->dropDownList($majorArray); ?>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('app', 'Add Course'), ['name'=> 'change_user_info', 'class' => 'btn btn-primary', 'data-mr-loader'=>'show']) ?>
    </div>
<?php ActiveForm::end();  ?>

<?= GridView::widget([
           'dataProvider' => $modelCourse,
           'columns' => [
               'name',
               'description',
               'url_name',
               'id_major',
               'creator',
               'status',
               'date_create',
               [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}',
                    'buttons' => [
                        'delete' => function ($url,$modelCourse) {
                                return Html::a(
                                '<span class="fa fa-times"></span>', 
                                'delete?id='.$modelCourse['id']);
                            
                        },
                    ],
                ],
           ],
       ])  ?>