<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;

?>
<h1>Classes</h1>
<?php
    if(Yii::$app->session->hasFlash('class_added')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => 'Class added',
        ]);
    endif; 
    if(Yii::$app->session->hasFlash('class_delete')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => 'Class deleted',
        ]);
    endif; 
    if(Yii::$app->session->hasFlash('class_notadded')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-error',
            ],
            'body' => 'Class not added',
        ]);
    endif; 
?>


<?php $form = ActiveForm::begin(); ?>
        <?= $form->field($modelNewClass, 'name')->textInput(); ?>
        <?= $form->field($modelNewClass, 'description')->textarea(); ?>
        <?= $form->field($modelNewClass, 'url_name')->textInput(); ?>
        <?= $form->field($modelNewClass, 'professor')->dropDownList($userArray); ?>
        <?= $form->field($modelNewClass, 'id_course')->dropDownList($courseArray); ?>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('app', 'Add Class'), ['name'=> 'change_user_info', 'class' => 'btn btn-primary', 'data-mr-loader'=>'show']) ?>
    </div>
<?php ActiveForm::end();  ?>

<?= GridView::widget([
           'dataProvider' => $modelClass,
           'columns' => [
               'name',
               'description',
               'url_name',
               'id_course',
               'creator',
               'status',
               'date_create',
               [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}',
                    'buttons' => [
                        'delete' => function ($url,$modelClass) {
                                return Html::a(
                                '<span class="fa fa-times"></span>', 
                                'delete?id='.$modelClass['id']);
                            
                        },
                    ],
                ],
           ],
       ]) ?>