<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;

?>
<h1>Create industry</h1>
<?php
    if(Yii::$app->session->hasFlash('industry_create')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => 'Industry created',
        ]);
    endif; 
    if(Yii::$app->session->hasFlash('industry_delete')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => 'Industry deleted',
        ]);
    endif; 
    if(Yii::$app->session->hasFlash('industry_notcreate')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-error',
            ],
            'body' => 'Industry not created',
        ]);
    endif; 
?>


<?php $form = ActiveForm::begin(); ?>
        <?= $form->field($modelNewIndustry, 'name')->textInput(); ?>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('app', 'Create Industry'), ['class' => 'btn btn-primary', 'data-mr-loader'=>'show']) ?>
    </div>
<?php ActiveForm::end();  ?>

<?= GridView::widget([
           'dataProvider' => $modelIndustry,
           'columns' => [
               'name',
               [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}',
                    'buttons' => [
                        'delete' => function ($url,$modelIndustry) {
                                return Html::a(
                                '<span class="fa fa-times"></span>', 
                                'deleteindustry?id='.$modelIndustry['id']);
                            
                        },
                    ],
                ],
           ],
       ]) ?>