<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;

/* @var $this yii\web\View */
/* @var $model app\models\Pages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pages-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'status',[
        'template' => ' {input}{label} {error}{hint}'
    ])->checkbox(['label' => null,
        'class'=>'checkbox'],false) ?>

    <?= $form->field($model, 'create_menu',[
        'template' => ' {input}{label} {error}{hint}'
    ])->checkbox(['label' => null,
        'class'=>'checkbox'],false) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>


    <?php echo $form->field($model, 'content')->widget(Widget::className(), [
        'settings' => [
            'minHeight'        => 200,
            'imageUpload'      => Url::to(['/redactor/image-upload']),
            'imageManagerJson' => Url::to(['/redactor/images-get']),
            // 'fileManagerJson' => Url::to(['/blog/files-get']),
            // 'fileUpload' => Url::to(['/blog/file-upload']),
            'plugins'          => [
                'clips',
                'fullscreen',
                'table',
                'counter',
                'definedlinks',
                //  'filemanager',
                'fontcolor',
                'fontfamily',
                'fontsize',
                'imagemanager',
                'limiter',
                'textdirection',
                'textexpander',
                'video'
            ]
        ],
        'options'=>[
            'id'=>'pg1'
        ],
    ]);
    ?>


    <?= $form->field($model, 'meta_t')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'meta_k')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'meta_d')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'seo_text')->widget(Widget::className(), [
        'settings' => [
            'minHeight'        => 200,
            'plugins'          => [
                'clips',
                'fullscreen',
                'table',
                'counter',
                'definedlinks',
                'fontcolor',
                'fontfamily',
                'fontsize',
                'imagemanager',
                'limiter',
                'textdirection',
                'textexpander',

            ]
        ],
        'options'=>[
            'id'=>'pg2'
        ],
    ]);
    ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>