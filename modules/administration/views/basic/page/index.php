<?php
/**
 * Created by PhpStorm.
 * User: minace
 * Date: 2/24/16
 * Time: 10:05 PM
 */

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Pages'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    Pjax::begin([
        'id' => 'mr_main_page',
    ]);
    ?>

    <?= GridView::widget([
        'options' => ['class' => 'grid-overflow-hidden'],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn',
            ],

            [
                'attribute' => 'title',
                'label' => Yii::t('administration', 'Title'),
                'width'=> '20%',
            ],
            [
                'attribute' => 'slug',
                'label' => Yii::t('administration', 'Link'),
            ],
            [
                'attribute' => 'content',
                'label' => Yii::t('administration', 'Content'),
                'value' => function ($model) {
                    return StringHelper::truncate(strip_tags($model->content),'300') ;
                }
            ],
            [
                'attribute' => Yii::t('app', 'Order'),
                'format' => 'raw',
                'value' => function ($model) {
                    return "<a href=" . Url::to(['page/up', 'id' => $model->primaryKey], true) . "'
                           class=\"btn btn-default move-up\" title='" . Yii::t('app', 'Move up') . "'><span
                                class=\"glyphicon glyphicon-arrow-up\"></span></a>
                        <a href='" . Url::to(['page/down', 'id' => $model->primaryKey], true) . "'
                           class=\"btn btn-default move-down\" title='" . Yii::t('app', 'Move down') . "'><span
                                class=\"glyphicon glyphicon-arrow-down\"></span></a>";
                },
            ],
            ['class' => 'kartik\grid\ActionColumn',
                'template' => '{delete} {views} {update}  ',
                'header' => Yii::t('administration', 'Action'),
                'width'=>'5%',

                'updateOptions' => [
                    'title' => Yii::t('administration','Edit'),
                ],

                'deleteOptions' => [
                    'title' => Yii::t('administration','Delete'),
                    'data-confirm' => '<h1 class="text-center"><i class="mr-modal-icon-remove ion ion-trash-a"></i></h1><h2>'.Yii::t('administration','Are you sure want to delete this item').'</h2>'
                ],

            ],

        ],

        'panelTemplate' => '{panelBefore}{items}{panelFooter}',
        'bordered'=>false,
        'striped'=>false,
        'condensed'=>false,
        'responsive'=>true,
        'responsiveWrap'=>true,
        'hover'=>true,

        'persistResize'=>false,
    ]); ?>
    <?php
    Pjax::end();
    ?>


</div>