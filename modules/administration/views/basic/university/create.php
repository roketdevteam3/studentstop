<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\University */

$this->title = Yii::t('app', 'Create University');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Universities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="university-create">

    <?= $this->render('_form', [
        'model' => $model,
        'userArray' => $arrayUser,
        'regionArray' => $regionArray,
    ]) ?>

</div>