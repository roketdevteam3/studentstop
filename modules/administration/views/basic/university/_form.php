<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\University */
/* @var $form yii\widgets\ActiveForm */
use dosamigos\tinymce\TinyMce;
?>
<div class="university-form">
    <div style="display:none">
      <div class="table table-striped previewTemplateQ" >

                      <div id="template" class="file-row">
                            <div>
                                    <span class="preview"><img data-dz-thumbnail  style="width: 248px" /></span>
                            </div>
                      </div>
      </div>
    </div>
    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ]
    ]); ?>

    <?= $form->field($model, 'status', [
        'template' => ' {input} <label for="university-status">' . Yii::t('site', 'Status') . '</label> {error}{hint}'
    ])->checkbox(['label' => null, 'class' => 'checkbox']); ?>

    <?= $form->field($model, 'id_owner')->dropDownList($userArray) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'history')->widget(TinyMce::className(), [
            'options' => ['rows' => 6,'id'=>'eventsContent'],
            'language' => 'en_GB',
            'clientOptions' => [
                    'plugins' => [],
                    'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
            ]
    ]); ?>

    <?= $form->field($model, 'country_id')->dropDownList([231 => 'USA']) ?>
    <?= $form->field($model, 'region_id')->dropDownList($regionArray) ?>
    <?= $form->field($model, 'location')->textInput() ?>
    <?= $form->field($model, 'year_founded')->textInput() ?>
    <?php if ($model->slug): ?>
        <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
    <?php endif ?>
    
    <?php if ($model->image) : ?>
        <img src="<?= $model->image ?>" style="width: 248px">
    <?php endif; ?>
        
    <?= $form->field($model, 'image')->hiddenInput()->label(false);?>
    <div class="col-sm-12">
        <a class="btn btn-success newPhoto">New logo</a>
        <div class="newPhotoU">
        </div>
    </div>
    <div style="clear:both;"></div>

    <?= $form->field($model, 'date')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'Enter event time ...'],
        'pluginOptions' => [
            'autoclose' => true
        ]
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>