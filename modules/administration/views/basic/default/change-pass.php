<?php
use yii\helpers\Html;
use yii\helpers\Url;

use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
        'modelClass' => 'Product',
    ]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Change pass')];
?>

<?php $form = ActiveForm::begin([
    'enableClientValidation' => true,
    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
]); ?>

<?= $form->field($model, 'password')->passwordInput(['value'=>'']) ?>

<?= $form->field($model, 'reNewPass')->passwordInput() ?>

<div class="form-group">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>

