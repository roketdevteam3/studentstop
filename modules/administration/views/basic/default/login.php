<?php
use yii\helpers\Html;
use yii\helpers\Url;

use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login admin-login-block">

    <div class="row">
        <div class="col-lg-8 col-centered admin-login">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
            <div class="col-md-4">
<!--                <img src="--><?//= Url::to('default_img/logo.png',true); ?><!--" class="img-responsive" >-->
            </div>
            <div class="col-md-8">
                <?= $form->field($model, 'email')->textInput() ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <div class="form-group text-center">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-admin-login', 'name' => 'login-button']) ?>
                </div>
            </div>
            
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
