<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('site', 'User'),
]) . ' ' . $model->name.' '.$model->surname;
$this->params['breadcrumbs'][] = ['label' => Yii::t('site', 'User'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name.' '.$model->surname];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="users-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
