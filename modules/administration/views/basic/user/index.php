<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;


$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-primary">
    <div class="box-body text-center">
        <?php
        Pjax::begin([
            'id' => 'mr_users_grid',
        ]);
        ?>
        <p>
            <?= Html::a('<i class="fa fa-user-plus"></i>', ['create'], ['class' => 'btn btn-success btn-add-grid','title'=>Yii::t('administration', 'Add User')]) ?>
        </p>
        <?= GridView::widget([
            'options' => ['class' => 'grid-overflow-hidden'],
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'kartik\grid\SerialColumn',
                ],

                [
                    'attribute' => 'email',
                    'label' => Yii::t('administration', 'Email'),
                    'width'=> '20%',
                ],
                [
                    'attribute' => 'name',
                    'label' => Yii::t('administration', 'First name'),
                ],
                [
                    'attribute' => 'surname',
                    'label' => Yii::t('administration', 'Surname'),
                ],
                [
                    'attribute' => 'status',
                    'label' => Yii::t('administration', 'Status'),
                ],


                ['class' => 'kartik\grid\ActionColumn',
                    'template' => '{delete} {views} {update}  ',
                    'header' => Yii::t('administration', 'Action'),
                    'width'=>'5%',

                    'updateOptions' => [
                        'title' => Yii::t('administration','Edit'),
                    ],

                    'deleteOptions' => [
                        'title' => Yii::t('administration','Delete'),
                        'data-confirm' => '<h1 class="text-center"><i class="mr-modal-icon-remove ion ion-trash-a"></i></h1><h2>'.Yii::t('administration','Are you sure want to delete this item').'</h2>'
                    ],

                ],

            ],

            'panelTemplate' => '{panelBefore}{items}{panelFooter}',
            'bordered'=>false,
            'striped'=>false,
            'condensed'=>false,
            'responsive'=>true,
            'responsiveWrap'=>true,
            'hover'=>true,

            'persistResize'=>false,
        ]); ?>
        <?php
        Pjax::end();
        ?>
</div>
</div>