<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;

?>
<h1>Question category</h1>
<?php
    if(Yii::$app->session->hasFlash('category_added')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => 'Category added',
        ]);
    endif; 
    if(Yii::$app->session->hasFlash('category_delete')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => 'Category deleted',
        ]);
    endif; 
    if(Yii::$app->session->hasFlash('category_notadded')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-error',
            ],
            'body' => 'Category not added',
        ]);
    endif; 
?>


<?php $form = ActiveForm::begin(); ?>
        <?= $form->field($modelNewCategory, 'name')->textInput(); ?>
        <?= $form->field($modelNewCategory, 'url_name')->textInput(); ?>
        <?= $form->field($modelNewCategory, 'parent_id')->dropDownList($categoryArray,['prompt'=> 'Select parent Category']); ?>
    <div class="form-group">
        <?= Html::submitButton( Yii::t('app', 'Add Category'), ['name'=> 'change_user_info', 'class' => 'btn btn-primary', 'data-mr-loader'=>'show']) ?>
    </div>
<?php ActiveForm::end();  ?>

<?= GridView::widget([
           'dataProvider' => $modelCategory,
           'columns' => [
               'name',
               'url_name',
               'parent_id',
               [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}',
                    'buttons' => [
                        'delete' => function ($url,$modelCategory) {
                                return Html::a(
                                '<span class="fa fa-times"></span>', 
                                'delete?id='.$modelCategory['id']);
                            
                        },
                    ],
                ],
           ],
       ]) ?>