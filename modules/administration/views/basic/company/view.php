<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\jobs\models\Company */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Companies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Back', ['index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'status:boolean',
            'name',
            'about:ntext',
            [
              'attribute' => 'logo_src',
              'format'=>'image',
              'value' => '/images/company_logo/'.$model->logo_src,
            ],

            'user.name',
            'user.surname',
            'date_create',
            'phone',
            'email:email',
            'website',
            'fax',
            'address',
            'industry_id',
            'facebook',
            'twitter',
            'linkedin',
            'company_create',
        ],
    ]) ?>

</div>
