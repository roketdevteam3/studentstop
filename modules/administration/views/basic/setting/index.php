<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 12.11.15
 * Time: 16:58
 */
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
?>

<div class="box box-primary">
    <div class="box-body">


        <?= GridView::widget([
            'options' => ['class' => 'grid-overflow-hidden'],
            'dataProvider' => $dataProvider,

            'columns' => [
                ['class' => 'kartik\grid\SerialColumn',
                ],

                [
                    'attribute' => 'key',
                    'label' => Yii::t('app', 'Title'),

                    'value' => function ($model) {
                        return ($model->key) ;
                    }
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'value',
                    'pageSummary' => 'Page Total',

                    'vAlign'=>'middle',
                    'headerOptions'=>['class'=>'kv-sticky-column'],
                    'contentOptions'=>['class'=>'kv-sticky-column'],
                    'editableOptions'=>[
                        'header'=>'Name', 'size'=>'md',
                        'ajaxSettings'=>[
                            'url'=> Url::to('/administration/setting/update',true),
                        ],
                        'editableValueOptions'=>['class'=>'fa mr-edit-grid-column']
                    ]
                ],
                [
                    'attribute' => 'category',
                    'label' => Yii::t('app', 'Category'),

                    'value' => function ($model) {
                        return ($model->category) ;
                    }
                ],





                ['class' => 'kartik\grid\ActionColumn',
                    'visible' => false
                ],

            ],

            'panelTemplate' => '{panelBefore}{items}{panelFooter}',
            // set your toolbar
            // parameters from the demo form
            'bordered'=>false,
            'striped'=>false,
            'condensed'=>false,
            'responsive'=>true,
            'responsiveWrap'=>true,
            'hover'=>true,

            'persistResize'=>false,
        ]); ?>


    </div>
</div>