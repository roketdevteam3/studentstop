<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MainMenuItem */

$this->title = Yii::t('app', 'Create Main Menu Item');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Main Menu Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-menu-item-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>