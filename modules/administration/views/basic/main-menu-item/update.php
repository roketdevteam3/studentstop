<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MainMenuItem */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
        'modelClass' => 'Main Menu Item',
    ]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Main Menu Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="main-menu-item-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>