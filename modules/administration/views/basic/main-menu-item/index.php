<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\MainMenuItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Main Menu Items');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
    <div class="box-body text-center">

        <ul class="nav nav-pills">

                <li class="active"><a data-toggle="pill" href="#type_menu1"><?= Yii::t('app', 'Logged') ?></a></li>
                <li class=""><a data-toggle="pill" href="#type_menu2"><?= Yii::t('app', 'Non Login') ?></a></li>
                <li class="">
                    <?= Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success btn-add-grid','title'=>Yii::t('administration', 'Add Organization')]) ?>
                </li>
        </ul>


        <div class="tab-content">
                <div id="type_menu1" class="tab-pane fade in active">

                    <?php
                    Pjax::begin([
                        'id' => 'mr_main_menu_grid_logged',
                    ]);
                    ?>

                    <?= GridView::widget([
                        'options' => ['class' => 'grid-overflow-hidden'],
                        'dataProvider' => $dataProviderLogged,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'kartik\grid\SerialColumn',
                            ],

                            [
                                'attribute' => 'title',
                                'label' => Yii::t('administration', 'Title'),
                                'width'=> '20%',
                            ],
                            [
                                'attribute' => 'link',
                                'label' => Yii::t('administration', 'Link'),
                            ],
                            [
                                'attribute' => Yii::t('app', 'Order'),
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return "<a href=" . Url::to(['main-menu-item/up', 'id' => $model->primaryKey], true) . "'
                           class=\"btn btn-default move-up\" title='" . Yii::t('app', 'Move up') . "'><span
                                class=\"glyphicon glyphicon-arrow-up\"></span></a>
                        <a href='" . Url::to(['main-menu-item/down', 'id' => $model->primaryKey], true) . "'
                           class=\"btn btn-default move-down\" title='" . Yii::t('app', 'Move down') . "'><span
                                class=\"glyphicon glyphicon-arrow-down\"></span></a>";
                                },
                            ],
                            ['class' => 'kartik\grid\ActionColumn',
                                'template' => '{delete} {views} {update}  ',
                                'header' => Yii::t('administration', 'Action'),
                                'width'=>'5%',

                                'updateOptions' => [
                                    'title' => Yii::t('administration','Edit'),
                                ],

                                'deleteOptions' => [
                                    'title' => Yii::t('administration','Delete'),
                                    'data-confirm' => '<h1 class="text-center"><i class="mr-modal-icon-remove ion ion-trash-a"></i></h1><h2>'.Yii::t('administration','Are you sure want to delete this item').'</h2>'
                                ],

                            ],

                        ],

                        'panelTemplate' => '{panelBefore}{items}{panelFooter}',
                        'bordered'=>false,
                        'striped'=>false,
                        'condensed'=>false,
                        'responsive'=>true,
                        'responsiveWrap'=>true,
                        'hover'=>true,

                        'persistResize'=>false,
                    ]); ?>
                    <?php
                    Pjax::end();
                    ?>
                </div>




            <div id="type_menu2" class="tab-pane fade">
                <?php
                Pjax::begin([
                    'id' => 'mr_main_menu_grid_non_logged',
                ]);
                ?>

                <?= GridView::widget([
                    'options' => ['class' => 'grid-overflow-hidden'],
                    'dataProvider' => $dataProviderNonLogin,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'kartik\grid\SerialColumn',
                        ],

                        [
                            'attribute' => 'title',
                            'label' => Yii::t('administration', 'Title'),
                            'width'=> '20%',
                        ],
                        [
                            'attribute' => 'link',
                            'label' => Yii::t('administration', 'Link'),
                        ],
                        [
                            'attribute' => Yii::t('app', 'Order'),
                            'format' => 'raw',
                            'value' => function ($model) {
                                return "<a href=" . Url::to(['main-menu-item/up', 'id' => $model->primaryKey], true) . "'
                           class=\"btn btn-default move-up\" title='" . Yii::t('app', 'Move up') . "'><span
                                class=\"glyphicon glyphicon-arrow-up\"></span></a>
                        <a href='" . Url::to(['main-menu-item/down', 'id' => $model->primaryKey], true) . "'
                           class=\"btn btn-default move-down\" title='" . Yii::t('app', 'Move down') . "'><span
                                class=\"glyphicon glyphicon-arrow-down\"></span></a>";
                            },
                        ],
                        ['class' => 'kartik\grid\ActionColumn',
                            'template' => '{delete} {views} {update}  ',
                            'header' => Yii::t('administration', 'Action'),
                            'width'=>'5%',

                            'updateOptions' => [
                                'title' => Yii::t('administration','Edit'),
                            ],

                            'deleteOptions' => [
                                'title' => Yii::t('administration','Delete'),
                                'data-confirm' => '<h1 class="text-center"><i class="mr-modal-icon-remove ion ion-trash-a"></i></h1><h2>'.Yii::t('administration','Are you sure want to delete this item').'</h2>'
                            ],

                        ],

                    ],

                    'panelTemplate' => '{panelBefore}{items}{panelFooter}',
                    'bordered'=>false,
                    'striped'=>false,
                    'condensed'=>false,
                    'responsive'=>true,
                    'responsiveWrap'=>true,
                    'hover'=>true,

                    'persistResize'=>false,
                ]); ?>
                <?php
                Pjax::end();
                ?>
            </div>


        </div>



    </div>
</div>
