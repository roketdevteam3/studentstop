<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MainMenuItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-primary">
    <div class="box-body ">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'absolute',[
            'template' => ' {input}{label} {error}{hint}'
        ])->checkbox(['label' => null,
            'class'=>'checkbox'],false) ?>

        <?= $form->field($model, 'status',[
            'template' => ' {input}{label} {error}{hint}'
        ])->checkbox(['label' => null,
            'class'=>'checkbox'],false) ?>

        <?php if($model->isNewRecord) $model->logged = true; ?>
        <?= $form->field($model, 'logged',[
            'template' => ' {input}{label} {error}{hint}'
        ])->checkbox(['label' => null,
            'class'=>'checkbox'],false) ?>

        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>


        <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>


        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>