<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\bootstrap\Alert;
    use yii\bootstrap\ActiveForm;
?>
<?php
    if(Yii::$app->session->hasFlash('setting_update')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => 'Setting update',
        ]);
    endif; 
    if(Yii::$app->session->hasFlash('setting_not_update')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-error',
            ],
            'body' => 'Setting not update',
        ]);
    endif; 
?>

<table class="table">
    <tr>
        <th>Setting</th>
        <th>Value</th>
        <th>Date change</th>
        <th></th>
    </tr>
    
    <?php foreach($modelSetting as $setting){ ?>
        <tr>
            <?php $form = ActiveForm::begin(); ?>
                <td><?= $setting->key; ?></td>
                <?= $form->field($setting, 'key')->hiddenInput()->label(false); ?>
                <td>
                    <?php if($setting->key == 'credits'){ ?>
                        <?= $form->field($setting, 'value')->input('number',['step' => "0.01", 'class'=>'form-control'])->label(false); ?>
                    <?php }elseif($setting->key == 'beginning of the semester'){ ?>
                        <?php $beginSemester = json_decode($setting['value']); ?>
                        <div class="col-sm-5" style="padding:0px;">
                            <input class="form-control" name="date_one" type="text" value="<?= $beginSemester->date_one ?>">
                        </div>
                        <div class="col-sm-2" style="text-align:center;">-</div>
                        <div class="col-sm-5" style="padding:0px;">
                            <input class="form-control" name="date_two"  type="text" value="<?= $beginSemester->date_two ?>">
                        </div>
                    <?php } ?>
                </td>
                <td><?= $setting->date_update; ?></td>
                <td><button class="btn btn-primary">Save</button></td>
            <?php $form = ActiveForm::end(); ?>
        </tr>
    <?php } ?>
</table>
