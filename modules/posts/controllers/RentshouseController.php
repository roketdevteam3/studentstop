<?php

namespace app\modules\posts\controllers;

use app\commands\Paypal;
use app\models\PaypalSetting;
use app\models\Usercredits;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Exception\PayPalConnectionException;
use PayPal\Rest\ApiContext;
use Yii;
use yii\base\UserException;
use yii\helpers\Url;
use yii\web\Controller;
use yii\db\Query;
use app\models\Categories;
use app\modules\posts\models\Posts;
use app\modules\posts\models\Postsinfo;
use app\modules\posts\models\Postsrent;
use app\modules\posts\models\Postsphotos;
use app\modules\users\models\User;
use app\models\Notificationusers;
use app\models\Notification;
use app\models\Ratingrequest;
use app\modules\users\models\UserInfo;
use app\modules\posts\models\UserImages;
use app\modules\posts\models\Userrenthouse;
use app\modules\posts\models\Rentshouse;
use app\modules\posts\models\Rentreserve;
use yii\data\ActiveDataProvider;
use yii\web\HttpException;

/**
 * Default controller for the `posts` module
 */
class RentshouseController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    
    public function beforeAction($action) {
        if(!\Yii::$app->user->isGuest){
            if(Yii::$app->user->identity->getUsertUniversity()){
                $this->enableCsrfValidation = false;
                return parent::beforeAction($action);
            }else{
                return $this->redirect(Url::to("/users/choiceuniversity",true));
            }
        }else{
            return $this->redirect(Url::to("/users/login",true));
        }
    }
    
    public function actionIndex()
    {
            $queryRent = new Query;
            $queryRent->select(['*','id' => '{{%renthouse}}.id'])
                ->from('{{%renthouse}}');
        if($_GET){
            //var_dump($_GET);exit;
                if(isset($_GET['rent_value'])){
                    if($_GET['rent_value'] != ''){
                        $queryRent->where(['LIKE', '{{%renthouse}}.address', $_GET['rent_value']]);
//                        $queryRent->orWhere(['LIKE', '{{%renthouse}}.content', $_GET['rent_value']]);
                    }
                }
            
            if((isset($_GET['reserve_from'])) || (isset($_GET['reserve_to']))){
                if(($_GET['reserve_from'] != '') && ($_GET['reserve_to'] == '')){
                    $modelReservRent = Rentreserve::find()
                        ->where('reserve_from >= "'.$_GET['reserve_from'].'" and reserve_to <= "'.$_GET['reserve_from'].'" and rent_type = "rent_house"')
                        ->all();
                }
                if(($_GET['reserve_from'] == '') && ($_GET['reserve_to'] != '')){
                    $modelReservRent = Rentreserve::find()
                        ->orWhere('reserve_from >= "'.$_GET['reserve_to'].'" and reserve_to <= "'.$_GET['reserve_to'].'" and rent_type = "rent_house"')
                        ->all();                    
                }
                if(($_GET['reserve_from'] != '') && ($_GET['reserve_to'] != '')){
                    $modelReservRent = Rentreserve::find()
                        ->where('reserve_from >= "'.$_GET['reserve_from'].'" and reserve_to <= "'.$_GET['reserve_to'].'" and rent_type = "rent_house"')
                        ->orWhere('reserve_from <= "'.$_GET['reserve_from'].'" and reserve_to >= "'.$_GET['reserve_to'].'" and rent_type = "rent_house"')
                        ->orWhere('reserve_from >= "'.$_GET['reserve_from'].'" and reserve_to <= "'.$_GET['reserve_from'].'" and rent_type = "rent_house"')
                        ->orWhere('reserve_from >= "'.$_GET['reserve_to'].'" and reserve_to <= "'.$_GET['reserve_to'].'" and rent_type = "rent_house"')
                        ->all();
                }
                if(($_GET['reserve_from'] != '') || ($_GET['reserve_to'] != '')){
                    $rent_id = '';
                    foreach($modelReservRent as $reserve){
                        $rent_id = $reserve->rent_id;
                    }
                    $queryRent->andWhere(['NOT IN', '{{%renthouse}}.id', $rent_id]);
                }
                
//                ->where(['NOT IN', 'userid', [1001,1002,1003,1004,1005]])
            }
            
            
            if(isset($_GET['room_count'])){
                if($_GET['room_count'] != ''){
                    $queryRent->andWhere('{{%renthouse}}.room_count >= '.$_GET['room_count']);
                }
            }
            
            if(isset($_GET['price_from'])){
                if($_GET['price_from'] != ''){
                    $queryRent->andWhere('{{%renthouse}}.price >= '.$_GET['price_from']);
                }
            }
            if(isset($_GET['price_to'])){
                if($_GET['price_to'] != ''){
                    $queryRent->andWhere('{{%renthouse}}.price <= '.$_GET['price_to']);
                }
            }
            if(isset($_GET['adults'])){
                if($_GET['adults'] != ''){
                    $queryRent->andWhere('{{%renthouse}}.adults >= '.$_GET['adults']);
                }
            }
            if(isset($_GET['university'])){
                if($_GET['university'] != ''){
                    $queryRent->andWhere(['{{%renthouse}}.university_id' => $_GET['university']]);
                }
            }
            if(isset($_GET['price'])){
                if($_GET['price'] != ''){
                    if($_GET['price'] == 1){
                        $queryRent->orderBy('{{%renthouse}}.price ASC');
                    }elseif($_GET['price'] == 2){
                        $queryRent->orderBy('{{%renthouse}}.price DESC');
                    }
                }
            }

            if(isset($_GET['wifi'])){
                if($_GET['wifi'] != ''){
                    $queryRent->andWhere(['{{%renthouse}}.wifi' => 1]);
                }
            }

            if(isset($_GET['parking'])){
                if($_GET['parking'] != ''){
                    $queryRent->andWhere(['{{%renthouse}}.parking' => 1]);
                }
            }


            if(isset($_GET['smoking'])){
                if($_GET['smoking'] != ''){
                    $queryRent->andWhere(['{{%renthouse}}.smoking' => 1]);
                }
            }

            if(isset($_GET['tv'])){
                if($_GET['tv'] != ''){
                    $queryRent->andWhere(['{{%renthouse}}.tv' => 1]);
                }
            }

            if(isset($_GET['animal'])){
                if($_GET['animal'] != ''){
                    $queryRent->andWhere(['{{%renthouse}}.animal' => 1]);
                }
            }

            if(isset($_GET['conditioner'])){
                if($_GET['conditioner'] != ''){
                    $queryRent->andWhere(['{{%renthouse}}.conditioner' => 1]);
                }
            }

            if(isset($_GET['room_count'])){
                if($_GET['room_count'] != ''){
                    $queryRent->andWhere(['{{%renthouse}}.room_count' => $_GET['room_count']]);
                }
            }

            if(isset($_GET['house_type'])){
                if($_GET['house_type'] != ''){
                    $queryRent->andWhere(['{{%renthouse}}.house_type' => $_GET['house_type']]);
                }
            }
            
        }
        $queryRent->orderBy('{{%renthouse}}.date_create DESC');
        
        if ($_POST){
            if (isset($_POST['reserve_paypal'])){
                $rent = new Rentreserve();
                $rent->scenario='reserve';
                $rent->total_price = $_POST['day']*$_POST['price_d'];
                $rent->rent_id = $_POST['rent_id'];
                $rent->reserve_from = $_POST['form_date_for'];
                $rent->reserve_to = $_POST['form_date_to'];
                $rent->rent_type = 'rent_house';
                $rent->user_id = Yii::$app->user->id;
                if($rent->save()){
                    $this->redirect('/rent_pay?id='.$rent->id);
                }
            } elseif (isset($_POST['reserve_credits'])) {
                $credits = Usercredits::find()->where(['user_id'=>Yii::$app->user->id])->one();
                if ($credits) {
                    if ($credits->credit_count >= ($_POST['day']*$_POST['price_c'])){
                        $rent = new Rentreserve();
                        $rent->scenario='reserve';
                        $rent->total_credits = $_POST['day']*$_POST['price_d'];
                        $rent->rent_id = $_POST['rent_id'];
                        $rent->reserve_from = $_POST['form_date_for'];
                        $rent->reserve_to = $_POST['form_date_to'];
                        $rent->rent_type = 'rent_house';
                        $rent->user_id = Yii::$app->user->id;
                        $rent->status = 1;
                        if ($rent->save()){
                            $credits->scenario ='update_credits_count';
                            $credits->credit_count = $credits->credit_count - $rent->total_credits;
                            $credits->save();
                            $rent_house = Rentshouse::findOne($_POST['rent_id']);
                            $addcredits = Usercredits::find()->where(['user_id'=>$rent_house->user_id])->one();
                            $addcredits->credit_count = $addcredits->credit_count + $rent->total_credits;
                            $addcredits->scenario = 'update_credits_count';
                            $addcredits->save();
                            Yii::$app->session->setFlash('rent_ok');
                        }
                    } else {
                        Yii::$app->session->setFlash('not_credits');
                    }
                }
            } elseif (isset($_POST['reserve_request'])){
                $rent = new Rentreserve();
                $rent->scenario='reserve';
                $rent->total_price = $_POST['day']*$_POST['price_d'];
                $rent->rent_id = $_POST['rent_id'];
                $rent->reserve_from = $_POST['form_date_for'];
                $rent->reserve_to = $_POST['form_date_to'];
                $rent->rent_type = 'rent_house';
                $rent->user_id = Yii::$app->user->id;
                $rent->request = 1;
                if($rent->save()){
                    Yii::$app->session->setFlash('reserve_request');
                }
            }
        }

        $modelRents = new ActiveDataProvider(['query' => $queryRent, 'pagination' => ['pageSize' => 20]]);

        $day = 0;
        if(isset($_GET['reserve_from']) and isset($_GET['reserve_to'])) {
            $reserve_to = strtotime($_GET['reserve_to']);
            $reserve_from = strtotime($_GET['reserve_from']);
            $datediff = $reserve_to - $reserve_from;

            $day = floor($datediff / (60 * 60 * 24));
        }
        
        return $this->render('index',[
            'modelRents' => $modelRents->getModels(),
            'pagination' => $modelRents->pagination,
            'count' => $modelRents->pagination->totalCount,
            'day' => $day,
        ]);
    }
    
    public function actionMyprofile()
    {

        $modelNewRenthouse = new Rentshouse();
        $modelNewRenthouse->scenario = 'add_rent';

        if($_POST){
//            var_dump(Yii::$app->request->post());
//            if ($paypalsearch->validate()) {

                $modelUserInfo = UserInfo::find()->where(['id_user' => \Yii::$app->user->id])->one();
                $request = Yii::$app->request;
                if ($modelNewRenthouse->load($request->post())) {
//                    var_dump($modelNewRenthouse);exit;
                    $modelNewRenthouse->university_id = $modelUserInfo->university_id;
                    if ($modelNewRenthouse->save()) {
                        if (isset($_POST['photosInput'])) {
                            foreach (json_decode($_POST['photosInput']) as $image_name) {
                                $modelNewPostsphotos = new Postsphotos();
                                $modelNewPostsphotos->scenario = 'add_photo';
                                $modelNewPostsphotos->post_id = $modelNewRenthouse->id;
                                $modelNewPostsphotos->image_src = $image_name;
                                $modelNewPostsphotos->save();
                            }
                        }
                        $modelNewRenthouse = new Rentshouse();
                        $modelNewRenthouse->scenario = 'add_rent';
                        
                        Yii::$app->session->setFlash('Rentadded');
                    } else {
                        Yii::$app->session->setFlash('Rentnotadded');
                    }
                }
           /* } else {
                Yii::$app->session->setFlash('Paypal');
            }*/

        }

//        $paypal = new PaypalSetting();

        if (Yii::$app->request->get('id')&&Yii::$app->request->get('request') ){
            $request_up=Rentreserve::find()->where(['id'=>Yii::$app->request->get('id'),'request'=>1])->one();
            if ($request_up){
                $request_up->request = Yii::$app->request->get('request');
                $request_up->view = 1;
                $request_up->scenario ='request';
                $request_up->save();
            }
        }




        $queryMyRent = Rentshouse::find()->where(['user_id' => \Yii::$app->user->id])->orderBy('date_create DESC');
        $myRent = new ActiveDataProvider(['query' => $queryMyRent, 'pagination' => ['pageSize' => 30]]);

        $mas_rent = [];

        foreach ($queryMyRent->all() as $item){
          $mas_rent[]=$item->id;
        }

        $request = Rentreserve::find()->where(['rent_id'=>$mas_rent,'rent_type' => 'rent_house', 'request'=>1 ,'status'=>0])->all();

        return $this->render('myprofile',[
            'modelNewRenthouse' => $modelNewRenthouse,
            'myRent' => $myRent->getModels(),
            'pagination' => $myRent->pagination,
            'count' => $myRent->pagination->totalCount,
            'request' => $request,
//            'paypal' => $paypalsearch
        ]);    
    }

    public function actionGetrentdata(){
        $rent_id = $_POST['rent_id'];
        $result = [];
        $arrayRentshouseData = Rentshouse::find()->where(['id' => $rent_id])->asArray()->one();
        if($arrayRentshouseData){
            $result['status'] = 'success';
            $modelRentsHouseImages = Postsphotos::find()->where(['post_id' => $rent_id])->asArray()->all();
            $result['rent_data'] = $arrayRentshouseData;
            $result['rent_img'] = $modelRentsHouseImages;
        }else{
            $result['status'] = 'error';            
        }
        echo json_encode($result);
    }
    
    
    public function actionPay_rent($id){
        if (!Yii::$app->user->isGuest) {

            $rent_ser = Rentreserve::findOne($id);
            $rent = Rentshouse::findOne($rent_ser->rent_id);



            $dPrice = $rent_ser->total_price;

//            $paypal = PaypalSetting::find()->where(['user_id'=>$rent->user_id])->one();

           /* if ($paypal){
                $pay = new ApiContext(
                    new OAuthTokenCredential(
                        $paypal->client_id,
                        $paypal->secret
                    )
                );
            } else {
                throw new HttpException(403);
            }*/

            $pay = New Paypal();
            $pay = $pay->api;

            $pay->setConfig([
                'model'=>'sandbox',
                'http.ConnectionTimeOut'=>30,
                'log.LogEnabled'=> YII_DEBUG ? 1 : 0,
                'log.FileName'=> '',
                'log.LogLevel' => 'FINE',
                'validation.level'=>'log'
            ]);

            $payer = new Payer();
            $payer->setPaymentMethod("paypal");

            $item2 = new Item();
            $item2->setName('Payment rent borrow')
                ->setCurrency('USD')
                ->setQuantity(1)
                ->setSku('N-C-'+time()) // Similar to `item_number` in Classic API
                ->setPrice($dPrice);
            $itemList = new ItemList();
            $itemList->setItems(array($item2));


            $details = new Details();
            $details->setShipping(0.00)
                ->setTax(0.00)
                ->setSubtotal($dPrice);


            $amount = new Amount();
            $amount->setCurrency("USD")
                ->setTotal($dPrice+0.00)
                ->setDetails($details);



            $transaction = new Transaction();
            $transaction->setAmount($amount)
                ->setItemList($itemList)
                ->setDescription('Paymend rent house ')
                ->setInvoiceNumber(uniqid());

            $redirectUrls = new RedirectUrls();
            $redirectUrls->setReturnUrl(Url::home(true).'pay?approved=true&user_paymend='.$rent->user_id.'&rent_id='.$id)
                ->setCancelUrl(Url::home(true).'pay?approved=false&user_paymend='.$rent->user_id.'&rent_id='.$id);


            $payment = new Payment();
            $payment->setIntent("sale")
                ->setPayer($payer)
                ->setRedirectUrls($redirectUrls)
                ->setTransactions(array($transaction));

            try{
                $payment->create($pay);
            }catch (PayPalConnectionException $e){

                throw new UserException($e->getMessage());

            }
            $trans= new \app\models\Transaction();
            $trans->user_id= Yii::$app->user->id;
            $trans->price=$dPrice;
            $trans->payment_id=$payment->getId();
            $trans->hash=md5($payment->getId());
            $trans->othe='Paymend rent house';
            $trans->project_id=$id;
            $trans->save();
            Yii::$app->getResponse()->redirect($payment->getApprovalLink());
        } else {
            throw new HttpException(403);
        }
    }

    public function actionPay(){
        $paypal = PaypalSetting::find()->where(['user_id'=>$_GET['user_paymend']])->one();

       /* if ($paypal){
            $pay = new ApiContext(
                new OAuthTokenCredential(
                    $paypal->client_id,
                    $paypal->secret
                )
            );
        } else {
            throw new HttpException(403);
        }*/
        $pay = New Paypal();
        $pay = $pay->api;

        if (isset($_GET['approved']) && $_GET['approved'] == 'true') {
            $paymentId = $_GET['paymentId'];
            $payment = Payment::get($paymentId, $pay);
            $execution = new PaymentExecution();
            $execution->setPayerId($_GET['PayerID']);
            try {
                $result = $payment->execute($execution, $pay);
            } catch (PayPalConnectionException $e) {
                throw new UserException($e->getMessage());
            }
            $transaction = \app\models\Transaction::find()->where(['user_id'=>Yii::$app->user->id,'hash'=>md5($paymentId)])->one();
            $transaction->complete=1;
            $transaction->save();

            $rent = Rentreserve::findOne($_GET['rent_id']);
            $rent->scenario = 'status';
            $rent->status = 1;
            if($rent->save()){
                
                $modelRent = Rents::find()->where(['id' => $_GET['rent_id']])->asArray()->one();
                $newModelNotification = new Notification();
                $newModelNotification->scenario = 'add_notification';
                $newModelNotification->module = 'rentshouse';
                $newModelNotification->from_object_id = $rent->id;
                $newModelNotification->to_object_id = $_GET['rent_id'];
                $newModelNotification->university_id = 0;
                $newModelNotification->notification_type = 'rent_my_rent';
                if($newModelNotification->save()){
                    $modelNotificationUser = new Notificationusers();
                    $modelNotificationUser->scenario = 'new_notification';
                    $modelNotificationUser->user_id = $modelRent['user_id'];
                    $modelNotificationUser->notification_id = $newModelNotification->id;
                    $modelNotificationUser->save();
                }
                
                $newModelNotification = new Notification();
                $newModelNotification->scenario = 'add_notification';
                $newModelNotification->module = 'rentshouse';
                $newModelNotification->from_object_id = $rent->id;
                $newModelNotification->to_object_id = $_GET['rent_id'];
                $newModelNotification->university_id = 0;
                $newModelNotification->notification_type = 'pay_my_rent';
                if($newModelNotification->save()){
                    $modelNotificationUser = new Notificationusers();
                    $modelNotificationUser->scenario = 'new_notification';
                    $modelNotificationUser->user_id = $modelRent['user_id'];
                    $modelNotificationUser->notification_id = $newModelNotification->id;
                    $modelNotificationUser->save();
                }
                
                $newModelNotification = new Notification();
                $newModelNotification->scenario = 'add_notification';
                $newModelNotification->module = 'rentshouse';
                $newModelNotification->from_object_id = $rent->id;
                $newModelNotification->to_object_id = $_GET['rent_id'];
                $newModelNotification->university_id = 0;
                $newModelNotification->notification_type = 'i_rent_rent';
                if($newModelNotification->save()){
                    $modelNotificationUser = new Notificationusers();
                    $modelNotificationUser->scenario = 'new_notification';
                    $modelNotificationUser->user_id = \Yii::$app->user->id;
                    $modelNotificationUser->notification_id = $newModelNotification->id;
                    $modelNotificationUser->save();
                }
                
            }

            $user_many = Usercredits::find()->where(['user_id'=>$rent->renthouse['user_id']])->one();
            if ($user_many) {
                $user_many->scenario = 'update_many_count';
                $user_many->many = $user_many->many + $transaction->price;
                $user_many->save();
            } else {
                $u_m = new Usercredits();
                $u_m->many=$transaction->price;
                $u_m->user_id = $rent->renthouse['user_id'];
                $u_m->credit_count = 0;
                $u_m->scenario = 'add_credits';
                $u_m->save();
            }

            Yii::$app->session['rent_ok']=$rent->id;

            return $this->redirect('/rentshouse');
        } else {
            Yii::$app->session['rent_cansel']=true;
            return $this->redirect('/rentshouse');
        }
    }
    
    public function actionView($rent_id)
    {
        $modelRent = Rentshouse::find()->where(['id' => $rent_id])->one();
        $newRatingRequest = new Ratingrequest();
        $query = new Query;
        $query->select(['*','id' => '{{%user}}.id'])
            ->from('{{%user}}')
            ->leftJoin('{{%user_info}}', '{{%user_info}}.id_user = {{%user}}.id')
            ->where(['{{%user}}.id' => $modelRent->user_id]);
        
        $command = $query->createCommand();
        $userPosted = $command->queryOne();
        
        if($_POST){
            if(isset($_POST['reserve_rent'])){
                $modelCancel = Rentreserve::find()
                        ->where('reserve_from >= "'.$_POST['reserve_from'].'" and reserve_to <= "'.$_POST['reserve_to'].'" and rent_type = "rent_house" and rent_id = "'.$rent_id.'"')
                        ->orWhere('reserve_from <= "'.$_POST['reserve_from'].'" and reserve_to >= "'.$_POST['reserve_to'].'" and rent_type = "rent_house" and rent_id = "'.$rent_id.'"')
                        ->orWhere('reserve_from >= "'.$_POST['reserve_from'].'" and reserve_to <= "'.$_POST['reserve_from'].'" and rent_type = "rent_house" and rent_id = "'.$rent_id.'"')
                        ->orWhere('reserve_from >= "'.$_POST['reserve_to'].'" and reserve_to <= "'.$_POST['reserve_to'].'" and rent_type = "rent_house" and rent_id = "'.$rent_id.'"')
                        ->all();
                if($modelCancel == null){
                    $modelNew = new Rentreserve();
                    $modelNew->scenario = 'reserve';
                    $modelNew->rent_id = $_POST['rent_id'];
                    $modelNew->user_id = \Yii::$app->user->id;
                    $modelNew->rent_type = 'rent_house';
                    $modelNew->reserve_from = $_POST['reserve_from'];
                    $modelNew->reserve_to = $_POST['reserve_to'];
                    if($modelNew->save()){
                        Yii::$app->session->setFlash('Reserve');
                    }else{
                        Yii::$app->session->setFlash('Notreserve');
                    }
                }else{
                    Yii::$app->session->setFlash('reserveBusy');
                }
            }
            if(isset($_POST['Rating'])){
                $modelRentreserve = Rentreserve::find()->where(['id' => $_POST['Rating']['reserve_id']])->one();
                $modelRentreserve->scenario = 'reserve_rating';
                $modelRentreserve->rating_comment = $_POST['Rating']['comment'];
                $modelRentreserve->rating_count = $_POST['Rating']['rating_count'];
                if($modelRentreserve->save()){
                    $modelRatingrequest = new Ratingrequest();
                    $modelRatingrequest->scenario = 'add_rating_request';
                    $modelRatingrequest->from_user_id = $_POST['Rating']['from_user_id'];
                    $modelRatingrequest->to_object_id = $_POST['Rating']['to_object_id'];
                    $modelRatingrequest->rating_count = $_POST['Rating']['rating_count'];
                    $modelRatingrequest->type = 'rental_a_house';
                        if($modelRatingrequest->save()){
                            return $this->redirect('/rentshouse/view/'.$rent_id);
                        }
                }
            }
        }

        $modelRentreserve = Rentreserve::find()->where(['rent_type' => 'rent_house',  'rent_id' => $rent_id])->all();
        return $this->render('view',[
            'rent' => $modelRent,
            'newRatingRequest' => $newRatingRequest,
            'userPosted' => $userPosted,
            'modelRentreserve' => $modelRentreserve
        ]);
    }
    
    public function actionMyreservations()
    {
        $request = Rentreserve::find()->where(['user_id'=>Yii::$app->user->id,'rent_type' => 'rent_house','status'=>0])->andWhere(['!=','request',0])->all();
        $query = new Query;
        $query->select(['*','id' => '{{%renthouse}}.id'])
            ->from('{{%rent_reserve}}')
            ->leftJoin('{{%renthouse}}', '{{%renthouse}}.id = {{%rent_reserve}}.rent_id')
            ->where(['{{%rent_reserve}}.user_id' => \Yii::$app->user->id, 'rent_type' => 'rent_house', 'status'=>1]);
        $reservationRent = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 30]]);

        if (isset($_POST['add_reting'])) {
            $modelRentreserve = Rentreserve::find()->where(['id' => $_POST['Rating']['reserve_id']])->one();
            $modelRentreserve->scenario = 'reserve_rating';
            $modelRentreserve->rating_comment = $_POST['Rating']['comment'];
            $modelRentreserve->rating_count = $_POST['Rating']['rating_count'];
            if($modelRentreserve->save()){
                $modelRatingrequest = new Ratingrequest();
                $modelRatingrequest->scenario = 'add_rating_request';
                $modelRatingrequest->from_user_id = $_POST['Rating']['from_user_id'];
                $modelRatingrequest->to_object_id = $_POST['Rating']['to_object_id'];
                $modelRatingrequest->rating_count = $_POST['Rating']['rating_count'];
                $modelRatingrequest->type = 'rental_a_house';
                if($modelRatingrequest->save()){
                    return $this->redirect('/rentshouse/myreservations');
                }
            }
        }

        return $this->render('myreservations',[
            'reservationRent' => $reservationRent->getModels(),
            'pagination' => $reservationRent->pagination,
            'request' => $request,
            'count' => $reservationRent->pagination->totalCount,
        ]);    
    }
    
    public function actionFavorite()
    {
        $query = new Query;
        $query->select(['*','id' => '{{%renthouse}}.id','date_create' => '{{%user_renthouse}}.date_create'])  
            ->from('{{%user_renthouse}}')
            ->leftJoin('{{%renthouse}}', '{{%renthouse}}.id = {{%user_renthouse}}.renthouse_id')
            ->where(['{{%user_renthouse}}.user_id' => \Yii::$app->user->id]);
        $favoriteRent = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 30]]);
        
        return $this->render('favorite',[
            'favoriteRent' => $favoriteRent->getModels(),
            'pagination' => $favoriteRent->pagination,
            'count' => $favoriteRent->pagination->totalCount,
        ]);    
    }
    
    public function actionAddrenthousetofavorite()
    {
        $result = [];
        $modalUserJobs = new Userrenthouse();
        $modalUserJobs->scenario = 'add_favorite_renthouse';
        $modalUserJobs->user_id = \Yii::$app->user->id;
        $modalUserJobs->renthouse_id = $_POST['renthouse_id'];
        if($modalUserJobs->save()){
            $result['status'] = 'success';
            
            $modelRentHouse = Rentshouse::find()->where(['id' => $_POST['renthouse_id']])->asArray()->one();
            $newModelNotification = new Notification();
            $newModelNotification->scenario = 'add_notification';
            $newModelNotification->module = 'rentshouse';
            $newModelNotification->from_object_id = \Yii::$app->user->id;
            $newModelNotification->to_object_id = $_POST['renthouse_id'];
            $newModelNotification->university_id = 0;
            $newModelNotification->notification_type = 'add_my_rentshouse_to_favorite';
            if($newModelNotification->save()){
                $modelNotificationUser = new Notificationusers();
                $modelNotificationUser->scenario = 'new_notification';
                $modelNotificationUser->user_id = $modelRentHouse['user_id'];
                $modelNotificationUser->notification_id = $newModelNotification->id;
                $modelNotificationUser->save();
            }
        }else{
            $result['status'] = 'error';
        }
        
        echo json_encode($result);
    }
    
    public function actionDeleterenthousewithfavorite()
    {
        $result = [];
        $modalUserJobs = Userrenthouse::find()->where(['user_id' => \Yii::$app->user->id, 'renthouse_id' => $_POST['renthouse_id']])->one();
        if($modalUserJobs->delete()){
            $result['status'] = 'success';
        }else{
            $result['status'] = 'error';
        }
        
        echo json_encode($result);
    }

    public function actionAdd_views($id){
        $renthouse = Rentshouse::findOne($id);
        $renthouse->scenario = 'view';
        $renthouse->views = $renthouse->views+1;
        $renthouse->save();
        return true;
    }

    public function actionMetod_pay($metod,$id){
        $rent_r = Rentreserve::findOne($id);
        if ($rent_r){
            if ($metod == '1') {
                $this->redirect('/rent_pay?id='.$rent_r->id);
            }
            if ($metod == '2') {
                $credits = Usercredits::find()->where(['user_id'=>Yii::$app->user->id])->one();
                if ($credits) {
                    if ($credits->credit_count >= $rent_r->total_credits){
                        $rent_r->scenario='status';
                        $rent_r->status = 1;
                        if ($rent_r->save()){
                            $credits->scenario ='update_credits_count';
                            $credits->credit_count = $credits->credit_count - $rent_r->total_credits;
                            $credits->save();
                            $rent_house = Rentshouse::findOne($rent_r->rent_id);
                            $addcredits = Usercredits::find()->where(['user_id'=>$rent_house->user_id])->one();
                            $addcredits->credit_count = $addcredits->credit_count + $rent_r->total_credits;
                            $addcredits->scenario = 'update_credits_count';
                            $addcredits->save();
                            Yii::$app->session['rent_ok']=$rent_r->id;
                        }
                    } else {
                        Yii::$app->session->setFlash('not_credits');
                    }
                }
            }

        } else {
            $this->redirect('/rentshouse/myreservations');
        }

    }

    public function actionDelete_renthouse($id){
      $rent = Rentshouse::findOne($id)->delete();
      return true;
    }
    
    public function actionRentupdate(){
        $modelNewRenthouse = Rentshouse::find()->where(['id' => $_POST['Rentshouse']['id']])->one();
        if($modelNewRenthouse){
            $modelNewRenthouse->scenario = 'add_rent';
            $request = Yii::$app->request;
            if($request){
                if ($modelNewRenthouse->load($request->post())) {
                    if ($modelNewRenthouse->save()) {
                        Postsphotos::deleteAll(['post_id' => $modelNewRenthouse->id]);
                        if (isset($_POST['photosInputC'])) {
                            $phototArray = json_decode($_POST['photosInputC']);
                            if($phototArray){
                                foreach ($phototArray as $image_name) {
                                    $modelNewPostsphotos = new Postsphotos();
                                    $modelNewPostsphotos->scenario = 'add_photo';
                                    $modelNewPostsphotos->post_id = $modelNewRenthouse->id;
                                    $modelNewPostsphotos->image_src = $image_name;
                                    $modelNewPostsphotos->save();
                                }                                
                            }
                        }
                        
                        Yii::$app->session->setFlash('Rentupdate');
                    } else {
                        Yii::$app->session->setFlash('Rentnotupdate');
                    }
                }                        
            }else{
                    Yii::$app->session->setFlash('Rentnotupdate');                
            }
        }else{
            Yii::$app->session->setFlash('Rentnotupdate');            
        }
        
        return $this->redirect('/rentshouse/myprofile');
    }
    
}
