<?php

namespace app\modules\posts\controllers;

use app\modules\posts\models\PostContsctRequest;
use Yii;
use yii\db\Query;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\Categories;
use app\modules\posts\models\Posts;
use app\models\Notificationusers;
use app\models\Notification;
use app\modules\posts\models\Userpost;
use app\modules\posts\models\Postsinfo;
use app\modules\users\models\User;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * Default controller for the `posts` module
 */
class PostsController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    
    public function beforeAction($action) {
        if(!\Yii::$app->user->isGuest){
            if(Yii::$app->user->identity->getUsertUniversity()){
                $this->enableCsrfValidation = false;
                return parent::beforeAction($action);
            }else{
                return $this->redirect(Url::to("/users/choiceuniversity",true));
            }
        }else{
            return $this->redirect(Url::to("/users/login",true));
        }
    }
    
    public function actionIndex()
    {
        $postCategory = Categories::find()->where(['category_type' => 3])->all();
        
        foreach ($postCategory as $category){
           $category->child_category = Categories::find()->where(['parent_id' => $category['id']])->limit(3)->all();
        }
        
        return $this->render('index',[
            'postCategory' => $postCategory,
        ]);
    }
    
    public function actionView_zero($id)
    {
        PostContsctRequest::updateAll(['view'=>1],['post_id'=>$id]);
        return true;
    }

    public function actionCategory($category_url)
    {
//        /echo $category;
        $postCategory = Categories::find()->where(['url_name' => $category_url])->one();
        $postCategory->child_category = Categories::find()->where(['parent_id' => $postCategory->id])->all();
        
        return $this->render('category',[
            'postCategory' => $postCategory,
        ]);
    }
    
    public function actionDelete($post_id)
    {
        $postModel = Posts::find()->where(['id' => $post_id])->one();
        $postCategory = Categories::find()->where(['id' => $postModel->category_id])->one();
        
        if($postModel->delete()){
            return $this->redirect(Url::home().'market/myprofile');
        }
    }
    
    public function actionFavorite(){
        $query = new Query;
        $query->select(['*','id' => '{{%posts}}.id'])  
            ->from('{{%user_post}}')
            ->leftJoin('{{%posts}}', '{{%posts}}.id = {{%user_post}}.post_id')
            ->where(['{{%user_post}}.user_id' => \Yii::$app->user->id]);

        $postModels = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 30]]);

        if(isset($_POST['contact_request'])){
            $cont_r = new PostContsctRequest();
            $cont_r->user_id = Yii::$app->user->id;
            $cont_r->post_id = $_POST['post_id'];
            $cont_r->save();
            Yii::$app->session->setFlash('contact_request');
        }


        return $this->render('favorite',[
            'postModel' => $postModels->getModels(),
            'pagination' => $postModels->pagination,
            'count' => $postModels->pagination->totalCount,
        ]);
    }
    
    public function actionCategorychild($category_url,$category_child)
    {
        if($_GET){
            if(isset($_GET['category'])){
                if($category_child != $_GET['category']){
                    $categoryGet = Categories::find()->where(['url_name' => $_GET['category']])->one();
                    if($categoryGet->parent_id){
                        $categoryGetP = Categories::find()->where(['id' => $categoryGet->parent_id])->one();
                        return $this->redirect(['/market/'.$categoryGetP['url_name'].'/'.$categoryGet['url_name'],
                            'posts_content' => $_GET['posts_content'],
                            'category' => $_GET['category'],
                            'university' => $_GET['university'],
                            'searchButton' => $_GET['searchButton'],
                        ]);
                    }
                }
            }      
        }
        $parentPostCategory = Categories::find()->where(['url_name' => $category_url])->one();
        $postCategory = Categories::find()->where(['url_name' => $category_child])->one();
        
        $query = new Query;
        $query->select(['*','id' => '{{%posts}}.id','date_create' => '{{%posts}}.date_create' ])  
            ->from('{{%posts}}')
            ->leftJoin('{{%user}}', '{{%user}}.id = {{%posts}}.user_id')
            ->where(['{{%posts}}.category_id' => $postCategory->id, '{{%posts}}.publish_status' => 1])
            ->orderBy(['{{%posts}}.date_create' => SORT_DESC]);

        if($_GET){
            if(isset($_GET['posts_content'])){
                if($_GET['posts_content'] != ''){
                    $query->andWhere(['like','{{%posts}}.title',$_GET['posts_content']])
                        ->orWhere(['like','{{%posts}}.content',$_GET['posts_content']]);
                }
            }
            if(isset($_GET['university'])){
                if($_GET['university'] != ''){
                    $query->andWhere(['{{%posts}}.university_id' => $_GET['university']]);
                }
            }
        }
        //$query = Posts::find()->where(['category_id' => $postCategory->id]);
        $postModel = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 30]]);

        if(isset($_POST['contact_request'])){
            $cont_r = new PostContsctRequest();
            $cont_r->user_id = Yii::$app->user->id;
            $cont_r->post_id = $_POST['post_id'];
            $cont_r->save();
                $modelPost = Posts::find()->where(['id' => $_POST['post_id']])->asArray()->one();
                $newModelNotification = new Notification();
                $newModelNotification->scenario = 'add_notification';
                $newModelNotification->module = 'market';
                $newModelNotification->from_object_id = \Yii::$app->user->id;
                $newModelNotification->to_object_id = $_POST['post_id'];
                $newModelNotification->university_id = 0;
                $newModelNotification->notification_type = 'request_to_post_user';
                if($newModelNotification->save()){
                    $modelNotificationUser = new Notificationusers();
                    $modelNotificationUser->scenario = 'new_notification';
                    $modelNotificationUser->user_id = $modelPost['user_id'];
                    $modelNotificationUser->notification_id = $newModelNotification->id;
                    $modelNotificationUser->save();
                }
                
            
            Yii::$app->session->setFlash('contact_request');
        }


        return $this->render('category_child',[
            'postCategory' => $postCategory,
            'parentPostCategory' => $parentPostCategory,
            'postModel' => $postModel->getModels(),
            'pagination' => $postModel->pagination,
            'count' => $postModel->pagination->totalCount,
        ]);
    }

    public function actionAddviews($id){
        $post = Posts::findOne($id);
        $post->scenario = 'views';
        $post->views = $post->views+1;
        $post->save();
        return true;
    }
    
    public function actionGetcategory(){
        $categoryArray = [];
        $postCategory = Categories::find()->where(['category_type' => 3])->all();
        foreach ($postCategory as $category){
            $categoryArray[$category->category_name] = ArrayHelper::map(Categories::find()->where(['parent_id' => $category['id']])->all(),'id','category_name');
        }
        echo json_encode($categoryArray);
    }
    
    public function actionSavemarketpost(){
        $result = [];
        
        $modelUserInfo = \app\modules\users\models\UserInfo::find()->where(['id_user' => \Yii::$app->user->id])->asArray()->one();       
        $modelFriend = \app\models\user\UserFriend::find()->where(['id_user' => \Yii::$app->user->id])->asArray()->all();
        $friendId = [];
        foreach($modelFriend as $friend){
            $friendId[] = $friend['id_friend'];
        }
        
        $newPost = new Posts;
        $newPost->scenario = 'add_posts';
        if($_POST){
            $newPost->user_id = \Yii::$app->user->id;
            $newPost->title = $_POST['post_title'];
            $newPost->content = $_POST['posts_content'];
            $newPost->category_id = $_POST['post_category'];
            $newPost->image_src = $_POST['posts_image_src'];
            $newPost->address = $_POST['posts_address'];
            $newPost->university_id = $modelUserInfo['university_id'];
            if($newPost->save()){
                
                $newModelNotification = new Notification();
                $newModelNotification->scenario = 'add_notification';
                $newModelNotification->module = 'market';
                $newModelNotification->from_object_id = \Yii::$app->user->id;
                $newModelNotification->to_object_id = $newPost->id;
                $newModelNotification->university_id = $modelUserInfo['university_id'];
                $newModelNotification->notification_type = 'friend_add_new_post';
                if($newModelNotification->save()){
                    foreach($friendId as $user_id){
                        $modelNotificationUser = new Notificationusers();
                        $modelNotificationUser->scenario = 'new_notification';
                        $modelNotificationUser->user_id = $user_id;
                        $modelNotificationUser->notification_id = $newModelNotification->id;
                        $modelNotificationUser->save();
                    }                
                }
                
                $result['status'] = 'success';
            }else{
                $result['status'] = 'error';
            }
        }else{
            $result['status'] = 'error';
        }
        echo json_encode($result);
    }
    
    public function actionMyprofile(){
        $postCategory = Categories::find()->where(['category_type' => 3])->all();
        $modelUserInfo = \app\modules\users\models\UserInfo::find()->where(['id_user' => \Yii::$app->user->id])->one();
        $categoryArray = [];
        foreach ($postCategory as $category){
            $categoryArray[$category->category_name] = ArrayHelper::map(Categories::find()->where(['parent_id' => $category['id']])->all(),'id','category_name');
        }
        
        $newPost = new Posts;
        $newPost->scenario = 'add_posts';
        if($_POST){
            $request = Yii::$app->request;
            if($newPost->load($request->post())){
                $newPost->user_id = \Yii::$app->user->id;
                $newPost->university_id = $modelUserInfo->university_id;
                if($newPost->save()){
                    Yii::$app->session->setFlash('PostsAdded');
                    $newPost = new Posts;
                    $newPost->scenario = 'add_posts';
                }
            }
        }
        
        $query = new Query;
        $query->select(['*','id' => '{{%posts}}.id','date_create' => '{{%posts}}.date_create', 'count_inquires' => '(SELECT count(id) FROM mr_post_contsct_request WHERE post_id = mr_posts.id)' ])
            ->from('{{%posts}}')
            ->leftJoin('{{%user}}', '{{%user}}.id = {{%posts}}.user_id')
            ->where(['{{%posts}}.user_id' => \Yii::$app->user->id])
            ->orderBy([ 'count_inquires'=>SORT_DESC]);

        $postModel = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 20]]);
        return $this->render('myprofile',[
            'newPost' => $newPost,
            'categoryArray' => $categoryArray,
            'postModel' => $postModel->getModels(),
            'pagination' => $postModel->pagination,
            'count' => $postModel->pagination->totalCount,
        ]);
    }
    
    public function actionView($post_id)
    {
        $postViewModel = Posts::find()->where(['id' => $post_id])->one();
        $postViewModel->scenario = 'update_posts';
        if($_POST){
            $request = Yii::$app->request;
            $postViewModel->date_update = date("Y-m-d H:i:s");
            if($postViewModel->load($request->post()) && ($postViewModel->save())){
                Yii::$app->session->setFlash('PostsUpdate');
            }
        }
        
        $query = new Query;
        $query	->select(['*','id'=>'{{%user}}.id'])
                        ->from('{{%user}}')
                        ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%user}}.id'
                                )->where(['{{%user}}.id' => $postViewModel->user_id])->limit('100');
        
        $command = $query->createCommand();
        $userPosted = $command->queryOne();
        
        return $this->render('view',[
            'postViewModel' => $postViewModel,
            'userPosted' => $userPosted,
        ]);
    }
    
    public function actionPostview($category_url,$category_child,$post_id)
    {
        
        $parentPostCategory = Categories::find()->where(['url_name' => $category_url])->one();
        $postCategory = Categories::find()->where(['url_name' => $category_child])->one();
        
        $postViewModel = Posts::find()->where(['id' => $post_id])->one();
        $postViewModel->scenario = 'update_posts';
        $postinfoViewModel = Postsinfo::find()->where(['post_id' => $postViewModel->id])->one();
        $postinfoViewModel->scenario = 'update_posts';
        if($_POST){
            $request = Yii::$app->request;
            $postViewModel->date_update = date("Y-m-d H:i:s");
            if($postViewModel->load($request->post()) && ($postViewModel->save())){
                if($postinfoViewModel->load($request->post()) && ($postinfoViewModel->save())){
                    Yii::$app->session->setFlash('PostsUpdate');
                }
            }
        }
        
        $query = new Query;
        $query	->select(['*','id'=>'{{%user}}.id'])
                        ->from('{{%user}}')
                        ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%user}}.id'
                                )->where(['{{%user}}.id' => $postViewModel->user_id])->limit('100');
        
        $command = $query->createCommand();
        $userPosted = $command->queryOne();
        
        return $this->render('post_view',[
            'postViewModel' => $postViewModel,
            'postinfoViewModel' => $postinfoViewModel,
            'parentPostCategory' => $parentPostCategory,
            'postCategory' => $postCategory,
            'userPosted' => $userPosted,
        ]);
    }
    
    
    
    public function actionSavepostsphoto()
    {
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/images/posts_images/';   //2
        // mkdir($storeFolder);
        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
              mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }
              $tempFile = $_FILES['file']['tmp_name'];          //3

              $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;  //4

              $for_name = time();
              
              $targetFile =  $targetPath.$for_name.'.jpg';  //5
              
              move_uploaded_file($tempFile,$targetFile); //6
              return \Yii::$app->user->id.'/'.$for_name.'.jpg';
        }
    }
    
    public function actionDeletepostsphoto(){
        $storeFolder = \Yii::getAlias('@webroot').'/images/posts_images/'; 
        $path = $storeFolder.$_POST['file_name'];
        if(unlink($path)){
            echo 'delete';
        }else{
            echo 'problem';
        }
    }
    
    public function actionPostdelete($category_url,$category_child,$post_id){
        $postModel = Posts::find()->where(['id' => $post_id])->one();
        $postModel->delete();
        Yii::$app->session->setFlash('PostsDelete');
        return $this->redirect('/market/'.$category_url.'/'.$category_child);
    }
    
    public function actionAddposttofavorite()
    {
        $result = [];
        $postModel = Posts::find()->where(['id' => $_POST['post_id']])->one();
        $modelUserPost = new Userpost();
        $modelUserPost->scenario = 'add_favorite_post';
        $modelUserPost->user_id = \Yii::$app->user->id;
        $modelUserPost->post_id = $_POST['post_id'];
        if($modelUserPost->save()){
                
                $newModelNotification = new Notification();
                $newModelNotification->scenario = 'add_notification';
                $newModelNotification->module = 'market';
                $newModelNotification->from_object_id = \Yii::$app->user->id;
                $newModelNotification->to_object_id = $postModel->id;
                $newModelNotification->university_id = 0;
                $newModelNotification->notification_type = 'add_my_post_to_favorite';
                if($newModelNotification->save()){
                    
                        $modelNotificationUser = new Notificationusers();
                        $modelNotificationUser->scenario = 'new_notification';
                        $modelNotificationUser->user_id = $postModel->user_id;
                        $modelNotificationUser->notification_id = $newModelNotification->id;
                        $modelNotificationUser->save();
                        
                }
            
            $result['status'] = 'success';
        }else{
            $result['status'] = 'error';
        }
        
        echo json_encode($result);
    }
    
    public function actionDeletepostwithfavorite()
    {
        $result = [];
        $modelUserPost = Userpost::find()->where(['user_id' => \Yii::$app->user->id, 'post_id' => $_POST['post_id']])->one();
        if($modelUserPost->delete()){
            $result['status'] = 'success';
        }else{
            $result['status'] = 'error';
        }
        
        echo json_encode($result);
    }
    public function actionGetcategorypost()
    {
        $modelCategory = Categories::find()->where(['url_name' => $_POST['category_url']])->one();
        if($modelCategory != null){
           $modelParentCategory = Categories::find()->where(['id' => $modelCategory->parent_id])->one();
           $result = $modelParentCategory->url_name;
        }else{
            $result = null;
        }
        echo json_encode($result);
    }
    
    public function actionGetpostinformation()
    {
        $query = new Query;
        $query->select(['*','id' => '{{%posts}}.id'])
            ->from('{{%posts}}')
            ->leftJoin('{{%user}}', '{{%user}}.id = {{%posts}}.user_id')
            ->leftJoin('{{%user_info}}', '{{%user_info}}.id_user = {{%posts}}.user_id')
            ->where(['{{%posts}}.id' => $_POST['post_id']]);
        $command = $query->createCommand();
        $post_information = $command->queryOne();
        echo json_encode($post_information);
    }
    
    public function actionChangepublishstatus()
    {
        $modelPost = Posts::find()->where(['id' => $_POST['post_id']])->one();
        $modelUserPost = Userpost::find()->where(['post_id' => $_POST['post_id']])->asArray()->all();
        $userArray = [];
        foreach($modelUserPost as $postuser){
            $userArray[] = $postuser['user_id'];            
        }
        
        $result = [];
        if($modelPost != null){
            if($_POST['status'] == 'publish'){
                $modelPost->scenario = 'publish_status';
                $modelPost->publish_status = 1;
                if($modelPost->save()){
                    $result['status'] = 'save';
                }else{
                    $result['status'] = 'not_save';
                }
            }elseif($_POST['status'] == 'unpublish'){
                $modelPost->scenario = 'publish_status';
                $modelPost->publish_status = 0;
                if($modelPost->save()){
                    $result['status'] = 'save';
                }else{
                    $result['status'] = 'not_save';
                }
            }else{
                $result['status'] = 'not_save';
            }
        }else{
            $result['status'] = 'not_save';
        }
        if($result['status'] == 'save'){
                $newModelNotification = new Notification();
                $newModelNotification->scenario = 'add_notification';
                $newModelNotification->module = 'market';
                $newModelNotification->from_object_id = $_POST['post_id'];
                $newModelNotification->to_object_id = $_POST['post_id'];
                $newModelNotification->university_id = 0;
                $newModelNotification->notification_type = 'my_favorite_post_change_status_on_'.$_POST['status'];
                if($newModelNotification->save()){
                    foreach($userArray as $user_id){
                        $modelNotificationUser = new Notificationusers();
                        $modelNotificationUser->scenario = 'new_notification';
                        $modelNotificationUser->user_id = $user_id;
                        $modelNotificationUser->notification_id = $newModelNotification->id;
                        $modelNotificationUser->save();
                    }                
                }
        }
        
        echo json_encode($result);
    }
    
}
