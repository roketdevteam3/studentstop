<?php

namespace app\modules\posts\controllers;

use app\commands\Paypal;
use app\models\PaypalSetting;
use app\models\Usercredits;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Exception\PayPalConnectionException;
use Yii;
use yii\base\UserException;
use yii\helpers\Url;
use yii\web\Controller;
use yii\db\Query;
use app\models\Categories;
use app\modules\posts\models\Posts;
use app\modules\posts\models\Postsinfo;
use app\modules\posts\models\Postsrent;
use app\modules\posts\models\Postsphotos;
use app\modules\posts\models\Rents;
use app\modules\posts\models\Rentreserve;
use app\modules\posts\models\Userrents;
use app\models\Notificationusers;
use app\models\Notification;
use app\modules\users\models\User;
use app\models\Ratingrequest;
use app\modules\users\models\UserInfo;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;
 error_reporting( E_ERROR );
/**
 * Default controller for the `posts` module
 */
class RentsController extends Controller
{
    
    public function beforeAction($action) {
        if(!\Yii::$app->user->isGuest){
        
            if(Yii::$app->user->identity->getUsertUniversity()){
                $this->enableCsrfValidation = false;
                return parent::beforeAction($action);
            }else{
                return $this->redirect(Url::to("/users/choiceuniversity",true));
            }
        }else{
            return $this->redirect(Url::to("/users/login",true));
        }
    }
    
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $postCategory = Categories::find()->where(['category_type' => 1])->all();
        
        foreach ($postCategory as $category){
           $category->child_category = Categories::find()->where(['parent_id' => $category['id']])->andWhere(['not', ['category_type' => 2]])->limit(3)->all();
        }
        
        return $this->render('index',[
            'postCategory' => $postCategory,
        ]);
    }
    
    public function actionCategory($category)
    {
//        /echo $category;
        $postCategory = Categories::find()->where(['url_name' => $category])->one();
        $postCategory->child_category = Categories::find()->where(['parent_id' => $postCategory->id])->all();
        
        return $this->render('category',[
            'postCategory' => $postCategory,
        ]);
    }
    
    public function actionView($rent_id)
    {
        $modelRent = Rents::find()->where(['id' => $rent_id])->one();
        $newRatingRequest = new Ratingrequest();
        $query = new Query;
        $query->select(['*','id' => '{{%user}}.id'])
            ->from('{{%user}}')
            ->leftJoin('{{%user_info}}', '{{%user_info}}.id_user = {{%user}}.id')
            ->where(['{{%user}}.id' => $modelRent->user_id]);
        
        $command = $query->createCommand();
        $userPosted = $command->queryOne();
        
        if($_POST){
            if(isset($_POST['reserve_rent'])){
                $modelCancel = Rentreserve::find()
                        ->where('reserve_from >= "'.$_POST['reserve_from'].'" and reserve_to <= "'.$_POST['reserve_to'].'" and rent_type = "rents" and rent_id = "'.$rent_id.'"')
                        ->orWhere('reserve_from <= "'.$_POST['reserve_from'].'" and reserve_to >= "'.$_POST['reserve_to'].'" and rent_type = "rents" and rent_id = "'.$rent_id.'"')
                        ->orWhere('reserve_from >= "'.$_POST['reserve_from'].'" and reserve_to <= "'.$_POST['reserve_from'].'" and rent_type = "rents" and rent_id = "'.$rent_id.'"')
                        ->orWhere('reserve_from >= "'.$_POST['reserve_to'].'" and reserve_to <= "'.$_POST['reserve_to'].'" and rent_type = "rents" and rent_id = "'.$rent_id.'"')
                        ->all();
                if($modelCancel == null){
                    $modelNew = new Rentreserve();
                    $modelNew->scenario = 'reserve';
                    $modelNew->rent_id = $_POST['rent_id'];
                    $modelNew->user_id = \Yii::$app->user->id;
                    $modelNew->rent_type = 'rents';
                    $modelNew->reserve_from = $_POST['reserve_from'];
                    $modelNew->reserve_to = $_POST['reserve_to'];
                    if($modelNew->save()){
                        Yii::$app->session->setFlash('Reserve');
                    }else{
                        Yii::$app->session->setFlash('Notreserve');
                    }
                }else{
                    Yii::$app->session->setFlash('reserveBusy');
                }
            }
            if(isset($_POST['Rating'])){
                $modelRentreserve = Rentreserve::find()->where(['id' => $_POST['Rating']['reserve_id']])->one();
                $modelRentreserve->scenario = 'reserve_rating';
                $modelRentreserve->rating_comment = $_POST['Rating']['comment'];
                $modelRentreserve->rating_count = $_POST['Rating']['rating_count'];
                if($modelRentreserve->save()){
                    $modelRatingrequest = new Ratingrequest();
                    $modelRatingrequest->scenario = 'add_rating_request';
                    $modelRatingrequest->from_user_id = $_POST['Rating']['from_user_id'];
                    $modelRatingrequest->to_object_id = $_POST['Rating']['to_object_id'];
                    $modelRatingrequest->rating_count = $_POST['Rating']['rating_count'];
                    $modelRatingrequest->type = 'rental';
                        if($modelRatingrequest->save()){
                            return $this->redirect('/rents/view/'.$rent_id);
                        }
                }
            }
        }
        
        $modelRentreserve = Rentreserve::find()->where(['rent_type' => 'rents', 'rent_id' => $rent_id])->all();
        return $this->render('view',[
            'rent' => $modelRent,
            'newRatingRequest' => $newRatingRequest,
            'userPosted' => $userPosted,
            'modelRentreserve' => $modelRentreserve
        ]);
    }
    
    public function actionMyreservations()
    {
        $request = Rentreserve::find()->where(['user_id'=>Yii::$app->user->id,'rent_type' => 'rents','status'=>0])->andWhere(['!=','request',0])->all();

        $query = new Query;
        $query->select(['*','id' => '{{%rents}}.id'])
            ->from('{{%rent_reserve}}')
            ->leftJoin('{{%rents}}', '{{%rents}}.id = {{%rent_reserve}}.rent_id')
            ->where(['{{%rent_reserve}}.user_id' => \Yii::$app->user->id, '{{%rent_reserve}}.rent_type' => 'rents','{{%rent_reserve}}.status' => 1]);
        $reservationRent = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 30]]);
        
        return $this->render('myreservations',[
            'reservationRent' => $reservationRent->getModels(),
            'pagination' => $reservationRent->pagination,
            'count' => $reservationRent->pagination->totalCount,
            'request' => $request,
        ]);
    }
    
    public function actionCategorychild($category_child)
    {
        
        $postCategory = Categories::find()->where(['url_name' => $category_child])->one();
        
        $queryRent = new Query;
        $queryRent->select(['*','id' => '{{%rents}}.id'])  
            ->from('{{%rents}}')
            ->where(['{{%rents}}.category_id' => $postCategory->id]);
	if($_GET){
            if((isset($_GET['reserve_from'])) || (isset($_GET['reserve_to']))){
                if(($_GET['reserve_from'] != '') && ($_GET['reserve_to'] == '')){
                    $modelReservRent = Rentreserve::find()
                        ->where('reserve_from >= "'.$_GET['reserve_from'].'" and reserve_to <= "'.$_GET['reserve_from'].'" and rent_type = "rents"')
                        ->all();
                }
                if(($_GET['reserve_from'] == '') && ($_GET['reserve_to'] != '')){
                    $modelReservRent = Rentreserve::find()
                        ->orWhere('reserve_from >= "'.$_GET['reserve_to'].'" and reserve_to <= "'.$_GET['reserve_to'].'" and rent_type = "rents"')
                        ->all();                    
                }
                if(($_GET['reserve_from'] != '') && ($_GET['reserve_to'] != '')){
                    $modelReservRent = Rentreserve::find()
                        ->where('reserve_from >= "'.$_GET['reserve_from'].'" and reserve_to <= "'.$_GET['reserve_to'].'" and rent_type = "rents"')
                        ->orWhere('reserve_from <= "'.$_GET['reserve_from'].'" and reserve_to >= "'.$_GET['reserve_to'].'" and rent_type = "rents"')
                        ->orWhere('reserve_from >= "'.$_GET['reserve_from'].'" and reserve_to <= "'.$_GET['reserve_from'].'" and rent_type = "rents"')
                        ->orWhere('reserve_from >= "'.$_GET['reserve_to'].'" and reserve_to <= "'.$_GET['reserve_to'].'" and rent_type = "rents"')
                        ->all();
                }
                if(($_GET['reserve_from'] != '') || ($_GET['reserve_to'] != '')){
                    $rent_id = '';
                    foreach($modelReservRent as $reserve){
                        $rent_id = $reserve->rent_id;
                    }
                    $queryRent->andWhere(['NOT IN', '{{%rents}}.id', $rent_id]);
                }
                
//                ->where(['NOT IN', 'userid', [1001,1002,1003,1004,1005]])
            }
            if(isset($_GET['university'])){
                if($_GET['university'] != ''){
                    $queryRent->andWhere(['{{%rents}}.university_id' =>  $_GET['university']]);
                }

            }
        if(isset($_GET['price_from'])){
            if($_GET['price_from'] != ''){
                $queryRent->andWhere('{{%rents}}.price >= '.$_GET['price_from']);
            }
        }
        if(isset($_GET['price_to'])){
            if($_GET['price_to'] != ''){
                $queryRent->andWhere('{{%rents}}.price <= '.$_GET['price_to']);
            }
        }
        if(isset($_GET['rent_value'])){
            if($_GET['rent_value'] != ''){
                $queryRent->where(['LIKE', '{{%rents}}.address', $_GET['rent_value']]);
//                        $queryRent->orWhere(['LIKE', '{{%renthouse}}.content', $_GET['rent_value']]);
            }
        }
        }

        if ($_POST){
            if (isset($_POST['reserve_paypal'])){
                $rent = new Rentreserve();
                $rent->scenario='reserve';
                $rent->total_price = $_POST['hour']*$_POST['price_d'];
                $rent->rent_id = $_POST['rent_id'];
                $rent->reserve_from = $_POST['form_date_for'];
                $rent->reserve_to = $_POST['form_date_to'];
                $rent->rent_type = 'rents';
                $rent->user_id = Yii::$app->user->id;
                if($rent->save()){
                    $this->redirect('/rents/rent_pay?id='.$rent->id);
                }
            } elseif (isset($_POST['reserve_credits'])) {
                $credits = Usercredits::find()->where(['user_id'=>Yii::$app->user->id])->one();
                if ($credits) {
                    if ($credits->credit_count >= ($_POST['hour']*$_POST['price_c'])){
                        $rent = new Rentreserve();
                        $rent->scenario='reserve';
                        $rent->total_credits = $_POST['hour']*$_POST['price_d'];
                        $rent->rent_id = $_POST['rent_id'];
                        $rent->reserve_from = $_POST['form_date_for'];
                        $rent->reserve_to = $_POST['form_date_to'];
                        $rent->rent_type = 'rents';
                        $rent->user_id = Yii::$app->user->id;
                        $rent->status = 1;
                        if ($rent->save()){
                            $credits->scenario ='update_credits_count';
                            $credits->credit_count = $credits->credit_count - $rent->total_credits;
                            $credits->save();
                            $rent_d = Rents::findOne($_POST['rent_id']);
                            $addcredits = Usercredits::find()->where(['user_id'=>$rent_d->user_id])->one();
                            $addcredits->credit_count = $addcredits->credit_count + $rent->total_credits;
                            $addcredits->scenario = 'update_credits_count';
                            $addcredits->save();
                          Yii::$app->session['rent_ok']=$rent->id;
                        }
                    } else {
                        Yii::$app->session->setFlash('not_credits');
                    }
                }
            } elseif (isset($_POST['reserve_request'])) {
                $rent = new Rentreserve();
                $rent->scenario='reserve';
                $rent->total_price = $_POST['hour']*$_POST['price_d'];
                $rent->total_credits = $_POST['hour']*$_POST['price_c'];
                $rent->rent_id = $_POST['rent_id'];
                $rent->reserve_from = $_POST['form_date_for'];
                $rent->reserve_to = $_POST['form_date_to'];
                $rent->rent_type = 'rents';
                $rent->request=1;
                $rent->user_id = Yii::$app->user->id;
                if($rent->save()){

                }
            }
        }
        
        //$command = $query->createCommand();
        //var_dump($command);exit;
        //$data = $command->queryAll();	
        
        //$query = Posts::find()->where(['category_id' => $postCategory->id]);
        $postModel = new ActiveDataProvider(['query' => $queryRent, 'pagination' => ['pageSize' => 30]]);
        $hour = 0;
        if(isset($_GET['reserve_from']) and isset($_GET['reserve_to'])) {
            $reserve_to = strtotime($_GET['reserve_to']);
            $reserve_from = strtotime($_GET['reserve_from']);
            $datediff = $reserve_to - $reserve_from;
            $hour = floor($datediff / (60 * 60 ));

        }
        return $this->render('category_child',[
            'postCategory' => $postCategory,
            'postModel' => $postModel->getModels(),
            'pagination' => $postModel->pagination,
            'count' => $postModel->pagination->totalCount,
            'hour'=> $hour,
        ]);
    }

    public function actionPay_rent($id){
        if (!Yii::$app->user->isGuest) {

            $rent_ser = Rentreserve::findOne($id);
            $rent = Rents::findOne($rent_ser->rent_id);



            $dPrice = $rent_ser->total_price;

//            $paypal = PaypalSetting::find()->where(['user_id'=>$rent->user_id])->one();

            /* if ($paypal){
                 $pay = new ApiContext(
                     new OAuthTokenCredential(
                         $paypal->client_id,
                         $paypal->secret
                     )
                 );
             } else {
                 throw new HttpException(403);
             }*/

            $pay = New Paypal();
            $pay = $pay->api;

            $pay->setConfig([
                'model'=>'sandbox',
                'http.ConnectionTimeOut'=>30,
                'log.LogEnabled'=> YII_DEBUG ? 1 : 0,
                'log.FileName'=> '',
                'log.LogLevel' => 'FINE',
                'validation.level'=>'log'
            ]);

            $payer = new Payer();
            $payer->setPaymentMethod("paypal");

            $item2 = new Item();
            $item2->setName('Payment rent')
                ->setCurrency('USD')
                ->setQuantity(1)
                ->setSku('N-C-'+time()) // Similar to `item_number` in Classic API
                ->setPrice($dPrice);
            $itemList = new ItemList();
            $itemList->setItems(array($item2));


            $details = new Details();
            $details->setShipping(0.00)
                ->setTax(0.00)
                ->setSubtotal($dPrice);


            $amount = new Amount();
            $amount->setCurrency("USD")
                ->setTotal($dPrice+0.00)
                ->setDetails($details);



            $transaction = new Transaction();
            $transaction->setAmount($amount)
                ->setItemList($itemList)
                ->setDescription('Paymend rent house ')
                ->setInvoiceNumber(uniqid());

            $redirectUrls = new RedirectUrls();
            $redirectUrls->setReturnUrl(Url::home(true).'rents/pay?approved=true&user_paymend='.$rent->user_id.'&rent_id='.$id)
                ->setCancelUrl(Url::home(true).'rents/pay?approved=false&user_paymend='.$rent->user_id.'&rent_id='.$id);


            $payment = new Payment();
            $payment->setIntent("sale")
                ->setPayer($payer)
                ->setRedirectUrls($redirectUrls)
                ->setTransactions(array($transaction));

            try{
                $payment->create($pay);
            }catch (PayPalConnectionException $e){

                throw new UserException($e->getMessage());

            }
            $trans= new \app\models\Transaction();
            $trans->user_id= Yii::$app->user->id;
            $trans->price=$dPrice;
            $trans->payment_id=$payment->getId();
            $trans->hash=md5($payment->getId());
            $trans->othe='Paymend rent borrow';
            $trans->project_id=$id;
            $trans->save();
            Yii::$app->getResponse()->redirect($payment->getApprovalLink());
        } else {
            throw new HttpException(403);
        }
    }

    public function actionPay(){
        $paypal = PaypalSetting::find()->where(['user_id'=>$_GET['user_paymend']])->one();

        /* if ($paypal){
             $pay = new ApiContext(
                 new OAuthTokenCredential(
                     $paypal->client_id,
                     $paypal->secret
                 )
             );
         } else {
             throw new HttpException(403);
         }*/
        $pay = New Paypal();
        $pay = $pay->api;

        if (isset($_GET['approved']) && $_GET['approved'] == 'true') {
            $paymentId = $_GET['paymentId'];
            $payment = Payment::get($paymentId, $pay);
            $execution = new PaymentExecution();
            $execution->setPayerId($_GET['PayerID']);
            try {
                $result = $payment->execute($execution, $pay);
            } catch (PayPalConnectionException $e) {
                throw new UserException($e->getMessage());
            }
            $transaction = \app\models\Transaction::find()->where(['user_id'=>Yii::$app->user->id,'hash'=>md5($paymentId)])->one();
            $transaction->complete=1;
            $transaction->save();

            $rent = Rentreserve::findOne($_GET['rent_id']);
            $rent->scenario = 'status';
            $rent->status = 1;
            if($rent->save()){
                $modelRent = Rents::find()->where(['id' => $_GET['rent_id']])->asArray()->one();
                $newModelNotification = new Notification();
                $newModelNotification->scenario = 'add_notification';
                $newModelNotification->module = 'rents';
                $newModelNotification->from_object_id = $rent->id;
                $newModelNotification->to_object_id = $_GET['rent_id'];
                $newModelNotification->university_id = 0;
                $newModelNotification->notification_type = 'rent_my_rent';
                if($newModelNotification->save()){
                    $modelNotificationUser = new Notificationusers();
                    $modelNotificationUser->scenario = 'new_notification';
                    $modelNotificationUser->user_id = $modelRent['user_id'];
                    $modelNotificationUser->notification_id = $newModelNotification->id;
                    $modelNotificationUser->save();
                }
                
                $newModelNotification = new Notification();
                $newModelNotification->scenario = 'add_notification';
                $newModelNotification->module = 'rents';
                $newModelNotification->from_object_id = $rent->id;
                $newModelNotification->to_object_id = $_GET['rent_id'];
                $newModelNotification->university_id = 0;
                $newModelNotification->notification_type = 'pay_my_rent';
                if($newModelNotification->save()){
                    $modelNotificationUser = new Notificationusers();
                    $modelNotificationUser->scenario = 'new_notification';
                    $modelNotificationUser->user_id = $modelRent['user_id'];
                    $modelNotificationUser->notification_id = $newModelNotification->id;
                    $modelNotificationUser->save();
                }
                
                $newModelNotification = new Notification();
                $newModelNotification->scenario = 'add_notification';
                $newModelNotification->module = 'rents';
                $newModelNotification->from_object_id = $rent->id;
                $newModelNotification->to_object_id = $_GET['rent_id'];
                $newModelNotification->university_id = 0;
                $newModelNotification->notification_type = 'i_rent_rent';
                if($newModelNotification->save()){
                    $modelNotificationUser = new Notificationusers();
                    $modelNotificationUser->scenario = 'new_notification';
                    $modelNotificationUser->user_id = \Yii::$app->user->id;
                    $modelNotificationUser->notification_id = $newModelNotification->id;
                    $modelNotificationUser->save();
                }
                
            }
            

            $user_many = Usercredits::find()->where(['user_id'=>$rent->rents['user_id']])->one();
            if ($user_many) {
                $user_many->scenario = 'update_many_count';
                $user_many->many = $user_many->many + $transaction->price;
                $user_many->save();
            } else {
                $u_m = new Usercredits();
                $u_m->many=$transaction->price;
                $u_m->user_id = $rent->rents['user_id'];
                $u_m->credit_count = 0;
                $u_m->scenario = 'add_credits';
                $u_m->save();
            }



//            Yii::$app->session['rent_ok']=true;
            Yii::$app->session['rent_ok']=$rent->id;

            return $this->redirect('/rents/'.$rent->rents->category['url_name']);
        } else {
            $rent = Rentreserve::findOne($_GET['rent_id']);
            Yii::$app->session['rent_cansel']=true;
            return $this->redirect('/rents/'.$rent->rents->category['url_name']);
        }
    }
    
    public function actionMyprofile(){
        $postCategory = Categories::find()->where(['category_type' => 1])->all();
        $categoryArray = [];
        foreach ($postCategory as $category){
            $categoryArray[$category->category_name] = ArrayHelper::map(Categories::find()->where(['parent_id' => $category['id']])->andWhere(['not', ['category_type' => 2]])->all(),'id','category_name');
        }

       /* $paypalsearch = PaypalSetting::find()->where(['user_id'=>Yii::$app->user->id])->one();


        if ($paypalsearch) {
            if (isset($_POST['paypal'])) {
                $paypalsearch->load(Yii::$app->request->post());
                $paypalsearch->save();
            }

        } else {
            $paypalsearch = new PaypalSetting();
            $paypalsearch->user_id = Yii::$app->user->id;
            if (isset($_POST['paypal'])) {
                $paypalsearch->load(Yii::$app->request->post());
                $paypalsearch->save();
            }
        }*/

        $modelNewRent = new Rents();
        $modelNewRent->scenario = 'add_rent';
        if($_POST){
//            if ($paypalsearch->validate()) {
                $modelUserInfo = UserInfo::find()->where(['id_user' => \Yii::$app->user->id])->one();
                $request = Yii::$app->request;
                if ($modelNewRent->load($request->post())) {
                    $modelNewRent->university_id = $modelUserInfo->university_id;
                    if ($modelNewRent->save()) {
                        if (isset($_POST['photosInput'])) {
                            foreach (json_decode($_POST['photosInput']) as $image_name) {
                                $modelNewPostsphotos = new Postsphotos();
                                $modelNewPostsphotos->scenario = 'add_photo';
                                $modelNewPostsphotos->post_id = $modelNewRent->id;
                                $modelNewPostsphotos->image_src = $image_name;
                                $modelNewPostsphotos->save();
                            }
                        }
                        Yii::$app->session->setFlash('Rentadded');
                   /* } else {
                        Yii::$app->session->setFlash('Rentnotadded');
                    }*/
                }
            }
        }

        if (Yii::$app->request->get('id')&&Yii::$app->request->get('request') ){
            $request_up=Rentreserve::find()->where(['id'=>Yii::$app->request->get('id'),'request'=>1])->one();
            if ($request_up){
                $request_up->request = Yii::$app->request->get('request');
                $request_up->view = 1;
                $request_up->scenario ='request';
                $request_up->save();
            }
        }
      $queryMyRent = Rents::find()->where(['user_id' => \Yii::$app->user->id]);
      $myRent = new ActiveDataProvider(['query' => $queryMyRent, 'pagination' => ['pageSize' => 30]]);

      $mas_rent = [];

      foreach ($queryMyRent->all() as $item){
        $mas_rent[]=$item->id;
      }

        $request = Rentreserve::find()->where(['rent_id'=>$mas_rent,'rent_type' => 'rents', 'request'=>1 ,'status'=>0])->all();



        
        return $this->render('myprofile',[
            'modelNewRent' => $modelNewRent,
            'categoryArray' => $categoryArray,
            'myRent' => $myRent->getModels(),
            'pagination' => $myRent->pagination,
            'count' => $myRent->pagination->totalCount,
            'request' => $request,
//            'paypal' => $paypalsearch,
        ]);
    }
    
    public function actionFavorite(){
        $query = new Query;
        $query->select(['*','id' => '{{%rents}}.id','date_create' => '{{%user_rents}}.date_create'])  
            ->from('{{%user_rents}}')
            ->leftJoin('{{%rents}}', '{{%rents}}.id = {{%user_rents}}.rent_id')
            ->where(['{{%user_rents}}.user_id' => \Yii::$app->user->id]);
        $favoriteRent = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 30]]);
        
        return $this->render('favorite',[
            'favoriteRent' => $favoriteRent->getModels(),
            'pagination' => $favoriteRent->pagination,
            'count' => $favoriteRent->pagination->totalCount,
        ]);  
    }
    
    public function actionPostview($category_child,$post_id)
    {
        $postCategory = Categories::find()->where(['url_name' => $category_child])->one();
        
//        $query = new Query;
//        $query->select(['*','id' => '{{%posts}}.id','date_create' => '{{%posts}}.date_create' ])  
//            ->from('{{%posts}}')
//            ->leftJoin('{{%posts_info}}', '{{%posts_info}}.post_id = {{%posts}}.id')
//            ->where(['{{%posts}}.id' => $post_id]);
//        	
//        $command = $query->createCommand();
//        $postViewModel = $command->queryOne();
        
        $postViewModel = Posts::find()->where(['id' => $post_id])->one();
        $postViewModel->scenario = 'update_posts';
        
        $postinfoViewModel = Postsinfo::find()->where(['post_id' => $postViewModel['id']])->one();
            if($postCategory->category_type != 2){
                $postinfoViewModel->scenario = 'update_rental';
            }else{
                $postinfoViewModel->scenario = 'update_rent_a_house';
            }
            
        $newPostRent = new Postsrent();
        $newPostRent->scenario = 'add_rent';
        if($_POST){
            $request = Yii::$app->request;
            if(isset($_POST['Posts'])){
                $postViewModel->date_update = date("Y-m-d H:i:s");
                if($postViewModel->load($request->post()) && ($postViewModel->save())){
                    if($postinfoViewModel->load($request->post()) && ($postinfoViewModel->save())){
                        if($postinfoViewModel->save()){
                            Yii::$app->session->setFlash('PostsUpdate');
                        }
                    }
                }
            }
            if(isset($_POST['Postsrent'])){
                if($newPostRent->load($request->post()) && ($newPostRent->save())){
                    Yii::$app->session->setFlash('RentAdded');
                }
            }
        }
        
        $postPhotos = Postsphotos::find()->where(['post_id' => $postViewModel['id']])->all();
        
        $query = new Query;
        $query	->select(['*','id'=>'{{%user}}.id'])
                        ->from('{{%user}}')
                        ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%user}}.id'
                                )->where(['{{%user}}.id' => $postViewModel['user_id']]);
        
        $command = $query->createCommand();
        $userPosted = $command->queryOne();
        //var_dump($data);exit;
        return $this->render('post_view',[
            'postCategory' => $postCategory,
            'postViewModel' => $postViewModel,
            'postinfoViewModel' => $postinfoViewModel,
            'postPhotos' => $postPhotos,
            'userPosted' => $userPosted,
            'newPostRent' => $newPostRent,
        ]);
    }
    
    public function actionPostdelete($category_child,$post_id){
        $postModel = Posts::find()->where(['id' => $post_id])->one();
        $postinfoModel = Postsinfo::find()->where(['post_id' => $post_id])->one();
        $postModel->delete();
        $postinfoModel->delete();
        //delete photo
        Yii::$app->session->setFlash('PostsDelete');
        return $this->redirect('/rents/'.$category_child);
    }
    
    public function actionRentrequest(){
        $rent_id = $_POST['rent_id'];
        $modelPostrent = Postsrent::find()->where(['id' => $rent_id])->one();
        $modelPostrent->scenario = 'status';
        $modelPostrent->status = '1';
        $result = [];
        if($modelPostrent->save()){
            $result['status'] = 'success';
            $result['rent_id'] = $_POST['rent_id'];
        }else{
            $result['status'] = 'error';
            $result['rent_id'] = $_POST['rent_id'];
        }
        
        echo json_encode($result);
    }
    
    public function actionAddrenttofavorite()
    {
        $result = [];
        $modalUserJobs = new Userrents();
        $modalUserJobs->scenario = 'add_favorite_rent';
        $modalUserJobs->user_id = \Yii::$app->user->id;
        $modalUserJobs->rent_id = $_POST['rent_id'];
        if($modalUserJobs->save()){
            $result['status'] = 'success';
            
            $modelRent = Rents::find()->where(['id' => $_POST['rent_id']])->asArray()->one();
            $newModelNotification = new Notification();
            $newModelNotification->scenario = 'add_notification';
            $newModelNotification->module = 'rents';
            $newModelNotification->from_object_id = \Yii::$app->user->id;
            $newModelNotification->to_object_id = $_POST['rent_id'];
            $newModelNotification->university_id = 0;
            $newModelNotification->notification_type = 'add_my_rents_to_favorite';
            if($newModelNotification->save()){
                $modelNotificationUser = new Notificationusers();
                $modelNotificationUser->scenario = 'new_notification';
                $modelNotificationUser->user_id = $modelRent['user_id'];
                $modelNotificationUser->notification_id = $newModelNotification->id;
                $modelNotificationUser->save();
            }
            
        }else{
            $result['status'] = 'error';
        }
        
        echo json_encode($result);
    }
    
    public function actionDeleterentwithfavorite()
    {
        $result = [];
        $modalUserJobs = Userrents::find()->where(['user_id' => \Yii::$app->user->id, 'rent_id' => $_POST['rent_id']])->one();
        if($modalUserJobs->delete()){
            $result['status'] = 'success';
        }else{
            $result['status'] = 'error';
        }
        
        echo json_encode($result);
    }
    
    public function actionGetcategoryurl()
    {
        $modelCategory = Categories::find()->where(['id' => $_POST['category_id']])->one();
        echo json_encode($modelCategory->url_name);
    }

    public function actionAddviews($id){
      $rent = Rents::findOne($id);
      if ($rent){
        $rent->scenario ='view';
        $rent->views +=1;
        $rent->save();
      }
    }

    public function actionMetod_pay($metod,$id){
        $rent_r = Rentreserve::findOne($id);
        if ($rent_r){
            var_dump($metod);
            if ($metod == '1') {
                $this->redirect('/rents/rent_pay?id='.$rent_r->id);
            }
            if ($metod == '2') {
                $credits = Usercredits::find()->where(['user_id'=>Yii::$app->user->id])->one();
                if ($credits) {
                    if ($credits->credit_count >= $rent_r->total_credits){
                        $rent_r->scenario='status';
                        $rent_r->status = 1;
                        if ($rent_r->save()){
                            $credits->scenario ='update_credits_count';
                            $credits->credit_count = $credits->credit_count - $rent_r->total_credits;
                            $credits->save();
                            $rent_rech = Rents::findOne($rent_r->rent_id);
                            $addcredits = Usercredits::find()->where(['user_id'=>$rent_rech->user_id])->one();
                            $addcredits->credit_count = $addcredits->credit_count + $rent_r->total_credits;
                            $addcredits->scenario = 'update_credits_count';
                            $addcredits->save();
                            Yii::$app->session['rent_ok']=$rent_r->id;
                        }
                    } else {
                        Yii::$app->session->setFlash('not_credits');
                    }
                }
            }

        } else {
            $this->redirect('/rents/myreservations');
        }
//        var_dump($metod);
//        $this->redirect('/rents/myreservations');

    }

  public function actionDelete_rent($id){
    $rent = Rents::findOne($id)->delete();
    return true;
  }

}
