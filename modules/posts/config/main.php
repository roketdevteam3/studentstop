<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 28.07.15
 * Time: 10:02
 */

$module_name = basename(dirname(dirname(__FILE__)));

return [
    'modules' => [
        'posts' => [
            'class' => 'app\modules\posts\Module',
        ],
    ],

    'components' => [
        'urlManager' => [
            'rules' => [
                'getcategorypost' => 'posts/posts/getcategorypost',
                'market/delete/<post_id:\w+>' => '/posts/posts/delete',
                'market/view/<post_id:\w+>' => '/posts/posts/view',
                'market/favorite' => 'posts/posts/favorite',
                'market/addviews' => 'posts/posts/addviews',
                'market/view_zero' => 'posts/posts/view_zero',
                'market/<category_url:\w+>/<category_child:\w+>/<post_id:\d+>' => '/posts/posts/postview',
                'market/<category_url:\w+>/<category_child:\w+>' => '/posts/posts/categorychild',
                'market/myprofile' => 'posts/posts/myprofile',
                'market/<category_url:\w+>' => '/posts/posts/category',
                'posts/posts/savepostsphoto' => '/posts/posts/savepostsphoto',
                'posts/posts/deletepostsphoto' => '/posts/posts/deletepostsphoto',
                'market/' => 'posts/posts/index',
                
                'addposttofavorite' => 'posts/posts/addposttofavorite',
                'deletepostwithfavorite' => 'posts/posts/deletepostwithfavorite',
                
                'rents/' => '/posts/rents/index',
                'rents/addviews' => '/posts/rents/addviews',
                'rents/delete_rent' => '/posts/rents/delete_rent',
                'rents/favorite' => '/posts/rents/favorite',
                'rents/myreservations' => 'posts/rents/myreservations',
                'rents/rent_pay' => '/posts/rents/pay_rent',
                'rents/pay' => '/posts/rents/pay',
                'rents/view/<rent_id:\w+>' => '/posts/rents/view',
                'addrenttofavorite' => '/posts/rents/addrenttofavorite',
                'deleterentwithfavorite' => '/posts/rents/deleterentwithfavorite',
                'rentshouse/myreservations' => 'posts/rentshouse/myreservations',
                'rentshouse/myprofile' => 'posts/rentshouse/myprofile',
                'rentshouse' => '/posts/rentshouse/index',
                'getcategoryurl' => '/posts/rents/getcategoryurl',
                'rentshouse/view/<rent_id:\d+>' => '/posts/rentshouse/view',
                'rentshouse/favorite' => '/posts/rentshouse/favorite',
                'rentshouse/metod_pay' => '/posts/rentshouse/metod_pay',
                'rentshouse/delete_renthouse' => '/posts/rentshouse/delete_renthouse',
                'rents/metod_pay' => '/posts/rents/metod_pay',
                'addrenthousetofavorite' => '/posts/rentshouse/addrenthousetofavorite',
                'deleterenthousewithfavorite' => '/posts/rentshouse/deleterenthousewithfavorite',
                'addviews/<id:\d+>' => '/posts/rentshouse/add_views',
                'rent_pay' => '/posts/rentshouse/pay_rent',
                'pay' => '/posts/rentshouse/pay',
                '/rents/rentrequest/' => '/posts/rents/rentrequest',
                'rents/myprofile' => 'posts/rents/myprofile',
                'rents/category/<category:\w+>' => '/posts/rents/category',
                '/rents/<category_child:\w+>' => '/posts/rents/categorychild',
                '/rents/<category_child:\w+>/<post_id:\d+>' => '/posts/rents/postview',
                '/rents/<category_child:\w+>/delete/<post_id:\d+>' => '/posts/rents/postdelete',
                
                '/posts/<category_url:\w+>/<category_child:\w+>/delete/<post_id:\d+>' => '/posts/posts/postdelete',
                //'/posts/' => '/posts/posts/index',
                $module_name . '/<action:\w+>/<id:\d+>' => $module_name . '/default/<action>',
                $module_name . '/<action:\w+>' => $module_name . '/default/<action>',
            ],
        ],
    ],
];