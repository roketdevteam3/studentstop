<?php

namespace app\modules\posts\models;

use Yii;

class Userrenthouse extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return '{{%user_renthouse}}';
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    public function scenarios()
    {
        return [
            'add_favorite_renthouse' => ['renthouse_id', 'user_id'],
	];
    }
    
    public function rules()
    {
        return [
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id'),
        ];
    }
    
}