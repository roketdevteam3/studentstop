<?php

namespace app\modules\posts\models;

use Yii;

/**
 * This is the model class for table "{{%user_role}}".
 *
 * @property integer $id
 * @property integer $name
 */
class Postsinfo extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%posts_info}}';
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              $this->status = 0;
        }

        return parent::beforeSave($insert);
    } 
    
    public function scenarios()
    {
        return [
            'add_posts' => ['post_id', 'address', 'zip_code', 'country'],
            'update_posts' => ['address', 'zip_code', 'country'],
            'add_rental' => ['post_id', 'price', 'time_rent','status', 'address', 'zip_code', 'country'],
            'add_rent_a_house' => ['post_id', 'price', 'time_rent','status', 'address', 'zip_code', 'country', 'building_floor', 'house_storeys', 'room_count', 'total_area'],
            'update_rental' => ['price', 'time_rent', 'status', 'address', 'zip_code', 'country'],
            'update_rent_a_house' => ['price', 'time_rent','status', 'address', 'zip_code', 'country', 'building_floor', 'house_storeys', 'room_count', 'total_area'],
        ];
    }
    
    public function rules()
    {
        return [
            ['price', 'required'],
            ['time_rent', 'required'],
            ['zip_code', 'required'],
            ['country', 'required'],
        ];
    }

}
