<?php

namespace app\modules\posts\models;

use app\models\user\UserBonus;
use app\modules\users\models\User;
use Yii;

/**
 * This is the model class for table "{{%user_role}}".
 *
 * @property integer $id
 * @property integer $name
 */
class Posts extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%posts}}';
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              $this->date_create = date("Y-m-d H:i:s");
              $this->publish_status = 1;
        }

        return parent::beforeSave($insert);
    }
    
    public function scenarios()
    {
        return [
            'publish_status' => ['publish_status'],
            'add_posts' => ['title', 'content', 'user_id', 'category_id', 'image_src', 'address','university_id'],
            'update_posts' => ['title', 'content', 'user_id', 'category_id', 'image_src', 'address','university_id'],
            'views'=>['views'],
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function rules()
    {
        return [
            ['title', 'required'],
            [['title'], 'string', 'min' => 2],
            ['content', 'required'],
        ];
    }

    public function afterSave($insert, $changedAttributes) {
        if ($this->isNewRecord) {
            $post_c = Posts::find()->where(['user_id'=>Yii::$app->user->id])->count();
            if ($post_c == 1){
                $bonus = new UserBonus();
                $bonus->user_id = Yii::$app->user->id;
                $bonus->bonus = 5;
                $bonus->type = '1_posts_market';
                $bonus->save();
            }elseif (is_int($post_c/25)){
                $bonus = new UserBonus();
                $bonus->user_id = Yii::$app->user->id;
                $bonus->bonus = 25 * ($post_c/25);
                $bonus->type = '25_posts_market';
                $bonus->save();
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }


}
