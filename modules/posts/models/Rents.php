<?php

namespace app\modules\posts\models;

use app\models\Categories;
use app\models\user\UserBonus;
use Yii;

/**
 * This is the model class for table "{{%user_role}}".
 *
 * @property integer $id
 * @property integer $name
 */
class Rents extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%rents}}';
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              //$this->status = 0;
              $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    } 
    
    public function scenarios()
    {
        return [
            'add_rent' => ['title', 'content', 'user_id','price', 'price_for',
            'position_lat', 'position_lng','img_src', 'country', 'city','category_id','credits','instant_pay'],
//            'status' => ['status'],
            'view' => ['views'],
        ];
    }

    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

    public function rules()
    {
        return [
            ['title', 'required'],
            [['title'], 'string', 'min' => 6],
            ['content', 'required'],
//            ['price', 'required'],
        ];
    }

    public function afterSave($insert, $changedAttributes) {
        if ($this->isNewRecord) {
            $count = Rents::find()->where(['user_id'=>Yii::$app->user->id])->count();
            if ($count == 1){
                $bonus = new UserBonus();
                $bonus->user_id = Yii::$app->user->id;
                $bonus->bonus = 5;
                $bonus->type = '1_post_rents';
                $bonus->save();
            }elseif (is_int($count/5)){
                $bonus = new UserBonus();
                $bonus->user_id = Yii::$app->user->id;
                $bonus->bonus = 5 * ($count/5);
                $bonus->type = '5_post_rents';
                $bonus->save();
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }

}
