<?php

namespace app\modules\posts\models;

use app\models\PaypalSetting;
use app\models\user\UserBonus;
use Yii;

/**
 * This is the model class for table "{{%user_role}}".
 *
 * @property integer $id
 * @property integer $name
 */
class Rentshouse extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%renthouse}}';
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              //$this->status = 0;
              $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    } 
    
    public function scenarios()
    {
        return [
            'add_rent' => ['title', 'content', 'address', 'user_id','room_count', 'price', 'price_for', 'wifi', 'parking', 'tv', 'smoking', 'animal','conditioner','adults', 'position_lat', 'position_lng','img_src','credits','house_type','instant_pay'],
//            'status' => ['status'],
            'view' => ['views'],
        ];
    }
    
    public function rules()
    {
        return [
            ['title', 'required'],
            [['title'], 'string', 'min' => 6],
//            ['room_count', 'required'],
            ['content', 'required'],
//            ['price', 'required'],
            ['price', 'number'],
            ['credits', 'number'],
        ];
    }

    public function getPaypal()
    {
        return $this->hasOne(PaypalSetting::className(), ['user_id' => 'user_id']);
    }

    public function afterSave($insert, $changedAttributes) {
        if ($this->isNewRecord) {
            $count = Rentshouse::find()->where(['user_id'=>Yii::$app->user->id])->count();
            if ($count == 1){
                $bonus = new UserBonus();
                $bonus->user_id = Yii::$app->user->id;
                $bonus->bonus = 5;
                $bonus->type = '1_post_rentshouse';
                $bonus->save();
            }elseif (is_int($count/5)){
                $bonus = new UserBonus();
                $bonus->user_id = Yii::$app->user->id;
                $bonus->bonus = 5 * ($count/5);
                $bonus->type = '5_post_rentshouse';
                $bonus->save();
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }

}
