<?php

namespace app\modules\posts\models;

use app\models\user\UserBonus;
use app\modules\users\models\User;
use Yii;

class Rentreserve extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return '{{%rent_reserve}}';
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
              $this->date_reserve = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }
    
    public function scenarios()
    {
        return [
            'reserve' => ['rent_id', 'user_id', 'rent_type', 'reserve_from' , 'reserve_to','status', 'total_price', 'total_credits','request'],
            'reserve_rating' => ['rating_comment', 'rating_count'],
            'status'=>['status'],
            'request'=>['request','view'],
	];
    }
    
    public function rules()
    {
        return [
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id'),
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getRenthouse()
    {
        return $this->hasOne(Rentshouse::className(), ['id' => 'rent_id']);
    }
    public function getRents()
    {
        return $this->hasOne(Rents::className(), ['id' => 'rent_id']);
    }


    public function afterSave($insert, $changedAttributes) {
        if ($this->status == 1) {
            if ($this->rent_type == 'rents') {
                $count_r = Rentreserve::find()
                    ->where([
                        'user_id' => Yii::$app->user->id,
                        'status' => 1,
                        'rent_type' => 'rents',
                    ])->count();
                if ($count_r == 1) {
                    $bonus = new UserBonus();
                    $bonus->user_id = Yii::$app->user->id;
                    $bonus->bonus = 5;
                    $bonus->type = '1_rents';
                    $bonus->save();
                } elseif (is_int($count_r / 10)) {
                    $bonus = new UserBonus();
                    $bonus->user_id = Yii::$app->user->id;
                    $bonus->bonus = 10 * ($count_r / 10);
                    $bonus->type = '10_rents';
                    $bonus->save();
                }
            }

            if ($this->rent_type == 'rent_house') {
                $count_h = Rentreserve::find()
                    ->where([
                        'user_id' => Yii::$app->user->id,
                        'status' => 1,
                        'rent_type' => 'rent_house',
                    ])->count();
                if ($count_h == 1) {
                    $bonus = new UserBonus();
                    $bonus->user_id = Yii::$app->user->id;
                    $bonus->bonus = 5;
                    $bonus->type = '1_rentshouse';
                    $bonus->save();
                } elseif (is_int($count_h / 10)) {
                    $bonus = new UserBonus();
                    $bonus->user_id = Yii::$app->user->id;
                    $bonus->bonus = 10 * ($count_h / 10);
                    $bonus->type = '10_rentshouse';
                    $bonus->save();
                }
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }
    
}