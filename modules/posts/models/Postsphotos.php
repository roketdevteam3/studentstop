<?php

namespace app\modules\posts\models;

use Yii;

/**
 * This is the model class for table "{{%user_role}}".
 *
 * @property integer $id
 * @property integer $name
 */
class Postsphotos extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%posts_photo}}';
    }
    
    public function scenarios()
    {
        return [
            'add_photo' => ['post_id', 'image_src'],
        ];
    }
    
    public function rules()
    {
        return [
        ];
    }

}
