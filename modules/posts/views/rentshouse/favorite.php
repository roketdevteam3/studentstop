<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use app\widgets\FavoriterenthouseWidget;

use app\assets\RentshousefavotiteAsset;
RentshousefavotiteAsset::register($this);
?>

	<div class="row">
		<?= $this->render('right_menu'); ?>
		<div class="rent-content">
			<div class="container-fluid background-block">
				<div class="row">
					<?php $form = ActiveForm::begin([
						'method' => 'get',
						'action' => Url::home().'rentshouse',
						'options' => ['class' => 'top-search-form']
					]); ?>
					<input type="text" name="rent_value" class="form-control searchInput" placeholder="search rents a house by title or keywords">
					<?php $form = ActiveForm::end(); ?>
				</div>
				<div class="rent-page">
					<ul class="nav nav-tabs view-tabs">
						<span class="choice">View</span>
						<li>
							<a data-toggle="tab" class="grid-appearance" href="#grid-content"></a>
						</li>
						<li class="active">
							<a data-toggle="tab" class="list-appearance" href="#list-content"></a>
						</li>
					</ul>

					<div class="tab-content rent-tab-content">
						<div id="list-content" class="tab-pane fade in active">
							<div class="rent-thing-wrap">
								<?php $arrayYesNo = [0 => 'No', 1 => 'Yes']; ?>
								<?php $price_for_array = [1 => 'day', 2 => 'month', 3 => 'year'];?>
								<?php foreach($favoriteRent as $rent){ $day = 0;?>
									<div class="rent-thing rentBlockInfo" data-rent_id="<?= $rent['id']; ?>" data-position_lat="<?= $rent['position_lat']; ?>" data-position_lng="<?= $rent['position_lng']; ?>">
										<div class="rent-thing-image">
											<?php if($rent['img_src'] == ''){ ?>
												<img class="img-responsive" src="<?= Url::home().'images/bg.jpg'; ?>">
											<?php }else{ ?>
												<img class="img-responsive" src="<?= Url::home().'images/posts_images/'.$rent['img_src']; ?>">
											<?php } ?>
										</div>

										<div class="" style="padding-top:10px">
											<div class="info">
												<h4 class="post-name"><?=$rent['title']?></h4>
											</div>
											<a class="btn btn-view" id="view" data-renthouse_id="<?=$rent['id']?>" >Book</a>

											<div class="total-rating ">
												Very good 8.4

											</div>
											<div class="" style="float: right">
												<div class="info star_style" style="position: relative; bottom: 4px;">
													<?= \app\widgets\rating\RatingrentalWidget::widget(['rent_id' => $rent['id'], 'type' => 'rent_house']); ?>

												</div>
											</div>
											<div class="views-count ">
												<?=$rent['views']?> Views
											</div>
										</div>


										<div class="centerrer-block">
											<div class="info">
												<div class="clearfix"></div>
												<p class="content-thing"><?= mb_substr($rent['content'], 0, 106, "UTF8").'...'; ?></p>
											</div>
											<div class="bottom-row style-bottom-row-cond">
												<div class="conditions">
													<span>
														<?php
														if($rent['wifi'] == '0'){
															?>
															<img class="img-responsive" src="<?= Url::home().'images/icon/wi-fi-ic2.png'; ?>">
															<?php
														} else {
															?>
															<img class="img-responsive" src="<?= Url::home().'images/icon/wi-fi-ic.png'; ?>">
															<?php
														}
														?>
													</span>
													<span>
														<?php
														if($rent['parking'] == '0'){
															?>
															<img class="img-responsive" src="<?= Url::home().'images/icon/parking2.png'; ?>">
															<?php
														} else {
															?>
															<img class="img-responsive" src="<?= Url::home().'images/icon/parking.png'; ?>">
															<?php
														}
														?>
													</span>
													<span>
														<?php
														if($rent['smoking'] == '0'){
															?>
															<img class="img-responsive" src="<?= Url::home().'images/icon/smoking2.png'; ?>">
															<?php
														} else {
															?>
															<img class="img-responsive" src="<?= Url::home().'images/icon/smoking.png'; ?>">
															<?php
														}
														?>
													</span>
													<span>
														<?php
														if($rent['animal'] == '0'){
															?>
															<img class="img-responsive" src="<?= Url::home().'images/icon/animal2.png'; ?>">
															<?php
														} else {
															?>
															<img class="img-responsive" src="<?= Url::home().'images/icon/animal.png'; ?>">
															<?php
														}
														?>
													</span>
													<span>
														<?php
														if($rent['conditioner'] == '0'){
															?>
															<img class="img-responsive" src="<?= Url::home().'images/icon/cond2.png'; ?>">
															<?php
														} else {
															?>
															<img class="img-responsive" src="<?= Url::home().'images/icon/cond.png'; ?>">
															<?php
														}
														?>
													</span>
												</div>
                                                                                            <h4 class="address style-addres-text"><?= $rent['address']; ?></h4>
											</div>
										</div>

										<div class="bottom-row">
											<div class="conditions mustyle_class">
                                                <div style="position: relative; top: 135px; padding-right: 22px;">
                                                    <p class="booking col-xs-6">Booked <?=\app\modules\posts\models\Rentreserve::find()->where(['rent_id'=>$rent['id'],'rent_type'=>'rent_house'])->count()?></p>
                                                    <?= FavoriterenthouseWidget::widget(['renthouse_id' => $rent['id']]) ?>
                                                </div>
												<div class="price-rent">
													<span class="price"><?= $rent['price']; ?>$</span>
												</div>

											</div>
										</div>

									</div>
									<?php // var_dump($rent); ?>

								<?php } ?>
							</div>
						</div>
						<div id="grid-content" class="tab-pane fade">
							<div class="gallery-wrap">
								<div class="row">
                                  <?php foreach($favoriteRent as $rent){ $day = 0;?>
									    <div class="col-md-4">
										<div class="grid-item">
											<div class="gallery-mask">
												<div class="top-info">
													<div class="house-name"><?=$rent['title']?></div>
                                                  <?= \app\widgets\rating\RatingrentalWidget::widget(['rent_id' => $rent['id'], 'type' => 'rent_house']); ?>
													<div class="clearfix"></div>
												</div>
												<div class="house-address"><?=$rent['address']?></div>
												<div class="info-text">
                                                  <?= mb_substr($rent['content'], 0, 106, "UTF8").'...'; ?>
                                                </div>
                                              <?= FavoriterenthouseWidget::widget(['renthouse_id' => $rent['id']]) ?>
                                                <a class="btn btn-view" id="view" data-renthouse_id="<?=$rent['id']?>" >Book</a>
											</div>
											<div class="gallery-img">
                                              <?php if($rent['img_src'] == ''){ ?>
                                                  <img class="img-responsive" src="<?= Url::home().'images/bg.jpg'; ?>">
                                              <?php }else{ ?>
                                                  <img class="img-responsive" src="<?= Url::home().'images/posts_images/'.$rent['img_src']; ?>">
                                              <?php } ?>
											</div>
										</div>
									</div>
                                    <?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
              <?php foreach($favoriteRent as $rent){ $day = 0;?>
                  <div class="modalRightPagesLS" id="view_rentshouse_<?=$rent['id']?>">
                    <?php
                    $query = new \yii\db\Query();
                    $query->select(['*','id' => '{{%user}}.id'])
                      ->from('{{%user}}')
                      ->leftJoin('{{%user_info}}', '{{%user_info}}.id_user = {{%user}}.id')
                      ->where(['{{%user}}.id' => $rent['user_id']]);

                    $command = $query->createCommand();
                    $userPosted = $command->queryOne();
                    ?>
                      <a class="btn btn-close pull-right" onclick="$('#view_rentshouse_<?=$rent['id']?>').hide()">
                          <span class="" aria-hidden="true">x</span>
                      </a>
                      <div class="user-info">
                          <div class="class-user-av">
                            <?php if ($userPosted['avatar']){?>
                                <img class="img-responsive" src="<?= Url::home(); ?>images/users_images/<?= $userPosted['avatar']; ?>">
                            <?php } else {?>
                                <img class="img-responsive" src="<?= Url::home(); ?>images/default_avatar.jpg">
                            <?php } ?>
                          </div>
                          <span class="user-name">
												<?= $userPosted['name']; ?> <?= $userPosted['surname']; ?>
											</span>
                          <div class="right-block">
                            <?= \app\widgets\rating\RatingrentaluserWidget::widget(['object_id' => $userPosted['id'], 'type' => 'rental_a_house']); ?>
                          </div>
                          <div class="clearfix"></div>
                      </div>
                      <div class="panel-content">

                          <!-- Nav tabs -->
                          <ul class="nav nav-tabs" role="tablist">
                              <li role="presentation" class="active"><a href="#description<?=$rent['id']?>" aria-controls="description<?=$rent['id']?>" role="tab" data-toggle="tab">Description</a></li>
                              <li role="presentation"><a href="#map<?=$rent['id']?>" aria-controls="map<?=$rent['id']?>" role="tab" data-toggle="tab">Map</a></li>
                              <li role="presentation"><a href="#images<?=$rent['id']?>" aria-controls="images<?=$rent['id']?>" role="tab" data-toggle="tab">Images</a></li>
                              <li role="presentation"><a href="#calendar<?=$rent['id']?>" aria-controls="calendar<?=$rent['id']?>" role="tab" data-toggle="tab" onmousemove="$('#calendar_w_<?=$rent['id']?>').fullCalendar('today');">Calendar</a></li>
                              <li role="presentation"><a href="#comments<?=$rent['id']?>" aria-controls="comments<?=$rent['id']?>" role="tab" data-toggle="tab">Reviews</a></li>
                              <li role="presentation"><?php if(Yii::$app->request->get('reserve_from') and Yii::$app->request->get('reserve_to')) {?>
                                      <!--<button type="button" class="btn" id="book" data-renthouse_id="--><?//=$rent['id']?><!--" data-renthouse_price_d="--><?//=$rent['price']?><!--" data-renthouse_price_c="--><?//=$rent['credits']?><!--"> Book </button>-->
                                      <a href="#book<?=$rent['id']?>" aria-controls="book<?=$rent['id']?>" role="tab" data-toggle="tab">Book</a>
                                <?php }?></li>
                          </ul>
                          <!-- Tab panes -->
                          <div class="tab-content">
                              <div role="tabpanel" class="tab-pane active" id="description<?=$rent['id']?>">
                                  <div class="info">
                                      <h4 class="address"><?= $rent['address']; ?></h4>
                                      <p class="content-thing"><?=$rent['content'] ?></p>
                                  </div>
                              </div>
                              <div role="tabpanel" class="tab-pane" id="map<?=$rent['id']?>">3...</div>
                              <div role="tabpanel" class="tab-pane" id="images<?=$rent['id']?>">
                                <?php
                                $images = \app\modules\posts\models\Postsphotos::find()->where(['post_id'=>$rent['id']])->all();
                                foreach($images as $image){
                                  echo '<img src="/images/posts_images/'.$image->image_src.'" />';
                                }
                                ?>
                              </div>
                              <div role="tabpanel" class="tab-pane" id="calendar<?=$rent['id']?>">
                                  <div id="calen_id_<?=$rent['id']?>">
                                    <?php
                                    $events = [];
                                    $renteds = \app\modules\posts\models\Rentreserve::find()->where(['rent_type'=>'rent_house', 'rent_id'=>$rent['id']])->all();
                                    foreach($renteds as $rented ){
                                      $event        = new \yii2fullcalendar\models\Event();
                                      $event->id    = $rented->id;
                                      $event->title = 'Rented';
                                      $event->start = date('Y-m-d\TH:i:s\Z', strtotime($rented->reserve_from));
                                      $event->end = date('Y-m-d\TH:i:s\Z', strtotime($rented->reserve_to));
                                      $events[] = $event;
                                    }
                                    /*echo \yii2fullcalendar\yii2fullcalendar::widget(array(
                                     'clientOptions' => [
//																'eventLimit'=> true,
//																'fixedWeekCount'=>false,
//															'defaultDate' => date('Y-m-d\TH:i:s\Z'),
                                         'nowIndicator'=>false,
                                             'defaultDate' => date('Y-m-d',time())
                                     ],
                                     'events'=> $events,
                             ));*/
                                    echo \yii2fullcalendar\yii2fullcalendar::widget([
                                      'id' => 'calendar_w_'.$rent['id'],
                                      'clientOptions' => [
                                        'getDate' => date('Y-m-d',time()),
                                        'defaultDate' => date('Y-m-d',time()),
                                        'today'=>true,
                                        //															'dayClick'=>new \yii\web\JsExpression('function (cellInfo, jsEvent) {console.log(cellInfo);}')
                                      ],
                                      'events'=> $events,
                                    ]);

                                    ?>
                                  </div>
                              </div>
                              <div role="tabpanel" class="tab-pane style-tb-pane-hr" id="comments<?=$rent['id']?>">
                                <?php
                                foreach($renteds as $rented){
                                  if ($rented->rating_comment)
                                    echo $rented->user['name']."<br/>".$rented->rating_comment."<br/><br/><hr><br/>";
                                }
                                ?>
                              </div>
                              <div role="tabpanel" class="tab-pane" id="book<?=$rent['id']?>">
                                <?php $form = ActiveForm::begin(); ?>
                                  <input type="hidden" name="rent_id" id="form_rent_id" value="<?=$rent['id']?>">
                                  <input type="hidden" name="price_d" id="price_d" value="<?=$rent['price']?>">
                                  <input type="hidden" name="price_c" id="price_c" value="<?=$rent['credits']?>">
                                  <input type="hidden" name="day" id="day" value="<?=$day?>">
                                  <input type="hidden" name="form_date_for" id="form_date_for" value="<?=Yii::$app->request->get('reserve_from')?>">
                                  <input type="hidden" name="form_date_to" id="form_date_to" value="<?=Yii::$app->request->get('reserve_to')?>">
                                  <!--<label class="control-label">From</label>
							<?php
                                  /*				echo \kartik\date\DatePicker::widget([
                                                    'name' => 'reserve_from',
                                                    'value' => date('Y-m-d'),
                                                    'options' => ['placeholder' => 'Select issue date ...','required' => 'true', 'id'=>'form_date_for'],
                                                    'pluginOptions' => [
                                                            'autoclose'=>true,
                                                            'format' => 'yyyy-mm-dd',
                                                            'todayHighlight' => true
                                                    ]
                                            ]);
                                            */?>
															<label class="control-label">To</label>
															--><?php
                                /*				echo \kartik\date\DatePicker::widget([
                                                  'name' => 'reserve_to',
                                                  'value' => date('Y-m-d', strtotime('+1 days')),
                                                  'options' => ['placeholder' => 'Select issue date ...','required' => 'true', 'id'=>'form_date_to'],
                                                  'pluginOptions' => [
                                                          'autoclose'=>true,
                                                          'format' => 'yyyy-mm-dd',
                                                          'todayHighlight' => true
                                                  ]
                                          ]);
                                          */?>

                                  <div class="col-xs-12">
                                      <div>Rent date for: <span id="rent_day"><?=Yii::$app->request->get('reserve_from')?></span></div>
                                      <div>Rent date to: <span id="rent_day"><?=Yii::$app->request->get('reserve_to')?></span></div>
                                      <div>Rent day: <span id="rent_day"><?=$day?></span></div>
                                      <div>Total price($): <span id="form_price_rent_d"><?=$rent['price']*$day?></span></div>
                                    <?php if ($rent['credits']) {?>
                                        <div id="credits_show">Total price(credits): <span id="form_price_rent_c"><?=$rent['credits']*$day?></span></div>
                                    <?php }?>
                                  </div>
                                <?= Html::submitButton( Yii::t('app', 'Pay with Paypal'), ['name'=> 'reserve_paypal', 'class' => 'btn btn-action']) ?>

                                <?php if ($rent['credits']) echo  Html::submitButton( Yii::t('app', 'Pay credits'), ['name'=> 'reserve_credits', 'id'=>'credits_show_button', 'class' => 'btn btn-action']); ?>
                                <?php $form = ActiveForm::end(); ?>
                              </div>
                          </div>
                      </div>
                  </div>
              <?php } ?>
			</div>
		</div>
	</div>

