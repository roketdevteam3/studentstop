<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use app\widgets\FavoriterenthouseWidget;
use kartik\rating\StarRating;
use app\widgets\rating\RatingrentalWidget;

use app\assets\RentshousefavotiteAsset;
RentshousefavotiteAsset::register($this);
use kartik\date\DatePicker;

?>

	<div class="row">
		<?= $this->render('right_menu',['userPosted' => $userPosted]); ?>
		<div class="rent-content">
			<div class="container-fluid background-block">
				<div class="row">
					<?php $form = ActiveForm::begin([
						'method' => 'get',
						'action' => Url::home().'rentshouse',
						'options' => [
							'class' => 'top-search-form'
							]
						]); ?>
						<input type="text" name="rent_value" class="form-control searchInput" placeholder="name / address">
					<?php $form = ActiveForm::end(); ?>
				</div>
				<div class="rent-thing-wrap">
					<div class="action-result-window">
						<?php
							if(Yii::$app->session->hasFlash('Reserve')):
								echo Alert::widget([
									'options' => [
										'class' => 'alert-info',
									],
									'body' => \Yii::t('app','Reserved'),
								]);
							endif;
			
							if(Yii::$app->session->hasFlash('Notreserve')):
								echo Alert::widget([
									'options' => [
										'class' => 'alert-warning',
									],
									'body' => \Yii::t('app','Not reserved'),
								]);
							endif;
							
							if(Yii::$app->session->hasFlash('reserveBusy')):
								echo Alert::widget([
									'options' => [
										'class' => 'alert-warning',
									],
									'body' => \Yii::t('app','Sorry, but house reserved!'),
								]);
							endif;
						?>
					</div>
					<?php $arrayYesNo = [0 => 'No', 1 => 'Yes']; ?>
					<?php $price_for_array = [1 => 'day', 2 => 'month', 3 => 'year'];?>
					<h3 class="rent-thing-header"><span><?= $rent['title']; ?></span></h3>
					<div class="rent-thing">
						<div class="rent-thing-image">
							<?php if($rent['img_src'] == ''){ ?>
								<img class="img-responsive" src="<?= Url::home().'images/rent-default.jpg'; ?>">
							<?php }else{ ?>
								<img class="img-responsive" src="<?= Url::home().'images/posts_images/'.$rent['img_src']; ?>">
							<?php } ?>
						</div>
						<div class="centerrer-block">
							<div class="info">
								<h4 class="post-name">Post name</h4>
								<?= RatingrentalWidget::widget(['rent_id' => $rent['id'], 'type' => 'rent_house']); ?>
								<div class="clearfix"></div>
								<h4 class="address"><?= $rent['address']; ?></h4>
								<p class="content-thing"><?= mb_substr($rent['content'], 0, 106, "UTF8").'...'; ?></p>
							</div>
							<div class="bottom-row">
								<p class="booking">Booked <?=\app\modules\posts\models\Rentreserve::find()->where(['rent_id'=>$rent['id'],'rent_type'=>'rents'])->count()?></p>
								<div class="conditions">
									<span>
										<?php
											if($rent['wifi'] == '0'){
										?>
												<img class="img-responsive" src="<?= Url::home().'images/icon/wi-fi-ic2.png'; ?>">
										<?php
											} else {
										?>
												<img class="img-responsive" src="<?= Url::home().'images/icon/wi-fi-ic.png'; ?>">
										<?php
											}
										?>
									</span>
									<span>
										<?php
											if($rent['parking'] == '0'){
										?>
												<img class="img-responsive" src="<?= Url::home().'images/icon/parking2.png'; ?>">
										<?php
											} else {
										?>
												<img class="img-responsive" src="<?= Url::home().'images/icon/parking.png'; ?>">
										<?php
											}
										?>
									</span>
									<span>
										<?php
											if($rent['smoking'] == '0'){
										?>
												<img class="img-responsive" src="<?= Url::home().'images/icon/smoking2.png'; ?>">
										<?php
											} else {
										?>
												<img class="img-responsive" src="<?= Url::home().'images/icon/smoking.png'; ?>">
										<?php
											}
										?>
									</span>
									<span>
										<?php
											if($rent['animal'] == '0'){
										?>
												<img class="img-responsive" src="<?= Url::home().'images/icon/animal2.png'; ?>">
										<?php
											} else {
										?>
												<img class="img-responsive" src="<?= Url::home().'images/icon/animal.png'; ?>">
										<?php
											}
										?>
									</span>
									<span>
										<?php
											if($rent['conditioner'] == '0'){
										?>
												<img class="img-responsive" src="<?= Url::home().'images/icon/cond2.png'; ?>">
										<?php
											} else {
										?>
												<img class="img-responsive" src="<?= Url::home().'images/icon/cond.png'; ?>">
										<?php
											}
										?>
									</span>
								</div>
							</div>
						</div>
						<div class="btn-wrap text-center">
							<div class="price-rent">
								<span>Price: </span><span class="price"><?= $rent['price']; ?>$/</span>
								<?= $price_for_array[$rent['price_for']]; ?>
							</div>
							<?= FavoriterenthouseWidget::widget(['renthouse_id' => $rent['id']]) ?>
							<button type="button" class="btn btn-reserve" data-toggle="modal" data-target="#reserve">Reserve</button>
							<div id="reserve" class="modal padding-body fade" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 class="modal-title">Reserve</h4>
										</div>
										<div class="modal-body">
											<?php $form = ActiveForm::begin(); ?>
											<input type="hidden" name="rent_id" value="<?= $rent['id']; ?>">
											<label class="control-label">From</label>
											<?php 
												echo DatePicker::widget([
													'name' => 'reserve_from', 
													'value' => date('Y-m-d'),
													'options' => ['placeholder' => 'Select issue date ...','required' => 'true'],
													'pluginOptions' => [
														'format' => 'yyyy-mm-dd',
														'todayHighlight' => true
													]
												]);
											?>
											<label class="control-label">To</label>
											<?php
												echo DatePicker::widget([
													'name' => 'reserve_to', 
													'value' => date('Y-m-d', strtotime('+1 days')),
													'options' => ['placeholder' => 'Select issue date ...','required' => 'true'],
													'pluginOptions' => [
														'format' => 'yyyy-mm-dd',
														'todayHighlight' => true
													]
												]);
											?>
											<?= Html::submitButton( Yii::t('app', 'Reserve'), ['name'=> 'reserve_rent', 'class' => 'btn btn-action']) ?>
											<?php $form = ActiveForm::end(); ?>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<?php if($modelRentreserve != null){ ?>
						<table class="rent-thing-detail table">
							<tr>
								<td><b>Reserve from</b></td>
								<td><b>Reserve to</b></td>
									<td>User</td>
								<?php if($rent['user_id'] == \Yii::$app->user->id){ ?>
									<td></td>
								<?php } ?>
							</tr>
							<?php foreach($modelRentreserve as $reserve){ ?>
								<tr>
									<td>
										<?= $reserve->reserve_from; ?>
									</td>
									<td>
										<?= $reserve->reserve_to; ?>
									</td>
										<td>
											<a href="<?= Url::home().'profile/'.$reserve->user_id; ?>">User</a>
										</td>
									<?php if($rent['user_id'] == \Yii::$app->user->id){ ?>
										<td class="delete"><i class="fa fa-times"></i></td>
									<?php } ?>
                                    <?php if(($reserve->user_id == \Yii::$app->user->id) && ($reserve['rating_count'] == 0)){ ?>
                                        <td>
                                            <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#add_feedback<?= $reserve['id']; ?>">Add Feedback</button>
                                            
                                            <div id="add_feedback<?= $reserve['id']; ?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Add Feedback</h4>
                                                        </div>
                                                        <?php $form = ActiveForm::begin(); ?>
                                                        <div class="modal-body">
                                                            <input type="hidden" name="Rating[reserve_id]" value="<?= $reserve['id']; ?>">
                                                            <input type="hidden" name="Rating[from_user_id]" value="<?= \Yii::$app->user->id; ?>">
                                                            <input type="hidden" name="Rating[to_object_id]" value="<?= $rent['user_id']; ?>">
                                                            <?php echo StarRating::widget([
                                                                'name' => 'Rating[rating_count]',
                                                                'pluginOptions' => [
                                                                    'step' => '1',
                                                                    'size'=>'xs',
                                                                    'showCaption' => false,
                                                                ],
                                                            ]); ?>
                                                            <textarea name="Rating[comment]" value="<?= $rent['user_id']; ?>"> </textarea>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Close</button>
                                                            <?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'add_reting', 'class' => 'btn btn-primary']) ?>
                                                        </div>
                                                        <?php $form = ActiveForm::end(); ?>
                                                   </div>
                                                </div>
                                            </div>
                                        </td>
                                    <?php } ?>
							<?php } ?>
						</table>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
