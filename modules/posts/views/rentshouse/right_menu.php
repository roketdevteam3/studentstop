<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\ActiveForm;
	use app\widgets\FilterrenthouseWidget;
		use app\widgets\rating\RatingrentaluserWidget;
	
	use app\assets\FilterAsset;
	FilterAsset::register($this);
	
	$allUrl = Yii::$app->request->url;
		
	if(strpos($allUrl, 'mycompany/jobs')){$class1 = 'active';}else{$class1 = '';}
	if(strpos($allUrl, 'mycompany/edit')){$class2 = 'active';}else{$class2 = '';}
	if(strpos($allUrl, 'addbusiness')){$class3 = 'active';}else{$class3 = '';}
	
?>
	<div class="rental-right-panel">
		<?php if(isset($userPosted)){ ?>
			<div class="class-user-av">
				<img class="img-responsive" src="<?= Url::home(); ?>images/users_images/<?= $userPosted['avatar']; ?>">
			</div>
			<div class="rating-blok">
				<?= RatingrentaluserWidget::widget(['object_id' => $userPosted['id'], 'type' => 'rental_a_house']); ?>
				<?php //= RatingstarWidget::widget(['user_id' => $modelClass['professor'], 'type' => 'professor','type_show' => 'with_rat']); ?>
				<div style="clear:both;"></div>
			</div>
			<div class="menu-wrapper">
				<div class="user-info" style="color:white;">
					<span>
						Name:
						<span>
								<?= $userPosted['name']; ?> <?= $userPosted['surname']; ?>
						</span>
					</span>
							<br>
					<span>
						Birthday:
						<span>
								<?= $userPosted['birthday']; ?>
						</span>
					</span>
							<br>
					<span>
						Phone:
						<span>
								<?= $userPosted['mobile']; ?>
						</span>
					</span>
							<br>
					<span>
						Email:
						<span>
								<?= $userPosted['email']; ?>
						</span>
					</span>
						<br>
				</div>
			</div>
		<?php } ?>
		<nav class="menu-right">
			<ul class="">
				<li class="<?= $class1; ?>">
					<?= HTML::a('Search',Url::home().'rentshouse',['class' => ""]); ?>
				</li>
				<li class="<?= $class2; ?>">
					<?= HTML::a('Favorites',Url::home().'rentshouse/favorite',['class' => ""]);?>
				</li>
				<li class="<?= $class3; ?>">
					<?= HTML::a('My account <span class="badge">'.\app\modules\posts\models\Rentreserve::find()->where(['user_id'=>Yii::$app->user->id,'rent_type' => 'rent_house','status'=>0,'request'=>1])->count().'</span>',Url::home().'rentshouse/myprofile',['class' => ""]); ?>
				</li>
				<li class="<?= $class3; ?>">
					<?= HTML::a('My booking <span class="badge">'.\app\modules\posts\models\Rentreserve::find()->where(['user_id'=>Yii::$app->user->id,'rent_type' => 'rent_house','status'=>0,'view'=>1])->andWhere(['!=','request',0])->count().'</span>',Url::home().'rentshouse/myreservations',['class' => ""]); ?>
				</li>
			</ul>
		</nav>
		<?php if(!isset($userPosted)){ ?>
			<?= FilterrenthouseWidget::widget(['route' => $this->context->getRoute()]); ?>
		<?php } ?>
	</div>
