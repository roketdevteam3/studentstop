<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use app\widgets\FavoriterenthouseWidget;

use app\assets\RentshousefavotiteAsset;
RentshousefavotiteAsset::register($this);
?>

	<div class="row">
		<?= $this->render('right_menu'); ?>
		<div class="rent-content">
			<div class="container-fluid background-block">
				<div class="row">
					<?php $form = ActiveForm::begin([
								'method' => 'get',
								'action' => Url::home().'rentshouse',
								'options' => ['class' => 'top-search-form']
							]); ?>
						<input type="text" name="rent_value" class="form-control searchInput" placeholder="search rents a house by title or keywords">
					<?php $form = ActiveForm::end(); ?>
				</div>
				<div class="rents-tab-wrap">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs navigation-tabs no-icons" role="tablist">
						<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Recent</a></li>
						<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Archive</a></li>
						<li role="presentation"><a href="#request" aria-controls="request" role="tab" data-toggle="tab">Request <span class="badge"><?=\app\modules\posts\models\Rentreserve::find()->where(['user_id'=>Yii::$app->user->id,'rent_type' => 'rent_house','status'=>0,'view'=>1])->andWhere(['!=','request',0])->count()?></span></a></li>
					</ul>
				</div>
				<!-- Tab panes -->
				<div class="tab-content rent-tab-content">
					<div role="tabpanel" class="tab-pane active" id="home">
						<div class="rent-thing-wrap">
							<?php $arrayYesNo = [0 => 'No', 1 => 'Yes']; ?>
							<?php $price_for_array = [1 => 'day', 2 => 'month', 3 => 'year'];?>
							<?php foreach($reservationRent as $rent){
								if ((strtotime($rent['reserve_from']) > time())|| (strtotime($rent['reserve_to']) > time())){

                                  $day = 0;
                                $reserve_to = strtotime($rent['reserve_to']);
                                $reserve_from = strtotime($rent['reserve_from']);
                                $datediff = $reserve_to - $reserve_from;

                                $day = floor($datediff / (60 * 60 * 24));

								?>
                                    <div class="rent-thing rentBlockInfo" data-rent_id="<?= $rent['id']; ?>" data-position_lat="<?= $rent['position_lat']; ?>" data-position_lng="<?= $rent['position_lng']; ?>">
                                        <div class="rent-thing-image">
                                          <?php if($rent['img_src'] == ''){ ?>
                                              <img class="img-responsive" src="<?= Url::home().'images/bg.jpg'; ?>">
                                          <?php }else{ ?>
                                              <img class="img-responsive" src="<?= Url::home().'images/posts_images/'.$rent['img_src']; ?>">
                                          <?php } ?>
                                        </div>
                                        <div class="" style="padding-top:10px">
                                            <div class="info">
                                                <h4 class="post-name"><?=$rent['title']?></h4>
                                            </div>
                                            <a class="btn btn-view" id="view" data-renthouse_id="<?=$rent['id']?>" >View</a>

                                            <div class="total-rating ">
                                              <?= \app\widgets\rank\RankrentsWidget::widget(['rent_id' => $rent['id'], 'type' => 'rent_house']); ?>
                                            </div>

                                            <div class="" style="float: right">
                                                <div class="info star_style" style="position: relative; bottom: 4px;">
                                                  <?= \app\widgets\rating\RatingrentalWidget::widget(['rent_id' => $rent['id'], 'type' => 'rent_house']); ?>
                                                </div>
                                            </div>
                                            <div class="views-count ">
                                              <?=$rent['views']?> Views
                                            </div>
                                        </div>
                                        <div class="centerrer-block">
                                            <div class="info">
                                                <div class="clearfix"></div>
                                                <p class="content-thing"><?= mb_substr($rent['content'], 0, 106, "UTF8").'...'; ?></p>
                                                <p>Rented for <?=$rent['reserve_from']?> to <?=$rent['reserve_to']?></p>
                                            </div>
                                            <div class="bottom-row style-bottom-row-cond">
                                               <!--<p class="booking">
													Booked <?=\app\modules\posts\models\Rentreserve::find()->where(['rent_id'=>$rent['id'],'rent_type'=>'rents'])->count()?>
												</p>-->
                                                <div class="conditions">
													<span>
														<?php
                                                        if($rent['wifi'] == '0'){
                                                          ?>
                                                            <img class="img-responsive" src="<?= Url::home().'images/icon/wi-fi-ic2.png'; ?>">
                                                          <?php
                                                        } else {
                                                          ?>
                                                            <img class="img-responsive" src="<?= Url::home().'images/icon/wi-fi-ic.png'; ?>">
                                                          <?php
                                                        }
                                                        ?>
													</span>
                                                    <span>
														<?php
                                                        if($rent['parking'] == '0'){
                                                          ?>
                                                            <img class="img-responsive" src="<?= Url::home().'images/icon/parking2.png'; ?>">
                                                          <?php
                                                        } else {
                                                          ?>
                                                            <img class="img-responsive" src="<?= Url::home().'images/icon/parking.png'; ?>">
                                                          <?php
                                                        }
                                                        ?>
													</span>
                                                    <span>
														<?php
                                                        if($rent['smoking'] == '0'){
                                                          ?>
                                                            <img class="img-responsive" src="<?= Url::home().'images/icon/smoking2.png'; ?>">
                                                          <?php
                                                        } else {
                                                          ?>
                                                            <img class="img-responsive" src="<?= Url::home().'images/icon/smoking.png'; ?>">
                                                          <?php
                                                        }
                                                        ?>
													</span>
                                                    <span>
														<?php
                                                        if($rent['animal'] == '0'){
                                                          ?>
                                                            <img class="img-responsive" src="<?= Url::home().'images/icon/animal2.png'; ?>">
                                                          <?php
                                                        } else {
                                                          ?>
                                                            <img class="img-responsive" src="<?= Url::home().'images/icon/animal.png'; ?>">
                                                          <?php
                                                        }
                                                        ?>
													</span>
                                                    <span>
														<?php
                                                        if($rent['conditioner'] == '0'){
                                                          ?>
                                                            <img class="img-responsive" src="<?= Url::home().'images/icon/cond2.png'; ?>">
                                                          <?php
                                                        } else {
                                                          ?>
                                                            <img class="img-responsive" src="<?= Url::home().'images/icon/cond.png'; ?>">
                                                          <?php
                                                        }
                                                        ?>
													</span>
                                                </div>
                                                <h4 class="address style-addres-text"><?= $rent['address']; ?></h4>
                                            </div>
                                        </div>

                                        <div class="bottom-row">
                                            <div class="conditions mustyle_class">
                                                <div class="price-rent">
                                                    <!--													<span class="price">--><?php //echo ($day) ? $rent['price']*$day.'$' : '' ?><!--</span>-->
                                                    <span class="price" style="font-size: 16px">
                                                      <?php echo ($rent['price']>0) ? 'Price: '.$rent['price']*$day.' $ ' : '' ?> <br/>
                                                      <?php echo ($rent['credits']>0) ? 'Credits: '.$rent['credits']*$day.' ' : '<br/>' ?>
                                                    </span>
                                                </div>
                                                <div style="position: relative; top: 46px; padding-right: 22px;">
                                                    <p class="booking col-xs-6">Booked <?=\app\modules\posts\models\Rentreserve::find()->where(['rent_id'=>$rent['id'],'rent_type'=>'rent_house'])->count()?></p>
                                                  <?= FavoriterenthouseWidget::widget(['renthouse_id' => $rent['id']]) ?>
                                                </div>
                                            </div>
                                        </div>



                                    </div>


                                    <div class="modalRightPagesLS" id="view_rentshouse_<?=$rent['id']?>">
										<?php
										$query = new \yii\db\Query();
										$query->select(['*','id' => '{{%user}}.id'])
												->from('{{%user}}')
												->leftJoin('{{%user_info}}', '{{%user_info}}.id_user = {{%user}}.id')
												->where(['{{%user}}.id' => $rent['user_id']]);

										$command = $query->createCommand();
										$userPosted = $command->queryOne();
										?>
										<a class="btn btn-close pull-right" onclick="$('#view_rentshouse_<?=$rent['id']?>').hide()">
											<span class="" aria-hidden="true">x</span>
										</a>
										<div class="user-info">
											<div class="class-user-av">
												<?php if ($userPosted['avatar']){?>
													<img class="img-responsive" src="<?= Url::home(); ?>images/users_images/<?= $userPosted['avatar']; ?>">
												<?php } else {?>
													<img class="img-responsive" src="<?= Url::home(); ?>images/default_avatar.jpg">
												<?php } ?>
											</div>
											<span class="user-name">
												<?= $userPosted['name']; ?> <?= $userPosted['surname']; ?>
											</span>
											<div class="right-block">
												<?= \app\widgets\rating\RatingrentaluserWidget::widget(['object_id' => $userPosted['id'], 'type' => 'rental_a_house']); ?>
												<span class="price"><?= $rent['price']?>$/day</span>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="panel-content">
											<!-- Nav tabs -->
											<ul class="nav nav-tabs" role="tablist">
												<li role="presentation" class="active"><a href="#description<?=$rent['id']?>" aria-controls="description<?=$rent['id']?>" role="tab" data-toggle="tab">Description</a></li>
												<li role="presentation"><a href="#map<?=$rent['id']?>" aria-controls="map<?=$rent['id']?>" role="tab" data-toggle="tab">Map</a></li>
												<li role="presentation"><a href="#images<?=$rent['id']?>" aria-controls="images<?=$rent['id']?>" role="tab" data-toggle="tab">Images</a></li>
												<li role="presentation"><a href="#calendar<?=$rent['id']?>" aria-controls="calendar<?=$rent['id']?>" role="tab" data-toggle="tab" onmousemove="$('#calendar_w_<?=$rent['id']?>').fullCalendar('today');">Calendar</a></li>
												<li role="presentation"><a href="#comments<?=$rent['id']?>" aria-controls="comments<?=$rent['id']?>" role="tab" data-toggle="tab">Reviews</a></li>
											</ul>
											<!-- Tab panes -->
											<div class="tab-content">
												<div role="tabpanel" class="tab-pane active" id="description<?=$rent['id']?>">
													<div class="rent-thing">
														<div class="info">
															<h4 class="address"><?= $rent['address']; ?></h4>
															<p class="content-thing"><?=$rent['content'] ?></p>
														</div>
													</div>
												</div>
												<div role="tabpanel" class="tab-pane" id="map<?=$rent['id']?>">3...</div>
												<div role="tabpanel" class="tab-pane" id="images<?=$rent['id']?>">
													<?php
													$images = \app\modules\posts\models\Postsphotos::find()->where(['post_id'=>$rent['id']])->all();
													foreach($images as $image){
														echo '<img src="/images/posts_images/'.$image->image_src.'" />';
													}
													?>
												</div>
												<div role="tabpanel" class="tab-pane" id="calendar<?=$rent['id']?>">
													<div id="calen_id_<?=$rent['id']?>">
														<?php
														$events = [];
														$renteds = \app\modules\posts\models\Rentreserve::find()->where(['rent_type'=>'rent_house', 'rent_id'=>$rent['id']])->all();
														foreach($renteds as $rented ){
															$event        = new \yii2fullcalendar\models\Event();
															$event->id    = $rented->id;
															$event->title = 'Rented';
															$event->start = date('Y-m-d\TH:i:s\Z', strtotime($rented->reserve_from));
															$event->end = date('Y-m-d\TH:i:s\Z', strtotime($rented->reserve_to));
															$events[] = $event;
														}
														/*echo \yii2fullcalendar\yii2fullcalendar::widget(array(
                        	                                    'clientOptions' => [
           												//			'eventLimit'=> true,
           												//			'fixedWeekCount'=>false,
           												//		'defaultDate' => date('Y-m-d\TH:i:s\Z'),
                        	                                        'nowIndicator'=>false,
                        	                                            'defaultDate' => date('Y-m-d',time())
                        	                                    ],
                        	                                    'events'=> $events,
                        	                            ));*/
														echo \yii2fullcalendar\yii2fullcalendar::widget([
																'id' => 'calendar_w_'.$rent['id'],
																'clientOptions' => [
																		'getDate' => date('Y-m-d',time()),
																		'defaultDate' => date('Y-m-d',time()),
																		'today'=>true,
//														'dayClick'=>new \yii\web\JsExpression('function (cellInfo, jsEvent) {console.log(cellInfo);}')
																],
																'events'=> $events,
														]);

														?>
													</div>
												</div>
												<div role="tabpanel" class="tab-pane" id="comments<?=$rent['id']?>">
													<?php
													foreach($renteds as $rented){
														if ($rented->rating_comment)
															echo "User name: ".$rented->user['name']."<br/> Comment: ".$rented->rating_comment."<br/><br/><br/>";
													}
													?>
												</div>
											</div>
										</div>
									</div>
								<?php }  }?>
						</div>
                    </div>
					<div role="tabpanel" class="tab-pane" id="profile">
						<div class="rent-thing-wrap">
							<?php $arrayYesNo = [0 => 'No', 1 => 'Yes']; ?>
							<?php $price_for_array = [1 => 'day', 2 => 'month', 3 => 'year'];?>
							<?php foreach($reservationRent as $rent){
							if (strtotime($rent['reserve_to']) < time()){
                              $day = 0;
                              $reserve_to = strtotime($rent['reserve_to']);
                              $reserve_from = strtotime($rent['reserve_from']);
                              $datediff = $reserve_to - $reserve_from;

                              $day = floor($datediff / (60 * 60 * 24));
								?>
								<div class="rent-thing rentBlockInfo" data-rent_id="<?= $rent['id']; ?>" data-position_lat="<?= $rent['position_lat']; ?>" data-position_lng="<?= $rent['position_lng']; ?>">
									<div class="rent-thing-image">
										<?php if($rent['img_src'] == ''){ ?>
											<img class="img-responsive" src="<?= Url::home().'images/bg.jpg'; ?>">
										<?php }else{ ?>
											<img class="img-responsive" src="<?= Url::home().'images/posts_images/'.$rent['img_src']; ?>">
										<?php } ?>
									</div>
									<div class="centerrer-block">
										<div class="info">
											<h4 class="post-name"><?=$rent['title']?></h4>
											<?= \app\widgets\rating\RatingrentalWidget::widget(['rent_id' => $rent['id'], 'type' => 'rent_house']); ?>
											<div class="clearfix"></div>
											<p class="content-thing"><?= mb_substr($rent['content'], 0, 106, "UTF8").'...'; ?></p>
											<p>Rented for <?=$rent['reserve_from']?> to <?=$rent['reserve_to']?></p>
										</div>
										<div class="rating-thing">
											<div class="bottom-row style-bottom-row-cond">
												<p class="booking">Booked <?=\app\modules\posts\models\Rentreserve::find()->where(['rent_id'=>$rent['id'],'rent_type'=>'rent_house'])->count()?></p>
												<div class="conditions">
													<span>
														<?php
														if($rent['wifi'] == '0'){
															?>
															<img class="img-responsive" src="<?= Url::home().'images/icon/wi-fi-ic2.png'; ?>">
															<?php
														} else {
															?>
															<img class="img-responsive" src="<?= Url::home().'images/icon/wi-fi-ic.png'; ?>">
															<?php
														}
														?>
													</span>
													<span>
														<?php
														if($rent['parking'] == '0'){
															?>
															<img class="img-responsive" src="<?= Url::home().'images/icon/parking2.png'; ?>">
															<?php
														} else {
															?>
															<img class="img-responsive" src="<?= Url::home().'images/icon/parking.png'; ?>">
															<?php
														}
														?>
													</span>
													<span>
														<?php
														if($rent['smoking'] == '0'){
															?>
															<img class="img-responsive" src="<?= Url::home().'images/icon/smoking2.png'; ?>">
															<?php
														} else {
															?>
															<img class="img-responsive" src="<?= Url::home().'images/icon/smoking.png'; ?>">
															<?php
														}
														?>
													</span>
													<span>
														<?php
														if($rent['animal'] == '0'){
															?>
															<img class="img-responsive" src="<?= Url::home().'images/icon/animal2.png'; ?>">
															<?php
														} else {
															?>
															<img class="img-responsive" src="<?= Url::home().'images/icon/animal.png'; ?>">
															<?php
														}
														?>
													</span>
													<span>
														<?php
														if($rent['conditioner'] == '0'){
															?>
															<img class="img-responsive" src="<?= Url::home().'images/icon/cond2.png'; ?>">
															<?php
														} else {
															?>
															<img class="img-responsive" src="<?= Url::home().'images/icon/cond.png'; ?>">
															<?php
														}
														?>
													</span>
												</div>
                                                <h4 class="address style-addres-text"><?= $rent['address']; ?></h4>
											</div>
										</div>

									</div>
									<div class="btn-wrap text-right">
										<div class="price-rent">
											<span>Price: </span><span class="price"><?= ($rent['total_price']) ? $rent['total_price'].' $' : $rent['total_credits'].' credits' ?></span>
										</div>
										<div class="views-count">
											<?=$rent['views']?> reviews
										</div>
										<a class="btn btn-view" id="view" data-renthouse_id="<?=$rent['id']?>" >View</a>
										<div class="price-rent">
											<span class="price" style="font-size: 16px">
                                                      <?php echo ($rent['price']>0) ? 'Price: '.$rent['price']*$day.' $ ' : '' ?> <br/>
                                                <?php echo ($rent['credits']>0) ? 'Credits: '.$rent['credits']*$day.' ' : '<br/>' ?>
                                            </span>
										</div>
										<?= FavoriterenthouseWidget::widget(['renthouse_id' => $rent['id']]) ?>
									</div>

								</div>
								<div class="modalRightPagesLS" id="view_rentshouse_<?=$rent['id']?>">
									<?php
									$query = new \yii\db\Query();
									$query->select(['*','id' => '{{%user}}.id'])
											->from('{{%user}}')
											->leftJoin('{{%user_info}}', '{{%user_info}}.id_user = {{%user}}.id')
											->where(['{{%user}}.id' => $rent['user_id']]);

									$command = $query->createCommand();
									$userPosted = $command->queryOne();
									?>
									<a class="btn btn-close pull-right" onclick="$('#view_rentshouse_<?=$rent['id']?>').hide()">
										<span class="" aria-hidden="true">x</span>
									</a>
									<div class="user-info">
										<div class="class-user-av">
											<?php if ($userPosted['avatar']){?>
												<img class="img-responsive" src="<?= Url::home(); ?>images/users_images/<?= $userPosted['avatar']; ?>">
											<?php } else {?>
												<img class="img-responsive" src="<?= Url::home(); ?>images/default_avatar.jpg">
											<?php } ?>
										</div>
										<span class="user-name">
											<?= $userPosted['name']; ?> <?= $userPosted['surname']; ?>
										</span>
										<div class="right-block">
											<?= \app\widgets\rating\RatingrentaluserWidget::widget(['object_id' => $userPosted['id'], 'type' => 'rental_a_house']); ?>
											<span class="price"><?= $rent['price']?>$/day</span>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="panel-content">
										<!-- Nav tabs -->
										<ul class="nav nav-tabs" role="tablist">
											<li role="presentation" class="active"><a href="#description<?=$rent['id']?>" aria-controls="description<?=$rent['id']?>" role="tab" data-toggle="tab">Description</a></li>
											<li role="presentation"><a href="#map<?=$rent['id']?>" aria-controls="map<?=$rent['id']?>" role="tab" data-toggle="tab">Map</a></li>
											<li role="presentation"><a href="#images<?=$rent['id']?>" aria-controls="images<?=$rent['id']?>" role="tab" data-toggle="tab">Images</a></li>
											<li role="presentation"><a href="#calendar<?=$rent['id']?>" aria-controls="calendar<?=$rent['id']?>" role="tab" data-toggle="tab" onmousemove="$('#calendar_w_<?=$rent['id']?>').fullCalendar('today');">Calendar</a></li>
											<li role="presentation"><a href="#comments<?=$rent['id']?>" aria-controls="comments<?=$rent['id']?>" role="tab" data-toggle="tab">Comments</a></li>
										</ul>
										<!-- Tab panes -->
										<div class="tab-content">
											<div role="tabpanel" class="tab-pane active" id="description<?=$rent['id']?>">
												<div class="rent-thing">
													<div class="info">
														<h4 class="address"><?= $rent['address']; ?></h4>
														<p class="content-thing"><?=$rent['content'] ?></p>
													</div>
												</div>
											</div>
											<div role="tabpanel" class="tab-pane" id="map<?=$rent['id']?>">3...</div>
											<div role="tabpanel" class="tab-pane" id="images<?=$rent['id']?>">
												<?php
												$images = \app\modules\posts\models\Postsphotos::find()->where(['post_id'=>$rent['id']])->all();
												foreach($images as $image){
													echo '<img src="/images/posts_images/'.$image->image_src.'" />';
												}
												?>
											</div>
											<div role="tabpanel" class="tab-pane" id="calendar<?=$rent['id']?>">
												<div id="calen_id_<?=$rent['id']?>">
													<?php
													$events = [];
													$renteds = \app\modules\posts\models\Rentreserve::find()->where(['rent_type'=>'rent_house', 'rent_id'=>$rent['id']])->all();
													foreach($renteds as $rented ){
														$event        = new \yii2fullcalendar\models\Event();
														$event->id    = $rented->id;
														$event->title = 'Rented';
														$event->start = date('Y-m-d\TH:i:s\Z', strtotime($rented->reserve_from));
														$event->end = date('Y-m-d\TH:i:s\Z', strtotime($rented->reserve_to));
														$events[] = $event;
													}
													/*echo \yii2fullcalendar\yii2fullcalendar::widget(array(
                        	                                'clientOptions' => [
       												//		'eventLimit'=> true,
       												//		'fixedWeekCount'=>false,
       												//	'defaultDate' => date('Y-m-d\TH:i:s\Z'),
                        	                                    'nowIndicator'=>false,
                        	                                        'defaultDate' => date('Y-m-d',time())
                        	                                ],
                        	                                'events'=> $events,
                        	                        ));*/
													echo \yii2fullcalendar\yii2fullcalendar::widget([
															'id' => 'calendar_w_'.$rent['id'],
															'clientOptions' => [
																	'getDate' => date('Y-m-d',time()),
																	'defaultDate' => date('Y-m-d',time()),
																	'today'=>true,
//														'dayClick'=>new \yii\web\JsExpression('function (cellInfo, jsEvent) {console.log(cellInfo);}')
															],
															'events'=> $events,
													]);
													?>
												</div>
											</div>
											<div role="tabpanel" class="tab-pane" id="comments<?=$rent['id']?>">
												<?php
												foreach($renteds as $rented){
													if ($rented->rating_comment)
														echo "User name: ".$rented->user['name']."<br/> Comment: ".$rented->rating_comment."<br/><br/><br/>";
												}
												?>
											</div>
										</div>
									</div>
								</div>
							<?php }  }?>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="request">
						<table class="table table-rent">
							<thead>
								<th>Rent house name</th>
								<th>Rent date for</th>
								<th>Rent date to</th>
								<th>Status</th>
								<th></th>
							</thead>
							<tbody>
								<?php foreach($request as $item){?>
										<tr>
											<td><?= $item->renthouse['title']?></td>
											<td><?= date('m-d-Y',strtotime($item->reserve_from)) ?></td>
											<td><?=date('m-d-Y',strtotime($item->reserve_to)) ?></td>
											<td>
												<?php if ($item->request == 1){
													echo 'Consider';
												} elseif($item->request == 2){
													echo 'Аpproved ';
													if ($item->renthouse['price']) echo '<a href="/rentshouse/metod_pay?metod=1&id='.$item->id.'" class="btn btn-action">Pay with Paypal</a> ';
													if ($item->total_credits) echo ' <a href="/rentshouse/metod_pay?metod=2&id='.$item->id.'" class="btn btn-action">Pay with Credits</a>';
												}elseif($item->request == 3){
													echo 'Rejected';
												} ?>
											</td>
											<td>
												<button class="btn btn-primary" id="view" data-renthouse_id="<?=$item->renthouse['id']?>">View</button>
												<div class="modalRightPagesLS" id="view_rentshouse_<?=$item->renthouse['id']?>">
													<?php
													$rent = \app\modules\posts\models\Rentshouse::find()->where(['id'=>$item->renthouse['id']])->asArray()->one();
													$query = new \yii\db\Query();
													$query->select(['*','id' => '{{%user}}.id'])
															->from('{{%user}}')
															->leftJoin('{{%user_info}}', '{{%user_info}}.id_user = {{%user}}.id')
															->where(['{{%user}}.id' => $item->renthouse['id']]);

													$command = $query->createCommand();
													$userPosted = $command->queryOne();
													?>
													<a class="btn btn-close pull-right" onclick="$('#view_rentshouse_<?=$rent['id']?>').hide()">
														<span class="" aria-hidden="true">x</span>
													</a>
													<div class="user-info">
														<div class="class-user-av">
															<?php if ($userPosted['avatar']){?>
																<img class="img-responsive" src="<?= Url::home(); ?>images/users_images/<?= $userPosted['avatar']; ?>">
															<?php } else {?>
																<img class="img-responsive" src="<?= Url::home(); ?>images/default_avatar.jpg">
															<?php } ?>
														</div>
														<span class="user-name">
															<?= $userPosted['name']; ?> <?= $userPosted['surname']; ?>
														</span>
														<div class="right-block">
															<?= \app\widgets\rating\RatingrentaluserWidget::widget(['object_id' => $userPosted['id'], 'type' => 'rental_a_house']); ?>
															<span class="price"><?= ($item->total_price) ? $item->total_price.'$' : $item->total_credits.' Credits' ?></span>
														</div>
														<div class="clearfix"></div>
													</div>
													<div class="panel-content">

														<!-- Nav tabs -->
														<ul class="nav nav-tabs" role="tablist">
															<li role="presentation" class="active"><a href="#description<?=$rent['id']?>" aria-controls="description<?=$rent['id']?>" role="tab" data-toggle="tab">Description</a></li>
															<li role="presentation"><a href="#map<?=$rent['id']?>" aria-controls="map<?=$rent['id']?>" role="tab" data-toggle="tab">Map</a></li>
															<li role="presentation"><a href="#images<?=$rent['id']?>" aria-controls="images<?=$rent['id']?>" role="tab" data-toggle="tab">Images</a></li>
															<li role="presentation"><a href="#calendar<?=$rent['id']?>" aria-controls="calendar<?=$rent['id']?>" role="tab" data-toggle="tab" onmousemove="$('#calendar_w_<?=$rent['id']?>').fullCalendar('today');">Calendar</a></li>
															<li role="presentation"><a href="#comments<?=$rent['id']?>" aria-controls="comments<?=$rent['id']?>" role="tab" data-toggle="tab">Comments</a></li>
														</ul>
														<!-- Tab panes -->
														<div class="tab-content">
															<div role="tabpanel" class="tab-pane active" id="description<?=$rent['id']?>">
																<div class="rent-thing">
																	<div class="info">
																		<h4 class="address"><?= $rent['address']; ?></h4>
																		<p class="content-thing"><?=$rent['content'] ?></p>
																	</div>
																</div>
															</div>
															<div role="tabpanel" class="tab-pane" id="map<?=$rent['id']?>">3...</div>
															<div role="tabpanel" class="tab-pane" id="images<?=$rent['id']?>">
																<?php
																$images = \app\modules\posts\models\Postsphotos::find()->where(['post_id'=>$rent['id']])->all();
																foreach($images as $image){
																	echo '<img src="/images/posts_images/'.$image->image_src.'" />';
																}
																?>
															</div>
															<div role="tabpanel" class="tab-pane" id="calendar<?=$rent['id']?>">
																<div id="calen_id_<?=$rent['id']?>">
																	<?php
																	$events = [];
																	$renteds = \app\modules\posts\models\Rentreserve::find()->where(['rent_type'=>'rent_house', 'rent_id'=>$rent['id'],'status'=>1])->all();
																	foreach($renteds as $rented ){
																		$event        = new \yii2fullcalendar\models\Event();
																		$event->id    = $rented->id;
																		$event->title = 'Rented';
																		$event->start = date('Y-m-d\TH:i:s\Z', strtotime($rented->reserve_from));
																		$event->end = date('Y-m-d\TH:i:s\Z', strtotime($rented->reserve_to));
																		$events[] = $event;
																	}
																	/*echo \yii2fullcalendar\yii2fullcalendar::widget(array(
                                                                            'clientOptions' => [
                                             //																'eventLimit'=> true,
                                             //																'fixedWeekCount'=>false,
                                             //															'defaultDate' => date('Y-m-d\TH:i:s\Z'),
                                                                                'nowIndicator'=>false,
                                                                                    'defaultDate' => date('Y-m-d',time())
                                                                            ],
                                                                            'events'=> $events,
                                                                    ));*/
																	echo \yii2fullcalendar\yii2fullcalendar::widget([
																			'id' => 'calendar_w_'.$rent['id'],
																			'clientOptions' => [
																					'getDate' => date('Y-m-d',time()),
																					'defaultDate' => date('Y-m-d',time()),
																					'today'=>true,
																				//															'dayClick'=>new \yii\web\JsExpression('function (cellInfo, jsEvent) {console.log(cellInfo);}')
																			],
																			'events'=> $events,
																	]);

																	?>
																</div>
															</div>
															<div role="tabpanel" class="tab-pane style-tb-pane-hr" id="comments<?=$rent['id']?>">
																<?php
																foreach($renteds as $rented){
																	if ($rented->rating_comment)
																		echo $rented->user['name']."<br/>".$rented->rating_comment."<br/><br/><hr><br/>";
																}
																?>
															</div>
														</div>
													</div>
												</div>
											</td>
										</tr>
								<?php }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>