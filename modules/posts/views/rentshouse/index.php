<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use app\widgets\FavoriterenthouseWidget;
use app\widgets\SortingrenthouseWidget;
use app\assets\RentshouseAsset;
RentshouseAsset::register($this);
use app\widgets\rating\RatingrentalWidget;

use app\assets\RentshousefavotiteAsset;
RentshousefavotiteAsset::register($this);

?>
	<div class="row">
	<?= $this->render('right_menu'); ?>
		<div class="rent-content">
			<div class="container-fluid background-block">
				<div class="row">
					<?php $form = ActiveForm::begin([
						'action' => '/rentshouse',
						'method' => 'get',
						'options' => [
							'class' => 'top-search-form renthouse-search form-inline'
							]
						]); ?>
					<?php 
						$rent_value = '';
						if(isset($_GET['rent_value'])){
							$rent_value = $_GET['rent_value'];
						} 
					?>
					<div class="input-group">
						<div class="form-group">
							<input type="text" name="rent_value" class="form-control searchInput" value="<?= $rent_value; ?>" placeholder="Destination, city, adress">
							<?php
								$reserve_from = '';
								if(isset($_GET['reserve_from'])){
									$reserve_from = $_GET['reserve_from'];
								}
								echo \kartik\date\DatePicker::widget([
										'name' => 'reserve_from',
										'type' => \kartik\date\DatePicker::TYPE_INPUT,
										'value' => $reserve_from,
										'options' => ['placeholder' => 'Check in','id'=>'filter_reserve_for','class' => 'PlaceholderInput',],
										'pluginOptions' => [
                                        'autoclose'=>true,
                                        'format' => 'yyyy-mm-dd',
                                          'todayHighlight' => true,
										]
								]);
							?>
							<?php
								$reserve_to = '';
								if(isset($_GET['reserve_to'])){
									$reserve_to = $_GET['reserve_to'];
								}
								echo \kartik\date\DatePicker::widget([
										'name' => 'reserve_to',
										'type' => \kartik\date\DatePicker::TYPE_INPUT,
										'value' => $reserve_to,
										'options' => ['placeholder' => 'Check out', 'onchange'=>'limitDate()', 'id'=>'filter_reserve_to'],
										'pluginOptions' => [
                                            'autoclose'=>true,
                                            'format' => 'yyyy-mm-dd',
                                          'todayHighlight' => true,
										]
								]);
							?>
							<?php
								$adults = '';
								if(isset($_GET['adults'])){
									$adults = $_GET['adults'];
								}
								echo Html::dropDownList('adults',$adults,['1'=>'1 quest','2'=>'2 quest','3'=>'3 quest','4'=>'4 quest','5'=>'5 quest'],['class' => 'form-control'])
							?>
							
							<?php
							$wifi = '';
							if(isset($_GET['wifi'])){
								$wifi = $_GET['wifi'];
							}
							?>
							<input type="hidden" name="wifi" value="<?=$wifi?>">
		
							<?php
							$parking = '';
							if(isset($_GET['parking'])){
								$parking = $_GET['parking'];
							}
							?>
							<input type="hidden" name="parking" value="<?=$parking?>">
		
							<?php
							$smoking = '';
							if(isset($_GET['smoking'])){
								$smoking = $_GET['smoking'];
							}
							?>
							<input type="hidden" name="smoking" value="<?=$smoking?>">
		
		
							<?php
							$tv = '';
							if(isset($_GET['tv'])){
								$tv = $_GET['tv'];
							}
							?>
							<input type="hidden" name="tv" value="<?=$tv?>">
		
		
							<?php
							$animal = '';
							if(isset($_GET['animal'])){
								$animal = $_GET['animal'];
							}
							?>
							<input type="hidden" name="animal" value="<?=$animal?>">
		
							<?php
							$conditioner = '';
							if(isset($_GET['conditioner'])){
								$conditioner = 'checked="checked"';
							}
							?>
							<input type="hidden" name="conditioner" value="<?=$conditioner?>">
		
		
							<?php
							$house_type = '';
							if(isset($_GET['house_type'])){
								$house_type = $_GET['house_type'];
							}
							?>
							<input type="hidden" name="house_type" value="<?=$house_type?>">
		
		
							<?php
							$rooms = '';
							if(isset($_GET['room_count'])){
								$rooms = $_GET['room_count'];
							}
							?>
							<input type="hidden" name="rooms" value="<?=$rooms?>">
		
		
							<?php
							$price_from = '';
							if(isset($_GET['price_from'])){
								$price_from = $_GET['price_from'];
							}
							?>
							<input type="hidden" name="price_from" value="<?=$price_from?>">
		
		
							<?php
							$price_to = '';
							if(isset($_GET['price_to'])){
								$price_to = $_GET['price_to'];
							}
							?>
							<input type="hidden" name="price_to" value="<?=$price_to?>">
		
		
							<?php
							$rating = '';
							if(isset($_GET['rating'])){
								$rating = $_GET['rating'];
							}
							?>
							<input type="hidden" name="rating" value="<?=$rating?>">
		
						</div>
						<span class="input-group-btn">
							<button type="submit" class="btn btn-search">Seasfsfsfrch</button>
						</span>
					</div>
					<?php $form = ActiveForm::end(); ?>
				</div>
				<div class="row">
					<?= SortingrenthouseWidget::widget() ?>
				</div>
				<div class="row">
					<div id="map" class="map">

					</div>
				</div>
				<div class="rent-page">
					<ul class="nav nav-tabs view-tabs">
						<span class="choice">View</span>
						<li>
							<a data-toggle="tab" class="grid-appearance" href="#grid-content"></a>
						</li>
						<li class="active">
							<a data-toggle="tab" class="list-appearance" href="#list-content"></a>
						</li>
					</ul>

					<div class="tab-content rent-tab-content">
						<div id="list-content" class="tab-pane fade in active">
							<div class="rent-thing-wrap">
								<?php
								//if(Yii::$app->request->get('message')=='false'):
								if(Yii::$app->session['rent_cansel']):
									Yii::$app->session['rent_cansel']=null;
                                    echo '<script> $(document).ready(function(){ swal("You cancel payment service!", "", "warning")});</script>';
								endif;
								if(Yii::$app->session['rent_ok']):
                                    $rent_r = \app\modules\posts\models\Rentreserve::findOne(Yii::$app->session['rent_ok']);
                                    if ($rent_r){
                                        $from_r = date('d.m.Y',strtotime($rent_r->reserve_from));
                                        $to_r = date('d.m.Y',strtotime($rent_r->reserve_to));
                                      echo '<script> $(document).ready(function(){ swal("Thank you! Your place is booked from '.$from_r.' to '.$to_r.'!", "", "success")});</script>';
                                    } else
                                      echo '<script> $(document).ready(function(){ swal("Thank you! Your place is booked!", "", "success")});</script>';
                                  Yii::$app->session['rent_ok']=null;
								endif;
								if(Yii::$app->session->setFlash('reserve_request')):
									echo Alert::widget([
											'options' => [
													'class' => 'alert-info',
											],
											'body' => \Yii::t('app','reserve_request!'),
									]);
								endif;
								if(Yii::$app->session['not_credits']):
									Yii::$app->session['not_credits']=null;
								    echo '<script> $(document).ready(function(){ swal("Not credits!", "", "warning")});</script>';
								endif;
								?>
								<?php $arrayYesNo = [0 => 'No', 1 => 'Yes']; ?>
								<?php $price_for_array = [1 => 'day', 2 => 'month', 3 => 'year'];

								?>

								<?php foreach($modelRents as $rent){ ?>
									<div class="rent-thing rentBlockInfo" data-rent_id="<?= $rent['id']; ?>" data-position_lat="<?= $rent['position_lat']; ?>" data-position_lng="<?= $rent['position_lng']; ?>">
                                        <div class="rent-thing-image">
											<?php if($rent['img_src'] == ''){ ?>

												<img class="img-responsive" src="<?= Url::home().'images/bg.jpg'; ?>">
											<?php }else{ ?>
												<img class="img-responsive" src="<?= Url::home().'images/posts_images/'.$rent['img_src']; ?>">
											<?php } ?>
										</div>
										<div class="" style="padding-top:10px">
											<div class="info">
												<h4 class="post-name"><?=$rent['title']?></h4>
											</div>
											<a class="btn btn-view" id="view" data-renthouse_id="<?=$rent['id']?>" >Book</a>

											<div class="total-rating ">
												<?= \app\widgets\rank\RankrentsWidget::widget(['rent_id' => $rent['id'], 'type' => 'rent_house']); ?>
											</div>

											<div class="" style="float: right">
												<div class="info star_style" style="position: relative; bottom: 4px;">
													<?= RatingrentalWidget::widget(['rent_id' => $rent['id'], 'type' => 'rent_house']); ?>
												</div>
											</div>
											<div class="views-count ">
												<?=$rent['views']?> Views
											</div>
										</div>
										<div class="centerrer-block">
											<div class="info">
												<div class="clearfix"></div>
												<p class="content-thing"><?= mb_substr($rent['content'], 0, 106, "UTF8").'...'; ?></p>
											</div>
											<div class="bottom-row style-bottom-row-cond">
												<!--<p class="booking">
													Booked <?=\app\modules\posts\models\Rentreserve::find()->where(['rent_id'=>$rent['id'],'rent_type'=>'rents'])->count()?>
												</p>-->
												<div class="conditions">
													<span>
														<?php
															if($rent['wifi'] == '0'){
														?>
																<img class="img-responsive" src="<?= Url::home().'images/icon/wi-fi-ic2.png'; ?>">
														<?php
															} else {
														?>
																<img class="img-responsive" src="<?= Url::home().'images/icon/wi-fi-ic.png'; ?>">
														<?php
															}
														?>
													</span>
													<span>
														<?php
															if($rent['parking'] == '0'){
														?>
																<img class="img-responsive" src="<?= Url::home().'images/icon/parking2.png'; ?>">
														<?php
															} else {
														?>
																<img class="img-responsive" src="<?= Url::home().'images/icon/parking.png'; ?>">
														<?php
															}
														?>
													</span>
													<span>
														<?php
															if($rent['smoking'] == '0'){
														?>
																<img class="img-responsive" src="<?= Url::home().'images/icon/smoking2.png'; ?>">
														<?php
															} else {
														?>
																<img class="img-responsive" src="<?= Url::home().'images/icon/smoking.png'; ?>">
														<?php
															}
														?>
													</span>
													<span>
														<?php
															if($rent['animal'] == '0'){
														?>
																<img class="img-responsive" src="<?= Url::home().'images/icon/animal2.png'; ?>">
														<?php
															} else {
														?>
																<img class="img-responsive" src="<?= Url::home().'images/icon/animal.png'; ?>">
														<?php
															}
														?>
													</span>
													<span>
														<?php
															if($rent['conditioner'] == '0'){
														?>
																<img class="img-responsive" src="<?= Url::home().'images/icon/cond2.png'; ?>">
														<?php
															} else {
														?>
																<img class="img-responsive" src="<?= Url::home().'images/icon/cond.png'; ?>">
														<?php
															}
														?>
													</span>
												</div>
                                                <h4 class="address style-addres-text"><?= $rent['address']; ?></h4>
											</div>
										</div>

										<div class="bottom-row">
											<div class="conditions mustyle_class">
                                                <div style="position: relative; top: 135px; padding-right: 22px;">
                                                    <p class="booking col-xs-6">Booked <?=\app\modules\posts\models\Rentreserve::find()->where(['rent_id'=>$rent['id'],'rent_type'=>'rent_house'])->count()?></p>
                                                    <?= FavoriterenthouseWidget::widget(['renthouse_id' => $rent['id']]) ?>
                                                </div>
												<div class="price-rent">
<!--													<span class="price">--><?php //echo ($day) ? $rent['price']*$day.'$' : '' ?><!--</span>-->
													<span class="price" style="font-size: 16px">
                                                        <?php if ($day) {?>
                                                          <?php echo ($rent['price']>0) ? 'Price: '.$rent['price']*$day.' $' : '' ?> <br/>
                                                          <?php echo ($rent['credits']>0) ? 'Credits: '.$rent['credits']*$day.'' : '<br/>' ?>
                                                        <?php } else {?>
                                                          <?php echo ($rent['price']>0) ? 'Price: '.$rent['price'].' $ / Day ' : '' ?> <br/>
                                                          <?php echo ($rent['credits']>0) ? 'Credits: '.$rent['credits'].'  / Day ' : '<br/>' ?>
                                                        <?php } ?>
                                                    </span>
												</div>

											</div>
										</div>



									</div>
									<?php // var_dump($rent); ?>
								<?php } ?>
							</div>
						</div>
						<div id="grid-content" class="tab-pane fade">
							<div class="gallery-wrap">
								<div class="row">
									<?php foreach($modelRents as $rent){ ?>
										<div class="col-md-4">
											<div class="grid-item">
												<div class="gallery-mask">
													<div class="top-info">
														<div class="house-name"><?=$rent['title']?></div>
														<?= RatingrentalWidget::widget(['rent_id' => $rent['id'], 'type' => 'rent_house']); ?>
														<div class="clearfix"></div>
													</div>
													<div class="house-address"><?= $rent['address']; ?></div>
													<div class="info-text">
														<?= mb_substr($rent['content'], 0, 106, "UTF8").'...'; ?>
													</div>
													<a href="#" class="add-favorite">Add to favorite <i class="fa fa-star" aria-hidden="true"></i></a>
													<a class="btn-view"  id="view" data-renthouse_id="<?=$rent['id']?>" >View</a>
												</div>
												<div class="gallery-img">
													<?php if($rent['img_src'] == ''){ ?>
														<img class="img-responsive" src="<?= Url::home().'images/bg.jpg'; ?>">
													<?php }else{ ?>
														<img class="img-responsive" src="<?= Url::home().'images/posts_images/'.$rent['img_src']; ?>">
													<?php } ?>
												</div>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php foreach($modelRents as $rent){ ?>

	<div class="modalRightPagesLS" id="view_rentshouse_<?=$rent['id']?>">
		<?php
		$query = new \yii\db\Query();
		$query->select(['*','id' => '{{%user}}.id'])
				->from('{{%user}}')
				->leftJoin('{{%user_info}}', '{{%user_info}}.id_user = {{%user}}.id')
				->where(['{{%user}}.id' => $rent['user_id']]);

		$command = $query->createCommand();
		$userPosted = $command->queryOne();
		?>
                <a class="btn btn-close pull-right closeModal" data-rent_id="<?= $rent['id']; ?>" >
			<span class="" aria-hidden="true">x</span>
		</a>
		<div class="user-info">
			<div class="class-user-av">
				<?php if ($userPosted['avatar']){?>
					<img class="img-responsive" src="<?= Url::home(); ?>images/users_images/<?= $userPosted['avatar']; ?>">
				<?php } else {?>
					<img class="img-responsive" src="<?= Url::home(); ?>images/default_avatar.jpg">
				<?php } ?>
			</div>
			<span class="user-name">
				<?= $userPosted['name']; ?> <?= $userPosted['surname']; ?>
			</span>
			<div class="right-block">
				<?= \app\widgets\rating\RatingrentaluserWidget::widget(['object_id' => $userPosted['id'], 'type' => 'rental_a_house']); ?>
				<span class="price"><?= ($day) ? $rent['price']*$day.'$' : '' ?></span>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="panel-content">

			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#description<?=$rent['id']?>" aria-controls="description<?=$rent['id']?>" role="tab" data-toggle="tab">Description</a></li>
				<li role="presentation">
                                    <a href="#map<?=$rent['id']?>" data-position_lat="<?=$rent['position_lat']?>" data-position_lng="<?=$rent['position_lng']?>"  class="rent_tab_show_map"  data-rent_id="<?=$rent['id']?>" aria-controls="map<?=$rent['id']?>" role="tab" data-toggle="tab">Map</a>
                                </li>
				<li role="presentation"><a href="#images<?=$rent['id']?>" aria-controls="images<?=$rent['id']?>" role="tab" data-toggle="tab">Images</a></li>
				<li role="presentation"><a href="#calendar<?=$rent['id']?>" aria-controls="calendar<?=$rent['id']?>" role="tab" data-toggle="tab" onmousemove="$('#calendar_w_<?=$rent['id']?>').fullCalendar('today');">Calendar</a></li>
				<li role="presentation"><a href="#comments<?=$rent['id']?>" aria-controls="comments<?=$rent['id']?>" role="tab" data-toggle="tab">Reviews</a></li>
				<li role="presentation"><?php if(Yii::$app->request->get('reserve_from') and Yii::$app->request->get('reserve_to')) {?>
						<!--<button type="button" class="btn" id="book" data-renthouse_id="--><?//=$rent['id']?><!--" data-renthouse_price_d="--><?//=$rent['price']?><!--" data-renthouse_price_c="--><?//=$rent['credits']?><!--"> Book </button>-->
						<a class="book-tab" href="#book<?=$rent['id']?>" aria-controls="book<?=$rent['id']?>" role="tab" data-toggle="tab">Book</a>
					<?php }?></li>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="description<?=$rent['id']?>">
					<div class="rent-thing">
						<div class="info">
							<h4 class="address"><?= $rent['address']; ?></h4>
							<p class="content-thing"><?=$rent['content'] ?></p>
						</div>
					</div>
				</div>
				<div role="tabpanel" class="tab-pane" id="map<?=$rent['id']?>">
                                    <div id="map_modal<?=$rent['id']?>" class="map">
                                    </div>
                                </div>
				<div role="tabpanel" class="tab-pane" id="images<?=$rent['id']?>">
					<?php
					$images = \app\modules\posts\models\Postsphotos::find()->where(['post_id'=>$rent['id']])->all();
					foreach($images as $image){
						echo '<img src="/images/posts_images/'.$image->image_src.'" style="max-width: 420px" />';
					}
					?>
				</div>
				<div role="tabpanel" class="tab-pane" id="calendar<?=$rent['id']?>">
					<div id="calen_id_<?=$rent['id']?>">
						<?php
						$events = [];
						$renteds = \app\modules\posts\models\Rentreserve::find()->where(['rent_type'=>'rent_house', 'rent_id'=>$rent['id']])->all();
						foreach($renteds as $rented ){
							$event        = new \yii2fullcalendar\models\Event();
							$event->id    = $rented->id;
							$event->title = 'Rented';
							$event->start = date('Y-m-d\TH:i:s\Z', strtotime($rented->reserve_from));
							$event->end = date('Y-m-d\TH:i:s\Z', strtotime($rented->reserve_to));
							$events[] = $event;
						}
						/*echo \yii2fullcalendar\yii2fullcalendar::widget(array(
                               'clientOptions' => [
//																'eventLimit'=> true,
//																'fixedWeekCount'=>false,
//															'defaultDate' => date('Y-m-d\TH:i:s\Z'),
                                   'nowIndicator'=>false,
                                       'defaultDate' => date('Y-m-d',time())
                               ],
                               'events'=> $events,
                       ));*/
						echo \yii2fullcalendar\yii2fullcalendar::widget([
								'id' => 'calendar_w_'.$rent['id'],
								'clientOptions' => [
										'getDate' => date('Y-m-d',time()),
										'defaultDate' => date('Y-m-d',time()),
										'today'=>true,
									//															'dayClick'=>new \yii\web\JsExpression('function (cellInfo, jsEvent) {console.log(cellInfo);}')
								],
								'events'=> $events,
						]);

						?>
					</div>
				</div>
				<div role="tabpanel" class="tab-pane style-tb-pane-hr" id="comments<?=$rent['id']?>">
					<?php
					foreach($renteds as $rented){
						if ($rented->rating_comment)
							echo $rented->user['name']."<br/>".$rented->rating_comment."<br/><br/><hr><br/>";
					}
					?>
				</div>
				<div role="tabpanel" class="tab-pane" id="book<?=$rent['id']?>">
					<?php $form = ActiveForm::begin(); ?>
					<input type="hidden" name="rent_id" id="form_rent_id" value="<?=$rent['id']?>">
					<input type="hidden" name="price_d" id="price_d" value="<?=$rent['price']?>">
					<input type="hidden" name="price_c" id="price_c" value="<?=$rent['credits']?>">
					<input type="hidden" name="day" id="day" value="<?=$day?>">
					<input type="hidden" name="form_date_for" id="form_date_for" value="<?=Yii::$app->request->get('reserve_from')?>">
					<input type="hidden" name="form_date_to" id="form_date_to" value="<?=Yii::$app->request->get('reserve_to')?>">
					<!--<label class="control-label">From</label>
							<?php
					/*				echo \kartik\date\DatePicker::widget([
                                            'name' => 'reserve_from',
                                            'value' => date('Y-m-d'),
                                            'options' => ['placeholder' => 'Select issue date ...','required' => 'true', 'id'=>'form_date_for'],
                                            'pluginOptions' => [
                                                    'autoclose'=>true,
                                                    'format' => 'yyyy-mm-dd',
                                                    'todayHighlight' => true
                                            ]
                                    ]);
                                    */?>
															<label class="control-label">To</label>
															--><?php
					/*				echo \kartik\date\DatePicker::widget([
                                            'name' => 'reserve_to',
                                            'value' => date('Y-m-d', strtotime('+1 days')),
                                            'options' => ['placeholder' => 'Select issue date ...','required' => 'true', 'id'=>'form_date_to'],
                                            'pluginOptions' => [
                                                    'autoclose'=>true,
                                                    'format' => 'yyyy-mm-dd',
                                                    'todayHighlight' => true
                                            ]
                                    ]);
                                    */?>

					<div class="book-table">
						<div>Rent date for: <span id="rent_day"><?=Yii::$app->request->get('reserve_from')?></span></div>
						<div>Rent date to: <span id="rent_day"><?=Yii::$app->request->get('reserve_to')?></span></div>
						<div>Rent day: <span id="rent_day"><?=$day?></span></div>
                      <?php if ($rent['price']>0) {?>
						<div>Total price: <span id="form_price_rent_d"><?=$rent['price']*$day?> $</span></div>
                      <?php }?>
						<?php if ($rent['credits']>0) {?>
							<div id="credits_show">Total credits: <span id="form_price_rent_c"><?=$rent['credits']*$day?></span></div>
						<?php }?>
					</div>
					<?php if ($rent['instant_pay']==0){?>
                      <div class="row">
                        <?php if ($rent['price']>0) {?>
                          <?= Html::submitButton( Yii::t('app', 'Pay with Paypal'), ['name'=> 'reserve_paypal', 'class' => 'btn btn-pay']) ?>
                        <?php }?>
                        <?php if ($rent['credits']) echo  Html::submitButton( Yii::t('app', 'Pay credits'), ['name'=> 'reserve_credits', 'id'=>'credits_show_button', 'class' => 'btn btn-pay']); ?>
                        <?php } else {?>
                          <?= Html::submitButton( Yii::t('app', 'Request'), ['name'=> 'reserve_request', 'class' => 'btn btn-pay']) ?>
                        <?php }?>
                      </div>
					<?php $form = ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
<?php }?>
