<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use app\widgets\FavoriterenthouseWidget;
use app\assets\RentshouseprofileAsset;
RentshouseprofileAsset::register($this);

use app\assets\RentshousefavotiteAsset;
RentshousefavotiteAsset::register($this);

$this->registerJsFile('/js/rentshouse.js');
$this->registerJsFile('/js/price_valid.js');
?>

<div style="display:none;">
	<div class="table table-striped previewTemplateQ" >
		<div class="row">
			<div id="template" class="file-row">
				<div class="col-sm-3">
					<span class="preview"><img data-dz-thumbnail /></span>
				</div>
				<div class="col-sm-3">
				  <button data-dz-remove class="btn btn-danger delete">
					<i class="glyphicon glyphicon-trash"></i>
					<span>Delete</span>
				  </button>
				</div>
			</div>
		</div>
	</div>
</div>
<div style="display:none;">
	<div class="table table-striped previewTemplateC" >
		<div class="row">
			<div id="template" class="file-row">
				<div class="col-sm-3">
					<span class="preview"><img data-dz-thumbnail /></span>
				</div>
				<div class="col-sm-3">
				  <button data-dz-remove class="btn btn-danger delete">
					<i class="glyphicon glyphicon-trash"></i>
					<span>Delete</span>
				  </button>
				</div>
			</div>
		</div>
	</div>
</div>

	<div class="row">
		<?= $this->render('right_menu'); ?>
		<div class="rent-content">
			<div class="container-fluid background-block">
				<div class="row">
					<?php $form = ActiveForm::begin([
									'method' => 'get',
									'action' => Url::home().'rentshouse',
									'options' => ['class' => 'top-search-form']
							]); ?>
						<input type="text" name="rent_value" class="form-control searchInput" placeholder="search rents a house by title or keywords">
					<?php $form = ActiveForm::end(); ?>
				</div>

				<div class="rents-tab-wrap">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs navigation-tabs no-icons" role="tablist">
						<li role="presentation" class="active"><a href="#rent" aria-controls="rent" role="tab" data-toggle="tab">Rent</a></li>
						<li role="presentation"><a href="#request" aria-controls="request" role="tab" data-toggle="tab">Request <span class="badge"><?=\app\modules\posts\models\Rentreserve::find()->where(['user_id'=>Yii::$app->user->id,'rent_type' => 'rent_house','status'=>0,'request'=>1])->count()?></span></a></li>
					</ul>
					<button type="button" class="btn btn-add-rent" data-toggle="modal" data-target="#addRent">Add rent</button>
					<div id="addRent" class="modal padding-body fade" role="dialog">
						<div class="modal-dialog" >
						<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">x</button>
									<h4 class="modal-title">Add rent</h4>
								</div>
								<div class="modal-body">
                                                                  <?php $form = ActiveForm::begin(['id' => 'formAddrenthouse']); ?>
									<?= $form->field($modelNewRenthouse, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false); ?>
									<?= $form->field($modelNewRenthouse, 'title')->textInput(); ?>
									<?= $form->field($modelNewRenthouse, 'content')->textarea(); ?>
									<?= $form->field($modelNewRenthouse, 'house_type')->dropDownList(['Private'=>'Private','Shared'=>'Shared'], ['id'=>'house_type']) ?>
									<div id="room_count_st" style="display: none">
										<?= $form->field($modelNewRenthouse, 'room_count')->textInput(['type' => 'number','value'=>1]); ?>
									</div>
									<?= $form->field($modelNewRenthouse, 'instant_pay')->dropDownList(['0'=>'Instant Pay','1'=>'Request'])->label('Type pay'); ?>

									<?= $form->field($modelNewRenthouse, 'price')->textInput(['type' => 'number','min'=>0, 'required'=>true]); ?>
									<?= $form->field($modelNewRenthouse, 'credits')->textInput(['type' => 'number','min'=>0, 'required'=>true]); ?>
									<?= $form->field($modelNewRenthouse, 'price_for')->dropDownList(['1' => 'day', '7' => 'week', '30' => 'month' ])->label('Min. rent'); ?>
									<div class="col-sm-12">
										<div class="row">
											<a class="add_header_photo btn btn-action"> Add header photo</a>
											<div class="show_header_photo"></div>
										</div>
									</div>
									<div style="clear:both;"></div>
									<?= $form->field($modelNewRenthouse, 'wifi')->checkBox(); ?>
									<?= $form->field($modelNewRenthouse, 'parking')->checkBox(); ?>
									<?= $form->field($modelNewRenthouse, 'smoking')->checkBox(); ?>
									<?= $form->field($modelNewRenthouse, 'tv')->checkBox(); ?>
									<?= $form->field($modelNewRenthouse, 'animal')->checkBox(); ?>
									<?= $form->field($modelNewRenthouse, 'conditioner')->checkBox(); ?>
									<?= $form->field($modelNewRenthouse, 'adults')->dropDownList(['1' => '1','2' => '2','3' => '3','4' => '4','5' => '5+']); ?>
									<div action="../../users/profile/savedropedfile" style="border:2px dashed #0087F7" class="user_photos">
										<div class="dz-message needsclick">
											<h3 style="color:#0087F7;">Drop files here or click to upload.</h3>
										</div>
									</div>
									<?= $form->field($modelNewRenthouse, 'img_src')->hiddenInput()->label(false); ?>
									<?= $form->field($modelNewRenthouse, 'position_lat')->hiddenInput(['id' => 'location_lat'])->label(false); ?>
									<?= $form->field($modelNewRenthouse, 'position_lng')->hiddenInput(['id' => 'location_lng'])->label(false); ?>
									<?= $form->field($modelNewRenthouse, 'address')->hiddenInput(['id' => 'rentahouse_address'])->label(false); ?>
										<div id="inputPhotos">
										</div>
										<?= Html::submitButton( Yii::t('app', ''), ['name'=> 'add_new_rents', 'style' => 'border:0px;height:0px;margin:0px;padding:0px;']) ?>
                                                      <?php $form = ActiveForm::end(); ?>

									<input type="text" class="enter_your_location form-control" id="editRentAddress" name="" placeholder="enter address">
									<div id="map" class="map"></div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
									<?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'add_new_rents_2', 'class' => 'btn btn-action']) ?>
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php
				if(Yii::$app->session->hasFlash('Rentadded')):
					echo Alert::widget([
						'options' => [
							'class' => 'alert-info',
						],
						'body' => \Yii::t('app','Rent a house added!'),
					]);
				endif;

				if(Yii::$app->session->hasFlash('Rentnotadded')):
					echo Alert::widget([
						'options' => [
							'class' => 'alert-warning',
						],
						'body' => \Yii::t('app','Rent a house not added!'),
					]);
				endif;


				if(Yii::$app->session->hasFlash('Rentupdate')):
					echo Alert::widget([
						'options' => [
							'class' => 'alert-info',
						],
						'body' => \Yii::t('app','Rent a house updated!'),
					]);
				endif;

				if(Yii::$app->session->hasFlash('Rentnotupdate')):
					echo Alert::widget([
						'options' => [
							'class' => 'alert-warning',
						],
						'body' => \Yii::t('app','Rent a house not updated!'),
					]);
				endif;

				if(Yii::$app->session->hasFlash('Paypal')):
					echo Alert::widget([
						'options' => [
							'class' => 'alert-warning',
						],
						'body' => \Yii::t('app','To create a renting object to fill data Paypal!'),
					]);
				endif;
				?>
				<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="rent">
						<div class="rent-thing-wrap">
							<?php $arrayYesNo = [0 => 'No', 1 => 'Yes']; ?>
							<?php $price_for_array = [1 => 'day', 2 => 'month', 3 => 'year'];?>
							<?php foreach($myRent as $rent){ ?>
								<div class="rent-thing rentBlockInfo" data-rent_id="<?= $rent['id']; ?>" data-position_lat="<?= $rent['position_lat']; ?>" data-position_lng="<?= $rent['position_lng']; ?>">
									<div class="rent-thing-image">
										<?php if($rent['img_src'] == ''){ ?>
											<img class="img-responsive" src="<?= Url::home().'images/bg.jpg'; ?>">
										<?php }else{ ?>
											<img class="img-responsive" src="<?= Url::home().'images/posts_images/'.$rent['img_src']; ?>">
										<?php } ?>
									</div>


									<div class="" style="padding-top:10px">
										<div class="info">
											<h4 class="post-name"><?=$rent['title']?></h4>
										</div>
                                        <a style="height: 30px;font-size: 13px; margin-top: -10px; padding: 0 20px;border-radius: 0;" class="btn btn-danger pull-right" id="delete" data-rent_id="<?=$rent['id']?>" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>

                                        <a class="btn btn-view" id="view" data-renthouse_id="<?=$rent['id']?>" >View</a>
                                        <button type="button" class="editRent" data-rent_id="<?= $rent['id']; ?>">Edit rent</button>

										<div class="total-rating ">
                                          <?= \app\widgets\rank\RankrentsWidget::widget(['rent_id' => $rent['id'], 'type' => 'rent_house']); ?>
										</div>
										<div class="" style="float: right">
											<div class="info star_style" style="position: relative; bottom: 4px;">
												<?= \app\widgets\rating\RatingrentalWidget::widget(['rent_id' => $rent['id'], 'type' => 'rent_house']); ?>

											</div>
										</div>
										<div class="views-count ">
											<?=$rent['views']?> Views
										</div>
									</div>


									<div class="centerrer-block" style="width: 60%">
										<div class="info">
											<div class="clearfix"></div>
											<h4 class="address"><?= $rent['address']; ?></h4>
											<p class="content-thing"><?= mb_substr($rent['content'], 0, 106, "UTF8").'...'; ?></p>
										</div>
										<div class="bottom-row">
											<div class="conditions" style="padding-right: 3%">
												<span>
													<?php
													if($rent['wifi'] == '0'){
														?>
														<img class="img-responsive" src="<?= Url::home().'images/icon/wi-fi-ic2.png'; ?>">
														<?php
													} else {
														?>
														<img class="img-responsive" src="<?= Url::home().'images/icon/wi-fi-ic.png'; ?>">
														<?php
													}
													?>
												</span>
												<span>
													<?php
													if($rent['parking'] == '0'){
														?>
														<img class="img-responsive" src="<?= Url::home().'images/icon/parking2.png'; ?>">
														<?php
													} else {
														?>
														<img class="img-responsive" src="<?= Url::home().'images/icon/parking.png'; ?>">
														<?php
													}
													?>
												</span>
												<span>
													<?php
													if($rent['smoking'] == '0'){
														?>
														<img class="img-responsive" src="<?= Url::home().'images/icon/smoking2.png'; ?>">
														<?php
													} else {
														?>
														<img class="img-responsive" src="<?= Url::home().'images/icon/smoking.png'; ?>">
														<?php
													}
													?>
												</span>
												<span>
													<?php
													if($rent['animal'] == '0'){
														?>
														<img class="img-responsive" src="<?= Url::home().'images/icon/animal2.png'; ?>">
														<?php
													} else {
														?>
														<img class="img-responsive" src="<?= Url::home().'images/icon/animal.png'; ?>">
														<?php
													}
													?>
												</span>
												<span>
													<?php
													if($rent['conditioner'] == '0'){
														?>
														<img class="img-responsive" src="<?= Url::home().'images/icon/cond2.png'; ?>">
														<?php
													} else {
														?>
														<img class="img-responsive" src="<?= Url::home().'images/icon/cond.png'; ?>">
														<?php
													}
													?>
												</span>
											</div>
										</div>
									</div>


									<div class="bottom-row">
										<div class="conditions mustyle_class">
                                            <div style="position: relative; top: 135px; padding-right: 22px;">
                                                <p class="booking col-xs-6">Booked <?=\app\modules\posts\models\Rentreserve::find()->where(['rent_id'=>$rent['id'],'rent_type'=>'rent_house'])->count()?></p>
                                                <?= FavoriterenthouseWidget::widget(['renthouse_id' => $rent['id']]) ?>
                                            </div>
											<div class="price-rent">
                                                                                            <span class="price" style="font-size: 16px">
												<?php echo ($rent['price']>0) ? 'Price: '.$rent['price'].' $ / Day ' : '' ?> <br/>
                                                      <?php echo ($rent['credits']>0) ? 'Credits: '.$rent['credits'].'  / Day ' : '<br/>' ?></span>
											</div>

										</div>
									</div>

								</div>
								<div class="modalRightPagesLS" id="view_rentshouse_<?=$rent['id']?>">
									<?php
									$query = new \yii\db\Query();
									$query->select(['*','id' => '{{%user}}.id'])
											->from('{{%user}}')
											->leftJoin('{{%user_info}}', '{{%user_info}}.id_user = {{%user}}.id')
											->where(['{{%user}}.id' => $rent['user_id']]);

									$command = $query->createCommand();
									$userPosted = $command->queryOne();
									?>
									<a class="btn btn-close pull-right" onclick="$('#view_rentshouse_<?=$rent['id']?>').hide()">
										<span class="" aria-hidden="true">x</span>
									</a>
									<div class="user-info">
										<div class="class-user-av">
											<?php if ($userPosted['avatar']){?>
												<img class="img-responsive" src="<?= Url::home(); ?>images/users_images/<?= $userPosted['avatar']; ?>">
											<?php } else {?>
												<img class="img-responsive" src="<?= Url::home(); ?>images/default_avatar.jpg">
											<?php } ?>
										</div>
										<span class="user-name">
											<?= $userPosted['name']; ?> <?= $userPosted['surname']; ?>
										</span>
										<div class="right-block">
											<?= \app\widgets\rating\RatingrentaluserWidget::widget(['object_id' => $userPosted['id'], 'type' => 'rental_a_house']); ?>
											<span class="price"><?= $rent['price']?>$/day</span>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="panel-content">

										<!-- Nav tabs -->
										<ul class="nav nav-tabs" role="tablist">
											<li role="presentation" class="active"><a href="#description<?=$rent['id']?>" aria-controls="description<?=$rent['id']?>" role="tab" data-toggle="tab">Description</a></li>
											
                                                                                        <li role="presentation">
                                                                                            <a href="#map<?=$rent['id']?>" data-position_lat="<?=$rent['position_lat']?>" data-position_lng="<?=$rent['position_lng']?>"  class="rent_tab_show_map"  data-rent_id="<?=$rent['id']?>" aria-controls="map<?=$rent['id']?>" role="tab" data-toggle="tab">Map</a>
                                                                                        </li>
                                                                                        
                                                                                        <li role="presentation"><a href="#images<?=$rent['id']?>" aria-controls="images<?=$rent['id']?>" role="tab" data-toggle="tab">Images</a></li>
											<li role="presentation"><a href="#calendar<?=$rent['id']?>" aria-controls="calendar<?=$rent['id']?>" role="tab" data-toggle="tab" onmousemove="$('#calendar_w_<?=$rent['id']?>').fullCalendar('today');">Calendar</a></li>
											<li role="presentation"><a href="#comments<?=$rent['id']?>" aria-controls="comments<?=$rent['id']?>" role="tab" data-toggle="tab">Reviews</a></li>
											<li role="presentation"><a href="#rented<?=$rent['id']?>" aria-controls="rented<?=$rent['id']?>" role="tab" data-toggle="tab">Rented</a></li>

										</ul>
										<!-- Tab panes -->
										<div class="tab-content">
											<div role="tabpanel" class="tab-pane active" id="description<?=$rent['id']?>">
												<div class="info">
													<h4 class="address"><?= $rent['address']; ?></h4>
													<p class="content-thing"><?=$rent['content'] ?></p>
												</div>
											</div>
											<div role="tabpanel" class="tab-pane" id="map<?=$rent['id']?>">
                                                                                            <div id="map_modal<?=$rent['id']?>" class="map">
                                                                                            </div>
                                                                                        </div>
											<div role="tabpanel" class="tab-pane" id="images<?=$rent['id']?>">
												<?php
												$images = \app\modules\posts\models\Postsphotos::find()->where(['post_id'=>$rent['id']])->all();
												foreach($images as $image){
													echo '<img src="/images/users_images/'.$image->image_src.'" />';
												}
												?>
											</div>
											<div role="tabpanel" class="tab-pane" id="calendar<?=$rent['id']?>">
												<div id="calen_id_<?=$rent['id']?>">
													<?php
													$events = [];
													$renteds = \app\modules\posts\models\Rentreserve::find()->where(['rent_type'=>'rent_house', 'rent_id'=>$rent['id']])->all();
													foreach($renteds as $rented ){
														$event        = new \yii2fullcalendar\models\Event();
														$event->id    = $rented->id;
														$event->title = 'Rented';
														$event->start = date('Y-m-d\TH:i:s\Z', strtotime($rented->reserve_from));
														$event->end = date('Y-m-d\TH:i:s\Z', strtotime($rented->reserve_to));
														$events[] = $event;
													}
													/*echo \yii2fullcalendar\yii2fullcalendar::widget(array(
                                                            'clientOptions' => [
            //															'eventLimit'=> true,
            //															'fixedWeekCount'=>false,
            //														'defaultDate' => date('Y-m-d\TH:i:s\Z'),
                                                                'nowIndicator'=>false,
                                                                    'defaultDate' => date('Y-m-d',time())
                                                            ],
                                                            'events'=> $events,
                                                    ));*/
													echo \yii2fullcalendar\yii2fullcalendar::widget([
															'id' => 'calendar_w_'.$rent['id'],
															'clientOptions' => [
																	'getDate' => date('Y-m-d',time()),
																	'defaultDate' => date('Y-m-d',time()),
																	'today'=>true,
//														'dayClick'=>new \yii\web\JsExpression('function (cellInfo, jsEvent) {console.log(cellInfo);}')
															],
															'events'=> $events,
													]);

													?>
												</div>
											</div>
											<div role="tabpanel" class="tab-pane style-tb-pane-hr" id="comments<?=$rent['id']?>">
												<?php
												foreach($renteds as $rented){
													if ($rented->rating_comment)
														echo $rented->user['name']."<br/>".$rented->rating_comment."<br/><br/><hr><br/>";
												}
												?>
											</div>
											<div role="tabpanel" class="tab-pane" id="rented<?=$rent['id']?>">
												<table class="table table-striped">
													<thead>
													<th>User name</th>
													<th>Date for</th>
													<th>Date to</th>
													<th>Day</th>
													<th>Price</th>
													<th>Credits</th>
													</thead>
													<tbody>
													<?php
													$rent_ser = \app\modules\posts\models\Rentreserve::find()->where(['rent_id'=>$rent['id'], 'rent_type'=>'rent_house'])->all();
													foreach($rent_ser as $item){
														?>
														<tr>
															<td><?=$item->user['name'].' '.$item->user['surname']?></td>
															<td><?=$item->reserve_from?></td>
															<td><?=$item->reserve_to?></td>
															<td>
																<?php
																$reserve_to = strtotime($item->reserve_to);
																$reserve_from = strtotime($item->reserve_from);
																$datediff = $reserve_to - $reserve_from;

																echo floor($datediff / (60 * 60 * 24));
																?>
															</td>
															<td><?=$item->total_price?></td>
															<td><?=$item->total_credits?></td>
														</tr>
													<?php } ?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="request">
						<table class="table table-rent">
							<thead>
							<th>Client name</th>
							<th>Rent name</th>
							<th>Rent date for</th>
							<th>Rent date to</th>
							<th>Options</th>
							</thead>
							<tbody>
							<?php foreach($request as $item){?>
								<tr>
									<td><?= $item->user['name'].' '.$item->user['surname']?></td>
									<td><?= $item->renthouse['title']?></td>
									<td><?= date('m-d-Y',strtotime($item->reserve_from)) ?></td>
									<td><?=date('m-d-Y',strtotime($item->reserve_to)) ?></td>
									<td>
										<a class="btn btn-success" href="?request=2&id=<?=$item->id?>">Agree</a>
										<a class="btn btn-danger" href="?request=3&id=<?=$item->id?>">Reject</a>
<!--										<button onclick="$('.user-layer').show();">Contact</button>-->
									</td>
								</tr>
							<?php }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>


    <div id="editRent" class="editRentt modal big-modal padding-body fade" role="dialog">
            <div class="modal-dialog" style="width:70%;">
            <!-- Modal content-->
                    <div class="modal-content" style="margin-top:170px;">
                            <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">x</button>
                                    <h4 class="modal-title">Add rent</h4>
                            </div>
                                        <div class="modal-body"  style="max-height:500px;">
                                            <?php $form = ActiveForm::begin(['id' => 'formAddrenthouse', 'action' => '/posts/rentshouse/rentupdate']); ?>
                                                <?= $form->field($modelNewRenthouse, 'id')->hiddenInput()->label(false); ?>
                                                <?= $form->field($modelNewRenthouse, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false); ?>
                                                <?= $form->field($modelNewRenthouse, 'title')->textInput(); ?>
                                                <?= $form->field($modelNewRenthouse, 'content')->textarea(); ?>
                                                <?= $form->field($modelNewRenthouse, 'house_type')->dropDownList(['Private'=>'Private','Shared'=>'Shared'], ['id'=>'house_type']) ?>
                                                <div id="room_count_st" style="display: none">
                                                        <?= $form->field($modelNewRenthouse, 'room_count')->textInput(['type' => 'number','value'=>1]); ?>
                                                </div>
                                                <?= $form->field($modelNewRenthouse, 'instant_pay')->dropDownList(['0'=>'Instant Pay','1'=>'Request'])->label('Type pay'); ?>

                                                <?= $form->field($modelNewRenthouse, 'price')->textInput(['type' => 'number','min'=>0, 'required'=>true]); ?>
                                                <?= $form->field($modelNewRenthouse, 'credits')->textInput(['type' => 'number','min'=>0, 'required'=>true]); ?>
                                                <?= $form->field($modelNewRenthouse, 'price_for')->dropDownList(['1' => 'day', '7' => 'week', '30' => 'month' ])->label('Min. rent'); ?>
                                                <div class="col-sm-12">
                                                        <div class="row">
                                                                <a class="change_header_photo btn btn-action"> Change header photo</a>
                                                                <div class="show_change_header_photo"></div>
                                                        </div>
                                                </div>
                                                <div style="clear:both;"></div>
                                                <?= $form->field($modelNewRenthouse, 'wifi')->checkBox(); ?>
                                                <?= $form->field($modelNewRenthouse, 'parking')->checkBox(); ?>
                                                <?= $form->field($modelNewRenthouse, 'smoking')->checkBox(); ?>
                                                <?= $form->field($modelNewRenthouse, 'tv')->checkBox(); ?>
                                                <?= $form->field($modelNewRenthouse, 'animal')->checkBox(); ?>
                                                <?= $form->field($modelNewRenthouse, 'conditioner')->checkBox(); ?>
                                                <?= $form->field($modelNewRenthouse, 'adults')->dropDownList(['1' => '1','2' => '2','3' => '3','4' => '4','5' => '5+']); ?>
                                                <div class="previewsPhotosRenthouse row" style="clear:both;padding:10px;"></div>
                                                <div action="../../users/profile/savedropedfile" style="border:2px dashed #0087F7" class="user_photosC">
                                                        <div class="dz-message needsclick">
                                                                <h3 style="color:#0087F7;">Drop files here or click to upload.</h3>
                                                        </div>
                                                </div>
                                                <?= $form->field($modelNewRenthouse, 'img_src')->hiddenInput()->label(false); ?>
                                                <?= $form->field($modelNewRenthouse, 'position_lat')->hiddenInput(['id' => 'location_lat_c'])->label(false); ?>
                                                <?= $form->field($modelNewRenthouse, 'position_lng')->hiddenInput(['id' => 'location_lng_c'])->label(false); ?>
                                                <?= $form->field($modelNewRenthouse, 'address')->hiddenInput(['id' => 'rentahouse_address_c'])->label(false); ?>

                                                        <div id="inputPhotosC">
                                                        </div>
                                                        <?= Html::submitButton( Yii::t('app', ''), ['name'=> 'edit_new_rents', 'style' => 'border:0px;height:0px;margin:0px;padding:0px;']) ?>
                                              <?php $form = ActiveForm::end(); ?>

                                                <input type="text" class="enter_your_location_c form-control" name="Rentshouse[address]" placeholder="enter address">
                                                <div id="mapEdit" class="mapEdit" style="height:300px;width:100%;"></div>
                                        </div>
                                        <div class="modal-footer">
                                                <button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
                                                <?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'edit_new_rents_2', 'class' => 'btn btn-action']) ?>
                                        </div>

                    </div>
            </div>
    </div>