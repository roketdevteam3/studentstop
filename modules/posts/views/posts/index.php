<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use app\widgets\MarketcategorycountWidget;
?>
	<div class="row">
	<?= $this->render('right_menu'); ?>
		<div class="classified-market-content background-block">
			<div class="posts-default-index">
				<div class="" style="padding-right: 10px; padding-left: 10px;">
					<div class="row">
						<?php foreach($postCategory as $category){ ?>
							<div class="col-md-4 col-sm-6">
								<div class="classified-block">
									<h2>
										<?= $category->category_name; ?>
									</h2>
									<?php
									$k = 0;
									foreach($category->child_category as $child_category){ 
										$k = $k + 1;
										?>
										<a href="/market/<?=$category->url_name.'/'.$child_category->url_name?>">
											<?=$child_category->category_name?>
											<span class="badge">
												<?= \app\modules\posts\models\Posts::find()->where(['category_id'=>$child_category->id, 'publish_status' => 1])->count() ?>
											</span>
										</a>
									<?php } ?>
									<?php if($k == 3){ ?>
										<?= HTML::a('More',Url::home().'market/'.$category->url_name,['class' => 'more']); ?>
									<?php } ?>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
