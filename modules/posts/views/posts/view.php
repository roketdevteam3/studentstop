<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\bootstrap\Alert;

?>
	<?php
		if(Yii::$app->session->hasFlash('PostsUpdate')):
			echo Alert::widget([
				'options' => [
					'class' => 'alert-info',
				],
				'body' => \Yii::t('app','Posts updated'),
			]);
		endif;
	?>

	<div class="row">
		<?= $this->render('right_menu',['userPosted' => $userPosted, 'postViewModel' => $postViewModel]); ?>
		<div class="classified-market-content">
			<div class="container-fluid background-block">
				<div class="main-content">
					<div class="posts-default-index">
						<?php if($postViewModel['user_id'] == \Yii::$app->user->id){ ?>
							<div class="text-right">
								<button class='btn btn-udate-market-post' data-toggle="modal" data-target="#modalUpdatePosts">Update</button>
								<?= HTML::a("Delete", Url::home().'market/delete/'.$postViewModel['id'], ['class' => 'btn btn-del-mr-post']); ?>
							</div>
							<div class="modal padding-body fade" id="modalUpdatePosts" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h4 style="text-align: center">Update Posts</h4>
										</div>
										<div class="modal-body">
											<?php $form = ActiveForm::begin(); ?>
												<?= $form->field($postViewModel, 'title')->textInput(); ?>
												<?= $form->field($postViewModel, 'content')->textarea(); ?>
												<?= $form->field($postViewModel, 'address')->textInput(); ?>
												<?= $form->field($postViewModel, 'country')->textInput(); ?>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-close" data-dismiss="modal">Cancel</button>
												<?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'change_user_info', 'class' => 'btn btn-action']) ?>
											<?php ActiveForm::end();  ?>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
						<div class="market-post">
							<span class="post-market-date"><?= $postViewModel->date_create; ?></span>
							<h4 class="post-market-title">
								<?= $postViewModel->title; ?>
							</h4>
							<p class="post-market-content"><?= $postViewModel->content; ?></p>
							<div class="row">
								<div class="col-md-offset-3 col-md-6">
									<div class="post-market-image">
										<?php if($postViewModel->image_src != ''){ ?>
											<img class="img-responsive" src="<?= Url::home().'images/posts_images/'.$postViewModel->image_src; ?>">
										<?php } ?>
									</div>
								</div>
							</div>
							<div class="table-wrap">
								<table class="table">
									<tr>
										<td><b>Address</b></td>
										<td><?= $postViewModel['address']; ?></td>
									</tr>
									<tr>
										<td><b>Country</b></td>
										<td><?= $postViewModel['country']; ?></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
