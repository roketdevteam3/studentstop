<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\ActiveForm;
	use app\widgets\FiltermarketWidget;
	
	use app\assets\FilterAsset;
	FilterAsset::register($this);
	
	$allUrl = Yii::$app->request->url;
		
	if(strpos($allUrl, 'mycompany/jobs')){$class1 = 'active';}else{$class1 = '';}
	if(strpos($allUrl, 'mycompany/edit')){$class2 = 'active';}else{$class2 = '';}
	if(strpos($allUrl, 'addbusiness')){$class3 = 'active';}else{$class3 = '';}
	
?>
        <input type="hidden" name="myUserId" value="<?= \Yii::$app->user->id; ?>">
	<div class="jobs-right-panel">
		<?php if(isset($userPosted)){ ?>
			<div class="post-market-author">
				<div class="author-info">
					<img class="img-responsive post-market-author-av" src="<?= Url::home().'images/users_images/'.$userPosted['avatar']; ?>">
					<h4 class="post-market-author-name"><?php echo HTML::a($userPosted['name'].$userPosted['surname'], '/profile/'.$userPosted['id']); ?></h4>
				</div>
				<table class="table">
					<tr>
						<td>Email</td>
						<td><?= $userPosted['email']; ?></td>
					</tr>
					<tr>
						<td>Mobile</td>
						<td><?= $userPosted['mobile']; ?></td>
					</tr>
					<tr>
						<td>Address</td>
						<td><?= $userPosted['address']; ?></td>
					</tr>
				</table>
				<button class="btn contact-request" data-recipient-id="<?= $userPosted['id']; ?>" data-post-id="<?= $postViewModel->id; ?>" data-user-id="<?= \Yii::$app->user->id; ?>" >Contact request</button>
			</div>
		<?php } ?>
		<nav class="menu-right">
			<ul class="">
				<li class="<?= $class1; ?>">
					<?= HTML::a('Dashboard',Url::home().'market',['class' => ""]); ?>
				</li>
				<li class="<?= $class2; ?>">
					<?= HTML::a('My favorites',Url::home().'market/favorite',['class' => ""]);?>
				</li>
				<li class="<?= $class3; ?>">
					<?= HTML::a('My account <span class="badge">'.\app\widgets\count\PostscountWidget::widget().'</span>',Url::home().'market/myprofile',['class' => ""]); ?>
				</li>
				<li class="">
					<?= HTML::a('Add post','#',['class' => "add_post_jobs"]); ?>
				</li>
			</ul>
		</nav>
		<?= FiltermarketWidget::widget(['route' => $this->context->getRoute()]); ?>
	</div>
