<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
?>
	<div class="row">
		<?= $this->render('right_menu'); ?>
		<div class="rent-content">
			<div class="container-fluid background-block">
				<div class="posts-default-index">
					<div class="row">
						<div class="col-md-4 col-sm-6">
							<div class="classified-block">
								<h2><?= $postCategory->category_name?></h2>
								<?php foreach($postCategory->child_category as $child_category){ ?>
									<?php echo HTML::a($child_category->category_name, '/market/'.$postCategory->url_name.'/'.$child_category->url_name); ?>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
