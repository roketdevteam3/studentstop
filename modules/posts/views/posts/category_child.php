<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\widgets\Breadcrumbs;
use app\widgets\FavoritepostWidget;
use app\assets\FavoritepostAsset;
FavoritepostAsset::register($this);

?>
<div style="display:none;">
	<div class="table table-striped previewTemplateQ" >
		<div class="row">
			<div id="template" class="file-row">
				<!-- This is used as the file preview template -->
				<div class="col-sm-3">
					<span class="preview"><img data-dz-thumbnail /></span>
				</div>
				<div class="col-sm-3">
					<p class="name" data-dz-name></p>
					<strong class="error text-danger" data-dz-errormessage></strong>
				</div>
				<div class="col-sm-3">
					<p class="size" data-dz-size></p>
					<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
					  <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
					</div>
				</div>
				<div class="col-sm-3">
				  <button data-dz-remove class="btn btn-danger delete">
					<i class="glyphicon glyphicon-trash"></i>
					<span>Delete</span>
				  </button>
				</div>
			</div>
		</div>
	</div>
</div>

	<div class="row">
		<?= $this->render('right_menu'); ?>
		<div class="classified-market-content">
			<div class="container-fluid background-block">
				<div class="row">
					<div class="input-group">
                                                <form method="get">
                                                    <input type="text" class="form-control" name="posts_content" placeholder="Enter search text ...">
                                                </form>
						<span class="input-group-btn">
							<button class="btn btn-add-post btn-ripple add_post_jobs" type="button">Add Post +</button>
						</span>
					</div>
				</div>
				<?php

				if(Yii::$app->session->hasFlash('contact_request')):
					echo Alert::widget([
							'options' => [
									'class' => 'alert-info',
							],
							'body' => 'Your request has been sent to owner',
					]);
				endif;

				?>
				<ul class="nav nav-tabs view-tabs">
					<span class="choice">View</span>
					<li>
						<a data-toggle="tab" class="grid-appearance" href="#grid-content"></a>
					</li>
					<li class="active">
						<a data-toggle="tab" class="list-appearance" href="#list-content"></a>
					</li>
				</ul>
				<div class="tab-content" style="height: auto;">
					<div id="list-content" class="tab-pane fade in active">
						<div class="market-thing-wrap">
							<?php foreach($postModel as $post){ ?>
								<div class="market-thing">
									<div class="market-thing-image">
										<?php if($post['image_src'] == ''){ ?>
											<img class="img-responsive" src="<?= Url::home().'images/bg.jpg'; ?>">
										<?php }else{ ?>
											<img class="img-responsive" src="<?= Url::home().'images/posts_images/'.$post['image_src']; ?>">
										<?php } ?>
									</div>
									<div class="info">
										<h4 class="post-name" style="padding-top: 10px"><?=$post['title']?></h4>
										<?php $user = \app\modules\users\models\User::findOne($post['user_id']);?>
										<span class="by" style="    position: relative; top: 9px;">(by <?= $user->name.' '.$user->surname?>)</span>
										<p class="content-thing" style="margin-top: 50px;"><?= mb_substr($post['content'], 0, 106, "UTF8").'...'; ?></p>
										<h4 class="address"><?= $post['address']; ?></h4>
									</div>
									<div class="btn-wrap text-right sviat_wrap">
										<span class="date-post" style="float: left; padding-top: 7px;""><?=date('d/m/Y',strtotime($post['date_create']))?></span>

										<div class="views-counter sviat_container" style="padding-top: 7px;">
											(views <?=$post['views']?>)
										</div>
										<div class="mystar sviat_star"><?=\app\widgets\rating\UserratingWidget::widget(['user_id'=>$post['user_id']])?></div>

										<a style=" margin-bottom: 26px; margin-top: 11px;" class="btn btn-view " id="view" data-post_id="<?=$post['id']?>" onclick="$('#view_post_<?=$post['id']?>').show()">View</a>
										<?= FavoritepostWidget::widget(['post_id' => $post['id']]) ?><br />

									</div>

								</div>
							<?php } ?>
						</div>
					</div>
					<div id="grid-content" class="tab-pane fade">
						<div class="market-grid">
							<div class="row">
								<?php foreach($postModel as $post){ ?>
									<div class="col-md-4">
									<div class="grid-item">
										<div class="gallery-mask">
											<div class="top-info">
												<div class="house-name"><?= $post['title']; ?></div>
												<?php $user = \app\modules\users\models\User::findOne($post['user_id']);?>
                                                                                                <span class="by">(by <?= $user->name.' '.$user->surname?><div class="mystar" style="    position: relative;    bottom: 4px; padding-left: 8px;"><?=\app\widgets\rating\UserratingWidget::widget(['user_id'=>$post['user_id']])?></div>)</span>
												<div class="date-post">
													<?=date('d/m/Y',strtotime($post['date_create']))?>
												</div>
											</div>
											<div class="info-text">
												<?= mb_substr($post['content'], 0, 106, "UTF8").'...'; ?>
											</div>
											<?= FavoritepostWidget::widget(['post_id' => $post['id']]) ?>
											<a href="#" class="btn-view" id="view" data-post_id="<?=$post['id']?>" onclick="$('#view_post_<?=$post['id']?>').show()">View</a>
											<span class="view-count">
												(views <?=$post['views']?>)
											</span>
										</div>
										<div class="gallery-img">
											<?php if($post['image_src'] == ''){ ?>
												<img class="img-responsive" src="<?= Url::home().'images/bg.jpg'; ?>">
											<?php }else{ ?>
												<img class="img-responsive" src="<?= Url::home().'images/posts_images/'.$post['image_src']; ?>">
											<?php } ?>
										</div>
									</div>
								</div>
								<?php }?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php foreach($postModel as $post){ ?>
				<div class="modalRightPagesLS" id="view_post_<?=$post['id']?>">

				<a class="btn btn-close pull-right" onclick="$('#view_post_<?=$post['id']?>').hide()">
					<span class="" aria-hidden="true">x</span>
				</a>

				<h4 class="post-market-title">
					<?= $post['title']; ?>
				</h4>
				<div class="panel-content">
					<div class="market-post">
						<span class="post-market-date"><?= $post['date_create']; ?></span>
                                                <?php $form = ActiveForm::begin();?>
                                                    <input type="hidden" name="post_id" value="<?=$post['id']?>">
                                                    <button type="submit" class="btn btn-contact-request" name="contact_request">Contact request</button>
                                                <?php ActiveForm::end();?>
						<div class="row">
							<div class="col-md-offset-3 col-md-12">
								<div class="post-market-image">
									<?php if($post['image_src'] != ''){ ?>
										<img class="img-responsive" src="<?= Url::home().'images/posts_images/'.$post['image_src']; ?>">
									<?php } ?>
								</div>
							</div>
						</div>
						<div class="table-wrap">
							<table class="table">
								<tr>
									<td style="border-right: 2px solid #ddd; border-top: 2px solid #ddd;"><b>Address</b></td>
									<td style="padding-left: 10px; border-top: 2px solid #ddd;"><?= $post['address']; ?></td>
								</tr>
                                                                <tr>
									<td style="border-right: 2px solid #ddd;  border-top: 2px solid #ddd;""><b>Text</b></td>
									<td style="padding-left: 10px;  border-top: 2px solid #ddd;""><?= $post['content']; ?></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
			<?php }?>
			<?= LinkPager::widget(['pagination'=>$pagination]); ?>
		</div>
	</div>
