<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\bootstrap\Alert;

?>
<div class="posts-default-index">
    <?php
        if(Yii::$app->session->hasFlash('PostsUpdate')):
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-info',
                ],
                'body' => \Yii::t('app','Posts updated'),
            ]);
        endif;
    ?>

    <div class='col-sm-10' style="padding:0px;">
        <ul class="breadcrumb">
            <li><a href="<?= Url::home(); ?>">Home</a></li>
            <li>
                <?php echo HTML::a($parentPostCategory->category_name, '/market/'.$parentPostCategory->url_name); ?>
            </li>
            <li>
                <?php echo HTML::a($postCategory->category_name, '/market/'.$parentPostCategory->url_name.'/'.$postCategory->url_name); ?>
            </li>
            <li class="active">
                <?= $postViewModel->title; ?>
            </li>
        </ul>
        <div class='col-sm-12'>
            <?php if($postViewModel['user_id'] == \Yii::$app->user->id){ ?>
                <button class='btn btn-primary' data-toggle="modal" data-target="#modalUpdatePosts">Update</button>
                <?php echo HTML::a("Delete", '/posts/'.$parentPostCategory->url_name.'/'.$postCategory->url_name.'/delete/'.$postViewModel['id'], ['class' => 'btn btn-danger']); ?>
                <div class="modal fade" id="modalUpdatePosts" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h1 style="text-align: center">Update Posts</h1>
                        </div>
                        <div class="modal-body">
                            <?php $form = ActiveForm::begin(); ?>
                                    <?= $form->field($postViewModel, 'title')->textInput(); ?>
                                    <?= $form->field($postViewModel, 'content')->textarea(); ?>
                                    <?= $form->field($postinfoViewModel, 'address')->textInput(); ?>
                                    <?= $form->field($postinfoViewModel, 'zip_code')->textInput(); ?>
                                    <?= $form->field($postinfoViewModel, 'country')->textInput(); ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'change_user_info', 'class' => 'btn btn-primary']) ?>
                            <?php ActiveForm::end();  ?>
                        </div>
                      </div>

                    </div>
                </div>
            <?php } ?>
        </div>
        <div class='col-sm-9'>
            <h1>
                <?= $postViewModel->title; ?>
            </h1>
            <hr>
            <span><?= $postViewModel->date_create; ?></span>
            <p><?= $postViewModel->content; ?></p>
            <div class="col-sm-4">
                <?php if($postViewModel->image_src != ''){ ?>
                    <img src="<?= Url::home().'images/posts_images/'.$postViewModel->image_src; ?>" style="width:100%;">
                <?php } ?>
            </div>
            <div class="col-sm-12">
                <table class="table">
                    <tr>
                        <td><b>Address</b></td>
                        <td><?= $postinfoViewModel['address']; ?></td>
                        <td><b>Zip code</b></td>
                        <td><?= $postinfoViewModel['zip_code']; ?></td>
                    </tr>
                    <tr>
                        <td><b>Country</b></td>
                        <td><?= $postinfoViewModel['country']; ?></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class='col-sm-3' style='margin-bottom:20px;'>
            <img src="<?= Url::home().'images/users_images/'.$userPosted['avatar']; ?>" style="width:100%;min-height:150px;">
            <h3><?php echo HTML::a($userPosted['name'].$userPosted['surname'], '/profile/'.$userPosted['id']); ?></h3>
            <table class="table">
                <tr>
                    <td>Email</td>
                    <td><?= $userPosted['email']; ?></td>
                </tr>
                <tr>
                    <td>Mobile</td>
                    <td><?= $userPosted['mobile']; ?></td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td><?= $userPosted['address']; ?></td>
                </tr>
                <tr>
                    <td>Facebook link</td>
                    <td><?= $userPosted['facebook']; ?></td>
                </tr>
                <tr>
                    <td>Twitter link</td>
                    <td><?= $userPosted['twitter']; ?></td>
                </tr>
            </table>
            <button class="btn btn-primary contact-request" data-recipient-id="<?= $userPosted['id']; ?>" data-post-id="<?= $postViewModel->id; ?>" data-user-id="<?= \Yii::$app->user->id; ?>" style="width:100%;">Contact request</button>
        </div>
    </div>
    <div class="col-sm-2" style='padding: 0px;background-color:#525274;position:relative;'>
        <div class="col-sm-12" style="padding:0px;position:fixed;">
            <div class="col-sm-12" style="padding:0px;background-color:#525274;min-height:100vh">
                <?= $this->render('right_menu'); ?>
            </div>
        </div>
    </div>
</div>
