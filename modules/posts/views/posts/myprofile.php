<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use app\widgets\FavoriterenthouseWidget;
use app\widgets\PublishmarketpostWidget;
//use app\assets\PostsAsset;
//PostsAsset::register($this);

use app\assets\FilterAsset;
FilterAsset::register($this);

$this->registerJsFile('/js/ls_post.js');

?>

<div style="display:none;">
	<div class="table table-striped previewTemplateQ" >
		<div class="row">
			<div id="template" class="file-row">
				<div class="col-sm-3">
					<span class="preview"><img data-dz-thumbnail /></span>
				</div>
				<div class="col-sm-3">
				  <button data-dz-remove class="btn btn-danger delete">
					<i class="glyphicon glyphicon-trash"></i>
					<span>Delete</span>
				  </button>
				</div>
			</div>
		</div>
	</div>
</div>
	
	<div class="row">
		<?= $this->render('right_menu'); ?>
		<div class="classified-market-content">
			<div class="container-fluid background-block">
				<div class="row">
					<div class="input-group">
                        <form method="get" action="/market/community/activities">
                            <input type="text" class="form-control" name="posts_content" placeholder="Enter search text ...">
                        </form>
						<span class="input-group-btn">
							<button class="btn btn-add-post btn-ripple add_post_jobs" type="button">Add Post +</button>
						</span>
					</div>
				</div>
				<ul class="nav nav-tabs view-tabs">
					<span class="market-header">My profile</span>
					<span class="choice">View</span>
					<li>
						<a data-toggle="tab" class="grid-appearance" href="#grid-content"></a>
					</li>
					<li class="active">
						<a data-toggle="tab" class="list-appearance" href="#list-content"></a>
					</li>
				</ul>
				<div class="tab-content">
					<div id="list-content" class="tab-pane fade in active">
						<div class="market-thing-wrap">
							<div class="action-result-window">
								<?php
										if(Yii::$app->session->hasFlash('PostsAdded')):
												echo Alert::widget([
														'options' => [
																'class' => 'alert-info',
														],
														'body' => \Yii::t('app','Posts added'),
												]);
										endif;  
										if(Yii::$app->session->hasFlash('PostsDelete')):
												echo Alert::widget([
														'options' => [
																'class' => 'alert-warning',
														],
														'body' => \Yii::t('app','Posts deleted'),
												]);
										endif;  
								?>
							</div>
							<?php /* ?>
							<button class='btn btn-add-post' data-toggle="modal" data-target="#modalAddPosts">Add new posts</button>
							<div class="modal big-modal padding-body fade" id="modalAddPosts" role="dialog">
									<div class="modal-dialog">
											<div class="modal-content">
													<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal">&times;</button>
															<h4 class="modal-title">Add new posts</h4>
													</div>
													<div class="modal-body">
															<?php $form = ActiveForm::begin(); ?>
																	<?= $form->field($newPost, 'title')->textInput(); ?>
																	<?= $form->field($newPost, 'content')->textarea(['rows' => '6']); ?>
																	<span class="btn newPostPhoto">
																			<span>Upload photo</span>
																	</span>
																	<div class='post_container_photo'></div>
																	<?= $form->field($newPost, 'image_src')->hiddenInput()->label(false); ?>
																	<?= $form->field($newPost, 'category_id')->dropDownList($categoryArray); ?>
																	<?= $form->field($newPost, 'country')->hiddenInput()->label(false); ?>
																	<?= $form->field($newPost, 'address')->hiddenInput(); ?>
																	<?= Html::submitButton( Yii::t('app', 'Add post'), ['name'=> 'add_posts', 'class' => 'btn btn-add-post']) ?>
															<?php ActiveForm::end();  ?>
															<input type="text" class="form-controle enter_your_location" >
															<div id="map"></div>
															<div class="form-group">
																	<?= Html::submitButton( Yii::t('app', 'Add post'), ['name'=> 'add_posts_2', 'class' => 'btn btn-add-post']) ?>
															</div>
													</div>
											</div>
									</div>
							</div>
							<?php */ ?>
							<?php foreach($postModel as $post){ ?>
								<div class="market-thing">
									<div class="market-thing-image">
										<?php if($post['image_src'] == ''){ ?>
												<img class="img-responsive" src="<?= Url::home().'images/bg.jpg'; ?>">
										<?php }else{ ?>
												<img class="img-responsive" src="<?= Url::home().'images/posts_images/'.$post['image_src']; ?>">
										<?php } ?>
									</div>
									<div class="info" style="margin-top: 10px;">
                                        <h4 class="post-name"><?=$post['title']?></h4>
										<?php $user = \app\modules\users\models\User::findOne($post['user_id']);?>
										<span><div class="mystar" style="float: right;position: relative;"><?=\app\widgets\rating\UserratingWidget::widget(['user_id'=>$post['user_id']])?></div>)</span>
										<span class="date-post" style="width: 230px; top: 2px">
											<?=date('d/m/Y',strtotime($post['date_create']))?>
											<br>
											<a id="inquires_user" style="float: right;position: relative;bottom: 22px;margin-right: 11px;" data-inquires_post="<?=$post['id']?>">
												<?=(\app\modules\posts\models\PostContsctRequest::find()->where(['post_id'=>$post['id'],'view'=>0])->count()>0) ? 'NEW INQUIRES ('.\app\modules\posts\models\PostContsctRequest::find()->where(['post_id'=>$post['id'],'view'=>0])->count().')': 'NEW INQUIRES <k class="number_inquarest">(0)</k>'?>
											</a>
										</span>
										<p class="by">(by <?= $user->name.' '.$user->surname?></p>
										<p class="content-thing"><?= mb_substr($post['content'], 0, 106, "UTF8").'...'; ?></p>
                                        <h4 class="address"><?= $post['address']; ?></h4>
									</div>
									<div class="btn-wrap text-center" style="margin-top: 11px;">
										<?= PublishmarketpostWidget::widget(['post_id' => $post['id']]) ?>
										<a class="btn btn-view"  onclick="$('#view_post_<?=$post['id']?>').show()" data-post_id="<?= $post['id']; ?>">View</a>
										<?php //= HTML::a('View',Url::home().'market/view/'.$post['id'],['class' => 'btn btn-view']); ?>
										<?= HTML::a('Delete',Url::home().'market/delete/'.$post['id'],['class' => 'btn btn-delete-post']); ?>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
					<div id="grid-content" class="tab-pane fade">
						<div class="market-grid">
							<div class="row">
								<?php foreach($postModel as $post){ ?>
								<div class="col-md-4">
									<div class="grid-item">
										<div class="gallery-mask">
											<div class="top-info">
												<div class="house-name"><?= $post['title']; ?></div>
												<?php $user = \app\modules\users\models\User::findOne($post['user_id']);?>
                                                                                                <span class="by">(by <?= $user->name.' '.$user->surname?><div class="mystar"><?=\app\widgets\rating\UserratingWidget::widget(['user_id'=>$post['user_id']])?></div>)</span>
												<div class="date-post">
													<?=date('d/m/Y',strtotime($post['date_create']))?>
													<br>
													<a id="inquires_user" data-inquires_post="<?=$post['id']?>">
                                                      <?=(\app\modules\posts\models\PostContsctRequest::find()->where(['post_id'=>$post['id'],'view'=>0])->count()>0) ? 'NEW INQUIRES('.\app\modules\posts\models\PostContsctRequest::find()->where(['post_id'=>$post['id'],'view'=>0])->count().')': 'NEW INQUIRES(0)'?>
													</a>
												</div>
											</div>
											<div class="info-text">
												<?= mb_substr($post['content'], 0, 106, "UTF8").'...'; ?>
											</div>
											<a href="#" class="btn-view" onclick="$('#view_post_<?=$post['id']?>').show()" data-post_id="<?= $post['id']; ?>">View</a>
											<span class="view-count">
												(views <?=$post['views']?>)
											</span>

										</div>
										<div class="gallery-img">
											<?php if($post['image_src'] == ''){ ?>
												<img class="img-responsive" src="<?= Url::home().'images/bg.jpg'; ?>">
											<?php }else{ ?>
												<img class="img-responsive" src="<?= Url::home().'images/posts_images/'.$post['image_src']; ?>">
											<?php } ?>
										</div>
									</div>
								</div>
								<?php }?>
							</div>
						</div>
					</div>
				</div>
				<?php foreach($postModel as $post){ ?>
					<div class="modalRightPagesLS" id="view_post_<?=$post['id']?>">

						<a class="btn btn-close pull-right" onclick="$('#view_post_<?=$post['id']?>').hide()">
							<span class="" aria-hidden="true">x</span>
						</a>
						<h4 class="post-market-title">
							<?= $post['title']; ?>
						</h4>
						<div class="panel-content">
							<div class="market-post">
								<span class="post-market-date"><?= $post['date_create']; ?></span>
								<div class="row">
									<div class="col-md-offset-3 col-md-6">
										<div class="post-market-image">
											<?php if($post['image_src'] != ''){ ?>
												<img class="img-responsive" src="<?= Url::home().'images/posts_images/'.$post['image_src']; ?>">
											<?php } ?>
										</div>
									</div>
								</div>
								<div class="table-wrap">
									<table class="table">
										<tr>
											<td><b>Text</b></td>
											<td><?= $post['content']; ?></td>
										</tr>
										<tr>
											<td><b>Address</b></td>
											<td><?= $post['address']; ?></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				<?php }?>
				<?php foreach($postModel as $post){ ?>
					<div class="modalRightPagesLS" id="inquires_post_<?=$post['id']?>">

						<a class="btn btn-close pull-right" onclick="$('#inquires_post_<?=$post['id']?>').hide()">
							<span class="" aria-hidden="true">x</span>
						</a>
						<h4 class="post-market-title">
							Inquires
						</h4>
						<div class="panel-content">
							<?php
							$users = \app\modules\posts\models\PostContsctRequest::find()->where(['post_id'=>$post['id']])->orderBy(['id'=>SORT_DESC])->all();
							?>
							<table class="table classBlock">
								<thead>
									<tr>
										<td>User</td>
										<td>Renting</td>
										<td></td>
									</tr>
								</thead>
								<tbody>
								<?php
								foreach($users as $item){?>
									<tr>
										<td>
											<div class="class-user-av">
												<?php

												?>

												<?php if (isset($item->user->userinfo['avatar']) && ($item->user->userinfo['avatar'])){
													?>
													<img class="img-responsive" src="<?= Url::home(); ?>images/users_images/<?= $item->user->userinfo['avatar']; ?>">
												<?php } else {?>
													<img class="img-responsive" src="<?= Url::home(); ?>images/default_avatar.jpg">
												<?php } ?>
											</div>
											<span class="user-name">
												<?= $item->user['name']; ?> <?= $item->user['surname']; ?>
                                                <?=($item->view == 0) ? '<span class="label label-danger">NEW INQUIRE</span>' : ''?>
											</span>
										</td>
										<td>
											<?=\app\widgets\rating\UserratingWidget::widget(['user_id'=>$item->user_id])?>
										</td>
										<td><button class="startChat" data-sender-id="<?=Yii::$app->user->id?>" data-recipient-id="<?=$item->user_id?>" >Contact</button></td>
									</tr>
								<?php }?>
								</tbody>
							</table>
						</div>
					</div>
				<?php }?>
			</div>
		</div>
	</div>
