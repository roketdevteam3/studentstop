<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\widgets\Breadcrumbs;
use app\widgets\FavoritepostWidget;
use app\assets\FavoritepostAsset;
FavoritepostAsset::register($this);

?>
	<div class="row">
		<?= $this->render('right_menu'); ?>
		<div class="classified-market-content">
			<div class="container-fluid background-block">
				<div class="row">
					<div class="input-group">
                        <form method="get" action="/market/community/activities">
                            <input type="text" class="form-control" name="posts_content" placeholder="Enter search text ...">
                        </form>
						<span class="input-group-btn">
							<button class="btn btn-add-post btn-ripple " type="button">Add Post +</button>
						</span>
					</div>
				</div>
				<div class="action-result-window">
					<?php
					if(Yii::$app->session->hasFlash('PostsAdded')):
						echo Alert::widget([
								'options' => [
										'class' => 'alert-info',
								],
								'body' => \Yii::t('app','Posts added'),
						]);
					endif;
					if(Yii::$app->session->hasFlash('PostsDelete')):
						echo Alert::widget([
								'options' => [
										'class' => 'alert-warning',
								],
								'body' => \Yii::t('app','Posts deleted'),
						]);
					endif;
					?>
				</div>
				<ul class="nav nav-tabs view-tabs">
					<span class="market-header">My favorite</span>
					<span class="choice">View</span>
					<li>
						<a data-toggle="tab" class="grid-appearance" href="#grid-content"></a>
					</li>
					<li class="active">
						<a data-toggle="tab" class="list-appearance" href="#list-content"></a>
					</li>
				</ul>
				<div class="tab-content">
					<div id="list-content" class="tab-pane fade in active">
						<div class="market-thing-wrap">
							<?php foreach($postModel as $post){ ?>
								<div class="market-thing">
									<div class="market-thing-image">
										<?php if($post['image_src'] == ''){ ?>
											<img class="img-responsive" src="<?= Url::home().'images/bg.jpg'; ?>">
										<?php }else{ ?>
											<img class="img-responsive" src="<?= Url::home().'images/posts_images/'.$post['image_src']; ?>">
										<?php } ?>
									</div>
									<div class="info">
										<h4 class="post-name"><?=$post['title']?></h4>
										<?php $user = \app\modules\users\models\User::findOne($post['user_id']);?>
										<span class="by">(by <?= $user->name.' '.$user->surname?>)</span>
										<span class="date-post"><?=date('d/m/Y',strtotime($post['date_create']))?></span>
										<p class="content-thing"><?= mb_substr($post['content'], 0, 106, "UTF8").'...'; ?></p>
										<h4 class="address"><?= $post['address']; ?></h4>
									</div>
									<div class="btn-wrap text-right">
										<?= FavoritepostWidget::widget(['post_id' => $post['id']]) ?><br />
										<div class="views-counter">
											(views <?=$post['views']?>)
										</div>
										<a class="btn btn-view" id="view" data-post_id="<?=$post['id']?>" onclick="$('#view_post_<?=$post['id']?>').show()">View</a>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
					<div id="grid-content" class="tab-pane fade">
						<div class="market-grid">
							<div class="row">
								<?php foreach($postModel as $post){ ?>
									<div class="col-md-4">
										<div class="grid-item">
											<div class="gallery-mask">
												<div class="top-info">
													<div class="house-name"><?= $post['title']; ?></div>
													<?php $user = \app\modules\users\models\User::findOne($post['user_id']);?>
													<span class="by">(by <?= $user->name.' '.$user->surname?>)</span>
													<div class="date-post">
														<?=date('d/m/Y',strtotime($post['date_create']))?>
													</div>
												</div>
												<div class="info-text">
													<?= mb_substr($post['content'], 0, 106, "UTF8").'...'; ?>
												</div>
												<?= FavoritepostWidget::widget(['post_id' => $post['id']]) ?>
												<a href="#" class="btn-view" id="view" data-post_id="<?=$post['id']?>" onclick="$('#view_post_<?=$post['id']?>').show()">View</a>
											<span class="view-count">
												(views <?=$post['views']?>)
											</span>
											</div>
											<div class="gallery-img">
												<?php if($post['image_src'] == ''){ ?>
													<img class="img-responsive" src="<?= Url::home().'images/bg.jpg'; ?>">
												<?php }else{ ?>
													<img class="img-responsive" src="<?= Url::home().'images/posts_images/'.$post['image_src']; ?>">
												<?php } ?>
											</div>
										</div>
									</div>
								<?php }?>
							</div>
						</div>
					</div>
				</div>
				<?php foreach($postModel as $post){ ?>
					<div class="modalRightPagesLS" id="view_post_<?=$post['id']?>">

						<a class="btn btn-close pull-right" onclick="$('#view_post_<?=$post['id']?>').hide()">
							<span class="" aria-hidden="true">x</span>
						</a>    
						<h4 class="post-market-title">
							<?= $post['title']; ?>
						</h4>
						<div class="panel-content">
							<div class="market-post">
								<span class="post-market-date"><?= $post['date_create']; ?></span>
                                                                <?php $form = ActiveForm::begin(); ?>
                                                                    <input type="hidden" name="post_id" value="<?=$post['id']?>">
                                                                    <button type="submit" class="btn btn-contact-request" name="contact_request">Contact request</button>
                                                                <?php ActiveForm::end();?>

								<div class="row">
									<div class="col-md-offset-3 col-md-6">
										<div class="post-market-image">
											<?php if($post['image_src'] != ''){ ?>
												<img class="img-responsive" src="<?= Url::home().'images/posts_images/'.$post['image_src']; ?>">
											<?php } ?>
										</div>
									</div>
								</div>
								<div class="table-wrap">
									<table class="table">
										<tr>
											<td><b>Address</b></td>
											<td><?= $post['address']; ?></td>
										</tr>
                                                                                <tr>
											<td><b>Text</b></td>
											<td><?= $post['content']; ?></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				<?php }?>
				<?= LinkPager::widget(['pagination'=>$pagination]); ?>
			</div>
		</div>
	</div>