<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use app\widgets\FavoriterentsWidget;
use app\assets\FavoriterentAsset;
FavoriterentAsset::register($this);
use app\widgets\rating\RatingrentalWidget;

use app\assets\PostsAsset;
PostsAsset::register($this);


$arrayTimeRent = ['hour' => '1 hour', 'day' => '1 day','week' => '1 week','month' => '1 month','year' => '1 year'];
?>

	<?php
		if(Yii::$app->session->hasFlash('PostsAdded')):
			echo Alert::widget([
				'options' => [
					'class' => 'alert-info',
				],
				'body' => \Yii::t('app','Posts added'),
			]);
		endif;  
		if(Yii::$app->session->hasFlash('PostsDelete')):
			echo Alert::widget([
				'options' => [
					'class' => 'alert-warning',
				],
				'body' => \Yii::t('app','Post deleted'),
			]);
		endif;
	?>
	<div class="row">
		<?= $this->render('right_menu'); ?>
		<div class="rent-content">
			<div class="container-fluid background-block">
				<div class="row">
					<?php $form = ActiveForm::begin([
							'action'=> '/rents/'.$_GET['category_child'],
							'method' => 'get',
							'options' => [
									'class' => 'top-search-form rent-search form-inline'
							]
					]); ?>
					<div class="input-group">
						<div class="form-group">
							<?php
							$rent_value = '';
							if(isset($_GET['rent_value'])){
								$rent_value = $_GET['rent_value'];
							}
							?>
							<input type="text" name="rent_value" class="form-control searchInput" value="<?= $rent_value; ?>" placeholder="Destination, city, adress">
							<?php
							$reserve_from = '';
							if(isset($_GET['reserve_from'])){
								$reserve_from = $_GET['reserve_from'];
							}
							/*echo \kartik\date\DatePicker::widget([
									'name' => 'reserve_from',
									'type' => \kartik\date\DatePicker::TYPE_INPUT,
									'value' => $reserve_from,
									'options' => ['class' => 'PlaceholderInput','placeholder' => 'Check-in','id'=>'filter_reserve_for'],
									'pluginOptions' => [
											'autoclose'=>true,
											'format' => ''
									]
							]);*/
							echo \kartik\datetime\DateTimePicker::widget([
									'name' => 'reserve_from',
									'type' => \kartik\datetime\DateTimePicker::TYPE_INPUT,
									'value' => $reserve_from,
									'options' => ['class'=>'krajee-datepicker', 'placeholder' => 'Check in'],
									'pluginOptions' => [
                                                                            'autoclose'=>true,
                                                                            'format' => 'yyyy-mm-dd hh:ii',
                                                                            'startDate' => date('Y-m-d'),
                                                                            'minDate'=>0,
									]
							]);
							$reserve_to = '';
							if(isset($_GET['reserve_to'])){
								$reserve_to = $_GET['reserve_to'];
							}
							echo \kartik\datetime\DateTimePicker::widget([
									'name' => 'reserve_to',
									'type' => \kartik\datetime\DateTimePicker::TYPE_INPUT,
									'value' => $reserve_to,
									'options' => ['class'=>'krajee-datepicker', 'placeholder' => 'Check out'],
									'pluginOptions' => [
                                                                            'autoclose'=>true,
                                                                            'format' => 'yyyy-mm-dd hh:ii',
                                                                            'startDate' => date('Y-m-d'),
                                                                            'minDate'=>0,
									]
							]);
							/*echo \kartik\date\DatePicker::widget([
									'name' => 'reserve_to',
									'type' => \kartik\date\DatePicker::TYPE_INPUT,
									'value' => $reserve_to,
									'options' => ['placeholder' => 'Check-out', 'onchange'=>'limitDate()', 'id'=>'filter_reserve_to'],
									'pluginOptions' => [
											'autoclose'=>true,
											'format' => 'yyyy-mm-dd'
									]
							]);*/
							?>
							
						</div>
						<div class="input-group-btn">
							<button type="submit" class="btn btn-search">Search</button>
						</div>
					</div>
					<?php $form = ActiveForm::end(); ?>
				</div>

				<div class="row">
					<div id="map" class="map">

					</div>
				</div>
				<div class="rent-page">
					<ul class="nav nav-tabs view-tabs">
						<span class="choice">View</span>
						<li>
							<a data-toggle="tab" class="grid-appearance" href="#grid-content"></a>
						</li>
						<li class="active">
							<a data-toggle="tab" class="list-appearance" href="#list-content"></a>
						</li>
					</ul>
				</div>
				<?php
				//if(Yii::$app->request->get('message')=='false'):
				if(Yii::$app->session['rent_cansel']):
					Yii::$app->session['rent_cansel']=null;
                  echo '<script> $(document).ready(function(){ swal("You cancel payment service!", "", "warning")});</script>';
				endif;
				if(Yii::$app->session['rent_ok']):

                  $rent_r = \app\modules\posts\models\Rentreserve::findOne(Yii::$app->session['rent_ok']);
                  Yii::$app->session['rent_ok']=null;
                  if ($rent_r){
                    echo '<script> $(document).ready(function(){ swal("Thank you! Your place is booked from '.$rent_r->reserve_from.' to '.$rent_r->reserve_to.'!", "", "success")});</script>';
                  } else
                    echo '<script> $(document).ready(function(){ swal("Thank you! Your place is booked!", "", "success")});</script>';
				endif;
				if(Yii::$app->session->setFlash('reserve_request')):
					echo Alert::widget([
							'options' => [
									'class' => 'alert-info',
							],
							'body' => \Yii::t('app','reserve_request!'),
					]);
				endif;
				if(Yii::$app->session['not_credits']):
					Yii::$app->session['not_credits']=null;
                  echo '<script> $(document).ready(function(){ swal("Not credits!", "", "warning")});</script>';
				endif;
				?>
				<div class="tab-content rent-tab-content">
					<div id="list-content" class="tab-pane fade in active">
						<div class="rent-thing-wrap">
							<?php $price_for_array = [1 => 'hour', 2 => 'day', 3 => 'month', 4 => 'year'];?>
							<?php  foreach($postModel as $rent){ ?>
							<div class="rent-thing rentBlockInfo" data-rent_id="<?= $rent['id']; ?>" data-position_lat="<?= $rent['position_lat']; ?>" data-position_lng="<?= $rent['position_lng']; ?>">
								<div class="rent-thing-image">
									<?php if($rent['img_src'] == ''){ ?>
										<img class="img-responsive" src="<?= Url::home().'images/rent-default.jpg'; ?>">
									<?php }else{ ?>
										<img class="img-responsive" src="<?= Url::home().'images/posts_images/'.$rent['img_src']; ?>">
									<?php } ?>
								</div>
								<div class="" style="padding-top:10px">
									<div class="info">
										<h4 class="post-name"><?=$rent['title']?></h4>
									</div>
									<a class="btn btn-view" id="view" data-renthouse_id="<?=$rent['id']?>" >Book</a>

									<div class="total-rating ">
										<?= \app\widgets\rank\RankrentsWidget::widget(['rent_id' => $rent['id'], 'type' => 'rents']); ?>
									</div>

									<div class="" style="float: right">
										<div class="info star_style">
											<?= RatingrentalWidget::widget(['rent_id' => $rent['id'], 'type' => 'rents']); ?>
										</div>
									</div>
									<div class="views-count ">
										<?=$rent['views']?> views
									</div>
								</div>
								<div class="centerrer-block">
									<div class="info">

										<div class="clearfix"></div>
										<h4 class="address"><?= $rent['address']; ?></h4>
										<p class="content-thing"><?= mb_substr($rent['content'], 0, 106, "UTF8").'...'; ?></p>
									</div>
								</div>

								<div class="bottom-row">
									<div class="conditions mustyle_class">
										<div class="price-rent">
											<span class="price" style="font-size: 16px">
                                            <?php if($hour){?>
                                              <?php echo ($rent['price']>0) ? 'Price: '.$rent['price']*$hour.' $ ' : '' ?> <br/>
                                              <?php echo ($rent['credits']>0) ? 'Credits: '.$rent['credits']*$hour.'' : '<br/>' ?>
                                            <?php }else{?>
                                              <?php echo ($rent['price']>0) ? 'Price: '.$rent['price'].' $ / hour ' : '' ?> <br/>
                                              <?php echo ($rent['credits']>0) ? 'Credits: '.$rent['credits'].'  / hour ' : '<br/>' ?>
                                            <?php } ?>

                                            </span>

										</div>
										<div style="position: relative; top: 58px; padding-right: 22px;">
											<p class="booking col-xs-6">Booked <?=\app\modules\posts\models\Rentreserve::find()->where(['rent_id'=>$rent['id'],'rent_type'=>'rents'])->count()?></p>
											<?= FavoriterentsWidget::widget(['rent_id' => $rent['id']]) ?>
										</div>
									</div>
								</div>

								<?php//  var_dump($rent); ?>


							</div>
							<?php }  ?>
						</div>
					</div>
					<div id="grid-content" class="tab-pane fade">
						<div class="gallery-wrap">
							<div class="row">
								<?php  foreach($postModel as $rent){ ?>
									<div class="col-md-4">
									<div class="grid-item">
										<div class="gallery-mask">
											<div class="top-info">
												<div class="house-name"><?= $rent['title']; ?></div>
												<div class="rating">
                                                                                                    <div class="mystar"><?= \app\widgets\rank\RankrentsWidget::widget(['rent_id' => $rent['id'], 'type' => 'rents']); ?></div>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="house-address"><?= $rent['address']; ?></div>
											<div class="info-text">
												<?= mb_substr($rent['content'], 0, 106, "UTF8").'...'; ?>
											</div>
											<a href="#" class="add-favorite">Add to favorite <i class="fa fa-star" aria-hidden="true"></i></a>
											<a href="#" class="btn-view" id="view" data-renthouse_id="<?=$rent['id']?>">View</a>
										</div>
										<div class="gallery-img">
											<?php if($rent['img_src'] == ''){ ?>
												<img class="img-responsive" src="<?= Url::home().'images/rent-default.jpg'; ?>">
											<?php }else{ ?>
												<img class="img-responsive" src="<?= Url::home().'images/users_images/'.$rent['img_src']; ?>">
											<?php } ?>
										</div>
									</div>
								</div>
								<?php }?>
							</div>
						</div>
					</div>

					<?php  foreach($postModel as $rent){ ?>
						<div class="modalRightPagesLS" id="view_rentshouse_<?=$rent['id']?>">
						<?php
						$query = new \yii\db\Query();
						$query->select(['*','id' => '{{%user}}.id'])
								->from('{{%user}}')
								->leftJoin('{{%user_info}}', '{{%user_info}}.id_user = {{%user}}.id')
								->where(['{{%user}}.id' => $rent['user_id']]);

						$command = $query->createCommand();
						$userPosted = $command->queryOne();
						?>
						<a class="btn btn-close pull-right closeModal" data-rent_id="<?= $rent['id']; ?>" >
                                                        <span class="" aria-hidden="true">x</span>
                                                </a>
						<div class="user-info">
							<div class="class-user-av">
								<?php if ($userPosted['avatar']){?>
									<img class="img-responsive" src="<?= Url::home(); ?>images/users_images/<?= $userPosted['avatar']; ?>">
								<?php } else {?>
									<img class="img-responsive" src="<?= Url::home(); ?>images/default_avatar.jpg">
								<?php } ?>
							</div>
										<span class="user-name">
											<?= $userPosted['name']; ?> <?= $userPosted['surname']; ?>
										</span>
							<div class="right-block">
								<?= \app\widgets\rating\RatingrentaluserWidget::widget(['object_id' => $userPosted['id'], 'type' => 'rental_a_rent']); ?>
								<span class="price"><?= ($hour) ? $rent['price']*$hour.'$' : '' ?></span>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="panel-content">

							<!-- Nav tabs -->
							<ul class="nav nav-tabs" role="tablist">
								<li role="presentation" class="active"><a href="#description<?=$rent['id']?>" aria-controls="description<?=$rent['id']?>" role="tab" data-toggle="tab">Description</a></li>
								
                                                                <li role="presentation">
                                                                    <a href="#map<?=$rent['id']?>" data-position_lat="<?=$rent['position_lat']?>" data-position_lng="<?=$rent['position_lng']?>"  class="rent_tab_show_map"  data-rent_id="<?=$rent['id']?>" aria-controls="map<?=$rent['id']?>" role="tab" data-toggle="tab">Map</a>
                                                                </li>
								
                                                                <li role="presentation"><a href="#images<?=$rent['id']?>" aria-controls="images<?=$rent['id']?>" role="tab" data-toggle="tab">Images</a></li>
								<li role="presentation"><a href="#calendar<?=$rent['id']?>" aria-controls="calendar<?=$rent['id']?>" role="tab" data-toggle="tab" onmousemove="$('#calendar_w_<?=$rent['id']?>').fullCalendar('today');">Calendar</a></li>
								<li role="presentation"><a href="#comments<?=$rent['id']?>" aria-controls="comments<?=$rent['id']?>" role="tab" data-toggle="tab">Reviews</a></li>
								<li role="presentation"><?php if(Yii::$app->request->get('reserve_from') and Yii::$app->request->get('reserve_to')) {?>
										<!--<button type="button" class="btn" id="book" data-renthouse_id="--><?//=$rent['id']?><!--" data-renthouse_price_d="--><?//=$rent['price']?><!--" data-renthouse_price_c="--><?//=$rent['credits']?><!--"> Book </button>-->
										<a href="#book<?=$rent['id']?>" aria-controls="book<?=$rent['id']?>" role="tab" data-toggle="tab">Book</a>
									<?php }?></li>
							</ul>
							<!-- Tab panes -->
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="description<?=$rent['id']?>">
									<div class="rent-thin">
										<div class="info">
											<h4 class="address"><?= $rent['address']; ?></h4>
											<p class="content-thing"><?=$rent['content'] ?></p>
										</div>
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="map<?=$rent['id']?>">
                                                                    <div id="map_modal<?=$rent['id']?>" class="map">
                                                                    </div>
                                                                </div>
								<div role="tabpanel" class="tab-pane" id="images<?=$rent['id']?>">
									<?php
									$images = \app\modules\posts\models\Postsphotos::find()->where(['post_id'=>$rent['id']])->all();
									foreach($images as $image){
										echo '<img src="/images/users_images/'.$image->image_src.'" style="max-width: 420px" />';
									}
									?>
								</div>
								<div role="tabpanel" class="tab-pane" id="calendar<?=$rent['id']?>">
									<div id="calen_id_<?=$rent['id']?>">
										<?php
										$events = [];
										$renteds = \app\modules\posts\models\Rentreserve::find()->where(['rent_type'=>'rents', 'rent_id'=>$rent['id']])->all();
										foreach($renteds as $rented ){
											$event        = new \yii2fullcalendar\models\Event();
											$event->id    = $rented->id;
											$event->title = 'Rented';
											$event->start = date('Y-m-d\TH:i:s\Z', strtotime($rented->reserve_from));
											$event->end = date('Y-m-d\TH:i:s\Z', strtotime($rented->reserve_to));
											$events[] = $event;
										}
										/*echo \yii2fullcalendar\yii2fullcalendar::widget(array(
                                               'clientOptions' => [
//																'eventLimit'=> true,
//																'fixedWeekCount'=>false,
//															'defaultDate' => date('Y-m-d\TH:i:s\Z'),
                                                   'nowIndicator'=>false,
                                                       'defaultDate' => date('Y-m-d',time())
                                               ],
                                               'events'=> $events,
                                       ));*/
										echo \yii2fullcalendar\yii2fullcalendar::widget([
												'id' => 'calendar_w_'.$rent['id'],
												'clientOptions' => [
														'getDate' => date('Y-m-d',time()),
														'defaultDate' => date('Y-m-d',time()),
														'today'=>true,
													//															'dayClick'=>new \yii\web\JsExpression('function (cellInfo, jsEvent) {console.log(cellInfo);}')
												],
												'events'=> $events,
										]);

										?>
									</div>
								</div>
								<div role="tabpanel" class="tab-pane style-tb-pane-hr" id="comments<?=$rent['id']?>">
									<?php
									foreach($renteds as $rented){
										if ($rented->rating_comment)
											echo $rented->user['name']."<br/>".$rented->rating_comment."<br/><br/><hr><br/>";
									}
									?>
								</div>
								<div role="tabpanel" class="tab-pane" id="book<?=$rent['id']?>">
									<?php $form = ActiveForm::begin(); ?>
									<input type="hidden" name="rent_id" id="form_rent_id" value="<?=$rent['id']?>">
									<input type="hidden" name="price_d" id="price_d" value="<?=$rent['price']?>">
									<input type="hidden" name="price_c" id="price_c" value="<?=$rent['credits']?>">
									<input type="hidden" name="hour" id="hour" value="<?=$hour?>">
									<input type="hidden" name="form_date_for" id="form_date_for" value="<?=Yii::$app->request->get('reserve_from')?>">
									<input type="hidden" name="form_date_to" id="form_date_to" value="<?=Yii::$app->request->get('reserve_to')?>">
									<!--<label class="control-label">From</label>
						<?php
									/*				echo \kartik\date\DatePicker::widget([
                                                            'name' => 'reserve_from',
                                                            'value' => date('Y-m-d'),
                                                            'options' => ['placeholder' => 'Select issue date ...','required' => 'true', 'id'=>'form_date_for'],
                                                            'pluginOptions' => [
                                                                    'autoclose'=>true,
                                                                    'format' => 'yyyy-mm-dd',
                                                                    'todayHighlight' => true
                                                            ]
                                                    ]);
                                                    */?>
														<label class="control-label">To</label>
														--><?php
									/*				echo \kartik\date\DatePicker::widget([
                                                            'name' => 'reserve_to',
                                                            'value' => date('Y-m-d', strtotime('+1 days')),
                                                            'options' => ['placeholder' => 'Select issue date ...','required' => 'true', 'id'=>'form_date_to'],
                                                            'pluginOptions' => [
                                                                    'autoclose'=>true,
                                                                    'format' => 'yyyy-mm-dd',
                                                                    'todayHighlight' => true
                                                            ]
                                                    ]);
                                                    */?>

									<div class="col-xs-12 style-margin-pos-to-text">
										<div>Rent date for: <span id="rent_day"><?=Yii::$app->request->get('reserve_from')?></span></div>
										<div>Rent date to: <span id="rent_day"><?=Yii::$app->request->get('reserve_to')?></span></div>
										<div>Rent hour: <span id="rent_hour"><?=$hour?></span></div>
                                        <?php if ($rent['credits'] > 0) {?>
                                            <div>Total price($): <span id="form_price_rent_d"><?=$rent['price']*$hour?></span></div>
                                        <?php }?>
										<?php if ($rent['credits'] > 0) {?>
											<div id="credits_show">Total price(credits): <span id="form_price_rent_c"><?=$rent['credits']*$hour?></span></div>
										<?php }?>
									</div>
                                        <div class="style-center-butt-pay">
                                            <?php if ($rent['instant_pay']==0){?>
                                                    <?= ($rent['credits'] > 0) ? Html::submitButton( Yii::t('app', 'Pay with Paypal'), ['name'=> 'reserve_paypal', 'class' => 'btn btn-action']) : '' ?>
                                                    <?php if ($rent['credits'] > 0) echo  Html::submitButton( Yii::t('app', 'Pay credits'), ['name'=> 'reserve_credits', 'id'=>'credits_show_button', 'class' => 'btn btn-action']); ?>
                                            <?php } else {?>
                                                    <?= Html::submitButton( Yii::t('app', 'Request'), ['name'=> 'reserve_request', 'class' => 'btn btn-action']) ?>
                                            <?php }?>
                                            <?php $form = ActiveForm::end(); ?>
                                        </div>
								</div>
							</div>
						</div>
					</div>
					<?php }?>
					<?= LinkPager::widget(['pagination'=>$pagination]); ?>
				</div>
			</div>
		</div>
	</div>
