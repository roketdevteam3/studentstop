<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use kartik\datetime\DateTimePicker;

$arrayTimeRent = ['hour' => '1 hour', 'day' => '1 day','week' => '1 week','month' => '1 month','year' => '1 year'];
?>
	<div class="row">
		<div id="right-panel" class="rents-panel">
			<!-- <div class="u-av"> -->
				<img src="<?= Url::home().'images/users_images/'.$userPosted['avatar']; ?>" class="img-responsive">
			<!-- </div> -->
			<p class="user-name"><?php echo HTML::a($userPosted['name']." ".$userPosted['surname'], '/profile/'.$userPosted['id']); ?></p>
			<div class="user-info">
				<p class="value-name-row">Email:</p>
				<p class="value-row"><?= $userPosted['email']; ?></p>
				<p class="value-name-row">Mobile:</p>
				<p class="value-row"><?= $userPosted['mobile']; ?></p>
				<p class="value-name-row">Address:</p>
				<p class="value-row"><?= $userPosted['address']; ?></p>
				<p class="value-name-row">Facebook link:</p>
				<p class="value-row"><?= $userPosted['facebook']; ?></p>
				<p class="value-name-row">Twitter link:</p>
				<p class="value-row"><?= $userPosted['twitter']; ?></p>
			</div>
		</div>
		<div class="rent-content">
			<div class="container-fluid background-block">
				<div class="posts-default-index">
					<?php
						if(Yii::$app->session->hasFlash('PostsUpdate')):
							echo Alert::widget([
								'options' => [
									'class' => 'alert-info',
								],
								'body' => \Yii::t('app','Posts updated!'),
							]);
						endif;
						if(Yii::$app->session->hasFlash('RentAdded')):
							echo Alert::widget([
								'options' => [
									'class' => 'alert-info rent_request',
									'data-rent-id' => $newPostRent['id'],
									'data-recipient-id' => $userPosted['id'],
								],
								'body' => \Yii::t('app','Application form sent!'),
							]);
						endif;
					?>
					<div class="row">
						<div class="col-sm-12">
							<div class="row">
								<div class="col-sm-12">
									<?php if($postViewModel['user_id'] == \Yii::$app->user->id){ ?>
										<button class='btn btn-primary' data-toggle="modal" data-target="#modalUpdateRental">Update</button>
										<?php echo HTML::a("Delete", '/rents/'.$postCategory->url_name.'/delete/'.$postViewModel['id'], ['class' => 'btn btn-danger']); ?>
										<div class="modal fade" id="modalUpdateRental" role="dialog">
											<div class="modal-dialog">
											  <div class="modal-content">
												<div class="modal-header">
												  <button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4>Update rental</h4>
												</div>
												<div class="modal-body">
													<?php $form = ActiveForm::begin(); ?>
															<?= $form->field($postViewModel, 'title')->textInput(); ?>
															<!--<span class="btn btn-default newPostPhoto">
																<span>Upload photo</span>
															</span>
															<div class='post_container_photo'></div>-->
															<?php //= $form->field($postViewModel, 'image_src')->hiddenInput()->label(false); ?>
															<?php //= $form->field($newPostphoto, 'image_src')->hiddenInput(); ?>
															<!--<div action="/posts/posts/savepostsphoto" style="border:2px dashed #0087F7" class="post_photos">
																<div class="dz-message needsclick">
																	<h3 style="color:#0087F7;">Drop files here or click to upload.</h3>
																</div>
															</div> -->
															<?= $form->field($postViewModel, 'content')->textarea(['rows' => '6']); ?>
														<?php if($postCategory->category_type == 2){ ?>
															<div class="well">
																<?= $form->field($postinfoViewModel, 'building_floor')->textInput(); ?>
																<?= $form->field($postinfoViewModel, 'house_storeys')->textInput(); ?>
																<?= $form->field($postinfoViewModel, 'room_count')->textInput(); ?>
																<?= $form->field($postinfoViewModel, 'total_area')->textInput(); ?>
															</div>
														<?php } ?>
															<?= $form->field($postinfoViewModel, 'status')->dropDownList([0 => 'buzy', 1=> 'active']); ?>
															<?= $form->field($postinfoViewModel, 'price')->textInput(); ?>
															<?= $form->field($postinfoViewModel, 'time_rent')->dropDownList($arrayTimeRent); ?>
															<?= $form->field($postinfoViewModel, 'address')->textInput(); ?>
															<?= $form->field($postinfoViewModel, 'zip_code')->textInput(); ?>
															<?= $form->field($postinfoViewModel, 'country')->textInput(); ?>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
														<?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'change_user_info', 'class' => 'btn btn-primary']) ?>
													<?php ActiveForm::end();  ?>
												</div>
											  </div>
											</div>
										</div>
									<?php } ?>
								</div>
								<div class="col-sm-12">
									<table class="rent-table">
										<tr>
											<td class="rental-img" rowspan="5">
												<?php if($postViewModel['image_src'] != ''){ ?>
													<img src="<?= Url::home().'images/posts_images/'.$postViewModel['image_src']; ?>" style="width:100%;">
												<?php } ?>
											</td>
											<td>
												<h4>
													<?= $postViewModel['title']; ?>
												</h4>
												<span class="date">
													<?= $postViewModel['date_create']; ?>
												</span>
											</td>
											<td>
											</td>
											<td>
											</td>
											<td>
												<span class="pull-right">
													<?= $postinfoViewModel['price']; ?>$
												</span>
											</td>
										</tr>
										<tr>
											<td><b>Address</b></td>
											<td><?= $postinfoViewModel['address']; ?></td>
											<td><b>Zip code</b></td>
											<td><?= $postinfoViewModel['zip_code']; ?></td>
										</tr>
										<tr>
											<td><b>Time rent</b></td>
											<td><?= $postinfoViewModel['time_rent']; ?></td>
											<td><b>Status</b></td>
											<td><?php if($postinfoViewModel['status'] == 0): echo 'buzy'; else: echo 'active'; endif; ?></td>
										</tr>
										<?php if($postCategory->category_type == 2){ ?>
											<tr>
												<td><b>Building floor</b></td>
												<td><?= $postinfoViewModel['building_floor']; ?></td>
												<td><b>House storeys</b></td>
												<td><?= $postinfoViewModel['house_storeys']; ?></td>
											</tr>
											<tr>
												<td><b>Room count</b></td>
												<td><?= $postinfoViewModel['room_count']; ?></td>
												<td><b>Total area</b></td>
												<td><?= $postinfoViewModel['total_area']; ?></td>
											</tr>
										<?php } ?>
										<tr>
											<td><b>Country</b></td>
											<td><?= $postinfoViewModel['country']; ?></td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td class="text-left" colspan="4">
												<p><?= $postViewModel['content']; ?></p>
											</td>
										</tr>
									</table>
								</div>
								<div class="col-sm-12">
									<div class="rent-image-list">
										<?php foreach($postPhotos as $photo){ ?>
											<img src="<?= Url::home().'images/posts_images/'.$photo->image_src; ?>" style="width:200px;">
										<?php } ?>
									</div>
								</div>
								<div class="col-sm-12">
									<?php if($postinfoViewModel['status'] == 1){ ?>
										<button class='btn btn-action' data-toggle="modal" data-target="#modalRental">Rent</button>
										<div class="modal padding-body fade" id="modalRental" role="dialog">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title">Rent</h4>
													</div>
													<div class="modal-body">
														<?php $form = ActiveForm::begin(); ?>
															<?= $form->field($newPostRent, 'post_id')->hiddenInput(['value' => $postViewModel['id']])->label(false); ?>
															<?= $form->field($newPostRent, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false); ?>
															<?= $form->field($newPostRent, 'date_from')->widget(DateTimePicker::classname(), [
																'options' => ['placeholder' => 'Enter event time ...'],
																'pluginOptions' => [
																	'todayHighlight' => true
																]
															]); ?>
																	<?= $form->field($newPostRent, 'date_to')->widget(DateTimePicker::classname(), [
																'options' => ['placeholder' => 'Enter event time ...'],
																'pluginOptions' => [
																	'todayHighlight' => true
																]
															]); ?>
													</div>
													<div class="modal-footer">
														<?= Html::submitButton( Yii::t('app', 'Rent'), ['name'=> 'change_user_info', 'class' => 'btn btn-action']) ?>
														<button type="button" class="btn btn-close" data-dismiss="modal">Cancel</button>
														<?php ActiveForm::end();  ?>
													</div>
												</div>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
