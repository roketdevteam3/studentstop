<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\bootstrap\ActiveForm;
	use app\widgets\FilterrentsWidget;
	use app\widgets\rating\RatingrentaluserWidget;
		
	use app\assets\FilterAsset;
	FilterAsset::register($this);
	
	$allUrl = Yii::$app->request->url;
		
	if(strpos($allUrl, 'mycompany/jobs')){$class1 = 'active';}else{$class1 = '';}
	if(strpos($allUrl, 'mycompany/edit')){$class2 = 'active';}else{$class2 = '';}
	if(strpos($allUrl, 'addbusiness')){$class3 = 'active';}else{$class3 = '';}
	
?>
	<div class="rental-right-panel">
		<?php if(isset($userPosted)){ ?>
			<div class="class-user-av">
				<img class="img-responsive" src="<?= Url::home(); ?>images/users_images/<?= $userPosted['avatar']; ?>">
			</div>
			<div class="rating-blok">
				<?= RatingrentaluserWidget::widget(['object_id' => $userPosted['id'], 'type' => 'rental']); ?>
			</div>
			<div class="menu-wrapper">
				<div class="user-info">
					<span class="info-label">
						Name:
					</span>
					<span class="info-user">
						<?= $userPosted['name']; ?> <?= $userPosted['surname']; ?>
					</span>
					<span class="info-label">
						Birthday:
					</span>
					<span class="info-user">
						<?= $userPosted['birthday']; ?>
					</span>
					<span class="info-label">
						Phone:
					</span>
					<span class="info-user">
						<?= $userPosted['mobile']; ?>
					</span>
					<span class="info-label">
						Email:
					</span>
					<span class="info-user">
						<?= $userPosted['email']; ?>
					</span>
				</div>
			</div>
		<?php } ?>
		<nav class="menu-right">
			<ul class="">
				<li class="<?= $class1; ?>">
					<?= HTML::a('Dashboard',Url::home().'rents',['class' => ""]); ?>
				</li>
				<li class="<?= $class2; ?>">
					<?= HTML::a('Favorites',Url::home().'rents/favorite',['class' => ""]);?>
				</li>
				<li class="<?= $class3; ?>">
					<?= HTML::a('My account <span class="badge">'.\app\modules\posts\models\Rentreserve::find()->where(['user_id'=>Yii::$app->user->id,'rent_type' => 'rents','status'=>0,'request'=>1])->count().'</span>',Url::home().'rents/myprofile',['class' => ""]); ?>
				</li>
				<li class="<?= $class3; ?>">
					<?= HTML::a('My booking <span class="badge">'.\app\modules\posts\models\Rentreserve::find()->where(['user_id'=>Yii::$app->user->id,'rent_type' => 'rents','status'=>0,'view'=>1])->andWhere(['!=','request',0])->count().'</span>',Url::home().'rents/myreservations',['class' => ""]); ?>
				</li>
			</ul>
		</nav>
		<?= FilterrentsWidget::widget(['route' => $this->context->getRoute()]); ?>
	</div>
