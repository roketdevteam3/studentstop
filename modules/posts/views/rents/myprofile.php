<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use app\widgets\FavoriterenthouseWidget;
use app\assets\RentshouseprofileAsset;
RentshouseprofileAsset::register($this);
$this->registerJsFile('/js/rents.js');
$this->registerJsFile('/js/price_valid.js');
?>

<div style="display:none;">
	<div class="table table-striped previewTemplateQ" >
		<div class="row">
			<div id="template" class="file-row">
				<!-- This is used as the file preview template -->
				<div class="col-sm-3">
					<span class="preview"><img data-dz-thumbnail /></span>
				</div>
<!--                <div class="col-sm-3">
					<p class="name" data-dz-name></p>
					<strong class="error text-danger" data-dz-errormessage></strong>
				</div>
				<div class="col-sm-3">
					<p class="size" data-dz-size></p>
					<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
					  <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
					</div>
				</div>-->
				<div class="col-sm-3">
				  <button data-dz-remove class="btn btn-danger delete">
					<i class="glyphicon glyphicon-trash"></i>
					<span>Delete</span>
				  </button>
				</div>
			</div>
		</div>
	</div>
</div>
	<?php /* ?>
	<div class="col-sm-12" style="padding:0px;background-color: white">
	   <?php $form = ActiveForm::begin([
						'method' => 'get',
						'action' => Url::home().'rentshouse',
	//                            'options' => ['class' => 'top-search-form']
				]); ?>
			<input type="text" name="rent_value" class="form-control searchInput" placeholder="search rents a house by title or keywords" style='padding: 0 65px;height: 40px;line-height: 40px;font-size: 13px;background: url("../../web/default_img/icon/search.png") no-repeat 35px center;'>
		<?php $form = ActiveForm::end(); ?>
	</div>
	<?php */ ?>
	<div class="row">
		<?= $this->render('right_menu'); ?>
		<div class="rent-content">
			<div class="container-fluid background-block">
				<div class="rents-tab-wrap">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs navigation-tabs no-icons" role="tablist">
						<li role="presentation" class="active"><a href="#rent" aria-controls="rent" role="tab" data-toggle="tab">Rent</a></li>
						<li role="presentation"><a href="#request" aria-controls="request" role="tab" data-toggle="tab">Request <span class="badge"><?=\app\modules\posts\models\Rentreserve::find()->where(['user_id'=>Yii::$app->user->id,'rent_type' => 'rents','status'=>0,'request'=>1])->count()?></span></a></li>
					</ul>
					<button type="button" class="btn btn-add-rent" data-toggle="modal" data-target="#addRent">Add rent</button>
					<div id="addRent" class="modal" role="dialog">
						<div class="modal-dialog" >
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">x</button>
									<h4 class="modal-title">Add rent</h4>
								</div>
								<div class="modal-body">
									<?php $form = ActiveForm::begin(['id' => 'formAddrenthouse']); ?>
									<?= $form->field($modelNewRent, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false); ?>
									<?= $form->field($modelNewRent, 'title')->textInput(); ?>
									<?= $form->field($modelNewRent, 'content')->textarea(); ?>
									<?= $form->field($modelNewRent, 'img_src')->hiddenInput()->label(false); ?>
									<div style="clear:both;"></div>
									<div class="col-sm-12">
										<div class="row">
											<a class="add_header_photo btn btn-action"> Add header photo</a>
											<div class="show_header_photo"></div>
										</div>
									</div>
									<div style="clear:both;"></div>
									<?= $form->field($modelNewRent, 'category_id')->dropDownList($categoryArray); ?>

									<?= $form->field($modelNewRent, 'instant_pay')->dropDownList(['0'=>'Instant Pay','1'=>'Request'])->label('Type pay'); ?>

									<div id="price_show">
										<?= $form->field($modelNewRent, 'price')->textInput(['type' => 'number','min'=>0,'required'=>true])->label('Price'); ?>
										<!--<div class="col-sm-4">
											<?/*= $form->field($modelNewRent, 'price_for')->dropDownList(['1' => 'day', '7' => 'week', '30' => 'month' ])->label('Min. rent'); */?>
										</div>-->
										<?= $form->field($modelNewRent, 'credits')->textInput(['type' => 'number','min'=>0,'required'=>true])->label('Credits'); ?>
									</div>
									<div action="../../university/default/savedropedfile" style="border:2px dashed #0087F7" class="user_photos">
										<div class="dz-message needsclick">
											<h3 style="color:#0087F7;">Drop files here or click to upload.</h3>
										</div>
									</div>
									<?= $form->field($modelNewRent, 'position_lat')->hiddenInput(['id' => 'location_lat'])->label(false); ?>
									<?= $form->field($modelNewRent, 'position_lng')->hiddenInput(['id' => 'location_lng'])->label(false); ?>
									<div id="inputPhotos">
									</div>
									<?= Html::submitButton( Yii::t('app', ''), ['name'=> 'add_new_rents', 'style' => 'border:0px;height:0px;margin:0px;padding:0px;']) ?>
									<input type="text" class="enter_your_location form-control" placeholder="enter address">
									<div id="map" class="map"></div>
									<?php $form = ActiveForm::end(); ?>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
									<?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'add_new_rents_2', 'class' => 'btn btn-action']) ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
				if(Yii::$app->session->hasFlash('Rentadded')):
					echo Alert::widget([
						'options' => [
							'class' => 'alert-info',
						],
						'body' => \Yii::t('app','Rent a house added!'),
					]);
				endif;

				if(Yii::$app->session->hasFlash('Rentnotadded')):
					echo Alert::widget([
						'options' => [
							'class' => 'alert-warning',
						],
						'body' => \Yii::t('app','Rent a house not added!'),
					]);
				endif;
				?>
				<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="rent">
						<div class="rent-page">
							<ul class="nav nav-tabs view-tabs">
								<span class="choice">View</span>
								<li>
									<a data-toggle="tab" class="grid-appearance" href="#grid-content"></a>
								</li>
								<li class="active">
									<a data-toggle="tab" class="list-appearance" href="#list-content"></a>
								</li>
							</ul>

							<div class="tab-content rent-tab-content">
								<div id="list-content" class="tab-pane fade in active">
									<div class="rent-thing-wrap">
										<?php $arrayYesNo = [0 => 'No', 1 => 'Yes']; ?>
										<?php $price_for_array = [1 => 'hour', 2 => 'day', 3 => 'month', 4 => 'year'];?>
										<?php  foreach($myRent as $rent){ ?>
											<div class="rent-thing">
												<div class="rent-thing-image">
													<?php if($rent->img_src == ''){ ?>
														<img class="img-responsive" src="<?= Url::home().'images/rent-default.jpg'; ?>">
													<?php }else{ ?>
														<img class="img-responsive" src="<?= Url::home().'images/posts_images/'.$rent->img_src; ?>">
													<?php } ?>
												</div>


												<div class="" style="padding-top:10px">
													<div class="info">
														<h4  style="width: 29%"  class="post-name"><?=$rent['title']?></h4>
													</div>
                                                    <a style="height: 30px;font-size: 13px; margin-top: -10px; padding: 0 20px;border-radius: 0;" class="btn btn-danger pull-right" id="delete" data-rent_id="<?=$rent['id']?>" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>

                                                    <a class="btn btn-view" id="view" data-renthouse_id="<?=$rent['id']?>" >View</a>

													<div class="total-rating ">
														<?= \app\widgets\rank\RankrentsWidget::widget(['rent_id' => $rent['id'], 'type' => 'rents']); ?>

													</div>
													<div class="" style="float: right">
														<div class="info">
														<div class="rating-container">
															<i class="rating-active-star fa fa-star"></i>
															<i class="rating-active-star fa fa-star"></i>
															<i class="rating-active-star fa fa-star"></i>
															<i class="rating-active-star fa fa-star"></i>
															<i class="fa fa-star"></i>
														</div>
															</div>
													</div>
													<div class="views-count ">
														<?=$rent['views']?> reviews
													</div>
												</div>





												<div class="centerrer-block">
													<div class="info">
														<div class="clearfix"></div>
														<h4 class="address"><?= $rent->address; ?></h4>
														<p class="content-thing"><?= mb_substr($rent['content'], 0, 106, "UTF8").'...'; ?></p>
													</div>
													<div class="bottom-row">
														<div class="conditions">

														</div>
													</div>
												</div>


												<div class="bottom-row">
													<div class="conditions mustyle_class">
														<div class="price-rent">
															<span class="price">
                                                                <?php echo ($rent['price']>0) ? 'Price: '.$rent['price'].' $ / Hour ' : '' ?> <br/>
                                                              <?php echo ($rent['credits']>0) ? 'Credits: '.$rent['credits'].'  / Hour ' : '<br/>' ?>
                                                            </span>
														</div>
														<div style="position: relative; top: 58px; padding-right: 22px;">
															<p class="booking col-xs-6">Booked <?=\app\modules\posts\models\Rentreserve::find()->where(['rent_id'=>$rent['id'],'rent_type'=>'rent_house'])->count()?></p>
															<?php // = FavoriterenthouseWidget::widget(['renthouse_id' => $rent['id']]) ?>
														</div>
													</div>
												</div>
												<?php//  var_dump($rent); ?>
											</div>
										<?php }  ?>
									</div>
								</div>
								<div id="grid-content" class="tab-pane fade">
									<div class="gallery-wrap">
										<div class="row">
											<?php  foreach($myRent as $rent){ ?>
												<div class="col-md-4">
													<div class="grid-item">
														<div class="gallery-mask">
															<div class="top-info">
																<div class="house-name"><?= $rent->title; ?></div>
																<div class="rating">
																	<i class="fa fa-star" aria-hidden="true"></i>
																	<i class="fa fa-star" aria-hidden="true"></i>
																	<i class="fa fa-star" aria-hidden="true"></i>
																	<i class="fa fa-star-o" aria-hidden="true"></i>
																	<i class="fa fa-star-o" aria-hidden="true"></i>
																</div>
																<div class="clearfix"></div>
															</div>
															<div class="house-address"><?= $rent->address; ?></div>
															<div class="info-text">
																<?= mb_substr($rent['content'], 0, 106, "UTF8").'...'; ?>
															</div>
															<!--<a href="#" class="add-favorite">Add to favorite <i class="fa fa-star" aria-hidden="true"></i></a> -->
															<a href="#" class="btn-view" id="view" data-renthouse_id="<?=$rent['id']?>" >View</a>
														</div>
														<div class="gallery-img">
															<?php if($rent->img_src == ''){ ?>
																<img class="img-responsive" src="<?= Url::home().'images/rent-default.jpg'; ?>">
															<?php }else{ ?>
																<img class="img-responsive" src="<?= Url::home().'images/posts_images/'.$rent->img_src; ?>">
															<?php } ?>
														</div>
													</div>
												</div>
											<?php }?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="request">
						<table class="table table-rent">
							<thead>
							<th>Client name</th>
							<th>Rent name</th>
							<th>Rent date for</th>
							<th>Rent date to</th>
							<th>Options</th>
							</thead>
							<tbody>
							<?php foreach($request as $item){?>
								<tr>
									<td><?= $item->user['name'].' '.$item->user['surname']?></td>
									<td><?= $item->renthouse['title']?></td>
									<td><?= date('m-d-Y',strtotime($item->reserve_from)) ?></td>
									<td><?=date('m-d-Y',strtotime($item->reserve_to)) ?></td>
									<td>
										<a class="btn btn-success" href="?request=2&id=<?=$item->id?>">Agree</a>
										<a class="btn btn-danger" href="?request=3&id=<?=$item->id?>">Reject</a>
										<!--											<button onclick="$('.user-layer').show();">Contact</button>-->
									</td>
								</tr>
							<?php }?>
							</tbody>
						</table>
					</div>
				</div>

				</div>
				<?php  foreach($myRent as $rent){ ?>
					<div class="modalRightPagesLS" id="view_rentshouse_<?=$rent['id']?>">
						<?php
						$query = new \yii\db\Query();
						$query->select(['*','id' => '{{%user}}.id'])
								->from('{{%user}}')
								->leftJoin('{{%user_info}}', '{{%user_info}}.id_user = {{%user}}.id')
								->where(['{{%user}}.id' => $rent['user_id']]);

						$command = $query->createCommand();
						$userPosted = $command->queryOne();
						?>
						<a class="btn btn-close pull-right" onclick="$('#view_rentshouse_<?=$rent['id']?>').hide()">
							<span class="" aria-hidden="true">x</span>
						</a>
						<div class="user-info">
							<div class="class-user-av">
								<?php if ($userPosted['avatar']){?>
									<img class="img-responsive" src="<?= Url::home(); ?>images/users_images/<?= $userPosted['avatar']; ?>">
								<?php } else {?>
									<img class="img-responsive" src="<?= Url::home(); ?>images/default_avatar.jpg">
								<?php } ?>
							</div>
											<span class="user-name">
												<?= $userPosted['name']; ?> <?= $userPosted['surname']; ?>
											</span>
							<div class="right-block">
								<?= \app\widgets\rating\RatingrentaluserWidget::widget(['object_id' => $userPosted['id'], 'type' => 'rental_a_house']); ?>
								<span class="price"><?= $rent['price']?>$/day</span>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="panel-content">

							<!-- Nav tabs -->
							<ul class="nav nav-tabs" role="tablist">
								<li role="presentation" class="active"><a href="#description<?=$rent['id']?>" aria-controls="description<?=$rent['id']?>" role="tab" data-toggle="tab">Description</a></li>
								
                                                                <li role="presentation">
                                                                    <a href="#map<?=$rent['id']?>" data-position_lat="<?=$rent['position_lat']?>" data-position_lng="<?=$rent['position_lng']?>"  class="rent_tab_show_map"  data-rent_id="<?=$rent['id']?>" aria-controls="map<?=$rent['id']?>" role="tab" data-toggle="tab">Map</a>
                                                                </li>
                                                                
                                                                <li role="presentation"><a href="#images<?=$rent['id']?>" aria-controls="images<?=$rent['id']?>" role="tab" data-toggle="tab">Images</a></li>
								<li role="presentation"><a href="#calendar<?=$rent['id']?>" aria-controls="calendar<?=$rent['id']?>" role="tab" data-toggle="tab" onmousemove="$('#calendar_w_<?=$rent['id']?>').fullCalendar('today');">Calendar</a></li>
								<li role="presentation"><a href="#comments<?=$rent['id']?>" aria-controls="comments<?=$rent['id']?>" role="tab" data-toggle="tab">Reviews</a></li>
								<li role="presentation"><a href="#rented<?=$rent['id']?>" aria-controls="rented<?=$rent['id']?>" role="tab" data-toggle="tab">History</a></li>
							</ul>

							<!-- Tab panes -->
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="description<?=$rent['id']?>">
									<div class="info">
										<h4 class="address"><?= $rent['address']; ?></h4>
										<p class="content-thing"><?=$rent['content'] ?></p>
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="map<?=$rent['id']?>">
                                                                    <div id="map_modal<?=$rent['id']?>" class="map">
                                                                    </div>
                                                                </div>
								<div role="tabpanel" class="tab-pane" id="images<?=$rent['id']?>">
									<?php
									$images = \app\modules\posts\models\Postsphotos::find()->where(['post_id'=>$rent['id']])->all();
									foreach($images as $image){
										echo '<img src="/images/users_images/'.$image->image_src.'" style="max-width: 420px" />';
									}
									?>
								</div>
								<div role="tabpanel" class="tab-pane" id="calendar<?=$rent['id']?>">
									<div id="calen_id_<?=$rent['id']?>">
										<?php
										$events = [];
										$renteds = \app\modules\posts\models\Rentreserve::find()->where(['rent_type'=>'rents', 'rent_id'=>$rent['id']])->all();
										foreach($renteds as $rented ){
											$event        = new \yii2fullcalendar\models\Event();
											$event->id    = $rented->id;
											$event->title = 'Rented';
											$event->start = date('Y-m-d\TH:i:s\Z', strtotime($rented->reserve_from));
											$event->end = date('Y-m-d\TH:i:s\Z', strtotime($rented->reserve_to));
											$events[] = $event;
										}
										/*echo \yii2fullcalendar\yii2fullcalendar::widget(array(
                                               'clientOptions' => [
//																'eventLimit'=> true,
//																'fixedWeekCount'=>false,
//															'defaultDate' => date('Y-m-d\TH:i:s\Z'),
                                                   'nowIndicator'=>false,
                                                       'defaultDate' => date('Y-m-d',time())
                                               ],
                                               'events'=> $events,
                                       ));*/
										echo \yii2fullcalendar\yii2fullcalendar::widget([
												'id' => 'calendar_w_'.$rent['id'],
												'clientOptions' => [
														'getDate' => date('Y-m-d',time()),
														'defaultDate' => date('Y-m-d',time()),
														'today'=>true,
													//															'dayClick'=>new \yii\web\JsExpression('function (cellInfo, jsEvent) {console.log(cellInfo);}')
												],
												'events'=> $events,
										]);

										?>
									</div>
								</div>
								<div role="tabpanel" class="tab-pane style-tb-pane-hr" id="comments<?=$rent['id']?>">
									<?php
									foreach($renteds as $rented){
										if ($rented->rating_comment)
											echo $rented->user['name']."<br/>".$rented->rating_comment."<br/><br/><hr><br/>";
									}
									?>
								</div>
								<div role="tabpanel" class="tab-pane" id="rented<?=$rent['id']?>">
									<table class="table table-striped">
										<thead>
										<th>User name</th>
										<th>Date for</th>
										<th>Date to</th>
										<th>Day</th>
										<th>Price</th>
										<th>Credits</th>
										</thead>
										<tbody>
										<?php
										$rent_ser = \app\modules\posts\models\Rentreserve::find()->where(['rent_id'=>$rent['id'], 'rent_type'=>'rent_house'])->all();
										foreach($rent_ser as $item){
											?>
											<tr>
												<td><?=$item->user['name'].' '.$item->user['surname']?></td>
												<td><?=$item->reserve_from?></td>
												<td><?=$item->reserve_to?></td>
												<td>
													<?php
													$reserve_to = strtotime($item->reserve_to);
													$reserve_from = strtotime($item->reserve_from);
													$datediff = $reserve_to - $reserve_from;

													echo floor($datediff / (60 * 60 * 24));
													?>
												</td>
												<td><?=$item->total_price?></td>
												<td><?=$item->total_credits?></td>
											</tr>
										<?php } ?>
										</tbody>
									</table>
								</div>
							</div>

						</div>
					</div>
				<?php }  ?>
			</div>
		</div>
	</div>
