<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

?>
	<div class="row">
		<?= $this->render('right_menu'); ?>
		<div class="rent-content">
			<div class="container-fluid background-block">
                <div class="row">
                  <?php $form = ActiveForm::begin([
                    'action'=> '/rents/auto',
                    'method' => 'get',
                    'options' => [
                      'class' => 'top-search-form rent-search form-inline'
                    ]
                  ]); ?>
                    <div class="input-group">
                        <div class="form-group">
                          <?php
                          $rent_value = '';
                          if(isset($_GET['rent_value'])){
                            $rent_value = $_GET['rent_value'];
                          }
                          ?>
                            <input type="text" name="rent_value" class="form-control searchInput" value="<?= $rent_value; ?>" placeholder="Destination, city, adress">
                          <?php
                          $reserve_from = '';
                          if(isset($_GET['reserve_from'])){
                            $reserve_from = $_GET['reserve_from'];
                          }
                          /*echo \kartik\date\DatePicker::widget([
                'name' => 'reserve_from',
                'type' => \kartik\date\DatePicker::TYPE_INPUT,
                'value' => $reserve_from,
                'options' => ['class' => 'PlaceholderInput','placeholder' => 'Check-in','id'=>'filter_reserve_for'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => ''
                ]
            ]);*/
                          echo \kartik\datetime\DateTimePicker::widget([
                            'name' => 'reserve_from',
                            'type' => \kartik\datetime\DateTimePicker::TYPE_INPUT,
                            'value' => $reserve_from,
                            'options' => ['class'=>'krajee-datepicker', 'placeholder' => 'Check in'],
                            'pluginOptions' => [
                              'autoclose'=>true,
                                'startDate'=>date("yy-mm-dd"),
                               'minDate'=>'0',
                              'format' => 'yyyy-mm-dd hh:ii'
                            ]
                          ]);
                          $reserve_to = '';
                          if(isset($_GET['reserve_to'])){
                            $reserve_to = $_GET['reserve_to'];
                          }
                          echo \kartik\datetime\DateTimePicker::widget([
                            'name' => 'reserve_to',
                            'type' => \kartik\datetime\DateTimePicker::TYPE_INPUT,
                            'value' => $reserve_to,
                            'options' => ['class'=>'krajee-datepicker', 'placeholder' => 'Check out'],
                            'pluginOptions' => [
                              'autoclose'=>true,
                              'format' => 'yyyy-mm-dd hh:ii'
                            ]
                          ]);
                          /*echo \kartik\date\DatePicker::widget([
                'name' => 'reserve_to',
                'type' => \kartik\date\DatePicker::TYPE_INPUT,
                'value' => $reserve_to,
                'options' => ['placeholder' => 'Check-out', 'onchange'=>'limitDate()', 'id'=>'filter_reserve_to'],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]);*/
                          ?>

                        </div>
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-search">Search</button>
                        </div>
                    </div>
                  <?php $form = ActiveForm::end(); ?>
                </div>
				<div class="posts-default-index">
					<div class="">
						<div class="row">
							<?php foreach($postCategory as $category){ ?>
								<div class="col-md-3 col-sm-6">
									<div class="classified-block">
										<h2>
											<?= $category->category_name; ?>
										</h2>
										<?php $k = 0;
											foreach($category->child_category as $child_category){
												$k = $k + 1;
												?>
													<a href="<?='/rents/'.$child_category->url_name.''?>">
														<?=$child_category->category_name?>
														<span class="badge">
															<?=\app\modules\posts\models\Rents::find()->where(['category_id'=>$child_category->id])->count(); ?>
														</span>
													</a>
										<?php }
										if($k == 3){ ?>
											<?= HTML::a('More', '/rents/category/'.$category->url_name,['class' => 'more']); ?>
										<?php }?>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
