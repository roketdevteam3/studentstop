<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use app\widgets\FavoriterentsWidget;

use app\assets\FavoriterentAsset;
FavoriterentAsset::register($this);
$this->registerJsFile('/js/rents.js');
$this->registerCssFile('/css/ls_style.css');
$hour = 1;
?>

	<div class="row">
		<?= $this->render('right_menu'); ?>
		<div class="rent-content">
			<div class="container-fluid background-block">
				<div class="rents-tab-wrap">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs navigation-tabs no-icons" role="tablist">
						<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Recent</a></li>
						<li role="presentation"><a href="#archive" aria-controls="archive" role="tab" data-toggle="tab">Archive</a></li>
						<li role="presentation"><a href="#request" aria-controls="request" role="tab" data-toggle="tab">Request <span class="badge"><?=\app\modules\posts\models\Rentreserve::find()->where(['user_id'=>Yii::$app->user->id,'rent_type' => 'rents','status'=>0,'view'=>1])->andWhere(['!=','request',0])->count()?></span></a></li>
					</ul>
				</div>
				<!-- Tab panes -->
				<div class="tab-content rent-tab-content">
					<div role="tabpanel" class="tab-pane active" id="home">
						<div class="rent-thing-wrap">
							<?php $arrayYesNo = [0 => 'No', 1 => 'Yes']; ?>
							<?php $price_for_array = [1 => 'day', 2 => 'month', 3 => 'year'];?>
							<?php foreach($reservationRent as $rent){
								if ((strtotime($rent['reserve_from']) > time())||(strtotime($rent['reserve_to']) > time())){
									?>
									<div class="rent-thing">
										<div class="rent-thing-image">
											<?php if($rent['img_src'] == ''){ ?>
												<img class="img-responsive" src="<?= Url::home().'images/rent-default.jpg'; ?>">
											<?php }else{ ?>
												<img class="img-responsive" src="<?= Url::home().'images/posts_images/'.$rent['img_src']; ?>">
											<?php } ?>
										</div>


										<div class="" style="padding-top:10px">
											<div class="info">
												<h4 class="post-name"><?=$rent['title']?></h4>
											</div>
											<a href="#" class="btn btn-view" id="view" data-renthouse_id="<?=$rent['id']?>">View</a>

											<div class="total-rating ">
												<?= \app\widgets\rank\RankrentsWidget::widget(['rent_id' => $rent['id'], 'type' => 'rents']); ?>

											</div>
											<div class="" style="float: right">
												<div class="info star_style">
													<?= \app\widgets\rating\RatingrentalWidget::widget(['rent_id' => $rent['id'], 'type' => 'rents']); ?>

												</div>
											</div>
											<div class="views-count ">
												<?=$rent['views']?> reviews
											</div>
										</div>
										<div class="centerrer-block">
											<div class="info">
												<div class="clearfix"></div>
												<h4 class="address"><?= $rent['address']; ?></h4>
												<p class="content-thing"><?= mb_substr($rent['content'], 0, 106, "UTF8").'...'; ?></p>
											</div>
											<div class="bottom-row">
												<p class="booking">Booked <?=\app\modules\posts\models\Rentreserve::find()->where(['rent_id'=>$rent['id'],'rent_type'=>'rents'])->count()?></p>
												<div class="conditions">
	
												</div>
											</div>
										</div>

										<div class="bottom-row">
											<div class="conditions mustyle_class">
												<div class="price-rent">
													<span class="price"><?= $rent['price']; ?>$</span>
												</div>
												<div style="position: relative; top: 58px; padding-right: 22px;">
													<p class="booking col-xs-6">Booked <?=\app\modules\posts\models\Rentreserve::find()->where(['rent_id'=>$rent['id'],'rent_type'=>'rent_house'])->count()?></p>
													<?= FavoriterentsWidget::widget(['rent_id' => $rent['id']]) ?>
												</div>
											</div>
										</div>


										<?php//  var_dump($rent); ?>
	
	
									</div>
									<div class="modalRightPagesLS" id="view_rentshouse_<?=$rent['id']?>">
										<?php
										$query = new \yii\db\Query();
										$query->select(['*','id' => '{{%user}}.id'])
												->from('{{%user}}')
												->leftJoin('{{%user_info}}', '{{%user_info}}.id_user = {{%user}}.id')
												->where(['{{%user}}.id' => $rent['user_id']]);
	
										$command = $query->createCommand();
										$userPosted = $command->queryOne();
										?>
										<a class="btn btn-danger pull-right" onclick="$('#view_rentshouse_<?=$rent['id']?>').hide()"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
										<div class="col-xs-12">
											<div class="col-xs-6">
												<div class="class-user-av col-xs-6">
													<?php if ($userPosted['avatar']){?>
														<img class="img-responsive" src="<?= Url::home(); ?>images/users_images/<?= $userPosted['avatar']; ?>">
													<?php } else {?>
														<img class="img-responsive" src="<?= Url::home(); ?>images/default_avatar.jpg">
													<?php } ?>
												</div>
												<span>
													<?= $userPosted['name']; ?> <?= $userPosted['surname']; ?>
												</span>
											</div>
											<div class="col-xs-6">
												<?= \app\widgets\rating\RatingrentaluserWidget::widget(['object_id' => $userPosted['id'], 'type' => 'rental_a_house']); ?>
												<span class="price"><?= $rent['price']?>$/day</span>
											</div>
										</div>
										<div class="row">
											<!-- Nav tabs -->
											<ul class="nav nav-tabs" role="tablist">
												<li role="presentation" class="active"><a href="#description<?=$rent['id']?>" aria-controls="description<?=$rent['id']?>" role="tab" data-toggle="tab">Description</a></li>
												<li role="presentation"><a href="#map<?=$rent['id']?>" aria-controls="map<?=$rent['id']?>" role="tab" data-toggle="tab">Map</a></li>
												<li role="presentation"><a href="#images<?=$rent['id']?>" aria-controls="images<?=$rent['id']?>" role="tab" data-toggle="tab">Images</a></li>
												<li role="presentation"><a href="#calendar<?=$rent['id']?>" aria-controls="calendar<?=$rent['id']?>" role="tab" data-toggle="tab">Calendar</a></li>
												<li role="presentation"><a href="#comments<?=$rent['id']?>" aria-controls="comments<?=$rent['id']?>" role="tab" data-toggle="tab">Reviews</a></li>
											</ul>
	
											<!-- Tab panes -->
											<div class="tab-content">
												<div role="tabpanel" class="tab-pane active" id="description<?=$rent['id']?>">
													<div class="rent-thing">
														<div class="info">
															<h4 class="address"><?= $rent['address']; ?></h4>
															<p class="content-thing"><?=$rent['content'] ?></p>
														</div>
													</div>
												</div>
												<div role="tabpanel" class="tab-pane" id="map<?=$rent['id']?>">3...</div>
												<div role="tabpanel" class="tab-pane" id="images<?=$rent['id']?>">
													<?php
													$images = \app\modules\posts\models\Postsphotos::find()->where(['post_id'=>$rent['id']])->all();
													foreach($images as $image){
														echo '<img src="/images/posts_images/'.$image->image_src.'" style="max-width: 420px" />';
													}
													?>
												</div>
												<div role="tabpanel" class="tab-pane" id="calendar<?=$rent['id']?>">
													<div>
														Calendar
	
														<?php
														$events = [];
														$renteds = \app\modules\posts\models\Rentreserve::find()->where(['rent_type'=>'rents', 'rent_id'=>$rent['id']])->all();
														foreach($renteds as $rented ){
															$events[] = [
																	'title'=>'Rented',
																	'id'=>$rented->id,
																	'start' => date('Y-m-d\TH:i:s\Z', strtotime($rented->reserve_from)),
																	'end' => date('Y-m-d\TH:i:s\Z', strtotime($rented->reserve_to)),
															];
														}
														echo \yii2fullcalendar\yii2fullcalendar::widget(array(
																'clientOptions' => [
																		'defaultDate' => date('Y-m-d'),
																		'nowIndicator'=>true,
																],
																'events'=> $events,
														));
														?>
													</div>
												</div>
												<div role="tabpanel" class="tab-pane style-tb-pane-hr" id="comments<?=$rent['id']?>">
													<?php
													$commit_user = \app\modules\posts\models\Rentreserve::find()->where(['rent_id'=>$rent['id'], 'rent_type'=>'rents'])->one();
													if (!$commit_user->rating_comment) {?>
														<div>
															<?php $form = ActiveForm::begin(); ?>
															<div class="modal-body">
																<input type="hidden" name="Rating[reserve_id]" value="<?= $commit_user->id; ?>">
																<input type="hidden" name="Rating[from_user_id]" value="<?= \Yii::$app->user->id; ?>">
																<input type="hidden" name="Rating[to_object_id]" value="<?= $rent['user_id']; ?>">
																<?php echo \kartik\rating\StarRating::widget([
																		'name' => 'Rating[rating_count]',
																		'pluginOptions' => [
																				'step' => '1',
																				'size'=>'xs',
																				'showCaption' => false,
																		],
																]); ?>
																<textarea name="Rating[comment]" value="<?= $rent['user_id']; ?>"> </textarea>
															</div>
															<div class="modal-footer">
																<?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'add_reting', 'class' => 'btn btn-primary']) ?>
															</div>
															<?php $form = ActiveForm::end(); ?>
														</div>
													<?php }
													foreach($renteds as $rented){
														if ($rented->rating_comment)
															echo $rented->user['name']."<br/>".$rented->rating_comment."<br/><br/><hr><br/>";
													}
													?>
												</div>
											</div>
										</div>
									</div>
								<?php }  }?>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="archive">
						<div class="rent-thing-wrap">
							<?php $arrayYesNo = [0 => 'No', 1 => 'Yes']; ?>
							<?php $price_for_array = [1 => 'day', 2 => 'month', 3 => 'year'];?>
							<?php foreach($reservationRent as $rent){
								if (strtotime($rent['reserve_to']) < time()){
									?>
									<div class="rent-thing">
										<div class="rent-thing-image">
											<?php if($rent['img_src'] == ''){ ?>
												<img class="img-responsive" src="<?= Url::home().'images/rent-default.jpg'; ?>">
											<?php }else{ ?>
												<img class="img-responsive" src="<?= Url::home().'images/posts_images/'.$rent['img_src']; ?>">
											<?php } ?>
										</div>


										<div class="" style="padding-top:10px">
											<div class="info">
												<h4 class="post-name"><?=$rent['title']?></h4>
											</div>
											<a href="#" class="btn btn-view" id="view" data-renthouse_id="<?=$rent['id']?>">View</a>

											<div class="total-rating ">
												<?= \app\widgets\rank\RankrentsWidget::widget(['rent_id' => $rent['id'], 'type' => 'rents']); ?>

											</div>
											<div class="" style="float: right">
												<div class="info star_style">
													<?= \app\widgets\rating\RatingrentalWidget::widget(['rent_id' => $rent['id'], 'type' => 'rents']); ?>

												</div>
											</div>
											<div class="views-count ">
												<?=$rent['views']?> reviews
											</div>
										</div>
										<div class="centerrer-block">
											<div class="info">
												<div class="clearfix"></div>
												<h4 class="address"><?= $rent['address']; ?></h4>
												<p class="content-thing"><?= mb_substr($rent['content'], 0, 106, "UTF8").'...'; ?></p>
											</div>
											<div class="bottom-row">
												<p class="booking">Booked <?=\app\modules\posts\models\Rentreserve::find()->where(['rent_id'=>$rent['id'],'rent_type'=>'rents'])->count()?></p>
												<div class="conditions">

												</div>
											</div>
										</div>

										<div class="bottom-row">
											<div class="conditions mustyle_class">
												<div class="price-rent">
													<span class="price"><?= $rent['price']; ?>$</span>
												</div>
												<div style="position: relative; top: 58px; padding-right: 22px;">
													<p class="booking col-xs-6">Booked <?=\app\modules\posts\models\Rentreserve::find()->where(['rent_id'=>$rent['id'],'rent_type'=>'rent_house'])->count()?></p>
													<?= FavoriterentsWidget::widget(['rent_id' => $rent['id']]) ?>
												</div>
											</div>
										</div>


										<?php//  var_dump($rent); ?>


									</div>
									<div class="modalRightPagesLS" id="view_rentshouse_<?=$rent['id']?>">
										<?php
										$query = new \yii\db\Query();
										$query->select(['*','id' => '{{%user}}.id'])
												->from('{{%user}}')
												->leftJoin('{{%user_info}}', '{{%user_info}}.id_user = {{%user}}.id')
												->where(['{{%user}}.id' => $rent['user_id']]);

										$command = $query->createCommand();
										$userPosted = $command->queryOne();
										?>
										<a class="btn btn-danger pull-right" onclick="$('#view_rentshouse_<?=$rent['id']?>').hide()"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
										<div class="col-xs-12">
											<div class="col-xs-6">
												<div class="class-user-av col-xs-6">
													<?php if ($userPosted['avatar']){?>
														<img class="img-responsive" src="<?= Url::home(); ?>images/users_images/<?= $userPosted['avatar']; ?>">
													<?php } else {?>
														<img class="img-responsive" src="<?= Url::home(); ?>images/default_avatar.jpg">
													<?php } ?>
												</div>
												<span>
													<?= $userPosted['name']; ?> <?= $userPosted['surname']; ?>
												</span>
											</div>
											<div class="col-xs-6">
												<?= \app\widgets\rating\RatingrentaluserWidget::widget(['object_id' => $userPosted['id'], 'type' => 'rental_a_house']); ?>
												<span class="price"><?= $rent['price']?>$/day</span>
											</div>
										</div>
										<div class="row">
											<!-- Nav tabs -->
											<ul class="nav nav-tabs" role="tablist">
												<li role="presentation" class="active"><a href="#description<?=$rent['id']?>" aria-controls="description<?=$rent['id']?>" role="tab" data-toggle="tab">Description</a></li>
												<li role="presentation"><a href="#map<?=$rent['id']?>" aria-controls="map<?=$rent['id']?>" role="tab" data-toggle="tab">Map</a></li>
												<li role="presentation"><a href="#images<?=$rent['id']?>" aria-controls="images<?=$rent['id']?>" role="tab" data-toggle="tab">Images</a></li>
												<li role="presentation"><a href="#calendar<?=$rent['id']?>" aria-controls="calendar<?=$rent['id']?>" role="tab" data-toggle="tab">Calendar</a></li>
												<li role="presentation"><a href="#comments<?=$rent['id']?>" aria-controls="comments<?=$rent['id']?>" role="tab" data-toggle="tab">Reviews</a></li>
											</ul>

											<!-- Tab panes -->
											<div class="tab-content">
												<div role="tabpanel" class="tab-pane active" id="description<?=$rent['id']?>">
													<div class="rent-thing">
														<div class="info">
															<h4 class="address"><?= $rent['address']; ?></h4>
															<p class="content-thing"><?=$rent['content'] ?></p>
														</div>
													</div>
												</div>
												<div role="tabpanel" class="tab-pane" id="map<?=$rent['id']?>">3...</div>
												<div role="tabpanel" class="tab-pane" id="images<?=$rent['id']?>">
													<?php
													$images = \app\modules\posts\models\Postsphotos::find()->where(['post_id'=>$rent['id']])->all();
													foreach($images as $image){
														echo '<img src="/images/posts_images/'.$image->image_src.'" style="max-width: 420px" />';
													}
													?>
												</div>
												<div role="tabpanel" class="tab-pane" id="calendar<?=$rent['id']?>">
													<div>
														Calendar

														<?php
														$events = [];
														$renteds = \app\modules\posts\models\Rentreserve::find()->where(['rent_type'=>'rents', 'rent_id'=>$rent['id']])->all();
														foreach($renteds as $rented ){
															$events[] = [
																	'title'=>'Rented',
																	'id'=>$rented->id,
																	'start' => date('Y-m-d\TH:i:s\Z', strtotime($rented->reserve_from)),
																	'end' => date('Y-m-d\TH:i:s\Z', strtotime($rented->reserve_to)),
															];
														}
														echo \yii2fullcalendar\yii2fullcalendar::widget(array(
																'clientOptions' => [
																		'defaultDate' => date('Y-m-d'),
																		'nowIndicator'=>true,
																],
																'events'=> $events,
														));
														?>
													</div>
												</div>
												<div role="tabpanel" class="tab-pane style-tb-pane-hr" id="comments<?=$rent['id']?>">
													<?php
													$commit_user = \app\modules\posts\models\Rentreserve::find()->where(['rent_id'=>$rent['id'], 'rent_type'=>'rents'])->one();
													if (!$commit_user->rating_comment) {?>
														<div>
															<?php $form = ActiveForm::begin(); ?>
															<div class="modal-body">
																<input type="hidden" name="Rating[reserve_id]" value="<?= $commit_user->id; ?>">
																<input type="hidden" name="Rating[from_user_id]" value="<?= \Yii::$app->user->id; ?>">
																<input type="hidden" name="Rating[to_object_id]" value="<?= $rent['user_id']; ?>">
																<?php echo \kartik\rating\StarRating::widget([
																		'name' => 'Rating[rating_count]',
																		'pluginOptions' => [
																				'step' => '1',
																				'size'=>'xs',
																				'showCaption' => false,
																		],
																]); ?>
																<textarea name="Rating[comment]" value="<?= $rent['user_id']; ?>"> </textarea>
															</div>
															<div class="modal-footer">
																<?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'add_reting', 'class' => 'btn btn-primary']) ?>
															</div>
															<?php $form = ActiveForm::end(); ?>
														</div>
													<?php }
													foreach($renteds as $rented){
														if ($rented->rating_comment)
															echo $rented->user['name']."<br/>".$rented->rating_comment."<br/><br/><hr><br/>";
													}
													?>
												</div>
											</div>
										</div>
									</div>
								<?php }  }?>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="request">
						<table class="table table-rent">
							<thead>
							<th>Rent name</th>
							<th>Rent date for</th>
							<th>Rent date to</th>
							<th>Status</th>
							<th></th>
							</thead>
							<tbody>
							<?php foreach($request as $item){?>
								<tr>
									<td><?= $item->rents['title']?></td>
									<td><?= date('m-d-Y',strtotime($item->reserve_from)) ?></td>
									<td><?=date('m-d-Y',strtotime($item->reserve_to)) ?></td>
									<td>
										<?php if ($item->request == 1){
											echo 'Consider';
										} elseif($item->request == 2){
											echo 'Аpproved <a href="/rents/metod_pay?metod=1&id='.$item->id.'" class="btn btn-action">Pay with Paypal</a> ';
											if ($item->total_credits) echo ' <a href="/rents/metod_pay?metod=2&id='.$item->id.'" class="btn btn-action">Pay with Credits</a>';
										}elseif($item->request == 3){
											echo 'Rejected';
										} ?>
									</td>
									<td>
										<button class="btn btn-primary" id="view" data-renthouse_id="<?=$item->rents['id']?>">View</button>
										<?php $rent = $item->rents;?>
										<div class="modalRightPagesLS" id="view_rentshouse_<?=$rent['id']?>">
											<?php
											$query = new \yii\db\Query();
											$query->select(['*','id' => '{{%user}}.id'])
													->from('{{%user}}')
													->leftJoin('{{%user_info}}', '{{%user_info}}.id_user = {{%user}}.id')
													->where(['{{%user}}.id' => $rent['user_id']]);
	
											$command = $query->createCommand();
											$userPosted = $command->queryOne();
											?>
											<a class="btn btn-danger pull-right" onclick="$('#view_rentshouse_<?=$rent['id']?>').hide()"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
											<div class="col-xs-12">
												<div class="col-xs-6">
													<div class="class-user-av col-xs-6">
														<?php if ($userPosted['avatar']){?>
															<img class="img-responsive" src="<?= Url::home(); ?>images/users_images/<?= $userPosted['avatar']; ?>">
														<?php } else {?>
															<img class="img-responsive" src="<?= Url::home(); ?>images/default_avatar.jpg">
														<?php } ?>
													</div>
							<span>
								<?= $userPosted['name']; ?> <?= $userPosted['surname']; ?>
							</span>
												</div>
												<div class="col-xs-6">
													<?= \app\widgets\rating\RatingrentaluserWidget::widget(['object_id' => $userPosted['id'], 'type' => 'rental_a_house']); ?>
													<span class="price"><?= $rent['price']?>$/day</span>
												</div>
											</div>
											<div class="row">
	
												<!-- Nav tabs -->
												<ul class="nav nav-tabs" role="tablist">
													<li role="presentation" class="active"><a href="#description<?=$rent['id']?>" aria-controls="description<?=$rent['id']?>" role="tab" data-toggle="tab">Description</a></li>
													<li role="presentation"><a href="#map<?=$rent['id']?>" aria-controls="map<?=$rent['id']?>" role="tab" data-toggle="tab">Map</a></li>
													<li role="presentation"><a href="#images<?=$rent['id']?>" aria-controls="images<?=$rent['id']?>" role="tab" data-toggle="tab">Images</a></li>
													<li role="presentation"><a href="#calendar<?=$rent['id']?>" aria-controls="calendar<?=$rent['id']?>" role="tab" data-toggle="tab">Calendar</a></li>
													<li role="presentation"><a href="#comments<?=$rent['id']?>" aria-controls="comments<?=$rent['id']?>" role="tab" data-toggle="tab">Comments</a></li>
												</ul>
	
												<!-- Tab panes -->
												<div class="tab-content">
													<div role="tabpanel" class="tab-pane active" id="description<?=$rent['id']?>">
														<div class="rent-thing">
															<div class="info">
																<h4 class="address"><?= $rent['address']; ?></h4>
																<p class="content-thing"><?=$rent['content'] ?></p>
															</div>
														</div>
													</div>
													<div role="tabpanel" class="tab-pane" id="map<?=$rent['id']?>">3...</div>
													<div role="tabpanel" class="tab-pane" id="images<?=$rent['id']?>">
														<?php
														$images = \app\modules\posts\models\Postsphotos::find()->where(['post_id'=>$rent['id']])->all();
														foreach($images as $image){
															echo '<img src="/images/posts_images/'.$image->image_src.'" />';
														}
														?>
													</div>
													<div role="tabpanel" class="tab-pane" id="calendar<?=$rent['id']?>">
														<div>
															Calendar
	
															<?php
															$events = [];
															$renteds = \app\modules\posts\models\Rentreserve::find()->where(['rent_type'=>'rents', 'rent_id'=>$rent['id']])->all();
															foreach($renteds as $rented ){
																$events[] = [
																		'title'=>'Rented',
																		'id'=>$rented->id,
																		'start' => date('Y-m-d\TH:i:s\Z', strtotime($rented->reserve_from)),
																		'end' => date('Y-m-d\TH:i:s\Z', strtotime($rented->reserve_to)),
																];
															}
															echo \yii2fullcalendar\yii2fullcalendar::widget(array(
																	'clientOptions' => [
																			'defaultDate' => date('Y-m-d'),
																			'nowIndicator'=>true,
																	],
																	'events'=> $events,
															));
															?>
														</div>
													</div>
													<div role="tabpanel" class="tab-pane" id="comments<?=$rent['id']?>">
														<?php
														$commit_user = \app\modules\posts\models\Rentreserve::find()->where(['rent_id'=>$rent['id'], 'rent_type'=>'rents'])->one();
														if (!$commit_user->rating_comment) {?>
															<div>
																<?php $form = ActiveForm::begin(); ?>
																<div class="modal-body">
																	<input type="hidden" name="Rating[reserve_id]" value="<?= $commit_user->id; ?>">
																	<input type="hidden" name="Rating[from_user_id]" value="<?= \Yii::$app->user->id; ?>">
																	<input type="hidden" name="Rating[to_object_id]" value="<?= $rent['user_id']; ?>">
																	<?php echo \kartik\rating\StarRating::widget([
																			'name' => 'Rating[rating_count]',
																			'pluginOptions' => [
																					'step' => '1',
																					'size'=>'xs',
																					'showCaption' => false,
																			],
																	]); ?>
																	<textarea name="Rating[comment]" value="<?= $rent['user_id']; ?>"> </textarea>
																</div>
																<div class="modal-footer">
																	<?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'add_reting', 'class' => 'btn btn-primary']) ?>
																</div>
																<?php $form = ActiveForm::end(); ?>
															</div>
														<?php }
														foreach($renteds as $rented){
															if ($rented->rating_comment)
																echo "User name: ".$rented->user['name']."<br/> Comment: ".$rented->rating_comment."<br/><br/><br/>";
														}
														?>
													</div>
												</div>
	
											</div>
										</div>
									</td>
								</tr>
							<?php }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>