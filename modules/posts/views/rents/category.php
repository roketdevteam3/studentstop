<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
?>
<div class="posts-default-index">
        <div class="col-sm-12">
            <div class="well">
                <h1><?= $postCategory->category_name?></h1>
                <?php foreach($postCategory->child_category as $child_category){ ?>
                    <?php echo HTML::a($child_category->category_name, '/market/'.$postCategory->url_name.'/'.$child_category->url_name); ?>
                <?php } ?>
            </div>
        </div>
</div>
