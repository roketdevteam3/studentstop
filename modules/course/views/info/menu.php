<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    
    $class1 = ($this->context->getRoute() == 'course/info/index')?'active':''; 
    $class2 = ($this->context->getRoute() == 'course/info/videolist')?'active':''; 
    $class3 = ($this->context->getRoute() == 'course/info/mypage')?'active':''; 
?>

<ul class="nav nav-tabs nav-justified" style="margin-top:30px;">
    <li role="presentation" class="<?= $class1; ?>">
        <?= HTML::a('Classes',Url::home().'classes'); ?>
    </li>
    
    <li role="presentation" class="<?= $class2; ?>">
        <?= HTML::a('Video list',Url::home().'videolist'); ?>
    </li>
    
    <li role="presentation" class="<?= $class3; ?>">
        <?= HTML::a('My page',Url::home().'mypage'); ?>
    </li>
    
</ul>

