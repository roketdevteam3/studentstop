<?php

    use yii\bootstrap\Html;
    use yii\helpers\Url;
    use yii\widgets\LinkPager;
    use app\assets\ClassesAsset;
    ClassesAsset::register($this);
    
?>
<?= $this->render('menu'); ?>
<h1>My Classes</h1>
<?php foreach($modelClass as $class){ ?>
    <div class="well">
        <button class="btn btn-primary pull-right followClass" data-status="Unfollow" data-id_object="<?= $class['id_object']; ?>">Unfollow</button>
        <h3>
            <?= HTML::a($class['class_name'],Url::home().'class/'.$class['url_name']); ?>
        </h3>
    </div>
<?php } ?>
<div class="col-sm-12">
    <?= LinkPager::widget(['pagination'=>$pagination]); ?>
</div>