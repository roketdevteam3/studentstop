<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use yii\helpers\Url;
	use yii\widgets\LinkPager;
	use yii\bootstrap\Alert;
	use app\widgets\VideoclasscountWidget;
?>
<div class="row">
	<?php echo $this->render('right_menu'); ?>
	<div class="page-content">
		<div class="container-fluid background-block">
			<div class="row">
				<?= $this->render('header_conference'); ?>
				<div class="posts-default-index">
					<div class="container container_width_media">
						<div id="masonry_hybrid_demo2">
							<div class="grid-sizer"></div>
							<div class="gutter-sizer"></div>
							<?php foreach($majorArray as $major){ ?>

                                                                    <?php if($major['courses']){ ?>
									<div class="grid-item">
                                                                            <div class=" classified-block ">

                                                                                    <h2>
                                                                                            <?= $major['info']['name']; ?>
                                                                                    </h2>
                                                                                    <?php 
                                                                                        foreach($major['courses'] as $courses){ ?>
                                                                                        <a href="<?= Url::home(); ?>videoclasslist/<?= $courses['id']; ?>" >
                                                                                                       <div class="name_majorlist"> <?= $courses['name']; ?></div>
                                                                                                        <div class="badge">
                                                                                                                <?= VideoclasscountWidget::widget(['course_id' => $courses['id']]); ?>
                                                                                                        </div>
                                                                                                </a>
                                                                                        <?php } ?>                                                                                    
                                                                            </div>
										</div>
                                                                <?php } ?>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


