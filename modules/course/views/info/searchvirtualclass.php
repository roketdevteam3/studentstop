<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use yii\helpers\Url;
	use yii\widgets\LinkPager;
	use yii\bootstrap\Alert;
	use app\widgets\JoinconferenceWidget;
	use app\widgets\FavoritevirtualclassWidget;
	use app\assets\FollowconferenceAsset;
		use app\widgets\VideolistmembersWidget;
//        use app\widgets\JoinclassWidget;
		use app\widgets\rating\RatingvirtualclassWidget;
	FollowconferenceAsset::register($this);
?>

	<div class="row">
		<?php echo $this->render('right_menu',[]); ?>
		<div class="page-content">
			<div class="container-fluid background-block">
				<div class="row">
					<?= $this->render('header_conference'); ?>
					<div class="main-content">
						<?php foreach($videoconferences as $date => $videoconference){ ?>
							<table class="table virtualClassBlock">
								<tr>
									<td class="date-td">
										<h4 class="virtual-class-date"><?= $date; ?></h4>
									</td>
								</tr>
								<?php foreach($videoconference as $key => $class){ ?>
									<tr class="virtual-class-info">
										<td>
											<?= $key + 1; ?>
										</td>
										<td>
											<?= $class['className']; ?>; 
										</td>
										<td>
											<div class="user-img">
												<?php if($class['avatar'] != ''){ ?>
														<img src="<?= Url::home(); ?>images/users_images/<?= $class['avatar']; ?>" style='height:40px;width:40px;'>
												<?php }else{ ?>
														<img src="<?= Url::home(); ?>images/default_avatar.jpg"  style='height:40px;width:40px;'>
												<?php }  ?>
											</div>
											<span class="name">
												<?= $class['name'].' '.$class['surname']; ?>
											</span>
										</td>
										<td>
											<?php if($class['class_status'] == '1'){ ?>
													<span class="active-status">
															Active
													</span>
											<?php }else{ ?>
													<span class="no-active-status">
															Not started
													</span>
											<?php } ?>
										</td>
										<td>
											<?= VideolistmembersWidget::widget(['class_id' =>  $class['id']]) ?>
										</td>
										<td style="    position: relative;   bottom: 5px;">
											<?= RatingvirtualclassWidget::widget(['class_id' =>  $class['id'] ]); ?>
										</td>
										<td>
											<div class="start-wrap">
												<span class="start-title">
													Start:
												</span>
												<span class="start-time">
													<?php 
														$date = date_create($class['date_start']);
        													echo date_format($date, 'h:i A');
													?>
												</span>
											</div>
										</td>
										<td>
											<?= FavoritevirtualclassWidget::widget(['virtual_class_id' =>  $class['id']]) ?>
										</td>
										<td>
											<div class="price">
												<?php if(($class['price_m'] != '') && ($class['price_m'] != null)){ ?>
                                                                                                    <?=  $class['price_m'];  ?>$                                                                                           
                                                                                                <?php }else{ ?>
                                                                                                    <?=  $class['price_c'];  ?>Credits                                                                                           
                                                                                                <?php } ?>
											</div>
										</td>
										<td>
											<?= JoinconferenceWidget::widget(['conference_id' =>  $class['id']]) ?>
											<?php //= HTML::a('Watching',Url::home().'videoconference/'.$class['id'],['class' => 'btn btn-watch']); ?>
										</td>
									</tr>
								<?php } ?>
							</table>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
		
	<div class="row">
            <div id="VirtualClassInfoModal" class="modal fade" role="dialog">
              <div class="modal-dialog" style="width:70%;">
                    <div class="modal-content ">
                      <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">x</button>
                      </div>
                      <div class="modal-body style-modal-body-vir">
                            <div class="col-sm-6" style="   border-right: 1px solid #d8d8d8;">
                                <div class="style-img-username">
                                    <img class="tutorAvatar" src="<?= Url::home(); ?>images/default_avatar.jpg" style="width:21%; float:left;">
                                    <h4 class="tutorUsername" style="padding-left: 25px; font-size: 16px; padding-top: 43px; float: left;"></h4>
                                </div>
                                <div class="style-text-mar-top">
                                    <p class="vitrualClassDescription"></p>
                                    if you want to join the class you must pay  - <span class="vitrualClassPrice_M"></span><br>
                                    <span class="vitrualClassPrice_C"></span>
                                </div>
                            </div>
                            <div class="col-sm-6 style-center-text">
                                      <h2 class="vitrualClassTitle style-font-size-h2"></h2>
                                      <i class="vitrualClassDateStart style-font-size-i"></i>

                              </div>
                      </div>
                      <div class="modal-footer">
                            <div class="col-sm-6 style-co-sm-flo-rig">
                                <button type="button" class="btn btn-close" data-dismiss="modal" style="float: right;">Close</button>
                            </div>
                          <div class="col-sm-6 style-co-sm-flo-left">
                            <?php $form = ActiveForm::begin(['action'=>'/virtualclass/pay_class', 'id'=>'form_pay','options' => [
                'class' => 'moneyform'
             ]]); ?>
                              <input type="hidden" name="price" id="pay_price">
                              <input type="hidden" name="class_id" id="pay_class_id">
                              <input type="hidden" name="redirect" value="searchvirtualclass">
                              <button type="submit" class="btn btn-action" id="pay_method_paypal"  href="#" >Pay money</button>
                            <?php ActiveForm::end()?>
                            <?php $form = ActiveForm::begin(['action'=>'/virtualclass/pay_class_credits', 'id'=>'form_pay','options' => [
                'class' => 'creditsform'
             ]]); ?>
                              <input type="hidden" name="price" id="pay_price">
                              <input type="hidden" name="class_id" id="pay_class_id">
                              <input type="hidden" name="redirect" value="searchvirtualclass">
                              <button type="submit" class="btn btn-action" id="pay_method_paypal"  href="#" >Pay credits</button>
                            <?php ActiveForm::end()?>
                              
                                <?php /*$form = ActiveForm::begin(['action'=>'', 'id'=>'form_pay']); ?>
                                    <input type="hidden" name="price" id="pay_price">
                                    <input type="hidden" name="class_id" id="pay_class_id">
                                    <input type="hidden" name="redirect" value="searchvirtualclass">
                                    <button type="submit" class="btn btn-action" id="pay_method_paypal"  href="#" >Pay</button>
                                <?php ActiveForm::end() */ ?>
                          </div>
                      </div>
                    </div>

              </div>
            </div>
	</div>
