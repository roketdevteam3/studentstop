<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use yii\bootstrap\Alert;
	use yii\helpers\Url;
		use kartik\rating\StarRating;
	use yii\widgets\LinkPager;
	use kartik\datetime\DateTimePicker;
	use app\assets\StreamingownerAsset;
	use app\assets\StreamingreciverAsset;
	use app\assets\VideoconferenceviewAsset;
		VideoconferenceviewAsset::register($this);
		use app\widgets\rating\RatingvirtualclassWidget
?>

	<div class="row">
		<input type="hidden" name="vitrual_class_id" value="<?= $modelVideoconference->id; ?>">
		<?php if(($classEnd != '2') || ($modelTutorsUser->id == \Yii::$app->user->id)){ ?>
		<?php echo $this->render('right_menu_conference',['modelVideoconference' => $modelVideoconference,
			'modelTutorsUserInfo' => $modelTutorsUserInfo,
			'modelTutorsUser' => $modelTutorsUser,
			'modelTutorsInfo' => $modelTutorsInfo,
		]); ?>
		<div class="page-content">
			<div class="container-fluid background-block">
				<div class="video-page">
					<div class="row">
						<div class="col-sm-6">
							<input type="hidden" id="user_id" value="<?=\Yii::$app->user->id;?>">
							<input type="hidden" id="class_id" value="<?=$modelVideoconference->id;?>">
							<div class="message-class-container">
								<div class="send-message" >
									<div class="input-group">
										<div class="inputer inputer-blue">
												<div class="input-wrapper">
														<textarea rows="2" id="text-message-class" class="form-control js-auto-size" placeholder="Message"></textarea>
														<div class="add-files btn-ripple"></div>
												</div>
										</div>
										<!--.inputer-->
										<span class="input-group-btn">
												<button id="send-message-class" class="btn btn-ripple" type="button">Send</button>
										</span>
									</div>
								</div>
								<div class="select-file-box animated"  style="top:40px;">
										<i class="fa fa-times close-box" aria-hidden="true"></i>
										<div action="<?=Url::home();?>course/default/savedropedfile/<?=$modelVideoconference->id;?>" class="user_photos">
												<div class="dz-message needsclick">
														<h3 style="color:#0087F7;">Drop files here or click to upload.</h3>
												</div>
										</div>
								</div>
								<div class="messages"></div>
								<!--.messages-->
								<!--.send-message-->
							</div>
						</div>
						<div class="col-sm-6">
							<div class="videoconference download-div-margin">
								<div class="users-form">
									<h3 class="page-title">Videoconference</h3>
									<h4 class="page-subtitle"><?= $modelVideoconference->name; ?></h4>
									<?php if($modelVideoconference->tutors_id == \Yii::$app->user->id){ ?>
										<?php StreamingownerAsset::register($this); ?>
										<video id="localVideoC" class="video-block" data-owner-id="<?= $modelVideoconference->tutors_id ?>" autoplay muted></video>
										<style media="screen">
											.remV > video{
												width: 15%;
											}
										</style>
										<div id="remV">

										</div>
										<div class="btn-wrap">
											<button type="button" name="start" id="start">Start Stream</button>
											<div class="call-controll-v" style="display:none">
												<button type="button" class="mute-video" name="button" >Stop video</button>
												<button type="button" class="unmute-video" name="button">Start video</button>
												<button type="button" class="mute-sound" name="button" >Stop sound</button>
												<button type="button" class="unmute-sound" style="display:none" name="button">Start soun</button>
												<button type="button" class="shareScreen" style="" name="button">Start screen</button>
												<button type="button" class="unshareScreen" style="display:none" name="button">Stop screen</button>
												<button type="button" class="cancel-vc" name="button" data-type="audio">Cancel</button>
											</div>
										</div>
									<?php }else{ ?>
										<?php StreamingreciverAsset::register($this); ?>
										<style media="screen">
										#reciverScrren > video{
											width: 99%;
										}
										</style>
											<div  id="reciverScrren" class="">

											</div>
											<video id="reciver" class="video-block" height="50px" data-owner-id="<?= $modelVideoconference->tutors_id ?>" data-reciver-id="<?=\Yii::$app->user->id ?>" autoplay></video>
											<div class="call-controll-v" id="join" style="display:none">
												<button type="button" class="mute-video" name="button" >Stop video</button>
												<button type="button" class="unmute-video" name="button">Start video</button>
												<button type="button" class="mute-sound" name="button" >Stop sound</button>
												<button type="button" class="unmute-sound" style="display:none" name="button">Start soun</button>
												<!-- <button type="button" class="cancel-vc" name="button" data-type="audio">Cancel</button> -->
											</div>
											<!-- <video id="reciverScrren" class="video-block" data-owner-id="<?= $modelVideoconference->tutors_id ?>" data-reciver-id="<?=\Yii::$app->user->id ?>" autoplay></video> -->
									<?php } ?>
								</div>
                                <div class="row" >
                                    <div class="col-sm-12 text-center">
                                        <span class="download-text-style">Please download extension to share your screen</span>
                                    </div>
                                    <div class="download-btn-div-style download-btn-div-margin">
                                        <a class="download-a-style" href="/download/chrome-extension-sample.zip">
                                            <img class="download-img-style" src="/images/Google_Chrome.png" alt="">
                                        </a>
                                    </div>
                                    <div class="download-btn-div-style">
                                        <a class="download-a-style" href="/download/firefox-extension-sample.zip">
                                            <img class="download-img-style" src="/images/firefox.png" alt="">
                                        </a>
                                    </div>
                                </div>
							</div>
							<div class="rating-class-block">
								<ul class="nav nav-tabs">
									<li role="presentation" class="active topStudents">
										<a>Top 5 students</a>
									</li>
									<li role="presentation" class="topProfessor">
										<a>Top 5 tutors</a>
									</li>
									<li role="presentation" class="topClass">
										<a>Top 5 class</a>
									</li>
								</ul>
								<div class="topClassContainer">

								</div>
							</div>
							<div class="bottom-class-block">
								<div class="class-name"><?= $modelVideoconference->name; ?></div>
								<div class="rating">
									<?= RatingvirtualclassWidget::widget(['class_id' =>  $modelVideoconference->id ]); ?>
								</div>
                                                                <?php if($modelTutorsUser->id == \Yii::$app->user->id){ ?>
                                                                        <button class="btn btn-primary closeConference style-button-close-confer" data-videoclass_id="<?= $modelVideoconference->id; ?>">Close virtual class</button>
                                                                    <?php } ?>
                                                                <ul class="nav nav-tabs">
									<li class="active virtualclassStudentShow">
										<a >Students</a>
									</li>
									<li class="virtualclassInfoShow">
										<a >Vitrual class info</a>
									</li>
								</ul>
								<div class="virtualClassInfoContainer">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			<?php }else{ ?>
					<div class="showClassEnded">
						<input name="this_class_id" value="<?= $modelVideoconference->id; ?>" type="hidden">

						<div id="modalRating" class="modal fade" role="dialog">
							<div class="modal-dialog" style="width:70%;">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">&times;</button>
									</div>
									<div class="modal-body">
										<h2 style="color:#00adee">Rating Do's and Don'ts</h2>
										<div class="col-sm-12">
											<div class="col-sm-4">
												<h3 style="color:#00adee">Do</h3>
												<p style="color:white">Double check your comments before posting. Course codes must be accurate, and it doesn’t hurt to check grammar</p>
											</div>
											<div class="col-sm-4">
												<h3 style="color:#00adee">Do</h3>
												<p style="color:white">Discuss the professor’s professional abilities including teaching style and ability to convey the material clearly.</p>
											</div>
											<div class="col-sm-4">
												<h3 style="color:#00adee">Don`t</h3>
												<p style="color:white">Use profanity, name-calling, derogatory terms, definitive language, (e.g., "always","never","etc."). And, don’t claim that the professor shows bias or favoritism for or against students.</p>
											</div>
										</div>
										<div class="col-sm-12">
											<h4 style="color:#00adee">It's your turn to grade Professor Kush Jenkins.</h4>
										<?php $form = ActiveForm::begin(['method' => 'post']); ?>
											<?= $form->field($modelNewRating, 'class_id')->hiddenInput(['value' => $modelVideoconference->id])->label(false); ?>
											<?= $form->field($modelNewRating, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false); ?>
											<?= $form->field($modelNewRating, 'rate_professor')->widget(StarRating::classname(), [
												'pluginOptions' => [
													'step' => '1',
													'size'=>'xs',
													'showCaption' => false,
												],
											]); ?>

											<?=  $form->field($modelNewRating, 'prof_again')->inline(true)->radioList([
												'1' => 'Yeah',
												'2' => 'Um, no',
											]); ?>
											<?=  $form->field($modelNewRating, 'take_for_credit')->inline(true)->radioList([
												'1' => 'Yeah',
												'2' => 'Um, no',
											]); ?>
											<?=  $form->field($modelNewRating, 'textbook_use')->inline(true)->radioList([
												'1' => 'Yeah',
												'2' => 'Um, no',
											]); ?>
											<?=  $form->field($modelNewRating, 'attendance')->inline(true)->radioList([
												'1' => 'Mendatory',
												'2' => 'Non mandatory',
											]); ?>
											<?=  $form->field($modelNewRating, 'hotness')->inline(true)->radioList([
												'1' => 'Yeah',
												'2' => 'Um, no',
											]); ?>

											<?= $form->field($modelNewRating, 'more_specific')->textarea(); ?>

											<?= $form->field($modelNewRating, 'level_of_difficulty')->textarea(); ?>
										</div>
									</div>
									<div class="modal-footer">
									  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													<?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'change_user_info', 'class' => 'btn btn-primary']) ?>
										<?php ActiveForm::end();  ?>
									</div>
								</div>
							</div>
						</div>
					</div>
			<?php } ?>
	</div>
