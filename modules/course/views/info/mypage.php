<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use yii\helpers\Url;
	use yii\widgets\LinkPager;
	use yii\bootstrap\Alert;
	use kartik\datetime\DateTimePicker;
	use app\widgets\JoinconferenceWidget;
	use app\assets\TutorsprofileAsset;
	use app\widgets\FavoritevirtualclassWidget;
	use app\widgets\rating\RatingvirtualprofessorWidget;
	use app\widgets\VideolistmembersWidget;
        use app\widgets\rating\RatingvirtualclassWidget;
	TutorsprofileAsset::register($this);
//        exit;
?>
<div style="display:none;">
	<div class="previewTemplateQ">
	</div>
</div>

	<div class="row">
		<?php echo $this->render('right_menu',[]); ?>
		<div class="page-content">
			<div class="container-fluid background-block">
				<div class="row">
					<div class="tutors-content">
						<?php if($modelTutorsInfo != null){ ?>
							<div class="user-tutors-info style-no-padding-user-tut">
								<div class="user-img">
                                                                    <?php if($modelUserinfo['avatar'] != ''){ ?>
									<img class="img-responsive" src="<?= Url::home().'images/users_images/'.$modelUserinfo['avatar']; ?>">
                                                                    <?php }else{ ?>
									<img class="img-responsive" src="<?= Url::home().'images/default_avatar.jpg'; ?>">
                                                                    <?php } ?>
								</div>
								<div class="user-btn-wrap">
								<button type="button" class="btn btn-update" data-toggle="modal" data-target="#updateTutor">Update tutors profile</button>
								</div>
								<div style="float: right;    font-size: 23px; position: relative; bottom: 1px; color: green; font-weight: 600;   padding-right: 20px;">
									<td class="table-label">Rate</td>
									<td class="table-value">$<?= $modelTutorsInfo['price']; ?></td>
									</div>
								<div class="center-block" style="    position: relative; bottom: 34px;">
									<div class="user-data" style="position: relative; bottom: 12px">
										<p class="user-name">
											<?= $modelUser->name; ?>
											<?= $modelUser->surname; ?>
										</p>
                                                                                
                                                                                <?php if($modelUserRole){ ?>
                                                                                    <p class="post">
                                                                                        (<?= $modelUserRole->name; ?>)
                                                                                    </p>
                                                                                <?php } ?>
										<div class="rating-star">
											<?= RatingvirtualprofessorWidget::widget(['user_id' => $modelUser->id]); ?>
										</div>
									</div>
									<!-- <div class="user-name"> -->
										<!-- <?php //= $modelUser->surname; ?> -->
									<!-- </div> -->
									<table class="table">
										<tr>
											<td class="table-label">Birthday:</td>
											<td class="table-value"><?= $modelUserinfo['birthday']; ?></td>
											<td class="table-label">Mobile:</td>
											<td class="table-value"><?= $modelUserinfo['mobile']; ?></td>
											<td class="table-label">Address:</td>
											<td class="table-value"><?= $modelUserinfo['address']; ?></td>
										</tr>
										<tr>
											<td class="table-label">University:</td>
											<td class="table-value"><?= $modelUniversityUser['name']; ?></td>

										</tr>
									</table>
									<div class="address">
										About:
										<span><?= $modelTutorsInfo['about']; ?></span>
									</div>
								</div>
								<div class="user-btn-wrap">
									<?php // var_dump($modelUserinfo); ?>
									<div id="updateTutor" class="modal fade" role="dialog">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">x</button>
													<h4 class="modal-title">Update</h4>
												</div>
												<?php $form = ActiveForm::begin(); ?>
													<div class="modal-body">
														<?= $form->field($modelTutorsInfo, 'price')->input('number',['step' => "0.01", 'class'=>'form-control']); ?>
														<?= $form->field($modelTutorsInfo, 'about')->textarea(); ?>
                                                                                                            <div style="max-height:300px;overflow:auto">
                                                                                                                <table class="table" >
                                                                                                                    <?php foreach($courseAllArray as $key => $course){ ?>
                                                                                                                    <tr>
                                                                                                                        <?php if (in_array($course, $courseArray)){ ?>
                                                                                                                            <td><input type="checkbox" name="Tutorsinfo[course][]" checked="checked" value="<?= $key; ?>"></td>                                                                                                                            
                                                                                                                        <?php }else{ ?>
                                                                                                                            <td><input type="checkbox" name="Tutorsinfo[course][]" value="<?= $key; ?>"></td>
                                                                                                                        <?php } ?>

                                                                                                                        <td><?= $course; ?></td>
                                                                                                                    </tr>
                                                                                                                    <?php } ?>
                                                                                                                </table>
                                                                                                            </div>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
														<?= Html::submitButton( Yii::t('app', 'Update'), ['name' => 'update_tutor', 'class' => 'btn btn-action']) ?>
													</div>
												<?php ActiveForm::end();  ?>
											</div>
										</div>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
							<?php if($tutors_id == \Yii::$app->user->id){ ?>
								<button type="button" class="btn btn-add-conference" data-toggle="modal" data-target="#addConference">Add conference</button>
								<div id="addConference" class="modal fade" role="dialog">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">x</button>
												<h4 class="modal-title">New Conference</h4>
											</div>
											<?php $form = ActiveForm::begin(); ?>
												<div class="modal-body">
													<?= $form->field($modelNewVideoconference, 'tutors_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false); ?>
													<?= $form->field($modelNewVideoconference, 'img_src')->hiddenInput()->label(false); ?>
													<?= $form->field($modelNewVideoconference, 'id_university')->hiddenInput(['value' => $modelUserinfo['university_id']])->label(false); ?>
													<?= $form->field($modelNewVideoconference, 'id_major')->hiddenInput(['value' => $modelUserinfo['major_id']])->label(false); ?>
													<div class="imageConferenceWrap">
														<img class="imageConference" src="<?= Url::home(); ?>images/default_wrapper.png" style="width:100%;">
														<a class="btn btn-add-image newImageConference">Change Image</a>
													</div>
													<?= $form->field($modelNewVideoconference, 'name')->textInput(['class'=>'form-control']); ?>
													<?= $form->field($modelNewVideoconference, 'date_start')->widget(DateTimePicker::classname(), [
														'options' => ['placeholder' => 'Enter date time start...'],
														'pluginOptions' => [
															'autoclose' => true
														]
													]); ?>
													<?php //= $form->field($modelNewVideoconference, 'url_name')->textInput(['class'=>'form-control'])->label('slug'); ?>
													<?= $form->field($modelNewVideoconference, 'id_course')->dropDownList($courseArray); ?>
													<?php //= $form->field($modelNewVideoconference, 'price_type')->dropDownList(['0' => 'Credits', '1' => 'Money'])->label(false); ?>
													<?= $form->field($modelNewVideoconference, 'price_m')->textInput(['type' => 'number','class'=>'form-control', 'value' => 0, 'min' => 0])->label("Price money"); ?>
													<?= $form->field($modelNewVideoconference, 'price_c')->textInput(['type' => 'number','class'=>'form-control', 'value' => 0, 'min' => 0])->label("Price credits"); ?>
													<?= $form->field($modelNewVideoconference, 'status')->dropDownList(['0' => 'open', '1' =>'closed']); ?>
													<div class="classClosedBlock" style="display:none;clear:both;">
														<div class="tagsContainer"></div>
														<div style="clear:both;"></div>
														<div class="position:relative;">
															<div class="form-group field-videoconference-url_name">
																<label class="control-label" for="videoconference-url_name">Choise user</label>
																<input type="hidden" id="choise_user_array" name="Videoconference[choise_user]" value="">
																<input type="text" id="videoconference-choise_user" class="form-control" value="">
																<div style="clear:both;"></div>
																<div style="z-index: 999;border: 1px solid #b9b2ad;background-color: white;position: absolute;width: 100%;">
																	<ul class="choiseUserBlock">
																	</ul>
																</div>
															</div>
														</div>
													</div>
													<?= $form->field($modelNewVideoconference, 'description')->textarea(['class'=>'form-control']); ?>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
													<?= Html::submitButton( Yii::t('app', 'Save'), ['name' => 'add_conference', 'class' => 'btn btn-action']) ?>
												</div>
											<?php ActiveForm::end();  ?>
										</div>
									</div>
								</div>
							<?php } ?>
							<table class="table classBlock">
								<thead>
									<tr>
										<td>
											№
										</td>
										<td>
											Virtual class
										</td>
										<td>
											Tutor
										</td>
										<td>
											Status
										</td>
										<td>
											Students
										</td>
										<td>
											Rating
										</td>
										<td>
											Start
										</td>
										<td>
											Favorite
										</td>
										<td>
											Price
										</td>
										<td>
										</td>
									</tr>
								</thead>
								<?php foreach($modelVideoconferences as $key => $class){ ?>
									<tr>
										<td>
											<?= $key + 1; ?>
										</td>
										<td>
											<?= $class['className']; ?>; 
										</td>
										<td>
											<div class="user-img">
												<?php if($class['avatar'] != ''){ ?>
													<img src="<?= Url::home(); ?>images/users_images/<?= $class['avatar']; ?>" style='height:40px;width:40px;'>
												<?php }else{ ?>
													<img src="<?= Url::home(); ?>images/default_avatar.jpg"  style='height:40px;width:40px;'>
												<?php }  ?>
											</div>
											<span class="name">
												<?= $class['name'].' '.$class['surname']; ?>
											</span>
										</td>
										<td>
											<?php if($class['class_status'] == '1'){ ?>
												<span class="active-status">
													Active
												</span>
											<?php }else{ ?>
												<span class="active">
													Not started
												</span>
											<?php } ?>
										</td>
										<td>
											<?= VideolistmembersWidget::widget(['class_id' =>  $class['id']]) ?>
										</td>
										<td>
											<?= RatingvirtualclassWidget::widget(['class_id' =>  $class['id'] ]); ?>
										</td>
										<td>
											<span class="start-time">
												<?php
                                                                                                    $date = date_create($class['date_start']);
                                                                                                    echo date_format($date, 'h:i A');
                                                                                                ?>
											</span>
										</td>
										<td>
											<?= FavoritevirtualclassWidget::widget(['virtual_class_id' =>  $class['id']]) ?>
										</td>
										<td>
											<div class="price">
												<?php if(($class['price_m'] != '') && ($class['price_m'] != null)){ ?>
                                                                                                    <?=  $class['price_m'];  ?>$                                                                                           
                                                                                                <?php }else{ ?>
                                                                                                    <?=  $class['price_c'];  ?>Credits                                                                                           
                                                                                                <?php } ?>
											</div>
										</td>
										<td>
											<?= HTML::a('View',Url::home().'videoconference/'.$class['id'],['class' => 'btn btn-watch']); ?>
										</td>
										<?php //= JoinclassWidget::widget(['class_id' =>  $class['id']]) ?>
									</tr>
								<?php } ?>
							</table>
						<?php }else{ ?>
							<div style="width:80%;margin:30px auto;background-color: white;  padding:15px;box-shadow: 0 0 14px rgba(0,0,0,0.5);">
								<?php $form = ActiveForm::begin(); ?>
									<?= $form->field($modelBecomeTutors, 'price')->input('number',['step' => "0.01", 'class'=>'form-control border_forms']); ?>
									<?= $form->field($modelBecomeTutors, 'about')->textarea(['class'=>'form-control border_forms']); ?>
                                                                        <div style="max-height:300px;overflow:auto">
                                                                            <table class="table" >
                                                                                <?php foreach($courseAllArray as $key => $course){ ?>
                                                                                <tr>
                                                                                    <td><input type="checkbox" name="Tutorsinfo[course][]" value="<?= $key; ?>"></td>

                                                                                    <td><?= $course; ?></td>
                                                                                </tr>
                                                                                <?php } ?>
                                                                            </table>
                                                                        </div>
									<?= Html::submitButton( Yii::t('app', 'Save'), ['name' => 'become_a_tutor', 'class' => 'btn btn-primary']) ?>
								<?php ActiveForm::end();  ?>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
