<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use yii\helpers\Url;
	use yii\widgets\LinkPager;
	use yii\bootstrap\Alert;
	use app\widgets\JoinconferenceWidget;
	use app\widgets\FavoritevirtualclassWidget;
		use app\widgets\VideolistmembersWidget;
		use app\widgets\rating\RatingvirtualclassWidget;
	use app\assets\FollowconferenceAsset;
	FollowconferenceAsset::register($this);
?>

	<div class="row">
		<?php echo $this->render('right_menu',[]); ?>
		<div class="page-content">
			<div class="container-fluid background-block">
				<div class="row">
					<div class="main-content">
						<?php foreach($videoconferences as $date => $videoconference){ ?>
							<table class="table virtualClassBlock">
								<tr>
									<td class="date-td">
										<h4 class="virtual-class-date"><?= $date; ?></h4>
									</td>
								</tr>
								<?php foreach($videoconference as $key => $class){ ?>
									<tr class="virtual-class-info">
										<td>
											<?= $key + 1; ?>
										</td>
										<td>
											<?= $class['className']; ?>;
										</td>
										<td>
											<div class="user-img">
												<?php if($class['avatar'] != ''){ ?>
														<img src="<?= Url::home(); ?>images/users_images/<?= $class['avatar']; ?>" style='height:40px;width:40px;'>
												<?php }else{ ?>
														<img src="<?= Url::home(); ?>images/default_avatar.jpg"  style='height:40px;width:40px;'>
												<?php }  ?>
											</div>
											<span class="name">
												<?= $class['name'].' '.$class['surname']; ?>
											</span>
										</td>
										<td>
											<?php if($class['class_status'] == '1'){ ?>
												<span class="active-status">
													Active
												</span>
											<?php }else{ ?>
												<span class="no-active-status">
													Not started
												</span>
											<?php } ?>
										</td>
										<td>
											<?= VideolistmembersWidget::widget(['class_id' =>  $class['id']]) ?>
										</td>
										<td>
											<?= RatingvirtualclassWidget::widget(['class_id' =>  $class['id'] ]); ?>
										</td>
										<td>
											<div class="start-wrap">
												<span class="start-title">
													Start:
												</span>
												<span class="start-time">
												<?php
													$date = date_create($class['date_start']);
													echo date_format($date, 'h:i A');
												?>
												</span>
											</div>
										</td>
										<td>
											<?= FavoritevirtualclassWidget::widget(['virtual_class_id' =>  $class['id']]) ?>
										</td>
										<td>
											<div class="price">
												<?= $class['price']; ?>
											</div>
										</td>
										<td>
											<?= HTML::a('Watching',Url::home().'videoconference/'.$class['id'],['class' => 'btn btn-watch narrow']); ?>
										</td>
										<td>
											<a class="btn JoinConference narrow" data-conference_id="<?= $class['id']; ?>" >Join</a>
											<?php  /* $form = ActiveForm::begin(['method' => 'post']); ?>
												<input type="hidden" name="invite[conference_id]" value="<?= $class['id']; ?>">
												<input type="hidden" name="invite[user_id]" value="<?= \Yii::$app->user->id; ?>">
												<input type="hidden" name="invite[status]" value="accept">
												<button class="btn btn-success">Accept</button>
											<?php ActiveForm::end();  */ ?>
										</td>
										<td>
											<?php $form = ActiveForm::begin(['method' => 'post']); ?>
												<input type="hidden" name="invite[conference_id]" value="<?= $class['id']; ?>">
												<input type="hidden" name="invite[user_id]" value="<?= \Yii::$app->user->id; ?>">
												<input type="hidden" name="invite[status]" value="reject">
												<button class="btn btn-reject">Reject</button>
											<?php ActiveForm::end();  ?>
										</td>
									</tr>
								<?php } ?>
							</table>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>

		<div id="VirtualClassInfoModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
			  </div>
			  <div class="modal-body">
				  <div class="col-sm-9">
					  <h2 class="vitrualClassTitle"></h2>
					  <i class="vitrualClassDateStart"></i>
					  <p class="vitrualClassDescription"></p>
					  if you want to join the class you must pay  - <b><span class="vitrualClassPrice_M"></span><br>
                                    <span class="vitrualClassPrice_C"></span></b>
				  </div>
				  <div class="col-sm-3">
					  <img class="tutorAvatar" src="<?= Url::home(); ?>images/default_avatar.jpg" style="width:100%;">
					  <h4 class="tutorUsername"></h4>
				  </div>
			  </div>
			  <div class="modal-footer">
                              <?php $form = ActiveForm::begin(['action'=>'/virtualclass/pay_class', 'id'=>'form_pay','options' => [
                'class' => 'moneyform'
             ]]); ?>
                              <input type="hidden" name="price" id="pay_price">
                              <input type="hidden" name="class_id" id="pay_class_id">
                              <input type="hidden" name="redirect" value="invitevirtualclass">
                              <button type="submit" class="btn btn-action" id="pay_method_paypal"  href="#" >Pay money</button>
                            <?php ActiveForm::end()?>
                            <?php $form = ActiveForm::begin(['action'=>'/virtualclass/pay_class_credits', 'id'=>'form_pay','options' => [
                'class' => 'creditsform'
             ]]); ?>
                              <input type="hidden" name="price" id="pay_price">
                              <input type="hidden" name="class_id" id="pay_class_id">
                              <input type="hidden" name="redirect" value="invitevirtualclass">
                              <button type="submit" class="btn btn-action" id="pay_method_paypal"  href="#" >Pay credits</button>
                            <?php ActiveForm::end()?>
                              
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>

		  </div>
		</div>