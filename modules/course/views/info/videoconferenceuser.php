<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use yii\bootstrap\Alert;
	use yii\helpers\Url;
	use yii\widgets\LinkPager;
	use kartik\datetime\DateTimePicker;
?>

	<div class="row">
		<?php echo $this->render('right_menu_conference',['modelVideoconference' => $modelVideoconference,
			'modelTutorsUserInfo' => $modelTutorsUserInfo,
			'modelTutorsUser' => $modelTutorsUser,
			'modelTutorsInfo' => $modelTutorsInfo,
		]); ?>
		<div class="page-content">
			<div class="container-fluid background-block">
				<div class="video-page">
					<table class="table table-videoconference">
						<thead>
							<tr>
								<td>
									
								</td>
								<td>
									Name Surname
								</td>
								<td>
									Mobile
								</td>
								<td>
									Address
								</td>
								<td>
									Birthday
								</td>
							</tr>
						</thead>
						<?php foreach($modelVideoconferenceuser as $user){ ?>
							<tbody class="user-list">
								<tr>
									<td>
										<img src="<?= Url::home().'images/users_images/'.$user['avatar']; ?>" style="width:70px;">
									</td>
									<td>
										<?= $user['name'].' '.$user['surname']; ?>
									</td>
									<td>
										<?= $user['mobile']; ?>
									</td>
									<td>
										<?= $user['address']; ?>
									</td>
									<td>
										<?= $user['birthday']; ?>
									</td>
								</tr>
							</tbody>
						<?php } ?>
					</table>
				</div>
			</div>
		</div>
	</div>

