<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use yii\bootstrap\Alert;
	use yii\helpers\Url;
	use yii\widgets\LinkPager;
	use kartik\datetime\DateTimePicker;
	use app\widgets\JoinconferenceWidget;
	use app\widgets\FavoritevirtualclassWidget;
	use app\widgets\VideolistmembersWidget;
	use app\widgets\rating\RatingvirtualclassWidget;
	use app\assets\FollowconferenceAsset;
	FollowconferenceAsset::register($this);
?>

	<div class="row">
		<?php echo $this->render('right_menu'); ?>
		<div class="page-content">
			<div class="container-fluid background-block">
				<div class="row">
					<?= $this->render('header_conference'); ?>
					<div class="main-content">
						<div class="users-form">
							<table class="table videoBlock">
								<?php foreach($videoconferences as $date => $videoconference){ ?>
										<tr>
											<td class="date-td">
												<h4 class="videolist-date"><?= $date; ?></h4>
											</td>
										</tr>
									<?php foreach($videoconference as $key => $class){ ?>
										<tr class="video-info">
											<td>
												<?= $key + 1; ?>
											</td>
											<td>
												<?= $class['className']; ?>; 
											</td>
											<td>
												<div class="user-img">
													<?php if($class['avatar'] != ''){ ?>
															<img src="<?= Url::home(); ?>images/users_images/<?= $class['avatar']; ?>" style='height:40px;width:40px;'>
													<?php }else{ ?>
															<img src="<?= Url::home(); ?>images/default_avatar.jpg"  style='height:40px;width:40px;'>
													<?php }  ?>
												</div>
												<span class="name">
														<?= $class['name'].' '.$class['surname']; ?>
												</span>
											</td>
											<td>
												<?php if($class['class_status'] == '1'){ ?>
													<span class="active-status">
														Active
													</span>
												<?php }else{ ?>
													<span class="no-active-status">
														Not started
													</span>
												<?php } ?>
											</td>
											<td>
												<?= VideolistmembersWidget::widget(['class_id' =>  $class['id']]) ?>
											</td>
											<td>
												<?= RatingvirtualclassWidget::widget(['class_id' =>  $class['id'] ]); ?>
											</td>
											<td>
												<div class="start-wrap">
													<span class="start-title">
														Start:
													</span>
													<span class="start-time">
														<?php 
															$date = date_create($class['date_start']);
															echo date_format($date, 'h:i A');
														?>
													</span>
												</div>
											</td>
											<td>
												<?= FavoritevirtualclassWidget::widget(['virtual_class_id' =>  $class['id']]) ?>
											</td>
											<td>
												<div class="price">
													<?= $class['price']; ?>
												</div>
											</td>
											<td>
												<?= HTML::a('Watching',Url::home().'videoconference/'.$class['id'],['class' => 'btn btn-watch']); ?>
											</td>
											<?php //= JoinclassWidget::widget(['class_id' =>  $class['id']]) ?>
										</tr>
									<?php } ?>
								<?php } ?>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
