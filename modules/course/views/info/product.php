<?php
    use yii\bootstrap\ActiveForm;
    use yii\bootstrap\Html;
    use yii\helpers\Url;
    use yii\widgets\LinkPager;
    use yii\bootstrap\Alert;
?>
<div class="users-form">
    <div class="row" style='background: url("../../web/images/bg.jpg") no-repeat;background-size: cover;background-position: center center;'>  
        <div class="col-sm-10" style="padding:0px;padding-top:20px;">
            <div class="col-sm-9">
                <div class="well">
                    <h4>
                        <?= HTML::a('<span class="fa fa-arrow-left"> </span> Back to all products','/class/'.$modelProduct['classUrlName'].'/classproducts',['class' => 'btn btn-primary']); ?>
                    </h4>
                    <h1 style="text-align:center;"><?= $modelProduct['title']; ?></h1>
                    <div class="col-sm-12">
                        <?= $modelProduct['description']; ?>
                    </div>

                    <div class="col-sm-12">
                        <?php $count = strpos($modelProduct['file_src'], '/', 1) + 1; ?>
                        <?php if(($modelProductpay != null) || (\Yii::$app->user->id == $modelProduct['user_id'])){ ?>
                            <?= HTML::a(substr($modelProduct['file_src'],$count,500),Url::home().'files/users_files/'.$modelProduct['file_src']); ?>
                        <?php }else{ ?>
                            <?= substr($modelProduct['file_src'],$count,500); ?>
                            <?php if(\Yii::$app->user->id != $modelProduct['user_id']){ ?>
                                <button class="btn btn-primary" data-toggle="modal" data-target="#buyProduct"><span class="fa fa-money"></span></button>

                                <div class="modal fade" id="buyProduct" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                              <h1 style="text-align: center">Buy product</h1>
                                            </div>
                                            <div class="modal-body">
                                                <span>Pay via paypal - </span><button class="btn btn-primary"><span class="fa fa-cc-paypal"></span></button><br><br>
                                                <span>Pay via credites - </span><button class="btn btn-primary"><span class="fa fa-credit-card-alt"></span></button>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <div style="clear:both;"></div>
                </div>
            </div>
            <div class="col-sm-3">
                <img src="<?= Url::home(); ?>images/users_images/<?= $modelProduct['avatar']; ?>" style="width:100%;">
                <h4><?= $modelProduct['userName'].' '.$modelProduct['surname']; ?></h4>
                <?php if(\Yii::$app->user->id != $modelProduct['user_id']){ ?>
                    <button class="btn pull-right btn-primary startChat" style="width:100%;" data-sender-id="<?=\Yii::$app->user->id?>" data-recipient-id="<?= $modelProduct['user_id']; ?>">Write <i class="fa fa-envelope"></i></button>
                <?php } ?>
            </div>
        </div>
        <div class="col-sm-2" style='padding: 0px;background-color:#525274;position:relative;'>
            <div class="col-sm-12" style='padding:0px;'>
                <div class="col-sm-12" style="padding:0px;background-color:#525274;min-height:100vh">
                    <?php echo $this->render('/class/right_menu',[
                        'modelProfessor' => $modelProfessor,
                        'modelClass' => $modelClass,
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
