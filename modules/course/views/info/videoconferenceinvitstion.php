<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use yii\bootstrap\Alert;
	use yii\helpers\Url;
	use yii\widgets\LinkPager;
	use kartik\datetime\DateTimePicker;
	use app\widgets\InviteconferenceWidget;
	use app\assets\VideoconferenceviewAsset;
	VideoconferenceviewAsset::register($this);
?>

	<div class="row">
		<?php echo $this->render('right_menu_conference',['modelVideoconference' => $modelVideoconference,
			'modelTutorsUserInfo' => $modelTutorsUserInfo,
			'modelTutorsUser' => $modelTutorsUser,
			'modelTutorsInfo' => $modelTutorsInfo,
		]); ?>
		<div class="page-content">
			<div class="container-fluid background-block">
				<div class="row">
					<?php $form = ActiveForm::begin(['method' => 'get',
													'options' => [
														'class' => 'top-filter'
													]
					]); ?>
						<div class="form-group">
							<input type="text" class="form-control" name="userNameSurname">
						</div>
						<div class="form-group">
							<?php 
								$university = '';
								if(isset($_GET['university_class'])){
										$university = $_GET['university_class'];
								}else{
										$university= null;
								}
							?>
							<?= Html::dropDownList('university_class', $university, $universityArray,['prompt' => 'University','class' => 'form-control']); ?>
						</div>
						<div class="form-group">
							<?php 
								$major = '';
								if(isset($_GET['major_class'])){
										$major = $_GET['major_class'];
								}else{
										$major= null;
								}
							?>
							<?= Html::dropDownList('major_class', $major, $majorArray,['prompt' => 'Major','class' => 'form-control']); ?>
						</div>
						<div class="form-group">
							<?php 
								$course = '';
								if(isset($_GET['course_class'])){
										$course = $_GET['course_class'];
								}else{
										$course= null;
								}
							?>
							<?= Html::dropDownList('course_class', $course, $courseArray,['prompt' => 'Course','class' => 'form-control']); ?>
						</div>
						<div class="form-group">
							<button class="btn" name="filterButton">Filter</button>
						</div>
					<?php ActiveForm::end();  ?>
					<div class="main-content videoconference">
						<div class="users-form">
							<div class="table table-videoconference">
								<div class="table-row thead" style="line-height: 6; height: 81px">
									<div class="table-cell">
									</div>
									<div class="table-cell">
										Name Surname
									</div>
									<div class="table-cell">
										Mobile
									</div>
									<div class="table-cell">
										Address
									</div>
									<div class="table-cell">
										Birthday
									</div>
									<div class="table-cell">
									</div>
								</div>
								<?php foreach($modelUsers as $user){ ?>
									<div class="table-row user-list sviat_userlist ">
										<div class="table-cell">
											<?php if($user['avatar'] != ''){ ?>
												<img src="<?= Url::home().'images/users_images/'.$user['avatar']; ?>" style="width:70px; height: 81px;">
											<?php }else{ ?>
												<img src="<?= Url::home(); ?>images/default_avatar.jpg" style="width:70px; height: 81px;">
											<?php } ?>
										</div>
										<div class="table-cell">
											<?= $user['name'].' '.$user['surname']; ?>
										</div>
										<div class="table-cell">
											<?= $user['mobile']; ?>
										</div>
										<div class="table-cell">
											<?= $user['address']; ?>
										</div>
										<div class="table-cell">
											<?= $user['birthday']; ?>
										</div>
										<div class="table-cell">
											<a href="<?= Url::home(); ?>profile/<?= $user['id']; ?>" class="btn btn-view-profile">View profile</a>
											<?= InviteconferenceWidget::widget(['conference_id' =>  $modelVideoconference['id'], 'user_id' => $user['id']]); ?>
										</div>
									</div>
								<?php } ?>
							</div>
						</div>
						<div class="col-sm-12 text-center">
							<?= LinkPager::widget(['pagination'=>$pagination]);  ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

