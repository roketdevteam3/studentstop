<?php

	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\widgets\ActiveForm;
	use yii\bootstrap\Alert;
		use app\widgets\FilterclassesWidget;
		use app\widgets\TutorsstatusWidget;
		use app\assets\ConferencerightmenuAsset;
		ConferencerightmenuAsset::register($this);
		use app\widgets\FilterconferenceWidget;
		use app\widgets\InvitevideocountWidget;
		
		$class1 = '';
		$class2 = '';
		$class3 = '';
		$class4 = '';
?>

	<div class="courseclases-menu">
		<nav class="menu-right top">
			<li role="presentation" class="<?= $class1; ?>">
				<?php echo HTML::a('Dashboard', Url::home().'videolist'); ?>
			</li>
			<li role="presentation" class="<?= $class2; ?>">
				<?php echo HTML::a('Search', Url::home().'searchvirtualclass'); ?>
			</li>
			<li role="presentation" class="<?= $class2; ?>">
				<?php echo HTML::a('Favorite', Url::home().'favoritevirtualclass'); ?>
			</li>
			<li role="presentation" class="<?= $class2; ?>">
				<?php echo HTML::a('Invite <span class="badge" style="position: relative;top: -8px;right: -3px;width: 16px;height: 16px;padding: 0;font-size: 10px;line-height: 17px;text-indent: -1px;font-weight: normal;border-radius: 20px;background-color: #f75c62;">'.InvitevideocountWidget::widget().'</span>', Url::home().'invitevirtualclass'); ?>
			</li>
			<li role="presentation" class="<?= $class2; ?>">
				<?php echo HTML::a('Archived', Url::home().'archivedvirtualclass'); ?>
			</li>
			<?= TutorsstatusWidget::widget() ?> 
			<!--<li role="presentation" class="<?php //= $class3; ?>">
				<?php //echo HTML::a('Market', Url::home().'marketclass'); ?>
			</li>
			<li role="presentation" class="<?php //= $class4; ?>">
				<?php // echo HTML::a('Watching', Url::home().'watching'); ?>
			</li> -->
		</nav>
			<?= FilterconferenceWidget::widget() ?> 
	</div>