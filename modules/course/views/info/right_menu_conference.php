<?php

	use yii\helpers\Html;
	use yii\helpers\Url;
	use yii\widgets\ActiveForm;
	use yii\bootstrap\Alert;
		use app\widgets\FilterclassesWidget;
		use app\widgets\TutorsstatusWidget;
		use app\assets\ConferencerightmenuAsset;
		ConferencerightmenuAsset::register($this);
		use app\widgets\FilterconferenceWidget;
		use app\widgets\rating\RatingvirtualprofessorWidget;
		
		$class1 = '';
		$class2 = '';
		$class3 = '';
		$class4 = '';
?>

	<div class="user-right-panel">
		<div class="user-av">
                    <?php if(($modelTutorsUserInfo->avatar != '') &&($modelTutorsUserInfo->avatar != null)){ ?>
			<img class="img-responsive" src="<?= Url::home().'images/users_images/'.$modelTutorsUserInfo->avatar; ?>">
                    <?php  }else{ ?>
                        <img class="img-responsive search-jobs-img-size" src="<?= Url::home(); ?>images/default_avatar.jpg">
                    <?php  } ?>
		</div>
		<div class="rating-blok">
			<span style="font-size: 14px;">Rating:</span>
			<?= RatingvirtualprofessorWidget::widget(['user_id' => $modelTutorsUserInfo->id_user]) ?> 

		</div>
		<div class="menu-wrapper">
			<div class="user-info">
				<span class="info-label">Name: </span>
				<span class="info-user">
					<?= $modelTutorsUser['name']; ?> <?= $modelTutorsUser['surname']; ?>
				</span>
				<span class="info-label">Birthday: </span>
				<span class="info-user">
					<?= $modelTutorsUserInfo['birthday']; ?>
				</span>
				<span class="info-label">Phone: </span>
				<span class="info-user">
					<?= $modelTutorsUserInfo['mobile']; ?>
				</span>
				<span class="info-label">Email: </span>
				<span class="info-user">
					<?= $modelTutorsUser['email']; ?>
				</span>
			</div>
			<nav class="menu-right bottom-menu">
				<ul>
					<li role="presentation" class="<?= $class1; ?>">
						<?php echo HTML::a('Chat', Url::home().'videoconference/'.$modelVideoconference->id.'/chat'); ?>
					</li>
					<li role="presentation" class="<?= $class2; ?>">
						<?php echo HTML::a('Users', Url::home().'videoconference/'.$modelVideoconference->id.'/users'); ?>
					</li>
					<li role="presentation" class="<?= $class3; ?>">
						<?php echo HTML::a('Dashboard', Url::home().'videoconference/'.$modelVideoconference->id); ?>
					</li>
					<?php if($modelTutorsUser['id'] == \Yii::$app->user->id){ ?>
						<li role="presentation" class="<?= $class2; ?>">
							<?php echo HTML::a('Invitation', Url::home().'videoconference/'.$modelVideoconference->id.'/invitation'); ?>
						</li>
					<?php } ?>
				</ul>
			</nav>
			<div class="col-sm-12">
				<?php //= FilterconferenceWidget::widget() ?> 
			</div>
		</div>
	</div>