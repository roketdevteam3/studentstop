<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use yii\bootstrap\Alert;
	use yii\helpers\Url;
	use yii\widgets\LinkPager;
		
		use app\assets\ClassrightblockAsset;
		ClassrightblockAsset::register($this);
?>
	<div style="display:none">
		<div class="table table-striped previewTemplateFiles" >
			<div id="template" class="file-row">
				<!-- This is used as the file preview template -->
				<div class="new-photo">
					<span class="preview"><img data-dz-thumbnail /></span>
					<div class="photo-name">
						<p class="name" data-dz-name></p>
						<strong class="error text-danger" data-dz-errormessage></strong>
						<p class="size" data-dz-size></p>
					</div>
				</div>
				<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
					<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
				</div>
				<button data-dz-remove class="btn delete">
					<i class="glyphicon glyphicon-trash"></i>
					<span>Delete</span>
				</button>
			</div>
		</div>
	</div>

	<div class="row">
		<?php echo $this->render('right_menu',[
			'modelProfessor' => $modelProfessor,
			'modelClass' => $modelClass,
		]); ?>
		<div class="page-content">
			<div class="container-fluid background-block">
				<div class="class-page">
					<div class="row">
						<div class="col-sm-6">

							<div class="modal padding-body fade" id="addNewFiles" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">x</button>
											<h4 class="modal-title">Add class</h4>
										</div>
										<?php $form = ActiveForm::begin(['method' => 'post']); ?>
											<div class="modal-body">
												<?= $form->field($modelNewFiles, 'class_id')->hiddenInput(['value' => $modelClass->id])->label(false); ?>
												<?= $form->field($modelNewFiles, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false); ?>
												<?= $form->field($modelNewFiles, 'file_type')->hiddenInput(['value' => 'files'])->label(false); ?>
												<?= $form->field($modelNewFiles, 'description')->textarea(); ?>
												<a class="btn btn-action newFiles">Add new Files</a>
												<div class="row user_class_new_file">
												</div>
												<?= $form->field($modelNewFiles, 'file_src')->hiddenInput()->label(false); ?>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-close" data-dismiss="modal">Cancel</button>
													<?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'change_user_info', 'class' => 'btn btn-action']) ?>
											</div>
										<?php ActiveForm::end();  ?>
									</div>
								</div>
							</div>
							<table class="table file-table">
								<thead>
									<tr>
										<td>User</td>
										<td>File</td>
										<td>Description</td>
										<td colspan="2" class="no-padding"><a class="btn btn-add-files button_add_file" data-toggle="modal" data-target="#addNewFiles">Add Files</a>
										</td>
									</tr>
								</thead>
								<?php foreach($modelFiles as $files){ ?>
									<tbody>
										<tr>
											<td>
                                                <?= $files['name'].' '.$files['surname']; ?></td>
											<td>
                                                <a href="<?= Url::home().'files/classes/'.$files['file_src']; ?>"><img src="/web/images/download_img.svg" style="height: 20px" ></td></a>
												<?php $count = strpos($files['file_src'], '/', 1) + 1; ?>
<!--												--><?//= HTML::a(substr($files['file_src'],$count,500), ); ?>
                                                <?= HTML::a  (substr($files , $count,500),Url::home().'files/classes/'.$files['file_src']); ?>

                                            </td>
											<td><?= $files['description']; ?></td>
											<td><?= $files['fileCreate']; ?></td>
											<?php if($files['user_id'] == \Yii::$app->user->id){ ?>
												<td>
													<?= HTML::a('<span class="fa fa-times fa-2x"></span>',Url::home().'filesdelete/'.$files['id'],['class' => 'pull-right']); ?>
												</td>
											<?php } ?>
										</tr>
									</tbody>
								<?php } ?>
							</table>
							<?= LinkPager::widget(['pagination'=>$pagination]);  ?>
						</div>
						<div class="col-sm-6">
							<input type="hidden" id="user_id" value="<?=\Yii::$app->user->id;?>">
							<input type="hidden" id="class_id" value="<?=$modelClass->id;?>">
							<?php if($modelClassFilesImage){ ?>
								<div id="myCarousel" class="class-slider carousel slide" data-ride="carousel">
									<!-- Indicators -->
									<ol class="carousel-indicators">
									<?php foreach($modelClassFilesImage as $key => $image){ ?>
										<?php if($key == '0'){ ?>
											<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
										<?php }else{ ?>
											<li data-target="#myCarousel" data-slide-to="<?= $key; ?>" ></li>
										<?php } ?>
									<?php } ?>
									</ol>
									<!-- Wrapper for slides -->
									<div class="carousel-inner" role="listbox">
										<?php foreach($modelClassFilesImage as $key => $image){ ?>
										<?php $classsActive = ''; ?>
										<?php if($key == '0'){ ?>
											<?php $classsActive = ' active '; ?>
										<?php } ?>
										<div class="item <?= $classsActive; ?>">
											<img class="img-responsive slider-img" src="../../files/<?= $modelClass->id;?>/<?= $image['file_src']; ?>">
										</div>
										<?php } ?>
									</div>
									<!-- Left and right controls -->
									<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
										<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
										<span class="sr-only">Previous</span>
									</a>
									<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
										<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
										<span class="sr-only">Next</span>
									</a>
								</div>
							<?php } ?>
							<div class="rating-class-block">
								<ul class="nav nav-tabs">
									<li role="presentation" class="active topStudents">
										<a>Top 5 students</a>
									</li>
									<li role="presentation" class="topProfessor">
										<a>Top 5 professor</a>
									</li>
									<li role="presentation" class="topClass">
										<a>Top 5 class</a>
									</li>
								</ul>
								<div class="topClassContainer">
									
								</div>
							</div>
							<div class="bottom-class-block">
								<?= $this->render('class_info',[
											//'modelProfessor' => $modelProfessor,
											'modelMajor' => $modelMajor,
											'modelUniversity' => $modelUniversity,
											'modelCourse' => $modelCourse,
											'modelClass' => $modelClass,
											'followStatus' => $followStatus,
											'userArray' => $userArray,
									]); ?>
								<ul class="nav nav-tabs">
									<li class="active classStudentShow">
										<a href="">Students</a>
									</li>
									<li class="classInfoShow">
										<a href="">Class info</a>
									</li>
								</ul>
								<div class="ClassInfoContainer">
								</div>
							</div>

							<div class="button_compleat">
								<?php
								if(Yii::$app->session->hasFlash('files_added')):
									echo Alert::widget([
										'options' => [
											'class' => 'alert-info button_comleat_class',
										],
										'body' => 'Files added',
									]);
								endif;
								if(Yii::$app->session->hasFlash('files_not_added')):
									echo Alert::widget([
										'options' => [
											'class' => 'alert-error',
										],
										'body' => 'Files not added',
									]);
								endif;
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
