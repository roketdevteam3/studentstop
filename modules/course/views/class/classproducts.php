<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use yii\helpers\Url;
	use yii\widgets\LinkPager;
	use yii\bootstrap\Alert;
		
		use app\assets\ClassrightblockAsset;
		ClassrightblockAsset::register($this);
?>
<style>
    .modal select {
        max-width: 303px;
        height: 30px;
        line-height: 30px;
        display: inline-block;
        float: right;
        border: 1px solid #d2d2d2;
    }
    #modalNewProduct select {
        max-width: 870px;
        width: 100%;
    }
</style>
	<div style="display:none">
		<div class="table table-striped previewTemplateFiles" >
			<div id="template" class="file-row">
				<!-- This is used as the file preview template -->
				<div class="new-photo">
					<span class="preview"><img data-dz-thumbnail /></span>
					<div class="photo-name">
						<p class="name" data-dz-name></p>
						<strong class="error text-danger" data-dz-errormessage></strong>
						<p class="size" data-dz-size></p>
					</div>
				</div>
				<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
					<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
				</div>
				<button data-dz-remove class="btn delete">
					<i class="glyphicon glyphicon-trash"></i>
					<span>Delete</span>
				</button>
			</div>
		</div>
	</div>

	<div class="row">
		<?php echo $this->render('right_menu',[
			'modelProfessor' => $modelProfessor,
			'modelClass' => $modelClass,
		]); ?>
		<div class="page-content">
			<div class="container-fluid background-block">
				<div class="class-page">
					<div class="row">
						<div class="col-sm-6">
							<?php
								if(Yii::$app->session->hasFlash('product_added')):
									echo Alert::widget([
										'options' => [
											'class' => 'alert-info',
										],
										'body' => 'Product added',
									]);
								endif; 
								if(Yii::$app->session->hasFlash('product_delete')):
									echo Alert::widget([
										'options' => [
											'class' => 'alert-info',
										],
										'body' => 'Product deleted',
									]);
								endif; 
								if(Yii::$app->session->hasFlash('product_notadded')):
									echo Alert::widget([
										'options' => [
											'class' => 'alert-error',
										],
										'body' => 'Product not added',
									]);
								endif;

								if(Yii::$app->session['pay_cansel']):
                                    Yii::$app->session['pay_cansel']=null;
                                    echo '<script> $(document).ready(function(){ swal("You cancel payment service!", "", "warning")});</script>';
                                endif;
								if(Yii::$app->session['pay_ok']):
                                    echo '<script> $(document).ready(function(){ swal("Thank you! Your paymend product!", "", "success")});</script>';
                                    Yii::$app->session['pay_ok']=null;
                                endif;
								if(Yii::$app->session['not_credits']):
                                    Yii::$app->session['not_credits']=null;
                                    echo '<script> $(document).ready(function(){ swal("Not credits!", "", "warning")});</script>';
                                endif;
							?>
							<button class="btn btn-add-product" data-toggle="modal" data-target="#modalNewProduct">Add new product</button>
							<div class="modal fade" id="modalNewProduct" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">x</button>
											<h4 class="modal-title">New Product</h4>
										</div>
										<div class="modal-body">
											<?php $form = ActiveForm::begin(); ?>
												<?= $form->field($modelNewProduct, 'class_id')->hiddenInput(['value'=>$modelClass->id])->label(false); ?>
												<?= $form->field($modelNewProduct, 'user_id')->hiddenInput(['value'=>\Yii::$app->user->id])->label(false); ?>
												<?= $form->field($modelNewProduct, 'title')->textInput(); ?>
												<?= $form->field($modelNewProduct, 'description')->textarea(['rows' => '6']); ?>
												<?= $form->field($modelNewProduct, 'price_type')->dropDownList(['0' => 'Credits', "1" => 'Paypal'], ['style' => ' ']); ?>
												<?= $form->field($modelNewProduct, 'price')->textInput(); ?>
												<?php if(count($myFilesArray) != 0){ ?>
													<?= $form->field($modelNewProduct, 'file_id')->dropDownList($myFilesArray,['prompt' => 'My Files']); ?>
													<h2>or</h2>
												<?php } ?>
												<a class="btn btn-primary newFilesProduct btn-action">Add new Files</a>
												<div class="row user_class_new_file_product">
												</div>
												<input type="hidden" class="filesIdProduct" name="ProductFile">
												<?php //= $form->field($modelNewProduct, 'file_id')->hiddenInput(['class' => 'filesIdProduct']); ?>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-close" data-dismiss="modal">Cancel</button>
												<?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'change_user_info', 'class' => 'btn btn-action']) ?>
											<?php ActiveForm::end();  ?>
										</div>
									</div>
								</div>
							</div>
							<?php foreach($modelProduct as $product){ ?>
                                                                <div class="product-description">
									<h4>
										<?= HTML::a($product['title'], Url::home().'product/'.$product['id'].'/'.$modelClass->url_name); ?>
										<span class="date"><?= $product['productDatecreate']; ?></span>
										<div class="clearfix"></div>
									</h4>
									<span class="by-author">
										by <?= $product['name'].$product['surname']; ?><br>
									</span>
                                                                        <?php if(\Yii::$app->user->id != $product['user_id']){ ?>
                                                                            <button class="btn btn-primary" data-toggle="modal" data-target="#buyProduct"><span class="fa fa-money"></span></button>

                                                                            <div class="modal fade" id="buyProduct" role="dialog">
                                                                                <div class="modal-dialog">
                                                                                    <div class="modal-content">
                                                                                        <div class="modal-header">
                                                                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                          <h1 style="text-align: center">Buy product</h1>
                                                                                        </div>
                                                                                        <div class="modal-body">
                                                                                            <span>Pay via paypal - </span><a href="/product/pay_product?product_id=<?=$product['id']?>&price=<?=$product['price']?>&redirect=class/<?=$_GET['class_name']?>/classproducts"  class="btn btn-primary"><span class="fa fa-cc-paypal"></span></a><br><br>
                                                                                            <span>Pay via credites - </span><a href="/product/pay_product_credits?product_id=<?=$product['id']?>&price=<?=$product['price']?>&redirect=class/<?=$_GET['class_name']?>/classproducts"  class="btn btn-primary"><span class="fa fa-credit-card-alt"></span></a>
                                                                                        </div>
                                                                                        <div class="modal-footer">
                                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        <?php } ?>
									<?php 
										if(strlen($product['description']) > 300){
											$end_str = '...';
										}else{
											$end_str = '';
										} 
									?>
									<?= substr($product['description'], 0, 300).$end_str; ?>
								</div>
							<?php } ?>
							<?= LinkPager::widget(['pagination'=>$pagination]);  ?>
						</div>
						<div class="col-sm-6">
							<input type="hidden" id="user_id" value="<?=\Yii::$app->user->id;?>">
							<input type="hidden" id="class_id" value="<?=$modelClass->id;?>">
							<?php if($modelClassFilesImage){ ?>
								<div id="myCarousel" class="products-slider carousel slide" data-ride="carousel">
									<!-- Indicators -->
									<ol class="carousel-indicators">
									<?php foreach($modelClassFilesImage as $key => $image){ ?>
										<?php if($key == '0'){ ?>
											<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
										<?php }else{ ?>
											<li data-target="#myCarousel" data-slide-to="<?= $key; ?>" ></li>
										<?php } ?>
									<?php } ?>
									</ol>
									<!-- Wrapper for slides -->
									<div class="carousel-inner" role="listbox">
										<?php foreach($modelClassFilesImage as $key => $image){ ?>
										<?php $classsActive = ''; ?>
										<?php if($key == '0'){ ?>
											<?php $classsActive = ' active '; ?>
										<?php } ?>
										<div class="item <?= $classsActive; ?>">
											<img class="img-responsive slider-img" src="../../files/<?= $modelClass->id;?>/<?= $image['file_src']; ?>">
										</div>
										<?php } ?>
									</div>
									<!-- Left and right controls -->
									<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
										<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
										<span class="sr-only">Previous</span>
									</a>
									<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
										<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
										<span class="sr-only">Next</span>
									</a>
								</div>
							<?php } ?>
							<div class="rating-class-block">
								<ul class="nav nav-tabs">
									<li role="presentation" class="active topStudents">
										<a>Top 5 students</a>
									</li>
									<li role="presentation" class="topProfessor">
										<a>Top 5 professor</a>
									</li>
									<li role="presentation" class="topClass">
										<a>Top 5 class</a>
									</li>
								</ul>
								<div class="topClassContainer">
									
								</div>
							</div>
							<div class="bottom-class-block">
								<?= $this->render('class_info',[
											//'modelProfessor' => $modelProfessor,
											'modelMajor' => $modelMajor,
											'modelUniversity' => $modelUniversity,
											'modelCourse' => $modelCourse,
											'modelClass' => $modelClass,
											'followStatus' => $followStatus,
											'userArray' => $userArray,
									]); ?>
								<ul class="nav nav-tabs">
									<li class="active classStudentShow">
										<a href="">Students</a>
									</li>
									<li class="classInfoShow">
										<a href="">Class info</a>
									</li>
								</ul>
								<div class="ClassInfoContainer">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
