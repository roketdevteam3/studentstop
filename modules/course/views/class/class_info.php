<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use yii\bootstrap\Alert;
	use yii\helpers\Url;
	use yii\widgets\LinkPager;
	use app\widgets\JoinclassWidget;
	use app\widgets\ClassmembersWidget;
	use app\assets\ClassesAsset;
	ClassesAsset::register($this);
        use app\widgets\rating\RatingclassWidget;
?>
        <?php
                if(Yii::$app->session->hasFlash('class_update')):
                        echo Alert::widget([
                                'options' => [
                                        'class' => 'alert-info',
                                ],
                                'body' => 'Class update',
                        ]);
                endif; 
                if(Yii::$app->session->hasFlash('class_notupdate')):
                        echo Alert::widget([
                                'options' => [
                                        'class' => 'alert-error',
                                ],
                                'body' => 'Class not update',
                        ]);
                endif; 
        ?>
        <div class="class-head">
        	<div class="class-name">
            	<?= $modelClass['name']; ?>
        	</div>
                
		<?php if($modelClass['creator'] == \Yii::$app->user->id){ ?>
                    <button class="btn btn-update-class" data-toggle="modal" data-target="#updateClass"></button>
                <?php } ?>
        	
                <div class="right-block">
	        	<div class="rating">Rating: <?= RatingclassWidget::widget(['class_id' => $modelClass['id'], 'type_show' => 'without_rat']); ?></div>
	            <?= JoinclassWidget::widget(['class_id' =>  $modelClass['id']]) ?>
        	</div>
            <input type="hidden" name="ClassInfoId" value="<?= $modelClass['id']; ?>">
        </div>
	<!--<div class="course-default-index"> -->
		<?php if($modelClass['creator'] == \Yii::$app->user->id){ ?>
			<div class="modal big-modal padding-body fade" id="updateClass" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">x</button>
							<h4 class="modal-title">Update class</h4>
						</div>
						<?php $form = ActiveForm::begin(['method' => 'post', 'action'=> Url::home().'updateclass/'.$modelClass->id]); ?>
							<div class="modal-body">
								<?= $form->field($modelClass, 'id_course')->hiddenInput(['value'=>$modelCourse->id])->label(false); ?>
								<?= $form->field($modelClass, 'name')->textInput(); ?>
								<?= $form->field($modelClass, 'url_name')->textInput(); ?>
								<div class="user_class_photo" action="<?= Url::home(); ?>course/classes/savedropedfile"  style="background-image:url('<?= Url::home(); ?>images/classes/<?= $modelClass->img_src; ?>');">
								</div>
								<?= $form->field($modelClass, 'img_src')->hiddenInput()->label(false); ?>
								<?= $form->field($modelClass, 'description')->textarea(['rows' => '6']); ?>
								<?= $form->field($modelClass, 'professor')->dropDownList($userArray); ?>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-close" data-dismiss="modal">Cancel</button>
								<?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'change_user_info', 'class' => 'btn btn-action']) ?>
							</div>
						<?php ActiveForm::end();  ?>
					</div>
				</div>
			</div>
		<?php } ?>
				<div class="clearfix"></div>
		<?php /*?>
                <div class="class-info-table">
			<h4><?= $modelClass->name; ?></h4>
			<table>
				<tr>
					<td>University:</td>
					<td><?= $modelUniversity->name; ?></td>
					<td>Course:</td>
					<td><?= $modelCourse->name; ?></td>
				</tr>
				<tr>
					<td>Major:</td>
					<td><?= $modelMajor->name; ?></td>
					<td>Members:</td>
					<td><?= ClassmembersWidget::widget(['class_id' =>  $modelMajor->id]) ?></td>
				</tr>
			</table>
		</div>
		<?php */ ?>
	<!--</div> -->
