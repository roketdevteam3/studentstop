<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\assets\ChatclassAsset;

ChatclassAsset::register($this);
?>
	<div class="row">
		<?php echo $this->render('right_menu',[
			'modelProfessor' => $modelProfessor,
			'modelClass' => $modelClass,
		]); ?>
		<div class="page-content">
			<div class="container-fluid background-block">
				<div class="row">
					<div class="col-sm-8">
							<h4>Chat</h4>
							<input type="hidden" id="user_id" value="<?=\Yii::$app->user->id;?>">
							<input type="hidden" id="class_id" value="<?=$modelClass->id;?>">
							<div class="col-md-3 col-md-offset-1">
								<ul class="list-material class_users"></ul>
							</div>
							<div class="col-md-7" style="border-left: 1px solid #f0f0f0;">
								<div class="message-class-container">
									<div class="select-file-box animated" style="top:40px;">
										<i class="fa fa-times close-box" aria-hidden="true"></i>
										<div action="<?=Url::home();?>course/default/savedropedfile/<?=$modelClass->id;?>" style="border:2px dashed #0087F7" class="user_photos">
											<div class="dz-message needsclick">
												<h3 style="color:#0087F7;">Drop files here or click to upload.</h3>
											</div>
										</div>
									</div>
									<div class="messages" style="height: 400px; overflow-y: auto;"></div>
									<!--.messages-->
									<div class="send-message" >
										<div class="input-group">
											<div class="inputer inputer-blue">
												<div class="input-wrapper">
													<textarea rows="2" id="text-message-class" class="form-control js-auto-size"
															  placeholder="Message" style="height: 67px; padding-right: 70px"></textarea>
													<div class="add-files btn-ripple"></div>
												</div>
											</div>
											<!--.inputer-->
											<span class="input-group-btn">
												<button id="send-message-class" class="btn btn-blue btn-ripple" type="button" style="height: 67px;">Send</button>
											</span>
										</div>
									</div>
									<!--.send-message-->
								</div>
								<!--.message-send-container-->
							</div>
							<div style="clear: both"></div>
						</div>
                                                <div class="col-sm-4">
                                                    <div style="background-color:white;">
                                                        <?= $this->render('class_info',[
    //                                                                'modelProfessor' => $modelProfessor,
                                                                    'modelMajor' => $modelMajor,
                                                                    'modelUniversity' => $modelUniversity,
                                                                    'modelCourse' => $modelCourse,
                                                                    'modelClass' => $modelClass,
                                                                    'followStatus' => $followStatus,
                                                                    'userArray' => $userArray,
                                                            ]); ?>
                                                        <ul class="nav nav-tabs">
                                                            <li class="active classStudentShow">
                                                                <a href="">Students</a>
                                                            </li>
                                                            <li class="classInfoShow">
                                                                <a href="">Class history</a>
                                                            </li>
                                                        </ul>
                                                        <div class="ClassInfoContainer">

                                                        </div>
                                                    </div>
                                                </div>
					</div>
			</div>
		</div>
	</div>
