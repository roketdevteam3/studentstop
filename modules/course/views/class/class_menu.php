<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    
    $class = ($this->context->getRoute() == 'course/default/class')?'active':''; 
    $class1 = ($this->context->getRoute() == 'course/default/classtutors')?'active':''; 
    $class2 = ($this->context->getRoute() == 'course/default/classproducts')?'active':''; 
    $class3 = ($this->context->getRoute() == 'course/default/classfiles')?'active':''; 
    $class4 = ($this->context->getRoute() == 'course/default/classchat')?'active':''; 
?>

<ul class="nav nav-tabs nav-justified" style="margin-top:30px;">
    <li role="presentation" class="<?= $class1; ?>">
        <?= HTML::a('Tutors',Url::home().'class/'.$modelClass->url_name.'/classtutors'); ?>
    </li>
    <li role="presentation" class="<?= $class2; ?>">
        <?= HTML::a('Products',Url::home().'class/'.$modelClass->url_name.'/classproducts'); ?>
    </li>
    <li role="presentation" class="<?= $class3; ?>">
        <?= HTML::a('Files',Url::home().'class/'.$modelClass->url_name.'/classfiles'); ?>
    </li>
    <li role="presentation" class="<?= $class4; ?>">
        <?= HTML::a('Chat',Url::home().'class/'.$modelClass->url_name.'/classchat'); ?>
    </li>
</ul>

