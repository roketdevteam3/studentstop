<?php

    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;
    use yii\bootstrap\Alert;
    use app\widgets\FilterclassesWidget;
    use app\widgets\RatingstarWidget;
    use app\widgets\rating\RatingclassWidget;
    use app\assets\ClassrightmenuAssets;
    ClassrightmenuAssets::register($this);

    $class = ($this->context->getRoute() == 'course/default/class')?'active':''; 
    $class1 = ($this->context->getRoute() == 'course/default/classtutors')?'active':''; 
    $class2 = ($this->context->getRoute() == 'course/default/classproducts')?'active':''; 
    $class3 = ($this->context->getRoute() == 'course/default/classfiles')?'active':''; 
    $class4 = ($this->context->getRoute() == 'course/default/classchat')?'active':''; 
?>
	<div id="profile-right-panel">
		<div class="class-user-av">
                    <?php if(($modelProfessor['avatar'] != '') && ($modelProfessor['avatar'] != null)){ ?>
			<img class="img-responsive" src="<?= Url::home(); ?>images/users_images/<?= $modelProfessor['avatar']; ?>">
                    <?php }else{ ?>
			<img class="img-responsive" src="<?= Url::home(); ?>images/default_avatar.jpg">                        
                    <?php } ?>
		</div>

		<div class="profile-rating">
			<span style="font-size: 13px;">Rank:</span>
			<?= RatingclassWidget::widget(['class_id' => $modelClass->id, 'type_show' => 'with_rat']); ?>
		</div>
		<div class="menu-wrapper">
			<div class="user-info">
				<span class="info-label">
					Name:
				</span>
				<span class="info-user">
					<?= $modelProfessor['name']; ?> <?= $modelProfessor['surname']; ?>
				</span>
				<span class="info-label">
					Birthday:
				</span>
				<span class="info-user">
					<?= $modelProfessor['birthday']; ?>
				</span>
				<span class="info-label">
					Phone:
				</span>
				<span class="info-user">
					<?= $modelProfessor['mobile']; ?>
				</span>
<!--				<span class="info-label">-->
<!--					Email:-->
<!--				</span>-->
				<span class="info-user">
					<?= $modelProfessor['email']; ?>
				</span>
			</div>
			<nav class="menu-right bottom-menu">
				<ul>
					<li role="presentation" class="<?= $class1; ?>">
						<?= HTML::a('Dashboard',Url::home().'class/'.$modelClass->url_name); ?>
					</li>
					<li role="presentation" class="<?= $class1; ?>">
						<?= HTML::a('Tutors',Url::home().'class/'.$modelClass->url_name.'/classtutors'); ?>
					</li>
					<div style="clear:both;"></div>
					<li role="presentation" class="<?= $class2; ?>">
						<?= HTML::a('Products',Url::home().'class/'.$modelClass->url_name.'/classproducts'); ?>
					</li>
                	                <?php /* ?>
					<li role="presentation" class="<?= $class2; ?>">
						<?= HTML::a('Rating',Url::home().'class/'.$modelClass->url_name.'/rating'); ?>
					</li>
                	                <?php */ ?>
					<div style="clear:both;"></div>
					<li role="presentation" class="<?= $class3; ?>">
						<?= HTML::a('Files',Url::home().'class/'.$modelClass->url_name.'/classfiles'); ?>
					</li>
                	                <?php /* ?>
					<li role="presentation" class="<?= $class4; ?>">
						<?= HTML::a('Chat',Url::home().'class/'.$modelClass->url_name.'/classchat'); ?>
					</li>
					<div style="clear:both;"></div>
                	                <?php */ ?>
				</ul>
			</nav>
		</div>
		<?php //= FilterclassesWidget::widget() ?> 
	</div>
