<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use yii\helpers\Url;
	use yii\widgets\LinkPager;
		use kartik\rating\StarRating;
		use app\assets\ShowclassendedAsset;
		ShowclassendedAsset::register($this);
		use app\assets\ChatclassAsset;
		use kartik\datetime\DateTimePicker;
		use dosamigos\tinymce\TinyMce;
		ChatclassAsset::register($this);
?>
	<div class="row">
		<?php if($classEnded != 2){ ?>
			<?php echo $this->render('right_menu',[
				'modelProfessor' => $modelProfessor,
				'modelClass' => $modelClass,
			]); ?>
			<div class="page-content">
				<div class="container-fluid background-block">
					<div class="class-page">
						<div class="row">
							<div class="col-sm-6">
								<ul class="nav nav-tabs chat-tabs" style="background-color: white">
									<li role="presentation" class="active chatTabsButton">
										<a href="#">Chat</a>
									</li>
									<li role="presentation" class="eventsTabsButton">
										<a href="#">Events</a>
									</li>
									<li role="presentation" class="QuestionTabsButton">
										<a href="#">Question bank</a>
									</li>
								</ul>
								<div class="tab-content ClassTabscontainerInfo" style="display:none;">
                                                                </div>
								<?php 
                                                                if($followStatus != 'Follow'){ ?>	
                                                                        <div class="tab-content ClassTabscontainerChat">
                                                                                <input type="hidden" id="user_id" value="<?=\Yii::$app->user->id;?>">
                                                                                <input type="hidden" id="class_id" value="<?=$modelClass->id;?>">
                                                                                <div class="message-class-container">
                                                                                        <div class="send-message" >
                                                                                                <div class="input-group">
                                                                                                        <div class="inputer inputer-blue">
                                                                                                                <div class="input-wrapper">
                                                                                                                        <textarea rows="2" id="text-message-class" class="form-control js-auto-size" placeholder="Message"></textarea>
                                                                                                                        <div class="add-files btn-ripple"></div>
                                                                                                                </div>
                                                                                                        </div>
                                                                                                        <!--.inputer-->
                                                                                                        <span class="input-group-btn">
                                                                                                                <button id="send-message-class" class="btn btn-ripple" type="button">Send</button>
                                                                                                        </span>
                                                                                                </div>
                                                                                        </div>
                                                                                        <div class="select-file-box animated"  style="top:40px;">
                                                                                                <i class="fa fa-times close-box" aria-hidden="true"></i>
                                                                                                <div action="<?=Url::home();?>course/default/savedropedfile/<?=$modelClass->id;?>" class="user_photos">
                                                                                                        <div class="dz-message needsclick">
                                                                                                                <h3 style="color:#0087F7;">Drop files here or click to upload.</h3>
                                                                                                        </div>
                                                                                                </div>
                                                                                        </div>
                                                                                        <div class="messages"></div>
                                                                                        <!--.messages-->
                                                                                        <!--.send-message-->
                                                                                </div>
                                                                                <!--.message-send-container-->
                                                                                <div style="clear: both"></div>
                                                                        </div>
                                                                <?php }else{ ?>
                                                                    <p style="text-align:center;">Chat available</p>
                                                                <?php } ?>
							</div>
							<div class="col-sm-6">
								<div class="">
									<?php if($modelClassFilesImage){ ?>
										<div id="myCarousel" class="carousel slide" data-ride="carousel">
											<!-- Indicators -->
											<ol class="carousel-indicators">
											<?php foreach($modelClassFilesImage as $key => $image){ ?>
												<?php if($key == '0'){ ?>
													<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
												<?php }else{ ?>
													<li data-target="#myCarousel" data-slide-to="<?= $key; ?>" ></li>
												<?php } ?>
											<?php } ?>
											</ol>
											<!-- Wrapper for slides -->
											<div class="carousel-inner" role="listbox">
												<?php foreach($modelClassFilesImage as $key => $image){ ?>
												<?php $classsActive = ''; ?>
												<?php if($key == '0'){ ?>
													<?php $classsActive = ' active '; ?>
												<?php } ?>
												<div class="item <?= $classsActive; ?>" style="height: 250px;">
													<img class="my_style_slider_img" src="../../files/<?= $modelClass->id;?>/<?= $image['file_src']; ?>">
												</div>
												<?php } ?>
											</div>
											<!-- Left and right controls -->
											<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
												<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
												<span class="sr-only">Previous</span>
											</a>
											<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
												<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
												<span class="sr-only">Next</span>
											</a>
										</div>
									<?php } ?>
								</div>
								<div class="rating-class-block" style="margin-top: 20px;">
									<ul class="nav nav-tabs">
										<li role="presentation" class="active topStudents">
											<a>Top 5 students</a>
										</li>
										<li role="presentation" class="topProfessor">
											<a>Top 5 professor</a>
										</li>
										<li role="presentation" class="topClass">
											<a>Top 5 class</a>
										</li>
									</ul>
									<div class="topClassContainer" style="padding-left: 0px;">
										
									</div>
								</div>
								<div class="bottom-class-block">
									<?= $this->render('class_info',[
												//'modelProfessor' => $modelProfessor,
												'modelMajor' => $modelMajor,
												'modelUniversity' => $modelUniversity,
												'modelCourse' => $modelCourse,
												'modelClass' => $modelClass,
												'followStatus' => $followStatus,
												'userArray' => $userArray,
										]); ?>
									<ul class="nav nav-tabs">
										<li class="active classStudentShow">
											<a href="">Students</a>
										</li>
										<li class="classInfoShow">
											<a href="">Class info</a>
										</li>
									</ul>
									<div class="ClassInfoContainer">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		
		<?php }else{ ?>
			<div class="showClassEnded">
				<input name="this_class_id" value="<?= $modelClass->id; ?>" type="hidden">

				<div id="modalRating" class="modal fade" role="dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">x</button>
							</div>
							<div class="modal-body">
								<h2 class="modal-rating-header">Rating Do's and Don'ts</h2>
								<div class="row">
									<div class="col-sm-4">
										<h3 class="subheader">Do</h3>
										<p>Double check your comments before posting. Course codes must be accurate, and it doesn’t hurt to check grammar</p>
									</div>
									<div class="col-sm-4">
										<h3 class="subheader">Do</h3>
										<p>Discuss the professor’s professional abilities including teaching style and ability to convey the material clearly.</p>
									</div>
									<div class="col-sm-4">
										<h3 class="subheader">Don`t</h3>
										<p>Use profanity, name-calling, derogatory terms, definitive language, (e.g., "always","never","etc."). And, don’t claim that the professor shows bias or favoritism for or against students.</p>
									</div>
								</div>
								<h4 class="modal-rating-header">It's your turn to grade Professor Kush Jenkins.</h4>
								<?php $form = ActiveForm::begin(['method' => 'post']); ?>
									<?= $form->field($modelNewRating, 'class_id')->hiddenInput(['value' => $modelClass->id])->label(false); ?>
									<?= $form->field($modelNewRating, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false); ?>
									<?= $form->field($modelNewRating, 'rate_professor')->widget(StarRating::classname(), [
										'pluginOptions' => [
											'step' => '1',
											'size'=>'xs',
											'showCaption' => false,
										],
									]); ?>
									
									<?=  $form->field($modelNewRating, 'prof_again')->inline(true)->radioList([
										'1' => 'Yeah',
										'2' => 'Um, no',
									]); ?>
									<?=  $form->field($modelNewRating, 'take_for_credit')->inline(true)->radioList([
										'1' => 'Yeah',
										'2' => 'Um, no',
									]); ?>
									<?=  $form->field($modelNewRating, 'textbook_use')->inline(true)->radioList([
										'1' => 'Yeah',
										'2' => 'Um, no',
									]); ?>
									<?=  $form->field($modelNewRating, 'attendance')->inline(true)->radioList([
										'1' => 'Mendatory',
										'2' => 'Non mandatory',
									]); ?>
									<?=  $form->field($modelNewRating, 'hotness')->inline(true)->radioList([
										'1' => 'Yeah',
										'2' => 'Um, no',
									]); ?>

									<?= $form->field($modelNewRating, 'more_specific')->textarea(); ?>

									<?= $form->field($modelNewRating, 'level_of_difficulty')->textarea(); ?>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
								<?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'change_user_info', 'class' => 'btn btn-action']) ?>
								<?php ActiveForm::end();  ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>


<div class="modal fade" id="eventCreateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
			</div>
			<div class="modal-body">
				<form class="model-form formCreateClassEvent">
					<div class="form-group field-event-title required has-success">
						<label class="control-label" for="event-title">Title</label>
						<input type="text" id="event-title" class="form-control" name="event_title" maxlength="255" required>
					</div>
					<div class="form-group field-event-place_type required">
						<input type="hidden" class="form-control" name="event_place_type" value="university">
					</div>
					<div class="form-group field-event-place_id required">
						<input type="hidden" class="form-control" name="event_place_id" value="<?= $modelClass->id_university; ?>">
					</div>
					<div class="form-group field-event-class_id required">
						<input type="hidden" class="form-control" name="event_class_id" value="<?= $modelClass->id; ?>">
					</div>
					<div class="form-group field-event-image">
					<label class="control-label" for="event-image">Image</label>
						<input type="hidden" name="event_image" value=""><input type="file" id="event-image" name="Event[image]" data-preview="true">
					</div>
					<div class="form-group field-event-short_desc">
						<label class="control-label" for="eventshort_desc">Short Desc</label>
						<textarea id="event-short_desc" class="form-control" name="event_short_desc" rows="6"></textarea>
					</div>
					<div class="form-group">
						<label class="control-label">Start Time</label>
						<?php 
						echo DateTimePicker::widget([
							'name' => 'event_date_start', 
							'value' => date('Y-m-d', strtotime('+1 days')),
							'options' => ['placeholder' => 'Enter event time ...'],
							'pluginOptions' => [
								'format' => 'yyyy-mm-dd',
								'todayHighlight' => true
							]
						])
						?>
					</div>
					<div class="form-group">
						<label class="control-label">End Time</label>
						<?php 
						echo DateTimePicker::widget([
							'name' => 'event_date_end', 
							'value' => date('Y-m-d', strtotime('+2 days')),
							'options' => ['placeholder' => 'Enter event time ...'],
							'pluginOptions' => [
								'format' => 'yyyy-mm-dd',
								'todayHighlight' => true
							]
						])
						?>
					</div>
					<div class="form-group field-event-content">
						<label class="control-label" for="event-short_desc">Content</label>
						<textarea id="event-content" class="form-control" name="event_content" rows="6"></textarea>
					</div>

					<div class="form-group text-center">
						<?= Html::submitButton(Yii::t('app', 'Create'),['class' => 'btn btn-action createClassEvent']) ?>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="questionCreateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
			</div>
			<div class="modal-body">
				<form class="formCreateClassQuestion">
					<div class="form-group field-question-question_title required has-error">
						<label class="control-label" for="question-question_title">Question Title</label>
						<input type="text" id="question-question_title" class="form-control" name="question_question_title">
					</div>
					<div class="form-group field-question-question_text required has-error">
						<label class="control-label" for="question-question_text">Question Text</label>
						<textarea id="question-question_text" class="form-control" name="question_question_text"></textarea>
					</div>
					<div class="form-group field-question-price required has-success">
						<label class="control-label" for="question-price">Price</label>
						<input type="number" id="question-price" class="form-control" name="question_price">
					</div>
					<div class="form-group field-question-category">
						<label class="control-label" for="question-category">Category</label>
						<select id="question-category" class="form-control" name="question_category_parent">
							<?php foreach($category_array as $category_id => $category){ ?>
								<option value="<?= $category_id?>"><?= $category; ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="form-group field-question-category">
						<label class="control-label" for="question-category">sub category</label>
						<select id="question-category_id" class="form-control" name="question_category">
						</select>
					</div>
					<div class="form-group field-question-privacy">
						<label class="control-label" for="question-privacy">Privacy</label>
						<select id="question-privacy" class="form-control" name="question_privacy">
							<option value="1">All User</option>
							<option value="2">My friend</option>
							<option value="3">Class mates</option>
						</select>
					</div>
					<div class="form-group text-center">
						<?= Html::submitButton(Yii::t('app', 'Create'),['class' => 'btn btn-action createClassEvent']) ?>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalShowPost" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
			</div>
			<div class="modal-body modalContent">
				<div class="eventShowModalContainer">
			</div>
		</div>
	</div>
</div>