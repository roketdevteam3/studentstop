<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use yii\helpers\Url;
	use yii\widgets\LinkPager;
		use kartik\rating\StarRating;
		use app\assets\ShowclassendedAsset;
		ShowclassendedAsset::register($this);
		
		use app\assets\ClassrightblockAsset;
		ClassrightblockAsset::register($this);
?>
	<div class="row">
		<?php if($classEnded != 2){ ?>
			<?php echo $this->render('right_menu',[
				'modelProfessor' => $modelProfessor,
				'modelClass' => $modelClass,
			]); ?>

            <?php if($newTutors){ ?>
<!--                <button type="button" style="    height: 40px; font-size: 14px;    border-radius: 0px;    float: right;" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Become tutor</button>-->

                    <div id="myModal" class="modal fade" role="dialog">
                      <div class="modal-dialog">
                        <!-- Modal content-->
                          <?php $form = ActiveForm::begin(); ?>
                            <div class="modal-content">
                                <div class="modal-body">
                                        <h2>Become tutors</h2>
                                        <?= $form->field($newTutors, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false); ?>
                                        <?= $form->field($newTutors, 'price')->input('number',['step' => "0.01", 'class'=>'form-control']); ?>
                                        <?= $form->field($newTutors, 'about')->textarea(); ?>
                                        <div style="max-height:300px;overflow:auto">
                                            <table class="table" >
                                                <?php foreach($modelCourses as $course){ ?>
                                                <tr>
                                                    <td><input type="checkbox" name="Tutorsinfo[course][]" value="<?= $course['id']; ?>"></td>

                                                    <td><?= $course['name']; ?></td>
                                                </tr>
                                                <?php } ?>
                                            </table>
                                        </div>
                                </div>
                                <div class="modal-footer">
                                        <button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
                                        <?= Html::submitButton( Yii::t('app', 'Become'), ['name' => 'become_tutor', 'class' => 'btn btn-action']) ?>
                                </div>
                            </div>
                        <?php ActiveForm::end();  ?>

                      </div>
                    </div>
            <?php } ?>
		<div class="page-content">
			<div class="container-fluid background-block">
				<div class="class-page">
					<div class="row">
						<div class="col-sm-6">
                            <div class="class-user-info" style="padding: 0px">
                                <h4 class="tutors-title" style="    padding: 12px;  margin: 0px;   display: inline-block;">Tutors</h4>
                                <button type="button" style="    height: 40px; font-size: 14px;    border-radius: 0px;    float: right;" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Become tutor</button>

                            </div>
							<?php foreach($modelTutors as $tutors){ ?>
								<div class="class-user-info" style="padding-bottom: 0px; padding-top: 0px; padding-left: 0px">
									<div class="user-img-wrap">
                                                                                <?php if($tutors['avatar'] != ''){ ?>
                                                                                    <img class="img-responsive" src="<?= Url::home().'images/users_images/'.$tutors['avatar']; ?>">
                                                                                <?php }else{ ?>
                                                                                    <img class="img-responsive" src="<?= Url::home().'images/default_avatar.jpg'; ?>">                                                                                    
                                                                                <?php } ?>
									</div>
									<div class="user-information" style="padding-top: 10px; padding-bottom: 5px; display: ">
										<h4 style="display: inline-block; float: left"><?= $tutors['name'].''.$tutors['surname']; ?></h4>
                                        <div class="" style="float: right; padding-left: 10px">
                                            <?= $tutors['price']; ?>
                                        </div>
                                        <div class="" style="float: right;">
											<b>Price:</b>
										</div>
                                                                                <div class='col-sm-10'>
                                                                                    <?= $tutors['about']; ?>
										</div>
                                        <div class="col-sm-2" style="padding: 0px">
                                            <?php if(\Yii::$app->user->id != $tutors['user_id']){ ?>
                                                <button class="btn startChat my_button_chat" style="background-image: url('../../../../web/images/chat_cv.png'); background-size: 20px; background-repeat: no-repeat; float: right" data-sender-id="<?=\Yii::$app->user->id?>" data-recipient-id="<?= $tutors['user_id']; ?>"></button>
                                            <?php } ?>
                                        </div>
										<div class="clearfix"></div>
									</div>
									<div class="clearfix"></div>
								</div>
							<?php } ?>
							<?= LinkPager::widget(['pagination'=>$pagination]);  ?>
						</div>
						<div class="col-sm-6">
							<input type="hidden" id="user_id" value="<?=\Yii::$app->user->id;?>">
							<input type="hidden" id="class_id" value="<?=$modelClass->id;?>">
							<div class="">
								<?php if($modelClassFilesImage){ ?>
									<div id="myCarousel" class="carousel slide" data-ride="carousel">
										<!-- Indicators -->
										<ol class="carousel-indicators">
										<?php foreach($modelClassFilesImage as $key => $image){ ?>
											<?php if($key == '0'){ ?>
												<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
											<?php }else{ ?>
												<li data-target="#myCarousel" data-slide-to="<?= $key; ?>" ></li>
											<?php } ?>
										<?php } ?>
										</ol>
										<!-- Wrapper for slides -->
										<div class="carousel-inner" role="listbox">
											<?php foreach($modelClassFilesImage as $key => $image){ ?>
											<?php $classsActive = ''; ?>
											<?php if($key == '0'){ ?>
												<?php $classsActive = ' active '; ?>
											<?php } ?>
											<div class="item <?= $classsActive; ?>">
												<img class="my_style_slider_img" src="../../files/<?= $modelClass->id;?>/<?= $image['file_src']; ?>">
											</div>
											<?php } ?>
										</div>
										<!-- Left and right controls -->
										<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
											<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
											<span class="sr-only">Previous</span>
										</a>
										<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
											<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
											<span class="sr-only">Next</span>
										</a>
									</div>
								<?php } ?>
							</div>
							<div class="rating-class-block">
								<ul class="nav nav-tabs">
									<li role="presentation" class="active topStudents">
										<a>Top 5 students</a>
									</li>
									<li role="presentation" class="topProfessor">
										<a>Top 5 professor</a>
									</li>
									<li role="presentation" class="topClass">
										<a>Top 5 class</a>
									</li>
								</ul>
								<div class="topClassContainer">
									
								</div>
							</div>
							<div class="bottom-class-block">
								<?= $this->render('class_info',[
											//'modelProfessor' => $modelProfessor,
											'modelMajor' => $modelMajor,
											'modelUniversity' => $modelUniversity,
											'modelCourse' => $modelCourse,
											'modelClass' => $modelClass,
											'followStatus' => $followStatus,
											'userArray' => $userArray,
									]); ?>
								<ul class="nav nav-tabs">
									<li class="active classStudentShow">
										<a href="">Students</a>
									</li>
									<li class="classInfoShow">
										<a href="">Class info</a>
									</li>
								</ul>
								<div class="ClassInfoContainer">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			<?php }else{ ?>
				<div class="showClassEnded">
					<input name="this_class_id" value="<?= $modelClass->id; ?>" type="hidden">
					<div id="modalRating" class="modal fade" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">x</button>
								</div>
								<div class="modal-body">
									<h2 class="modal-rating-header">Rating Do's and Don'ts</h2>
									<div class="row">
										<div class="col-sm-4">
											<h3 class="subheader">Do</h3>
											<p>Double check your comments before posting. Course codes must be accurate, and it doesn’t hurt to check grammar</p>
										</div>
										<div class="col-sm-4">
											<h3 class="subheader">Do</h3>
											<p>Discuss the professor’s professional abilities including teaching style and ability to convey the material clearly.</p>
										</div>
										<div class="col-sm-4">
											<h3 class="subheader">Don`t</h3>
											<p>Use profanity, name-calling, derogatory terms, definitive language, (e.g., "always","never","etc."). And, don’t claim that the professor shows bias or favoritism for or against students.</p>
										</div>
									</div>
									<h4 class="modal-rating-header">It's your turn to grade Professor Kush Jenkins.</h4>
									<?php $form = ActiveForm::begin(['method' => 'post']); ?>
										<?= $form->field($modelNewRating, 'class_id')->hiddenInput(['value' => $modelClass->id])->label(false); ?>
										<?= $form->field($modelNewRating, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false); ?>
										<?= $form->field($modelNewRating, 'rate_professor')->widget(StarRating::classname(), [
											'pluginOptions' => [
												'step' => '1',
												'size'=>'xs',
												'showCaption' => false,
											],
										]); ?>
										
										<?=  $form->field($modelNewRating, 'prof_again')->inline(true)->radioList([
											'1' => 'Yeah',
											'2' => 'Um, no',
										]); ?>
										<?=  $form->field($modelNewRating, 'take_for_credit')->inline(true)->radioList([
											'1' => 'Yeah',
											'2' => 'Um, no',
										]); ?>
										<?=  $form->field($modelNewRating, 'textbook_use')->inline(true)->radioList([
											'1' => 'Yeah',
											'2' => 'Um, no',
										]); ?>
										<?=  $form->field($modelNewRating, 'attendance')->inline(true)->radioList([
											'1' => 'Mendatory',
											'2' => 'Non mandatory',
										]); ?>
										<?=  $form->field($modelNewRating, 'hotness')->inline(true)->radioList([
											'1' => 'Yeah',
											'2' => 'Um, no',
										]); ?>

										<?= $form->field($modelNewRating, 'more_specific')->textarea(); ?>

										<?= $form->field($modelNewRating, 'level_of_difficulty')->textarea(); ?>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-close" data-dismiss="modal">Close</button>
									<?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'change_user_info', 'class' => 'btn btn-action']) ?>
									<?php ActiveForm::end();  ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
	</div>
