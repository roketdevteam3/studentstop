<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use yii\helpers\Url;
	use yii\widgets\LinkPager;
        use kartik\rating\StarRating;
?>
	<div class="row">
                <?php echo $this->render('right_menu',[
                        'modelProfessor' => $modelProfessor,
                        'modelClass' => $modelClass,
                ]); ?>
                <div class="page-content">
                        <div class="container-fluid background-block">
                                <div class="row">
                                    <div class="col-sm-8">
                                                        <?php foreach($modelClassRating as $rating){ ?>
                                                            <div class="col-sm-2">
                                                                <img src="<?= Url::home(); ?>images/users_images/<?= $rating['avatar']; ?>" style="width:100%;">
                                                            </div>
                                                            <div class="col-sm-10">
                                                                <?php
                                                                    echo StarRating::widget([
                                                                        'name' => 'rating_1',
                                                                        'value' => '3',
                                                                        'pluginOptions' => [
                                                                            'disabled'=>true,
                                                                            'showClear'=>false,
                                                                            'size'=>'xs',
                                                                            'value' => $rating['rate_professor'],
                                                                            'showCaption' => false,
                                                                            ]
                                                                    ]);
                                                                ?>
                                                                
                                                                <?= $rating['level_of_difficulty'].'<br>'; ?>
                                                                <?= $rating['prof_again'].'<br>'; ?>
                                                                <?= $rating['take_for_credit'].'<br>'; ?>
                                                                <?= $rating['textbook_use'].'<br>'; ?>
                                                                <?= $rating['attendance'].'<br>'; ?>
                                                                <?= $rating['hotness'].'<br>'; ?>
                                                                <?= $rating['more_specific'].'<br>'; ?>
                                                            </div>
                                                        <?php } ?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div style="background-color:white;">
                                                        <?= $this->render('class_info',[
    //                                                                'modelProfessor' => $modelProfessor,
                                                                    'modelMajor' => $modelMajor,
                                                                    'modelUniversity' => $modelUniversity,
                                                                    'modelCourse' => $modelCourse,
                                                                    'modelClass' => $modelClass,
                                                                    'followStatus' => $followStatus,
                                                                    'userArray' => $userArray,
                                                            ]); ?>
                                                        <ul class="nav nav-tabs">
                                                            <li class="active classStudentShow">
                                                                <a href="">Students</a>
                                                            </li>
                                                            <li class="classInfoShow">
                                                                <a href="">Class history</a>
                                                            </li>
                                                        </ul>
                                                        <div class="ClassInfoContainer">

                                                        </div>
                                                    </div>
                                                </div>
                                </div>
                        </div>
                </div>
        </div>