<?php
    use yii\bootstrap\ActiveForm;
    use yii\bootstrap\Html;
    use yii\helpers\Url;
    use yii\widgets\LinkPager;
    
?>
<div class="course-default-index">
    <?= HTML::a('Course & class',URL::home().'courseclases'); ?>
    <span class="fa fa-angle-right"></span>
    <span>Majors</span>
    <hr>
    <h1><b>Majors</b></h1>
    <div class="col-sm-12">
        <?php foreach($modelMajors as $majors){ ?>
            <div class="col-sm-4">
                <div class="well">
                    <h3>
                        <?= HTML::a($majors->name,'/courses/'.$majors->url_name); ?>
                    </h3>
                </div>
            </div>
        <?php } ?>
        <div class="col-sm-12">
            <?= LinkPager::widget(['pagination'=>$pagination]); ?>
        </div>
                                    
    </div>
</div>
