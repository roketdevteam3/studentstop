<?php
    use yii\bootstrap\ActiveForm;
    use yii\bootstrap\Html;
    use yii\helpers\Url;
    
?>
<div class="course-default-index">
    <h1><b>Course & class</b></h1>
    <div class="col-sm-1"></div>
    <div class="col-sm-5">
        <h2>
            <?= HTML::a('Block 1','/courses/'); ?>
        </h2>
        <img src="" style="width:100%;height:100px;">
    </div>
    <div class="col-sm-5">
        <h2>
            <?= HTML::a('Block 2','/majors'); ?>
        </h2>
        <img src="" style="width:100%;height:100px;">
    </div>
</div>
