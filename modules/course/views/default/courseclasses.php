<?php
    use yii\bootstrap\ActiveForm;
    use yii\bootstrap\Html;
    use yii\helpers\Url;
    
?>
<div class="course-default-index">
    <?php if($status != 'error'){ ?>
        <h1><b>Course & class</b></h1>
        <div class="col-sm-1"></div>
        <div class="col-sm-5">
                <div class="panel" style="border: 1px solid rgba(217, 217, 217, 0.6);position:relative;">
                    <div class="panel-body" style="padding:0px;">
                        <img src="<?= Url::home(); ?>images/block_1.jpg" style="width:100%">
                    </div> 
                    <h2 style='text-align:center;padding:5px 0px 5px 0px;position:absolute;top:0px;background-color:rgba(255,255,255,0.6);display:block;width:100%;'>
                        <b><?= HTML::a('Classes','/classes'); ?></b>
                    </h2>
                </div>
        </div>
         <div class="col-sm-5">
                <div class="panel" style="border: 1px solid rgba(217, 217, 217, 0.6);position:relative;">
                    <div class="panel-body" style="padding:0px;">
                        <img src="<?= Url::home(); ?>images/block_2.jpg" style="width:100%">
                    </div> 
                    <h2 style='text-align:center;padding:5px 0px 5px 0px;position:absolute;top:0px;background-color:rgba(255,255,255,0.6);display:block;width:100%;'>
                        <b><?= HTML::a('Courses','/majors'); ?></b>
                    </h2>
                </div>
        </div>
    <?php }else{ ?>
        <h1><b>Course & class</b></h1>
        <p>If you want to see the information, first select University</p>
    <?php } ?>
</div>
