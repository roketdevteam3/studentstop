<?php
    use yii\bootstrap\ActiveForm;
    use yii\bootstrap\Html;
    use yii\helpers\Url;
    use yii\widgets\LinkPager;
    use yii\bootstrap\Alert;
?>
<div class="course-default-index">
    <?php
    if(Yii::$app->session->hasFlash('course_added')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => 'Course added',
        ]);
    endif; 
    if(Yii::$app->session->hasFlash('course_delete')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => 'Course deleted',
        ]);
    endif; 
    if(Yii::$app->session->hasFlash('Course_notadded')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-error',
            ],
            'body' => 'Course not added',
        ]);
    endif; 
    ?>
    
    <?= HTML::a('Course & class',URL::home().'courseclases'); ?>
    <span class="fa fa-angle-right"></span>
    <?= HTML::a('Majors',URL::home().'majors'); ?>
    <span class="fa fa-angle-right"></span>
    <span><?= $modelMajor->name; ?></span>
    <hr>
    
    <button class="btn btn-primary" data-toggle="modal" data-target="#modalNewCourse">Add new Course</button>
    <div class="modal fade" id="modalNewCourse" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h1 style="text-align: center">New Course</h1>
                </div>
                <div class="modal-body">
                    <?php $form = ActiveForm::begin(); ?>
                        <?= $form->field($modelNewCourse, 'id_major')->hiddenInput(['value'=>$modelMajor->id])->label(false); ?>
                        <?= $form->field($modelNewCourse, 'name')->textInput(); ?>
                        <?= $form->field($modelNewCourse, 'url_name')->textInput(); ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <?= Html::submitButton( Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
                    <?php ActiveForm::end();  ?>
                </div>
            </div>
        </div>
    </div>
    
    
    <h1>Major: <b><?= $modelMajor->name; ?></b></h1>
    <h3>Courses:</h3>
    <div class="col-sm-12">
        <?php  foreach($modelCourse as $course){ ?>
            <div class="col-sm-4">
                <div class="well">
                    <h3>
                        <?= HTML::a($course->name,'/course/'.$course->url_name); ?>
                    </h3>
                </div>
            </div>
        <?php } ?>
        <div class="col-sm-12">
            <?= LinkPager::widget(['pagination'=>$pagination]); ?>
        </div>
    </div>
</div>
