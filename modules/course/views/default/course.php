<?php
    use yii\bootstrap\ActiveForm;
    use yii\bootstrap\Html;
    use yii\bootstrap\Alert;
    use yii\helpers\Url;
    use yii\widgets\LinkPager;
    
?>
<div class="course-default-index">
    <?php
    if(Yii::$app->session->hasFlash('course_update')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => 'Course updated',
        ]);
    endif; 
    if(Yii::$app->session->hasFlash('course_notupdate')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-error',
            ],
            'body' => 'Course not update',
        ]);
    endif; 
    ?>
    <?php
    if(Yii::$app->session->hasFlash('class_added')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => 'Class added',
        ]);
    endif; 
    if(Yii::$app->session->hasFlash('class_delete')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => 'Class deleted',
        ]);
    endif; 
    if(Yii::$app->session->hasFlash('class_notadded')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-error',
            ],
            'body' => 'Class not added',
        ]);
    endif; 
    ?>
    <?= HTML::a('Course & class',URL::home().'courseclases'); ?>
    <span class="fa fa-angle-right"></span>
    <?= HTML::a('Majors',URL::home().'majors'); ?>
    <span class="fa fa-angle-right"></span>
    <?= HTML::a($modelMajor->name,URL::home().'courses/'.$modelMajor->url_name); ?>
    <span class="fa fa-angle-right"></span>
    <?= $modelCourse->name; ?>
    <hr>
    <?php if($modelCourse->creator == \Yii::$app->user->id){ ?>
        <button class="btn btn-primary" data-toggle="modal" data-target="#updateCourse">Update Course</button>
        <div class="modal fade" id="updateCourse" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h1 style="text-align: center">Update Course</h1>
                    </div>
                    <div class="modal-body">
                            <?php $form = ActiveForm::begin(); ?>
                            <?= $form->field($modelCourse, 'id_major')->hiddenInput(['value'=>$modelMajor->id])->label(false); ?>
                            <?= $form->field($modelCourse, 'name')->textInput(); ?>
                            <?= $form->field($modelCourse, 'url_name')->textInput(); ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'change_user_info', 'class' => 'btn btn-primary']) ?>
                        <?php ActiveForm::end();  ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <h1>Course: <b><?= $modelCourse->name; ?></b></h1>
    <h3>Classes:</h3>
    <button class="btn btn-primary" data-toggle="modal" data-target="#modalNewClass">Add new Class</button>
    <div class="modal fade" id="modalNewClass" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h1 style="text-align: center">New Class</h1>
                </div>
                <div class="modal-body">
                    <?php $form = ActiveForm::begin(); ?>
                        <?= $form->field($modelNewClass, 'id_course')->hiddenInput(['value'=>$modelCourse->id])->label(false); ?>
                        <?= $form->field($modelNewClass, 'name')->textInput(); ?>
                        <?= $form->field($modelNewClass, 'url_name')->textInput(); ?>
                        <?= $form->field($modelNewClass, 'description')->textarea(['rows' => '6']); ?>
                        <?= $form->field($modelNewClass, 'professor')->dropDownList($userArray); ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'change_user_info', 'class' => 'btn btn-primary']) ?>
                    <?php ActiveForm::end();  ?>
                </div>
            </div>
        </div>
    </div>
    
    
    
    <div class="col-sm-12">
        <?php  foreach($modelClass as $class){ ?>
            <div class="col-sm-4">
                <div class="well">
                    <h3>
                        <?= HTML::a($class->name,'/class/'.$class->url_name); ?>
                    </h3>
                </div>
            </div>
        <?php } ?>
        <div class="col-sm-12">
            <?= LinkPager::widget(['pagination'=>$pagination]);  ?>
        </div>
    </div> 
</div>
