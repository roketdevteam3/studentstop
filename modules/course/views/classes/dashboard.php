<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use yii\helpers\Url;
	use app\widgets\ClassmembersWidget;
	use app\widgets\JoinclassWidget;
	use app\widgets\RatingstarWidget;
	use app\widgets\rating\RatingclassWidget;
	use app\widgets\rating\RankclassWidget;
	
?>

	<div class="row">
		<?php echo $this->render('right_menu'); ?>
		<div class="page-content">
			<div class="container-fluid background-block">
				<div class="row">
					<?= $this->render('header_profile'); ?>
					<div class="main-content">
						<div class="users-form">
							<div class="row">
								
								<table class="table classBlock">
									<thead>
										<tr>
											<td>
												#
											</td>
											<td>
												Class
											</td>
											<td>
												Professor
											</td>
											<td>
												Status
											</td>
											<td>
												Students
											</td>
											<td>
												Rank
											</td>
											<td>
												Rating
											</td>
											<td>
											</td>
										</tr>
									</thead>
									<?php foreach($modelClass as $key => $class){ ?>
										<tr>
											<td>
												<?= $key + 1; ?>
											</td>
											<td>
												<?= $class['class_name']; ?>; 
											</td>
											<td>
												<div class="user-img">
													<?php if($class['avatar'] != ''){ ?>
														<img src="<?= Url::home(); ?>images/users_images/<?= $class['avatar']; ?>" style='height:40px;width:40px;'>
													<?php }else{ ?>
														<img src="<?= Url::home(); ?>images/default_avatar.jpg"  style='height:40px;width:40px;'>
													<?php } ?>
												</div>
												<span class="name">
													<?= $class['name'].' '.$class['surname']; ?>
												</span>
											</td>
											<td>
												<?php if($class['status'] == '1'){ ?>
                                                                                                    <?php if($class['follow_status'] == '0'){ ?>
														<span class="active">Active</span>
													<?php }else{ ?>
														<span class="no-active">Inactive</span>
                                                                                                     <?php } ?>
												 <?php }else{ ?>
                                                                                                        <?php if((int)ClassmembersWidget::widget(['class_id' =>  $class['id']]) < 25){ ?>
                                                                                                            <span class="no-active">Pending</span>
                                                                                                         <?php }else{ ?>
                                                                                                            <span class="no-active">Inactive</span>
                                                                                                         <?php } ?>
                                                                                                                
												 <?php } ?>
											</td>
											<td>
												<?= ClassmembersWidget::widget(['class_id' =>  $class['id']]) ?>
											</td>
											<td>
												<?= RankclassWidget::widget(['class_id' =>  $class['id'], 'course_id' => $class['id_course']]) ?>
											</td>
											<td>
												<?= RatingclassWidget::widget(['class_id' => $class['id'], 'type_show' => 'without_rat']); ?>
											</td>
											<td>
												<a href="<?= Url::home().'class/'.$class['class_url_name']; ?>" class="btn btn-watch">View</a>
											</td>
											<?php //= JoinclassWidget::widget(['class_id' =>  $class['id']]) ?>
										</tr>
									<?php } ?>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>