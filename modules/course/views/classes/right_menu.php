<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\bootstrap\Alert;
use app\widgets\FilterclassesWidget;
use app\assets\ClassrightmenuAssets;
ClassrightmenuAssets::register($this);

$class1 = '';
$class2 = '';
$class3 = '';
$class4 = '';

?>

	<div class="courseclases-menu">
		<nav class="menu-right top">
			<li role="presentation" class="<?= $class1; ?>">
                            <?php echo HTML::a('Dashboard', Url::home().'courseclases'); ?>
			</li>
			<li role="presentation" class="<?= $class2; ?>">
                            <?php echo HTML::a('Find class', Url::home().'searchclasses'); ?>
			</li>
			<!--<li role="presentation" class="<?php //= $class3; ?>">
				<?php //echo HTML::a('Market', Url::home().'marketclass'); ?>
			</li>
			<li role="presentation" class="<?php //= $class4; ?>">
				<?php // echo HTML::a('Watching', Url::home().'watching'); ?>
			</li> -->
		</nav>
		<?= FilterclassesWidget::widget() ?> 
	</div>
