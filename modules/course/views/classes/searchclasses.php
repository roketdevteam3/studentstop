<?php
	use yii\bootstrap\ActiveForm;
	use yii\bootstrap\Html;
	use yii\bootstrap\Alert;
	use yii\helpers\Url;
	use app\widgets\JoinclassWidget;
	use app\widgets\ClassmembersWidget;
	use app\widgets\RatingstarWidget;
	use app\widgets\rating\RatingclassWidget;
	
        $university_id = 0;
        if(!\Yii::$app->user->isGuest){
            $university_id = \Yii::$app->user->identity->getUsertUniversity();
        }
?>

<div class="modal fade" id="modalNewClass" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4 class="modal-title">New Class</h4>
			</div>
			<div class="modal-body">
				<?php $form = ActiveForm::begin(); ?>
				<?= $form->field($modelNewClass, 'name')->textInput(['readonly' => 'readonly', 'class' => 'class_new_name']); ?>
				<?php //= $form->field($modelNewClass, 'url_name')->textInput(); ?>
				<div style='display:none;'>
					<?= $form->field($modelNewClass, 'id_university')->hiddenInput(['value' => $university_id]); ?>
				</div>
				<?= $form->field($modelNewClass, 'id_major')->dropDownList($majorArray,['prompt' => 'Major']); ?>
				<?= $form->field($modelNewClass, 'id_course')->dropDownList([],['class' => 'change_form_course form-control','prompt' => 'Course']); ?>
				<div class="user_class_photo" action="<?= Url::home(); ?>course/classes/savedropedfile" style="background-image:url('<?= Url::home(); ?>images/default_wrapper.png');"></div>
				<?= $form->field($modelNewClass, 'img_src')->hiddenInput()->label(false); ?>
				<?= $form->field($modelNewClass, 'description')->textarea(['rows' => '6']); ?>
				<?= $form->field($modelNewClass, 'professor')->dropDownList($professorArray,['class' => 'class_new_professor']); ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-close" data-dismiss="modal">Cancel</button>
				<?= Html::submitButton( Yii::t('app', 'Save'), ['name'=> 'change_user_info', 'class' => 'btn btn-action']) ?>
				<?php ActiveForm::end();  ?>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<?php echo $this->render('right_menu'); ?>
	<div class="page-content">
		<div class="container-fluid background-block">
			<div class="row">
				<?= $this->render('header_profile'); ?>
                            <?php
                                if(Yii::$app->session->hasFlash('class_there')):
                                        echo Alert::widget([
                                                'options' => [
                                                        'class' => 'alert-warning',
                                                ],
                                                'body' => \Yii::t('app','The class has already been created!'),
                                        ]);
                                endif; ?>    
                            
                            
				<div class="horisontal-navigation">
					<div class="container-fluid">
						<?php // var_dump($modelCourse); ?>
						<div class="row">
							<div class="col-sm-1">
								<div class="customNavigation">
									<a class="prev">
										<img src="images/icon/left-arrow.png" alt="left">
									</a>
								</div>
							</div>
							<script>
								$(document).on('click', '.fon',function(){
									$('.fon').each( function () {
										$('.text_magorlist').css({'color':'#0051ba','background-color':'#ddd'});
									});
									$("#dre-"+$(this).data('major_id')).css( {'color':'white','background-color':'#7364cc'});
								});
							</script>
							<div class="col-sm-10">
								<div id="owl-demo" class="owl-carousel">
									<div class="item universityMajor fon  active" id="dtr-all" data-major_id="all">
										<div style="height: 100px; background-image: url('/web/images/photo/3.jpg'); background-position: center; background-repeat: no-repeat; background-size: cover;" ></div>
										<div class="text_magorlist" id="dre-all" data-major_id="all" >All</div>
										</div>
									<?php foreach($modelMajor as $major){
										//var_dump($major);
										?>

										<div class="item universityMajor fon"  id="dtr-<?= $major['id']; ?>" data-major_id="<?= $major['id']; ?>">
											<div style="height: 100px;background-image: url('<?= $major['img_src']; ?>');    background-position: center; background-repeat: no-repeat; background-size: cover;">
											</div>
											<div class="text_magorlist"  id="dre-<?= $major['id']; ?>" data-major_id="<?= $major['id']; ?> "> <?= $major['name']; ?></div>
										</div>
									<?php } ?>
								</div>
							</div>
							
							<div class="col-sm-1">
								<div class="customNavigation">
									<a class="next">
										<img src="images/icon/right-arrow.png" alt="right">
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="main-content">
					<div class="users-form">
						<div class="classSearchContainer" >
							<?php 
		//                                            $class_not_fount = 'display:none;';
													/*
								if($modelClass){
									foreach($modelClass as $class){ ?>
											<div class="col-sm-12 classBlock" style="background-color: white;margin-top:20px;">
												Class: <?= $class['class_name']; ?>; 
												Proffessor: <?= $class['name'].' '.$class['surname'];?>;
												Students: <?= ClassmembersWidget::widget(['class_id' =>  $class['id']]) ?>
												<?= RatingclassWidget::widget(['class_id' => $class['id'], 'type_show' => 'without_rat']); ?>
												<a href="<?= Url::home().'class/'.$class['class_url_name']; ?>" class="btn btn-watch">
														Watching
												</a>
												<?= JoinclassWidget::widget(['class_id' =>  $class['id']]) ?>
											</div>
									<?php } 
								}else{
									$class_not_fount = '';
							} */ ?>
						</div>
						<div class="classNotFound" style="display:none;">
							<h3 class="not-found-class">Class not found</h3>
							<button class="btn btn-create-class" data-toggle="modal" data-target="#modalNewClass" style="margin">Create new Class</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
