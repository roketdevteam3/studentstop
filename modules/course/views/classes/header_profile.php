<?php
/**
 * Created by PhpStorm.
 * User: minace
 * Date: 2/29/16
 * Time: 10:50 PM
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use app\widgets\FollowbuttonWidget;

?>
	<div class="search-class-wrapper">
		<?php
			$input_value = '';
			if(isset($_GET['search_key'])){ 
				$input_value = $_GET['search_key'];
		} ?>
		<form class="find-class-form" method="get" action="<?= Url::home(); ?>searchclasses">
			<div class="input-group">
				<input type="text" class="form-control getSearchInput" value="<?= $input_value; ?>" data-tabs_type="members" name="search_key" placeholder="Enter search text...">
				<?php if($this->context->getRoute() != 'course/classes/searchclasses'){ ?>
					<span class="input-group-btn">
						<input type="submit" class="btn build-course" type="button" value="Build Course +">
					</span>
				<?php }else{ ?>
					<span class="input-group-btn">
						<a class="btn modalClassShow" data-toggle="modal" data-target="#modalNewClass">Create new Class</a>
					</span>
				<?php } ?>
			</div>
		</form>
	</div>
