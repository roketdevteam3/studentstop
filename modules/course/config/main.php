<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 28.07.15
 * Time: 10:02
 */

$module_name = basename(dirname(dirname(__FILE__)));

return [
    'modules' => [
        'course' => [
            'class' => 'app\modules\course\Module',
        ],
    ],

    'components' => [
        'urlManager' => [
            'rules' => [
                'resetclass' => 'course/default/resetclass',
                'changefollowclassstatus' => 'course/default/changefollowclassstatus',

                'virtualclass/pay_class' => 'course/info/pay_class',
                'virtualclass/pay_class_credits' => 'course/info/pay_class_credits',
                'virtualclass/pay' => 'course/info/pay',

                'product/pay_product' => 'course/default/pay_product',
                'product/pay_product_credits' => 'course/default/pay_product_credits',
                'product/pay' => 'course/default/pay',

                
                'marketclass' => '/course/classes/marketclass',
                'getclassarray' => 'course/classes/getclassarray',
                'course/classes/savedropedfile' => 'course/classes/savedropedfile',
                'course/classes/saveclassfile' => 'course/classes/saveclassfile',
                'getcoursearray' => 'course/classes/getcoursearray',
                'getmajorsarray' => 'course/classes/getmajorsarray',
                'gettutorsarray' => 'course/classes/gettutorsarray',
                'searchclasses' => 'course/classes/searchclasses',
                'courseclases' => 'course/classes/dashboard',
                'course/default/savedropedfile/<class_id:\d+>' => 'course/default/savedropedfile',
                'course/default/deletefilechat/<class_id:\d+>' => 'course/default/deletefilechat',
                'majors' => 'course/default/majors',
                'classes' => 'course/info/index',
                
                'ajaxfavoritevirtualclass' => 'course/info/ajaxfavoritevirtualclass',
                'favoritevirtualclass' => 'course/info/favoritevirtualclass',
                'followconference' => 'course/info/followconference',
                'searchvirtualclass' => 'course/info/searchvirtualclass',
                'videoclass/majorlist' => 'course/info/majorlist',
                'videoclasslist/<course_id:\d+>' => 'course/info/videoclasslist',
                'videolist' => 'course/info/videolist',
                'tutorspage/<tutors_id:\d+>' => 'course/info/tutorspage',
                
                'videoconference/<videoconference_id:\d+>/chat' => 'course/info/videoconference',
                'videoconference/<videoconference_id:\d+>' => 'course/info/videoconference',
                'videoconference/<videoconference_id:\d+>/users' => 'course/info/videoconferenceusers',
                'videoconference/<videoconference_id:\d+>/invitation' => 'course/info/videoconferenceinvitstion',
                'conferenceinvite' => 'course/info/conferenceinvite',
                'invitevirtualclass' => 'course/info/invitevirtualclass',
                'archivedvirtualclass' => 'course/info/archivedvirtualclass',
                
                'courses/<major_name:\w+>' => 'course/default/courses',
                'product/<product_id:\d+>/<class_name:\w+>' => 'course/info/product',
                'course/<course_name:\w+>' => 'course/default/course',
                'tutorsdelete/<tutors_id:\d+>' => 'course/info/tutorsdelete',
                'updateclass/<class_id:\d+>' => 'course/default/updateclass',
                'ajaxfollowclass' => 'course/default/ajaxfollowclass',
                
                'class/<class_name>' => 'course/default/class',
                'class/<class_name>/classtutors' => 'course/default/classtutors',
                'class/<class_name>/classproducts' => 'course/default/classproducts',
                'class/<class_name>/classfiles' => 'course/default/classfiles',
                'class/<class_name>/classchat' => 'course/default/classchat',
                'class/<class_name>/rating' => 'course/default/rating',
                
                $module_name . '/<action:\w+>/<id:\d+>' => $module_name . '/default/<action>',
                $module_name . '/<action:\w+>' => $module_name . '/default/<action>',
            ],
        ],
    ],
];