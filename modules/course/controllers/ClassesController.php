<?php

namespace app\modules\course\controllers;

use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use app\models\user\User;
use app\models\user\UserInfo;
use app\models\user\UserFriend;
use app\models\user\UserFriendRequest;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use Yii;
use app\models\Major;
use app\models\Course;
use app\models\Classes;
use app\modules\university\models\University;

use app\models\Tutors;
use app\models\Files;
use app\models\Product;
use app\models\Productpay;
use app\models\Follower;
use app\models\user\Event;
use app\models\user\UserEdit;
use app\models\Ratingrequest;
use app\models\Classreting;
use yii\db\Query;
use app\modules\users\models\UserImages;
use app\modules\users\models\UserNews;
use app\modules\users\models\UserEvents;
 error_reporting( E_ERROR );
/**
 * Default controller for the `course` module
 */
class ClassesController extends Controller
{
    
    public function beforeAction($action) {
        if(!\Yii::$app->user->isGuest){
            if(Yii::$app->user->identity->getUsertUniversity()){
                $this->enableCsrfValidation = false;
                return parent::beforeAction($action);
            }else{
                return $this->redirect(Url::to("/users/choiceuniversity",true));
            }
        }else{
            return $this->redirect(Url::to(Url::home()."users/login",true));
        }
    }

    public function actionDashboard()
    {
        
        $modelNewClass = new Classes();
        $modelNewClass->scenario = 'add_class';
        if($_POST){
            $request = Yii::$app->request;
            if($modelNewClass->load($request->post())){
                if($modelNewClass->save()){
                    $newModelFollow = new Follower();
                    $newModelFollow->id_user = \Yii::$app->user->id;
                    $newModelFollow->id_object = $modelNewClass->id;
                    $newModelFollow->type_object = 'class';
                    if($newModelFollow->save()){}
                    Yii::$app->session->setFlash('class_added');
                }else{
                    Yii::$app->session->setFlash('class_not_added');
                }
            }
        }
        $query = new Query();
        $query->select(['*', 'id' => '{{%class}}.id', 'follow_status' => '{{%follower}}.status' , 'avatar' => '{{%user_info}}.avatar' ,'class_name' => '{{%class}}.name', 'class_url_name' => '{{%class}}.url_name' , 'course_name' => '{{%course}}.name', 
            'major_name' => '{{%major}}.name', 'class_status' => '{{%class}}.status'])
                        ->from('{{%follower}}')
                        ->join('LEFT JOIN',
                                    '{{%class}}',
                                    '{{%class}}.id = {{%follower}}.id_object')
                        ->join('LEFT JOIN',
                                    '{{%course}}',
                                    '{{%course}}.id = {{%class}}.id_course')
                        ->join('LEFT JOIN',
                                    '{{%major}}',
                                    '{{%major}}.id = {{%course}}.id_major')
                        ->join('LEFT JOIN',
                                    '{{%user}}',
                                    '{{%user}}.id = {{%class}}.professor')
                        ->join('LEFT JOIN',
                                    '{{%user_info}}',
                                    '{{%user_info}}.id_user = {{%class}}.professor'
                            )->where(['{{%follower}}.id_user' => \Yii::$app->user->id, '{{%follower}}.type_object' => 'class'])->orderBy('{{%class}}.date_create DESC');

        $modelClass = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 10]]);
        $universityArray = ArrayHelper::map(University::find()->all(), 'id', 'name');
        $professorArray = ArrayHelper::map(User::find()->all(), 'id', 'name');
        $university_id = 0;
        if(!\Yii::$app->user->isGuest){
            $university_id = \Yii::$app->user->identity->getUsertUniversity();
        }
        $majorArray = ArrayHelper::map(Major::find()->where(['university_id' => $university_id])->all(), 'id','name');
        
        return $this->render('dashboard',[
            'majorArray' => $majorArray,
            'professorArray' => $professorArray,
            'universityArray' => $universityArray,
            'modelNewClass' => $modelNewClass,
            'modelClass' => $modelClass->getModels(),
            'pagination' => $modelClass->pagination,
            'count' => $modelClass->pagination->totalCount,
        ]);
    }
    
    public function actionGetclassrank()
    {
        $result = [];
        $class_id = $_POST['class_id'];
        $modelClas = Classes::find()->where(['id' => $class_id])->one();
        $course_id = $modelClas['id_course'];
        $modelClasses = Classes::find()->where(['id_course' => $course_id])->all();
        $count_entry = 0;
        $count_star = 0;
        $classRatingArray = [];
        $class_count = 0;
        foreach($modelClasses as $class){
            $class_count++;
            $modelRatingClass = Classreting::find()->where(['class_id' => $class['id']])->all();
            $countUser = 0;
            $countStar = 0;
            foreach($modelRatingClass as $request){
                $countUser++;
                $countStar = $countStar + $request->rate_professor;
            }
            $result = '';
            if($countUser == 0){
                $countStarClassResult = 0;
            }else{
                $count_entry++;
                $countStarClassResult = round($countStar/$countUser,1); 
            }
            $classRatingArray[$class['id']] = $countStarClassResult;
        }
        arsort($classRatingArray);
        $rent_number = 0;
        foreach($classRatingArray as $key => $class_rating){
            $rent_number = $rent_number + 1;
            if($key == $class_id){
                break;
            }
        }
        $result['rank'] = $rent_number.'/'.$class_count;
        echo json_encode($result);
    }
    
    public function actionGetcourserating()
    {
        $result = [];
        $course_id = $_POST['course_id'];
        $modelClass = Classes::find()->where(['id_course' => $course_id])->all();
        $k = 0;
        $countStarCourse = 0;
        foreach($modelClass as $class){
            $k = $k + 1;
            $modelRatingClass = Classreting::find()->where(['class_id' => $class['id']])->all();
            $countUser = 0;
            $countStar = 0;
            foreach($modelRatingClass as $request){
                $countUser++;
                $countStar = $countStar + $request->rate_professor;
            }
            $result = '';
            if($countUser == 0){
                $countStarResult = 0;
            }else{
                $countStarResult = round($countStar/$countUser,1); 
            }
            $countStarCourse = $countStarCourse + $countStarResult;
        }
        $result['count_star'] = round($countStarCourse/$k,1);
        $result['class_count'] = $k;
        echo json_encode($result);
    }
    
    
    public function actionGetclasscourse()
    {
//        $major_id = 'all';
        $major_id = $_POST['major_id'];
        
        $query = new Query();
        $query->select(['*', 'id' => '{{%class}}.id', 'class_date_active' => '{{%class}}.date_active', 'class_status' => '{{%class}}.status', 'username' => '{{%user}}.name',  'class_url_name' => '{{%class}}.url_name', 
            'class_name' => '{{%class}}.name', 'course_name' => '{{%course}}.name', 'major_name' => '{{%major}}.name', 'class_status' => '{{%class}}.status'])
                        ->from('{{%class}}')
                        ->join('LEFT JOIN',
                                    '{{%course}}',
                                    '{{%course}}.id = {{%class}}.id_course')
                        ->join('LEFT JOIN',
                                    '{{%major}}',
                                    '{{%major}}.id = {{%course}}.id_major')
                        ->join('LEFT JOIN',
                                    '{{%user}}',
                                    '{{%user}}.id = {{%class}}.professor')
                        ->join('LEFT JOIN',
                                    '{{%user_info}}',
                                    '{{%user_info}}.id_user = {{%class}}.professor'
                            );
            $university_id = 0;
            if(!\Yii::$app->user->isGuest){
                $university_id = \Yii::$app->user->identity->getUsertUniversity();
            }
            
            if($major_id == 'all'){
                $query->where(['{{%major}}.university_id' =>  $university_id]);
            }elseif($major_id == 'ready'){
                $query->where(['{{%major}}.university_id' =>  $university_id]);
                
                if($_POST['search_key'] != ''){
                    $search_key = $_POST['search_key'];
                    $query->andWhere(['LIKE', '{{%class}}.name', $search_key]);
                    $query->orWhere(['LIKE', '{{%class}}.description', $search_key]);
                }
                if(($_POST['major_class'] != '') && ($_POST['major_class'] != 0)){
                    $query->andWhere(['{{%major}}.id' => $_POST['major_class']]);
                }
                if(($_POST['course_class'] != '')&& ($_POST['course_class'] != 0)){
                    $query->andWhere(['{{%course}}.id' => $_POST['course_class']]);
                }
                if(($_POST['professor_class'] != '') && ($_POST['professor_class'] != 0)){
                    $query->andWhere(['{{%class}}.professor' => $_POST['professor_class']]);
                }
                
            }else{
                $query->where(['{{%course}}.id_major' => $major_id]);
            }
        
        $command = $query->createCommand();
        $modelClasses = $command->queryAll();
//        var_dump($modelClasses);exit;
        
        //$classArray = [];
        $courseClassArray = [];
        foreach($modelClasses as $class){
            
            $query = new Query();
            $query->select(['*', 'id' => '{{%course}}.id', 'course_name' => '{{%course}}.name', 'major_name' => '{{%major}}.name', 'university_name' => '{{%university}}.name', 'university_image' => '{{%university}}.image',])
                            ->from('{{%course}}')
                            ->join('LEFT JOIN',
                                        '{{%major}}',
                                        '{{%major}}.id = {{%course}}.id_major')
                            ->join('LEFT JOIN',
                                        '{{%university}}',
                                        '{{%university}}.id = {{%major}}.university_id')
                            ->where(['{{%course}}.id' => $class['id_course']]);
            $modelCourse = $query->createCommand();
            
            $courseClassArray[$class['id_course']]['course_info'] = $modelCourse->queryOne();
            $classArray = [];
            $classArray['class_name'] = $class['class_name'];
            $classArray['class_status'] = $class['class_status'];
            $classArray['class_date_active'] = $class['class_date_active'];
            $classArray['class_id'] = $class['id'];
            $classArray['professor'] = $class['username'].' '.$class['surname'];
            $classArray['professor_avatar'] = $class['avatar'];
            $classArray['members'] = Follower::find()->where(['id_object' => $class['id'], 'type_object' => 'class'])->count();
            $classArray['class_url'] = $class['class_url_name'];
            $classArray['follow'] = Follower::find()->where(['id_object' => $class['id'], 'id_user' => \Yii::$app->user->id, 'type_object' => 'class'])->asArray()->one();
            
            $modelRatingClass = Classreting::find()->where(['class_id' => $class['id']])->all();
            $countUser = 0;
            $countStar = 0;
            foreach($modelRatingClass as $request){
                $countUser++;
                $countStar = $countStar + $request->rate_professor;
            }
            $result = '';
            if($countUser == 0){
                $countStarResult = 0;
            }else{
                $countStarResult = round($countStar/$countUser,1); 
            }
            
            $classArray['count_star'] = $countStarResult;
            $courseClassArray[$class['id_course']]['course_classes'][$class['id']] = $classArray;
//		
//            $modelFollow = Follower::find()->where(['id_user' => \Yii::$app->user->id, 'id_object' => $class['id'], 'type_object' => 'class'])->one();
//            if($modelFollow == null){
//                $classArray[$class['id']]['class_join'] =  '<a class="btn JoinClass" data-class_id="'.$class['id'].'">Join</a>';
//            }else{
//                $classArray[$class['id']]['class_join'] =  '<a class="btn UnfollowClass" data-class_id="'.$class['id'].'">Unfollow</a>';
//            }
        }
        
        //var_dump($courseClassArray);exit;
        echo json_encode($courseClassArray);
    }
    
    public function actionSearchclasses()
    {   
        $modelNewClass = new Classes();
        $modelNewClass->scenario = 'add_class';
        if($_POST){
            $request = Yii::$app->request;
            if($modelNewClass->load($request->post())){
                $modelClass = Classes::find()->where(['id_course' => $modelNewClass->id_course, 'professor' => $modelNewClass->professor])->one();
                if(!$modelClass){
                    if($modelNewClass->save()){
                        $newModelFollow = new Follower();
                        $newModelFollow->id_user = \Yii::$app->user->id;
                        $newModelFollow->id_object = $modelNewClass->id;
                        $newModelFollow->type_object = 'class';
                        if($newModelFollow->save()){}
                            $modelNewClass = new Classes();
                            $modelNewClass->scenario = 'add_class';
                            Yii::$app->session->setFlash('class_added');
                            return $this->redirect('/searchclasses');
                    }else{
                        Yii::$app->session->setFlash('class_not_added');
                    }                    
                }else{
                    $modelNewClass = new Classes();
                    $modelNewClass->scenario = 'add_class';
                    Yii::$app->session->setFlash('class_there');                    
                }
            }
        }
        
//                        ->join('LEFT JOIN',
//                                    '{{%major}}',
//                                    '{{%major}}.id = {{%course}}.id_major')
//                        ->join('LEFT JOIN',
//                                    '{{%user}}',
//                                    '{{%user}}.id = {{%class}}.professor'
//                            );
//        $query->select(['*', 'id' => '{{%class}}.id',  'class_url_name' => '{{%class}}.url_name', 'class_name' => '{{%class}}.name', 'course_name' => '{{%course}}.name', 
//            'major_name' => '{{%major}}.name', 'class_status' => '{{%class}}.status'])
//                        ->from('{{%class}}')
//                        ->join('LEFT JOIN',
//                                    '{{%course}}',
//                                    '{{%course}}.id = {{%class}}.id_course')
//                        ->join('LEFT JOIN',
//                                    '{{%major}}',
//                                    '{{%major}}.id = {{%course}}.id_major')
//                        ->join('LEFT JOIN',
//                                    '{{%user}}',
//                                    '{{%user}}.id = {{%class}}.professor'
//                            );
//        if($_GET){
//            if(isset($_GET['search_key'])){
//                if($_GET['search_key'] != ''){
//                    $search_key = $_GET['search_key'];
//                    $query->where(['LIKE', '{{%class}}.name', $search_key]);
//                    $query->orWhere(['LIKE', '{{%class}}.description', $search_key]);
//                }
//            }
//            if(isset($_GET['university_class'])){
//                if(($_GET['university_class'] != '') && ($_GET['university_class'] != '0')){
//                    $query->where(['{{%class}}.id_university' => $_GET['university_class']]);
//                }
//            }
//            if(isset($_GET['major_class'])){
//                if(($_GET['major_class'] != '') && ($_GET['major_class'] != '0')){
//                    $query->andWhere(['{{%major}}.id' => $_GET['major_class']]);
//                }
//            }
//            if(isset($_GET['course_class'])){
//                if(($_GET['course_class'] != '') && ($_GET['course_class'] != '0')){
//                    $query->andWhere(['{{%course}}.id' => $_GET['course_class']]);
//                }
//            }
//            if(isset($_GET['class_class'])){
//                if(($_GET['class_class'] != '') && ($_GET['class_class'] != '0')){
//                    $query->andWhere(['{{%class}}.id' => $_GET['class_class']]);
//                }
//            }
//            if(isset($_GET['professor_class'])){
//                if(($_GET['professor_class'] != '') && ($_GET['professor_class'] != '0')){
//                    $query->andWhere(['{{%class}}.professor' => $_GET['professor_class']]);
//                }
//            }
//            
//        }
        $university_id = 0;
        if(!\Yii::$app->user->isGuest){
            $university_id = \Yii::$app->user->identity->getUsertUniversity();
        }
        $majorArray = ArrayHelper::map(Major::find()->where(['university_id' => $university_id])->all(), 'id','name');
        $modelMajor = Major::find()->where(['university_id' => $university_id])->all();
//        $modelClass = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 10]]);
//        var_dump($modelClass->getModels());exit;
        
        $universityArray = ArrayHelper::map(University::find()->all(), 'id', 'name');
        
        
        $query = new Query();
        $query->select(['*', 'id' => '{{%user}}.id'])
                        ->from('{{%user}}')
                        ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%user}}.id'
                                )->where(['{{%user_info}}.academic_status' => 3]);

        $command = $query->createCommand();
        $modelProfessor = $command->queryAll();
        $professorArray = [];
        foreach($modelProfessor as $professor){
            $professorArray[$professor['id_user']] = $professor['name'].' '.$professor['surname'];
        }
        
        return $this->render('searchclasses',[
            'universityArray' => $universityArray,
            'professorArray' => $professorArray,
            'modelNewClass' => $modelNewClass,
            'modelMajor' => $modelMajor,
            'majorArray' => $majorArray,
//            'modelClass' => $modelClass->getModels(),
//            'pagination' => $modelClass->pagination,
//            'count' => $modelClass->pagination->totalCount,
        ]);
    }
    
    public function actionMarketclass()
    {
        
        return $this->render('marketclass',[
        ]);
    }
    
    
    public function actionGetmajorsarray()
    {
        $majorArray = ArrayHelper::map(Major::find()->where(['university_id' => $_POST['university_id']])->all(), 'id', 'name');
        echo json_encode($majorArray);
    }
    
    public function actionGetcoursearray()
    {
        $courseArray = ArrayHelper::map(Course::find()->where(['id_major' => $_POST['major_id']])->all(), 'id', 'name');
        echo json_encode($courseArray);
    }
    
    public function actionGettutorsarray()
    {
        $query = new Query();
        $query->select(['*', 'id' => '{{%user_tutors_info}}.id', 'about' => '{{%user_tutors_info}}.about'])
                        ->from('{{%tutors_course}}')
                        ->join('LEFT JOIN',
                                        '{{%user_tutors_info}}',
                                        '{{%user_tutors_info}}.id = {{%tutors_course}}.tutors_id')
                        ->join('LEFT JOIN',
                                        '{{%user}}',
                                        '{{%user}}.id = {{%user_tutors_info}}.user_id')
                        ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%user_tutors_info}}.user_id'
                                )->where(['{{%tutors_course}}.course_id' => $_POST['course_id']]);
        $command = $query->createCommand();
        $modelTutors = $command->queryAll();
        $tutorsArray = [];
        foreach($modelTutors as $tutors){
            $tutorsArray[$tutors['id']] = $tutors['name'].' '.$tutors['surname'];
        }
        echo json_encode($tutorsArray);
    }
    
    public function actionGetclassarray()
    {
        $classArray = ArrayHelper::map(Classes::find()->where(['id_course' => $_POST['course_id']])->all(), 'id', 'name');
        echo json_encode($classArray);
    }
    
    public function actionSavedropedfile(){
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/images/classes/';   //2
        // mkdir($storeFolder);
        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
              mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }
              $tempFile = $_FILES['file']['tmp_name'];          //3

              $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;  //4

              $for_name = time();

              $targetFile =  $targetPath.$for_name.'.jpg';  //5

              move_uploaded_file($tempFile,$targetFile); //6
              return \Yii::$app->user->id.'/'.$for_name.'.jpg';
        }

    }
    
    public function actionSaveclassfile(){
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/files/classes/';   //2
        // mkdir($storeFolder);
        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
                mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }
            $tempFile = $_FILES['file']['tmp_name'];          //3
            $fileType = $_FILES['file']['type'];    //3
            $arrayFormat = [
                'video/x-flv' => 'flv',
                'video/mp4' => 'mp4',
                'video/MP2T' => 'ts',
                'application/x-mpegURL' => 'm3u8',
                'video/3gpp' => '3gp',
                'video/quicktime' => 'mov',
                'video/x-msvideo' => 'avi',
                'video/x-ms-wmv' => 'wmv',
                'application/msword' => 'doc',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'docx',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.template' => 'dotx',
                'application/vnd.ms-word.document.macroEnabled.12' => 'docm',
                'application/vnd.ms-word.template.macroEnabled.12' => 'dotm',
                'application/vnd.ms-excel' => 'xls',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 'xlsx',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.template' => 'xltx',
                'application/vnd.ms-excel.sheet.macroEnabled.12' => 'xlsm',
                'application/vnd.ms-excel.template.macroEnabled.12' => 'xltm',
                'application/vnd.ms-excel.addin.macroEnabled.12' => 'xlam',
                'application/vnd.ms-excel.sheet.binary.macroEnabled.12' => 'xlsb',
                'application/vnd.ms-powerpoint' => 'ppt',
                'application/vnd.openxmlformats-officedocument.presentationml.presentation' => 'pptx',
                'application/vnd.openxmlformats-officedocument.presentationml.template' => 'potx',
                'application/vnd.openxmlformats-officedocument.presentationml.slideshow' => 'ppsx',
                'application/vnd.ms-powerpoint.addin.macroEnabled.12' => 'ppam',
                'application/vnd.ms-powerpoint.presentation.macroEnabled.12' => 'pptm',
                'application/vnd.ms-powerpoint.template.macroEnabled.12' => 'potm',
                'application/vnd.ms-powerpoint.slideshow.macroEnabled.12' => 'ppsm',
                'application/x-7z-compressed' => '7z',
                'application/x-gtar' => 'tar.gz',
                'application/zip' => 'zip',
                'application/x-rar-compressed' => 'rar',
                'image/bmp' => 'bmp',
                'image/gif' => 'gif',
                'image/jpeg' => 'jpeg',
                'image/png' => 'png',

            ];
            $keyStatus = array_key_exists($fileType, $arrayFormat);
            if($keyStatus == true){
                $fileType = $arrayFormat[$fileType];
            }else{
                $fileType = 'rar';
            }

            $targetPath =  $storeFolder.$ds.Yii::$app->user->id.$ds;  //4
            $for_name = time();
            $targetFile =  $targetPath.$for_name.'.'.$fileType;  //5

            move_uploaded_file($tempFile,$targetFile); //6
            return \Yii::$app->user->id.'/'.$for_name.'.'.$fileType;
        }

    }
}
