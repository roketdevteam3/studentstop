<?php

namespace app\modules\course\controllers;

use app\commands\Paypal;
use app\models\Usercredits;
use Yii;
use yii\base\UserException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use app\models\user\User;
use app\models\user\UserInfo;
use app\models\user\UserFriend;
use app\models\user\UserFriendRequest;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use app\models\Major;
use app\models\Course;
use app\models\Classes;
use app\models\University;
use yii\helpers\Url;
use app\models\Tutorsinfo;

use app\models\Notification;
use app\models\Notificationusers;

use app\models\Creditsactioncount;
use app\models\Creditsaction;


use app\modules\question\models\Question;
use app\modules\question\models\Questioncategory;
use app\models\history\Historyclassmessage;
use app\models\history\Historyfollower;
use app\models\history\Historyfiles;
use app\models\history\Historyproduct;
use app\models\Classmessages;
use app\models\Files;
use app\models\Ratingrequest;
use app\models\Classreting;
use app\models\Product;
use app\models\Productpay;
use app\models\Follower;
use app\models\user\Event;
use app\models\user\UserEdit;
use yii\db\Query;
use app\modules\users\models\UserImages;
use app\modules\users\models\UserNews;
use app\modules\users\models\UserEvents;
use app\models\Settingadmin;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Exception\PayPalConnectionException;
error_reporting( E_ERROR );
/**
 * Default controller for the `course` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function beforeAction($action) {
        if(!\Yii::$app->user->isGuest){
            if(Yii::$app->user->identity->getUsertUniversity()){
                $this->enableCsrfValidation = false;
                return parent::beforeAction($action);
            }else{
                return $this->redirect(Url::to("/users/choiceuniversity",true));
            }
        }else{
            return $this->redirect(Url::to("/users/login",true));
        }
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionResetclass()
    {
        $modelClassDateReset = Settingadmin::find()->where(['key' => 'beginning of the semester'])->one();
        $arrayClassDateReset = json_decode($modelClassDateReset->value);
        $today = date("d").'.'.date("m");
        
            $modelClass = Classes::find()->all();
            foreach($modelClass as $class){
                
                
                $modelClass = Classes::find()->where(['id' => $class->id])->asArray()->one();            
                $userArray = [];
                $modelFollower = Follower::find()->where(['id_object' => $class->id, 'type_object' => 'class'])->asArray()->all();
                foreach($modelFollower as $follower){
                    $userArray[] = $follower['id_user'];
                }

                if($modelClass != null){
                    $university_id = $class['id_university']; 

                    $newModelNotification = new Notification();
                    $newModelNotification->scenario = 'add_notification';
                    $newModelNotification->module = 'course_and_class';
                    $newModelNotification->from_object_id = $class->id;
                    $newModelNotification->to_object_id = $class->id;
                    $newModelNotification->university_id = $university_id;
                    $newModelNotification->notification_type = 'reset_class';
                    if($newModelNotification->save()){
                        foreach($userArray as $user_id){
                            $modelNotificationUser = new Notificationusers();
                            $modelNotificationUser->scenario = 'new_notification';
                            $modelNotificationUser->user_id = $user_id;
                            $modelNotificationUser->notification_id = $newModelNotification->id;
                            $modelNotificationUser->save();
                        }                
                    }
                }
                
                
                $modelFollowerClass = Follower::find()->where(['type_object' => 'class', 'id_object' => $class->id])->all();
                foreach($modelFollowerClass as $follow){
                    $modelFolClass = Follower::find()->where(['id' => $follow->id])->one();
                    if($modelFolClass != null){
                        $modelFolClass->status = 2;
                        $modelFolClass->save();
                    }
                    $modelFollowerHistory = new Historyfollower();
                    $modelFollowerHistory->scenario = 'add_follower';
                    $modelFollowerHistory->id_user = $follow->id_user;
                    $modelFollowerHistory->id_object = $follow->id_object;
                    $modelFollowerHistory->type_object = $follow->type_object;
                    $modelFollowerHistory->date_create = $follow->date_create;
                    $modelFollowerHistory->save();
                }

                $modelMessageClass = Classmessages::find()->where(['class_id' => $class->id])->all();
                foreach($modelMessageClass as $message){
                    $modelHistoryclassmessage = new Historyclassmessage();
                    $modelHistoryclassmessage->scenario = 'add_message'; 
                    $modelHistoryclassmessage->user_id = $message->user_id; 
                    $modelHistoryclassmessage->class_id = $message->class_id; 
                    $modelHistoryclassmessage->message = $message->message; 
                    $modelHistoryclassmessage->created_add = $message->created_add;
                    if($modelHistoryclassmessage->save()){
                        $modelFiles = Files::find()->where(['class_id' => $class->id])->all();
                        foreach($modelFiles as $file){
                            $modelHistoryfiles = new Historyfiles();
                            $modelHistoryfiles->scenario = 'add_file';
                            $modelHistoryfiles->user_id = $file->user_id;
                            $modelHistoryfiles->file_src = $file->file_src;
                            $modelHistoryfiles->file_type = $file->file_type;
                            $modelHistoryfiles->class_id = $file->class_id;
                            $modelHistoryfiles->description = $file->description;
                            $modelHistoryfiles->date_create = $file->date_create;
                            if($modelHistoryfiles->save()){
                                $modelFile = Files::find()->where(['id' => $file->id])->one();
                                $modelFile->delete();
                            }
                        }
                        $modelHClassMessage = Classmessages::find()->where(['id' => $message->id])->one();
                        $modelHClassMessage->delete();

                    }

                }

            }
    }
    
    public function actionAddclassevent()
    {
        $result = [];
        $modelNewEvent = new \app\models\Event();
        $modelNewEvent->place_type = $_POST['place_type'];
        $modelNewEvent->place_id = $_POST['place_id'];
        $modelNewEvent->class_id = $_POST['class_id'];
        $modelNewEvent->owner_id = \Yii::$app->user->id;
        $modelNewEvent->title = $_POST['title'];
        $modelNewEvent->image = $_POST['image'];
        $modelNewEvent->short_desc = $_POST['short_desc'];
        $modelNewEvent->content = $_POST['content'];
        $modelNewEvent->type = $_POST['type'];
        $modelNewEvent->date_start = $_POST['date_start'];
        $modelNewEvent->date_end = $_POST['date_end'];
        if($modelNewEvent->save()){
            
            
            
            $modelClass = Classes::find()->where(['id' => $_POST['class_id']])->asArray()->one();
            
            $userArray = [];
            $modelFollower = Follower::find()->where(['id_object' => $modelClass['id'], 'type_object' => 'class'])->asArray()->all();
            foreach($modelFollower as $follower){
                $userArray[] = $follower['id_user'];                
            }

            if($modelClass != null){
                $university_id = $modelClass['id_university']; 

                $newModelNotification = new Notification();
                $newModelNotification->scenario = 'add_notification';
                $newModelNotification->module = 'course_and_class';
                $newModelNotification->from_object_id = \Yii::$app->user->id;
                $newModelNotification->to_object_id = $modelNewEvent->id;
                $newModelNotification->university_id = $university_id;
                $newModelNotification->notification_type = 'add_event_to_class';
                if($newModelNotification->save()){
                    foreach($userArray as $user_id){
                        $modelNotificationUser = new Notificationusers();
                        $modelNotificationUser->scenario = 'new_notification';
                        $modelNotificationUser->user_id = $user_id;
                        $modelNotificationUser->notification_id = $newModelNotification->id;
                        $modelNotificationUser->save();
                    }                
                }
            }
            
            
            
            $result['status'] = 'save';
        }else{
            $result['status'] = 'error';
        }
        echo json_encode($result);
    }
    
    public function actionAddclassquestion()
    {
        $result = [];
        $modelNewQuestion = new Question();
        $modelNewQuestion->scenario = 'add_question';
        $modelNewQuestion->question_title = $_POST['question_title'];
        if(!\Yii::$app->user->isGuest){
            $university_id = \Yii::$app->user->identity->getUsertUniversity();
        }
        $modelNewQuestion->university_id = $university_id;
        $modelNewQuestion->question_text = $_POST['question_text'];
        $modelNewQuestion->privacy = $_POST['question_privacy'];
        $modelNewQuestion->user_id = \Yii::$app->user->id;
        $modelNewQuestion->category_id = $_POST['question_category'];
        $modelNewQuestion->price = $_POST['question_price'];
        $modelNewQuestion->class_id = $_POST['question_class_id'];

        if($modelNewQuestion->save()){
            
            
            $modelClass = Classes::find()->where(['id' => $_POST['question_class_id']])->asArray()->one();
            
            $userArray = [];
            $modelFollower = Follower::find()->where(['id_object' => $modelClass['id'], 'type_object' => 'class'])->asArray()->all();
            foreach($modelFollower as $follower){
                $userArray[] = $follower['id_user'];                
            }

            if($modelClass != null){
                $university_id = $modelClass['id_university']; 

                $newModelNotification = new Notification();
                $newModelNotification->scenario = 'add_notification';
                $newModelNotification->module = 'course_and_class';
                $newModelNotification->from_object_id = \Yii::$app->user->id;
                $newModelNotification->to_object_id = $modelNewQuestion->id;
                $newModelNotification->university_id = $university_id;
                $newModelNotification->notification_type = 'add_file_to_class';
                if($newModelNotification->save()){
                    foreach($userArray as $user_id){
                        $modelNotificationUser = new Notificationusers();
                        $modelNotificationUser->scenario = 'new_notification';
                        $modelNotificationUser->user_id = $user_id;
                        $modelNotificationUser->notification_id = $newModelNotification->id;
                        $modelNotificationUser->save();
                    }                
                }
            }
            
            
            $result['status'] = 'save';
        }else{
            $result['status'] = 'error';
        }
        echo json_encode($result);
    }
    
    public function actionClassquestion()
    {
        $class_id = $_POST['class_id'];
        $modelClassQuestion = Question::find()->where(['class_id' => $class_id])->asArray()->all();
        echo json_encode($modelClassQuestion);
    }
    
    public function actionGetclassevent()
    {
        $class_id = $_POST['class_id'];
        $modelEvents = \app\models\Event::find()->where(['class_id' => $class_id])->asArray()->all();
        echo json_encode($modelEvents);
    }
    
    public function actionCourseclases()
    {
        $modelUserinfo = Userinfo::find()->where(['id_user' => \Yii::$app->user->id])->one();
        if($modelUserinfo != null){
            if($modelUserinfo->university_id != 0){
                
                return $this->render('courseclasses',[
                    'status' => 'success',
                ]);
                
            }else{
                return $this->render('courseclasses',[
                    'status' => 'error',
                ]);
            }
        }else{
            return $this->render('courseclasses',[
                'status' => 'error',
            ]);
        }
    }
    
    public function actionGetclasstop()
    {
        $result = [];
        $class_id = $_POST['class_id'];
        $modelClas = Classes::find()->where(['id' => $class_id])->one();
        $course_id = $modelClas['id_course'];
        $modelClasses = Classes::find()->where(['id_course' => $course_id])->all();
        $count_entry = 0;
        $count_star = 0;
        $classRatingArray = [];
        $class_count = 0;
        foreach($modelClasses as $class){
            $class_count++;
            $modelRatingClass = Classreting::find()->where(['class_id' => $class['id']])->all();
            $countUser = 0;
            $countStar = 0;
            foreach($modelRatingClass as $request){
                $countUser++;
                $countStar = $countStar + $request->rate_professor;
            }
            $result = '';
            if($countUser == 0){
                $countStarClassResult = 0;
            }else{
                $count_entry++;
                $countStarClassResult = round($countStar/$countUser,1); 
            }
            $classRatingArray[$class['id']] = $countStarClassResult;
        }
        arsort($classRatingArray);
        $k = 0;
        $classArray = [];
        foreach($classRatingArray as $class_id => $rating){
            $k = $k + 1;
            if($k < 6){
                $modelClass = Classes::find()->where(['{{%class}}.id' => $class_id])->asArray()->one();
        
                $classArray[$class_id]['class_info'] = $modelClass;
                $classArray[$class_id]['class_rating'] = $rating;
            }else{
                break;
            }
        }
        
        echo json_encode($classArray);
    }
    
    public function actionGetclasstopprofessor()
    {
        $class_id = $_POST['class_id'];
        $classModel = Classes::find()->where(['id' => $class_id])->one();
        $modelClasses = Classes::find()->where(['id_course' => $classModel['id_course']])->asArray()->all();

        $professor_id = [];
        foreach($modelClasses as $class){
            $professor_id[] = $class['professor'];
        }
        
        $professorRating = [];
        foreach($professor_id as $id){
            $modelRatingrequest = Ratingrequest::find()->where(['to_object_id' => $id, 'type' => 'professor'])->asArray()->all();
            
            $countUser = 0;
            $countStar = 0;
            foreach($modelRatingrequest as $rating){
                $countUser++;
                $countStar = $countStar + $rating['rating_count'];
            }
            $result = '';
            if($countUser == 0){
                $countStarResult = 0;
            }else{
                $countStarResult = round($countStar/$countUser,1); 
            }
            
            $professorRating[$id] =  $countStarResult;
        }
        arsort($professorRating);
        $k = 0;
        $professorArray = [];
        foreach($professorRating as $professor_id => $rating){
            $k = $k + 1;
            if($k < 6){
                $query = new Query();
                $query->select(['*', 'id' => '{{%user}}.id', 'username' => '{{%user}}.name', 'user_role_name' => '{{%user_role}}.name'])
                                ->from('{{%user}}')
                                ->join('LEFT JOIN',
                                            '{{%user_info}}',
                                            '{{%user_info}}.id_user = {{%user}}.id')
                                ->join('LEFT JOIN',
                                            '{{%user_role}}',
                                            '{{%user_role}}.id = {{%user_info}}.academic_status'
                                        )->where(['{{%user}}.id' => $professor_id]);
                $command = $query->createCommand();
                $modelUser = $command->queryOne();
        
                $professorArray[$professor_id]['professor_info'] = $modelUser;
                $professorArray[$professor_id]['professor_rating'] = $rating;
            }else{
                break;
            }
//            ksort($professorArray);
        }
        echo json_encode($professorArray);
    }
    
    public function actionGetclasstopuser()
    {
        $class_id = $_POST['class_id'];
        $classModel = Classes::find()->where(['id' => $class_id])->one();
        $modelClasses = Classes::find()->where(['id_course' => $classModel['id_course']])->asArray()->all();
        
        $class_id = [];
        foreach($modelClasses as $class){
            $class_id[] = $class['id'];
        }
        $modelClassFollowers = Follower::find()->where(['type_object' => 'class', 'id_object' => $class_id])->all();
        $userClassFollowId = [];
        foreach($modelClassFollowers as $classFollow){
            $userClassFollowId[] = $classFollow->id_user;
        }
        
        
        $userRating = [];
        foreach($userClassFollowId as $user_id){
            $modelRatingUser = Ratingrequest::find()->where(['to_object_id' => $user_id])->all();
            $countUser = 0;
            $countStar = 0;
            foreach($modelRatingUser as $request){
                $countUser++;
                $countStar = $countStar + $request['rating_count'];
            }
            $result = '';
            if($countUser == 0){
                $countStarResult = 0;
            }else{
                $countStarResult = round($countStar/$countUser,1);
            }
            $userRating[$user_id] =  $countStarResult;
            
        }

        $userArray = [];
        foreach($userRating as $user_id => $rating){
            $k = $k + 1;
            if($k < 6){
                $query = new Query();
                $query->select(['*', 'id' => '{{%user}}.id', 'username' => '{{%user}}.name', 'user_role_name' => '{{%user_role}}.name'])
                                ->from('{{%user}}')
                                ->join('LEFT JOIN',
                                            '{{%user_info}}',
                                            '{{%user_info}}.id_user = {{%user}}.id')
                                ->join('LEFT JOIN',
                                            '{{%user_role}}',
                                            '{{%user_role}}.id = {{%user_info}}.academic_status'
                                        )->where(['{{%user}}.id' => $user_id]);
                $command = $query->createCommand();
                $modelUser = $command->queryOne();
                
                $userArray[$user_id]['user_info'] = $modelUser;
                $userArray[$user_id]['user_rating'] = $rating;
            }else{
                break;
            }
        }
            
        echo json_encode($userArray);
        
    }
    
    public function actionMajors()
    {
        $modelUserinfo = Userinfo::find()->where(['id_user' => \Yii::$app->user->id])->one();
        if($modelUserinfo->university_id != 0){
            $queryMajors = Major::find()->where(['university_id' => $modelUserinfo->university_id]);
            $modelMajors = new ActiveDataProvider(['query' => $queryMajors, 'pagination' => ['pageSize' => 10]]);
            
            return $this->render('majors',[
                'modelMajors' => $modelMajors->getModels(),
                'pagination' => $modelMajors->pagination,
                'count' => $modelMajors->pagination->totalCount,
            ]);
            
        }else{
            echo '404';
        }
    }
    
    public function actionCourses($major_name)
    {
        $modelMajor = Major::find()->where(['url_name' => $major_name])->one();
        $modelNewCourse = new Course();
        $modelNewCourse->scenario = 'add_course';
        if($_POST){
            $request = Yii::$app->request;
            if($modelNewCourse->load($request->post())){
                if($modelNewCourse->save()){
                    Yii::$app->session->setFlash('course_added');
                }else{
                    Yii::$app->session->setFlash('course_notadded');
                }
            }
        }
        $queryCourse = Course::find()->where(['id_major' => $modelMajor->id]);
        $modelCourse = new ActiveDataProvider(['query' => $queryCourse, 'pagination' => ['pageSize' => 10]]);
        
        return $this->render('courses',[
            'modelNewCourse' => $modelNewCourse,
            'modelMajor' => $modelMajor,
            'modelCourse' => $modelCourse->getModels(),
            'pagination' => $modelCourse->pagination,
            'count' => $modelCourse->pagination->totalCount,
        ]);
    }
    
    public function actionCourse($course_name)
    {
        $modelCourse = Course::find()->where(['url_name' => $course_name])->one();
        $modelCourse->scenario = 'update_course';
        $modelMajor = Major::find()->where(['id' => $modelCourse->id_major])->one();
        $modelNewClass = new Classes();
        $modelNewClass->scenario = 'add_class';
        if($_POST){
            if(isset($_POST['Course'])){
                $request = Yii::$app->request;
                if($modelCourse->load($request->post())){
                    if($modelCourse->save()){
                        Yii::$app->session->setFlash('course_update');
                    }else{
                        Yii::$app->session->setFlash('course_notupdate');
                    }
                }
            }
            if(isset($_POST['Classes'])){
                $request = Yii::$app->request;
                if($modelNewClass->load($request->post())){
                    if($modelNewClass->save()){
                        Yii::$app->session->setFlash('class_added');
                    }else{
                        Yii::$app->session->setFlash('class_notadded');
                    }
                }
            }
        }
        $userArray = ArrayHelper::map(User::find()->all(), 'id', 'name');
        $queryClass = Classes::find()->where(['id_course' => $modelCourse->id]);
        $modelClass = new ActiveDataProvider(['query' => $queryClass, 'pagination' => ['pageSize' => 10]]);
        return $this->render('course',[ 
            'userArray' => $userArray,
            'modelMajor' => $modelMajor,
            'modelCourse' => $modelCourse,
            'modelNewClass' => $modelNewClass,
            'modelClass' => $modelClass->getModels(),
            'pagination' => $modelClass->pagination,
            'count' => $modelClass->pagination->totalCount,
        ]);
    }
    
    public function actionRating($class_name)
    {
        $modelClass = Classes::find()->where(['url_name' => $class_name])->one();
        $modelClass->scenario = 'class_update';
        $modelCourse = Course::find()->where(['id' => $modelClass->id_course])->one();
        $modelMajor = Major::find()->where(['id' => $modelCourse->id_major])->one();
        $modelUniversity = University::find()->where(['id' => $modelMajor->university_id])->one();
        $modelFollower = Follower::find()->where(['id_user' => \Yii::$app->user->id,'id_object' => $modelClass->id,'type_object' => 'class' ])->one();
        if($modelFollower == null){
            $followStatus = 'Follow';
        }else{
            $followStatus = 'Unfollow';
        }
        $userArray = ArrayHelper::map(User::find()->all(), 'id', 'name');
        
        $query = new Query();
        $query->select(['*', 'id' => '{{%user}}.id'])
                        ->from('{{%user}}')
                        ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%user}}.id'
                                )->where(['{{%user}}.id' => $modelClass->professor]);
        
        $command = $query->createCommand();
        $modelProfessor = $command->queryOne();
        
        //$queryClassRating = Classreting::find()->where(['class_id' => $modelClass->id]);
        
        $queryClassRating = new Query();
        $queryClassRating->select(['*', 'id' => '{{%class_rating}}.id'])
                        ->from('{{%class_rating}}')
                        ->join('LEFT JOIN',
                                    '{{%user}}',
                                    '{{%user}}.id = {{%class_rating}}.user_id')
                        ->join('LEFT JOIN',
                                    '{{%user_info}}',
                                    '{{%user_info}}.id_user = {{%class_rating}}.user_id'
                                )->where(['{{%class_rating}}.class_id' => $modelClass->id]);
        
        $modelClassRating = new ActiveDataProvider(['query' => $queryClassRating, 'pagination' => ['pageSize' => 10]]);
        
        return $this->render('/class/rating',[
            'modelProfessor' => $modelProfessor,
            'userArray' => $userArray,
            'followStatus' => $followStatus,
            'modelClass' => $modelClass,
            'modelCourse' => $modelCourse,
            'modelMajor' => $modelMajor,
            'modelUniversity' => $modelUniversity,
            'modelClassRating' => $modelClassRating->getModels(),
            'pagination' => $modelClassRating->pagination,
            'count' => $modelClassRating->pagination->totalCount,
        ]);
    }
    
    public function actionGetclassinfo()
    {
        $class_id = $_POST['class_id'];
        $result = [];
        $result['class_info'] = $modelClass = Classes::find()->where(['id' => $class_id])->asArray()->one();
        $result['course_info'] = $modelCourse = Course::find()->where(['id' => $modelClass['id_course']])->asArray()->one();
        $result['major_info'] = $modelMajor = Major::find()->where(['id' => $modelCourse['id_major']])->asArray()->one();
        $result['university_info'] = $modelUniversity = University::find()->where(['id' => $modelMajor['university_id']])->asArray()->one();
        $query = new Query();
        $query->select(['*', 'id' => '{{%user}}.id'])
                        ->from('{{%user}}')
                        ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%user}}.id'
                                )->where(['{{%user}}.id' => $modelClass['professor']]);

        $command = $query->createCommand();
        $modelProfessor = $command->queryOne();
        $result['professor_info'] = $modelProfessor;
        echo json_encode($result);
    }
    
    public function actionGetclassusers()
    {
        $class_id = $_POST['class_id'];
        
        $modelClassFollowers = Follower::find()->where(['type_object' => 'class', 'id_object' => $class_id])->all();
        $userClassFollowId = [];
        foreach($modelClassFollowers as $classFollow){
            $userClassFollowId[] = $classFollow->id_user;
        }
        
        $query = new Query();
        $query->select(['*', 'id' => '{{%user}}.id', 'username' => '{{%user}}.name', 'user_role_name' => '{{%user_role}}.name'])
                        ->from('{{%user}}')
                        ->join('LEFT JOIN',
                                    '{{%user_info}}',
                                    '{{%user_info}}.id_user = {{%user}}.id')
                        ->join('LEFT JOIN',
                                    '{{%user_role}}',
                                    '{{%user_role}}.id = {{%user_info}}.academic_status'
                                )->where(['{{%user}}.id' => $userClassFollowId]);
        $command = $query->createCommand();
        $modelUserClassFollow = $command->queryAll();
        echo json_encode($modelUserClassFollow);
    }
    
    public function actionClassevent()
    {
        $result = [];
        $modelEvent = \app\models\Event::find()->where(['id' => $_POST['event_id']])->asArray()->one();
        $result['event'] = $modelEvent;
        
        $modelUser = User::find()->where(['id' => $modelEvent['owner_id']])->one();
        $modelUserInfo = UserInfo::find()->where(['id_user' => $modelEvent['owner_id']])->one();
        $result['user_info']['username'] = $modelUser->name." ".$modelUser->surname;
        $result['user_info']['avatar'] = $modelUserInfo->avatar;
        
        echo json_encode($result);
    }
    public function actionClassgetquestion()
    {
        $result = [];
        $question_id = $_POST['question_id'];
        $modelQuestion = Question::find()->where(['id' => $question_id])->asArray()->one();
        $result['question'] = $modelQuestion;
        
//        $modelAnswers = \app\modules\question\models\Answer::find()->where(['question_id' => $question_id])->asArray()->all();
        $query = new Query();
        $query->select(['*', 'id' => '{{%answer}}.id'])
                        ->from('{{%answer}}')
                        ->join('LEFT JOIN',
                                        '{{%user}}',
                                        '{{%user}}.id = {{%answer}}.user_id')
                        ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%user}}.id'
                                )->where(['{{%answer}}.question_id' => $modelQuestion['id']]);
        $command = $query->createCommand();
        $modelAnswers = $command->queryAll();
        
        $result['answers'] = $modelAnswers;
        
        $modelUser = User::find()->where(['id' => $modelQuestion['user_id']])->one();
        $modelUserInfo = UserInfo::find()->where(['id_user' => $modelQuestion['user_id']])->one();
        
        $result['user_info']['username'] = $modelUser->name." ".$modelUser->surname;
        $result['user_info']['avatar'] = $modelUserInfo->avatar;
        
        echo json_encode($result);
    }
    
    public function actionClass($class_name)
    {
        //return $this->redirect('/class/'.$class_name.'/classtutors');
        $modelNewRating = new Classreting();
        $modelNewRating->scenario = 'add_rating';
        $model_event = new \app\models\Event();
        $modelClass = Classes::find()->where(['url_name' => $class_name])->one();
        $modelClass->scenario = 'class_update';
        $modelCourse = Course::find()->where(['id' => $modelClass->id_course])->one();
        $modelMajor = Major::find()->where(['id' => $modelCourse->id_major])->one();
        $modelUniversity = University::find()->where(['id' => $modelMajor->university_id])->one();
        $query = new Query();
        $query->select(['*', 'id' => '{{%user}}.id'])
                        ->from('{{%user}}')
                        ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%user}}.id'
                                )->where(['{{%user}}.id' => $modelClass->professor]);

        $command = $query->createCommand();
        $modelProfessor = $command->queryOne();
        
        
        $modelFollower = Follower::find()->where(['id_user' => \Yii::$app->user->id,'id_object' => $modelClass->id,'type_object' => 'class' ])->one();
        //var_dump($modelFollower);exit;
        $modelFollowerStatus = '';
        if($modelFollower == null){
            $followStatus = 'Follow';
            $modelFollowerStatus = '';
        }else{
            $followStatus = 'Unfollow';
            $modelFollowerStatus = $modelFollower->status;
        }
        if($_POST){
            $request = Yii::$app->request;
            if($modelNewRating->load($request->post())){
                if($modelNewRating->save()){
                    $modelRatingRequest = new Ratingrequest();
                    $modelRatingRequest->scenario = 'add_rating_request';
                    $modelRatingRequest->from_user_id = \Yii::$app->user->id;
                    $modelRatingRequest->to_object_id = $modelClass->professor;
                    $modelRatingRequest->rating_count = $modelNewRating->rate_professor;
                    $modelRatingRequest->type = 'professor';
                    if($modelRatingRequest->save()){
                        
                    }
                    return $this->redirect('/class/'.$modelClass->url_name);
                }
            }
        }
        
//        $modelClassFollowers = Follower::find()->where(['type_object' => 'class', 'id_object' => $modelClass->id])->all();
//        $userClassFollowId = [];
//        foreach($modelClassFollowers as $classFollow){
//            $userClassFollowId[] = $classFollow->id_user;
//        }
//        
//        $query = new Query();
//        $query->select(['*', 'id' => '{{%user}}.id'])
//                        ->from('{{%user}}')
//                        ->join('LEFT JOIN',
//                                    '{{%user_info}}',
//                                    '{{%user_info}}.id_user = {{%user}}.id'
//                                )->where(['{{%user}}.id' => $userClassFollowId]);
//        $command = $query->createCommand();
//        $modelUserClassFollow = $command->queryAll();
        $category_array = ArrayHelper::map(Questioncategory::find()->where(['parent_id' => 0])->all(), 'id', 'name'); 
        $userArray = ArrayHelper::map(User::find()->all(), 'id', 'name');
        
        $modelClassFilesImage = Files::find()->where(['class_id' => $modelClass['id'], 'mime_type' => 'image'])->asArray()->limit(4)->all();
                
        return $this->render('/class/class',[ 
            'modelClassFilesImage' => $modelClassFilesImage,
            'category_array' => $category_array,
            'modelUniversity' => $modelUniversity,
            'modelProfessor' => $modelProfessor,
            'modelNewRating' => $modelNewRating,
            'followStatus' => $followStatus,
            'modelCourse' => $modelCourse,
            'modelClass' => $modelClass,
            'modelMajor' => $modelMajor,
            'userArray' => $userArray,
            'modelFollower' => $modelFollower,
            'classEnded' => $modelFollowerStatus,
            'model_event' => $model_event
        ]);
    }
                
    public function actionGetclassquestion()
    {
        echo json_encode($_POST);
    }
    
    public function actionClasstutors($class_name)
    {
        $modelNewRating = new Classreting();
        $modelNewRating->scenario = 'add_rating';
        
        $modelClass = Classes::find()->where(['url_name' => $class_name])->one();
        $modelClass->scenario = 'class_update';
        $modelCourse = Course::find()->where(['id' => $modelClass->id_course])->one();
        
        $modelMajor = Major::find()->where(['id' => $modelCourse->id_major])->one();
        $modelUniversity = University::find()->where(['id' => $modelMajor->university_id])->one();
        $query = new Query();
        $query->select(['*', 'id' => '{{%user}}.id'])
                        ->from('{{%user}}')
                        ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%user}}.id'
                                )->where(['{{%user}}.id' => $modelClass->professor]);

        $command = $query->createCommand();
        $modelProfessor = $command->queryOne();
        $modelTurorsInfo = Tutorsinfo::find()->where(['user_id' => \Yii::$app->user->id])->one();
        $newTutors = null;
        if(!$modelTurorsInfo){
            $newTutors = new Tutorsinfo;
            $newTutors->scenario = 'add_tutors';
        }
        $modelCourses = Course::find()->asArray()->all();
        if($_POST){
            if($newTutors->load(\Yii::$app->request->post())){
                if($newTutors->save()){
                    if(isset($_POST['Tutorsinfo']['course'])){
                        foreach($_POST['Tutorsinfo']['course'] as $course_id){
                            $modelNewTutorsCourse = new \app\models\Tutorscourse();
                            $modelNewTutorsCourse->scenario = 'add_tutors';
                            $modelNewTutorsCourse->tutors_id = $newTutors->id;
                            $modelNewTutorsCourse->course_id = $course_id;
                            $modelNewTutorsCourse->save();
                        }
                    }                    
                    $newTutors = null;
                }
            }
        }
        
        $modelFollower = Follower::find()->where(['id_user' => \Yii::$app->user->id, 'id_object' => $modelClass->id, 'type_object' => 'class' ])->one();
        $modelFollowerStatus = '';
        if($modelFollower == null){
            $followStatus = 'Follow';
            $modelFollowerStatus = '';
        }else{
            $followStatus = 'Unfollow';
            $modelFollowerStatus = $modelFollower->status;
        }
        
        $query = new Query();
        $query->select(['*', 'id' => '{{%user_tutors_info}}.id', 'about' => '{{%user_tutors_info}}.about'])
                        ->from('{{%tutors_course}}')
                        ->join('LEFT JOIN',
                                        '{{%user_tutors_info}}',
                                        '{{%user_tutors_info}}.id = {{%tutors_course}}.tutors_id')
                        ->join('LEFT JOIN',
                                        '{{%user}}',
                                        '{{%user}}.id = {{%user_tutors_info}}.user_id')
                        ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%user_tutors_info}}.user_id'
                                )->where(['{{%tutors_course}}.course_id' => $modelCourse['id']]);
        $modelTutors = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 10]]);
        
        $userArray = ArrayHelper::map(User::find()->all(), 'id', 'name');
        $modelClassFilesImage = Files::find()->where(['class_id' => $modelClass['id'], 'mime_type' => 'image'])->asArray()->limit(4)->all();
        
        return $this->render('/class/classtutors',[
            'modelCourses' => $modelCourses,
            'newTutors' => $newTutors,
            'modelClassFilesImage' => $modelClassFilesImage,
            'modelUniversity' => $modelUniversity,
            'modelProfessor' => $modelProfessor,
            'followStatus' => $followStatus,
            'modelCourse' => $modelCourse,
            'modelClass' => $modelClass,
            'modelMajor' => $modelMajor,
            'userArray' => $userArray,
            'modelTutors' => $modelTutors->getModels(),
            'pagination' => $modelTutors->pagination,
            'count' => $modelTutors->pagination->totalCount,
            'classEnded' => $modelFollowerStatus,
            'modelNewRating' => $modelNewRating,
        ]);
    }
                
    public function actionClassproducts($class_name)
    {
        $modelClass = Classes::find()->where(['url_name' => $class_name])->one();
        $modelClass->scenario = 'class_update';
        $modelCourse = Course::find()->where(['id' => $modelClass->id_course])->one();
        $modelMajor = Major::find()->where(['id' => $modelCourse->id_major])->one();
        $modelUniversity = University::find()->where(['id' => $modelMajor->university_id])->one();
        $modelNewProduct = new Product();
        $modelNewProduct->scenario = 'add_product';
        
        
        if($_POST){
            if(isset($_POST['ProductFile'])){
                        $request = Yii::$app->request;
                        $modelNewFile = new Files();
                        $modelNewFile->scenario = 'add_file';
                        $modelNewFile->class_id = $modelClass->id;
                        $modelNewFile->user_id = \Yii::$app->user->id;
                        $modelNewFile->file_src = $_POST['ProductFile'];
                        $modelNewFile->file_type = 'files';
                        if($modelNewFile->save()){
                            
                            
                            $userArray = [];
                            $modelFollower = Follower::find()->where(['id_object' => $modelClass['id'], 'type_object' => 'class'])->asArray()->all();
                            foreach($modelFollower as $follower){
                                $userArray[] = $follower['id_user'];                
                            }
                            
                            if($modelClass != null){
                                $university_id = $modelClass['id_university']; 
                                        
                                $newModelNotification = new Notification();
                                $newModelNotification->scenario = 'add_notification';
                                $newModelNotification->module = 'course_and_class';
                                $newModelNotification->from_object_id = \Yii::$app->user->id;
                                $newModelNotification->to_object_id = $modelNewFile->id;
                                $newModelNotification->university_id = $university_id;
                                $newModelNotification->notification_type = 'add_file_to_class';
                                if($newModelNotification->save()){
                                    foreach($userArray as $user_id){
                                        $modelNotificationUser = new Notificationusers();
                                        $modelNotificationUser->scenario = 'new_notification';
                                        $modelNotificationUser->user_id = $user_id;
                                        $modelNotificationUser->notification_id = $newModelNotification->id;
                                        $modelNotificationUser->save();
                                    }                
                                }
                            }
                            
                            if($modelNewProduct->load($request->post())){
                                $modelNewProduct->file_id = $modelNewFile->id;
                                if($modelNewProduct->save()){
                                    Yii::$app->session->setFlash('product_added');
                                }else{
                                    Yii::$app->session->setFlash('product_notadded');  
                                }
                            }
                        }
                    
            }else{
                if($modelNewProduct->load($request->post())){
                    $modelNewProduct->file_id = $modelNewFile->id;
                    if($modelNewProduct->save()){
                        Yii::$app->session->setFlash('product_added');
                    }else{
                        Yii::$app->session->setFlash('product_notadded');  
                    }
                }
            }
        }
        
        $query = new Query();
        $query->select(['*', 'id' => '{{%user}}.id'])
                        ->from('{{%user}}')
                        ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%user}}.id'
                                )->where(['{{%user}}.id' => $modelClass->professor]);

        $command = $query->createCommand();
        $modelProfessor = $command->queryOne();
        
        $modelFollower = Follower::find()->where(['id_user' => \Yii::$app->user->id,'id_object' => $modelClass->id,'type_object' => 'class' ])->one();
        $modelFollowerStatus = '';
        if($modelFollower == null){
            $followStatus = 'Follow';
            $modelFollowerStatus = '';
        }else{
            $followStatus = 'Unfollow';
            $modelFollowerStatus = $modelFollower->status;
        }
        
        $queryProduct = new Query();
        $queryProduct->select(['*', 'id' => '{{%product}}.id', 'productDatecreate' => '{{%product}}.date_create'])
                        ->from('{{%product}}')
                        ->join('LEFT JOIN',
                                        '{{%user}}',
                                        '{{%user}}.id = {{%product}}.user_id'
                                )->where(['{{%product}}.class_id' => $modelClass->id])->orderBy('{{%product}}.date_create DESC');

        $modelProduct = new ActiveDataProvider(['query' => $queryProduct, 'pagination' => ['pageSize' => 10]]);
        
        $myFilesArray = ArrayHelper::map(Files::find()->where(['user_id' => \Yii::$app->user->id,'file_type' => 'files','class_id' => $modelClass->id])->all(), 'id', 'file_src');
        
        $userArray = ArrayHelper::map(User::find()->all(), 'id', 'name');
        $modelClassFilesImage = Files::find()->where(['class_id' => $modelClass['id'], 'mime_type' => 'image'])->asArray()->limit(4)->all();
        return $this->render('/class/classproducts',[ 
            'modelClassFilesImage' => $modelClassFilesImage,
            'userArray' => $userArray,
            'modelProduct' => $modelProduct->getModels(),
            'pagination' => $modelProduct->pagination,
            'count' => $modelProduct->pagination->totalCount,
            'modelClass' => $modelClass,
            'modelUniversity' => $modelUniversity,
            'myFilesArray' => $myFilesArray,
            'modelNewProduct' => $modelNewProduct,
            'modelCourse' => $modelCourse,
            'modelMajor' => $modelMajor,
            'modelProfessor' => $modelProfessor,
            'followStatus' => $followStatus,
            'classEnded' => $modelFollowerStatus
        ]);
    }

    ///////listat_an

    public function actionPay_product_credits(){

        $product = Product::findOne($_GET['product_id']);
        if($product) {
            $user_credits = Usercredits::find()->where(['user_id' => $product->user_id])->one();
            $user_credits->scenario = 'update_credits_count';
            $my_credits = Usercredits::find()->where(['user_id' => Yii::$app->user->id])->one();
            $my_credits->scenario = 'update_credits_count';
            if(isset($_GET['price']))
                if ($_GET['price'] <= $my_credits->credit_count) {
                    $my_credits->credit_count = $my_credits->credit_count - $_GET['price'];
                    $user_credits->credit_count = $user_credits->credit_count - $_GET['price'];
                    $my_credits->save();
                    $user_credits->save();

                    // TODO "дії з продуктом коли оплата пройшла успішно можливо ще нотифікації"

                    $product_pay = new Productpay();
                    $product_pay->scenario = 'add_productpay';
                    $product_pay->product_id = $_GET['product_id'];
                    $product_pay->user_id = Yii::$app->user->id;
                    $product_pay->status = 1;
                    $product_pay->save();

                    Yii::$app->session['pay_ok']=true;
                } else {
                    Yii::$app->session['not_credits']=true;
                }
        }
        $this->redirect('/'.$_GET['redirect']);
    }


    public function actionPay_product(){
        if (!Yii::$app->user->isGuest) {


            if (isset($_GET['price']) && isset($_GET['product_id'])){
                $dPrice = $_GET['price'];
                $product = Product::findOne($_GET['product_id']);
                if($product){
                    $user_paymend = $product->user_id;
                    $product_id = $product->id;
                } else {
                    throw new \yii\web\HttpException(403);
                }
            } else {
                throw new \yii\web\HttpException(403);
            }

            $pay = New Paypal();
            $pay = $pay->api;

            $pay->setConfig([
                'model'=>'sandbox',
                'http.ConnectionTimeOut'=>30,
                'log.LogEnabled'=> YII_DEBUG ? 0 : 0,
                'log.FileName'=> '',
                'log.LogLevel' => 'FINE',
                'validation.level'=>'log'
            ]);

            $payer = new Payer();
            $payer->setPaymentMethod("paypal");

            $item2 = new Item();
            $item2->setName('Payment product')
                ->setCurrency('USD')
                ->setQuantity(1)
                ->setSku('N-C-'+time()) // Similar to `item_number` in Classic API
                ->setPrice($dPrice);
            $itemList = new ItemList();
            $itemList->setItems(array($item2));


            $details = new Details();
            $details->setShipping(0.00)
                ->setTax(0.00)
                ->setSubtotal($dPrice);


            $amount = new Amount();
            $amount->setCurrency("USD")
                ->setTotal($dPrice+0.00)
                ->setDetails($details);



            $transaction = new Transaction();
            $transaction->setAmount($amount)
                ->setItemList($itemList)
                ->setDescription('Paymend product')
                ->setInvoiceNumber(uniqid());

            $redirectUrls = new RedirectUrls();
            $redirectUrls->setReturnUrl(Url::home(true).'product/pay?approved=true&user_paymend='.$user_paymend.'&product_id='.$product_id.'&redirect='.$_GET['redirect'])
                ->setCancelUrl(Url::home(true).'product/pay?approved=false&user_paymend='.$user_paymend.'&product_id='.$product_id.'&redirect='.$_GET['redirect']);


            $payment = new Payment();
            $payment->setIntent("sale")
                ->setPayer($payer)
                ->setRedirectUrls($redirectUrls)
                ->setTransactions(array($transaction));

            try{
                $payment->create($pay);
            }catch (PayPalConnectionException $e){

                throw new UserException($e->getMessage());

            }
            $trans= new \app\models\Transaction();
            $trans->user_id= Yii::$app->user->id;
            $trans->price=$dPrice;
            $trans->payment_id=$payment->getId();
            $trans->hash=md5($payment->getId());
            $trans->othe='Paymend product';
            $trans->project_id=$product_id;
            $trans->save();
            Yii::$app->getResponse()->redirect($payment->getApprovalLink());
        } else {
            throw new \HttpException(403);
        }
    }

    public function actionPay(){

        $pay = New Paypal();
        $pay = $pay->api;

        if (isset($_GET['approved']) && $_GET['approved'] == 'true') {
            $paymentId = $_GET['paymentId'];
            $payment = Payment::get($paymentId, $pay);
            $execution = new PaymentExecution();
            $execution->setPayerId($_GET['PayerID']);
            try {
                $result = $payment->execute($execution, $pay);
            } catch (PayPalConnectionException $e) {
                throw new UserException($e->getMessage());
            }
            $transaction = \app\models\Transaction::find()->where(['user_id'=>Yii::$app->user->id,'hash'=>md5($paymentId)])->one();
            $transaction->complete=1;
            $transaction->save();



            // TODO "дії з продуктом коли оплата пройшла успішно можливо ще нотифікації"
            $product_pay = new Productpay();
            $product_pay->scenario = 'add_productpay';
            $product_pay->product_id = $_GET['product_id'];
            $product_pay->user_id = Yii::$app->user->id;
            $product_pay->status = 1;
            $product_pay->save();

            $user_many = Usercredits::find()->where(['user_id'=>$_GET['user_paymend']])->one();
            if ($user_many) {
                $user_many->scenario = 'update_many_count';
                $user_many->many = $user_many->many + $transaction->price;
                $user_many->save();
            } else {
                $u_m = new Usercredits();
                $u_m->many=$transaction->price;
                $u_m->user_id = $_GET['user_paymend'];
                $u_m->credit_count = 0;
                $u_m->scenario = 'add_credits';
                $u_m->save();
            }



            Yii::$app->session['pay_ok']=true;

            return $this->redirect('/'.$_GET['redirect']);
        } else {
            Yii::$app->session['pay_cansel']=true;
            return $this->redirect('/'.$_GET['redirect']);
        }
    }

    ////////////////////////////////

                
    public function actionClassfiles($class_name)
    {
        $modelNewFiles = new Files();
        
        $modelClass = Classes::find()->where(['url_name' => $class_name])->one();
        $modelClass->scenario = 'class_update';
        $modelCourse = Course::find()->where(['id' => $modelClass->id_course])->one();
        $modelMajor = Major::find()->where(['id' => $modelCourse->id_major])->one();
        $modelUniversity = University::find()->where(['id' => $modelMajor->university_id])->one();
        $query = new Query();
        $query->select(['*', 'id' => '{{%user}}.id'])
                        ->from('{{%user}}')
                        ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%user}}.id'
                                )->where(['{{%user}}.id' => $modelClass->professor]);

        $command = $query->createCommand();
        $modelProfessor = $command->queryOne();
        
        $modelNewFiles->scenario = 'add_file';
        if($_POST){
            $request = Yii::$app->request;
            if($modelNewFiles->load($request->post())){
                if($modelNewFiles->save()){
                    Yii::$app->session->setFlash('files_added');
                }else{
                    Yii::$app->session->setFlash('files_not_added');  
                }
            }
        }
        
        $modelFollower = Follower::find()->where(['id_user' => \Yii::$app->user->id,'id_object' => $modelClass->id,'type_object' => 'class' ])->one();
        $modelFollowerStatus = '';
        if($modelFollower == null){
            $followStatus = 'Follow';
            $modelFollowerStatus = '';
        }else{
            $followStatus = 'Unfollow';
            $modelFollowerStatus = $modelFollower->status;
        }
        
        $queryFiles = new Query();
        $queryFiles->select(['*', 'id' => '{{%files}}.id', 'fileCreate' => '{{%files}}.date_create'])
                        ->from('{{%files}}')
                        ->join('LEFT JOIN',
                                    '{{%user}}',
                                    '{{%user}}.id = {{%files}}.user_id'
                                )->where(['{{%files}}.file_type' => 'files','{{%files}}.class_id' => $modelClass->id])->orderBy('{{%files}}.date_create DESC');
        
        //$queryFiles = Files::find()->where(['file_type' => 'files','class_id' => $modelClass->id]);
        $modelFiles = new ActiveDataProvider(['query' => $queryFiles, 'pagination' => ['pageSize' => 10]]);
        
        $userArray = ArrayHelper::map(User::find()->all(), 'id', 'name');
        $modelClassFilesImage = Files::find()->where(['class_id' => $modelClass['id'], 'mime_type' => 'image'])->asArray()->limit(4)->all();
        
        return $this->render('/class/classfiles',[ 
            'modelClassFilesImage' => $modelClassFilesImage,
            'count' => $modelFiles->pagination->totalCount,
            'modelFiles' => $modelFiles->getModels(),
            'pagination' => $modelFiles->pagination,
            'modelUniversity' => $modelUniversity,
            'modelProfessor' => $modelProfessor,
            'modelNewFiles' => $modelNewFiles,
            'followStatus' => $followStatus,
            'modelCourse' => $modelCourse,
            'modelMajor' => $modelMajor,
            'modelClass' => $modelClass,
            'userArray' => $userArray,
            'classEnded' => $modelFollowerStatus
        ]);
    }

    public function actionSavedropedfile($class_id){
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/files/';   //2
        // mkdir($storeFolder);
        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.$class_id.$ds.Yii::$app->user->id)){
                mkdir($storeFolder.$ds.$class_id.$ds.Yii::$app->user->id, 0777, true);
            }
            $tempFile = $_FILES['file']['tmp_name'];          //3
            $fileType = $_FILES['file']['type'];    //3
            $arrayFormat = [
                'video/x-flv' => 'flv',
                'video/mp4' => 'mp4',
                'video/MP2T' => 'ts',
                'application/x-mpegURL' => 'm3u8',
                'video/3gpp' => '3gp',
                'video/quicktime' => 'mov',
                'video/x-msvideo' => 'avi',
                'video/x-ms-wmv' => 'wmv',
                'application/msword' => 'doc',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'docx',
                'application/vnd.openxmlformats-officedocument.wordprocessingml.template' => 'dotx',
                'application/vnd.ms-word.document.macroEnabled.12' => 'docm',
                'application/vnd.ms-word.template.macroEnabled.12' => 'dotm',
                'application/vnd.ms-excel' => 'xls',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 'xlsx',
                'application/vnd.openxmlformats-officedocument.spreadsheetml.template' => 'xltx',
                'application/vnd.ms-excel.sheet.macroEnabled.12' => 'xlsm',
                'application/vnd.ms-excel.template.macroEnabled.12' => 'xltm',
                'application/vnd.ms-excel.addin.macroEnabled.12' => 'xlam',
                'application/vnd.ms-excel.sheet.binary.macroEnabled.12' => 'xlsb',
                'application/vnd.ms-powerpoint' => 'ppt',
                'application/vnd.openxmlformats-officedocument.presentationml.presentation' => 'pptx',
                'application/vnd.openxmlformats-officedocument.presentationml.template' => 'potx',
                'application/vnd.openxmlformats-officedocument.presentationml.slideshow' => 'ppsx',
                'application/vnd.ms-powerpoint.addin.macroEnabled.12' => 'ppam',
                'application/vnd.ms-powerpoint.presentation.macroEnabled.12' => 'pptm',
                'application/vnd.ms-powerpoint.template.macroEnabled.12' => 'potm',
                'application/vnd.ms-powerpoint.slideshow.macroEnabled.12' => 'ppsm',
                'application/x-7z-compressed' => '7z',
                'application/x-gtar' => 'tar.gz',
                'application/zip' => 'zip',
                'application/x-rar-compressed' => 'rar',
                'image/bmp' => 'bmp',
                'image/gif' => 'gif',
                'image/jpeg' => 'jpeg',
                'image/png' => 'png',

            ];
            $keyStatus = array_key_exists($fileType, $arrayFormat);
            if($keyStatus == true){
                $fileType = $arrayFormat[$fileType];
            }else{
                $fileType = 'rar';
            }

            $targetPath =  $storeFolder . $ds.$class_id.$ds.Yii::$app->user->id.$ds;  //4
            $for_name = time();
            $targetFile =  $targetPath.$for_name.'.'.$fileType;  //5

            move_uploaded_file($tempFile,$targetFile); //6
            return \Yii::$app->user->id.'/'.$for_name.'.'.$fileType;
        }
        return false;
    }

    public function actionDeletefilechat($class_id){
        $storeFolder = \Yii::getAlias('@webroot').'/files/'.$class_id.'/';
        $path = $storeFolder.$_POST['file_name'];
        if(unlink($path)){
            echo 'delete';
        }else{
            echo 'problem';
        }
    }
    
    public function actionClasschat($class_name)
    {
        $modelClass = Classes::find()->where(['url_name' => $class_name])->one();
        $modelClass->scenario = 'class_update';
        $modelCourse = Course::find()->where(['id' => $modelClass->id_course])->one();
        $modelMajor = Major::find()->where(['id' => $modelCourse->id_major])->one();
        $modelUniversity = University::find()->where(['id' => $modelMajor->university_id])->one();
        $query = new Query();
        $query->select(['*', 'id' => '{{%user}}.id'])
                        ->from('{{%user}}')
                        ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%user}}.id'
                                )->where(['{{%user}}.id' => $modelClass->professor]);

        $command = $query->createCommand();
        $modelProfessor = $command->queryOne();
        
        $modelFollower = Follower::find()->where(['id_user' => \Yii::$app->user->id,'id_object' => $modelClass->id,'type_object' => 'class' ])->one();
        $modelFollowerStatus = '';
        if($modelFollower == null){
            $followStatus = 'Follow';
            $modelFollowerStatus = '';
        }else{
            $followStatus = 'Unfollow';
            $modelFollowerStatus = $modelFollower->status;
        }
        $userArray = ArrayHelper::map(User::find()->all(), 'id', 'name');
        
        return $this->render('/class/classchat',[
            'userArray' => $userArray,
            'modelClass' => $modelClass,
            'modelCourse' => $modelCourse,
            'modelUniversity' => $modelUniversity,
            'modelMajor' => $modelMajor,
            'modelProfessor' => $modelProfessor,
            'followStatus' => $followStatus,
            'classEnded' => $modelFollowerStatus
        ]);
    }


    public function actionAjaxfollowclass()
    {
        $result = [];
        $id_object = $_POST['id_object'];
        $followStatus = $_POST['followStatus'];
        
        $modelClass = Classes::find()->where(['id' => $id_object])->asArray()->one();
        $userArray = [];
        $friendId = [];
        if($modelClass != null){
            $modelFollower = Follower::find()->where(['id_object' => $id_object, 'type_object' => 'class'])->asArray()->all();
            foreach($modelFollower as $follower){
                $userArray[] = $follower['id_user'];                
            }
            
            $modelFriend = UserFriend::find()->where(['id_user' => \Yii::$app->user->id])->asArray()->all();
            foreach($modelFriend as $friend){
                $friendId[] = $friend['id_friend'];
            }
        }
        
        $university_id = $modelClass['id_university'];
        if($followStatus == 'Follow'){
            $newModelFollow = new Follower();
            $newModelFollow->id_user = \Yii::$app->user->id;
            $newModelFollow->id_object = $id_object;
            $newModelFollow->type_object = 'class';
            if($newModelFollow->save()){
                $result['status'] = 'success';
                $result['statusFoloow'] = 'Unfollow';
                
                
                
                //add credits action begin
                            $modelCreditsaction = Creditsaction::find()->where(['object_id' => \Yii::$app->user->id, 'action' => 'join_class'])->one();
                            if($modelCreditsaction){
                                $modelCreditsaction->scenario = 'change_action_count';
                                $modelCreditsaction->action_count = $modelCreditsaction->action_count + 1;
                                if($modelCreditsaction->save()){
                                }
                                
                                
                                if((($modelCreditsaction->action_count % 10) == 0)){
                                    $modelCreditsactionCount = Creditsactioncount::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                    if($modelCreditsactionCount){
                                        $modelCreditsactionCount->scenario = 'update_credits_count';
                                        $modelCreditsactionCount->credits_count = $modelCreditsactionCount->credits_count + 1;
                                        $modelCreditsactionCount->date_update = date("Y-m-d H:i:s");
                                        if($modelCreditsactionCount->save()){
                                            
                                        }
                                    }else{
                                        $newModelCreditsactionCount = new Creditsactioncount();
                                        $newModelCreditsactionCount->scenario = 'add_credits_count';
                                        $newModelCreditsactionCount->user_id = \Yii::$app->user->id;
                                        $newModelCreditsactionCount->credits_count = 1;
                                        if($newModelCreditsactionCount->save()){

                                        }
                                    }
                                    
                                    $modelCredits = \app\models\Usercredits::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                    if($modelCredits){
                                        $modelCredits->scenario = 'update_credits_count';
                                        $modelCredits->credit_count = $modelCredits->credit_count + 1;
                                        if($modelCredits->save()){
                                        }                            
                                    }else{
                                        $modelNewCredits = new \app\models\Usercredits();
                                        $modelNewCredits->scenario = 'add_credits';
                                        $modelNewCredits->user_id = \Yii::$app->user->id;
                                        $modelNewCredits->credit_count = 1;
                                        $modelNewCredits->many = 0;
                                        if($modelNewCredits->save()){

                                        }
                                    }
                                    
                                        $modelNotification = new Notification();
                                        $modelNotification->scenario = 'add_notification';
                                        $modelNotification->module = 'credits';
                                        $modelNotification->from_object_id = \Yii::$app->user->id;
                                        $modelNotification->to_object_id = \Yii::$app->user->id;
                                        $modelNotification->notification_type = 'post_credits';
                                        $modelNotification->university_id = $modelClass->id_university;
                                        if($modelNotification->save()){
                                            $modelNotificationUser = new Notificationusers();
                                            $modelNotificationUser->scenario = 'new_notification';
                                            $modelNotificationUser->user_id = \Yii::$app->user->id;
                                            $modelNotificationUser->notification_id = $modelNotification->id;
                                            $modelNotificationUser->save();
                                        }
                                }
                                
                                
                                
                            }else{
                                        $newModelCreditsaction = new Creditsaction();
                                        $newModelCreditsaction->scenario = 'add_credits';
                                        $newModelCreditsaction->object_id = \Yii::$app->user->id;
                                        $newModelCreditsaction->action = 'join_class';
                                        $newModelCreditsaction->action_count = 1;
                                        if($newModelCreditsaction->save()){
                                            $modelCreditsactionCount = Creditsactioncount::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                            if($modelCreditsactionCount){
                                                $modelCreditsactionCount->scenario = 'update_credits_count';
                                                $modelCreditsactionCount->credits_count = $newModelCreditsactionCount->credits_count + 3;
                                                $modelCreditsactionCount->date_update = date("Y-m-d H:i:s");
                                                if($modelCreditsactionCount->save()){
                                                }
                                            }else{
                                                $newModelCreditsactionCount = new Creditsactioncount();
                                                $newModelCreditsactionCount->scenario = 'add_credits_count';
                                                $newModelCreditsactionCount->user_id = \Yii::$app->user->id;
                                                $newModelCreditsactionCount->credits_count = 3;
                                                if($newModelCreditsactionCount->save()){

                                                }
                                            }
                                        }
                                        $modelCredits = \app\models\Usercredits::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                        if($modelCredits){
                                            $modelCredits->scenario = 'update_credits_count';
                                            $modelCredits->credit_count = $modelCredits->credit_count + 3;
                                            if($modelCredits->save()){
                                            }                            
                                        }else{
                                            $modelNewCredits = new \app\models\Usercredits();
                                            $modelNewCredits->scenario = 'add_credits';
                                            $modelNewCredits->user_id = \Yii::$app->user->id;
                                            $modelNewCredits->credit_count = 3;
                                            $modelNewCredits->many = 0;
                                            if($modelNewCredits->save()){

                                            }
                                        }

                                        $modelNotification = new Notification();
                                        $modelNotification->scenario = 'add_notification';
                                        $modelNotification->module = 'university';
                                        $modelNotification->from_object_id = \Yii::$app->user->id;
                                        $modelNotification->to_object_id = \Yii::$app->user->id;
                                        $modelNotification->notification_type = 'first_class_join_credits';
                                        $modelNotification->university_id = $modelClass->id_university;
                                        if($modelNotification->save()){
                                            $modelNotificationUser = new Notificationusers();
                                            $modelNotificationUser->scenario = 'new_notification';
                                            $modelNotificationUser->user_id = \Yii::$app->user->id;
                                            $modelNotificationUser->notification_id = $modelNotification->id;
                                            $modelNotificationUser->save();
                                        }

                            }
                    //add credits action end
                
                
                
                
            }else{
                $result['status'] = 'error';
            }
            $newModelNotification = new Notification();
            $newModelNotification->scenario = 'add_notification';
            $newModelNotification->module = 'course_and_class';
            $newModelNotification->from_object_id = \Yii::$app->user->id;
            $newModelNotification->to_object_id = $id_object;
            $newModelNotification->university_id = $university_id;
            $newModelNotification->notification_type = 'user_add_to_class';
            if($newModelNotification->save()){
                foreach($userArray as $user_id){
                    $modelNotificationUser = new Notificationusers();
                    $modelNotificationUser->scenario = 'new_notification';
                    $modelNotificationUser->user_id = $user_id;
                    $modelNotificationUser->notification_id = $newModelNotification->id;
                    $modelNotificationUser->save();
                }                
            }
            
            
            $newModelNotificationF = new Notification();
            $newModelNotificationF->scenario = 'add_notification';
            $newModelNotificationF->module = 'course_and_class';
            $newModelNotificationF->from_object_id = \Yii::$app->user->id;
            $newModelNotificationF->to_object_id = $id_object;
            $newModelNotificationF->university_id = $university_id;
            $newModelNotificationF->notification_type = 'friend_add_to_class';
            if($newModelNotificationF->save()){
                foreach($friendId as $friend_id){
                    $modelNotificationUserF = new Notificationusers();
                    $modelNotificationUserF->scenario = 'new_notification';
                    $modelNotificationUserF->user_id = $friend_id;
                    $modelNotificationUserF->notification_id = $newModelNotificationF->id;
                    $modelNotificationUserF->save();
                }                
            }
            
        }else{
            $modelFollower = Follower::find()->where(['id_user' => \Yii::$app->user->id,'id_object' => $id_object,'type_object' => 'class'])->one();
            if($modelFollower->delete()){
                $result['status'] = 'success';
                $result['statusFoloow'] = 'Join';
            }else{
                $result['status'] = 'error';
            }
        }
        
        echo json_encode($result);   
    }
    
    public function actionUpdateclass($class_id){
        $modelClass = Classes::find()->where(['id' => $class_id])->one();
        $modelClass->scenario = 'class_update';
        if($_POST){
            $request = Yii::$app->request;
            if($modelClass->load($request->post())){
                if($modelClass->save()){
                    
                    
                    
                    
                    $modelClass = Classes::find()->where(['id' => $_POST['class_id']])->asArray()->one();
            
                    $userArray = [];
                    $modelFollower = Follower::find()->where(['id_object' => $modelClass->id, 'type_object' => 'class'])->asArray()->all();
                    foreach($modelFollower as $follower){
                        $userArray[] = $follower['id_user'];                
                    }

                    if($modelClass != null){
                        $university_id = $modelClass['id_university']; 

                        $newModelNotification = new Notification();
                        $newModelNotification->scenario = 'add_notification';
                        $newModelNotification->module = 'course_and_class';
                        $newModelNotification->from_object_id = \Yii::$app->user->id;
                        $newModelNotification->to_object_id = $modelClass->id;
                        $newModelNotification->university_id = $university_id;
                        $newModelNotification->notification_type = 'update_class';
                        if($newModelNotification->save()){
                            foreach($userArray as $user_id){
                                $modelNotificationUser = new Notificationusers();
                                $modelNotificationUser->scenario = 'new_notification';
                                $modelNotificationUser->user_id = $user_id;
                                $modelNotificationUser->notification_id = $newModelNotification->id;
                                $modelNotificationUser->save();
                            }                
                        }
                    }
                    
                    
                    
                    
                    Yii::$app->session->setFlash('class_update');
                    return $this->redirect(Url::home().'class/'.$modelClass->url_name);
                }else{
                    Yii::$app->session->setFlash('class_notupdate');  
                    return $this->redirect(Url::home().'class/'.$modelClass->url_name);
                }
            }
        }
    }
    
    public function actionChangefollowclassstatus(){
        $modelFollowClass = Follower::find()->where(['id_user' => \Yii::$app->user->id,'id_object' => $_POST['class_id'],'type_object' => 'class' ])->one();
        if($_POST['status'] == 'confirm'){
            $modelFollowClass->status = 0;
        }else{
            $modelFollowClass->status = 1;
        }
        $result= [];
        if($modelFollowClass->save()){
            $result['status'] = 'success';
        }else{
            $result['status'] = 'error';
        }
        echo json_encode($result);
    }
}