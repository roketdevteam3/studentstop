<?php

namespace app\modules\course\controllers;

use app\commands\Paypal;
use app\models\PaypalSetting;
use app\models\Usercredits;
use yii\base\UserException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use app\models\user\User;
use app\models\user\UserInfo;
use app\models\user\UserFriend;
use app\models\user\UserFriendRequest;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use Yii;
use app\models\Major;
use app\models\Course;
use app\models\Classreting;
use app\modules\university\models\University;
use app\models\Notification;
use app\models\Notificationusers;

use app\models\Creditsactioncount;
use app\models\Creditsaction;

use app\models\Productpay;
use app\models\Videoconferenceuser;
use app\models\Virtualclassreting;
use app\models\Ratingrequest;
use app\models\Tutors;
use app\models\Favorite;
use app\models\Videoconference;
use app\models\Tutorsinfo;
use app\models\Classes;
use app\models\Product;
use app\models\Follower;
use app\models\user\Event;
use app\models\user\UserEdit;
use yii\db\Query;
use app\modules\users\models\UserImages;
use app\modules\users\models\UserNews;
use app\modules\users\models\UserEvents;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Exception\PayPalConnectionException;
/**
 * Default controller for the `course` module
 */
error_reporting(E_ERROR);

class InfoController extends Controller
{
    
    public function beforeAction($action) {
        if(!\Yii::$app->user->isGuest){
            if(Yii::$app->user->identity->getUsertUniversity()){
                $this->enableCsrfValidation = false;
                return parent::beforeAction($action);
            }else{
                return $this->redirect(Url::to("/users/choiceuniversity",true));
            }
        }else{
            return $this->redirect(Url::to("/users/login",true));
        }
    }
    
    public function actionIndex()
    {
        $query = new Query();
        $query->select(['*', 'id' => '{{%follower}}.id','class_name' => '{{%class}}.name'])
                        ->from('{{%follower}}')
                        ->join('LEFT JOIN',
                                        '{{%class}}',
                                        '{{%class}}.id = {{%follower}}.id_object'
                                )->where(['{{%follower}}.id_user' => \Yii::$app->user->id, '{{%follower}}.type_object' => 'class']);

//        $command = $query->createCommand();
//        $modelProfessor = $command->queryOne();
        $modelClass = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 10]]);
        return $this->render('index',[
            'modelClass' => $modelClass->getModels(),
            'pagination' => $modelClass->pagination,
            'count' => $modelClass->pagination->totalCount,
        ]);
    }
    
    public function actionMajorlist()
    {
        if(!\Yii::$app->user->isGuest){
            $modelMajor = \app\modules\university\models\Major::find()->where(['university_id' => Yii::$app->user->identity->getUsertUniversity()])->asArray()->all();

            $majorArray = [];
            foreach($modelMajor as $major){
                $majorArray[$major['id']]['info'] = $major;
                $majorArray[$major['id']]['courses'] = \app\modules\university\models\Course::find()->where(['id_major' => $major['id']])->asArray()->all();
            }
            return $this->render('majorlist',[
                'majorArray' => $majorArray
            ]);
        }else{
            return $this->redirect(Url::to("/users/login",true));
        }
    }
    
    
    public function actionVideoclasslist($course_id=null)
    {
        $modelNewVideoconference = new Videoconference();
        $modelNewVideoconference->scenario = 'add_conference';
        
        if($_POST){
            $request = Yii::$app->request;
            if($modelNewVideoconference->load($request->post())){
                if($modelNewVideoconference->save()){
                    foreach($_POST['Videoconference']['users_id'] as $user_id){
                        $modelNewVideoconferenceuser = new Videoconferenceuser();
                        $modelNewVideoconferenceuser->scenario = 'add_conference';
                        $modelNewVideoconferenceuser->conference_id = $modelNewVideoconference->id;
                        $modelNewVideoconferenceuser->status = 0;
                        $modelNewVideoconferenceuser->user_id = $user_id;
                        $modelNewVideoconferenceuser->save();
                    }
                }
            }
        }
        
        $arrayUser = [];
        $modelUser = User::find()->all();
        foreach($modelUser as $user){
            $arrayUser[$user->id] = $user->name.' '.$user->surname;
        }
        
        $queryVideoconference = new Query();
        $queryVideoconference->select(['*', 'id' => '{{%videoconference}}.id', 'class_status' => '{{%videoconference}}.status',  'className' => '{{%videoconference}}.name','userName' => '{{%user}}.name', 'conferenceDateCreate' => '{{%videoconference}}.date_create'])
                        ->from('{{%videoconference}}')
                        ->join('LEFT JOIN',
                                        '{{%user}}',
                                        '{{%user}}.id = {{%videoconference}}.tutors_id')
                        ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%videoconference}}.tutors_id')
                        ->where(['{{%videoconference}}.id_course' => $course_id]);
        
        $queryVideoconference->limit(20);
        $queryVideoconference->orderBy([
            '{{%videoconference}}.date_start' => SORT_DESC,
        ]);
        
        $command = $queryVideoconference->createCommand();
        $modelVideoconference = $command->queryAll();
 
        $videoconferences = [];
        foreach($modelVideoconference as $modelConference){
            $date = date_create($modelConference['date_start']);
            $videoconferences[date_format($date, 'd.m.Y')][] = $modelConference;
        }
        
        return $this->render('videoclasslist',[
            'modelNewVideoconference' => $modelNewVideoconference,
            'arrayUser' => $arrayUser,
            'videoconferences' => $videoconferences
        ]);
    }
    
    public function actionVideolist()
    {
        $modelNewVideoconference = new Videoconference();
        $modelNewVideoconference->scenario = 'add_conference';
        
        if($_POST){
            $request = Yii::$app->request;
            if($modelNewVideoconference->load($request->post())){
                if($modelNewVideoconference->save()){
                    foreach($_POST['Videoconference']['users_id'] as $user_id){
                        $modelNewVideoconferenceuser = new Videoconferenceuser();
                        $modelNewVideoconferenceuser->scenario = 'add_conference';
                        $modelNewVideoconferenceuser->conference_id = $modelNewVideoconference->id;
                        $modelNewVideoconferenceuser->status = 0;
                        $modelNewVideoconferenceuser->user_id = $user_id;
                        $modelNewVideoconferenceuser->save();
                    }
                }
            }
        }
        
        $arrayUser = [];
        $modelUser = User::find()->all();
        foreach($modelUser as $user){
            $arrayUser[$user->id] = $user->name.' '.$user->surname;
        }
        
        //$queryVideoconference = Videoconference::find()->where(['tutors_id' => \Yii::$app->user->id]);
        
        $queryVideoconference = new Query();
        $queryVideoconference->select(['*', 'id' => '{{%videoconference}}.id', 'class_status' => '{{%videoconference}}.status',  'className' => '{{%videoconference}}.name','userName' => '{{%user}}.name', 'conferenceDateCreate' => '{{%videoconference}}.date_create'])
                        ->from('{{%videoconference_users}}')
                        ->join('LEFT JOIN',
                                        '{{%videoconference}}',
                                        '{{%videoconference}}.id = {{%videoconference_users}}.conference_id')
                        ->join('LEFT JOIN',
                                        '{{%user}}',
                                        '{{%user}}.id = {{%videoconference}}.tutors_id')
                        ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%videoconference}}.tutors_id')
                        ->where(['{{%videoconference_users}}.user_id' => \Yii::$app->user->id, '{{%videoconference_users}}.status' => 1]);
        
        $queryVideoconference->limit(20);
        $queryVideoconference->orderBy([
            '{{%videoconference}}.date_start' => SORT_DESC,
        ]);
        
        $command = $queryVideoconference->createCommand();
        $modelVideoconference = $command->queryAll();
 
        $videoconferences = [];
        foreach($modelVideoconference as $modelConference){
            $date = date_create($modelConference['date_start']);
            $videoconferences[date_format($date, 'd.m.Y')][] = $modelConference;
        }
        
        return $this->render('dashboard',[
            'modelNewVideoconference' => $modelNewVideoconference,
            'arrayUser' => $arrayUser,
            'videoconferences' => $videoconferences,
        ]);
    }
    
    public function actionGetvitrualclassusers()
    {
        $videoconference_id = $_POST['videoconference_id'];
        $modelVideoconference = Videoconference::find()->where(['id' => $videoconference_id])->one();
        $query = new Query();
        $query->select(['*', 'id' => '{{%videoconference_users}}.id', 'status' => '{{%videoconference_users}}.id'])
                        ->from('{{%videoconference_users}}')
                        ->join('LEFT JOIN',
                                '{{%user_info}}',
                                '{{%user_info}}.id_user = {{%videoconference_users}}.user_id'
                            )
                        ->join('LEFT JOIN',
                                        '{{%user}}',
                                        '{{%user}}.id = {{%videoconference_users}}.user_id')
                        ->join('LEFT JOIN',
                                    '{{%user_role}}',
                                    '{{%user_role}}.id = {{%user_info}}.academic_status'
                                )->where(['{{%videoconference_users}}.conference_id' => $modelVideoconference->id]);
        $command = $query->createCommand();
        $modelVirtualClassUser = $command->queryAll();
        
        echo json_encode($modelVirtualClassUser);
        
    }
    
    public function actionGetvitrualclassinfo()
    {
        $videoconference_id = $_POST['videoconference_id'];
        $modelVideoconference = Videoconference::find()->where(['id' => $videoconference_id])->asArray()->one();
        echo json_encode($modelVideoconference);
    }
    
    public function actionVideoconferenceusers($videoconference_id)
    {
//        $modeVideoconferenceUser = Videoconferenceuser::find()->where(['conference_id' => $videoconference_id, 'user_id' => \Yii::$app->user->id])->one();
//        if($modeVideoconferenceUser == null){
//            return $this->redirect(Url::to("/videoclass/majorlist",true));
//        }else{
//            if($modeVideoconferenceUser->status != 1){
//                return $this->redirect(Url::to("/videoclass/majorlist",true));
//            }
//        } 
        $modelNewRating = new Classreting();
        $modelNewRating->scenario = 'add_rating';
        
        $modelVideoconference = Videoconference::find()->where(['id' => $videoconference_id])->one();
        $modelTutorsInfo = Tutorsinfo::find()->where(['user_id' => $modelVideoconference->tutors_id])->one();
        $modelTutorsUserInfo = UserInfo::find()->where(['id_user' => $modelVideoconference->tutors_id])->one();
        $modelTutorsUser = User::find()->where(['id' => $modelVideoconference->tutors_id])->one();
        $query = new Query();
        $query->select(['*', 'id' => '{{%videoconference_users}}.id', 'status' => '{{%videoconference_users}}.id'])
                        ->from('{{%videoconference_users}}')
                        ->join('LEFT JOIN',
                                '{{%user_info}}',
                                '{{%user_info}}.id_user = {{%videoconference_users}}.user_id'
                            )
                        ->join('LEFT JOIN',
                                        '{{%user}}',
                                        '{{%user}}.id = {{%videoconference_users}}.user_id'
                                )->where(['{{%videoconference_users}}.conference_id' => $modelVideoconference->id]);

        $modelVideoconferenceuser = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 10]]);
        
        return $this->render('videoconferenceuser',[
            'modelVideoconference' => $modelVideoconference,
            'modelVideoconferenceuser' => $modelVideoconferenceuser->getModels(),
            
            'modelTutorsUserInfo' => $modelTutorsUserInfo,
            'modelTutorsUser' => $modelTutorsUser,
            'modelTutorsInfo' => $modelTutorsInfo,
            'modelNewRating' => $modelNewRating,
        ]);
    }
    
    public function actionClosevideoclass(){
        $virtualclass_id = $_POST['videoclass_id'];
        $result = [];
        if($virtualclass_id != ''){
            $modelVonference = Videoconference::find()->where(['id' => $virtualclass_id])->one();
            $modelVonference->scenario = 'change_status';
            $modelVonference->status = '2';
            $modelVonference->save();
            
            $modelVonferenseI = Videoconferenceuser::updateAll(['status' => '2'], ['conference_id' => $virtualclass_id]);
            
            $userArray = [];
            $conferenceAllUser = Videoconferenceuser::find()->where(['conference_id' => $virtualclass_id])->asArray()->all();
            foreach($conferenceAllUser as $follower){
                $userArray[] = $follower['user_id'];
            }

            $newModelNotification = new Notification();
            $newModelNotification->scenario = 'add_notification';
            $newModelNotification->module = 'virtual_class';
            $newModelNotification->from_object_id = $virtualclass_id;
            $newModelNotification->to_object_id = $virtualclass_id;
            $newModelNotification->university_id = $modelVonference->id_university;
            $newModelNotification->notification_type = 'virtualclass_close';
            if($newModelNotification->save()){
                foreach($userArray as $user_id){
                    $modelNotificationUser = new Notificationusers();
                    $modelNotificationUser->scenario = 'new_notification';
                    $modelNotificationUser->user_id = $user_id;
                    $modelNotificationUser->notification_id = $newModelNotification->id;
                    $modelNotificationUser->save();
                }                
            }
            
            
            
            $result['status'] = true;
        }else{
            $result['status'] = false;
        }
        
        echo json_encode($result);
    }
    
    public function actionVideoconferenceinvitstion($videoconference_id)
    {
        $modeVideoconferenceUser = Videoconferenceuser::find()->where(['conference_id' => $videoconference_id, 'user_id' => \Yii::$app->user->id])->one();
        if($modeVideoconferenceUser == null){
            return $this->redirect(Url::to("/videoclass/majorlist",true));
        }else{
            if($modeVideoconferenceUser->status != 1){
                return $this->redirect(Url::to("/videoclass/majorlist",true));
            }
        } 
        
        $modelVideoconference = Videoconference::find()->where(['id' => $videoconference_id])->one();
        $modelTutorsInfo = Tutorsinfo::find()->where(['user_id' => $modelVideoconference->tutors_id])->one();
        $modelTutorsUserInfo = UserInfo::find()->where(['id_user' => $modelVideoconference->tutors_id])->one();
        $modelTutorsUser = User::find()->where(['id' => $modelVideoconference->tutors_id])->one();
        
        $query = new Query();
        $query->select(['*', 'id' => '{{%user}}.id',])
                        ->from('{{%user}}')
                        ->join('LEFT JOIN',
                                '{{%user_info}}',
                                '{{%user_info}}.id_user = {{%user}}.id'
                                );
        if($_GET){
            if(isset($_GET['university_class'])){
                if(isset($_GET['userNameSurname'])){
                    if($_GET['userNameSurname'] != ''){
                        $query->andWhere(['LIKE', '{{%user}}.name', $_GET['userNameSurname']]);
                        $query->orWhere(['LIKE', '{{%user}}.surname', $_GET['userNameSurname']]);
                    }
                }
                if(isset($_GET['university_class'])){
                    if($_GET['university_class'] != ''){
                        $query->andWhere(['LIKE', '{{%user_info}}.university_id', $_GET['university_class']]);
                    }
                }
                if(isset($_GET['major_class'])){
                    if($_GET['major_class'] != ''){
                        $query->andWhere(['LIKE', '{{%user_info}}.major_id', $_GET['major_class']]);
                    }
                }
//                if(isset($_GET['course_class'])){
//                    if($_GET['course_class'] != ''){
//                        echo $_GET['course_class'].'<br>';
//                    }
//                }
            }
        }
        $modelVideoconferenceuser = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 10]]);
        
        
        $universityArray = ArrayHelper::map(University::find()->all(), 'id', 'name');
        
        $majorArray = [];
        if(isset($_GET['university_class'])){
            if($_GET['university_class'] != ''){
                $majorArray = ArrayHelper::map(Major::find()->where(['university_id' => $_GET['university_class']])->all(), 'id', 'name');
            }
        }
        
        $courseArray = [];
        if(isset($_GET['major_class'])){
            if($_GET['major_class'] != ''){
                $courseArray = ArrayHelper::map(Course::find()->where(['id_major' => $_GET['major_class']])->all(), 'id', 'name');
            }
        }
        
        
        
        return $this->render('videoconferenceinvitstion',[
            'modelVideoconference' => $modelVideoconference,
            
            'universityArray' => $universityArray,
            'majorArray' => $majorArray,
            'courseArray' => $courseArray,
            
            'modelUsers' => $modelVideoconferenceuser->getModels(),
            'pagination' => $modelVideoconferenceuser->pagination,
            'count' => $modelVideoconferenceuser->pagination->totalCount,
            
            'modelTutorsUserInfo' => $modelTutorsUserInfo,
            'modelTutorsUser' => $modelTutorsUser,
            'modelTutorsInfo' => $modelTutorsInfo,
        ]);
    }
    
    public function actionChangefollowstatus()
    {
        $vitrualclass_id = $_POST['class_id'];
        $modelConferenceuser = Videoconferenceuser::find()->where(['conference_id' => $vitrualclass_id])->one();
        $modelConferenceuser->scenario = 'change_status';
        if($_POST['status'] == 'confirm'){
            $modelConferenceuser->status = '1';
        }else{
            $modelConferenceuser->status = '2';
        }
        $result = [];
        if($modelConferenceuser->save()){
            $result['status'] = 'success';
        }else{
            $result['status'] = 'error';
        }
        
        echo json_encode($result);
        
    }
    
    public function actionVideoconference($videoconference_id)
    {
//        $modeVideoconferenceUser = Videoconferenceuser::find()->where(['conference_id' => $videoconference_id, 'user_id' => \Yii::$app->user->id])->one();
//        if($modeVideoconferenceUser == null){
//            return $this->redirect(Url::to("/videoclass/majorlist",true));
//        }else{
//            if($modeVideoconferenceUser->status != 1){
//                return $this->redirect(Url::to("/videoclass/majorlist",true));
//            }
//        } 
        
        $modelNewRating = new Virtualclassreting();
        $modelNewRating->scenario = 'add_rating';
        $modelVideoconference = Videoconference::find()->where(['id' => $videoconference_id])->one();
        if($modelVideoconference){
            $joinClassStatus = Videoconferenceuser::find()->where(['conference_id' => $modelVideoconference->id, 'user_id' => \Yii::$app->user->id])->one();
            
//            if((!$joinClassStatus) || ($joinClassStatus->status != 1)){
//                //modal pay
//                var_dump($modelVideoconference->price_type); //0-credits, 1-paypal 
//                var_dump($modelVideoconference->price); 
//                var_dump($modelVideoconference->id); //conference_id 
//            }
                    
            $modelTutorsInfo = Tutorsinfo::find()->where(['user_id' => $modelVideoconference->tutors_id])->one();
            $modelTutorsUserInfo = UserInfo::find()->where(['id_user' => $modelVideoconference->tutors_id])->one();
            $modelTutorsUser = User::find()->where(['id' => $modelVideoconference->tutors_id])->one();

            $modelVideoconferenceuser = Videoconferenceuser::find()->where(['conference_id' => $modelVideoconference->id])->all();

            if($_POST){
                $request = Yii::$app->request;
                if($modelNewRating->load($request->post())){
                    if($modelNewRating->save()){
                        $modelRatingRequest = new Ratingrequest();
                        $modelRatingRequest->scenario = 'add_rating_request';
                        $modelRatingRequest->from_user_id = \Yii::$app->user->id;
                        $modelRatingRequest->to_object_id = $modelVideoconference->tutors_id;
                        $modelRatingRequest->rating_count = $modelNewRating->rate_professor;
                        $modelRatingRequest->type = 'virtualclass_professor';
                        if($modelRatingRequest->save()){

                        }
                        return $this->redirect('/videoconference/'.$modelVideoconference->id);
                    }
                }
            }

            $query = new Query();
            $query->select(['*', 'id' => '{{%videoconference_users}}.id', 'status' => '{{%videoconference_users}}.id'])
                            ->from('{{%videoconference_users}}')
                            ->join('LEFT JOIN',
                                    '{{%user_info}}',
                                    '{{%user_info}}.id = {{%videoconference_users}}.user_id'
                                )
                            ->join('LEFT JOIN',
                                            '{{%user}}',
                                            '{{%user}}.id = {{%videoconference_users}}.user_id'
                                    )->where(['{{%videoconference_users}}.conference_id' => $modelVideoconference->id]);

            $command = $query->createCommand();
            $modelVideoconferenceuser = $command->queryAll();
            $modelVonferenseI = Videoconferenceuser::find()->where(['user_id' => \Yii::$app->user->id, 'conference_id' => $videoconference_id])->one();
            $classEnd = '';
            if($modelVonferenseI){
                $classEnd = $modelVonferenseI->status;
            }

            return $this->render('videoconference',[
                'modelVideoconferenceuser' => $modelVideoconferenceuser,
                'modelVideoconference' => $modelVideoconference,
                'modelTutorsUserInfo' => $modelTutorsUserInfo,
                'modelTutorsUser' => $modelTutorsUser,
                'modelTutorsInfo' => $modelTutorsInfo,
                'modelNewRating' => $modelNewRating,
                'classEnd' => $classEnd,
            ]);
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    public function actionTutorspage($tutors_id=null)
    {
        if($tutors_id == null){
            $tutors_id == \Yii::$app->user->id;
        }
        
        $modelBecomeTutors = new Tutorsinfo();
        $modelBecomeTutors->scenario = 'add_tutors';
        
        $modelNewVideoconference = new Videoconference();
        $modelNewVideoconference->scenario = 'add_conference';
        $modelTutorsInfo = Tutorsinfo::find()->where(['user_id' => $tutors_id])->one();
        if($modelTutorsInfo){
            $modelTutorsInfo->scenario = 'add_tutors';           
        }
        
        if($_POST){
            $request = Yii::$app->request;
            if(isset($_POST['update_tutor'])){
                if($modelTutorsInfo->load($request->post())){
                    \app\models\Tutorscourse::deleteAll(['tutors_id' => $modelTutorsInfo->id]);
                    if($modelTutorsInfo->save()){
                            if(isset($_POST['Tutorsinfo']['course'])){
                                foreach($_POST['Tutorsinfo']['course'] as $course_id){
                                    $modelNewTutorsCourse = new \app\models\Tutorscourse();
                                    $modelNewTutorsCourse->scenario = 'add_tutors';
                                    $modelNewTutorsCourse->tutors_id = $modelTutorsInfo->id;
                                    $modelNewTutorsCourse->course_id = $course_id;
                                    $modelNewTutorsCourse->save();
                                }
                            }
                    }
                }
            }
            
            if(isset($_POST['become_a_tutor'])){
                $modelTutorsInfo = Tutorsinfo::find()->where(['user_id' => $tutors_id])->one();
                if($modelTutorsInfo == null){
                    if($modelBecomeTutors->load($request->post())){
                        $modelBecomeTutors->user_id = \Yii::$app->user->id;
                        if($modelBecomeTutors->save()){
                            if(isset($_POST['Tutorsinfo']['course'])){
                                foreach($_POST['Tutorsinfo']['course'] as $course_id){
                                    $modelNewTutorsCourse = new \app\models\Tutorscourse();
                                    $modelNewTutorsCourse->scenario = 'add_tutors';
                                    $modelNewTutorsCourse->tutors_id = $modelBecomeTutors->id;
                                    $modelNewTutorsCourse->course_id = $course_id;
                                    $modelNewTutorsCourse->save();
                                }
                            }
                            $university_id = 0;
                            if(!\Yii::$app->user->isGuest){
                                if(Yii::$app->user->identity->getUsertUniversity()){
                                    $university_id = Yii::$app->user->identity->getUsertUniversity();
                                }
                            }
                            //add credits action begin
                            $modelCreditsaction = Creditsaction::find()->where(['object_id' => \Yii::$app->user->id, 'action' => 'become_tutor'])->one();
                                if(!$modelCreditsaction){
                                            $newModelCreditsaction = new Creditsaction();
                                            $newModelCreditsaction->scenario = 'add_credits';
                                            $newModelCreditsaction->object_id = \Yii::$app->user->id;
                                            $newModelCreditsaction->action = 'become_tutor';
                                            $newModelCreditsaction->action_count = 1;
                                            if($newModelCreditsaction->save()){
                                                $modelCreditsactionCount = Creditsactioncount::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                                if($modelCreditsactionCount){
                                                    $modelCreditsactionCount->scenario = 'update_credits_count';
                                                    $modelCreditsactionCount->credits_count = $newModelCreditsactionCount->credits_count + 3;
                                                    $modelCreditsactionCount->date_update = date("Y-m-d H:i:s");
                                                    if($modelCreditsactionCount->save()){
                                                    }
                                                }else{
                                                    $newModelCreditsactionCount = new Creditsactioncount();
                                                    $newModelCreditsactionCount->scenario = 'add_credits_count';
                                                    $newModelCreditsactionCount->user_id = \Yii::$app->user->id;
                                                    $newModelCreditsactionCount->credits_count = 3;
                                                    if($newModelCreditsactionCount->save()){
                                                        
                                                    }
                                                }
                                            }
                                            $modelCredits = \app\models\Usercredits::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                            if($modelCredits){
                                                $modelCredits->scenario = 'update_credits_count';
                                                $modelCredits->credit_count = $modelCredits->credit_count + 3;
                                                if($modelCredits->save()){
                                                }                            
                                            }else{
                                                $modelNewCredits = new \app\models\Usercredits();
                                                $modelNewCredits->scenario = 'add_credits';
                                                $modelNewCredits->user_id = \Yii::$app->user->id;
                                                $modelNewCredits->credit_count = 3;
                                                $modelNewCredits->many = 0;
                                                if($modelNewCredits->save()){

                                                }
                                            }

                                            $modelNotification = new Notification();
                                            $modelNotification->scenario = 'add_notification';
                                            $modelNotification->module = 'university';
                                            $modelNotification->from_object_id = \Yii::$app->user->id;
                                            $modelNotification->to_object_id = \Yii::$app->user->id;
                                            $modelNotification->notification_type = 'become_tutor';
                                            $modelNotification->university_id = $university_id;
                                            if($modelNotification->save()){
                                                $modelNotificationUser = new Notificationusers();
                                                $modelNotificationUser->scenario = 'new_notification';
                                                $modelNotificationUser->user_id = \Yii::$app->user->id;
                                                $modelNotificationUser->notification_id = $modelNotification->id;
                                                $modelNotificationUser->save();
                                            }

                                }
                                return $this->redirect('/tutorspage/'.$tutors_id);
                            
                        }
                    }
                }
            }
            
            if(isset($_POST['add_conference'])){
                if($modelNewVideoconference->load($request->post())){
                    if($modelNewVideoconference->save()){
                        
                        
                        
                        
                        //add credits action begin
                            $modelCreditsaction = Creditsaction::find()->where(['object_id' => \Yii::$app->user->id, 'action' => 'create_virtual_class'])->one();
                                if($modelCreditsaction){
                                    $modelCreditsaction->scenario = 'change_action_count';
                                    $modelCreditsaction->action_count = $modelCreditsaction->action_count + 1;
                                    if($modelCreditsaction->save()){
                                    }


                                    if((($modelCreditsaction->action_count % 10) == 0)){
                                        $modelCreditsactionCount = Creditsactioncount::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                        if($modelCreditsactionCount){
                                            $modelCreditsactionCount->scenario = 'update_credits_count';
                                            $modelCreditsactionCount->credits_count = $modelCreditsactionCount->credits_count + 1;
                                            $modelCreditsactionCount->date_update = date("Y-m-d H:i:s");
                                            if($modelCreditsactionCount->save()){

                                            }
                                        }else{
                                            $newModelCreditsactionCount = new Creditsactioncount();
                                            $newModelCreditsactionCount->scenario = 'add_credits_count';
                                            $newModelCreditsactionCount->user_id = \Yii::$app->user->id;
                                            $newModelCreditsactionCount->credits_count = 1;
                                            if($newModelCreditsactionCount->save()){

                                            }
                                        }

                                        $modelCredits = \app\models\Usercredits::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                        if($modelCredits){
                                            $modelCredits->scenario = 'update_credits_count';
                                            $modelCredits->credit_count = $modelCredits->credit_count + 1;
                                            if($modelCredits->save()){
                                            }
                                        }else{
                                            $modelNewCredits = new \app\models\Usercredits();
                                            $modelNewCredits->scenario = 'add_credits';
                                            $modelNewCredits->user_id = \Yii::$app->user->id;
                                            $modelNewCredits->credit_count = 1;
                                            $modelNewCredits->many = 0;
                                            if($modelNewCredits->save()){

                                            }
                                        }

                                            $modelNotification = new Notification();
                                            $modelNotification->scenario = 'add_notification';
                                            $modelNotification->module = 'university';
                                            $modelNotification->from_object_id = \Yii::$app->user->id;
                                            $modelNotification->to_object_id = \Yii::$app->user->id;
                                            $modelNotification->notification_type = 'virtual_class_credits';
                                            $modelNotification->university_id = 0;
                                            if($modelNotification->save()){
                                                $modelNotificationUser = new Notificationusers();
                                                $modelNotificationUser->scenario = 'new_notification';
                                                $modelNotificationUser->user_id = \Yii::$app->user->id;
                                                $modelNotificationUser->notification_id = $modelNotification->id;
                                                $modelNotificationUser->save();
                                            }
                                    }



                                }else{
                                            $newModelCreditsaction = new Creditsaction();
                                            $newModelCreditsaction->scenario = 'add_credits';
                                            $newModelCreditsaction->object_id = \Yii::$app->user->id;
                                            $newModelCreditsaction->action = 'create_virtual_class';
                                            $newModelCreditsaction->action_count = 1;
                                            if($newModelCreditsaction->save()){
                                                $modelCreditsactionCount = Creditsactioncount::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                                if($modelCreditsactionCount){
                                                    $modelCreditsactionCount->scenario = 'update_credits_count';
                                                    $modelCreditsactionCount->credits_count = $modelCreditsactionCount->credits_count + 2;
                                                    $modelCreditsactionCount->date_update = date("Y-m-d H:i:s");
                                                    if($modelCreditsactionCount->save()){
                                                    }
                                                }else{
                                                    $newModelCreditsactionCount = new Creditsactioncount();
                                                    $newModelCreditsactionCount->scenario = 'add_credits_count';
                                                    $newModelCreditsactionCount->user_id = \Yii::$app->user->id;
                                                    $newModelCreditsactionCount->credits_count = 2;
                                                    if($newModelCreditsactionCount->save()){

                                                    }
                                                }
                                            }
                                            $modelCredits = \app\models\Usercredits::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                            if($modelCredits){
                                                $modelCredits->scenario = 'update_credits_count';
                                                $modelCredits->credit_count = $modelCredits->credit_count + 2;
                                                if($modelCredits->save()){
                                                }                            
                                            }else{
                                                $modelNewCredits = new \app\models\Usercredits();
                                                $modelNewCredits->scenario = 'add_credits';
                                                $modelNewCredits->user_id = \Yii::$app->user->id;
                                                $modelNewCredits->credit_count = 2;
                                                $modelNewCredits->many = 0;
                                                if($modelNewCredits->save()){

                                                }
                                            }

                                            $modelNotification = new Notification();
                                            $modelNotification->scenario = 'add_notification';
                                            $modelNotification->module = 'university';
                                            $modelNotification->from_object_id = \Yii::$app->user->id;
                                            $modelNotification->to_object_id = \Yii::$app->user->id;
                                            $modelNotification->notification_type = 'forst_create_virtual_class';
                                            $modelNotification->university_id = 0;
                                            if($modelNotification->save()){
                                                $modelNotificationUser = new Notificationusers();
                                                $modelNotificationUser->scenario = 'new_notification';
                                                $modelNotificationUser->user_id = \Yii::$app->user->id;
                                                $modelNotificationUser->notification_id = $modelNotification->id;
                                                $modelNotificationUser->save();
                                            }

                                }
                        //add credits action end
                        
                        
                        
                        
//                        $userArray = [];
//                        $conferenceAllUser = Videoconferenceuser::find()->where(['conference_id' => $modelNewVideoconference->id])->asArray()->all();
//                        foreach($conferenceAllUser as $follower){
//                            $userArray[] = $follower['user_id'];
//                        }
//                        if($class != null){
//                            $university_id = $class->id_university; 
//                            $newModelNotification = new Notification();
//                            $newModelNotification->scenario = 'add_notification';
//                            $newModelNotification->module = 'virtual_class';
//                            $newModelNotification->from_object_id = \Yii::$app->user->id;
//                            $newModelNotification->to_object_id = $class->class_id;
//                            $newModelNotification->university_id = $university_id;
//                            $newModelNotification->notification_type = 'user_add_to_virtualclass';
//                            if($newModelNotification->save()){
//                                foreach($userArray as $user_id){
//                                    $modelNotificationUser = new Notificationusers();
//                                    $modelNotificationUser->scenario = 'new_notification';
//                                    $modelNotificationUser->user_id = $user_id;
//                                    $modelNotificationUser->notification_id = $newModelNotification->id;
//                                    $modelNotificationUser->save();
//                                }                
//                            }
//                        }
                        
                        
                        
                        
                        $choise_user = json_decode($_POST['Videoconference']['choise_user']);
                        if($choise_user){
                            foreach($choise_user as $user_id){
                                $modelVideoconferenceuser = new Videoconferenceuser();
                                $modelVideoconferenceuser->scenario = 'add_conference';
                                $modelVideoconferenceuser->conference_id = $modelNewVideoconference->id;
                                $modelVideoconferenceuser->user_id = $user_id;
                                $modelVideoconferenceuser->status = '0';
                                if($modelVideoconferenceuser->save()){
                                    $newModelNotification = new Notification();
                                    $newModelNotification->scenario = 'add_notification';
                                    $newModelNotification->module = 'virtual_class';
                                    $newModelNotification->from_object_id = $user_id;
                                    $newModelNotification->to_object_id = $modelNewVideoconference->id;
                                    $newModelNotification->university_id = $university_id;
                                    $newModelNotification->notification_type = 'user_invite_to_virtualclass';
                                    if($newModelNotification->save()){
                                        $modelNotificationUser = new Notificationusers();
                                        $modelNotificationUser->scenario = 'new_notification';
                                        $modelNotificationUser->user_id = $user_id;
                                        $modelNotificationUser->notification_id = $newModelNotification->id;
                                        $modelNotificationUser->save();
                                    }
                                }
                            }
                        }
                        return $this->redirect(Url::home().'videoconference/'.$modelNewVideoconference->id);
                    }
                }
            }
        }
        
        $query = new Query();
        $query->select(['*', 'id' => '{{%videoconference}}.id', 'class_status' => '{{%videoconference}}.status',  'className' => '{{%videoconference}}.name','userName' => '{{%user}}.name', 'conferenceDateCreate' => '{{%videoconference}}.date_create'])
                        ->from('{{%videoconference}}')
                        ->join('LEFT JOIN',
                                '{{%user_info}}',
                                '{{%user_info}}.id = {{%videoconference}}.tutors_id')
                        ->join('LEFT JOIN',
                                        '{{%user}}',
                                        '{{%user}}.id = {{%videoconference}}.tutors_id')
                ->where(['{{%videoconference}}.tutors_id' => $tutors_id]);
        $modelVideoconferences = new ActiveDataProvider(['query' => $query, 'pagination' => ['pageSize' => 10]]);
        
        $modelUserinfo = Userinfo::find()->where(['id_user' => $tutors_id])->one();
        $modelUserRole = \app\modules\users\models\UserRole::find()->where(['id' => $modelUserinfo->academic_status])->one();
        $modelUser = User::find()->where(['id' => $tutors_id])->one();
        $courseArray = [];
        if($modelTutorsInfo){
            $modelTurorsCourse = \app\models\Tutorscourse::find()->where(['tutors_id' => $modelTutorsInfo->id])->all();
            $course_id = [];
            if($modelTurorsCourse){
                foreach($modelTurorsCourse as $turorsCourse){
                    $course_id[] = $turorsCourse->course_id;
                }            
            }
            $courseArray = ArrayHelper::map(Course::find()->where(['id' => $course_id])->all(), 'id', 'name');            
        }
        $courseAllArray = ArrayHelper::map(Course::find()->all(), 'id', 'name');
        $modelUniversityUser = University::find()->where(['id' => $modelUserinfo['university_id']])->one();
        return $this->render('mypage',[
            'courseAllArray' => $courseAllArray,
            'modelVideoconferences' => $modelVideoconferences->getModels(),
            'pagination' => $modelVideoconferences->pagination,
            'count' => $modelVideoconferences->pagination->totalCount,            
            'modelNewVideoconference' => $modelNewVideoconference,
            'modelUniversityUser' => $modelUniversityUser,
            'modelBecomeTutors' => $modelBecomeTutors,
            'modelTutorsInfo' => $modelTutorsInfo,
            'modelUserinfo' => $modelUserinfo,
            'modelUserRole' => $modelUserRole,
            'courseArray' => $courseArray,
            'modelUser' => $modelUser,
            'tutors_id' => $tutors_id,
        ]);
    }
    
    public function actionTutorsdelete($tutors_id)
    {
        $modelTutors = Tutors::find()->where(['id' => $tutors_id])->one();
        if($modelTutors->delete()){
            return $this->redirect('/mypage');
        }
    }
    
    public function actionProduct($product_id,$class_name)
    {
        $modelClass = Classes::find()->where(['url_name' => $class_name])->one();
        $query = new Query();
        $query->select(['*', 'id' => '{{%user}}.id'])
                        ->from('{{%user}}')
                        ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%user}}.id'
                                )->where(['{{%user}}.id' => $modelClass->professor]);

        $command = $query->createCommand();
        $modelProfessor = $command->queryOne();
        
        
        $modelProduct = Product::find()->where(['id' => $product_id])->one();
        $queryProduct = new Query();
        $queryProduct->select(['*', 'id' => '{{%product}}.id',  'classUrlName' => '{{%class}}.url_name','userName' => '{{%user}}.name', 'productDatecreate' => '{{%product}}.date_create'])
                        ->from('{{%product}}')
                        ->join('LEFT JOIN',
                                        '{{%user}}',
                                        '{{%user}}.id = {{%product}}.user_id'
                        )->join('LEFT JOIN',
                                        '{{%class}}',
                                        '{{%class}}.id = {{%product}}.class_id'
                        )->join('LEFT JOIN',
                                        '{{%files}}',
                                        '{{%files}}.id = {{%product}}.file_id'
                        )->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%product}}.user_id'
                                )->where(['{{%product}}.id'=>$product_id])->orderBy('{{%product}}.date_create DESC');

        $command = $queryProduct->createCommand();
        $modelProduct = $command->queryOne();
        
        $modelProductpay = Productpay::find()->where([
            'user_id' => \Yii::$app->user->id,
            'product_id' => $modelProduct['id'],
            'status' => 1,
        ])->one();
            
        return $this->render('product',[
            'modelClass' => $modelClass,
            'modelProfessor' => $modelProfessor,
            'modelProduct' => $modelProduct,
            'modelProductpay' => $modelProductpay,
        ]);
    }
    
    public function actionSearchvirtualclass()
    {
        
        
        
        
        
        $queryVideoconference = new Query();
        $queryVideoconference->select(['*', 'id' => '{{%videoconference}}.id', 'class_status' => '{{%videoconference}}.status',  'className' => '{{%videoconference}}.name','userName' => '{{%user}}.name', 'conferenceDateCreate' => '{{%videoconference}}.date_create'])
                        ->from('{{%videoconference}}')
                        ->join('LEFT JOIN',
                                        '{{%user}}',
                                        '{{%user}}.id = {{%videoconference}}.tutors_id')
                       ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%videoconference}}.tutors_id')
                        ->where(['not in', '{{%videoconference}}.status', 2]);
        if($_GET){
            if(isset($_GET['name_conference'])){
                if($_GET['name_conference'] != ''){
                    $queryVideoconference->andWhere(['LIKE', '{{%videoconference}}.name', $_GET['name_conference']]);
                }
            }
            
            if(isset($_GET['university_conference'])){
                if(($_GET['university_conference'] != '') && ($_GET['university_conference'] != '0')){
                    $queryVideoconference->andWhere(['{{%videoconference}}.id_university' => $_GET['university_conference']]);
                }
            }
            if(isset($_GET['major_conference'])){
                if(($_GET['major_conference'] != '') && ($_GET['major_conference'] != '0')){
                    $queryVideoconference->andWhere(['{{%videoconference}}.id_major' => $_GET['major_conference']]);
                }
            }
            if(isset($_GET['course_conference'])){
                if(($_GET['course_conference'] != '') && ($_GET['course_conference'] != '0')){
                    $queryVideoconference->andWhere(['{{%videoconference}}.id_course' => $_GET['course_conference']]);
                }
            }
            if(isset($_GET['tutors_conference'])){
                if(($_GET['tutors_conference'] != '') && ($_GET['tutors_conference'] != '0')){
                    $queryVideoconference->andWhere(['{{%videoconference}}.tutors_id' => $_GET['tutors_conference']]);
                }
            }
        }
        
        $queryVideoconference->limit(20);
        $queryVideoconference->orderBy([
            '{{%videoconference}}.date_start' => SORT_DESC,
        ]);
        
        $command = $queryVideoconference->createCommand();
        $modelVideoconference = $command->queryAll();
 
        $videoconferences = [];
        foreach($modelVideoconference as $modelConference){
            $date = date_create($modelConference['date_start']);
            $videoconferences[date_format($date, 'd.m.Y')][] = $modelConference;
        }
        
        return $this->render('searchvirtualclass',[
            'videoconferences' => $videoconferences
        ]);
    }

    ///////listat_an

  public function actionPay_class_credits(){

    $class = Videoconference::findOne($_POST['class_id']);
    if($class) {
      $user_credits = Usercredits::find()->where(['user_id' => $class->tutors_id])->one();
      $user_credits->scenario = 'update_credits_count';
      $my_credits = Usercredits::find()->where(['user_id' => Yii::$app->user->id])->one();
      $my_credits->scenario = 'update_credits_count';
      if(isset($_GET['price']))
        if ($_GET['price'] <= $my_credits->credit_count) {
        $my_credits->credit_count = $my_credits->credit_count - $_GET['price'];
        $user_credits->credit_count = $user_credits->credit_count - $_GET['price'];
        $my_credits->save();
        $user_credits->save();

        $conferens_user = Videoconferenceuser::findOne(['conference_id' => $_GET['class_id'], 'user_id' => Yii::$app->user->id]);
        
        
        
        
        
        $userArray = [];
        $conferenceAllUser = Videoconferenceuser::find()->where(['conference_id' => $class->class_id])->asArray()->all();
        foreach($conferenceAllUser as $follower){
            $userArray[] = $follower['user_id'];
        }
        if($class != null){
            $university_id = $class->id_university; 
            $newModelNotification = new Notification();
            $newModelNotification->scenario = 'add_notification';
            $newModelNotification->module = 'virtual_class';
            $newModelNotification->from_object_id = \Yii::$app->user->id;
            $newModelNotification->to_object_id = $class->class_id;
            $newModelNotification->university_id = $university_id;
            $newModelNotification->notification_type = 'user_add_to_virtualclass';
            if($newModelNotification->save()){
                foreach($userArray as $user_id){
                    $modelNotificationUser = new Notificationusers();
                    $modelNotificationUser->scenario = 'new_notification';
                    $modelNotificationUser->user_id = $user_id;
                    $modelNotificationUser->notification_id = $newModelNotification->id;
                    $modelNotificationUser->save();
                }                
            }
        }
        
        $modelFriend = UserFriend::find()->where(['id_user' => \Yii::$app->user->id])->asArray()->all();
        foreach($modelFriend as $friend){
            $friendId[] = $friend['id_friend'];
        }
        $newModelNotificationF = new Notification();
        $newModelNotificationF->scenario = 'add_notification';
        $newModelNotificationF->module = 'virtual_class';
        $newModelNotificationF->from_object_id = \Yii::$app->user->id;
        $newModelNotificationF->to_object_id = $class->class_id;
        $newModelNotificationF->university_id = $university_id;
        $newModelNotificationF->notification_type = 'friend_add_to_virtualclass';
        if($newModelNotificationF->save()){
            foreach($friendId as $friend_id){
                $modelNotificationUserF = new Notificationusers();
                $modelNotificationUserF->scenario = 'new_notification';
                $modelNotificationUserF->user_id = $friend_id;
                $modelNotificationUserF->notification_id = $newModelNotificationF->id;
                $modelNotificationUserF->save();
            }                
        }
        
        $newModelNotificationF = new Notification();
        $newModelNotificationF->scenario = 'add_notification';
        $newModelNotificationF->module = 'virtual_class';
        $newModelNotificationF->from_object_id = \Yii::$app->user->id;
        $newModelNotificationF->to_object_id = $class->class_id;
        $newModelNotificationF->university_id = $university_id;
        $newModelNotificationF->notification_type = 'you_added_to_virtualclass';
        if($newModelNotificationF->save()){
                $modelNotificationUserF = new Notificationusers();
                $modelNotificationUserF->scenario = 'new_notification';
                $modelNotificationUserF->user_id = \Yii::$app->user->id;
                $modelNotificationUserF->notification_id = $newModelNotificationF->id;
                $modelNotificationUserF->save();
        }
        
        
        
        
        if ($conferens_user) {
            $conferens_user->scenario = 'add_conference';
            $conferens_user->status = 1;
            $conferens_user->save();
        } else {
            $modelNewVideoconferenceuser = new Videoconferenceuser();
            $modelNewVideoconferenceuser->scenario = 'add_conference';
            $modelNewVideoconferenceuser->conference_id = $_GET['class_id'];
            $modelNewVideoconferenceuser->status = 1;
            $modelNewVideoconferenceuser->user_id = Yii::$app->user->id;
            $modelNewVideoconferenceuser->save();
        }

      }
    }
      $this->redirect('/'.$_GET['redirect']);
  }


  public function actionPay_class(){
    if (!Yii::$app->user->isGuest) {


      if (isset($_POST['price']) && isset($_POST['class_id'])){
        $dPrice = $_POST['price'];
        $class = Videoconference::findOne($_POST['class_id']);
        if($class){
          $user_paymend = $class->tutors_id;
          $class_id = $class->id;
        } else {
          throw new \yii\web\HttpException(403);
        }
      } else {
        throw new \yii\web\HttpException(403);
      }

      $pay = New Paypal();
      $pay = $pay->api;

      $pay->setConfig([
        'model'=>'sandbox',
        'http.ConnectionTimeOut'=>30,
        'log.LogEnabled'=> YII_DEBUG ? 0 : 0,
        'log.FileName'=> '',
        'log.LogLevel' => 'FINE',
        'validation.level'=>'log'
      ]);

      $payer = new Payer();
      $payer->setPaymentMethod("paypal");

      $item2 = new Item();
      $item2->setName('Payment virtual class')
        ->setCurrency('USD')
        ->setQuantity(1)
        ->setSku('N-C-'+time()) // Similar to `item_number` in Classic API
        ->setPrice($dPrice);
      $itemList = new ItemList();
      $itemList->setItems(array($item2));


      $details = new Details();
      $details->setShipping(0.00)
        ->setTax(0.00)
        ->setSubtotal($dPrice);


      $amount = new Amount();
      $amount->setCurrency("USD")
        ->setTotal($dPrice+0.00)
        ->setDetails($details);



      $transaction = new Transaction();
      $transaction->setAmount($amount)
        ->setItemList($itemList)
        ->setDescription('Paymend virtual class')
        ->setInvoiceNumber(uniqid());

      $redirectUrls = new RedirectUrls();
      $redirectUrls->setReturnUrl(Url::home(true).'virtualclass/pay?approved=true&user_paymend='.$user_paymend.'&class_id='.$class_id.'&redirect='.$_POST['redirect'])
        ->setCancelUrl(Url::home(true).'virtualclass/pay?approved=false&user_paymend='.$user_paymend.'&class_id='.$class_id.'&redirect='.$_POST['redirect']);


      $payment = new Payment();
      $payment->setIntent("sale")
        ->setPayer($payer)
        ->setRedirectUrls($redirectUrls)
        ->setTransactions(array($transaction));

      try{
        $payment->create($pay);
      }catch (PayPalConnectionException $e){

        throw new UserException($e->getMessage());

      }
      $trans= new \app\models\Transaction();
      $trans->user_id= Yii::$app->user->id;
      $trans->price=$dPrice;
      $trans->payment_id=$payment->getId();
      $trans->hash=md5($payment->getId());
      $trans->othe='Paymend virtual class';
      $trans->project_id=$class_id;
      $trans->save();
      Yii::$app->getResponse()->redirect($payment->getApprovalLink());
    } else {
      throw new \HttpException(403);
    }
  }

  public function actionPay(){

    $pay = New Paypal();
    $pay = $pay->api;

    if (isset($_GET['approved']) && $_GET['approved'] == 'true') {
      $paymentId = $_GET['paymentId'];
      $payment = Payment::get($paymentId, $pay);
      $execution = new PaymentExecution();
      $execution->setPayerId($_GET['PayerID']);
      try {
        $result = $payment->execute($execution, $pay);
      } catch (PayPalConnectionException $e) {
        throw new UserException($e->getMessage());
      }
      $transaction = \app\models\Transaction::find()->where(['user_id'=>Yii::$app->user->id,'hash'=>md5($paymentId)])->one();
      $transaction->complete=1;
      $transaction->save();

      $conferens_user = Videoconferenceuser::findOne(['conference_id'=>$_GET['class_id'],'user_id'=>Yii::$app->user->id]);

      if ($conferens_user){
        $conferens_user->scenario = 'add_conference';
        $conferens_user->status = 1;
        $conferens_user->save();
      }else{
        $modelNewVideoconferenceuser = new Videoconferenceuser();
        $modelNewVideoconferenceuser->scenario = 'add_conference';
        $modelNewVideoconferenceuser->conference_id = $_GET['class_id'];
        $modelNewVideoconferenceuser->status = 1;
        $modelNewVideoconferenceuser->user_id = Yii::$app->user->id;
        $modelNewVideoconferenceuser->save();
      }
      
      
      
      
      
      //add credits action begin
                            $modelCreditsaction = Creditsaction::find()->where(['object_id' => \Yii::$app->user->id, 'action' => 'join_virtual_class'])->one();
                            if($modelCreditsaction){
                                $modelCreditsaction->scenario = 'change_action_count';
                                $modelCreditsaction->action_count = $modelCreditsaction->action_count + 1;
                                if($modelCreditsaction->save()){
                                }
                                
                                
                                if((($modelCreditsaction->action_count % 10) == 0)){
                                    $modelCreditsactionCount = Creditsactioncount::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                    if($modelCreditsactionCount){
                                        $modelCreditsactionCount->scenario = 'update_credits_count';
                                        $modelCreditsactionCount->credits_count = $modelCreditsactionCount->credits_count + 1;
                                        $modelCreditsactionCount->date_update = date("Y-m-d H:i:s");
                                        if($modelCreditsactionCount->save()){
                                            
                                        }
                                    }else{
                                        $newModelCreditsactionCount = new Creditsactioncount();
                                        $newModelCreditsactionCount->scenario = 'add_credits_count';
                                        $newModelCreditsactionCount->user_id = \Yii::$app->user->id;
                                        $newModelCreditsactionCount->credits_count = 1;
                                        if($newModelCreditsactionCount->save()){

                                        }
                                    }
                                    
                                    $modelCredits = \app\models\Usercredits::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                    if($modelCredits){
                                        $modelCredits->scenario = 'update_credits_count';
                                        $modelCredits->credit_count = $modelCredits->credit_count + 1;
                                        if($modelCredits->save()){
                                        }                            
                                    }else{
                                        $modelNewCredits = new \app\models\Usercredits();
                                        $modelNewCredits->scenario = 'add_credits';
                                        $modelNewCredits->user_id = \Yii::$app->user->id;
                                        $modelNewCredits->credit_count = 1;
                                        $modelNewCredits->many = 0;
                                        if($modelNewCredits->save()){

                                        }
                                    }
                                    
                                        $modelNotification = new Notification();
                                        $modelNotification->scenario = 'add_notification';
                                        $modelNotification->module = 'university';
                                        $modelNotification->from_object_id = \Yii::$app->user->id;
                                        $modelNotification->to_object_id = $_GET['class_id'];
                                        $modelNotification->notification_type = 'join_virtual_class_credits';
                                        $modelNotification->university_id = $modelNewVideoconferenceuser->id_university;
                                        if($modelNotification->save()){
                                            $modelNotificationUser = new Notificationusers();
                                            $modelNotificationUser->scenario = 'new_notification';
                                            $modelNotificationUser->user_id = \Yii::$app->user->id;
                                            $modelNotificationUser->notification_id = $modelNotification->id;
                                            $modelNotificationUser->save();
                                        }
                                }
                                
                                
                                
                            }else{
                                        $newModelCreditsaction = new Creditsaction();
                                        $newModelCreditsaction->scenario = 'add_credits';
                                        $newModelCreditsaction->object_id = \Yii::$app->user->id;
                                        $newModelCreditsaction->action = 'join_virtual_class';
                                        $newModelCreditsaction->action_count = 1;
                                        if($newModelCreditsaction->save()){
                                            $modelCreditsactionCount = Creditsactioncount::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                            if($modelCreditsactionCount){
                                                $modelCreditsactionCount->scenario = 'update_credits_count';
                                                $modelCreditsactionCount->credits_count = $modelCreditsactionCount->credits_count + 2;
                                                $modelCreditsactionCount->date_update = date("Y-m-d H:i:s");
                                                if($modelCreditsactionCount->save()){
                                                }
                                            }else{
                                                $newModelCreditsactionCount = new Creditsactioncount();
                                                $newModelCreditsactionCount->scenario = 'add_credits_count';
                                                $newModelCreditsactionCount->user_id = \Yii::$app->user->id;
                                                $newModelCreditsactionCount->credits_count = 2;
                                                if($newModelCreditsactionCount->save()){

                                                }
                                            }
                                        }
                                        $modelCredits = \app\models\Usercredits::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                        if($modelCredits){
                                            $modelCredits->scenario = 'update_credits_count';
                                            $modelCredits->credit_count = $modelCredits->credit_count + 2;
                                            if($modelCredits->save()){
                                            }                            
                                        }else{
                                            $modelNewCredits = new \app\models\Usercredits();
                                            $modelNewCredits->scenario = 'add_credits';
                                            $modelNewCredits->user_id = \Yii::$app->user->id;
                                            $modelNewCredits->credit_count = 2;
                                            $modelNewCredits->many = 0;
                                            if($modelNewCredits->save()){

                                            }
                                        }

                                        $modelNotification = new Notification();
                                        $modelNotification->scenario = 'add_notification';
                                        $modelNotification->module = 'university';
                                        $modelNotification->from_object_id = \Yii::$app->user->id;
                                        $modelNotification->to_object_id = $_GET['class_id'];
                                        $modelNotification->notification_type = 'join_virtual_class_credits';
                                        $modelNotification->university_id = $modelNewVideoconferenceuser->id_university;
                                        if($modelNotification->save()){
                                            $modelNotificationUser = new Notificationusers();
                                            $modelNotificationUser->scenario = 'new_notification';
                                            $modelNotificationUser->user_id = \Yii::$app->user->id;
                                            $modelNotificationUser->notification_id = $modelNotification->id;
                                            $modelNotificationUser->save();
                                        }

                            }
                    //add credits action end
      
      



      $user_many = Usercredits::find()->where(['user_id'=>$_GET['user_paymend']])->one();
      if ($user_many) {
        $user_many->scenario = 'update_many_count';
        $user_many->many = $user_many->many + $transaction->price;
        $user_many->save();
      } else {
        $u_m = new Usercredits();
        $u_m->many=$transaction->price;
        $u_m->user_id = $_GET['user_paymend'];
        $u_m->credit_count = 0;
        $u_m->scenario = 'add_credits';
        $u_m->save();
      }



      Yii::$app->session['rent_ok']=true;

      return $this->redirect('/'.$_GET['redirect']);
    } else {
      return $this->redirect('/'.$_GET['redirect']);
    }
  }

  ////////////////////////////////


    
    public function actionFavoritevirtualclass()
    {
        $queryVideoconference = new Query();
        $queryVideoconference->select(['*', 'id' => '{{%videoconference}}.id', 'class_status' => '{{%videoconference}}.status',  'className' => '{{%videoconference}}.name','userName' => '{{%user}}.name', 'conferenceDateCreate' => '{{%videoconference}}.date_create'])
                        ->from('{{%favorite}}')
                        ->join('LEFT JOIN',
                                        '{{%videoconference}}',
                                        '{{%videoconference}}.id = {{%favorite}}.object_id')
                        ->join('LEFT JOIN',
                                    '{{%user}}',
                                    '{{%user}}.id = {{%videoconference}}.tutors_id')
                         ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%videoconference}}.tutors_id')
                ->where(['{{%favorite}}.user_id' => \Yii::$app->user->id, '{{%favorite}}.type' => 'virtual_class']);
        
        $queryVideoconference->limit(20);
        $queryVideoconference->orderBy([
            '{{%videoconference}}.date_start' => SORT_DESC,
        ]);
        
        $command = $queryVideoconference->createCommand();
        $modelVideoconference = $command->queryAll();
 
        $videoconferences = [];
        foreach($modelVideoconference as $modelConference){
            $date = date_create($modelConference['date_start']);
            $videoconferences[date_format($date, 'd.m.Y')][] = $modelConference;
        }
        
        
        return $this->render('favoritevirtualclass',[
            'videoconferences' => $videoconferences,
        ]);
    }
    
    public function actionInvitevirtualclass()
    {
        if($_POST){
            if(isset($_POST['invite'])){
                if($_POST['invite']['status'] == 'accept'){
                    $modelVideoConference = Videoconferenceuser::find()->where(['conference_id' => $_POST['invite']['conference_id'], 'user_id' => $_POST['invite']['user_id']])->one();
                    $modelVideoConference->scenario = 'add_conference';
                    $modelVideoConference->status = 1;
                    if($modelVideoConference->save()){
                    }
                }else{
                    $modelVideoConference = Videoconferenceuser::find()->where(['conference_id' => $_POST['invite']['conference_id'], 'user_id' => $_POST['invite']['user_id']])->one();
                    if($modelVideoConference->delete()){
                    }
                }
            }
        }
        
        $queryVideoconference = new Query();
        $queryVideoconference->select(['*', 'id' => '{{%videoconference}}.id', 'class_status' => '{{%videoconference}}.status',  'className' => '{{%videoconference}}.name','userName' => '{{%user}}.name', 'conferenceDateCreate' => '{{%videoconference}}.date_create'])
                        ->from('{{%videoconference_users}}')
                        ->join('LEFT JOIN',
                                        '{{%videoconference}}',
                                        '{{%videoconference}}.id = {{%videoconference_users}}.conference_id')
                        ->join('LEFT JOIN',
                                        '{{%user}}',
                                        '{{%user}}.id = {{%videoconference}}.tutors_id')
                        ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%videoconference}}.tutors_id')
                        ->where(['{{%videoconference_users}}.user_id' => \Yii::$app->user->id, '{{%videoconference_users}}.status' => 0]);
        
        $queryVideoconference->limit(20);
        $queryVideoconference->orderBy([
            '{{%videoconference}}.date_start' => SORT_DESC,
        ]);
        
        $command = $queryVideoconference->createCommand();
        $modelVideoconference = $command->queryAll();
 
        $videoconferences = [];
        foreach($modelVideoconference as $modelConference){
            $date = date_create($modelConference['date_start']);
            $videoconferences[date_format($date, 'd.m.Y')][] = $modelConference;
        }
        
        
        
        return $this->render('invitevirtualclass',[
            'videoconferences' => $videoconferences,
        ]);
    }
    
    public function actionArchivedvirtualclass()
    {
        
        $queryVideoconference = new Query();
        $queryVideoconference->select(['*', 'id' => '{{%videoconference}}.id', 'class_status' => '{{%videoconference}}.status',  'className' => '{{%videoconference}}.name','userName' => '{{%user}}.name', 'conferenceDateCreate' => '{{%videoconference}}.date_create'])
                        ->from('{{%videoconference_users}}')
                        ->join('LEFT JOIN',
                                        '{{%videoconference}}',
                                        '{{%videoconference}}.id = {{%videoconference_users}}.conference_id')
                        ->join('LEFT JOIN',
                                        '{{%user}}',
                                        '{{%user}}.id = {{%videoconference}}.tutors_id')
                        ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%videoconference}}.tutors_id')
                        ->where(['{{%videoconference_users}}.user_id' => \Yii::$app->user->id, '{{%videoconference_users}}.status' => 2]);
        
        $queryVideoconference->limit(20);
        $queryVideoconference->orderBy([
            '{{%videoconference}}.date_start' => SORT_DESC,
        ]);
        
        $command = $queryVideoconference->createCommand();
        $modelVideoconference = $command->queryAll();
 
        $videoconferences = [];
        foreach($modelVideoconference as $modelConference){
            $date = date_create($modelConference['date_start']);
            $videoconferences[date_format($date, 'd.m.Y')][] = $modelConference;
        }
        
        
        
        return $this->render('archivedvirtualclass',[
            'videoconferences' => $videoconferences,
        ]);
    }
    
    public function actionFollowconference()
    {
        $followStatus = $_POST['status'];
        $conference_id = $_POST['conference_id'];
        $result = [];
        if($followStatus == 'Follow'){
            $videoConferenceUser = new Videoconferenceuser();
            $videoConferenceUser->scenario = 'add_conference';
            $videoConferenceUser->conference_id = $conference_id;
            $videoConferenceUser->user_id = \Yii::$app->user->id;
            $videoConferenceUser->status = 1;
            if($videoConferenceUser->save()){
                $result['status'] = 'success';
            }else{
                $result['status'] = 'error';
            }
        }else{
            $videoConferenceUser = Videoconferenceuser::find()->where(['conference_id' => $conference_id, 'user_id' => \Yii::$app->user->id])->one();
            if($videoConferenceUser != null){
                if($videoConferenceUser->delete()){
                    $result['status'] = 'success';
                }else{
                    $result['status'] = 'error';
                }
            }
        }
        echo json_encode($result);
    }
    
    public function actionAjaxfavoritevirtualclass()
    {
        $favoriteStatus = $_POST['status'];
        $conference_id = $_POST['conference_id'];
        $result = [];
        if($favoriteStatus == 'Add'){
            $modelFavoriteConference = new Favorite();
            $modelFavoriteConference->scenario = 'add_to_favorite';
            $modelFavoriteConference->object_id = $conference_id;
            $modelFavoriteConference->user_id = \Yii::$app->user->id;
            $modelFavoriteConference->type = 'virtual_class';
            if($modelFavoriteConference->save()){
                $result['status'] = 'success';
            }else{
                $result['status'] = 'error';
            }
            
        }else{
            $modelFavoriteConference = Favorite::find()->where(['object_id' => $conference_id, 'type' => 'virtual_class', 'user_id' => \Yii::$app->user->id])->one();
            if($modelFavoriteConference != null){
                if($modelFavoriteConference->delete()){
                    $result['status'] = 'success';
                }else{
                    $result['status'] = 'error';
                }
            }
        }
        echo json_encode($result);
    }
    
    public function actionConferenceinvite(){
        $user_id = $_POST['user_id'];
        $conference_id = $_POST['conference_id'];
        $modelVideocinference = Videoconferenceuser::find()->where(['conference_id' => $conference_id, 'user_id' => $user_id])->one();
        
        $result = [];
        if($modelVideocinference == null){
            $videoConferenceUser = new Videoconferenceuser();
            $videoConferenceUser->scenario = 'add_conference';
            $videoConferenceUser->conference_id = $conference_id;
            $videoConferenceUser->user_id = $user_id;
            $videoConferenceUser->status = 0;
            if($videoConferenceUser->save()){
                $result['status'] = 'success';
            }else{
                $result['status'] = 'error 1';
            }
        }else{
            $result['status'] = 'error 2';
        }
        echo json_encode($result);
    }
    
    
    public function actionGetclasstopuser()
    {
        $videoconference_id = $_POST['videoconference_id'];
        
        $modelVideoconference = Videoconference::find()->where(['id' => $videoconference_id])->one();
        $query = new Query();
        $query->select(['*', 'id' => '{{%videoconference_users}}.id', 'status' => '{{%videoconference_users}}.id'])
                        ->from('{{%videoconference_users}}')
                        ->join('LEFT JOIN',
                                '{{%user_info}}',
                                '{{%user_info}}.id_user = {{%videoconference_users}}.user_id'
                            )
                        ->join('LEFT JOIN',
                                        '{{%user}}',
                                        '{{%user}}.id = {{%videoconference_users}}.user_id'
                                )->where(['{{%videoconference_users}}.conference_id' => $modelVideoconference->id]);
        $command = $query->createCommand();
        $modelVideoconferenceuser = $command->queryAll();       
        
        $virtualClassUserId = [];
        foreach($modelVideoconferenceuser as $video_conference){
            $virtualClassUserId[] = $video_conference['user_id'];
        }
        
        $userRating = [];
        foreach($virtualClassUserId as $user_id){
            $modelRatingUser = Ratingrequest::find()->where(['to_object_id' => $user_id])->all();
            $countUser = 0;
            $countStar = 0;
            foreach($modelRatingUser as $request){
                $countUser++;
                $countStar = $countStar + $request['rating_count'];
            }
            $result = '';
            if($countUser == 0){
                $countStarResult = 0;
            }else{
                $countStarResult = round($countStar/$countUser,1);
            }
            $userRating[$user_id] =  $countStarResult;
            
        }

        $userArray = [];
        $k = 0;
        foreach($userRating as $user_id => $rating){
            $k = $k + 1;
            if($k < 6){
                $query = new Query();
                $query->select(['*', 'id' => '{{%user}}.id', 'username' => '{{%user}}.name', 'user_role_name' => '{{%user_role}}.name'])
                                ->from('{{%user}}')
                                ->join('LEFT JOIN',
                                            '{{%user_info}}',
                                            '{{%user_info}}.id_user = {{%user}}.id')
                                ->join('LEFT JOIN',
                                            '{{%user_role}}',
                                            '{{%user_role}}.id = {{%user_info}}.academic_status'
                                        )->where(['{{%user}}.id' => $user_id]);
                $command = $query->createCommand();
                $modelUser = $command->queryOne();
                
                $userArray[$user_id]['user_info'] = $modelUser;
                $userArray[$user_id]['user_rating'] = $rating;
            }else{
                break;
            }
        }
            
        echo json_encode($userArray);
        
    }
    
    
    public function actionGetclasstopprofessor()
    {
        $videoconference_id = $_POST['videoconference_id'];
        $modelVideoconference = Videoconference::find()->where(['id' => $videoconference_id])->one();
        $modelVideoconferences = Videoconference::find()->where(['id_course' => $modelVideoconference->id_course])->asArray()->all();
        
        $virtual_class_tutors_id = [];
        foreach($modelVideoconferences as $video_class){
            $virtual_class_tutors_id[] = $video_class['tutors_id'];
        }
        
        $professorRating = [];
        foreach($virtual_class_tutors_id as $id){
            $modelRatingrequest = Ratingrequest::find()->where(['to_object_id' => $id, 'type' => 'virtualclass_professor'])->asArray()->all();
            
            $countUser = 0;
            $countStar = 0;
            foreach($modelRatingrequest as $rating){
                $countUser++;
                $countStar = $countStar + $rating['rating_count'];
            }
            $result = '';
            if($countUser == 0){
                $countStarResult = 0;
            }else{
                $countStarResult = round($countStar/$countUser,1); 
            }
            
            $professorRating[$id] =  $countStarResult;
        }
        arsort($professorRating);
        $k = 0;
        $professorArray = [];
        foreach($professorRating as $professor_id => $rating){
            $k = $k + 1;
            if($k < 6){
                $query = new Query();
                $query->select(['*', 'id' => '{{%user}}.id', 'username' => '{{%user}}.name', 'user_role_name' => '{{%user_role}}.name'])
                                ->from('{{%user}}')
                                ->join('LEFT JOIN',
                                            '{{%user_info}}',
                                            '{{%user_info}}.id_user = {{%user}}.id')
                                ->join('LEFT JOIN',
                                            '{{%user_role}}',
                                            '{{%user_role}}.id = {{%user_info}}.academic_status'
                                        )->where(['{{%user}}.id' => $professor_id]);
                $command = $query->createCommand();
                $modelUser = $command->queryOne();
        
                $professorArray[$professor_id]['professor_info'] = $modelUser;
                $professorArray[$professor_id]['professor_rating'] = $rating;
            }else{
                break;
            }
//            ksort($professorArray);
        }
        echo json_encode($professorArray);
    }
    
    public function actionGetclasstop()
    {
        $result = [];
        $videoconference_id = $_POST['videoconference_id'];
        $modelVideoconference = Videoconference::find()->where(['id' => $videoconference_id])->one();
        $modelVideoconferences = Videoconference::find()->where(['id_course' => $modelVideoconference->id_course])->asArray()->all();
        $count_entry = 0;
        $count_star = 0;
        $classRatingArray = [];
        $class_count = 0;
        foreach($modelVideoconferences as $class){
            $class_count++;
            $modelRatingClass = Virtualclassreting::find()->where(['class_id' => $class['id']])->all();
            $countUser = 0;
            $countStar = 0;
            foreach($modelRatingClass as $request){
                $countUser++;
                $countStar = $countStar + $request->rate_professor;
            }
            $result = '';
            if($countUser == 0){
                $countStarClassResult = 0;
            }else{
                $countStarClassResult = round($countStar/$countUser,1); 
            }
            $classRatingArray[$class['id']] = $countStarClassResult;
        }
        
        arsort($classRatingArray);
        $k = 0;
        $classArray = [];
        foreach($classRatingArray as $class_id => $rating){
            $k = $k + 1;
            if($k < 6){
                $modelVideoconference = Videoconference::find()->where(['id' => $class_id])->asArray()->one();
        
                $classArray[$class_id]['class_info'] = $modelVideoconference;
                $classArray[$class_id]['class_rating'] = $rating;
            }else{
                break;
            }
        }
        
        echo json_encode($classArray);
    }
    
    public function actionGetvirtualclassinfo()
    {
        $conference_id = $_POST['conference_id'];
        $modalConference = Videoconference::find()->where(['id' => $conference_id])->asArray()->one();
        $result = [];
        if($modalConference){
            $result['status'] = 'success';
            $query = new Query();
            $query->select(['*', 'id' => '{{%user}}.id', 'username' => '{{%user}}.name'])
                            ->from('{{%user}}')
                            ->join('LEFT JOIN',
                                        '{{%user_info}}',
                                        '{{%user_info}}.id_user = {{%user}}.id'
                                    )->where(['{{%user}}.id' => $modalConference['tutors_id']]);
            $command = $query->createCommand();
            $modelUser = $command->queryOne();
            $result['tutors_info'] = $modelUser;
            $result['conference'] = $modalConference;
        }else{
            $result['status'] = 'error';
        }
                
        echo json_encode($result);
    }
    
}
