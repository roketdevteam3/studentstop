<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/21/15
 * Time: 8:40 PM
 */

namespace app\rbac;

use Yii;
use yii\rbac\Rule;

class IsChildHasPermissionRule extends Rule
{
    public $name = 'isChildHasPermissionRule';

    public function execute($user, $item, $params)
    {
        if (!Yii::$app->user->isGuest) {
            $auth = Yii::$app->authManager;
//            $role = $auth->getRolesByUser($user);
//            if(count($role)>0){
//                foreach($role as $k => $item){
//                  var_dump($auth->checkAccess(2,$item->name));
//
//                }
//            }

            $check = $auth->checkAccess($user,$item->name);
          //  print_r($auth->getRolesByUser($user));exit;
            return (!is_null($auth->getAssignment($item->name, $user))) ? true : false;
        }

        return false;
    }

}