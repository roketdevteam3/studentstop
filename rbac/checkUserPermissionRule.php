<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/21/15
 * Time: 8:40 PM
 */

namespace app\rbac;

use Yii;
use yii\rbac\Rule;

class checkUserPermissionRule extends Rule
{
    public $name = 'checkUserPermission';

    public function execute($user, $item, $params)
    {

        $chek = false;


        if (!Yii::$app->user->isGuest) {
            $auth = Yii::$app->authManager;
            $roles = $auth->getRolesByUser($user);

            if (isset($roles)) {

                foreach ($roles as $role) {
                    if ($auth->hasChild($role, $item)) {
                        $chek = true;
                    }
                }
                if($chek)
                    return true;
            }
            return (!is_null($auth->getAssignment($item->name, $user))) ? true : false;
        }else{

            $auth = Yii::$app->authManager;
            $role = $auth->getRole('guest');
     
            if ($auth->hasChild($role, $item)) {

                 return true;
            }else{
                return (!is_null($auth->getAssignment($item->name, 0))) ? true : false;
            }
        }
    }


}