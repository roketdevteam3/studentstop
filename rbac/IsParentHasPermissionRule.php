<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/21/15
 * Time: 8:40 PM
 */

namespace app\rbac;

use Yii;
use yii\rbac\Rule;

class IsParentHasPermissionRule extends Rule
{
    public $name = 'IsParentHasPermissionRule';

    public function execute($user, $item, $params)
    {
        if (!Yii::$app->user->isGuest) {
            $auth = Yii::$app->authManager;
            return (!is_null($auth->getAssignment($item->name, $user))) ? true : false;
        }

        return false;
    }

}