<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 7/1/15
 * Time: 10:15 PM
 */

namespace app\rbac;

use Yii;
use yii\rbac\Rule;
use yii\rbac\Item;

class UserProfileOwnerRule extends Rule
{
    public $name = 'isProfileOwner';

    /**
     * @param string|integer $user   the user ID.
     * @param Item           $item   the role or permission that this rule is associated with
     * @param array          $params parameters passed to ManagerInterface::checkAccess().
     *
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        if (!Yii::$app->user->isGuest) {
            $auth = Yii::$app->authManager;
            return (!is_null($auth->getAssignment($item->name, $user))) ? true : false;
        }

        return false;
    }
}
