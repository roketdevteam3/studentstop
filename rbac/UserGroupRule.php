<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 6/30/15
 * Time: 11:35 PM
 */

namespace app\rbac;

use Yii;
use yii\rbac\Rule;

class UserGroupRule extends Rule
{
    public $name = 'userGroup';

    public function execute($user, $item, $params)
    {
        if (!\Yii::$app->user->isGuest) {
            $group = \Yii::$app->user->identity->group;
            if ($item->name === 'admin') {
                return $group == 'admin';
            } elseif ($item->name === 'user') {
                return $group == 'admin' || $group == 'user';
            } elseif ($item->name === 'moderator') {
                return $group == 'admin' || $group == 'moderator';
            }
        }
        return true;
    }
}