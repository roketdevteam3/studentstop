<?php
namespace app\components;

/**
 * Base active query class for models
 * @package yii\easyii\components
 */
class ActiveQuery extends \yii\db\ActiveQuery
{
    /**
     * Apply condition by status
     * @param $status
     * @return $this
     */
    public function status($status)
    {
        $this->andWhere(['status' => (int)$status]);
        return $this;
    }

    /**
     * Order by primary key DESC
     * @return $this
     */
    public function desc()
    {
        $model = $this->modelClass;
        $this->orderBy([$model::primaryKey()[0] => SORT_DESC]);
        return $this;
    }

    /**
     * Order by primary key ASC
     * @return $this
     */
    public function asc()
    {
        $model = $this->modelClass;
        $this->orderBy([$model::primaryKey()[0] => SORT_ASC]);
        return $this;
    }

    /**
     * Order by order_num
     * @return $this
     */
    public function sort()
    {
        $this->orderBy(['order_num' => SORT_DESC]);
        return $this;
    }

    /**
     * Order by order_num
     * @return $this
     */
    public function drag_sort()
    {
        $this->orderBy(['order_num' => SORT_ASC]);
        return $this;
    }

    /**
     * Order by date
     * @return $this
     */
    public function sortDate()
    {
        $this->orderBy(['date_create' => SORT_DESC]);
        return $this;
    }

    /**
     * Order by order_num
     * @return $this
     */
    public function lang()
    {
        $this->where(['lang_key' => \Yii::$app->language]);
        return $this;
    }

    public static function enumItem($model,$attribute)
    {
        $attr=$attribute;
       // self::resolveName($model,$attr);
        preg_match('/\((.*)\)/',$model->tableSchema->columns[$attr]->dbType,$matches);
        foreach(explode(',', $matches[1]) as $value)
        {
            $value=str_replace("'",null,$value);
            $values[$value]=\Yii::t('app',$value);
        }

        return $values;
    }

}