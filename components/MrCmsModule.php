<?php

namespace app\components;

use yii\base\Module;

class MrCmsModule extends Module
{
    const CACHE_KEY_THEME = 'setting_site_theme';

    public function init()
    {
        $theme = \Yii::$app->cache->get(self::CACHE_KEY_THEME) ? \Yii::$app->cache->get(self::CACHE_KEY_THEME) : 'basic';
        $this->viewPath = $this->viewPath.'/'.$theme;
        parent::init();
        // custom initialization code goes here
    }


}
