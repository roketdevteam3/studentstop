<?php
namespace app\components;

use app\models\Auth;
use app\modules\users\models\User;
use Yii;
use yii\authclient\ClientInterface;
use yii\helpers\ArrayHelper;

/**
 * AuthHandler handles successful authentification via Yii auth component
 */
class AuthHandler
{
    /**
     * @var ClientInterface
     */
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function handle()
    {
        $attributes = $this->client->getUserAttributes();

        switch($this->client->getId()){
            case 'facebook' :
                $data = $this->getFacebookData();
                break;
            case 'twitter' :
                $data = $this->getTwitterData();
                break;
            case 'linkedin' :
                $data = $this->getLinkedData();
                break;
            default:
                $data = [
                    'email' => '',
                    'name' => '',
                    'surname' => '',
                    'id' => ''
                ];
        }

        /** @var Auth $auth */
        $auth = Auth::find()->where([
            'source' => $this->client->getId(),
            'source_id' => $data['id'],
        ])->one();

        if (Yii::$app->user->isGuest) {
            if ($auth) { // login
                /** @var User $user */
                $user = $auth->user;
                Yii::$app->user->login($user);
            } else { // signup


                if ($data['email'] !== null && User::find()->where(['email' => $data['email']])->exists()) {
                    Yii::$app->getSession()->setFlash('error', [
                        Yii::t('app', "User with the same email as in {client} account already exists but isn't linked to it. Login using email first to link it.", ['client' => $this->client->getTitle()]),
                    ]);
                } else {


                    $password = Yii::$app->security->generateRandomString(6);
                    $user = new User([
                        'name' => $data['name'],
                        'surname' => $data['surname'],
                        'email' => $data['email'],
                        'password' => $password,
                    ]);
                    $user->generateAuthKey();
                    $user->generatePasswordResetToken();

                    $transaction = User::getDb()->beginTransaction();

                    if ($user->save()) {
                        $auth = new Auth([
                            'user_id' => $user->id,
                            'source' => $this->client->getId(),
                            'source_id' => (string)$data['id'],
                        ]);
                        if ($auth->save()) {
                            $transaction->commit();
                            Yii::$app->user->login($user);
                        } else {
                            Yii::$app->getSession()->setFlash('error', [
                                Yii::t('app', 'Unable to save {client} account: {errors}', [
                                    'client' => $this->client->getTitle(),
                                    'errors' => json_encode($auth->getErrors()),
                                ]),
                            ]);
                        }
                    } else {
                        Yii::$app->getSession()->setFlash('error', [
                            Yii::t('app', 'Unable to save user: {errors}', [
                                'client' => $this->client->getTitle(),
                                'errors' => json_encode($user->getErrors()),
                            ]),
                        ]);
                    }
                }
            }
        } else { // user already logged in
            if (!$auth) { // add auth provider
                $auth = new Auth([
                    'user_id' => Yii::$app->user->id,
                    'source' => $this->client->getId(),
                    'source_id' => (string)$attributes['id'],
                ]);
                if ($auth->save()) {
                    /** @var User $user */
                    $user = $auth->user;
                    Yii::$app->getSession()->setFlash('success', [
                        Yii::t('app', 'Linked {client} account.', [
                            'client' => $this->client->getTitle()
                        ]),
                    ]);
                } else {
                    Yii::$app->getSession()->setFlash('error', [
                        Yii::t('app', 'Unable to link {client} account: {errors}', [
                            'client' => $this->client->getTitle(),
                            'errors' => json_encode($auth->getErrors()),
                        ]),
                    ]);
                }
            } else { // there's existing auth
                Yii::$app->getSession()->setFlash('error', [
                    Yii::t('app',
                        'Unable to link {client} account. There is another user using it.',
                        ['client' => $this->client->getTitle()]),
                ]);
            }
        }
    }

    private function ifUniqueEmail($email)
    {
        return $email !== null && User::find()->where(['email' => $email])->exists();
    }

    private function getFacebookData()
    {
        $attributes = $this->client->getUserAttributes();

        return[
            'name' => explode(' ', $attributes['name'])[0],
            'surname' => explode(' ', $attributes['name'])[1],
            'email' => $attributes['email'],
            'id' => $attributes['id']
        ];
    }

    private function getTwitterData()
    {
        $attributes = $this->client->getUserAttributes();

        return[
            'name' => explode(' ', $attributes['name'])[0],
            'surname' => explode(' ', $attributes['name'])[1],
            'email' => '',
            'id' => $attributes['id']
        ];
    }

    private function getLinkedData()
    {
        $attributes = $this->client->getUserAttributes();

        return[
            'name' => $attributes['first-name'],
            'surname' => $attributes['last-name'],
            'email' => $attributes['email-address'],
            'id' => $attributes['id']
        ];
    }

}