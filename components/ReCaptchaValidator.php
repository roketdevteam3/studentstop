<?php

namespace app\components;

use \Yii;

/**
 * ReCaptcha widget validator.
 *
 * @author HimikLab
 * @package himiklab\yii2\recaptcha
 */
class ReCaptchaValidator extends \himiklab\yii2\recaptcha\ReCaptchaValidator
{

    /**
     * @param string $value
     * @return array|null
     * @throws Exception
     */
    protected function validateValue($value)
    {
        if(!Yii::$app->request->isAjax && Yii::$app->request->post(self::CAPTCHA_RESPONSE_FIELD)){
            return;
        }

        return parent::validateValue($value);
    }

}
