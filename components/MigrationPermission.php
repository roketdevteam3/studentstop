<?php
/**
 * Created by PhpStorm.
 * User: mackrais
 * Date: 22.02.16
 * Time: 16:13
 * Email: mackraiscms@gmail.com
 * Site: http://mackrais.tk
 */
namespace app\components;

use app\rbac\checkUserPermissionRule;
use yii\db\Schema;
use yii\db\Migration;


class MigrationPermission extends Migration
{
    protected $permissions = [];
    protected $checkUserPermissionRule;

    /**
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    protected function getAuthManager()
    {
        if(!isset($this->checkUserPermissionRule) && empty($this->checkUserPermissionRule)){
            $this->checkUserPermissionRule = new checkUserPermissionRule();
        }

        $authManager = \Yii::$app->getAuthManager();
        if ($authManager instanceof \yii\rbac\DbManager) {
            if ( $this->checkUserPermissionRule instanceof \yii\rbac\Rule) {

            } else {
                throw new \yii\base\InvalidConfigException('You should configure "authManager or $checkUserPermissionRule" component to use database before executing this migration.');
            }
        } else {
            throw new \yii\base\InvalidConfigException('You should configure "authManager" component to use database before executing this migration.');
        }
        return $authManager;
    }

    public function up()
    {
        $authManager = $this->getAuthManager();

        foreach ($this->permissions as $permissionName => $roles) {
            if (!$authManager->getPermission($permissionName)) {
                $permission = $authManager->createPermission($permissionName);
                $permission->ruleName = $this->checkUserPermissionRule->name;
                $authManager->add($permission);
            } else {
                $permission = $authManager->getPermission($permissionName);
            }
            foreach ($roles as $role) {
                if (!$authManager->hasChild($authManager->getRole($role), $permission)) {
                    $authManager->addChild($authManager->getRole($role), $permission);
                }
            }
        }
    }

    public function down()
    {
        $authManager = $this->getAuthManager();

        foreach ($this->permissions as $permissionName => $roles) {
            if ($permission = $authManager->getPermission($permissionName)) {
                foreach ($roles as $role) {
                    if ($authManager->getRole($role) && $permission) {
                        $authManager->removeChild($authManager->getRole($role), $permission);
                    }
                }
                $authManager->remove($permission);
            }
        }
    }
}