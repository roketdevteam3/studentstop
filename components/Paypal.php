<?php
/**
 * @author    Alexander Vizhanov <lembadm@gmail.com>
 * @author    Tymkіv Roman <trymod@gmail.com>
 * @copyright 2015 Astwell Soft <astwellsoft.com>
 */

namespace app\components;

use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Exception\PayPalConnectionException;
use PayPal\Rest\ApiContext;
use RuntimeException;
use yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\helpers\FileHelper;
use yii\helpers\Url;

/**
 * Class Paypal
 *
 * @property ApiContext $apiContext
 */
class Paypal extends Component
{
    /**
     * Mode development
     */
    const MODE_SANDBOX = 'sandbox';

    /**
     * Mode production
     */
    const MODE_LIVE = 'live';

    /**
     * Logging level can be one of FINE or DEBUG.
     */
    const LOG_LEVEL_FINE = 'FINE';
    const LOG_LEVEL_DEBUG = 'DEBUG';

    public $mode = self::MODE_SANDBOX;

    public $clientId;
    public $clientSecret;

    public $returnUrl;
    public $cancelUrl;

    public $logPath = '@runtime/logs/paypal.log';

    /**
     * @var ApiContext
     */
    private $apiContextInstance;

    public function init()
    {
        if (!$this->clientId || !$this->clientSecret) {
            throw new InvalidConfigException('clientId or clientSecret not set');
        }

        if (!$this->returnUrl || !$this->cancelUrl) {
            throw new InvalidConfigException('returnUrl or cancelUrl not set');
        }

        $this->prepareLogFile();
    }

    /**
     * Create a payment_id using the buyer's paypal account as the funding instrument.
     * Your app will have to redirect the buyer to the paypal website, obtain their
     * consent to the payment_id and subsequently execute the payment_id using the execute API call.
     *
     * @param integer $orderId  ID in Orders table
     * @param float   $amount   payment_id amount in DDD.DD format
     * @param string  $details  A description about the payment_id
     * @param string  $currency 3 letter ISO currency code such as 'USD'
     *
     * @return \PayPal\Api\Payment
     */
    public function createPayment($orderId, $amount, $details, $currency = 'USD')
    {
        $payer = (new Payer())->setPaymentMethod('paypal');

        $item = (new Item())
            ->setName($details)
            ->setCurrency($currency)
            ->setQuantity(1)
            ->setPrice($amount);

        $itemList = (new ItemList())
            ->addItem($item);

        $amount = (new Amount())
            ->setCurrency($currency)
            ->setTotal($amount);

        $transaction = (new Transaction())
            ->setItemList($itemList)
            ->setAmount($amount)
            ->setDescription($details);

        $redirectUrls = (new RedirectUrls())
            ->setReturnUrl(Url::to([$this->returnUrl, 'orderId' => $orderId], true))
            ->setCancelUrl(Url::to([$this->cancelUrl, 'orderId' => $orderId], true));

        try {
            return (new Payment())
                ->setRedirectUrls($redirectUrls)
                ->setIntent('sale')
                ->setPayer($payer)
                ->setTransactions([$transaction])
                ->create($this->getApiContext());
        } catch (PayPalConnectionException $e) {
            throw new RuntimeException(sprintf("ERROR: %s %s", $e->getMessage(), $e->getData()));
        }
    }

    /**
     * @param string $paymentId
     * @param string $payerId
     *
     * @return Payment
     */
    public function execute($paymentId, $payerId) {
        $execution = (new PaymentExecution())
            ->setPayerId($payerId);

        try {
            return Payment::get($paymentId, $this->getApiContext())
                ->execute($execution, $this->getApiContext());
        } catch (PayPalConnectionException $e) {
            throw new RuntimeException($e->getData());
        }
    }

    protected function getApiContext()
    {
        if (!$this->apiContextInstance) {

            $this->apiContextInstance = new ApiContext(
                new OAuthTokenCredential(
                    $this->clientId,
                    $this->clientSecret
                )
            );

            $this->apiContextInstance->setConfig([
                'mode'                   => $this->mode,
                'http.ConnectionTimeOut' => 30,
                'http.Retry'             => 1,
                'log.LogEnabled'         => true,
                'log.FileName'           => $this->logPath,
                'log.LogLevel'           => YII_DEBUG ? self::LOG_LEVEL_DEBUG : self::LOG_LEVEL_FINE,
                'validation.level'       => 'log',
                'cache.enabled'          => 'true'
            ]);
        }

        return $this->apiContextInstance;
    }

    protected function prepareLogFile()
    {
        $this->logPath = Yii::getAlias($this->logPath);

        $logDir = dirname($this->logPath);

        if (!is_dir($logDir)) {
            FileHelper::createDirectory($logDir, 0775, true);
        }
    }
}
