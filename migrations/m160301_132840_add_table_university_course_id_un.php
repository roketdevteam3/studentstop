<?php

use yii\db\Migration;
use yii\db\Schema;

class m160301_132840_add_table_university_course_id_un extends Migration
{
    public function up()
    {
        $this->addColumn(
            '{{%university_course}}',
            'id_university',
            Schema::TYPE_INTEGER
        );
    }

    public function down()
    {
        $this->dropColumn("{{%university_course}}", "id_university");
    }
}
