<?php

use yii\db\Migration;
use yii\db\Schema;

class m160224_195318_tbl_page extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%pages}}', [
            'id' => Schema::TYPE_PK,
            'parent_id' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT "0"',
            'title' => Schema::TYPE_STRING . '(255) NOT NULL',
            'slug' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'content' => Schema::TYPE_TEXT . ' DEFAULT NULL',

            'meta_t' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'meta_k' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'meta_d' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'seo_text' => Schema::TYPE_TEXT . ' DEFAULT NULL',
            'order_num' => Schema::TYPE_INTEGER,
            'date_create' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT '0000-00-00 00:00:00' ",
            'date_update' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'status' => Schema::TYPE_BOOLEAN . " DEFAULT '1'"
        ], $tableOptions);
        $this->createIndex('slug', '{{%pages}}', 'slug', true);
    }

    public function Down()
    {
        $this->dropTable('{{%pages}}');
    }
}
