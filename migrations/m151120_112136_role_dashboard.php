<?php

use app\rbac\checkUserPermissionRule;
use yii\db\Schema;
use yii\db\Migration;

class m151120_112136_role_dashboard extends Migration
{
    public function up()
    {
        $authManager = \Yii::$app->authManager;
        // Get roles
        $admin  = $authManager->getRole('admin');

        // Add rule, based on UserExt->group === $user->group
        $checkUserPermissionRule = new checkUserPermissionRule();

        $index  = $authManager->createPermission('administration/dashboard/index');

        $index->ruleName = $checkUserPermissionRule->name;
        // Add permissions in Yii::$app->authManager
        $authManager->add($index);


        //add permission user role
        $authManager->addChild($admin,$index);
    }

    public function down()
    {
        echo "m151120_112136_role_dashboard cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
