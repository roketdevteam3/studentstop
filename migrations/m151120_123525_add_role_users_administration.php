<?php

use app\rbac\checkUserPermissionRule;
use yii\db\Schema;
use yii\db\Migration;

class m151120_123525_add_role_users_administration extends Migration
{
    public function up()
    {
        $authManager = \Yii::$app->authManager;
        // Get roles
        $admin  = $authManager->getRole('admin');

        // Add rule, based on UserExt->group === $user->group
        $checkUserPermissionRule = new checkUserPermissionRule();

        $index  = $authManager->createPermission('administration/user/index');
        $index->ruleName = $checkUserPermissionRule->name;

        $view = $authManager->createPermission('administration/user/view');
        $view->ruleName = $checkUserPermissionRule->name;

        $create = $authManager->createPermission('administration/user/create');
        $create->ruleName = $checkUserPermissionRule->name;

        $update = $authManager->createPermission('administration/user/update');
        $update->ruleName = $checkUserPermissionRule->name;

        $delete = $authManager->createPermission('administration/user/delete');
        $delete->ruleName = $checkUserPermissionRule->name;

        // Add permissions in Yii::$app->authManager
        $authManager->add($index);
        $authManager->add($view);
        $authManager->add($create);
        $authManager->add($update);
        $authManager->add($delete);

        //add permission user role
        $authManager->addChild($admin,$index);
        $authManager->addChild($admin,$view);
        $authManager->addChild($admin,$create);
        $authManager->addChild($admin,$update);
        $authManager->addChild($admin,$delete);

    }

    public function down()
    {
        echo "m151029_210018_create_default_roles cannot be reverted.\n";

        return true;
    }
}
