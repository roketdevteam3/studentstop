<?php

use yii\db\Migration;
use yii\db\Schema;

class m160221_200148_tbl_major_course extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%major2course}}', [
            'id' => Schema::TYPE_PK,
            'id_major' => Schema::TYPE_INTEGER,
            'id_course' => Schema::TYPE_INTEGER,
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%major2university}}');
    }

}
