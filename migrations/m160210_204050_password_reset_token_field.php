<?php

use yii\db\Migration;
use yii\db\Schema;

class m160210_204050_password_reset_token_field extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'password_reset_token',  Schema::TYPE_STRING . ' NOT NULL');
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'password_reset_token');
    }

}
