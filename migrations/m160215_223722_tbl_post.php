<?php

use yii\db\Migration;
use yii\db\Schema;

class m160215_223722_tbl_post extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%news}}', [
            'id' => Schema::TYPE_PK,
            'place_type' => Schema::TYPE_STRING . '(255) NOT NULL',
            'place_id' => Schema::TYPE_INTEGER,
            'owner_id' => Schema::TYPE_INTEGER,
            'title' => Schema::TYPE_STRING . '(255) NOT NULL',
            'image' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'slug'=>Schema::TYPE_STRING . '(255) NOT NULL',
            'date_publication' => Schema::TYPE_DATE . " NOT NULL DEFAULT '0000-00-00' ",
            'short_desc' => Schema::TYPE_TEXT . ' DEFAULT NULL',
            'content' => Schema::TYPE_TEXT . ' DEFAULT NULL',
            'date_create' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT '0000-00-00 00:00:00' ",
            'date_update' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'status' => Schema::TYPE_BOOLEAN . " DEFAULT '1'"
        ], $tableOptions);
        $this->createIndex('slug', '{{%news}}', 'slug', true);
    }

    public function Down()
    {
        $this->dropTable('{{%news}}');
    }
}
