<?php

use yii\db\Migration;
use yii\db\Schema;

class m160315_010758_add_class_chat_tbl extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%class_chat}}', [
            'id' => Schema::TYPE_PK,
            'id_class' => Schema::TYPE_INTEGER,
            'id_user' => Schema::TYPE_INTEGER,
            'message'=> Schema::TYPE_TEXT,
            'type'=>Schema::TYPE_INTEGER,
            'data_create' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT '0000-00-00 00:00:00' "
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%class_chat}}');
    }


}
