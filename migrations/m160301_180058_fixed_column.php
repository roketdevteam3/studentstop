<?php

use yii\db\Migration;
use yii\db\Schema;

class m160301_180058_fixed_column extends Migration
{
    public function up()
    {
        $this->addColumn(
            '{{%university_course}}',
            'creator',
            Schema::TYPE_INTEGER
        );

        $this->addColumn(
            '{{%university_major}}',
            'creator',
            Schema::TYPE_INTEGER
        );

        $this->addColumn(
            '{{%university_course_with_major}}',
            'creator',
            Schema::TYPE_INTEGER
        );

        $this->dropColumn("{{%university_course}}", "id_major");

    }

    public function down()
    {
        echo "m160301_180058_fixed_column cannot be reverted.\n";

        return true;
    }

}
