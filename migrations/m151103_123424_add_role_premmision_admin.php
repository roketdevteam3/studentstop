<?php

use app\rbac\checkUserPermissionRule;
use yii\db\Schema;
use yii\db\Migration;

class m151103_123424_add_role_premmision_admin extends Migration
{
    public function up()
    {
        $authManager = \Yii::$app->authManager;
        // Get roles
        $admin  = $authManager->getRole('admin');

        // Add rule, based on UserExt->group === $user->group
        $checkUserPermissionRule = new checkUserPermissionRule();


        $assignment_index  = $authManager->createPermission('administration/accesses/assignment/index');
        $assignment_index->ruleName = $checkUserPermissionRule->name;

        $assignment  = $authManager->createPermission('administration/accesses/assignment/assignment');
        $assignment->ruleName = $checkUserPermissionRule->name;

        $role_index = $authManager->createPermission('administration/accesses/role/index');
        $role_index->ruleName = $checkUserPermissionRule->name;

        $role_view = $authManager->createPermission('administration/accesses/role/view');
        $role_view->ruleName = $checkUserPermissionRule->name;

        $role_create = $authManager->createPermission('administration/accesses/role/create');
        $role_create->ruleName = $checkUserPermissionRule->name;

        $role_update = $authManager->createPermission('administration/accesses/role/update');
        $role_update->ruleName = $checkUserPermissionRule->name;

        $role_delete = $authManager->createPermission('administration/accesses/role/delete');
        $role_delete->ruleName = $checkUserPermissionRule->name;

        $permission_index = $authManager->createPermission('administration/accesses/permission/index');
        $permission_index->ruleName = $checkUserPermissionRule->name;

        $permission_view = $authManager->createPermission('administration/accesses/permission/view');
        $permission_view->ruleName = $checkUserPermissionRule->name;

        $permission_create = $authManager->createPermission('administration/accesses/permission/create');
        $permission_create->ruleName = $checkUserPermissionRule->name;

        $permission_update = $authManager->createPermission('administration/accesses/permission/update');
        $permission_update->ruleName = $checkUserPermissionRule->name;

        $permission_delete = $authManager->createPermission('administration/accesses/permission/delete');
        $permission_delete->ruleName = $checkUserPermissionRule->name;

        $rule_index = $authManager->createPermission('administration/accesses/rule/index');
        $rule_index->ruleName = $checkUserPermissionRule->name;

        $rule_view = $authManager->createPermission('administration/accesses/rule/view');
        $rule_view->ruleName = $checkUserPermissionRule->name;

        $rule_create = $authManager->createPermission('administration/accesses/rule/create');
        $rule_create->ruleName = $checkUserPermissionRule->name;

        $rule_update = $authManager->createPermission('administration/accesses/rule/update');
        $rule_update->ruleName = $checkUserPermissionRule->name;

        $rule_delete = $authManager->createPermission('administration/accesses/rule/delete');
        $rule_delete->ruleName = $checkUserPermissionRule->name;



        // Add permissions in Yii::$app->authManager
        $authManager->add($assignment_index);
        $authManager->add($assignment);
        $authManager->add($role_index);
        $authManager->add($role_view);
        $authManager->add($role_create);
        $authManager->add($role_update);
        $authManager->add($role_delete);
        $authManager->add($permission_index);
        $authManager->add($permission_view);
        $authManager->add($permission_create);
        $authManager->add($permission_update);
        $authManager->add($permission_delete);
        $authManager->add($rule_index);
        $authManager->add($rule_view);
        $authManager->add($rule_create);
        $authManager->add($rule_update);
        $authManager->add($rule_delete);


        //add permission user role
        $authManager->addChild($admin,$assignment_index);
        $authManager->addChild($admin,$assignment);
        $authManager->addChild($admin,$role_index);
        $authManager->addChild($admin,$role_view);
        $authManager->addChild($admin,$role_create);
        $authManager->addChild($admin,$role_update);
        $authManager->addChild($admin,$role_delete);
        $authManager->addChild($admin,$permission_index);
        $authManager->addChild($admin,$permission_view);
        $authManager->addChild($admin,$permission_create);
        $authManager->addChild($admin,$permission_update);
        $authManager->addChild($admin,$permission_delete);
        $authManager->addChild($admin,$rule_index);
        $authManager->addChild($admin,$rule_view);
        $authManager->addChild($admin,$rule_create);
        $authManager->addChild($admin,$rule_update);
        $authManager->addChild($admin,$rule_delete);

    }

    public function down()
    {
        echo "m151029_210018_create_default_roles cannot be reverted.\n";

        return true;
    }
}
