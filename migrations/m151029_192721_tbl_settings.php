<?php

use yii\db\Schema;
use yii\db\Migration;

class m151029_192721_tbl_settings extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%setting}}', [
            'id' => Schema::TYPE_PK,
            'key' => Schema::TYPE_STRING . ' NOT NULL',
            'value' => Schema::TYPE_STRING . ' NOT NULL',
            'category' => Schema::TYPE_STRING . ' NOT NULL',
            'type_input' => Schema::TYPE_STRING . ' NOT NULL'
        ], $tableOptions);
        $this->createIndex('key', '{{%setting}}', 'key', true);
    }

    public function down()
    {
        $this->dropTable('{{%setting}}');
    }

}
