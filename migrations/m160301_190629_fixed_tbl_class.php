<?php

use yii\db\Migration;
use yii\db\Schema;

class m160301_190629_fixed_tbl_class extends Migration
{
    public function up()
    {
        $this->addColumn(
            '{{%class}}',
            'creator',
            Schema::TYPE_INTEGER
        );
        $this->addColumn(
            '{{%class}}',
            'professor',
            Schema::TYPE_INTEGER
        );

        $this->addColumn(
            '{{%class}}',
            'id_major',
            Schema::TYPE_INTEGER
        );

        $this->addColumn(
            '{{%class}}',
            'id_university',
            Schema::TYPE_INTEGER
        );
    }

    public function down()
    {
        echo "m160301_190629_fixed_tbl_class cannot be reverted.\n";

        return false;
    }


}
