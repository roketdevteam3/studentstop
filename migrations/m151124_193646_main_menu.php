<?php

use yii\db\Schema;
use yii\db\Migration;

class m151124_193646_main_menu extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%main_menu_item}}', [
            'id' => Schema::TYPE_PK,
            'link' => Schema::TYPE_STRING,
            'title' => Schema::TYPE_STRING,
            'icon_class' => Schema::TYPE_STRING,
            'order_num'=> Schema::TYPE_INTEGER,
            'absolute' => Schema::TYPE_BOOLEAN . " DEFAULT '1'",
            'status' => Schema::TYPE_BOOLEAN . " DEFAULT '1'"
        ], $tableOptions);
    }

    public function down()
    {
        echo "m151124_193646_main_menu cannot be reverted.\n";

        return false;
    }

}
