<?php

use yii\db\Migration;
use yii\db\Schema;

class m160202_224403_tbl_university_manager extends Migration
{
    public function up()
    {
        $tableOptions = null;
        $this->createTable('{{%university_manager}}', [
            'id' => Schema::TYPE_PK,
            'id_user' => Schema::TYPE_INTEGER,
            'id_university' => Schema::TYPE_INTEGER,
            'role' => Schema::TYPE_INTEGER,
            'date_create' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT '0000-00-00 00:00:00' ",
            'date_update' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ], $tableOptions);

        $this->addForeignKey(
            '{{%manager_user}}',
            '{{%university_manager}}',
            'id_user',
            '{{%user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            '{{%manager_university}}',
            '{{%university_manager}}',
            'id_university',
            '{{%university}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey("{{%manager_user}}", "{{%university_manager");
        $this->dropForeignKey("{{%manager_university}}", "{{%university_manager");
        $this->dropTable("{{%university_manager}}");
    }

}
