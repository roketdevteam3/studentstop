<?php

use app\rbac\checkUserPermissionRule;
use yii\db\Schema;
use yii\db\Migration;

class m160205_210022_role_university_actions extends Migration
{
    public function up()
    {
        $authManager = \Yii::$app->authManager;
        // Get roles
        $admin  = $authManager->getRole('admin');

        // Add rule, based on UserExt->group === $user->group
        $checkUserPermissionRule = new checkUserPermissionRule();

        $index  = $authManager->createPermission('administration/university/index');
        $index->ruleName = $checkUserPermissionRule->name;

        $view = $authManager->createPermission('administration/university/view');
        $view->ruleName = $checkUserPermissionRule->name;

        $create = $authManager->createPermission('administration/university/create');
        $create->ruleName = $checkUserPermissionRule->name;

        $update = $authManager->createPermission('administration/university/update');
        $update->ruleName = $checkUserPermissionRule->name;

        $delete = $authManager->createPermission('administration/university/delete');
        $delete->ruleName = $checkUserPermissionRule->name;

        // Add permissions in Yii::$app->authManager
        $authManager->add($index);
        $authManager->add($view);
        $authManager->add($create);
        $authManager->add($update);
        $authManager->add($delete);

        //add permission user role
        $authManager->addChild($admin,$index);
        $authManager->addChild($admin,$view);
        $authManager->addChild($admin,$create);
        $authManager->addChild($admin,$update);
        $authManager->addChild($admin,$delete);
    }

    public function down()
    {
        echo "m160205_210022_role_university_actions cannot be reverted.\n";

        return false;
    }
}
