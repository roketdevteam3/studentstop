<?php

use yii\db\Schema;
use yii\db\Migration;

class m150728_132237_create_admin extends Migration
{
    public function up()
    {
        $auth_key = Yii::$app->security->generateRandomString();
        $token = Yii::$app->security->generateRandomString() . '_' . time();
        $this->insert('{{%user}}', [
            'name'          => 'Admin',
            'surname'          => 'Admin',
            'email'         => 'admin@domain.com',
            'password' =>     Yii::$app->security->generatePasswordHash('Admin85'),
            'auth_key'         => $auth_key,
            'token'         => $token,
            'status' => '1',
            'group' => 'admin'
        ]);
    }

    public function down()
    {
        $this->delete('{{%user}}', ['email' => 'admin@domain.com']);
    }

}
