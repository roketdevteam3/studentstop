<?php

use yii\db\Migration;
use yii\db\Schema;


class m160303_153940_tbl_user_friend_request extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%user_friend_request}}', [
            'id' => Schema::TYPE_PK,
            'who_send' => Schema::TYPE_INTEGER,
            'whom_send' => Schema::TYPE_INTEGER,
            'status' => Schema::TYPE_INTEGER,
            'date_create' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT '0000-00-00 00:00:00' ",
            'date_update' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%user_friend_request}}');
    }

}
