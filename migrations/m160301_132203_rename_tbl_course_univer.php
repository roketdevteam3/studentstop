<?php

use yii\db\Migration;

class m160301_132203_rename_tbl_course_univer extends Migration
{
    public function up()
    {
        $this->renameTable('{{%major2course}}','{{%university_course}}');

    }

    public function down()
    {
        $this->renameTable('{{%university_course}}', '{{%major2course}}');
    }
}
