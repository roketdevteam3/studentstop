<?php

use yii\db\Migration;
use yii\db\Schema;

class m160203_234858_tbl_user_role extends Migration
{
    public function up()
    {
        $tableOptions = null;
        $this->createTable('{{%user_role}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_INTEGER,
            'status' => Schema::TYPE_INTEGER,
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable("{{%user_role}}");
    }

}
