<?php

use yii\db\Migration;
use yii\db\Schema;

class m160221_200126_tbl_major_niversity extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%major2university}}', [
            'id' => Schema::TYPE_PK,
            'id_major' => Schema::TYPE_INTEGER,
            'id_university' => Schema::TYPE_INTEGER,
        ], $tableOptions);

        $this->addForeignKey(
            '{{%major2university}}',
            '{{%major2university}}',
            'id_university',
            '{{%university}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

    }

    public function down()
    {
        $this->dropTable('{{%major2university}}');
    }
}
