<?php

use yii\db\Schema;
use yii\db\Migration;

class m150627_070601_create_table_user_and_add_admin extends Migration
{


    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%user}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'surname' => Schema::TYPE_STRING . ' NOT NULL',
            'password' => Schema::TYPE_STRING . ' NOT NULL',
            'auth_key' => Schema::TYPE_STRING . ' NOT NULL',
            'token' => Schema::TYPE_STRING . ' NOT NULL',
            'email' => Schema::TYPE_STRING . ' NOT NULL',
            'group' => Schema::TYPE_STRING . ' NOT NULL',
            'status' => Schema::TYPE_SMALLINT . ' DEFAULT 1 NOT NULL',
            'date_create' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT '0000-00-00 00:00:00'",
            'date_update' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
        ], $tableOptions);

        $this->createTable('{{%user_info}}', [
            'id' => Schema::TYPE_PK,
            'id_user' => Schema::TYPE_INTEGER,
            'network' => Schema::TYPE_INTEGER,
            'academic_status' => Schema::TYPE_STRING,
            'class' => Schema::TYPE_STRING,
            'birthday' => Schema::TYPE_STRING

        ], $tableOptions);
        $this->createIndex('email', '{{%user}}', 'email', true);

        $this->addForeignKey(
            '{{%id_user}}',
            '{{%user_info}}',
            'id_user',
            '{{%user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

    }

    public function safeDown()
    {
        $this->dropForeignKey("{{%id_user}}", "{{%user_info}}");
        $this->dropTable('{{%user}}');
        $this->dropTable('{{%user_info}}');
    }
}
