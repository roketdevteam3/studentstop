<?php

use yii\db\Migration;
use yii\db\Schema;

class m160309_222957_social_auth_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%auth}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'source' => Schema::TYPE_STRING . '(255) NOT NULL',
            'source_id' => Schema::TYPE_STRING . '(255) NOT NULL',
        ], $tableOptions);

        $this->addForeignKey(
            '{{%auth}}',
            '{{%auth}}',
            'user_id',
            '{{%user}}',
            'id'
        );
    }

    public function down()
    {
        $this->dropForeignKey('{{%auth}}', '{{%auth}}');
        $this->dropTable('{{%auth}}');
    }

}
