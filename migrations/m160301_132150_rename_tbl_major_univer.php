<?php

use yii\db\Migration;

class m160301_132150_rename_tbl_major_univer extends Migration
{
    public function up()
    {
        $this->renameTable('{{%major2university}}','{{%university_major}}');

    }

    public function down()
    {
        $this->renameTable('{{%university_major}}', '{{%major2university}}');
    }

}
