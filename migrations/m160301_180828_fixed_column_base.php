<?php

use yii\db\Migration;
use yii\db\Schema;

class m160301_180828_fixed_column_base extends Migration
{
    public function up()
    {
        $this->addColumn(
            '{{%major}}',
            'creator',
            Schema::TYPE_INTEGER
        );

        $this->addColumn(
            '{{%course}}',
            'creator',
            Schema::TYPE_INTEGER
        );

    }

    public function down()
    {
        echo "m160301_180828_fixed_column_base cannot be reverted.\n";

        return true;
    }


}
