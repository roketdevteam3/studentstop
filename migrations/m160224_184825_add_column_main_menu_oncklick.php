<?php

use yii\db\Migration;
use yii\db\Schema;

class m160224_184825_add_column_main_menu_oncklick extends Migration
{
    public function up()
    {
        $this->addColumn('{{%main_menu_item}}', 'onclick',  Schema::TYPE_STRING . ' DEFAULT "" ');
    }

    public function down()
    {
        $this->dropColumn('{{%main_menu_item}}', 'onclick');
    }
}
