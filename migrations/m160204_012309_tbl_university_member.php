<?php

use yii\db\Migration;
use yii\db\Schema;

class m160204_012309_tbl_university_member extends Migration
{
    public function up()
    {
        $tableOptions = null;
        $this->createTable('{{%university_member}}', [
            'id' => Schema::TYPE_PK,
            'id_user' => Schema::TYPE_INTEGER,
            'id_university' => Schema::TYPE_INTEGER,
            'date_create' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT '0000-00-00 00:00:00' ",
        ], $tableOptions);

        $this->addForeignKey(
            '{{%member_user}}',
            '{{%university_member}}',
            'id_user',
            '{{%user}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            '{{%member_university}}',
            '{{%university_member}}',
            'id_university',
            '{{%university}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

    }

    public function down()
    {
        $this->dropForeignKey("{{%member_user}}", "{{%university_member");
        $this->dropForeignKey("{{%member_university}}", "{{%university_member");
        $this->dropTable("{{%university_member}}");
    }

}
