<?php

use yii\db\Migration;
use yii\db\Schema;

class m160202_214705_tbl_university extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%university}}', [
            'id' => Schema::TYPE_PK,
            'id_owner' => Schema::TYPE_INTEGER,
            'name' => Schema::TYPE_STRING . '(255) NOT NULL',
            'history' => Schema::TYPE_TEXT,
            'status' => Schema::TYPE_INTEGER,
            'slug' => Schema::TYPE_STRING,
            'date' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT '0000-00-00 00:00:00' ",
            'date_create' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT '0000-00-00 00:00:00' ",
            'date_update' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ], $tableOptions);

        $this->createTable('{{%university_info}}', [
            'id' => Schema::TYPE_PK,
            'id_university' => Schema::TYPE_INTEGER,

        ], $tableOptions);

        $this->addForeignKey(
            '{{%id_university}}',
            '{{%university_info}}',
            'id_university',
            '{{%university}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey("{{%id_university}}", "{{%university_info}}");
        $this->dropTable('{{%university}}');
        $this->dropTable('{{%university_info}}');
    }

}
