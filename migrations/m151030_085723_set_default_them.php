<?php

use yii\db\Schema;
use yii\db\Migration;



class m151030_085723_set_default_them extends Migration
{
    const CACHE_KEY_THEME = 'site_theme';
    const CACHE_VALUE_THEME = 'basic';

    public function up()
    {
        $this->insert('{{%setting}}', [
            'key' => self::CACHE_KEY_THEME,
            'value' => self::CACHE_VALUE_THEME,
            'category' => 'design',
            'type_input'  => 'hidden'
        ]);
        Yii::$app->cache->set('setting_'.self::CACHE_KEY_THEME, self::CACHE_VALUE_THEME);
    }

    public function down()
    {
        Yii::$app->cache->delete('setting_'.self::CACHE_KEY_THEME);
        $this->delete('{{%setting}}', ['key' => self::CACHE_KEY_THEME]);
    }


}
