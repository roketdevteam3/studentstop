<?php

use yii\db\Migration;
use yii\db\Schema;

class m160224_181529_add_column_main_menu_status_user extends Migration
{
    public function up()
    {
        $this->addColumn('{{%main_menu_item}}', 'logged',  Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT true');
    }

    public function down()
    {
        $this->dropColumn('{{%main_menu_item}}', 'logged');
    }
}
