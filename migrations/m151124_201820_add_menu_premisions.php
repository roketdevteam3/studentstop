<?php

use yii\db\Schema;
use yii\db\Migration;
use app\rbac\checkUserPermissionRule;

class m151124_201820_add_menu_premisions extends Migration
{
    public function up()
    {
        $authManager = \Yii::$app->authManager;
        // Get roles
        $admin  = $authManager->getRole('admin');

        // Add rule, based on UserExt->group === $user->group
        $checkUserPermissionRule = new checkUserPermissionRule();

        $index  = $authManager->createPermission('administration/main-menu-item/index');
        $index->ruleName = $checkUserPermissionRule->name;

        $view = $authManager->createPermission('administration/main-menu-item/view');
        $view->ruleName = $checkUserPermissionRule->name;

        $create = $authManager->createPermission('administration/main-menu-item/create');
        $create->ruleName = $checkUserPermissionRule->name;

        $update = $authManager->createPermission('administration/main-menu-item/update');
        $update->ruleName = $checkUserPermissionRule->name;

        $delete = $authManager->createPermission('administration/main-menu-item/delete');
        $delete->ruleName = $checkUserPermissionRule->name;

        $status_on = $authManager->createPermission('administration/main-menu-item/on');
        $status_on->ruleName = $checkUserPermissionRule->name;

        $status_off = $authManager->createPermission('administration/main-menu-item/off');
        $status_off->ruleName = $checkUserPermissionRule->name;

        $order_up = $authManager->createPermission('administration/main-menu-item/up');
        $order_up->ruleName = $checkUserPermissionRule->name;

        $order_down = $authManager->createPermission('administration/main-menu-item/down');
        $order_down->ruleName = $checkUserPermissionRule->name;

        // Add permissions in Yii::$app->authManager
        $authManager->add($index);
        $authManager->add($view);
        $authManager->add($create);
        $authManager->add($update);
        $authManager->add($delete);
        $authManager->add($status_on);
        $authManager->add($status_off);
        $authManager->add($order_up);
        $authManager->add($order_down);

        //add permission user role
        $authManager->addChild($admin,$index);
        $authManager->addChild($admin,$view);
        $authManager->addChild($admin,$create);
        $authManager->addChild($admin,$update);
        $authManager->addChild($admin,$delete);
        $authManager->addChild($admin,$status_on);
        $authManager->addChild($admin,$status_off);
        $authManager->addChild($admin,$order_up);
        $authManager->addChild($admin,$order_down);
    }

    public function down()
    {
        echo "m151124_201820_add_menu_premisions cannot be reverted.\n";

        return false;
    }

}
