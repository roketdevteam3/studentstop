<?php


use yii\db\Schema;
use yii\db\Migration;
use app\rbac\checkUserPermissionRule;

class m151029_210018_create_default_role extends Migration
{
    public function up()
    {
       $authManager = \Yii::$app->authManager;
        // Create roles
        $guest  = $authManager->createRole('guest');
        $user  = $authManager->createRole('user');
        $admin  = $authManager->createRole('admin');
        $moderator  = $authManager->createRole('moderator');

         //Add roles
        $authManager->add($guest);
        $authManager->add($user);
        $authManager->add($admin);
        $authManager->add($moderator);

         // Add rule, based on UserExt->group === $user->group
        $checkUserPermissionRule = new checkUserPermissionRule();
        $authManager->add($checkUserPermissionRule);

        $login  = $authManager->createPermission('users/default/login');
        $login->ruleName = $checkUserPermissionRule->name;

        $signup  = $authManager->createPermission('users/default/signup');
        $signup->ruleName = $checkUserPermissionRule->name;

        $logout = $authManager->createPermission('users/default/logout');
        $logout->ruleName = $checkUserPermissionRule->name;

        $siteIndex = $authManager->createPermission('basic/site/index');
        $siteIndex->ruleName = $checkUserPermissionRule->name;

        $siteContact = $authManager->createPermission('basic/site/contact');
        $siteContact->ruleName = $checkUserPermissionRule->name;

        $siteAbout = $authManager->createPermission('basic/site/about');
        $siteAbout->ruleName = $checkUserPermissionRule->name;

        $error  = $authManager->createPermission('basic/site/error');
        $error->ruleName = $checkUserPermissionRule->name;

   // Add permissions in Yii::$app->authManager
        $authManager->add($login);
        $authManager->add($signup);
        $authManager->add($logout);
        $authManager->add($siteIndex);
        $authManager->add($siteContact);
        $authManager->add($siteAbout);
        $authManager->add($error);

       //add permission guest role
        $authManager->addChild($guest, $siteIndex);
        $authManager->addChild($guest, $siteContact);
        $authManager->addChild($guest, $siteAbout);
        $authManager->addChild($guest, $error);
        $authManager->addChild($guest, $login);
        $authManager->addChild($guest, $signup);

               //add permission user role
        $authManager->addChild($user, $siteIndex);
        $authManager->addChild($user, $siteContact);
        $authManager->addChild($user, $siteAbout);
        $authManager->addChild($user, $error);
        $authManager->addChild($user, $logout);

        //add permission user role
        $authManager->addChild($admin, $siteIndex);
        $authManager->addChild($admin, $siteContact);
        $authManager->addChild($admin, $siteAbout);
        $authManager->addChild($admin, $error);
        $authManager->addChild($admin, $logout);

        $authManager->addChild($moderator, $siteIndex);
        $authManager->addChild($moderator, $siteContact);
        $authManager->addChild($moderator, $siteAbout);
        $authManager->addChild($moderator, $error);
        $authManager->addChild($moderator, $logout);
        $authManager->addChild($moderator, $login);

     //assign user with user_id 1 as $admin
        $authManager->assign($admin, 1);




    }

    public function down()
    {
        echo "m151029_210018_create_default_roles cannot be reverted.\n";

        return true;
    }

}
