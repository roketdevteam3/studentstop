<?php

use yii\db\Migration;
use yii\db\Schema;

class m160221_200056_tbl_clase extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%class}}', [
            'id' => Schema::TYPE_PK,
            'id_course' => Schema::TYPE_INTEGER,
            'name' => Schema::TYPE_STRING . '(255) NOT NULL',
            'description' => Schema::TYPE_TEXT,
            'date_create' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT '0000-00-00 00:00:00' ",
            'date_update' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'status' => Schema::TYPE_BOOLEAN . " DEFAULT '1'"
        ], $tableOptions);

        $this->addForeignKey(
            '{{%class2course}}',
            '{{%class}}',
            'id_course',
            '{{%course}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey("{{%class2course}}", '{{%class}}');
        $this->dropTable("{{%class}}");
    }

}
