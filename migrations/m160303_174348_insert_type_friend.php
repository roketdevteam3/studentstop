<?php

use yii\db\Migration;

class m160303_174348_insert_type_friend extends Migration
{
    public function up()
    {

        $this->insert('{{%user_friend_type}}', [
            'title'          => 'Friend by class',
            'status'         => 1,
            'date_create' => date('Y-m-d H:i:s')
        ]);

        $this->insert('{{%user_friend_type}}', [
            'title'          => 'Friend professor',
            'status'         => 1,
            'date_create' => date('Y-m-d H:i:s')
        ]);

        $this->insert('{{%user_friend_type}}', [
            'title'          => 'Friend by university',
            'status'         => 1,
            'date_create' => date('Y-m-d H:i:s')
        ]);

        $this->insert('{{%user_friend_type}}', [
            'title'          => 'Other friend',
            'status'         => 1,
            'date_create' => date('Y-m-d H:i:s')
        ]);
    }

    public function down()
    {
        $this->delete('{{%user_friend_type}}', ['id' => 1]);
        $this->delete('{{%user_friend_type}}', ['id' => 2]);
        $this->delete('{{%user_friend_type}}', ['id' => 3]);
        $this->delete('{{%user_friend_type}}', ['id' => 4]);
    }
}
