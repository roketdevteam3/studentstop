<?php

use yii\db\Migration;
use yii\db\Schema;

class m160301_160037_create_table_university_majors_with_course extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%university_course_with_major}}', [
            'id' => Schema::TYPE_PK,
            'id_major' => Schema::TYPE_INTEGER,
            'id_university' => Schema::TYPE_INTEGER,
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%university_course_with_major}}');
    }
}
