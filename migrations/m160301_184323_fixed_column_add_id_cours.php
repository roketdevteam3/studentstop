<?php

use yii\db\Migration;
use yii\db\Schema;

class m160301_184323_fixed_column_add_id_cours extends Migration
{
    public function up()
    {
        $this->addColumn(
            '{{%university_course_with_major}}',
            'id_course',
            Schema::TYPE_INTEGER
        );
    }

    public function down()
    {
        echo "m160301_184323_fixed_column_add_id_cours cannot be reverted.\n";

        return true;
    }


}
