<?php

use app\components\MigrationPermission as Migration;

class m160224_203302_add_role extends Migration
{
    var $permissions = [
        'administration/page/index'=>['admin'],
        'administration/page/view'=>['admin'],
        'administration/page/on'=>['admin'],
        'administration/page/create'=>['admin'],
        'administration/page/off'=>['admin'],
        'administration/page/update'=>['admin'],
        'administration/page/delete'=>['admin'],
        'administration/page/up'=>['admin'],
        'administration/page/down'=>['admin'],
    ];

}
