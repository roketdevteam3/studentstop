<?php

use yii\db\Migration;
use yii\db\Schema;

class m160303_131002_tbl_class_user extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%class_user}}', [
            'id' => Schema::TYPE_PK,
            'id_class' => Schema::TYPE_INTEGER,
            'id_user' => Schema::TYPE_INTEGER,
            'date_join' => Schema::TYPE_TIMESTAMP . " NOT NULL DEFAULT '0000-00-00 00:00:00' "
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%class_user}}');
    }


}
