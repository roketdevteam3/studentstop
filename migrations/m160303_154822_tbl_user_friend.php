<?php

use yii\db\Migration;
use yii\db\Schema;

class m160303_154822_tbl_user_friend extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%user_friend}}', [
            'id' => Schema::TYPE_PK,
            'id_user' => Schema::TYPE_INTEGER,
            'id_friend' => Schema::TYPE_INTEGER,
            'type' => Schema::TYPE_INTEGER,
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%user_friend}}');
    }


}
