<?php

use yii\db\Migration;
use yii\db\Schema;

class m160218_001104_image_field_to_university extends Migration
{
    public function up()
    {
        $this->addColumn(
            '{{%university}}',
            'image',
            Schema::TYPE_STRING
        );
    }

    public function down()
    {
        $this->dropColumn("{{university}}", "image");
    }

}
