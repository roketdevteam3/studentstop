<?php

use app\rbac\checkUserPermissionRule;
use yii\db\Schema;
use yii\db\Migration;

class m160205_210938_role_university_user_role extends Migration
{
    public function up()
    {
        $authManager = \Yii::$app->authManager;
        // Get roles
        $admin  = $authManager->getRole('admin');

        // Add rule, based on UserExt->group === $user->group
        $checkUserPermissionRule = new checkUserPermissionRule();

        $index  = $authManager->createPermission('administration/user-role/index');
        $index->ruleName = $checkUserPermissionRule->name;

        $create = $authManager->createPermission('administration/user-role/create');
        $create->ruleName = $checkUserPermissionRule->name;

        $update = $authManager->createPermission('administration/user-role/update');
        $update->ruleName = $checkUserPermissionRule->name;

        $delete = $authManager->createPermission('administration/user-role/delete');
        $delete->ruleName = $checkUserPermissionRule->name;

        // Add permissions in Yii::$app->authManager
        $authManager->add($index);
        $authManager->add($create);
        $authManager->add($update);
        $authManager->add($delete);

        //add permission user role
        $authManager->addChild($admin,$index);
        $authManager->addChild($admin,$create);
        $authManager->addChild($admin,$update);
        $authManager->addChild($admin,$delete);
    }

    public function down()
    {
        echo "m160205_210938_role_university_user_role cannot be reverted.\n";

        return false;
    }

}
